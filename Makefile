DOCKER_COMMAND=docker
DOCKER_COMPOSE=$(DOCKER_COMMAND) compose -f docker-compose.yml -f docker-compose.dev.yml
APPLICATION_CONTAINER_NAME=serveur
EXEC_COMPOSER=$(DOCKER_COMPOSE) exec $(APPLICATION_CONTAINER_NAME) composer

.DEFAULT_GOAL := help
.PHONY: help

DOCKER_COMPOSE_UP=$(DOCKER_COMPOSE) up -d

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean: clean-backend clean-frontend## Clear and remove dependencies

clean-backend: ## Clear and remove backend dependencies
	rm -rf vendor

clean-frontend: ## Clear and remove frontend dependencies
	rm -rf frontend/dist
	rm -rf frontend/node_modules
	rm -rf frontend/.angular


docker-compose-up: ## Up all container
	$(DOCKER_COMPOSE_UP)

start:  ## Start all services
	$(DOCKER_COMPOSE) up -d --remove-orphans

stop: ## Stop all services
	$(DOCKER_COMPOSE) down

build: build-collabora build-nginx ## Build the application containers

build-collabora: ## Build the collabora container
	$(DOCKER_COMPOSE) build collabora

build-nginx: build-frontend ## Build the nginx container
	$(DOCKER_COMPOSE) build reverse

build-frontend: ## Build the files needed for the frontend
	$(DOCKER_COMPOSE) build frontend-watcher
	$(DOCKER_COMPOSE) run frontend-watcher npm install
	$(DOCKER_COMPOSE) run frontend-watcher ng build

bash: docker-compose-up ## Get a bash console in the application container
	$(DOCKER_COMPOSE) exec  $(APPLICATION_CONTAINER_NAME) bash

logs: docker-compose-up ## Display last application logs (in follow mode)
	$(DOCKER_COMPOSE) logs --tail 50 -f $(APPLICATION_CONTAINER_NAME)

test: hadolint phpcs phpunit phpmd ## Run all tests (code style, unit test, ...)

hadolint: ## Test the Dockefile
	$(DOCKER_COMMAND) run --rm -i hadolint/hadolint < Dockerfile

phpcs: docker-compose-up ## Check code style through docker compose
	$(EXEC_COMPOSER) phpcs

phpcbf: docker-compose-up ## Fix all code style errors
	$(EXEC_COMPOSER) phpcbf

phpunit: docker-compose-up ## Run unit test through docker compose
	$(EXEC_COMPOSER) phpunit

phpmd: docker-compose-up ## Run mess detector through docker compose
	$(EXEC_COMPOSER) phpmd

phpdd: docker-compose-up ## Run deprecation detector through docker compose
	$(EXEC_COMPOSER) phpdd
