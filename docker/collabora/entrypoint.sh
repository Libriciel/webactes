#! /bin/bash

if [[ -z "${ENABLE_LOCAL_COLLABORA}" ]]
then
  echo "ENABLE_LOCAL_COLLABORA is not set. Sleeping indefinitely."
  sleep infinity
fi
env
export dictionaries=fr_FR
export extra_params="--o:ssl.enable=true --o:welcome.enable=false --o:remote_config.remote_url=${EDITOR_WOPI_COLLABORA_REMOTE_CONFIG_URL}"

echo $extra_params;

echo "Execute command : " "$@"

exec "$@"
