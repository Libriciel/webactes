# Conteneur nginx "reverse" pour webactes

## Variables d'environnement activable

| Nom de la variable | Signification                                                    |
|--------------------|------------------------------------------------------------------|
| DEV_MODE           | Permet d'activer la mise à jour automatique de la partie angular |
| ENABLE_COLLABORA   | Permet d'activer Collabora                                       |
| ENABLE_MYEC3       | Permet d'activer MyEC3 (fonction déprécié en 2.0.17)             |


## Certificats

Si le conteneur trouve un certificat dans /etc/nginx/ssl alors, il ne fait rien. Sinon, il en génère un certificat autosigné.

Il est donc conseillé de créer un volume afin que les certificats ne soient pas créé à chaque lancement du conteneur.


## Certificats let's encrypt

Si la variable d'environnement ENABLE_LETS_ENCRYPT est défini alors le conteneur va lancer un challenge acme sur le port 443 avant le lancement de nginx.
Il est nécessaire de spécifier un email dans LETS_ENCRYPT_EMAIL afin de recevoir les notifications de renouvellement.

Il est également nécessaire de créer un volume sur `/etc/letsencrypt` afin de ne pas perdre les certificats
