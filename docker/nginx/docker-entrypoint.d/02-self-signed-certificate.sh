#! /bin/bash

NGINX_CERTIFICATE_DIRECTORY=/etc/nginx/ssl/
KEY_NAME=privkey.pem
CERT_NAME=fullchain.pem
OPENSSL_CONFIG_TEMPLATE=/var/local/openssl-certificate-extension.cnf


PRIVATE_KEY_PATH=${NGINX_CERTIFICATE_DIRECTORY}/${KEY_NAME}
CERTIFICATE_PATH=${NGINX_CERTIFICATE_DIRECTORY}/${CERT_NAME}


if [ -f ${PRIVATE_KEY_PATH} ]
then
    echo "Le fichier ${PRIVATE_KEY_PATH} existe déjà [PASS]"
    exit 0
fi;

if [ -f ${CERTIFICATE_PATH} ]
then
    echo "Le fichier ${CERTIFICATE_PATH} existe déjà [ERROR]";
    exit 1;
fi;

echo "Le certificat du site n'a pas été trouvé, on en génère un"

OPENSSL_CONFIG_PATH=/tmp/openssl.cnf
sed  "s/%SERVER_NAME%/${SERVER_NAME}/" ${OPENSSL_CONFIG_TEMPLATE} > ${OPENSSL_CONFIG_PATH}


CSR_TEMP_PATH=/tmp/$$_csr.pem

openssl req  \
        -new \
        -newkey rsa:4096 \
        -days 825 \
        -nodes  \
        -subj "/C=FR/ST=HERAULT/L=MONTPELLIER/O=LIBRICIEL/OU=CERTIFICAT_AUTO_SIGNE/CN=${SERVER_NAME}/emailAddress=test@localhost" \
        -keyout ${PRIVATE_KEY_PATH} \
        -out ${CSR_TEMP_PATH}

if [ $? -ne 0 ]
then
echo "Problème lors de la génération du CSR"
exit 4
fi

openssl x509 \
        -req \
        -days 825 \
        -in ${CSR_TEMP_PATH} \
        -signkey ${PRIVATE_KEY_PATH} \
        -out ${CERTIFICATE_PATH} \
        -extfile ${OPENSSL_CONFIG_PATH}

if [ $? -ne 0 ]
then
echo "Problème lors de la signature du certificat"
exit 5
fi

rm ${CSR_TEMP_PATH}
rm ${OPENSSL_CONFIG_PATH}

chmod 400 ${PRIVATE_KEY_PATH}

echo "Génération de la clé et du certificat terminé"
exit 0
