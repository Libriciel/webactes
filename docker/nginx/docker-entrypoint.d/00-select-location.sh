#!/bin/bash

mkdir /etc/nginx/location.d/

cp /var/local/location.d/common/* /etc/nginx/location.d/

for dir in /var/local/location.d/*; do
   dir_name=$(basename $dir)
   if [[ $dir_name == "common" ]]; then
       break
   fi
   if [[ -n "${!dir_name}" ]]; then
       echo "$dir_name is active"
       cp $dir/* /etc/nginx/location.d/
   fi
done
