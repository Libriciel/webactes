#! /bin/bash

envsubst < /var/www/html/dist/webact/assets/env.template.js > /var/www/html/dist/webact/assets/env.js

if [[ -f /var/www/html/src/assets/env.template.js ]]; then
    echo "Inject environnement in frontend src for development..."
    envsubst < /var/www/html/src/assets/env.template.js > /var/www/html/src/assets/env.js
fi
