# Changelog

*Les changements notables du projet seront documentés dans ce fichier.*

Le format est basé sur [Tenez un Changelog](https://keepachangelog.com/fr/1.0.0/) et ce projet suit la [Gestion sémantique de version](https://semver.org/lang/fr/spec/v2.0.0.html).

## [2.0.17]

### Corrections

- Permettre de récupérer la totalité des enregistrements sur certains champs de selection #1432
- Les dates se généraient en anglais dans les modèles #1424
- Le terme "ordre du jour" a été remplacé partout par le terme "note de synthèse" #1422
- Un administrateur ne doit pas pouvoir modifier un super administrateur #1095
- Un secrétaire ne peut pas visualiser un projet depuis l'écran de vote #1135
- Les mots de passe précédemment sauvegardé dans les connecteurs ne sont plus récupéré sur l'interface #1421
- La suppression d'une annexe sur webactes avant envoi iparapheur est désormais synchronisée sur Pastell #915
- La suppression d'une annexe n'est pas prise en compte lors de l'envoi de la séance à idelibre #1381
- Gestion de la pagination dans le menu administration > structures #1439
- Lorsqu'une annexe est remplacée cela ne sauvegarde pas la typologie de l'annexe #1431
- Permettre au service beanstalk de se relancer automatiquement #1444
- Cacher le choix "délibérant" sur les projets d'actes quand la mise en séance est désactivée #1037
- Permettre la modification d'un type d'acte lorsqu'il n'a plus de gabarit lié #1468

## Évolutions

- Séance : pouvoir renseigner le lieu de séance #1385
- Amélioration du paramétrage de recette (ajout de S3, S4, MASSIVE, nettoyage) #1125
- Amélioration du système de gestion des droits #1437
- fusion des docker-compose.yml utilisés pour le développement et la production #1409
- Création d'un docker nginx #1418
- Utilisation de volume nommé pour les certificats et let's encrypt et fixation des chemins dans le fichier .env.dist de production #1429
- Ajouter une variable de fusion pour le lieu de séance

## Suppression

- La directive NGINX_VHOST est supprimé car obsolète #1418
- La directive CERTBOT_WWW est supprimé au profit de LETS_ENCRYPT_VOLUME #1418

## Ajout

- Ajout des directives du `.env` : ENABLE_LETS_ENCRYPT, LETS_ENCRYPT_EMAIL et LETS_ENCRYPT_VOLUME #1418
- Ajout de la directive ENABLE_LOCAL_COLLABORA qui si elle n'est pas vide active un conteneur avec Collabora #1427
- Ajouter une variable de fusion pour le lieu de séance #1405

## Securité

- Mise à jour des paquets du Dockerfile Ubuntu 22.04 au 06/06/2024 #1441
- Mise à jour des paquets php8.1 (3 CVE) et poppler-utils au 10/05/2024 #1430

## [2.0.16] - 2024-04-24

### Corrections

- Fixer la version de libreofficedocker/libreoffice-unoserver suite au problème d'installation en 2.0.15 #1419
- Impossible de sélectionner un mandataire via chrome #1420
- Correction du problème d'encodage des classifications sur les plateformes s2low de formations #1423

## [2.0.15] - 2023-04-17

### Corrections

- Les éléments "Retraits" n'apparaissaient pas dans la partie "Versions" de la page "À propos - Aide"

### Evolutions

- Modification des termes "PV sommaire" en "Liste des délibérations" et "PV détaillé" en "Procès-verbal" &19
- Création d'un champ date de convocation, depuis le menu création de séance #1010
- Les noms de fichiers à télécharger sont rendus uniques et plus explicites en fonction de leur type, de leur identifiant, ou de leur date (en fonction du contexte) #1397
- Ajout de la variable secrétaire de séance dans le modèle d'acte #1392
- Simplification du démarrage de la pile docker pour le développement (.env.dist plus simple et valeur par défaut reporté dans le fichier docker-compose.yml) #1401

### Suppression

- Suppression du gabarit de synthese #1404

## [2.0.14] - 2023-03-13

### Suppressions

- Suppression de la possibilité de modifier l'information "Envoi de documents papiers complémentaires"[#914](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/914)

### Corrections

### Evolutions

- Enlever les liens des issues dans le changelog #1398

## [2.0.14] - 2023-03-13

### Suppressions

- Suppression de la possibilité de modifier l'information "Envoi de documents papiers complémentaires" #914

### Corrections

- Remplacer le document principal avant signature #647
- Structure sans génération : le document d'acte n'est plus visible #1341
- Le bouton annuler une séance ne fonctionne pas #1207
- Récupération des rôles à la création d'utilisateurs #1231
- Affichage des séquences limité à 10 en création et modification de compteur #1260
- Classement des compteurs par noms #1259
- Affichage icône "prend acte" dans l'historique du projet #1221
- Gestion de l'unicité du code acte #1046
- Recherche multi-critères : amélioration affichage types d'actes #1202
- Le numéro d'acte ne peut pas commencer par des numéros précédés d'underscore #1203
- Problème d'affichage du placard à balais #1257
- Régression : impossible de modifier un projet sur une structure où l'édition collaborative n'est pas activée#1261
- Modification de la séquence des compteurs #1204
- Problème d'affichage de champ heure dans la création / modification d'une séance#1216
- Modification de la séquence des compteurs #1204
- Amélioration sur les filtres #1164
- Homogénéiser la taille de la colonne "libellé" dans les différents écrans #1339
- Typologie manquante dans pastell pour dépôt TDT lors de la modification avant envoi #1363

### Evolutions

- Rendre obligatoire les modèles à la création/modification d'un type d'acte #1258
- Amélioration des services de génération #1224
- Le typage d'une annexe n'est pas pris en compte en modification de projet si retiré #1209
- Prise en compte d'un collabora externe dans la configuration nginx du client #1384
- Technique, partie serveur, passage en PHP 8.1 #1078
- Technique, partie serveur, passage du docker en Ubuntu 22.04 #1077
- Technique, partie serveur, passage en CakePHP 4.4 #1080
- Evolutions techniques mineures #1164 #1192
- Utilisation d'un service externe de conversion d'annexes PDF en ODT, plusieurs conversions sont à présent faites simultanément (par défaut: 4)#1197]
- Migration Angular #1079
- Politique de redémarrage des dockers #1365 : Ajout d'un bouton "Annuler" dans la fenêtre de confirmation de suppression d'un type d'acte

## [2.0.13] - 2023-10-10

### Corrections

- Affichage des acteurs mandants et mandataires dans le document d'acte #1219

## [2.0.12] - 2021-07-21

### Ajouts

- Personnalisation du spinner modal #1155

### Evolutions

- Amélioration de la gestion des erreurs de génération, vérifications et limitations à 1000 pages par projet pour les annexes jointes à la fusion #1183
- Restriction sur clôture séance #953
- Empêcher l'envoi d'un mail sécurisé si la convocation et l'odj n'ont pas été générés #1165
- Changer le libellé "Statut de reception" en "Statut de lecture" sur l'écran de suivi d'envoi des convocations#1160
- Modification de la page d'envoi des documents de séance via mail sécurisé #990]
- Empêcher l'envoi d'un mail sécurisé si la convocation et l'odj n'ont pas été générés#1165
- Changer le libellé "Statut de reception" en "Statut de lecture" sur l'écran de suivi d'envoi des convocations#1160
- Mauvais comportement du bouton "enregistrer" lors de l'envoi des documents de la séance #990
- Restriction sur clôture séance #953
- Prise en compte de la modification d'un acteur au vote #1096
- Notifications utilisateurs non fonctionnelles #1028
- Précision d'aide sur les formats d'annexe autorisés en modification de projet #1112
- Amélioration des performances concernant "Séances en cours > Envoyer la séance" et "Séances en cours > Ordre du jour de la séance" #1120
- Mise en place de la dernière version de collabora online 22.05.14.3.1 #1106
- Amélioration des performances concernant la récupération des séances #1066
- Si les éléments de vote n'ont pas été renseignés, l'enregistrement du vote est impossible #1094

### Corrections

- L'ordre des annexes est désormais correctement pris en compte #1191
- Un valideur ne peut valider qu'une seule fois un projet, après une demande de confirmation #870
- Un secrétaire ne peut pas modifier la liste des élus présents#1148
- Ordre des séances incohérent depuis les séances en cours#1131
- PDF corrompu lorsque l'on utilise des variables president_seance_..., secretaire_seance_... et president_ #1108
- Problème sur le vote par groupe d'acteur au détail des voix#1100
- Le super admin n'a pas accès à tous les menus #1089
- Typologie par défaut #1069
- Ordre du jour en double lors de la génération de la séance #1054
- Différence de comportement de l'ordre des projets selon l'étape à laquelle on rattache le projet à la séance #994
- Problème de décompte élus présents #973
- Certaines actions ne relancent pas la génération des actes #1067
- Impossible de voter un projet s'il n'a pas été rattaché à une séance dès sa création #1107
- Restriction mandat pour acteur absent #1158
- Ne pas pouvoir sélectionner un président absent #1159
- Le filtre d'un utilisateur ne fonctionne pas avec le champ Login #1145
- Modification possible du mode de vote #1035 et #1153
- Bouton de création ou de modification d'un type d'acte grisé si pas de compteur à renseigner #1098
- Notifications utilisateurs non fonctionnelles #1028
- La variable `annexes_nombre` ne renvoie pas la bonne valeur #1146
- Liste des acteurs erronée pour la sélection du président et secrétaire de séance" #1141
- Correction effectuée sur le nombre d'annexes et l'incorporation des annexes en génération #1087
- Libellé du projet tronqué dans les modales de signature #1043
- Récupération de la liste de présence précédente #1092
- Comportement identique au niveau des rôles lors de la création d'une structure, que ce soit via import CSV ou via Pastell : rôle "Super Administrateur" manquant via Pastell, possibilité d'utiliser le rôle "Secrétariat général" (`secretariat_general`) lors de l'import CSV #1101

## [2.0.11] - 21/04/2023

### Evolutions

- Optimisation de l'affichage des séances #1066
- Ajout des variables disponibles pour la fusion concernant les présidents de séance, secrétaire de séance, président·e de vote #1097

### Corrections

- Les variables `acte_signature` et `acte_signature_date` sont à présent bien envoyées à la fusion des PV suite à une signature du Parapheur #1075
- La structure d'un utilisateur est changée lorsque son rôle est modifié par un super administrateur #1049
- Typologie des actes par défaut #1069
- Mauvais calcul du résultat du vote en cas de vote au total des voix #1055
- Génération de projet possible même si non configuré dans la structure #996
- Rattachement du super administrateur à la structure renseignée lors de sa création #1036
- Un rédacteur/valideur a accès aux menus séances #980
- La génération du numéro d'acte avec la position dans la définition du compteur ne renvoi pas le numéro de position #1073
- Récupération de la liste des projets d'une séance #1071
- Correction du décalage de la date de consultation d'un mail sécurisé et ajout de la possibilité de spécifier l'adresse mail qui figurera dans le champ `to` des mails sécurisés Pastell, ajout des variables d'environnement `PASTELL_TIMEZONE` et `PASTELL_MAILSEC_TO` #910
- Les tâches automatisées ne se lancent plus ou n'aboutissent plus en 2.0.10 #1074

### Suppressions

- Liste de présence d'une délibération précédente #1013

## [2.0.10] - 11/04/2023

### Corrections

- La modification d'une séance duplique l'ordre du jour #998
- Le vote est proposé dans un type de séance non délibérante #992
- Mauvaise date lors de la signature manuelle #1056
- Erreur sur les variables acteurs mandatés / mandataires #1058
- La prise d'acte doit numéroter le projet #1030
- Suppression du document principal dès la modification du projet #1021
- La date de l'acte correspond à la date de dépôt sur le TdT au lieu de correspondre à la date de signature de l'acte ou à la date de séance #1002
- Restriction de doublons sur les types de séance et types d'acte #924
- Message d'erreur explicite en cas de mail déjà utilisé par un autre acteur #960
- Bouton enregistrer grisé à la modification de séquence #918
- Le thème parent ne s'affichait pas lorsqu'on éditait un thème enfant #841
- Mauvais nom d'un menu dans la fenêtre de confirmation de suppression d'une séance #1029
- Le document principal affiche maintenant un message si une génération est en cours #971
- La modification de l'ordre du jour n'est pas prise en compte en génération et envoi vers Idelibre #1019
- Restriction doublon sur types d'actes et types de séances #924
- Notifications reçues sans être activées #969
- Autoriser les caractères spéciaux dans le nom d'étape de circuit #1006
- La définition du compteur doit respecter le protocole @cte #995
- Un acteur désactivé est destinataire de la convocation et de l'ordre du jour #987
- Un rédacteur valideur ne peut pas renseigner la séance en création de projet #964
- Le mail d'un utilisateur doit avoir un format valide #948
- Affichage du nombre de séances limité en création de projet #1003
- Ne pas faire apparaître les 'documents TDT' lorsque le type d'acte n'est pas transmissible #991
- Problème d'historique et absence de message de confirmation lors de l'envoi en signature #866
- Génération projet : document Txt au lieu de pdf #976
- Problème de décompte élus présents #973
- Cloisonner la modification de la configuration aux administrateurs et super administrateurs #950
- Groupe d'acteurs obligatoire pour les types de séances #925
- Des paramètres sont sauvegardés (en affichage) même en cliquant sur annuler #999
- La définition du compteur doit respecter le protocole @cte #995
- Accès au menu séances pour le secrétariat général #882
- Champs téléphone non obligatoire pour les acteurs #962
- Empêcher la création d'utilisateurs avec le même login #944
- Un acteur désactivé ne peut plus être sélectionné comme président/secrétaire de séance #951
- Basculement de compte super administrateur vers administrateur accès à une page interdite avec blocage #938
- Modification de la politique de confidentialité #934
- Recherche des utilisateurs par le nom et le prénom à la création d'un circuit #946
- Contrôle du format du numéro de téléphone à la création d'un acteur #939
- Suppression logo de la sidebar #917
- Ne plus pouvoir se connecter avec un utilisateur désactivé #952
- Permettre uniquement les pdf en fusion documentaire #928
- Acte non délibérant numéroté à l'état validé #985
- Recherche multicritère limitée à la structure de l'utilisateur #993
- Modification des projets validés par tous les utilisateurs concernés #965
- Création de connecteur Idelibre #982
- Impossible de visualiser un utilisateur rattaché à une structure importée depuis pastell en tant que super administrateur #972
- Respect de la casse à l'enregistrement du prénom d'un utilisateur #936
- Mauvais utilisateur tracé dans l'historique d'un projet #958
- Rendre possible la création de plusieurs utilisateurs ayant le même mail #937
- Affichage de la structure du super admin #929
- Intégration du nouveau logo Libriciel SCOP #911
- Relance automatique de la génération si une erreur s'est produite #875
- Synchronisation des annexes de webactes vers pastell #913
- Suppression et modification de convocation et ordre du jour #902
- Un acteur désactivé apparait tout de même dans la liste des votants#868
- Interdire une définition de compteur de plus de 15 caractères #828
- La date de signature n’apparaît pas lors des générations #865
- Ajouter les retours de mail sécurisé à la supervision #910
- Les mails de notification ne prennent pas la configuration smtp en compte #846
- Erreur d'envoi au TDT et certains actes ne peuvent pas être envoyés en préfecture #906
- Impossible de le renvoyer dans un circuit après modification #871
- Prise en compte des modifications d'un projet après un refus Iparapheur#886
- Le bouton suivant reste grisé malgré si aucune séance n'existe #884
- Même modèle que la convocation au niveau de l'ordre du jour #901
- Génération des documents de convocation #800
- Modification de gabarit #867

## [2.0.9] - 28/10/2022

### Corrections

- Correction la gestion des actions sur les utilisateurs qui impactait la synchronisation via socle #863

## [2.0.8] - 01/07/2022

### Corrections

- Permettre d'avoir uniquement le document de convocation pour le mail sec #840

## [2.0.7] - 21/06/2022

### Corrections

- Message d'erreur en création de projet non pertinent

## [2.0.6] - 17/06/2022

### Evolutions

- Améliorations techniques

## [2.0.5] - 02/06/2022

### Evolutions

- Amélioration des requêtes Pastell

### Corrections

- Amélioration de la procédure d'installation

## [2.0.4] - 30/05/2022

### Ajouts

* Affichage message d'erreur si définition de compteur supérieure à 15
  caractères #828]

### Evolutions

- Créer / modifier un utilisateur #814
- Vérification de la sécurité du conteneur #834

### Corrections

- Spinner de pagination #773
- Pagination séances #820
- Création doublon type de séances #821
- Toaster de confirmation et mise à jour projet #621

## [2.0.3] - 08/12/2021

### Corrections

- Impossible d'envoyer un acte sur le parapheur #818
- Impossible de créer un nouveau thème portant le même libellé qu'un thème supprimé #819

## [2.0.2] - 10/11/2021

### Corrections

- Récupération du type de connecteur parfois erroné #817

## [2.0.1] - 25/10/2021

### Corrections

- Socle MYEC3 Lors d'un update créer une structure si celle-ci n'existe pas #816

## [2.0.0]

### Ajouts

- Voter un projet #609
- Générer un document #610
- L'éditeur en ligne des textes de projet #608
- Créer/Modifier des gabarits de projet #607
- Créer/Modifier un type de séance #107
- Effectuer une recherche multi-critères #53
- Ajout des gabarits de projets dans la rédaction des projets #679
- Placement des documents à générer #651
- Génération du numéro d'acte #666
- Effectuer une recherche multi-critères #53
- Ajout des gabarits de projets dans la rédaction des projets #679
- Ajout des gabarits de projets dans les types d'actes #678
- Intégration de l’éditeur dans l’application #668
- Créer/Modifier un gabarit de projet #677
- Créer/Modifier un modèle de document #649
- Migration cakePHP 4.2 #664
- Mise en place du moteur de génération #652
- Créer/Modifier un groupe d'acteur #201
- Créer/Modifier un thème #605
- Créer/Modifier une séquence #624
- Créer/Modifier un compteur #623
- Recherche multi-critères #53
- Récupérer les structures créées dans Pastell #682

### Corrections

- Impossible d'accéder aux pages de l'application #613
- Impossible d'effacer une classification #469
- Erreur d'orthographe dans variable d'environnement HTTP_PROXY_IP_ADDRESS #604
- Rendre les actes non transmissibles, transmissible #600
- Problème upload d'une image non png #518
- Ne pas limiter le choix des images aux formats pgn lors du choix du logo de la collectivité #614
- Menu "à ordonner" : changer l'ordre des colonnes #440
- Envoi sur pastell acte signé électroniquement #622

## [1.1.5] - 06/01/2021

### Corrections

- L'échec de la mise à jour de la classification initiale #661

## [1.1.4] - 01/12/2020

### Corrections

- Impossible de récupérer le nouvel état PASTELL d'un document s'il y a eu une erreur au préalable #632

## [1.1.3] - 16/11/2020

### Corrections

- Problème d'envoi des typologies vers pastell #628

## [1.1.2] - 12/11/2020

### Corrections

- Impossible d'accéder aux pages de l'application (patch-1) #613

## [1.1.1] - 01/10/2020

### Ajouts

- Ne pas limiter le choix des images aux formats pgn lors du choix du logo de la collectivité #614

### Corrections

- Impossible d'accéder aux pages de l'application #613
- Impossible d'effacer une classification #469
- Erreur d'orthographe dans variable d'environnement HTTP_PROXY_IP_ADDRESS #604
- Rendre les actes non transmissibles, transmissible #600
- Problème upload d'une image non png #518
- Ne pas limiter le choix des images aux formats pgn lors du choix du logo de la collectivité #614
- Menu "à ordonner" : changer l'ordre des colonnes #440
- Envoi sur pastell acte signé électroniquement #622

## [1.1.0] - 24/07/2020

### Ajouts

- Permettre l'utilisation d'un proxy #576
- Mise en place des notifications. #562 #563 #550
- Ajout de la page "à propos" #457
- Gestion des abonnements aux notifications #562, #563, #550

### Corrections

- Pour les écrans à faible hauteur, liste des types d'annexe invisible #577
- Problème d'envoi en signature si les typologies ne sont pas correctes #603
- Mise à jour de la classification #588
- Typologie perdue lorsqu'il y a des annexes transmissibles #578
- Choix validateur paginé #492]
- Problème sur la cohérence nature, typologie et type d'acte #582
- Mauvaise typologie "Actes Individuels" #581
- Impossible de récupérer les sous types IP pour le profil secrétariat général #595

## [1.0.2] - 11/06/2020

### Corrections

- Correction sur la typologie "Actes Individuels". #581
- Correction sur la cohérence nature, typologie et type d'acte. #582

## [1.0.1] - 04/06/2020

### Corrections

- Correction d'un bug sur l'envoi des typologies sur PASTELL v3 pour les annexes. #578
- Correction d'un sur la possibilité de supprimer un projet/acte dans l'état "à déposer sur le tdt et acquitté" #579
- Correction pour la récupération de la classification. #575

### Ajout

- Ajout du contexte des logs affichés.

## [1.0.0-1.0.22] - 24/02/2020

### Corrections

- Correction bug historique pour circuits supprimés #567

## [1.0.0-1.0.21] - 20/02/2020

### Ajouts

- Logs systématiques des interactions Pastell #549

## [1.0.0-1.0.20] - 11/02/2020

### Corrections

- Ajout de suivi des redirections
- Correction renvoi au I-Parapheur

## [1.0.0-1.0.19] - 11/02/2020

### Corrections

- Correction des bugs de relation WA<->PA

## [1.0.0-1.0.18] - 31/01/2020

### Ajouts

- Ajout de l'historique du parapheur #131
- Numéro d'acte - transformés en _

### Modifications

- Composant wa-button
- Présélection des circuits

### Corrections

- Entrée d'api pour connaître le menu d'un projet #478
- Corrections mineures sur certains boutons #541
- Action d'envoyer en circuit disponible en brouillon
- Correction des bugs de relation WA<->PA

## [1.0.0-1.0.17] - 21/01/2020

### Corrections

- Mauvaise date de décision envoyée au TDT #493
- Message erreur si document pas présent sur Pastell
- L'état envoi parapheur dès réception de la requête

## [1.0.0-1.0.16] - 21/01/2020

### Ajouts

* Mauvaise date de décision envoyée au TDT #493
* Readme correct #538

### Corrections

- Bugs sur la signature via I-Parapheur
- Menu du placard à balais par-dessus les pager #394

## [1.0.0-1.0.15] - 15/01/2020

### Modifications

- Logs supplémentaires pour la signature parapheur
- Ajout de priorités sur les actions beanstalk

## [1.0.0-1.0.14] - 14/01/2020

### Ajouts

- Logs supplémentaires pour la signature parapheur

## [1.0.0-1.0.13] - 20/12/2019

### Ajouts

- Signature via I-Parapheur
- Historique des actes #131

### Modifications

- Restructuration des menus #482
- Le type pdf n'est obligatoire qu'à la signature de l'acte #505
- Passage à PHP 7.3
- Message explicite en cas d'erreur de connexion à Pastell #512
- Entrée API pour connaître l'état d'un projet #478
- Restructuration des menus #482
- Amélioration de la saisie des heures et dates des séances #350
- Migration vers fontawesome 5

### Corrections

- Messages des champs de sélection vides en français #353
- Date de l'acte poussée sur slow correcte
- Permettre la sélection d'un utilisateur s'il y en a plus de 20 dans une collectivité #492
- Circuit non modifiable lorsque au moins un projet y a été instancié #495

## [1.0.0-1.0.12] - 14/11/2019

### Evolutions

- Actions unitaires en opacité réduite

## [1.0.0-1.0.11] - 13/11/2019

### Corrections

- Disparition d'historique pour les projets validés #491

## [1.0.0-1.0.10] - 13/11/2019

### Evolutions

- Meilleure gestion d'un projet sorti d'un circuit #477

### Modifications

- Montées de version des librairies Angular

### Corrections

- Mauvais droits pour l'édition de projets ou d'actes #472
- Validation en urgence valide un autre projet #488

## [1.0.0-1.0.9] - 04/11/2019

### Ajouts

- Changelog correct

### Evolutions

- changement de label CONNEXION_PASTELL_SUCCESS_MESSAGE
- changement de label CONNEXION_PASTELL_HELP
- Hover vert sur les titres clickable + modifications css mineures
- Ajout du nom du validateur pour la validation en urgence #461

### Corrections

- Breadcrumb de visualisation pour les actes #408
- Date de l'acte erronée et fixée au 11 février 2019 #474
- Les rédacteurs ne peuvent pas sélectionner de type ni de séances #475

## [1.0.0-1.0.8] - 30/10/2019

### Corrections

- Accès correct à la BDD flowable en test

## [1.0.0-1.0.7] - 29/10/2019

### Ajouts

- Visualisation au clic du libellé #460
- Admin fonctionnel peut valider en urgence
- Synchronisation du socle Worldline

### Evolutions

- Renommer le bouton déclarer #462
- Refonte des couleurs de l'application #275
- Amélioration visibilité commentaire #461
- Refonte de la barre de filtre #301

### Corrections

- Erreurs du serveur avec code http 500 #454
- Initialisation des droits corrects pour les structures #455
- Api correctement restreinte en fonction des rôles #439
- Amélioration des performances dans l'utilisation de flowable
- Utilisation de l'info panel partout
- Erreur orthographe Validation en urgence #463
- Liste déroulante : effacer en fr #361
- Effacement des personnes dans un circuit provoque la fermeture de la fenêtre "Popup"
- Correction orthographique "Confirmation de suppression d'étape dans un circuit"

## [1.0.0-1.0.6] - 16/10/2019

### Ajouts

- Installation scriptée en prod

### Evolutions

- Renommage menu et titre de page "Prêts pour télétransmission => A ordonner" #438
- Affichage des toaster 10s #392

### Corrections

- Incohérence pour les droits sur les projets en cours de validation pour l'admin #453
- Permissions correctes pour le rôle secrétaire

## [1.0.0-1.0.5] - 15/10/2019

### Ajouts

- Type d'utilisateur : Admin fonctionnel #436

### Evolutions

- Visualisation de la validation en urgence
- Quand un projet est dans un circuit, on ne doit pas pouvoir modifier son état #430
- Modification des droits sur les menus #431
- Circuits utilisés non éditables #451
- Les circuits restent actifs après édition

### Corrections

- Suppression de circuit sans erreurs au retour
- Suppression de l'usage de flash dans certains contrôleurs #258
- CI - Récupération de l'image correcte lors d'un déploiement

## [1.0.0-1.0.4] - 07/10/2019

### Ajouts

- Détail des circuits actifs #419]
- Infobulle sur les switch #429
- Icônes pour les états dans les tableaux #181
- Remise à des éléments relatifs à la classification et au typage en cas de modification de celle-ci #337
- Création circuit : vérification des utilisateurs soumis #376
- Informations pour client pour l'historique de la validation en urgence

### Evolutions

- Couleurs de l'application #275
- Actes signés non transmissibles → changement de titre #434
- Modification de titres divers #438
- Liste des Actes validés télétransmis - changement de titre #433
- Spinner lors de l'enregistrement d'un projet #386
- Modification des Toastes pour les erreurs de format des fichiers #423
- Édition de modèle de circuit bloquée s'il est instancié #371

### Corrections

- Suppression du bouton d'envoi en lot dans un circuit

## [1.0.0-1.0.3] - 30/09/2019

### Ajouts

- Colonnes étape courante #405
- Validation en urgence #403
- Boutons en fonction du statut du projet #403

### Évolutions

- Accepte un connecteur Pastell vide

## [1.0.0-1.0.2] - 30/09/2019

### Évolutions

- Couleur du bouton "Valider en urgence" #409
- Modification de comportement pour les documents principaux trop volumineux ou les annexes trop volumineuses
- Masquer la description du circuit lors de la sélection #402
- Utiliser le compo ls pour textarea #351
- Modification de projet - message d'erreur #359

### Corrections

- Refactor Technique : Enlever tous les "return new Observable()" #378

## [1.0.0-1.0.1] - 25/09/2019

### Ajouts

- Création et utilisation des circuits
- Validation en urgence que pour les administrateurs #399
- Validations unitaires pour les projets #401

### Évolutions

- Filtrage sans tenir compte de la casse
- Modification de message pour les informations manquantes de télétransmission
- Amélioration CI et déploiement

### Corrections

- Pas de validation en lot #400
- Fil d'Ariane des séances correct

## [1.0.0-1.0.0] - 23/09/2019

### Ajouts

- Contient le cas nominal de création de projets d'actes jusqu'à l'envoi au TDT mais sans la gestion des circuits.
