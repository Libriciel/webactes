# Plugin LibricielCakephp3Database

## Behaviors

### AssociatedTablesBehavior

The class AssociatedTablesBehavior provides utility function for getting table associations informations.

### GuardedAssociatedTablesBehavior

The class `GuardedAssociatedTablesBehavior` brings update and delete protection for records having associated records as
well as the "`is_deletable`", "`is_linked`" and "`is_updatable`" virtual fields.

Unguarded columns can be configured to allow updating them when there are associated records.

Unguarded tables can be configured to allow updating or deleting rows when there are associated records only in them.
By default, all tables and all fields are guarded.

__IMPORTANT:__ to get the virtual fields available as fields in a find query, this behavior should be added __BEFORE__ the
`VirtualFieldsBehavior`.

Example table class using the `GuardedAssociatedTables` and `VirtualFieldsBehavior` classes.

```php
class VirtualFieldsBehaviorArticlesTable extends Table
{
     public function initialize(array $config)
     {
         parent::initialize($config);

         //...

         $this->addBehavior('GuardedAssociatedTables');
         $this->addBehavior('VirtualFields');
     }
}
```

### VirtualFieldsBehavior

The class VirtualFieldsBehavior adds virtual field detection and replacement in the "`select`" part of a find query
before it is issued.

Virtual fields come from public methods from a table and it's associated behaviors.
Method names should follow the "`get<camelcased virtual field name>Subquery`" pattern.

Example virtual field method in a behavior class.

```php
class VirtualFieldsBehaviorExampleBehavior extends \Cake\ORM\Behavior
{
     public function getLastCommentIdSubquery()
     {
         $query = $this->_table->Comments->find();
 
         $query
             ->select('id')
             ->order(['id' => 'desc'])
             ->limit(1);
 
         return $query;
     }
 }
```

Example table class with a virtual field method using the `AssociatedTablesBehavior`, (the above) `VirtualFieldsBehaviorExample`
and `VirtualFieldsBehavior` classes.

```php
class VirtualFieldsBehaviorArticlesTable extends Table
{
     public function initialize(array $config)
     {
         parent::initialize($config);

         //...

         $this->addBehavior('AssociatedTables');
         $this->addBehavior('VirtualFieldsBehaviorExample');
         $this->addBehavior('VirtualFields');
     }

     public function getPublishedCommentsCountSubquery()
     {
         $query = $this->Comments
             ->find()
             ->select(['count' => 'count(Comments.id)'])
             ->where(
                 $this->getAssociationFullConditions('Comments')
                 + ['Comments.published' => 'Y']
             );

         return $query;
     }
}
```

By default, all virtual fields are automatically added when no field or the table instance are selected in a query:
they are the "auto" fields.
To add or remove
If you manually specify the fields for a query, virtual fields will be added regardless they should be automatically
added or not.

__IMPORTANT:__ This class should be loaded last, after all behaviors, so the virtual fields methods can all be gathered
at initialization.

## Shells

### SchemasDiffsShell

Compare the database schema with table classes associations or fixtures classes schemas and outputs the differences.

#### Check table classes subcommand

Compare the associations in table classes with the foreign keys defined in the production database and outputs the differences.

```bash
bin/cake SchemasDiffs classes \
    --ignore-constraints services_structure_id_fkey,systemlogs_keycloak_id_fkey \
    --ignore-tables acos,aros,aros_acos,beanstalk_jobs,beanstalk_phinxlog,beanstalk_workers,phinxlog \
    ; echo $?
```

#### Check fixtures classes subcommand

Compare the fields, indexes and constraints from the fixture classes with the production database tables and outputs the differences.

```bash
bin/cake SchemasDiffs fixtures \
    --ignore-tables acos,aros,aros_acos,beanstalk_jobs,beanstalk_phinxlog,beanstalk_workers,phinxlog \
    ; echo $?
```

#### Check table rules subcommand

Compare the validation rules (`validateUnique`, `existsIn` and `isUnique`) from the table classes with the production database tables constraints and outputs the differences.

```bash
bin/cake SchemasDiffs rules \
    --ignore-tables acos,aros,aros_acos,beanstalk_jobs,beanstalk_phinxlog,beanstalk_workers,phinxlog \
    ; echo $?
```

#### Options

| Option                       | Description                                                                                                                               |
|------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| --connection, -c             | The connection name for the production database (default: default)                                                                        |
| --help, -h                   | Display this help.                                                                                                                        |
| --ignore-foreign-key-actions | When comparing table classes associations and database foreign keys, don't compare actions (cascade, noAction, ...)                       |
| --ignore-constraints         | A comma-separated list of constraint names that should be ignored                                                                         |
| --ignore-constraint-names    | Ignore constraint (foreign key, index or unique) names when comparing (classes and fixtures command only, implicit for the rules command) |
| --ignore-tables, -i          | A comma-separated list of tables that should be ignored                                                                                   |
| --quiet, -q                  | Enable quiet output.                                                                                                                      |
| --verbose, -v                | Enable verbose output.                                                                                                                    |

#### To do

##### Check all @info

Especially in the shell

##### Comments (add, check and modify if needed)

##### Importing fixtures schemas from the database

```php
public $import = ['table' => 'pastellfluxactions'];
```

##### Missing tables in the shell

##### Missing relations in the shell

Example in `src/Model/Table/OrganizationsTable.php`
```php
$this->hasMany('Foos', [
    'foreignKey' => 'organization_id'
]);
 ```

##### Summary of caught exceptions

##### Unique constraints in SchemaClassesTask::getSchema
