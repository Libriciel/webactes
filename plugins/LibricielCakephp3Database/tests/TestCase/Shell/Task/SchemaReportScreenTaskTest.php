<?php

namespace LibricielCakephp3Database\Test\TestCase\Shell\Task;

use Cake\TestSuite\TestCase;
use LibricielCakephp3Database\Command\Task\SchemaReportScreenTask;

class TestableSchemaReportScreenTask extends SchemaReportScreenTask
{
    public function icon(bool $success)
    {
        return parent::_icon($success);
    }

    public function generateValues(array $report, string $source, string $target)
    {
        return parent::_generateValues($report, $source, $target);
    }
}

/**
 * SchemaReportScreenTaskTest class
 */
class SchemaReportScreenTaskTest extends TestCase
{
//    use \Cake\TestSuite\ConsoleIntegrationTestTrait;

    public $io = null;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->io = $this->getMockBuilder('Cake\Console\ConsoleIo')
            ->disableOriginalConstructor()
            ->getMock();

        $this->Task = $this->getMockBuilder('\LibricielCakephp3Database\Test\TestCase\Shell\Task\TestableSchemaReportScreenTask')
            ->setMethods(['in', 'out', 'err', '_stop'])
            ->setConstructorArgs([$this->io])
            ->getMock();

        $this->Task->SchemaUtilities = $this->getMockBuilder('\LibricielCakephp3Database\Shell\Task\SchemaUtilitiesTask')
            ->setMethods(['in', 'out', 'err', '_stop'])
            ->setConstructorArgs([$this->io])
            ->getMock();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->Task);
    }

    /**
     * Test the _icon method
     */
    public function testIcon()
    {
        $this->assertEquals('<success>✔</success>', $this->Task->icon(true));
        $this->assertEquals('<error>❌</error>', $this->Task->icon(false));
    }

    /**
     * Test the _generateValues method when there is no difference
     */
    public function testGenerateValuesSuccess()
    {
        $report = [
            '_missing' => [],
            '_extra' => [],
            '_different' => [],
            '_errors' => 0,
        ];
        $source = 'database';
        $target = 'table';

        $this->io->expects($this->never())->method('err');
        $this->io->expects($this->never())->method('out');
        $this->Task->generateValues($report, $source, $target);
    }

    /**
     * Test the _generateValues method with missing values
     */
    public function testGenerateValuesMissing()
    {
        $report = [
            '_missing' => [
                'foo' => 666,
            ],
            '_extra' => [],
            '_different' => [],
            '_errors' => 1,
        ];
        $source = 'database';
        $target = 'table';

        $this->io->expects($this->exactly(1))->method('err');
        $this->io->expects($this->at(0))->method('err')->with("\t\t\t<error>❌</error> Value of <info>foo</info> (<info>666</info>) is missing in the table");
        $this->io->expects($this->never())->method('out');
        $this->Task->generateValues($report, $source, $target);
    }

    /**
     * Test the _generateValues method with extra values
     */
    public function testGenerateValuesExtra()
    {
        $report = [
            '_missing' => [],
            '_extra' => [
                'foo' => 666,
            ],
            '_different' => [],
            '_errors' => 1,
        ];
        $source = 'database';
        $target = 'table';

        $this->io->expects($this->exactly(1))->method('err');
        $this->io->expects($this->at(0))->method('err')->with("\t\t\t<error>❌</error> Value of <info>foo</info> (<info>666</info>) is missing in the database");
        $this->io->expects($this->never())->method('out');
        $this->Task->generateValues($report, $source, $target);
    }

    /**
     * Test the _generateValues method with differences in values
     */
    public function testGenerateValuesDifferent()
    {
        $report = [
            '_missing' => [
                'length' => 255,
                'null' => false,
            ],
            '_extra' => [
                'length' => 25,
                'null' => true,
            ],
            '_different' => [
                0 => 'length',
                1 => 'null',
            ],
            '_errors' => 4,
        ];
        $source = 'database';
        $target = 'fixtures';

        $this->io->expects($this->exactly(2))->method('err');
        $this->io->expects($this->at(0))->method('err')->with("\t\t\t<error>❌</error> Value of <info>length</info> (<info>255</info>) is erroneous in the fixtures (<info>25</info>)");
        $this->io->expects($this->at(1))->method('err')->with("\t\t\t<error>❌</error> Value of <info>null</info> (<info>false</info>) is erroneous in the fixtures (<info>true</info>)");
        $this->io->expects($this->never())->method('out');
        $this->Task->generateValues($report, $source, $target);
    }

    /**
     * Test the generate method with a success report
     */
    public function testGenerateWithSuccess()
    {
        $report = [
            'groups' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'name' => false,
                        'created' => false,
                        'modified' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'name' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'constraints' => [
                    '_missing' => [
                        'groups.primary(id)' => false,
                        'groups.unique(name)' => false,
                    ],
                    'groups.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'groups.unique(name)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_errors' => 0,
                ],
                '_errors' => 0,
            ],
            '_errors' => 0,
        ];

        $this->io->expects($this->never())->method('err');
        $this->io->expects($this->exactly(12))->method('out');

        $this->Task->expects($this->never())->method('_stop');

        $this->io->expects($this->at(0))->method('out')->with("<success>✔</success> Table <info>groups</info>");
        $this->io->expects($this->at(1))->method('out')->with("\t<success>✔</success> Fields");
        $this->io->expects($this->at(2))->method('out')->with("\t\t<success>✔</success> Field <info>id</info>");
        $this->io->expects($this->at(3))->method('out')->with("\t\t<success>✔</success> Field <info>name</info>");
        $this->io->expects($this->at(4))->method('out')->with("\t\t<success>✔</success> Field <info>created</info>");
        $this->io->expects($this->at(5))->method('out')->with("\t\t<success>✔</success> Field <info>modified</info>");
        $this->io->expects($this->at(6))->method('out')->with("\t<success>✔</success> Constraints");
        $this->io->expects($this->at(7))->method('out')->with("\t\t<success>✔</success> Constraint <info>groups.primary(id)</info>");
        $this->io->expects($this->at(8))->method('out')->with("\t\t<success>✔</success> Constraint <info>groups.unique(name)</info>");
        $this->io->expects($this->at(9))->method('out')->with("\t<success>✔</success> Indexes");
        $this->io->expects($this->at(10))->method('out')->with("\n");
        $this->io->expects($this->at(11))->method('out')->with("<success>✔</success> No error was found in fixture classes");

        $this->Task->generate($report, 'database', 'fixtures');
    }

    /**
     * Test the generate method
     */
    public function testGenerateWithErrors()
    {
        $report = [
            'comments' => [
                '_missing' => 'database',
            ],
            'groups' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'name' => 'fixtures',
                        'created' => false,
                        'modified' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'name' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'constraints' => [
                    '_missing' => [
                        'groups.primary(id)' => false,
                        'groups.unique(name)' => false,
                        'users.foreign(group_id) -> groups.id' => false,
                    ],
                    'groups.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'groups.unique(name)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'users.foreign(group_id) -> groups.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_errors' => 0,
                ],
                '_errors' => 0,
            ],
            'tags' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'name' => false,
                        'created' => false,
                        'modified' => 'fixtures',
                        'updated' => 'database',
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => (int)0,
                    ],
                    'name' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => (int)0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => (int)0,
                    ],
                    '_errors' => (int)2,
                ],
                'constraints' => [
                    '_missing' => [
                        'tags.primary(id)' => false,
                        'tags.unique(name)' => 'fixtures',
                        'posts_tags.foreign(tag_id) -> tags.id' => false,
                    ],
                    'tags.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => (int)0,
                    ],
                    'posts_tags.foreign(tag_id) -> tags.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => (int)0,
                    ],
                    '_errors' => (int)1,
                ],
                'indexes' => [
                    '_errors' => (int)0,
                ],
                '_errors' => (int)2,
            ],
            '_errors' => 1,
        ];

        $this->io->expects($this->exactly(9))->method('err');
        $this->io->expects($this->exactly(17))->method('out');

        $this->Task->expects($this->never())->method('_stop');

        $this->io->expects($this->at(0))->method('err')->with("<error>❌</error> Table <info>comments</info> not found in database");
        $this->io->expects($this->at(1))->method('out')->with("<success>✔</success> Table <info>groups</info>");
        $this->io->expects($this->at(2))->method('out')->with("\t<success>✔</success> Fields");
        $this->io->expects($this->at(3))->method('out')->with("\t\t<success>✔</success> Field <info>id</info>");
        $this->io->expects($this->at(4))->method('err')->with("\t\t<error>❌</error> Field <info>name</info> not found in fixtures");
        $this->io->expects($this->at(5))->method('out')->with("\t\t<success>✔</success> Field <info>created</info>");
        $this->io->expects($this->at(6))->method('out')->with("\t\t<success>✔</success> Field <info>modified</info>");
        $this->io->expects($this->at(7))->method('out')->with("\t<success>✔</success> Constraints");
        $this->io->expects($this->at(8))->method('out')->with("\t\t<success>✔</success> Constraint <info>groups.primary(id)</info>");
        $this->io->expects($this->at(9))->method('out')->with("\t\t<success>✔</success> Constraint <info>groups.unique(name)</info>");
        $this->io->expects($this->at(10))->method('out')->with("\t\t<success>✔</success> Constraint <info>users.foreign(group_id) -> groups.id</info>");
        $this->io->expects($this->at(11))->method('out')->with("\t<success>✔</success> Indexes");
        $this->io->expects($this->at(12))->method('err')->with("<error>❌</error> Table <info>tags</info>");
        $this->io->expects($this->at(13))->method('err')->with("\t<error>❌</error> Fields");
        $this->io->expects($this->at(14))->method('out')->with("\t\t<success>✔</success> Field <info>id</info>");
        $this->io->expects($this->at(15))->method('out')->with("\t\t<success>✔</success> Field <info>name</info>");
        $this->io->expects($this->at(16))->method('out')->with("\t\t<success>✔</success> Field <info>created</info>");
        $this->io->expects($this->at(17))->method('err')->with("\t\t<error>❌</error> Field <info>modified</info> not found in fixtures");
        $this->io->expects($this->at(18))->method('err')->with("\t\t<error>❌</error> Field <info>updated</info> not found in database");
        $this->io->expects($this->at(19))->method('err')->with("\t<error>❌</error> Constraints");
        $this->io->expects($this->at(20))->method('out')->with("\t\t<success>✔</success> Constraint <info>tags.primary(id)</info>");
        $this->io->expects($this->at(21))->method('err')->with("\t\t<error>❌</error> Constraint <info>tags.unique(name)</info> not found in fixtures");
        $this->io->expects($this->at(22))->method('out')->with("\t\t<success>✔</success> Constraint <info>posts_tags.foreign(tag_id) -> tags.id</info>");
        $this->io->expects($this->at(23))->method('out')->with("\t<success>✔</success> Indexes");
        $this->io->expects($this->at(24))->method('out')->with("\n");
        $this->io->expects($this->at(25))->method('err')->with("<error>❌</error> 1 fixture class has errors");

        $this->Task->generate($report, 'database', 'fixtures');
    }
}
