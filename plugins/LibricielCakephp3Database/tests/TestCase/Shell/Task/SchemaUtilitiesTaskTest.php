<?php

namespace LibricielCakephp3Database\Test\TestCase\Shell\Task;

use Cake\TestSuite\TestCase;

/**
 * SchemaUtilitiesTaskTest class
 */
class SchemaUtilitiesTaskTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->io = $this->getMockBuilder('Cake\Console\ConsoleIo')
            ->disableOriginalConstructor()
            ->getMock();

        $this->Task = $this->getMockBuilder('LibricielCakephp3Database\Shell\Task\SchemaUtilitiesTask')
            ->setMethods(['in', 'out', 'err', '_stop'])
            ->setConstructorArgs([$this->io])
            ->getMock();

        $this->Task->params = ['connection' => 'default'];
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->Task);
    }

    /**
     * Test the getAssociationKey method
     */
    public function testGetAssociationKey()
    {
        $actual = $this->Task->getAssociationKey(
            ['table' => 'Foos', 'field' => 'bar_id'],
            ['table' => 'Bars', 'field' => 'id']
        );
        $expected = 'Foos.bar_id = Bars.id';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the getConnection method
     */
    public function testGetConnection()
    {
        $actual = $this->Task->getConnection()->configName();
        $expected = 'test';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the getForeignArrayKey method
     */
    public function testGetForeignArrayKey()
    {
        $actual = $this->Task->getForeignArrayKey('Foos', ['bar_id'], ['Bars', 'id']);
        $expected = 'Foos.foreign(bar_id) -> Bars.id';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the xxxx method
     */
    public function testGetIndexArrayKey()
    {
        $actual = $this->Task->getIndexArrayKey('Foos', ['foo', 'bar']);
        $expected = 'Foos.index(bar,foo)';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the getPrimaryArrayKey method
     */
    public function testGetPrimaryArrayKey()
    {
        $actual = $this->Task->getPrimaryArrayKey('Foos', ['id']);
        $expected = 'Foos.primary(id)';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the getUniqueArrayKey method
     */
    public function testGetUniqueArrayKey()
    {
        $actual = $this->Task->getUniqueArrayKey('Foos', ['foo', 'bar']);
        $expected = 'Foos.unique(bar,foo)';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the value method
     */
    public function testValue()
    {
        $this->assertEquals('Foos', $this->Task->value('Foos'));
        $this->assertEquals('null', $this->Task->value(null));
        $this->assertEquals('true', $this->Task->value(true));
        $this->assertEquals('false', $this->Task->value(false));
        $this->assertEquals('666', $this->Task->value(666));
        $this->assertEquals('6.66', $this->Task->value(6.66));
        $this->assertEquals('{"0":"foo","bar":"baz","1":{"bar":"boz"}}', $this->Task->value([0 => 'foo', 'bar' => 'baz', 1 => ['bar' => 'boz']]));
    }
}
