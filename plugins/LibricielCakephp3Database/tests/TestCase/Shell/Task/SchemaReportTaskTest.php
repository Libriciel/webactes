<?php

namespace LibricielCakephp3Database\Test\TestCase\Shell\Task;

use Cake\TestSuite\TestCase;

/**
 * SchemaReportTaskTest class
 */
class SchemaReportTaskTest extends TestCase
{
    protected $_schemas = [
        'correct' => [
            'comments' => [
                'constraints' => [
                    'comments.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'comments.foreign(post_id) -> posts.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'post_id',
                        ],
                        'references' => [
                            'posts',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'comments_post_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'name' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'post_id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                        'autoIncrement' => null,
                    ],
                    'content' => [
                        'type' => 'text',
                        'length' => null,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'created' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'modified' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                ],
                'indexes' => [
                    'comments.index(name)' => [
                        'type' => 'index',
                        'columns' => [
                            'name',
                        ],
                        'length' => [],
                        'name' => 'comments_name_idx',
                    ],
                    'comments.index(post_id)' => [
                        'type' => 'index',
                        'columns' => [
                            'post_id',
                        ],
                        'length' => [],
                        'name' => 'comments_post_id_idx',
                    ],
                ],
            ],
            'groups' => [
                'constraints' => [
                    'groups.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'users.foreign(group_id) -> groups.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'group_id',
                        ],
                        'references' => [
                            'groups',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'users_group_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'name' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'created' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'modified' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                ],
                'indexes' => [],
            ],
            'posts' => [
                'constraints' => [
                    'posts.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'posts.foreign(user_id) -> users.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'user_id',
                        ],
                        'references' => [
                            'users',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_user_id_fkey',
                    ],
                    'comments.foreign(post_id) -> posts.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'post_id',
                        ],
                        'references' => [
                            'posts',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'comments_post_id_fkey',
                    ],
                    'posts_tags.foreign(post_id) -> posts.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'post_id',
                        ],
                        'references' => [
                            'posts',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_tags_post_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'title' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'user_id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                        'autoIncrement' => null,
                    ],
                    'visible' => [
                        'type' => 'boolean',
                        'length' => null,
                        'default' => 1,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'introduction' => [
                        'type' => 'text',
                        'length' => null,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'content' => [
                        'type' => 'text',
                        'length' => null,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'created' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'modified' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                ],
                'indexes' => [
                    'posts.index(user_id)' => [
                        'type' => 'index',
                        'columns' => [
                            'user_id',
                        ],
                        'length' => [],
                        'name' => 'posts_user_id_idx',
                    ],
                    'posts.index(visible)' => [
                        'type' => 'index',
                        'columns' => [
                            'visible',
                        ],
                        'length' => [],
                        'name' => 'posts_visible_idx',
                    ],
                    'posts.index(created)' => [
                        'type' => 'index',
                        'columns' => [
                            'created',
                        ],
                        'length' => [],
                        'name' => 'posts_created_idx',
                    ],
                ],
            ],
            'posts_tags' => [
                'constraints' => [
                    'posts_tags.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'posts_tags.foreign(post_id) -> posts.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'post_id',
                        ],
                        'references' => [
                            'posts',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_tags_post_id_fkey',
                    ],
                    'posts_tags.foreign(tag_id) -> tags.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'tag_id',
                        ],
                        'references' => [
                            'tags',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_tags_tag_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'post_id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                        'autoIncrement' => null,
                    ],
                    'tag_id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                        'autoIncrement' => null,
                    ],
                ],
                'indexes' => [
                    'posts_tags.index(post_id)' => [
                        'type' => 'index',
                        'columns' => [
                            'post_id',
                        ],
                        'length' => [],
                        'name' => 'posts_tags_post_id_idx',
                    ],
                    'posts_tags.index(tag_id)' => [
                        'type' => 'index',
                        'columns' => [
                            'tag_id',
                        ],
                        'length' => [],
                        'name' => 'posts_tags_tag_id_idx',
                    ],
                ],
            ],
            'tags' => [
                'constraints' => [
                    'tags.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'posts_tags.foreign(tag_id) -> tags.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'tag_id',
                        ],
                        'references' => [
                            'tags',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_tags_tag_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'name' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'created' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'modified' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                ],
                'indexes' => [],
            ],
            'users' => [
                'constraints' => [
                    'users.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'users.foreign(group_id) -> groups.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'group_id',
                        ],
                        'references' => [
                            'groups',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'users_group_id_fkey',
                    ],
                    'posts.foreign(user_id) -> users.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'user_id',
                        ],
                        'references' => [
                            'users',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_user_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'username' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'password' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'group_id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                        'autoIncrement' => null,
                    ],
                    'created' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'modified' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                ],
                'indexes' => [
                    'users.index(group_id)' => [
                        'type' => 'index',
                        'columns' => [
                            'group_id',
                        ],
                        'length' => [],
                        'name' => 'users_group_id_idx',
                    ],
                ],
            ],
        ],
        'errors' => [
            'bars' => [
                'constraints' => [
                    'bars.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'name' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                ],
                'indexes' => [],
            ],
            'groups' => [
                'constraints' => [
                    'groups.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'users.foreign(group_id) -> groups.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'group_id',
                        ],
                        'references' => [
                            'groups',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'users_group_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'name' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'created' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'modified' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                ],
                'indexes' => [],
            ],
            'posts' => [
                'constraints' => [
                    'posts.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'comments.foreign(post_id) -> posts.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'post_id',
                        ],
                        'references' => [
                            'posts',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'comments_post_id_fkey',
                    ],
                    'posts_tags.foreign(post_id) -> posts.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'post_id',
                        ],
                        'references' => [
                            'posts',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_tags_post_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'title' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'user_id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                        'autoIncrement' => null,
                    ],
                    'introduction' => [
                        'type' => 'text',
                        'length' => null,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'content' => [
                        'type' => 'text',
                        'length' => null,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'created' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'modified' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                ],
                'indexes' => [
                    'posts.index(user_id)' => [
                        'type' => 'index',
                        'columns' => [
                            'user_id',
                        ],
                        'length' => [],
                        'name' => 'posts_user_id_idx',
                    ],
                    'posts.index(created)' => [
                        'type' => 'index',
                        'columns' => [
                            'created',
                        ],
                        'length' => [],
                        'name' => 'posts_created_idx',
                    ],
                ],
            ],
            'posts_tags' => [
                'constraints' => [
                    'posts_tags.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'posts_tags.foreign(post_id) -> posts.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'post_id',
                        ],
                        'references' => [
                            'posts',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_tags_post_id_fkey',
                    ],
                    'posts_tags.foreign(tag_id) -> tags.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'tag_id',
                        ],
                        'references' => [
                            'tags',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_tags_tag_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'post_id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                        'autoIncrement' => null,
                    ],
                    'tag_id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                        'autoIncrement' => null,
                    ],
                ],
                'indexes' => [
                    'posts_tags.index(post_id)' => [
                        'type' => 'index',
                        'columns' => [
                            'post_id',
                        ],
                        'length' => [],
                        'name' => 'posts_tags_post_id_idx',
                    ],
                    'posts_tags.index(tag_id)' => [
                        'type' => 'index',
                        'columns' => [
                            'tag_id',
                        ],
                        'length' => [],
                        'name' => 'posts_tags_tag_id_idx',
                    ],
                ],
            ],
            'tags' => [
                'constraints' => [
                    'tags.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'tags.foreign(parent_id) -> tags.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'parent_id',
                        ],
                        'references' => [
                            'tags',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'tags_parent_id_fkey',
                    ],
                    'posts_tags.foreign(tag_id) -> tags.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'tag_id',
                        ],
                        'references' => [
                            'tags',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_tags_tag_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'parent_id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                        'autoIncrement' => null,
                    ],
                    'name' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'created' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'modified' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                ],
                'indexes' => [
                    'tags.index(parent_id)' => [
                        'type' => 'index',
                        'columns' => [
                            'parent_id',
                        ],
                        'length' => [],
                        'name' => 'tags_parent_id_idx',
                    ],
                ],
            ],
            'users' => [
                'constraints' => [
                    'users.primary(id)' => [
                        'type' => 'primary',
                        'columns' => [
                            'id',
                        ],
                        'length' => [],
                        'name' => 'primary',
                    ],
                    'users.foreign(group_id) -> groups.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'group_id',
                        ],
                        'references' => [
                            'groups',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'users_group_id_fkey',
                    ],
                    'posts.foreign(user_id) -> users.id' => [
                        'type' => 'foreign',
                        'columns' => [
                            'user_id',
                        ],
                        'references' => [
                            'users',
                            'id',
                        ],
                        'update' => 'cascade',
                        'delete' => 'cascade',
                        'length' => [],
                        'name' => 'posts_user_id_fkey',
                    ],
                ],
                'fields' => [
                    'id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'autoIncrement' => true,
                        'default' => null,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                    ],
                    'username' => [
                        'type' => 'string',
                        'length' => 64,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'password' => [
                        'type' => 'string',
                        'length' => 255,
                        'default' => null,
                        'null' => false,
                        'collate' => null,
                        'comment' => null,
                        'precision' => null,
                        'fixed' => null,
                    ],
                    'group_id' => [
                        'type' => 'integer',
                        'length' => 10,
                        'default' => 0,
                        'null' => false,
                        'comment' => null,
                        'precision' => null,
                        'unsigned' => null,
                        'autoIncrement' => null,
                    ],
                    'created' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                    'modified' => [
                        'type' => 'timestamp',
                        'length' => null,
                        'default' => null,
                        'null' => true,
                        'comment' => null,
                        'precision' => null,
                    ],
                ],
                'indexes' => [
                    'users.index(group_id)' => [
                        'type' => 'index',
                        'columns' => [
                            'group_id',
                        ],
                        'length' => [],
                        'name' => 'users_group_id_idx',
                    ],
                ],
            ],
        ],
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->io = $this->getMockBuilder('Cake\Console\ConsoleIo')
            ->disableOriginalConstructor()
            ->getMock();

        $this->Task = $this->getMockBuilder('LibricielCakephp3Database\Shell\Task\SchemaReportTask')
            ->setMethods(['in', 'out', 'err', '_stop'])
            ->setConstructorArgs([$this->io])
            ->getMock();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->Task);
    }

    /**
     * Test the getReport method in cas of no differences
     */
    public function testGetReportSuccess()
    {
        $actual = $this->Task->getReport(
            $this->_schemas['correct'],
            'database',
            $this->_schemas['correct'],
            'classes'
        );
        $expected = [
            'comments' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'name' => false,
                        'post_id' => false,
                        'content' => false,
                        'created' => false,
                        'modified' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'name' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'post_id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'content' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'constraints' => [
                    '_missing' => [
                        'comments.primary(id)' => false,
                        'comments.foreign(post_id) -> posts.id' => false,
                    ],
                    'comments.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'comments.foreign(post_id) -> posts.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_missing' => [
                        'comments.index(name)' => false,
                        'comments.index(post_id)' => false,
                    ],
                    'comments.index(name)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'comments.index(post_id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                '_errors' => 0,
            ],
            'groups' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'name' => false,
                        'created' => false,
                        'modified' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'name' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'constraints' => [
                    '_missing' => [
                        'groups.primary(id)' => false,
                        'users.foreign(group_id) -> groups.id' => false,
                    ],
                    'groups.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'users.foreign(group_id) -> groups.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_errors' => 0,
                ],
                '_errors' => 0,
            ],
            'posts' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'title' => false,
                        'user_id' => false,
                        'visible' => false,
                        'introduction' => false,
                        'content' => false,
                        'created' => false,
                        'modified' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'title' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'user_id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'visible' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'introduction' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'content' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'constraints' => [
                    '_missing' => [
                        'posts.primary(id)' => false,
                        'posts.foreign(user_id) -> users.id' => false,
                        'comments.foreign(post_id) -> posts.id' => false,
                        'posts_tags.foreign(post_id) -> posts.id' => false,
                    ],
                    'posts.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts.foreign(user_id) -> users.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'comments.foreign(post_id) -> posts.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts_tags.foreign(post_id) -> posts.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_missing' => [
                        'posts.index(user_id)' => false,
                        'posts.index(visible)' => false,
                        'posts.index(created)' => false,
                    ],
                    'posts.index(user_id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts.index(visible)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts.index(created)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                '_errors' => 0,
            ],
            'posts_tags' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'post_id' => false,
                        'tag_id' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'post_id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'tag_id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'constraints' => [
                    '_missing' => [
                        'posts_tags.primary(id)' => false,
                        'posts_tags.foreign(post_id) -> posts.id' => false,
                        'posts_tags.foreign(tag_id) -> tags.id' => false,
                    ],
                    'posts_tags.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts_tags.foreign(post_id) -> posts.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts_tags.foreign(tag_id) -> tags.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_missing' => [
                        'posts_tags.index(post_id)' => false,
                        'posts_tags.index(tag_id)' => false,
                    ],
                    'posts_tags.index(post_id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts_tags.index(tag_id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                '_errors' => 0,
            ],
            'tags' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'name' => false,
                        'created' => false,
                        'modified' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'name' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'constraints' => [
                    '_missing' => [
                        'tags.primary(id)' => false,
                        'posts_tags.foreign(tag_id) -> tags.id' => false,
                    ],
                    'tags.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts_tags.foreign(tag_id) -> tags.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_errors' => 0,
                ],
                '_errors' => 0,
            ],
            'users' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'username' => false,
                        'password' => false,
                        'group_id' => false,
                        'created' => false,
                        'modified' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'username' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'password' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'group_id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'constraints' => [
                    '_missing' => [
                        'users.primary(id)' => false,
                        'users.foreign(group_id) -> groups.id' => false,
                        'posts.foreign(user_id) -> users.id' => false,
                    ],
                    'users.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'users.foreign(group_id) -> groups.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts.foreign(user_id) -> users.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_missing' => [
                        'users.index(group_id)' => false,
                    ],
                    'users.index(group_id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                '_errors' => 0,
            ],
            '_errors' => 0,
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the getReport method in case of differences
     */
    public function testGetReportErrors()
    {
        $actual = $this->Task->getReport(
            $this->_schemas['correct'],
            'database',
            $this->_schemas['errors'],
            'fixtures'
        );

        $expected = [
            'bars' => [
                '_missing' => 'database',
                '_errors' => 0,
            ],
            'comments' => [
                '_missing' => 'fixtures',
                '_errors' => 0,
            ],
            'groups' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'name' => false,
                        'created' => false,
                        'modified' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'name' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'constraints' => [
                    '_missing' => [
                        'groups.primary(id)' => false,
                        'users.foreign(group_id) -> groups.id' => false,
                    ],
                    'groups.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'users.foreign(group_id) -> groups.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_errors' => 0,
                ],
                '_errors' => 0,
            ],
            'posts' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'title' => false,
                        'user_id' => false,
                        'visible' => 'fixtures',
                        'introduction' => false,
                        'content' => false,
                        'created' => false,
                        'modified' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'title' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'user_id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'introduction' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'content' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 1,
                ],
                'constraints' => [
                    '_missing' => [
                        'posts.primary(id)' => false,
                        'posts.foreign(user_id) -> users.id' => 'fixtures',
                        'comments.foreign(post_id) -> posts.id' => false,
                        'posts_tags.foreign(post_id) -> posts.id' => false,
                    ],
                    'posts.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'comments.foreign(post_id) -> posts.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts_tags.foreign(post_id) -> posts.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 1,
                ],
                'indexes' => [
                    '_missing' => [
                        'posts.index(user_id)' => false,
                        'posts.index(visible)' => 'fixtures',
                        'posts.index(created)' => false,
                    ],
                    'posts.index(user_id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts.index(created)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 1,
                ],
                '_errors' => 3,
            ],
            'posts_tags' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'post_id' => false,
                        'tag_id' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'post_id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'tag_id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'constraints' => [
                    '_missing' => [
                        'posts_tags.primary(id)' => false,
                        'posts_tags.foreign(post_id) -> posts.id' => false,
                        'posts_tags.foreign(tag_id) -> tags.id' => false,
                    ],
                    'posts_tags.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts_tags.foreign(post_id) -> posts.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts_tags.foreign(tag_id) -> tags.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_missing' => [
                        'posts_tags.index(post_id)' => false,
                        'posts_tags.index(tag_id)' => false,
                    ],
                    'posts_tags.index(post_id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts_tags.index(tag_id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                '_errors' => 0,
            ],
            'tags' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'name' => false,
                        'created' => false,
                        'modified' => false,
                        'parent_id' => 'database',
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'name' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 1,
                ],
                'constraints' => [
                    '_missing' => [
                        'tags.primary(id)' => false,
                        'posts_tags.foreign(tag_id) -> tags.id' => false,
                        'tags.foreign(parent_id) -> tags.id' => 'database',
                    ],
                    'tags.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts_tags.foreign(tag_id) -> tags.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 1,
                ],
                'indexes' => [
                    '_missing' => [
                        'tags.index(parent_id)' => 'database',
                    ],
                    '_errors' => 1,
                ],
                '_errors' => 3,
            ],
            'users' => [
                '_missing' => false,
                'fields' => [
                    '_missing' => [
                        'id' => false,
                        'username' => false,
                        'password' => false,
                        'group_id' => false,
                        'created' => false,
                        'modified' => false,
                    ],
                    'id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'username' => [
                        '_missing' => [
                            'length' => 255,
                        ],
                        '_extra' => [
                            'length' => 64,
                        ],
                        '_different' => [
                            0 => 'length',
                        ],
                        '_errors' => 2,
                    ],
                    'password' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'group_id' => [
                        '_missing' => [
                            'default' => null,
                        ],
                        '_extra' => [
                            'default' => 0,
                        ],
                        '_different' => [
                            0 => 'default',
                        ],
                        '_errors' => 2,
                    ],
                    'created' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'modified' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 2,
                ],
                'constraints' => [
                    '_missing' => [
                        'users.primary(id)' => false,
                        'users.foreign(group_id) -> groups.id' => false,
                        'posts.foreign(user_id) -> users.id' => false,
                    ],
                    'users.primary(id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'users.foreign(group_id) -> groups.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    'posts.foreign(user_id) -> users.id' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                'indexes' => [
                    '_missing' => [
                        'users.index(group_id)' => false,
                    ],
                    'users.index(group_id)' => [
                        '_missing' => [],
                        '_extra' => [],
                        '_different' => [],
                        '_errors' => 0,
                    ],
                    '_errors' => 0,
                ],
                '_errors' => 1,
            ],
            '_errors' => 5,
        ];
        $this->assertEquals($expected, $actual);
    }
}
