<?php

namespace LibricielCakephp3Database\Test\TestCase\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use function LibricielCakephp3Database\sql;

class VirtualFieldsBehaviorExampleVirtualFieldsBehavior extends Behavior
{
    public function getLastCommentIdSubquery()
    {
        $query = $this->_table->Comments->find();

        $query
            ->select('id')
            ->order(['id' => SORT_DESC])
            ->limit(1);

        return $query;
    }
}

class VirtualFieldsBehaviorArticlesTable extends Table
{
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->addBehavior('LibricielCakephp3Database.AssociatedTables');

        $this->addBehavior(
            'ExampleVirtualFields',
            [
                'className' => '\LibricielCakephp3Database\Test\TestCase\Model\Behavior\VirtualFieldsBehaviorExampleVirtualFieldsBehavior',
            ]
        );

        $this->addBehavior('LibricielCakephp3Database.VirtualFields');

        // Associations

        $this->belongsTo('Authors');

        $this->belongsToMany('Tags')->setThrough('ArticlesTags');

        $this->hasMany('Comments');

        $this->hasOne('SpecialTags');
    }

    public function getPublishedCommentsCountSubquery()
    {
        $query = $this->Comments
            ->find()
            ->select(['count' => 'count(Comments.id)'])
            ->where(
                $this->getAssociationFullConditions('Comments')
                + ['Comments.published' => 'Y']
            );

        return $query;
    }
}

/**
 * Class VirtualFieldsBehaviorTest
 * This is the unit test class for the VirtualFieldsBehavior class.
 *
 * @package LibricielCakephp3Database\Test\TestCase\Model\Behavior
 */
class VirtualFieldsBehaviorTest extends TestCase
{
    /**
     * The Table object for Articles.
     *
     * @var Table
     */
    public $Articles = null;

    /**
     * fixtures
     *
     * @var array
     */
    public $fixtures = [
        'core.Articles',
        'core.Tags',
        'core.ArticlesTags',
        'core.Authors',
        'core.Comments',
        'core.SpecialTags',
    ];

    /**
     * Normalize spaces in a SQL query.
     *
     * @param string $sql
     * @return string
     */
    protected function normalizeSql(string $sql)
    {
        $replacements = ['( ' => '(', ' )' => ')'];

        return str_replace(
            array_keys($replacements),
            array_values($replacements),
            preg_replace('/\s+/', ' ', $sql)
        );
    }

    /**
     * Set up the Articles table and its associations.
     */
    public function setUp(): void
    {
        parent::setUp();

        $config = TableRegistry::getTableLocator()->exists('Articles')
            ? []
            : ['className' => VirtualFieldsBehaviorArticlesTable::class, 'table' => 'articles'];
        $this->Articles = TableRegistry::getTableLocator()->get('Articles', $config);

        $this->Articles->Authors->hasMany('Articles');
        $this->Articles->Comments->belongsTo('Articles');
    }

    public function testGetAuto()
    {
        $actual = $this->Articles->behaviors()->get('VirtualFields')->getAuto();
        var_dump($actual);
        $expected = [
            'published_comments_count',
            'last_comment_id',
        ];
        $this->assertEquals($expected, $actual);
    }
    /*
        public function testSetAutoWithNoField()
        {
            $this->Articles->behaviors()->get('VirtualFields')->setAuto([]);
            $actual = $this->Articles->behaviors()->get('VirtualFields')->getAuto();
            $expected = [];
            $this->assertEquals($expected, $actual);
        }

        public function testSetAutoWithFields()
        {
            $this->Articles->behaviors()->get('VirtualFields')->setAuto(['published_comments_count', 'id']);
            $actual = $this->Articles->behaviors()->get('VirtualFields')->getAuto();
            $expected = ['published_comments_count'];
            $this->assertEquals($expected, $actual);
        }

        public function testDisableAutoTrue()
        {
            $this->Articles->behaviors()->get('VirtualFields')->disableAuto(true);
            $actual = $this->Articles->behaviors()->get('VirtualFields')->getAuto();
            $expected = [];
            $this->assertEquals($expected, $actual);
        }

        public function testDisableAutoFalse()
        {
            $this->Articles->behaviors()->get('VirtualFields')->disableAuto(false);
            $actual = $this->Articles->behaviors()->get('VirtualFields')->getAuto();
            $expected = [
                'published_comments_count',
                'last_comment_id',
            ];
            $this->assertEquals($expected, $actual);
        }

        public function testDisableAutoWithFields()
        {
            $this->Articles->behaviors()->get('VirtualFields')->disableAuto(['published_comments_count']);
            $actual = $this->Articles->behaviors()->get('VirtualFields')->getAuto();
            $expected = ['last_comment_id'];
            $this->assertEquals($expected, $actual);
        }

        public function testEnableAutoTrue()
        {
            $this->Articles->behaviors()->get('VirtualFields')->enableAuto(true);
            $actual = $this->Articles->behaviors()->get('VirtualFields')->getAuto();
            $expected = [
                'published_comments_count',
                'last_comment_id',
            ];
            $this->assertEquals($expected, $actual);
        }

        public function testEnableAutoFalse()
        {
            $this->Articles->behaviors()->get('VirtualFields')->enableAuto(false);
            $actual = $this->Articles->behaviors()->get('VirtualFields')->getAuto();
            $expected = [];
            $this->assertEquals($expected, $actual);
        }

        public function testEnableAutoWithFields()
        {
            $this->Articles->behaviors()->get('VirtualFields')->disableAuto(false);
            $this->Articles->behaviors()->get('VirtualFields')->enableAuto(['published_comments_count']);
            $actual = $this->Articles->behaviors()->get('VirtualFields')->getAuto();
            $expected = ['published_comments_count'];
            $this->assertEquals($expected, $actual);
        }

        public function testQueryFindAllWithNoSpecificFieldsSql()
        {
            $query = $this->Articles
                ->find();
            $expected = 'SELECT
                            (SELECT count(Comments.id) AS "count" FROM comments Comments WHERE (Articles.id = Comments.article_id AND Comments.published = \'Y\')) AS "Articles__published_comments_count",
                            (SELECT Comments.id AS "Comments__id" FROM comments Comments ORDER BY id desc LIMIT 1) AS "Articles__last_comment_id",
                            Articles.id AS "Articles__id",
                            Articles.author_id AS "Articles__author_id",
                            Articles.title AS "Articles__title",
                            Articles.body AS "Articles__body",
                            Articles.published AS "Articles__published"
                        FROM articles Articles';

            $this->assertEquals($this->normalizeSql($expected), $this->normalizeSql(sql($query)));
        }

        public function testQueryFindAllWithSpecificFieldsSql()
        {
            $query = $this->Articles
                ->find()
                ->select(['id', 'last_comment_id']);
            $expected = 'SELECT
                            Articles.id AS "Articles__id",
                            (SELECT Comments.id AS "Comments__id" FROM comments Comments ORDER BY id desc LIMIT 1) AS "Articles__last_comment_id"
                        FROM articles Articles';

            $this->assertEquals($this->normalizeSql($expected), $this->normalizeSql(sql($query)));
        }

        public function testQueryFindAllWithNoSpecificFields()
        {
            $query = $this->Articles
                ->find()
                ->enableHydration(false)
                ->first();
            $expected = [
                'published_comments_count' => 3,
                'last_comment_id' => 6,
                'id' => 1,
                'author_id' => 1,
                'title' => 'First Article',
                'body' => 'First Article Body',
                'published' => 'Y',

            ];
            $this->assertEquals($expected, $query);
        }

        public function testQueryFindAllWithSpecificFieldsFromTable()
        {
            $query = $this->Articles
                ->find()
                ->select($this->Articles)
                ->enableHydration(false)
                ->first();
            $expected = [
                'published_comments_count' => 3,
                'last_comment_id' => 6,
                'id' => 1,
                'author_id' => 1,
                'title' => 'First Article',
                'body' => 'First Article Body',
                'published' => 'Y',

            ];
            $this->assertEquals($expected, $query);
        }

        public function testQueryFindAllWithSpecificFields()
        {
            $query = $this->Articles
                ->find()
                ->select(['id', 'last_comment_id'])
                ->enableHydration(false)
                ->first();
            $expected = [
                'last_comment_id' => 6,
                'id' => 1,
            ];
            $this->assertEquals($expected, $query);
        }

        public function testQueryFindAllWithNoSpecificFieldsAndContain()
        {
            $query = $this->Articles
                ->find()
                ->contain(['Authors'])// @info: left join
                ->enableHydration(false)
                ->first();
            $expected = [
                'published_comments_count' => 3,
                'last_comment_id' => 6,
                'id' => 1,
                'author_id' => 1,
                'title' => 'First Article',
                'body' => 'First Article Body',
                'published' => 'Y',
                'author' => [
                    'id' => 1,
                    'name' => 'mariano',
                ],
            ];
            $this->assertEquals($expected, $query);
        }

        public function testQueryFindAllAsContainedWithNoSpecificFields()
        {
            $query = $this->Articles->Authors
                ->find()
                ->contain(['Articles' => function (Query $query) {
                    return $query->select()
                        ->where(['Articles.id' => 1]);
                }])
                ->enableHydration(false)
                ->first();
            $expected = [
                'id' => 1,
                'name' => 'mariano',
                'articles' => [
                    [
                        'published_comments_count' => 3,
                        'last_comment_id' => 6,
                        'id' => 1,
                        'author_id' => 1,
                        'title' => 'First Article',
                        'body' => 'First Article Body',
                        'published' => 'Y',

                    ],
                ],
            ];
            $this->assertEquals($expected, $query);
        }

        public function testQueryFindAllAsContainedWithSpecificFieldsFromTable()
        {
            $query = $this->Articles->Authors
                ->find()
                ->contain(['Articles' => function (Query $query) {
                    return $query->select($this->Articles)
                        ->where(['Articles.id' => 1]);
                }])
                ->enableHydration(false)
                ->first();

            $expected = [
                'id' => 1,
                'name' => 'mariano',
                'articles' => [
                    [
                        'published_comments_count' => 3,
                        'last_comment_id' => 6,
                        'id' => 1,
                        'author_id' => 1,
                        'title' => 'First Article',
                        'body' => 'First Article Body',
                        'published' => 'Y',

                    ],
                ],
            ];
            $this->assertEquals($expected, $query);
        }

        public function testQueryFindAllAsContainedWithSpecificFields()
        {
            $query = $this->Articles->Authors
                ->find()
                ->contain(['Articles' => function (Query $query) {
                    return $query->select(['id', 'author_id', 'last_comment_id'])
                        ->where(['Articles.id' => 1]);
                }])
                ->enableHydration(false)
                ->first();

            $expected = [
                'id' => 1,
                'name' => 'mariano',
                'articles' => [
                    [
                        'last_comment_id' => 6,
                        'id' => 1,
                        'author_id' => 1,
                    ],
                ],
            ];
            $this->assertEquals($expected, $query);
        }

        public function testQueryFindAllInnerJoinWithNoField()
        {
            $query = $this->Articles
                ->find()
                ->innerJoinWith('Comments')
                ->enableHydration(false)
                ->first();

            $expected = [
                'published_comments_count' => 3,
                'last_comment_id' => 6,
                'id' => 1,
                'author_id' => 1,
                'title' => 'First Article',
                'body' => 'First Article Body',
                'published' => 'Y',
            ];
            $this->assertEquals($expected, $query);
        }

        public function testQueryFindAllInnerJoinWithSpecificFields()
        {
            $query = $this->Articles
                ->find()
                ->select(['id', 'Articles.author_id', 'last_comment_id', 'published'])
                ->innerJoinWith('Comments')
                ->enableHydration(false)
                ->first();

            $expected = [
                'last_comment_id' => 6,
                'id' => 1,
                'author_id' => 1,
                'published' => 'Y',
            ];
            $this->assertEquals($expected, $query);
        }

        public function testQueryFindAllInnerJoinWithSpecificFieldsFromTable()
        {
            $query = $this->Articles
                ->find()
                ->select($this->Articles)
                ->innerJoinWith('Comments')
                ->enableHydration(false)
                ->first();

            $expected = [
                'published_comments_count' => 3,
                'last_comment_id' => 6,
                'id' => 1,
                'author_id' => 1,
                'title' => 'First Article',
                'body' => 'First Article Body',
                'published' => 'Y',
            ];
            $this->assertEquals($expected, $query);
        }

        public function testQueryFindAllInnerJoinWithSpecificFieldsFromLinkedTable()
        {
            $query = $this->Articles
                ->find()
                ->select($this->Articles->Comments)
                ->innerJoinWith('Comments')
                ->enableHydration(false);

            $actual = json_decode(json_encode($query->first()), true);

            $expected = [
                '_matchingData' => [
                    'Comments' => [
                        'id' => 1,
                        'article_id' => 1,
                        'user_id' => 2,
                        'comment' => 'First Comment for First Article',
                        'published' => 'Y',
                        'created' => '2007-03-18T10:45:23+00:00',
                        'updated' => '2007-03-18T10:47:31+00:00',
                    ],
                ],
            ];
            $this->assertEquals($expected, $actual);
        }

        public function testQueryFindAllAsInnerJoinWithSpecificFieldsFromTable()
        {
            $query = $this->Articles->Comments
                ->find()
                ->select($this->Articles)
                ->innerJoinWith('Articles')
                ->enableHydration(false)
                ->first();

            $expected = [
                '_matchingData' => [
                    'Articles' => [
                        'id' => 1,
                        'author_id' => 1,
                        'title' => 'First Article',
                        'body' => 'First Article Body',
                        'published' => 'Y',
                        'published_comments_count' => 3,
                        'last_comment_id' => 6,
                    ],
                ],
            ];
            $this->assertEquals($expected, $query);
        }
    */
}
