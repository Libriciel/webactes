<?php

namespace LibricielCakephp3Database\Test\TestCase\Model\Behavior;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use UnexpectedValueException;
use function LibricielCakephp3Database\sql as sql;

class GuardedAssociatedTablesBehaviorArticlesTable extends Table
{
    public $virtual = [];

    public function initialize(array $config): void
    {
        parent::initialize($config);

        // belongsTo without alias and conditions
        $this->belongsTo('Authors');

        // belongsTo with alias and conditions
        $this->belongsTo(
            'EvenAuthors',
            [
                'className' => 'Authors',
                'foreignKey' => 'author_id',
                'conditions' => ['EvenAuthors.id % 2' => 0],
            ]
        );

        // belongsToMany without alias and conditions
        $this->belongsToMany('Tags')
            ->setThrough('ArticlesTags');

        // belongsToMany without alias and conditions
        $this->belongsToMany('Tags')
            ->setThrough('ArticlesTags');

        // belongsToMany with alias and conditions
        $this->belongsToMany(
            'AliasedTags',
            [
                'className' => 'Tags',
                'foreignKey' => 'article_id',
                'targetForeignKey' => 'tag_id',
                'joinTable' => 'articles_tags',
                // @info: conditions do not seem to work on join tables
            ]
        )->setThrough(
            TableRegistry::getTableLocator()->get(
                'AliasedArticlesTags',
                [
                    'className' => 'ArticlesTags',
                    'table' => 'articles_tags',
                ]
            )
        );

        // hasToMany without alias and conditions
        $this->hasMany('Comments');

        // hasToMany with alias and conditions
        $this->hasMany(
            'EvenComments',
            [
                'className' => 'Comments',
                'foreignKey' => 'article_id',
                'conditions' => ['EvenComments.id % 2 =' => 0],
            ]
        );

        // hasOne without alias and conditions
        $this->hasOne('SpecialTags');

        // hasOne with alias and conditions
        $this->hasOne(
            'EvenSpecialTags',
            [
                'className' => 'SpecialTags',
                'foreignKey' => 'article_id',
                'conditions' => ['EvenSpecialTags.id % 2 =' => 0],
            ]
        );
    }
}

/**
 * Class GuardedAssociatedTablesBehaviorTest
 * This is the unit test class for the GuardedAssociatedTablesBehavior class.
 *
 * @package LibricielCakephp3Database\Test\TestCase\Model\Behavior
 */
class GuardedAssociatedTablesBehaviorTest extends TestCase
{
    /**
     * The Table object for Articles.
     *
     * @var Table
     */
    public $table = null;

    /**
     * fixtures
     *
     * @var array
     */
    public $fixtures = [
        'core.Articles',
        'core.Tags',
        'core.ArticlesTags',
        'core.Authors',
        'core.Comments',
        'core.SpecialTags',
    ];

    /**
     * Normalize spaces in a SQL query.
     *
     * @param string $sql
     * @return string
     */
    protected function normalizeSql(string $sql)
    {
        $replacements = ['( ' => '(', ' )' => ')'];

        return str_replace(
            array_keys($replacements),
            array_values($replacements),
            preg_replace('/\s+/', ' ', $sql)
        );
    }

    /**
     * Set up the Articles table and its associations.
     */
    public function setUp(): void
    {
        parent::setUp();

//        $this->table = $this->getTableLocator()->get('Articles');
        $config = TableRegistry::getTableLocator()->exists('Articles')
            ? []
            : ['className' => GuardedAssociatedTablesBehaviorArticlesTable::class, 'table' => 'articles'];

        $this->table = TableRegistry::getTableLocator()->get('Articles', $config);
    }

    public function addBehaviors(array $config = [])
    {
        $this->table->addBehavior('LibricielCakephp3Database.AssociatedTables');
        $this->table->addBehavior('LibricielCakephp3Database.GuardedAssociatedTables', $config);
        $this->table->addBehavior('LibricielCakephp3Database.VirtualFields');
    }

    /**
     * Test the "enable", "disable" and "disabled" method.
     */
    public function testEnable()
    {
        $this->addBehaviors();
        $this->assertTrue($this->table->behaviors()->get('GuardedAssociatedTables')->enabled());

        $this->table->behaviors()->get('GuardedAssociatedTables')->disable();
        $this->assertFalse($this->table->behaviors()->get('GuardedAssociatedTables')->enabled());

        $this->table->behaviors()->get('GuardedAssociatedTables')->enable();
        $this->assertTrue($this->table->behaviors()->get('GuardedAssociatedTables')->enabled());
    }

    public function testIsUpdatableQuerySelectVirtualFieldSql()
    {
        $this->addBehaviors();
        $query = $this->table
            ->find('all')
            ->select(['id', 'title', 'is_updatable'])
            ->where(['id' => 1]);

        $expected = 'SELECT
                Articles.id AS "Articles__id",
                Articles.title AS "Articles__title",
                (
                    NOT (
                        (
                            EXISTS (
                                SELECT
                                    ArticlesTags.article_id AS "ArticlesTags__article_id",
                                    ArticlesTags.tag_id AS "ArticlesTags__tag_id"
                                FROM articles_tags ArticlesTags
                                WHERE
                                    Articles.id = ArticlesTags.article_id
                            )
                            OR EXISTS (
                                SELECT
                                    AliasedArticlesTags.article_id AS "AliasedArticlesTags__article_id",
                                    AliasedArticlesTags.tag_id AS "AliasedArticlesTags__tag_id"
                                FROM articles_tags AliasedArticlesTags
                                WHERE Articles.id = AliasedArticlesTags.article_id
                            )
                            OR EXISTS (
                                SELECT
                                    Comments.id AS "Comments__id"
                                FROM comments Comments
                                WHERE
                                    Articles.id = Comments.article_id
                            )
                            OR EXISTS (
                                SELECT
                                    EvenComments.id AS "EvenComments__id"
                                FROM comments EvenComments
                                WHERE
                                    (EvenComments.id % 2 = 0 AND Articles.id = EvenComments.article_id AND EvenComments.id % 2 = 0)
                            )
                            OR EXISTS (
                                SELECT
                                    SpecialTags.id AS "SpecialTags__id"
                                FROM special_tags SpecialTags
                                WHERE Articles.id = SpecialTags.article_id
                            )
                            OR EXISTS (
                                SELECT
                                    EvenSpecialTags.id AS "EvenSpecialTags__id"
                                FROM special_tags EvenSpecialTags
                                WHERE
                                    (EvenSpecialTags.id % 2 = 0 AND Articles.id = EvenSpecialTags.article_id AND EvenSpecialTags.id % 2 = 0)
                            )
                        )
                    )
                ) AS "Articles__is_updatable"
                FROM articles Articles
                WHERE
                    id = 1';

        $this->assertEquals($this->normalizeSql($expected), $this->normalizeSql(sql($query)));
    }

    public function testIsDeletableQuerySelectVirtualFieldSql()
    {
        $this->addBehaviors();
        $query = $this->table
            ->find('all')
            ->select(['id', 'title', 'is_deletable'])
            ->where(['id' => 1]);

        $expected = 'SELECT
                Articles.id AS "Articles__id",
                Articles.title AS "Articles__title",
                (
                    NOT (
                        (
                            EXISTS (
                                SELECT
                                    ArticlesTags.article_id AS "ArticlesTags__article_id",
                                    ArticlesTags.tag_id AS "ArticlesTags__tag_id"
                                FROM articles_tags ArticlesTags
                                WHERE
                                    Articles.id = ArticlesTags.article_id
                            )
                            OR EXISTS (
                                SELECT
                                    AliasedArticlesTags.article_id AS "AliasedArticlesTags__article_id",
                                    AliasedArticlesTags.tag_id AS "AliasedArticlesTags__tag_id"
                                FROM articles_tags AliasedArticlesTags
                                WHERE Articles.id = AliasedArticlesTags.article_id
                            )
                            OR EXISTS (
                                SELECT
                                    Comments.id AS "Comments__id"
                                FROM comments Comments
                                WHERE
                                    Articles.id = Comments.article_id
                            )
                            OR EXISTS (
                                SELECT
                                    EvenComments.id AS "EvenComments__id"
                                FROM comments EvenComments
                                WHERE
                                    (EvenComments.id % 2 = 0 AND Articles.id = EvenComments.article_id AND EvenComments.id % 2 = 0)
                            )
                            OR EXISTS (
                                SELECT
                                    SpecialTags.id AS "SpecialTags__id"
                                FROM special_tags SpecialTags
                                WHERE Articles.id = SpecialTags.article_id
                            )
                            OR EXISTS (
                                SELECT
                                    EvenSpecialTags.id AS "EvenSpecialTags__id"
                                FROM special_tags EvenSpecialTags
                                WHERE
                                    (EvenSpecialTags.id % 2 = 0 AND Articles.id = EvenSpecialTags.article_id AND EvenSpecialTags.id % 2 = 0)
                            )
                        )
                    )
                ) AS "Articles__is_deletable"
                FROM articles Articles
                WHERE
                    id = 1';

        $this->assertEquals($this->normalizeSql($expected), $this->normalizeSql(sql($query)));
    }

    public function testIsLinkedQuerySelectVirtualFieldSql()
    {
        $this->addBehaviors();
        $query = $this->table
            ->find('all')
            ->select(['id', 'title', 'is_linked'])
            ->where(['id' => 1]);

        $expected = 'SELECT
                Articles.id AS "Articles__id",
                Articles.title AS "Articles__title",
                (
                    (
                        EXISTS (
                            SELECT
                                ArticlesTags.article_id AS "ArticlesTags__article_id",
                                ArticlesTags.tag_id AS "ArticlesTags__tag_id"
                            FROM articles_tags ArticlesTags
                            WHERE
                                Articles.id = ArticlesTags.article_id
                        )
                        OR EXISTS (
                            SELECT
                                AliasedArticlesTags.article_id AS "AliasedArticlesTags__article_id",
                                AliasedArticlesTags.tag_id AS "AliasedArticlesTags__tag_id"
                            FROM articles_tags AliasedArticlesTags
                            WHERE Articles.id = AliasedArticlesTags.article_id
                        )
                        OR EXISTS (
                            SELECT
                                Comments.id AS "Comments__id"
                            FROM comments Comments
                            WHERE
                                Articles.id = Comments.article_id
                        )
                        OR EXISTS (
                            SELECT
                                EvenComments.id AS "EvenComments__id"
                            FROM comments EvenComments
                            WHERE
                                (EvenComments.id % 2 = 0 AND Articles.id = EvenComments.article_id AND EvenComments.id % 2 = 0)
                        )
                        OR EXISTS (
                            SELECT
                                SpecialTags.id AS "SpecialTags__id"
                            FROM special_tags SpecialTags
                            WHERE Articles.id = SpecialTags.article_id
                        )
                        OR EXISTS (
                            SELECT
                                EvenSpecialTags.id AS "EvenSpecialTags__id"
                            FROM special_tags EvenSpecialTags
                            WHERE
                                (EvenSpecialTags.id % 2 = 0 AND Articles.id = EvenSpecialTags.article_id AND EvenSpecialTags.id % 2 = 0)
                        )
                    )
                ) AS "Articles__is_linked"
                FROM articles Articles
                WHERE
                    id = 1';

        $this->assertEquals($this->normalizeSql($expected), $this->normalizeSql(sql($query)));
    }

    public function testCompleteQueryWithGuardedAssociatedTablesSummaryVirtualFieldsRecords()
    {
        $this->addBehaviors();
        $query = $this->table
            ->find('all')
            ->select(['id', 'title', 'is_updatable', 'is_deletable', 'is_linked'])
            ->where(['id' => 1])
            ->enableHydration(false);
        $actual = $query->all()->toArray();

        $expected = [
            [
                'id' => 1,
                'title' => 'First Article',
                'is_updatable' => false,
                'is_deletable' => false,
                'is_linked' => true,
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the getAllAssociatedTablesAliases method
     */
    public function testGetAllAssociatedTablesAliases()
    {
        $this->addBehaviors();
        $actual = $this->table->getAllAssociatedTablesAliases();

        $expected = ['ArticlesTags', 'AliasedArticlesTags', 'Comments', 'EvenComments', 'SpecialTags', 'EvenSpecialTags'];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the getGuardedAliases method for the "delete" operation.
     */
    public function testGetGuardedAliasesDelete()
    {
        $config = [
            'delete' => [
                'guardedAliases' => ['ArticlesTags', 'Comments'],
                'unguardedAliases' => ['Comments'],
            ],
        ];
        $this->addBehaviors($config);
        $actual = $this->table->getGuardedAliases('delete');

        $expected = ['ArticlesTags'];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the getGuardedAliases method for the "update" operation.
     */
    public function testGetGuardedAliasesUpdate()
    {
        $config = [
            'update' => [
                'unguardedAliases' => ['ArticlesTags', 'EvenSpecialTags'],
            ],
        ];
        $this->addBehaviors($config);
        $actual = $this->table->getGuardedAliases('update');

        $expected = ['AliasedArticlesTags', 'Comments', 'EvenComments', 'SpecialTags'];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the getGuardedAliases method with an invalid parameter.
     *
     * @expectedException UnexpectedValueException
     * @expectedExceptionMessage Invalid parameter "foo" for LibricielCakephp3Database\Model\Behavior\GuardedAssociatedTablesBehavior::getGuardedAliases; accepted values are: "delete"", "update"
     */
    public function testGetGuardedAliasesException()
    {
        $this->addBehaviors();
        $this->table->getGuardedAliases('foo');
    }

    /**
     * Test deleting an Article without any linked record.
     */
    public function testDelete()
    {
        $this->addBehaviors();
        $entity = $this->table->get(3);
        $this->assertTrue($this->table->delete($entity));
    }

    /**
     * Test deleting an Article with linked record.
     */
    public function testDeleteFailure()
    {
        $this->addBehaviors();
        $entity = $this->table->get(1);
        $this->assertFalse($this->table->delete($entity));

        $actual = $entity->getErrors();
        $expected = [
            'id' => [
                '_linkedIn' => 'Cannot delete a Articles with dependant records in ArticlesTags, AliasedArticlesTags, Comments, EvenComments, SpecialTags',
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test updating an Article without any linked record.
     */
    public function testUpdate()
    {
        $this->addBehaviors();
        $entity = $this->table->patchEntity(
            $this->table->get(3),
            ['published' => 'N']
        );
        $this->assertTrue((bool)$this->table->save($entity));
    }

    /**
     * Test updating an Article with linked record.
     */
    public function testUpdateFailure()
    {
        $this->addBehaviors();
        $entity = $this->table->patchEntity(
            $this->table->get(1),
            ['published' => 'N']
        );
        $this->assertFalse($this->table->save($entity));

        $actual = $entity->getErrors();
        $expected = [
            'id' => [
                '_linkedIn' => 'Cannot update a Articles fields with dependant records in ArticlesTags, AliasedArticlesTags, Comments, EvenComments, SpecialTags',
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test updating an Article unguarded field with linked record.
     */
    public function testUpdateConfiguredUnguardedFieldsSuccess()
    {
        $config = [
            'update' => [
                'unguardedFields' => ['published'],
            ],
        ];
        $this->addBehaviors($config);

        $entity = $this->table->patchEntity(
            $this->table->get(1),
            ['published' => 'N']
        );
        $this->assertTrue((bool)$this->table->save($entity));
    }

    /**
     * Test updating an Article guarded field with linked record.
     */
    public function testUpdateConfiguredUnguardedFieldsFailure()
    {
        $config = [
            'update' => [
                'unguardedFields' => ['published'],
            ],
        ];
        $this->addBehaviors($config);

        $entity = $this->table->patchEntity(
            $this->table->get(1),
            ['title' => 'New title']
        );
        $this->assertFalse($this->table->save($entity));

        $actual = $entity->getErrors();
        $expected = [
            'id' => [
                '_linkedIn' => 'Cannot update a Articles fields except published with dependant records in ArticlesTags, AliasedArticlesTags, Comments, EvenComments, SpecialTags',
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test updating an Article guarded field with linked record.
     */
    public function testUpdateConfiguredGuardedFieldsSuccess()
    {
        $config = [
            'update' => [
                'guardedFields' => ['title'],
            ],
        ];
        $this->addBehaviors($config);

        $entity = $this->table->patchEntity(
            $this->table->get(1),
            ['published' => 'N']
        );
        $this->assertTrue((bool)$this->table->save($entity));
    }

    /**
     * Test updating an Article guarded field with linked record.
     */
    public function testUpdateConfiguredGuardedFieldsFailure()
    {
        $config = [
            'update' => [
                'guardedFields' => ['title'],
            ],
        ];
        $this->addBehaviors($config);

        $entity = $this->table->patchEntity(
            $this->table->get(1),
            ['title' => 'New title']
        );
        $this->assertFalse($this->table->save($entity));

        $actual = $entity->getErrors();
        $expected = [
            'id' => [
                '_linkedIn' => 'Cannot update a Articles fields except author_id, body, id, published with dependant records in ArticlesTags, AliasedArticlesTags, Comments, EvenComments, SpecialTags',
            ],
        ];
        $this->assertEquals($expected, $actual);
    }
}
