<?php

namespace LibricielCakephp3Database\Test\TestCase\Model\Behavior;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Exception;

class AssociatedTablesBehaviorArticlesTable extends Table
{
    public $virtual = [];

    public function initialize(array $config): void
    {
        parent::initialize($config);

        // belongsTo without alias and conditions
        $this->belongsTo('Authors');

        // belongsTo with alias and conditions
        $this->belongsTo(
            'EvenAuthors',
            [
                'className' => 'Authors',
                'foreignKey' => 'author_id',
                'conditions' => ['EvenAuthors.id % 2' => 0],
            ]
        );

        // belongsToMany without alias and conditions
        $this->belongsToMany('Tags')
            ->setThrough('ArticlesTags');

        // belongsToMany with alias and conditions
        $this->belongsToMany(
            'AliasedTags',
            [
                'className' => 'Tags',
                'foreignKey' => 'article_id',
                'targetForeignKey' => 'tag_id',
                'joinTable' => 'articles_tags',
                // @info: conditions do not seem to work on join tables
            ]
        )->setThrough(
            TableRegistry::getTableLocator()->get(
                'AliasedArticlesTags',
                [
                    'className' => 'ArticlesTags',
                    'table' => 'articles_tags',
                ]
            )
        );

        // hasToMany without alias and conditions
        $this->hasMany('Comments');

        // hasToMany with alias and conditions
        $this->hasMany(
            'EvenComments',
            [
                'className' => 'Comments',
                'foreignKey' => 'article_id',
                'conditions' => ['EvenComments.id % 2' => 0],
            ]
        );

        // hasOne without alias and conditions
        $this->hasOne('SpecialTags');

        // hasOne with alias and conditions
        $this->hasOne(
            'EvenSpecialTags',
            [
                'className' => 'SpecialTags',
                'foreignKey' => 'article_id',
                'conditions' => ['EvenSpecialTags.id % 2' => 0],
            ]
        );
    }
}

/**
 * Class AssociatedTablesBehaviorTest
 * This is the unit test class for the AssociatedTablesBehavior class.
 *
 * @package LibricielCakephp3Database\Test\TestCase\Model\Behavior
 */
class AssociatedTablesBehaviorTest extends TestCase
{
    /**
     * The Table object for Articles.
     *
     * @var Table
     */
    public $table = null;

    /**
     * fixtures
     *
     * @var array
     */
    public $fixtures = [
        'core.Articles',
        'core.Tags',
        'core.ArticlesTags',
        'core.Authors',
        'core.Comments',
        'core.SpecialTags',
    ];

    /**
     * Normalize spaces in a SQL query.
     *
     * @param string $sql
     * @return string
     */
    protected function normalizeSql(string $sql): string
    {
        $replacements = ['( ' => '(', ' )' => ')'];

        return str_replace(
            array_keys($replacements),
            array_values($replacements),
            preg_replace('/\s+/', ' ', $sql)
        );
    }

    /**
     * Set up the Articles table and its associations.
     */
    public function setUp(): void
    {
        parent::setUp();

        $config = TableRegistry::getTableLocator()->exists('Articles')
            ? []
            : ['className' => AssociatedTablesBehaviorArticlesTable::class, 'table' => 'articles'];
        $this->Articles = TableRegistry::getTableLocator()->get('Articles', $config);
        $this->Articles->addBehavior('LibricielCakephp3Database.AssociatedTables');
    }

    public function testGetAssociationByAliasBelongsToNoAlias()
    {
        $actual = $this->Articles->getAssociationByAlias('Authors');
        $this->assertEquals('Authors', $actual->getAlias());
        $this->assertEquals('authors', $actual->getTable());
        $this->assertEquals('Cake\ORM\Association\BelongsTo', get_class($actual));
    }

    public function testGetAssociationByAliasBelongsToAliasAndConditions()
    {
        $actual = $this->Articles->getAssociationByAlias('EvenAuthors');
        $this->assertEquals('EvenAuthors', $actual->getAlias());
        $this->assertEquals('authors', $actual->getTable());
        $this->assertEquals('Cake\ORM\Association\BelongsTo', get_class($actual));
    }

    public function testGetAssociationByAliasBelongsToManyThroughNoAlias()
    {
        $actual = $this->Articles->getAssociationByAlias('ArticlesTags');
        $this->assertEquals('ArticlesTags', $actual->getAlias());
        $this->assertEquals('articles_tags', $actual->getTable());
        $this->assertEquals('Cake\ORM\Association\HasMany', get_class($actual));
    }

    public function testGetAssociationByAliasBelongsToManyThroughAliasAndConditions()
    {
        $actual = $this->Articles->getAssociationByAlias('AliasedArticlesTags');
        $this->assertEquals('AliasedArticlesTags', $actual->getAlias());
        $this->assertEquals('articles_tags', $actual->getTable());
        $this->assertEquals('Cake\ORM\Association\HasMany', get_class($actual));
    }

    public function testGetAssociationByAliasHasManyNoAlias()
    {
        $actual = $this->Articles->getAssociationByAlias('Comments');
        $this->assertEquals('Comments', $actual->getAlias());
        $this->assertEquals('comments', $actual->getTable());
        $this->assertEquals('Cake\ORM\Association\HasMany', get_class($actual));
    }

    public function testGetAssociationByAliasHasManyAliasAndConditions()
    {
        $actual = $this->Articles->getAssociationByAlias('EvenComments');
        $this->assertEquals('EvenComments', $actual->getAlias());
        $this->assertEquals('comments', $actual->getTable());
        $this->assertEquals('Cake\ORM\Association\HasMany', get_class($actual));
    }

    public function testGetAssociationByAliasHasOneNoAlias()
    {
        $actual = $this->Articles->getAssociationByAlias('SpecialTags');
        $this->assertEquals('SpecialTags', $actual->getAlias());
        $this->assertEquals('special_tags', $actual->getTable());
        $this->assertEquals('Cake\ORM\Association\HasOne', get_class($actual));
    }

    public function testGetAssociationByAliasHasOneAliasAndConditions()
    {
        $actual = $this->Articles->getAssociationByAlias('EvenSpecialTags');
        $this->assertEquals('EvenSpecialTags', $actual->getAlias());
        $this->assertEquals('special_tags', $actual->getTable());
        $this->assertEquals('Cake\ORM\Association\HasOne', get_class($actual));
    }


    public function testGetAssociationByAliasInexistantAssociation()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Class LibricielCakephp3Database\Test\TestCase\Model\Behavior\AssociatedTablesBehaviorArticlesTable does not have a Foo association');
        $this->Articles->getAssociationByAlias('Foo');
    }

    public function testGetAssociationFullConditionsBelongsToNoAlias()
    {
        $actual = $this->Articles->getAssociationFullConditions('Authors');
        $expected = ['Articles.author_id = Authors.id'];
        $this->assertEquals($expected, $actual);
    }

    public function testGetAssociationFullConditionsBelongsToAliasAndConditions()
    {
        $actual = $this->Articles->getAssociationFullConditions('EvenAuthors');
        $expected = [
            'Articles.author_id = EvenAuthors.id',
            'EvenAuthors.id % 2' => 0,
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testGetAssociationFullConditionsHasManyNoAlias()
    {
        $actual = $this->Articles->getAssociationFullConditions('Comments');
        $expected = ['Articles.id = Comments.article_id'];
        $this->assertEquals($expected, $actual);
    }

    public function testGetAssociationFullConditionsHasManyAliasAndConditions()
    {
        $actual = $this->Articles->getAssociationFullConditions('EvenComments');
        $expected = [
            'Articles.id = EvenComments.article_id',
            'EvenComments.id % 2' => 0,
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testGetAssociationFullConditionsHasOneNoAlias()
    {
        $actual = $this->Articles->getAssociationFullConditions('SpecialTags');
        $expected = ['Articles.id = SpecialTags.article_id'];
        $this->assertEquals($expected, $actual);
    }

    public function testGetAssociationFullConditionsHasOneAliasAndConditions()
    {
        $actual = $this->Articles->getAssociationFullConditions('EvenSpecialTags');
        $expected = [
            'Articles.id = EvenSpecialTags.article_id',
            'EvenSpecialTags.id % 2' => 0,
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testGetAssociationFullConditionsBelongsToMany()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Association Tags of type manyToMany in class LibricielCakephp3Database\Test\TestCase\Model\Behavior\AssociatedTablesBehaviorArticlesTable cannot be joined');
        $this->Articles->getAssociationFullConditions('Tags');
    }

    public function testGetAssociationFullConditionsBelongsToManyThroughNoAlias()
    {
        $actual = $this->Articles->getAssociationFullConditions('ArticlesTags');
        $expected = ['Articles.id = ArticlesTags.article_id'];
        $this->assertEquals($expected, $actual);
    }

    public function testGetAssociationFullConditionsBelongsToManyThroughAliasAndConditions()
    {
        $actual = $this->Articles->getAssociationFullConditions('AliasedArticlesTags');
        $expected = [
            'Articles.id = AliasedArticlesTags.article_id',
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testGetAssociationFullConditionsInexistantAssociation()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Class LibricielCakephp3Database\Test\TestCase\Model\Behavior\AssociatedTablesBehaviorArticlesTable does not have a Foo association');
        $this->Articles->getAssociationFullConditions('Foo');
    }
}
