<?php

namespace LibricielCakephp3Database\Test\TestCase;

use Cake\Datasource\ConnectionManager;
use Cake\TestSuite\TestCase;
use function LibricielCakephp3Database\array_diff_assoc_recursive as array_diff_assoc_recursive;
use function LibricielCakephp3Database\sql as sql;

/**
 * Class BasicsTest
 * This is the unit test class for the functions defined in src/basics.php.
 *
 * @package LibricielCakephp3Database\Test\TestCase
 */
class BasicsTest extends TestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'core.Articles',
    ];

    /**
     * Test the "sql" function.
     *
     * @return void
     */
    public function testSql()
    {
        $query = new \Cake\Database\Query(ConnectionManager::get('test'));

        $this->assertEquals('SELECT id FROM articles WHERE id = 5', sql($query->select(['id'])->from('articles')->where(['id' => 5])));
    }

    /**
     * Test the "array_diff_assoc_recursive" function.
     *
     * @return void
     */
    public function testArrayDiffAssocRecursive()
    {
        $actual = array_diff_assoc_recursive([], []);
        $expected = [];
        $this->assertEquals($expected, $actual);

        $actual = array_diff_assoc_recursive(['foo' => 'bar'], ['bar' => 'baz']);
        $expected = ['foo' => 'bar'];
        $this->assertEquals($expected, $actual);

        $actual = array_diff_assoc_recursive(['foo' => 'bar'], ['foo' => ['bar' => 'baz']]);
        $expected = ['foo' => 'bar'];
        $this->assertEquals($expected, $actual);

        $actual = array_diff_assoc_recursive(['foo' => ['bar' => 'baz', 'bar' => 'buz']], ['foo' => ['bar' => 'baz']]);
        $expected = ['foo' => ['bar' => 'buz']];
        $this->assertEquals($expected, $actual);

        $reference = [
            'actors.organization_id = organizations.id' => [
                'from' => [
                    'table' => 'actors',
                    'field' => 'organization_id',
                    'unique' => false,
                ],
                'to' => [
                    'table' => 'organizations',
                    'field' => 'id',
                    'unique' => true,
                ],
                'delete' => 'cascade',
                'update' => 'cascade',
            ],
            'actors.structure_id = structures.id' => [
                'from' => [
                    'table' => 'actors',
                    'field' => 'structure_id',
                    'unique' => false,
                ],
                'to' => [
                    'table' => 'structures',
                    'field' => 'id',
                    'unique' => true,
                ],
                'delete' => 'cascade',
                'update' => 'cascade',
            ],
        ];

        $candidate = [
            'actors.structure_id = structures.id' => [
                'from' => [
                    'table' => 'actors',
                    'field' => 'structure_id',
                    'unique' => true,
                ],
                'to' => [
                    'table' => 'structures',
                    'field' => 'id',
                    'unique' => true,
                ],
                'delete' => 'noAction',
                'update' => 'noAction',
            ],
            'actors.id = actors_acts.actor_id' => [
                'from' => [
                    'table' => 'actors_acts',
                    'field' => 'actor_id',
                    'unique' => false,
                ],
                'to' => [
                    'table' => 'actors',
                    'field' => 'id',
                    'unique' => true,
                ],
                'delete' => 'cascade',
                'update' => 'cascade',
            ],
        ];
        $actual = array_diff_assoc_recursive($reference, $candidate);
        $expected = [
            'actors.organization_id = organizations.id' => [
                'from' => [
                    'table' => 'actors',
                    'field' => 'organization_id',
                    'unique' => false,
                ],
                'to' => [
                    'table' => 'organizations',
                    'field' => 'id',
                    'unique' => true,
                ],
                'delete' => 'cascade',
                'update' => 'cascade',
            ],
            'actors.structure_id = structures.id' => [
                'from' => [
                    'unique' => false,
                ],
                'delete' => 'cascade',
                'update' => 'cascade',
            ],
        ];
        $this->assertEquals($expected, $actual);
    }
}
