<?php
declare(strict_types=1);

namespace LibricielCakephp3Database;

use Cake\Database\Query;
use RuntimeException;

/**
 * Returns the SQL code with interpolated variables from a Cake Query object.
 *
 * Adapted from:
 *  - vendor/cakephp/cakephp/src/Database/Query.php (sql and __debugInfo() method in CakePHP 3.7.8)
 *  - vendor/cakephp/debug_kit/src/DebugSql.php (interpolate method in DebugKit 3.19.0)
 *
 * @param \Cake\Database\Query $query The query to be transformed to SQL
 * @return string
 */
function sql(Query $query): string
{
    try {
        set_error_handler(function ($errno, $errstr) {
            throw new RuntimeException($errstr, $errno);
        }, E_ALL);
        $sql = $query->sql();
        $bindings = method_exists($query, 'getValueBinder')
            ? $query->getValueBinder()->bindings()
            : $query->valueBinder()->bindings();
    } catch (RuntimeException $e) {
        $sql = 'SQL could not be generated for this query as it is incomplete.';
        $bindings = [];
    } finally {
        restore_error_handler();
    }

    $params = array_map(function ($binding) {
        $value = $binding['value'];

        if ($value === null) {
            return 'NULL';
        }
        if (is_bool($value)) {
            return $value ? '1' : '0';
        }

        if (is_string($value)) {
            $replacements = [
                '$' => '\\$',
                '\\' => '\\\\\\\\',
                "'" => "''",
            ];

            $value = strtr($value, $replacements);

            return "'$value'";
        }

        return $value;
    }, $bindings);

    $keys = [];
    $paramsKeys = array_keys($params);
    $limit = is_int($paramsKeys[0]) ? 1 : -1;
    foreach ($paramsKeys as $key) {
        $keys[] = is_string($key) ? "/$key\b/" : '/[?]/';
    }

    return preg_replace($keys, $params, $sql, $limit);
}

/**
 * Returns a recursive array diff with keys and values taken into account.
 *
 * @param array $reference The reference array, to check against
 * @param array $candidate The candidate array
 * @return array
 */
function array_diff_assoc_recursive(array $reference, array $candidate): array
{
    $result = [];
    $keys = array_keys($reference);
    foreach ($keys as $key) {
        if (array_key_exists($key, $candidate) === false) {
            $result[$key] = $reference[$key];
        } elseif (is_array($reference[$key]) === true && is_array($candidate[$key]) === true) {
            $diff = array_diff_assoc_recursive($reference[$key], $candidate[$key]);
            if (empty($diff) === false) {
                $result[$key] = $diff;
            }
        } elseif ($reference[$key] !== $candidate[$key]) {
            $result[$key] = $reference[$key];
        }
    }

    return $result;
}
