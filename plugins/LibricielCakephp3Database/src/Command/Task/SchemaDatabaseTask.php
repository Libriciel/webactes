<?php
declare(strict_types=1);

namespace LibricielCakephp3Database\Command\Task;

use Cake\Console\Shell;
use Cake\Database\Exception;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
// @codingStandardsIgnoreFile

class SchemaDatabaseTask extends Shell
{
    /**
     * Tasks used by this task.
     *
     * @var array
     */
    public $tasks = [
        'LibricielCakephp3Database.SchemaUtilities',
    ];

    /**
     * Returns the table names from the database schema.
     *
     * @param array $ignored A list of table names to be ignored
     * @return array
     */
    public function getTables(array $ignored = []): array
    {
        $result = array_unique(array_diff($this->SchemaUtilities->getConnection()->getSchemaCollection()->listTables(), $ignored));
        sort($result);

        return $result;
    }

    /**
     * Returns the list of tables and the real foreign keys from the database.
     *
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @return array
     */
    public function getSchema(array $ignored = []): array
    {
        $ignored += ['constraints' => [], 'tables' => []];
        $return = [];

        foreach ($this->getTables($ignored['tables']) as $tableName) {
            $return[$tableName] = [];
            $table = TableRegistry::getTableLocator()->get($tableName);
            $schema = $table->getSchema();
            $constraints = $schema->constraints();

            $unique = [];
            foreach ($constraints as $constraintName) {
                if (in_array($constraintName, $ignored['constraints']) === false) {
                    $constraint = $schema->getConstraint($constraintName);
                    if ($constraint['type'] === 'unique') {
                        $unique[] = $constraint['columns'];
                    } elseif ($constraint['type'] === 'foreign') {
                        $association = [
                            'from' => [
                                'table' => $tableName,
                                'field' => count($constraint['columns']) === 1
                                    ? $constraint['columns'][0]
                                    : $constraint['columns'],
                                'unique' => array_search($constraint['columns'], $unique, true) !== false,
                            ],
                            'to' => [
                                'table' => $constraint['references'][0],
                                'field' => $constraint['references'][1],
                                'unique' => true,
                            ],
                        ];

                        if ($this->params['ignore-foreign-key-actions'] !== true) {
                            $association['delete'] = $constraint['delete'];
                            $association['update'] = $constraint['update'];
                        }

                        $key = $this->SchemaUtilities->getAssociationKey($association['from'], $association['to']);
                        $return[$tableName][$key] = $association;

                        $otherKey = $this->SchemaUtilities->getAssociationKey($association['from'], $association['to']);
                        $return[$constraint['references'][0]][$otherKey] = $association;
                    }
                }
            }
        }

        return $return;
    }

    /**
     * Returns the database table schemas by table name as an array with the "fields", "constraints" and "indexes" keys.
     *
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @return array
     */
    public function get(array $ignored = []): array
    {
        $ignored += ['constraints' => [], 'tables' => []];
        $return = [];
        $extras = [];

        foreach ($this->getTables($ignored['tables']) as $tableName) {
            try {
                $table = TableRegistry::getTableLocator()->get($tableName);
                $schema = $table->getSchema();

                $tableSchema = (array)Hash::get($return, $tableName) + [
                        'constraints' => [],
                        'fields' => [],
                        'indexes' => [],
                    ];

                // constraints
                foreach ($schema->constraints() as $name) {
                    if (in_array($name, $ignored['constraints']) === false) {
                        $constraint = $schema->getConstraint($name);
                        if ($this->params['ignore-constraint-names'] !== true) {
                            $constraint['name'] = $name;
                        }

                        if ($this->params['ignore-foreign-key-actions'] === true) {
                            unset($constraint['delete'], $constraint['update']);
                        }

                        switch ($constraint['type']) {
                            case 'primary':
                                $key = $this->SchemaUtilities->getPrimaryArrayKey($tableName, $constraint['columns']);
                                $tableSchema['constraints'][$key] = $constraint;
                                break;
                            case 'unique':
                                $key = $this->SchemaUtilities->getUniqueArrayKey($tableName, $constraint['columns']);
                                sort($constraint['columns']);
                                $tableSchema['constraints'][$key] = $constraint;
                                break;
                            case 'foreign':
                                $key = $this->SchemaUtilities->getForeignArrayKey($tableName, $constraint['columns'], $constraint['references']);
                                $tableSchema['constraints'][$key] = $constraint;

                                $extraKey = [$constraint['references'][0], 'constraints', $key];
                                $extras[] = ['key' => $extraKey, 'constraint' => $constraint];
                                break;
                        }
                    }
                }

                // fields
                foreach ($schema->columns() as $name) {
                    $tableSchema['fields'][$name] = $schema->getColumn($name);
                }

                // indexes
                foreach ($schema->indexes() as $name) {
                    $index = $schema->getIndex($name);
                    if ($this->params['ignore-constraint-names'] !== true) {
                        $index['name'] = $name;
                    }
                    $key = $this->SchemaUtilities->getIndexArrayKey($tableName, $index['columns']);
                    $tableSchema['indexes'][$key] = $index;
                }

                $return[$tableName] = $tableSchema;
            } catch (Exception $exc) {
                $this->_exceptions[] = $exc->getMessage();
            }
        }

        if (empty($extras) === false) {
            foreach ($extras as $extra) {
                $return[$extra['key'][0]][$extra['key'][1]][$extra['key'][2]] = $extra['constraint'];
            }
        }

        return $return;
    }

    /**
     * Returns the constraints (unique and foreign keys) for each of the tables from the database.
     *
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @return array
     */
    public function getRules(array $ignored = []): array
    {
        $ignored += ['constraints' => [], 'tables' => []];
        $return = $this->get($ignored);

        foreach ($return as $tableName => $schema) {
            $constraints = $schema['constraints'];
            foreach (array_keys($constraints) as $name) {
                if (strpos($name, "{$tableName}.") === 0) {
                    unset($constraints[$name]['delete'], $constraints[$name]['update']);
                } else {
                    unset($constraints[$name]);
                }
            }
            $return[$tableName] = compact('constraints');
        }

        return $return;
    }
}
