<?php
declare(strict_types=1);

namespace LibricielCakephp3Database\Command\Task;

use Cake\Console\Shell;
use Cake\Utility\Inflector;
// @codingStandardsIgnoreFile

class SchemaReportScreenTask extends Shell
{
    /**
     * Tasks used by this task.
     *
     * @var array
     */
    public $tasks = [
        'LibricielCakephp3Database.SchemaUtilities',
    ];

    /**
     * Returns an icon representing success or failure.
     *
     * @param bool $success Wether the icon should represent success or failure.
     * @return string
     */
    protected function _icon(bool $success)
    {
        if ($success === true) {
            return "<success>\xE2\x9C\x94</success>";
        } else {
            return "<error>\xE2\x9D\x8C</error>";
        }
    }

    /**
     * Prints a report for the "_extra", "_missing" and "_extra" keys of the report, which are usually contained in each
     * of the sub-keys of the "fields", "constraints", "indexes" keys of the report.
     *
     * @param array $report A report part, containing the comparison between a source and a target
     * @param string $source The name of the source, to be used in messages
     * @param string $target The name of the target, to be used in messages
     * @return void
     */
    protected function _generateValues(array $report, string $source, string $target)
    {
        $missing = $report['_missing'];
        $extra = $report['_extra'];
        $different = $report['_different'];

        if (count($missing) + count($extra) > 0) {
            foreach ($different as $idx) {
                $this->_io->err(sprintf(
                    "\t\t\t%s Value of <info>%s</info> (<info>%s</info>) is erroneous in the %s (<info>%s</info>)",
                    $this->_icon(false),
                    $idx,
                    $this->SchemaUtilities->value($report['_missing'][$idx]),
                    $target,
                    $this->SchemaUtilities->value($report['_extra'][$idx])
                ));
            }
            foreach (array_keys($missing) as $idx) {
                if (in_array($idx, $different) === false) {
                    $this->_io->err(sprintf(
                        "\t\t\t%s Value of <info>%s</info> (<info>%s</info>) is missing in the %s",
                        $this->_icon(false),
                        $idx,
                        $this->SchemaUtilities->value($report['_missing'][$idx]),
                        $target
                    ));
                }
            }
            foreach (array_keys($extra) as $idx) {
                if (in_array($idx, $different) === false) {
                    $this->_io->err(sprintf(
                        "\t\t\t%s Value of <info>%s</info> (<info>%s</info>) is missing in the %s",
                        $this->_icon(false),
                        $idx,
                        $this->SchemaUtilities->value($report['_extra'][$idx]),
                        $source
                    ));
                }
            }
        }
    }

    /**
     * Prints out a report from a comparison from a source and a target (exemple, the database tables and the fixtures
     * classes).
     *
     * @param array $report A comparison between a source and a target
     * @param string $source The name of the source, to be used in messages
     * @param string $target The name of the target, to be used in messages
     * @return void
     * @see \LibricielCakephp3Database\Command\Task\SchemaReportTask::getReport()
     */
    public function generate(array $report, string $source, string $target)
    {
        $keys = ['fields', 'constraints', 'indexes'];
        foreach ($report as $tableName => $schema) {
            if ($tableName !== '_errors') {
                if ($schema['_missing'] !== false) {
                    $this->_io->err(sprintf('%s Table <info>%s</info> not found in %s', $this->_icon(false), $tableName, $schema['_missing']));
                } else {
                    $method = $schema['_errors'] === 0 ? 'out' : 'err';
                    $this->_io->{$method}(sprintf('%s Table <info>%s</info>', $this->_icon($schema['_errors'] === 0), $tableName));

                    foreach ($keys as $key) {
                        if (isset($schema[$key]) === true) {
                            $title = Inflector::camelize($key);
                            $singular = Inflector::singularize($title);

                            if ($schema[$key]['_errors'] !== 0) {
                                $this->_io->err(sprintf("\t%s %s", $this->_icon(false), $title));

                                if (isset($schema[$key]['_missing']) === true) {
                                    foreach ($schema[$key]['_missing'] as $name => $missing) {
                                        if ($missing !== false) {
                                            $this->_io->err(sprintf("\t\t%s %s <info>%s</info> not found in %s", $this->_icon(false), $singular, $name, $missing));
                                        } else {
                                            $method = $schema[$key][$name]['_errors'] === 0 ? 'out' : 'err';
                                            $this->_io->{$method}(sprintf("\t\t%s %s <info>%s</info>", $this->_icon($schema[$key][$name]['_errors'] === 0), $singular, $name));
                                            $this->_generateValues($schema[$key][$name], $source, $target);
                                        }
                                    }
                                }
                            } else {
                                $method = $schema[$key]['_errors'] === 0 ? 'out' : 'err';
                                $this->_io->{$method}(sprintf("\t%s %s", $this->_icon($schema[$key]['_errors'] === 0), $title));

                                if (isset($schema[$key]['_missing']) === true) {
                                    foreach ($schema[$key]['_missing'] as $name => $missing) {
                                        if ($missing !== false) {
                                            $this->_io->err(sprintf("\t\t%s %s <info>%s</info> not found in %s", $this->_icon(false), $singular, $name, $missing));
                                        } else {
                                            $method = $schema[$key]['_errors'] === 0 ? 'out' : 'err';
                                            $this->_io->{$method}(sprintf("\t\t%s %s <info>%s</info>", $this->_icon($schema[$key]['_errors'] === 0), $singular, $name));
                                            $this->_generateValues($schema[$key][$name], $source, $target);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->_io->out("\n");

        $singular = Inflector::singularize($target);
        if ($report['_errors'] === 0) {
            $msgstr = __("No error was found in {$singular} classes");
            $this->_io->out(sprintf('%s %s', $this->_icon(true), $msgstr));
        } else {
            $msgstr = sprintf(__n("%d {$singular} class has errors", "%d {$singular} classes have errors", $report['_errors']), $report['_errors']);
            $this->_io->err(sprintf('%s %s', $this->_icon(false), $msgstr));
        }
    }
}
