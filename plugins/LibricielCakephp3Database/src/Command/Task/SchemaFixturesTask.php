<?php
declare(strict_types=1);

namespace LibricielCakephp3Database\Command\Task;

use Cake\Console\Shell;
use Cake\Utility\Inflector;
// @codingStandardsIgnoreFile

class SchemaFixturesTask extends Shell
{
    /**
     * Tasks used by this task.
     *
     * @var array
     */
    public $tasks = [
        'LibricielCakephp3Database.SchemaUtilities',
    ];

    /**
     * Returns the table names from the fixtures classes.
     *
     * @param array $ignored A list of table names to be ignored
     * @return array
     */
    public function getTables(array $ignored = []): array
    {
        $result = [];
        $paths = glob(ROOT . DS . 'tests' . DS . 'Fixture' . DS . '*Fixture.php');
        foreach ($paths as $path) {
            $tableName = preg_replace('/Fixture\.php$/', '', basename($path));
            if ($tableName !== $path) {
                $result[] = Inflector::underscore($tableName);
            }
        }
        $result = array_unique(array_diff($result, $ignored));

        sort($result);

        return $result;
    }

    /**
     * Returns the fixtures schemas by table name as an array with the "fields", "constraints" and "indexes" keys.
     *
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @return array
     */
    public function get(array $ignored = []): array
    {
        $ignored += ['constraints' => [], 'tables' => []];
        $return = [];
        $extras = [];

        foreach ($this->getTables($ignored['tables']) as $tableName) {
            $className = Inflector::camelize($tableName) . 'Fixture';

            $vars = get_class_vars("App\\Test\\Fixture\\{$className}");
            $fields = $vars['fields'];

            if (array_key_exists('_constraints', $fields) === true) {
                $constraints = [];
                foreach ($fields['_constraints'] as $name => $constraint) {
                    if (in_array($name, $ignored['constraints']) === false) {
                        if ($this->params['ignore-constraint-names'] !== true) {
                            $constraint['name'] = $name;
                        }

                        if ($this->params['ignore-foreign-key-actions'] === true) {
                            unset($constraint['delete'], $constraint['update']);
                        }

                        switch ($constraint['type']) {
                            case 'primary':
                                $key = $this->SchemaUtilities->getPrimaryArrayKey($tableName, $constraint['columns']);
                                break;
                            case 'unique':
                                sort($constraint['columns']);
                                $key = $this->SchemaUtilities->getUniqueArrayKey($tableName, $constraint['columns']);
                                break;
                            case 'foreign':
                                $key = $this->SchemaUtilities->getForeignArrayKey($tableName, $constraint['columns'], $constraint['references']);
                                $extraKey = [$constraint['references'][0], 'constraints', $key];
                                $extras[] = ['key' => $extraKey, 'constraint' => $constraint];
                                break;
                        }

                        $constraints[$key] = $constraint;
                    }
                }
                unset($fields['_constraints']);
            } else {
                $constraints = [];
            }

            if (array_key_exists('_indexes', $fields) === true) {
                $indexes = [];
                foreach ($fields['_indexes'] as $name => $index) {
                    $key = $this->SchemaUtilities->getIndexArrayKey($tableName, $index['columns']);
                    $indexes[$key] = array_merge(
                        ['type' => null, 'columns' => [], 'length' => []],
                        $index
                    );
                    if ($this->params['ignore-constraint-names'] !== true) {
                        $indexes[$key]['name'] = $name;
                    }
                }
                unset($fields['_indexes']);
            } else {
                $indexes = [];
            }

            $return[$tableName] = compact('fields', 'constraints', 'indexes');
        }

        if (empty($extras) === false) {
            foreach ($extras as $extra) {
                $return[$extra['key'][0]][$extra['key'][1]][$extra['key'][2]] = $extra['constraint'];
            }
        }

        return $return;
    }
}
