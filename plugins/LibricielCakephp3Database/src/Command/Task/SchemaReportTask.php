<?php
declare(strict_types=1);

namespace LibricielCakephp3Database\Command\Task;

use Cake\Console\Shell;
use Cake\Utility\Hash;
use function LibricielCakephp3Database\array_diff_assoc_recursive as array_diff_assoc_recursive;
// @codingStandardsIgnoreFile

class SchemaReportTask extends Shell
{
    /**
     * Tasks used by this task.
     *
     * @var array
     */
    public $tasks = [
        'LibricielCakephp3Database.SchemaUtilities',
    ];

    /**
     * Returns a comparison of recursive arrays containing, for each table name, the keys "fields", "constraints" and
     * "indexes".
     *
     * The returned array will have the special keys "_missing" and "_errors" for table names, "fields", "constraints"
     * and "indexes" keys and for the top level, as well as "_missing", "_extra" and "_different" sub-keys for the
     * "fields", "constraints" and "indexes" keys.
     *
     * @param array $sourceSchema A recursive array of what things should look like
     * @param string $source The name of the source, to be used in messages
     * @param array $targetSchema A recursive array to compare against the source schema
     * @param string $target The name of the target, to be used in messages
     * @return array
     */
    public function getReport(array $sourceSchema, string $source, array $targetSchema, string $target): array
    {
        $result = [];

        $tableNames = array_unique(array_merge(array_keys($sourceSchema), array_keys($targetSchema)));
        sort($tableNames);

        $keys = ['fields', 'constraints', 'indexes'];

        foreach ($tableNames as $tableName) {
            if (array_key_exists($tableName, $sourceSchema) === false) {
                $result[$tableName]['_missing'] = $source;
            } elseif (array_key_exists($tableName, $targetSchema) === false) {
                $result[$tableName]['_missing'] = $target;
            } else {
                $result[$tableName]['_missing'] = false;

                $table = $sourceSchema[$tableName];
                $fixture = $targetSchema[$tableName];

                foreach ($keys as $key) {
                    if (isset($table[$key]) === true && isset($fixture[$key]) === true) {
                        $result[$tableName][$key] = [];

                        $names = array_unique(
                            array_merge(
                                array_keys($table[$key]),
                                array_keys($fixture[$key])
                            )
                        );
                        foreach ($names as $name) {
                            if (array_key_exists($name, $table[$key]) === false) {
                                $result[$tableName][$key]['_missing'][$name] = $source;
                            } elseif (array_key_exists($name, $fixture[$key]) === false) {
                                $result[$tableName][$key]['_missing'][$name] = $target;
                            } else {
                                $result[$tableName][$key]['_missing'][$name] = false;

                                $missing = array_diff_assoc_recursive($table[$key][$name], $fixture[$key][$name]);
                                $extra = array_diff_assoc_recursive($fixture[$key][$name], $table[$key][$name]);
                                $different = array_intersect(array_keys($missing), array_keys($extra));

                                $result[$tableName][$key][$name]['_missing'] = $missing;
                                $result[$tableName][$key][$name]['_extra'] = $extra;
                                $result[$tableName][$key][$name]['_different'] = $different;

                                $result[$tableName][$key][$name]['_errors'] = count($missing) + count($extra);
                            }
                        }

                        $result[$tableName][$key]['_errors'] =
                            count(array_filter(Hash::extract($result[$tableName][$key], '{s}._errors')))
                            + (
                            isset($result[$tableName][$key]['_missing']) === true
                                ? count(array_filter($result[$tableName][$key]['_missing']))
                                : 0
                            );
                    }
                }
            }

            $result[$tableName]['_errors'] = count(array_filter(Hash::extract($result[$tableName], '{s}._errors')));
        }

        $result['_errors'] = count(array_filter(Hash::extract($result, '{s}._errors')))
            + count(array_filter(Hash::extract($result, '{s}._missing')));

        return $result;
    }
}
