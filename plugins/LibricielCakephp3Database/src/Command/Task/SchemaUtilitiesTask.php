<?php
declare(strict_types=1);

namespace LibricielCakephp3Database\Command\Task;

use Cake\Console\Shell;
use Cake\Datasource\ConnectionInterface;
use Cake\Datasource\ConnectionManager;
// @codingStandardsIgnoreFile

class SchemaUtilitiesTask extends Shell
{
    /**
     * Returns a connection to the database as defined by the --connection parameter.
     *
     * @return \Cake\Datasource\ConnectionInterface
     */
    public function getConnection(): ConnectionInterface
    {
        return ConnectionManager::get($this->params['connection']);
    }

    /**
     * Returns a string representation for a given value
     *
     * @param mixed $value The value to be represented as string
     * @return string
     */
    public static function value($value): string
    {
        return trim(json_encode($value), '""');
    }

    /**
     * Returns the key representing a table association.
     *
     * @param array $origin The association from side, containing the "table" and "field" keys
     * @param array $destination The association from side, containing the "table" and "field" keys
     * @return string
     */
    public static function getAssociationKey(array $origin, array $destination): string
    {
        return sprintf(
            '%s.%s = %s.%s',
            $origin['table'],
            $origin['field'],
            $destination['table'],
            $destination['field']
        );
    }

    /**
     * Returns a string to be used as an array key for a unique constraint.
     *
     * @param string $tableName The name of the table holding the constraint
     * @param array $columns The name of the columns to be unique
     * @return string
     */
    public static function getUniqueArrayKey(string $tableName, array $columns): string
    {
        $tmp = $columns;
        sort($tmp);

        return sprintf('%s.unique(%s)', $tableName, implode(',', $tmp));
    }

    /**
     * Returns a string to be used as an array key for a foreign key constraint.
     *
     * @param string $tableName The name of the table holding the constraint
     * @param array $columns The name of the columns holding the constraint
     * @param array $references The name of the table and columns the constraint references
     * @return string
     */
    public static function getForeignArrayKey(string $tableName, array $columns, array $references): string
    {
        $tmp = $columns;
        sort($tmp);

        return sprintf('%s.foreign(%s) -> %s', $tableName, implode(',', $tmp), implode('.', $references));
    }

    /**
     * Returns a string to be used as an array key for an index.
     *
     * @param string $tableName The name of the table holding the index
     * @param array $columns The name of the columns holding the index
     * @return string
     */
    public static function getIndexArrayKey(string $tableName, array $columns): string
    {
        $tmp = $columns;
        sort($tmp);

        return sprintf('%s.index(%s)', $tableName, implode(',', $tmp));
    }

    /**
     * Returns a string to be used as an array key for a primey key.
     *
     * @param string $tableName The name of the table holding the primary key
     * @param array $columns The name of the columns holding the primary key
     * @return string
     */
    public static function getPrimaryArrayKey(string $tableName, array $columns): string
    {
        $tmp = $columns;
        sort($tmp);

        return sprintf('%s.primary(%s)', $tableName, implode(',', $tmp));
    }
}
