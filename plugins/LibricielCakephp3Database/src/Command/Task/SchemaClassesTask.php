<?php
declare(strict_types=1);

namespace LibricielCakephp3Database\Command\Task;

use Cake\Console\Shell;
use Cake\Core\App;
use Cake\Core\Plugin;
use Cake\Database\Exception;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
// @codingStandardsIgnoreFile
class SchemaClassesTask extends Shell
{
    /**
     * Tasks used by this task.
     *
     * @var array
     */
    public $tasks = [
        'LibricielCakephp3Database.SchemaUtilities',
    ];

    /**
     * The list of exceptions raised when trying to access classes, tables or fixtures.
     *
     * @var array
     */
    protected $_exceptions = [];

    /**
     * Returns the paths (core, plugins and app) for a given class type.
     *
     * @param string $type The type of class for which to find the paths (ie. 'Model/Table')
     * @param bool $plugins Whether or not classes in plugins should be considered
     * @return array
     * @link https://book.cakephp.org/3.0/en/core-libraries/app.html#finding-paths-to-namespaces
     */
    protected function _getPathsByType(string $type, bool $plugins = true): array
    {
        $paths = App::core($type);

        if ($plugins === true) {
            foreach (Plugin::loaded() as $plugin) {
                $path = Plugin::classPath($plugin) . $type;
                if (is_dir($path) === true) {
                    $paths[] = $path;
                }
            }
        }
        $paths = array_merge($paths, App::path($type));

        return $paths;
    }

    /**
     * Returns the paths (core, plugins and app) to all files of a given class type.
     *
     * @param string $type The type of class for which to find the paths (ie. 'Model/Table')
     * @param bool $plugins Whether or not classes in plugins should be considered
     * @return array
     */
    protected function _getFilesByType(string $type, bool $plugins = true): array
    {
        $return = [];

        foreach ($this->_getPathsByType($type, $plugins) as $path) {
            $return = array_merge($return, glob(rtrim($path, DS) . DS . '*.php'));
        }
        sort($return);

        return $return;
    }

    /**
     * Returns the table names from the table classes.
     *
     * @param array $ignored A list of table names to be ignored
     * @return array
     */
    public function getTables(array $ignored = []): array
    {
        $result = [];

        $separator = preg_quote(DS, '/');
        $regexp = '/^.*' . $separator . '([^' . $separator . ']+)Table\.php$/';
        foreach ($this->_getFilesByType('Model/Table', false) as $path) {
            $tableName = preg_replace($regexp, '\1', $path);
            if ($tableName !== $path) {
                $result[] = Inflector::underscore($tableName);
            }
        }

        $result = array_unique(array_diff($result, $ignored));
        sort($result);

        return $result;
    }

    /**
     * Return an array representing the primary key constraint of a table.
     *
     * @param \Cake\ORM\Table $table The table for which to get the primary key constraint
     * @return array
     */
    protected function _getSchemaTablePrimaryKeyConstraint(Table $table)
    {
        $constraint = [
            'type' => 'primary',
            'name' => 'primary',
            'columns' => (array)$table->getPrimaryKey(),
            'length' => [],
        ];

        if ($this->params['ignore-constraint-names'] === true) {
            unset($constraint['name']);
        }

        return $constraint;
    }

    /**
     * Return an array in which the "0" key has the completed schema for all belongs to associations in the table as well
     * as a "1" key which hold the completed actions for all belongs to associations in the table.
     *
     * @param \Cake\ORM\Table $table The table for which to get the belongs to constraint
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @param array $schema An array containing the schema for each table in the database
     * @param array $actions An array containing the "dependent" boolean ("delete" and "update") for each table and each
     *  association constraint in the database
     * @return array
     */
    protected function _getSchemaBelongsToAndActions(Table $table, array $ignored, array $schema, array $actions): array
    {
        foreach ($table->associations()->getByType(['belongsTo']) as $association) {
            try {
                if (in_array($association->getTable(), $ignored['tables']) === false) {
                    $columns = (array)$association->getForeignKey();
                    if ($columns !== [false]) {
                        $name = sprintf('%s_%s_fkey', $table->getTable(), implode('_', $columns));
                        $references = [
                            $association->getTable(),
                            $association->getBindingKey(),
                        ];

                        $constraint = [
                            'type' => 'foreign',
                            'name' => $name,
                            'columns' => $columns,
                            'references' => $references,
                            'length' => [],
                        ];

                        if ($this->params['ignore-constraint-names'] === true) {
                            unset($constraint['name']);
                        }

                        $key = $this->SchemaUtilities->getForeignArrayKey(
                            $table->getTable(),
                            $constraint['columns'], $constraint['references']
                        );

                        if (
                            in_array($constraint['name'], $ignored['constraints']) === false
                            && isset($schema[$table->getTable()]['constraints'][$key]) === false
                        ) {
                            $schema[$table->getTable()]['constraints'][$key] = $constraint;
                        }
                    }
                }
            } catch (Exception $exc) {
                $this->_exceptions[] = $exc->getMessage();
            }
        }

        return [$schema, $actions];
    }

    /**
     * Return an array in which the "0" key has the completed schema for all belongs to many associations in the table as
     * well as a "1" key which hold the completed actions for all belongs to many associations in the table.
     *
     * @param \Cake\ORM\Table $table The table for which to get the belongs to many constraint
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @param array $schema An array containing the schema for each table in the database
     * @param array $actions An array containing the "dependent" boolean ("delete" and "update") for each table and each
     *  association constraint in the database
     * @return array
     */
    protected function _getSchemaBelongsToManyAndActions(Table $table, array $ignored, array $schema, array $actions): array
    {
        foreach ($table->associations()->getByType(['belongsToMany']) as $association) {
            try {
                if (in_array($association->junction()->getTable(), $ignored['tables']) === false) {
                    $columns = (array)$association->getForeignKey();
                    if ($columns !== [false]) {
                        $name = sprintf(
                            '%s_%s_fkey',
                            $association->junction()->getTable(), implode('_', $columns)
                        );
                        $references = [
                            $table->getTable(),
                            $association->getBindingKey(),
                        ];

                        $constraint = [
                            'type' => 'foreign',
                            'name' => $name,
                            'columns' => $columns,
                            'references' => $references,
                            'length' => [],
                        ];

                        if ($this->params['ignore-constraint-names'] === true) {
                            unset($constraint['name']);
                        }

                        $key = $this->SchemaUtilities->getForeignArrayKey(
                            $association->junction()->getTable(),
                            $constraint['columns'], $constraint['references']
                        );

                        $dependent = $association->getDependent() ? 'cascade' : 'noAction';
                        $actions[$association->junction()->getTable()][$key] = $actions[$table->getTable()][$key] = $dependent;

                        if (in_array($constraint['name'], $ignored['constraints']) === false) {
                            $schema[$association->junction()->getTable()]['constraints'][$key] = $constraint;
                        }
                    }
                }
            } catch (Exception $exc) {
                $this->_exceptions[] = $exc->getMessage();
            }
        }

        return [$schema, $actions];
    }

    /**
     * Return an array in which the "0" key has the completed schema for all has many or has one associations in the table
     * as well as a "1" key which hold the completed actions for all has many or has one associations in the table.
     *
     * @param string $type The type of associations to consider ("hasMany" or "hasOne")
     * @param \Cake\ORM\Table $table The table for which to get the has many or has one constraint
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @param array $schema An array containing the schema for each table in the database
     * @param array $actions An array containing the "dependent" boolean ("delete" and "update") for each table and each
     *  association constraint in the database
     * @return array
     */
    protected function _getSchemaHasAndActions(string $type, Table $table, array $ignored, array $schema, array $actions): array
    {
        foreach ($table->associations()->getByType([$type]) as $association) {
            try {
                if (in_array($association->getTable(), $ignored['tables']) === false) {
                    $columns = (array)$association->getForeignKey();
                    if ($columns !== [false]) {
                        $name = sprintf('%s_%s_fkey', $association->getTable(), implode('_', $columns));
                        $references = [
                            $table->getTable(),
                            $association->getBindingKey(),
                        ];

                        $constraint = [
                            'type' => 'foreign',
                            'name' => $name,
                            'columns' => $columns,
                            'references' => $references,
                            'length' => [],
                        ];

                        if ($this->params['ignore-constraint-names'] === true) {
                            unset($constraint['name']);
                        }

                        $key = $this->SchemaUtilities->getForeignArrayKey(
                            $association->getTable(),
                            $constraint['columns'],
                            $constraint['references']
                        );

                        $dependent = $association->getDependent() ? 'cascade' : 'noAction';
                        if (isset($actions[$table->getTable()][$key]) === false) {
                            $actions[$table->getTable()][$key] = $dependent;
                        }
                        if (isset($actions[$association->getTable()][$key]) === false) {
                            $actions[$association->getTable()][$key] = $dependent;
                        }

                        if (
                            in_array($constraint['name'], $ignored['constraints']) === false 
                            && isset($schema[$table->getTable()]['constraints'][$key]) === false
                        ) {
                            $schema[$table->getTable()]['constraints'][$key] = $constraint;
                        }
                    }
                }
            } catch (Exception $exc) {
                $this->_exceptions[] = $exc->getMessage();
            }
        }

        return [$schema, $actions];
    }

    /**
     * Return an array in which the "0" key has the completed schema for all has many associations in the table as well
     * as a "1" key which hold the completed actions for all has many associations in the table.
     *
     * @param \Cake\ORM\Table $table The table for which to get the has many constraint
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @param array $schema An array containing the schema for each table in the database
     * @param array $actions An array containing the "dependent" boolean ("delete" and "update") for each table and each
     *  association constraint in the database
     * @return array
     */
    protected function _getSchemaHasManyAndActions(Table $table, array $ignored, array $schema, array $actions): array
    {
        return $this->_getSchemaHasAndActions('hasMany', $table, $ignored, $schema, $actions);
    }

    /**
     * Return an array in which the "0" key has the completed schema for all has one associations in the table as well
     * as a "1" key which hold the completed actions for all has one associations in the table.
     *
     * @param \Cake\ORM\Table $table The table for which to get the has one constraint
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @param array $schema An array containing the schema for each table in the database
     * @param array $actions An array containing the "dependent" boolean ("delete" and "update") for each table and each
     *  association constraint in the database
     * @return array
     */
    protected function _getSchemaHasOneAndActions(Table $table, array $ignored, array $schema, array $actions): array
    {
        return $this->_getSchemaHasAndActions('hasOne', $table, $ignored, $schema, $actions);
    }

    /**
     * Returns the schema, with associations constraints completed by the "delete" and "update" keys if the shell
     * parameter "ignore-foreign-key-actions" is not true.
     *
     * @param array $schema An array containing the schema for each table in the database
     * @param array $actions An array containing the "dependent" boolean ("delete" and "update") for each table and each
     *  association constraint in the database
     * @return array
     */
    protected function _setSchemaForeignKeyActions(array $schema, array $actions): array
    {
        if ($this->params['ignore-foreign-key-actions'] !== true) {
            foreach ($actions as $tableName => $values) {
                foreach ($values as $name => $action) {
                    if (
                        isset($schema[$tableName]['constraints'][$name]) === true 
                        && (
                            isset($schema[$tableName]['constraints'][$name]['delete']) === false 
                            || $schema[$tableName]['constraints'][$name]['delete'] !== 'cascade'
                        )
                    ) {
                        $schema[$tableName]['constraints'][$name]['delete'] = $schema[$tableName]['constraints'][$name]['update'] = $action;
                    }
                }
            }
        }

        return $schema;
    }

    /**
     * Returns the list of table classes and the foreign keys deduced from their associations.
     *
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @return array
     */
    public function getSchema(array $ignored = []): array
    {
        $ignored += ['constraints' => [], 'tables' => []];
        $schema = [];
        $actions = [];

        foreach ($this->getTables($ignored['tables']) as $tableName) {
            $table = TableRegistry::getTableLocator()->get($tableName);

            $primary = $this->_getSchemaTablePrimaryKeyConstraint($table);
            if (in_array($primary['name'], $ignored['constraints']) === false) {
                $key = $this->SchemaUtilities->getPrimaryArrayKey($tableName, $primary['columns']);
                $schema[$tableName]['constraints'][$key] = $primary;
            }

            [$schema, $actions] = $this->_getSchemaBelongsToAndActions($table, $ignored, $schema, $actions);
            [$schema, $actions] = $this->_getSchemaBelongsToManyAndActions($table, $ignored, $schema, $actions);
            [$schema, $actions] = $this->_getSchemaHasManyAndActions($table, $ignored, $schema, $actions);
            [$schema, $actions] = $this->_getSchemaHasOneAndActions($table, $ignored, $schema, $actions);
        }

        return $this->_setSchemaForeignKeyActions($schema, $actions);
    }

    /**
     * Returns the constraints (unique and foreign keys) for each of the table classes.
     *
     * @param array $ignored An array with the keys "constraints" and "tables" containing the constraint or table names
     *  to be ignored
     * @return array
     */
    public function getRules(array $ignored = []): array
    {
        $ignored += ['constraints' => [], 'tables' => []];
        $return = [];

        foreach ($this->getTables($ignored['tables']) as $tableName) {
            $table = TableRegistry::getTableLocator()->get($tableName);
            $return[$tableName] = ['constraints' => $this->_getTableConstraints($table, $ignored['constraints'])];
        }

        return $return;
    }

    /**
     * Extracts the constraints (validateUnique, existsIn and isUnique) from a table object and formats them like in
     * the fixtures.
     *
     * @param \Cake\ORM\Table $table The table object from which to extract the constraints
     * @param array $ignored An array containing the constraint names to be ignored
     * @return array
     */
    protected function _getTableConstraints(Table $table, array $ignored = []): array
    {
        $return = [];

        $primary = [
            'name' => 'primary',
            'type' => 'primary',
            'columns' => (array)$table->getPrimaryKey(),
            'length' => [],
        ];

        if (in_array($primary['name'], $ignored) === false) {
            if ($this->params['ignore-constraint-names'] === true) {
                unset($primary['name']);
            }
            $key = $this->SchemaUtilities->getPrimaryArrayKey($table->getTable(), $primary['columns']);
            $return[$key] = $primary;
        }

        $prefix = chr(0) . '*' . chr(0);

        foreach ($table->getValidator() as $fieldName => $set) {
            foreach (((array)$set)["{$prefix}_rules"] as $rule) {
                if (((array)$rule)["{$prefix}_rule"] === 'validateUnique') {
                    $columns = [$fieldName];
                    $name = sprintf('%s_%s', $table->getTable(), implode('_', $columns));
                    if (in_array($name, $ignored) === false) {
                        $key = $this->SchemaUtilities->getUniqueArrayKey($table->getTable(), $columns);
                        sort($columns);
                        $return[$key] = [
                            'name' => $name,
                            'type' => 'unique',
                            'columns' => $columns,
                            'length' => [],
                        ];

                        if ($this->params['ignore-constraint-names'] === true) {
                            unset($return[$key]['name']);
                        }
                    }
                }
            }
        }

        $buildRules = array_filter((array)$table->buildRules(new RulesChecker()));
        unset($buildRules["{$prefix}_useI18n"]);

        if (empty($buildRules) === false) {
            foreach ($buildRules as $rules) {
                foreach ($rules as $rule) {
                    $rule = ((array)$rule)["{$prefix}rule"];
                    if (is_a($rule, 'Cake\ORM\Rule\ExistsIn') === true) {
                        $columns = ((array)$rule)["{$prefix}_fields"];
                        $repository = ((array)$rule)["{$prefix}_repository"];
                        $name = sprintf('%s_%s_fkey', $table->getTable(), implode('_', $columns));
                        if (in_array($name, $ignored) === false) {
                            $references = [
                                $table->{$repository}->getTable(),
                                $table->{$repository}->getPrimaryKey(),
                            ];
                            $key = $this->SchemaUtilities->getForeignArrayKey($table->getTable(), $columns, $references);

                            $return[$key] = [
                                'type' => 'foreign',
                                'name' => $name,
                                'columns' => $columns,
                                'references' => $references,
                                'length' => [],
                            ];

                            if ($this->params['ignore-constraint-names'] === true) {
                                unset($return[$key]['name']);
                            }
                        }
                    } elseif (is_a($rule, 'Cake\ORM\Rule\IsUnique') === true) {
                        $columns = ((array)$rule)["{$prefix}_fields"];
                        $name = sprintf('%s_%s', $table->getTable(), implode('_', $columns));
                        if (in_array($name, $ignored) === false) {
                            $key = $this->SchemaUtilities->getUniqueArrayKey($table->getTable(), $columns);
                            sort($columns);
                            $return[$key] = [
                                'type' => 'unique',
                                'name' => $name,
                                'columns' => $columns,
                                'length' => [],
                            ];

                            if ($this->params['ignore-constraint-names'] === true) {
                                unset($return[$key]['name']);
                            }
                        }
                    }
                }
            }
        }

        return $return;
    }
}
