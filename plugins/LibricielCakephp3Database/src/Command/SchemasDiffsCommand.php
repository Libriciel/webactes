<?php
declare(strict_types=1);

namespace LibricielCakephp3Database\Command;

use Cake\Console\Command;
use Cake\Console\ConsoleOptionParser;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;

// @codingStandardsIgnoreFile
/**
 * Class SchemasDiffsShell
 *
 * @package LibricielCakephp3Database\Command
 */
class SchemasDiffsCommand extends Command
{
    /**
     * Tasks used by this shell.
     *
     * @var array
     */
    public $tasks = [
        'LibricielCakephp3Database.SchemaClasses',
        'LibricielCakephp3Database.SchemaDatabase',
        'LibricielCakephp3Database.SchemaFixtures',
        'LibricielCakephp3Database.SchemaReport',
        'LibricielCakephp3Database.SchemaReportScreen',
        'LibricielCakephp3Database.SchemaUtilities',
    ];

    /**
     * The list of exceptions raised when trying to access classes, tables or fixtures.
     *
     * @var array
     */
    protected $_exceptions = [];

    /**
     * The list of ignored tables.
     *
     * @var array
     */
    protected $_ignored = [
        'constraints' => [],
        'tables' => [],
    ];

    /**
     * Startup the shell, populates the $_ignored attributes with the --ignore-constraints and --ignore-tables flags.
     *
     * @return void
     */
    public function startup()
    {
        parent::startup();

        foreach (['constraints', 'tables'] as $suffix) {
            $ignore = Hash::get($this->params, "ignore-{$suffix}");
            if (empty($ignore) === false) {
                $ignored = array_filter(explode(',', $ignore));
                foreach ($ignored as $value) {
                    $this->_ignored[$suffix][] = Inflector::underscore(trim($value));
                }
            }
        }
    }

    /**
     * Prints a summary of caught exceptions.
     *
     * @return void
     */
    protected function _printExceptionsSummary()
    {
        sort($this->_exceptions);
        $this->_exceptions = array_unique($this->_exceptions);

        if (empty($this->_exceptions) === false) {
            $count = count($this->_exceptions);
            $msgstr = __n('%d exception was raised:', '%d exceptions were raised:', $count);
            $this->err(sprintf($msgstr, $count));
            foreach ($this->_exceptions as $exception) {
                $this->err(sprintf("\t- %s", $exception));
            }
        } else {
            $this->success('No exception was raised');
        }
        $this->out();
    }

    /**
     * Checks for missing table classes or missing table associations with respect to the database schema and prints a
     * summary.
     *
     * @return void
     */
    public function classes()
    {
        $databaseSchema = $this->SchemaDatabase->get($this->_ignored);
        $tableClassesSchema = $this->SchemaClasses->getSchema($this->_ignored);

        foreach ($databaseSchema as $tableName => $schema) {
            if (isset($schema['constraints'])) {
                foreach ($schema['constraints'] as $name => $constraint) {
                    if ($constraint['type'] === 'unique') {
                        unset($databaseSchema[$tableName]['constraints'][$name]);
                    }
                }
            }
        }

        $report = $this->SchemaReport->getReport(
            $databaseSchema,
            'database',
            $tableClassesSchema,
            'classes'
        );

        $this->SchemaReportScreen->generate($report, 'database', 'table');

        $this->_stop($report['_errors'] + count($this->_exceptions) === 0 ? self::CODE_SUCCESS : self::CODE_ERROR);
    }

    /**
     * Checks for differences between the fixture classes and the database schema and outputs a summary.
     *
     * @return void
     */
    public function fixtures()
    {
        $report = $this->SchemaReport->getReport(
            $this->SchemaDatabase->get($this->_ignored),
            'database',
            $this->SchemaFixtures->get($this->_ignored),
            'fixtures'
        );

        $this->SchemaReportScreen->generate($report, 'database', 'fixtures');

        $this->_stop($report['_errors'] + count($this->_exceptions) === 0 ? self::CODE_SUCCESS : self::CODE_ERROR);
    }

    /**
     * Compare the validation rules (validateUnique, existsIn and isUnique) from the table classes with the production
     * database tables constraints and prints a summary.
     *
     * @return void
     */
    public function rules()
    {
        $this->params['ignore-constraint-names'] = true;

        $databaseRules = $this->SchemaDatabase->getRules($this->_ignored);
        $classesRules = $this->SchemaClasses->getRules($this->_ignored);

        $report = $this->SchemaReport->getReport(
            $databaseRules,
            'database',
            $classesRules,
            'classes'
        );

        $this->SchemaReportScreen->generate($report, 'database', 'classes');

        $this->_stop($report['_errors'] + count($this->_exceptions) === 0 ? self::CODE_SUCCESS : self::CODE_ERROR);
    }

    /**
     * Complete the option parser for this shell.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();

        $parser->setDescription(__('Compare the database schema with table classes associations or fixtures classes schemas and outputs the differences'));

        $parser->addSubcommands(
            [
                'classes' => [
                    'help' => __('Compare the associations in table classes with the foreign keys defined in the production database and outputs the differences'),
                ],
                'fixtures' => [
                    'help' => __('Compare the fields, indexes and constraints from the fixture classes with the production database tables and outputs the differences'),
                ],
                'rules' => [
                    'help' => __('Compare the validation rules (validateUnique, existsIn and isUnique) from the table classes with the production database tables constraints and outputs the differences'),
                ],
            ]
        );

        $parser->addOptions(
            [
                'connection' => [
                    'help' => 'The connection name for the production database',
                    'short' => 'c',
                    'default' => 'default',
                ],
                'ignore-foreign-key-actions' => [
                    'help' => 'When comparing table classes associations and database foreign keys, don\'t compare actions (cascade, noAction, ...)',
                    'boolean' => true,
                ],
                'ignore-constraints' => [
                    'help' => 'A comma-separated list of constraint names that should be ignored',
                    'short' => 'i',
                ],
                'ignore-constraint-names' => [
                    'help' => 'Ignore constraint (foreign key, index or unique) names when comparing (classes and fixtures command only, implicit for the rules command)',
                    'boolean' => true,
                ],
                'ignore-tables' => [
                    'help' => 'A comma-separated list of tables that should be ignored',
                    'short' => 'i',
                ],
            ]
        );

        return $parser;
    }
}
