<?php
declare(strict_types=1);
// @codingStandardsIgnoreFile

namespace LibricielCakephp3Database\Model\Behavior;

use ArrayObject;
use Cake\Cache\Cache;
use Cake\Event\EventInterface;
use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Exception;

/**
 * The class VirtualFieldsBehavior adds virtual field detection and replacement in the "select" part of a find query
 * before it is issued.
 *
 * Virtual fields come from public methods from a table and it's associated behaviors.
 * Method names should follow the "get<camelcased virtual field name>Subquery" pattern.
 *
 * Example virtual field method in a behavior class.
 *
 * ```php
 * class VirtualFieldsBehaviorExampleBehavior extends \Cake\ORM\Behavior
 * {
 *      public function getLastCommentIdSubquery()
 *      {
 *          $query = $this->_table->Comments->find();
 *
 *          $query
 *              ->select('id')
 *              ->order(['id' => 'desc'])
 *              ->limit(1);
 *
 *          return $query;
 *      }
 *  }
 * ```
 *
 * Example table class with a virtual field method using the AssociatedTablesBehavior, (the above) VirtualFieldsBehaviorExample
 * and VirtualFieldsBehavior classes.
 *
 * ```php
 * class VirtualFieldsBehaviorArticlesTable extends Table
 * {
 *      public function initialize(array $config)
 *      {
 *          parent::initialize($config);
 *
 *          //...
 *
 *          $this->addBehavior('AssociatedTables');
 *          $this->addBehavior('VirtualFieldsBehaviorExample');
 *          $this->addBehavior('VirtualFields');
 *      }
 *
 *      public function getPublishedCommentsCountSubquery()
 *      {
 *          $query = $this->Comments
 *              ->find()
 *              ->select(['count' => 'count(Comments.id)'])
 *              ->where(
 *                  $this->getAssociationFullConditions('Comments')
 *                  + ['Comments.published' => 'Y']
 *              );
 *
 *          return $query;
 *      }
 * }
 * ```
 *
 * By default, all virtual fields are automatically added when no field or the table instance are selected in a query:
 * they are the "auto" fields.
 * To add or remove
 * If you manually specify the fields for a query, virtual fields will be added regardless they should be automatically
 * added or not.
 *
 * IMPORTANT: This class should be loaded last, after all behaviors, so the virtual fields methods can all be gathered at
 * initialization.
 *
 * @package LibricielCakephp3Database\Model\Behavior
 */
class VirtualFieldsBehavior extends Behavior
{
    /**
     * The regular expression for virtual fields method names.
     */
    public const SUBQUERY_METHOD_REGEX = '/get([A-Z].*)Subquery$/';

    /**
     * The list of detected virtual fields, by table alias.
     *
     * @var array
     */
    protected $_virtual = [];

    /**
     * The list of current automatically added virtual fields, by table alias.
     *
     * @var array
     */
    protected $_auto = [];

    /**
     * Returns the cache key for a given method and the current config.
     *
     * @param string $method The namespaced method name
     * @return string
     */
    protected function cacheKey($method)
    {
        $hash = hash('sha256', serialize(Hash::filter($this->getConfig())));

        return str_replace(['\\', '::'], ['__', '__'], $method) . '__' . $this->_table->getTable() . '__' . $hash;
    }

    /**
     * Returns the list of all virtual fields for the attached table.
     *
     * Virtual fields come from the methods of the table class or it's attached components whose names follow the
     * "get<camelcased virtual field name>Subquery" pattern.
     *
     * @param bool $force True to skip reading from the cache
     * @return array
     * @throws \ReflectionException
     */
    public function getAllVirtualFields($force = false)
    {
        $cacheKey = $this->cacheKey(__METHOD__);
        $virtual = Cache::read($cacheKey);

        if (empty($virtual) || $force === true) {
            $virtual = [];

            $methods = get_class_methods($this->_table);
            sort($methods);
            foreach (get_class_methods($this->_table) as $method) {
                $matches = [];
                if (preg_match(self::SUBQUERY_METHOD_REGEX, $method, $matches) === 1) {
                    $virtual[] = Inflector::underscore($matches[1]);
                }
            }

            foreach ($this->_table->behaviors()->loaded() as $name) {
                foreach ($this->_table->getBehavior($name)->implementedMethods() as $method) {
                    $matches = [];
                    if (preg_match(self::SUBQUERY_METHOD_REGEX, $method, $matches) === 1) {
                        $virtual[] = Inflector::underscore($matches[1]);
                    }
                }
            }

            $virtual = array_unique($virtual);
            Cache::write($cacheKey, $virtual);
        }

        return $virtual;
    }

    /**
     * Initalize the behavior, detecting all virtual field methods in the table class or its behaviors and populating the
     * virtual and auto lists with all of them.
     *
     * @param array $config The configuration settings provided to this behavior.
     * @return void
     * @throws \ReflectionException
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $virtual = $this->getAllVirtualFields();
        $this->virtual[$this->_table->getAlias()] = $virtual;
        $this->auto[$this->_table->getAlias()] = $virtual;
    }

    /**
     * Sets the auto fields list.
     *
     * @param array $fields The fields to be set as auto virtual fields.
     * @return void
     */
    public function setAuto(array $fields)
    {
        $this->auto[$this->_table->getAlias()] = array_values(array_intersect($this->virtual[$this->_table->getAlias()], $fields));
    }

    /**
     * Gets the auto fields list.
     *
     * @return array
     */
    public function getAuto()
    {
        return $this->auto[$this->_table->getAlias()];
    }

    /**
     * Gets the list of all virtual fields defined for the current table.
     *
     * @return array
     */
    public function getVirtual()
    {
        return $this->virtual[$this->_table->getAlias()];
    }

    /**
     * Disables the given fields as auto fields.
     *
     * If the field list is true, no virtual fields will be "auto" fields.
     * If the field list is false, all virtual field will be "auto" fields.
     * If the field list contains one or more field, they will be removed from the "auto" fields (if they are virtual fields).
     *
     * @param true|false|string|array $fields The auto fields to disable
     * @return void
     */
    public function disableAuto($fields)
    {
        if ($fields === false) {
            $fields = $this->getVirtual();
        } elseif ($fields === true) {
            $fields = [];
        } else {
            $fields = array_diff(
                $this->getAuto(),
                (array)$fields
            );
        }

        $this->setAuto(array_values(array_unique($fields)));
    }

    /**
     * Enables the given fields as auto fields.
     *
     * If the field list is true, all virtual fields will be "auto" fields.
     * If the field list is false, no virtual field will be "auto" fields.
     * If the field list contains one or more field, they will be added to the "auto" fields (if they are virtual fields).
     *
     * @param true|false|string|array $fields The auto fields to enable
     * @return void
     */
    public function enableAuto($fields)
    {
        if ($fields === false) {
            $fields = [];
        } elseif ($fields === true) {
            $fields = $this->getVirtual();
        } else {
            $fields = array_intersect(
                $this->getAuto(),
                (array)$fields
            );
        }

        $this->setAuto(array_values(array_unique($fields)));
    }

    /**
     * Changes the find query, replacing the virtual fields when needed.
     *
     * By default, all virtual fields are automatically added when no field or the table instance are selected in a query:
     * they are the "auto" fields.
     * If you manually specify the fields for a query, virtual fields will be added regardless they should be automatically
     * added or not.
     *
     * @param \Cake\Event\Event $event The beforeFind event that was fired.
     * @param \Cake\ORM\Query $query Query
     * @param \ArrayObject $options The options for the query
     * @param bool $primary Indicates whether or not this is the root query, or an associated query
     * @return void
     * @throws \Exception WHen the method finally does not exist or is not accessible in the table class or its behaviors
     */
    public function beforeFind(EventInterface $event, Query $query, ArrayObject $options, $primary)
    {
        $selectedFields = array_map(
            function ($fieldname) {
                $aliased = str_replace('__', '.', $fieldname);

                return str_replace($this->_table->getAlias() . '.', '', $aliased);
            },
            array_keys(Hash::normalize($query->clause('select')))
        );

        $allFields = $query->aliasFields($this->_table->getSchema()->columns(), $this->_table->getAlias());

        if (empty($selectedFields) === true) {
            $query->enableAutoFields();
            $auto = $this->auto[$this->_table->getAlias()];
        } elseif ($selectedFields === $this->_table->getSchema()->columns()) {
            $auto = $this->auto[$this->_table->getAlias()];
            $selectedFields = array_merge($selectedFields, $auto);
        } else {
            $auto = array_intersect(
                array_merge(
                    $this->virtual[$this->_table->getAlias()],
                    $query->aliasFields($this->virtual[$this->_table->getAlias()], $this->_table->getAlias())
                ),
                $selectedFields
            );
        }

        foreach ($this->virtual[$this->_table->getAlias()] as $fieldname) {
            $virtual = empty($selectedFields) === true
                || in_array($fieldname, $selectedFields, true)
                || in_array($this->_table->getAlias() . '.' . $fieldname, $selectedFields, true)
                || array_intersect_assoc($selectedFields, $allFields) === $allFields;

            if ($virtual === true && in_array($fieldname, $auto, true) === true) {
                $candidate = Inflector::variable(sprintf('get_%s_subquery', $fieldname));
                if (method_exists($this->_table, $candidate) === true) {
                    $method = $candidate;
                } elseif ($this->_table->behaviors()->hasMethod($candidate) === true) {
                    $method = $candidate;
                }

                if ($method === null) {
                    $msgid = 'Class %s or its behaviors does not have a %s or %s method for the %s virtual field';
                    throw new Exception(sprintf(
                        $msgid,
                        get_class($this->_table),
                        Inflector::variable(sprintf('get_%s_subquery', $fieldname)),
                        $fieldname
                    ));
                }

                $alias = sprintf('%s__%s', $this->_table->getAlias(), $fieldname);
                $query->select([$alias => $this->_table->{$method}()]);
            }
        }
    }
}
