<?php
declare(strict_types=1);
// @codingStandardsIgnoreFile

namespace LibricielCakephp3Database\Model\Behavior;

use Cake\ORM\Behavior;
use Exception;

/**
 * The class AssociatedTablesBehavior provides utility function for getting table associations informations.
 *
 * @package LibricielCakephp3Database\Model\Behavior
 */
class AssociatedTablesBehavior extends Behavior
{
    /**
     * Returns the real list of "one to one" and "one to many" association aliases for the current table.
     * Association aliases are from the "hasOne", "hasMany" and the join model for the "belongsToMany" relations.
     *
     * @return array
     */
    public function getAllAssociatedTablesAliases()
    {
        $aliases = [];
        foreach ($this->_table->associations()->getByType(['belongsToMany', 'hasMany', 'hasOne']) as $association) {
            $type = $association->type();
            if ($type === 'oneToMany' || $type === 'oneToOne') {
                $aliases[] = $association->getName();
            } elseif ($type === 'manyToMany') {
                $aliases[] = $association->junction()->getAlias();
            }
        }

        return array_unique($aliases);
    }

    /**
     * Returns the association by it's alias, even for the join model of a the "belongsToMany" association.
     *
     * @param string $alias The alias of the needed asociation
     * @return \Cake\ORM\Association|null
     * @throws \Exception When the association is not found
     */
    public function getAssociationByAlias($alias)
    {
        $association = $this->_table->associations()->get($alias);
        if ($association === null) {
            foreach ($this->_table->associations()->getByType(['belongsToMany']) as $candidate) {
                if ($candidate->junction()->getAlias() === $alias) {
                    $association = $this->_table->associations()->get($candidate->junction()->getAlias());
                    break;
                }
            }
        }

        if ($association === null) {
            $msgid = 'Class %s does not have a %s association';
            throw new Exception(sprintf($msgid, get_class($this->_table), $alias));
        }

        return $association;
    }

    /**
     * Returns the real conditions (join conditions plus extra conditions) for an association, even for the join model
     * of a the "belongsToMany" association.
     *
     * @param string $alias The alias of the needed asociation
     * @return array
     * @throws \Exception When the association is not found, of type "belongsTo" or of type unknown
     */
    public function getAssociationFullConditions($alias)
    {
        $association = $this->getAssociationByAlias($alias);

        $conditions = [];
        $type = $association->type();
        switch ($type) {
            case 'manyToOne':
                $conditions[] = sprintf(
                    '%s.%s = %s.%s',
                    $this->_table->getAlias(),
                    $association->getForeignKey(),
                    $association->getAlias(),
                    $association->getBindingKey()
                );
                break;
            case 'oneToMany':
            case 'oneToOne':
                $conditions[] = sprintf(
                    '%s.%s = %s.%s',
                    $this->_table->getAlias(),
                    $association->getBindingKey(),
                    $association->getAlias(),
                    $association->getForeignKey()
                );
                break;
            case 'manyToMany':
                $msgid = 'Association %s of type %s in class %s cannot be joined';
                throw new Exception(sprintf($msgid, $association->getAlias(), $association->type(), get_class($this->_table)));
            default:
                $msgid = 'Association %s of type %s is unknown in class %s';
                throw new Exception(sprintf($msgid, $association->getAlias(), $association->type(), get_class($this->_table)));
        }

        $conditions = array_merge($conditions, $association->getConditions());

        return $conditions;
    }
}
