<?php
declare(strict_types=1);
// @codingStandardsIgnoreFile

namespace LibricielCakephp3Database\Model\Behavior;

use Cake\Event\EventInterface;
use Cake\ORM\RulesChecker;
use Cake\Utility\Inflector;
use UnexpectedValueException;

/**
 * The class GuardedAssociatedTablesBehavior brings update and delete protection for records having associated records as
 * well as the "is_deletable", "is_linked" and "is_updatable" virtual fields.
 *
 * Unguarded columns can be configured to allow updating them when there are associated records.
 * Unguarded tables can be configured to allow updating or deleting rows when there are associated records only in them.
 * By default, all tables and all fields are guarded.
 *
 * IMPORTANT: to get the virtual fields available as fields in a find query, this behavior should be added *BEFORE* the
 * VirtualFieldsBehavior.
 *
 * Example table class using the GuardedAssociatedTables and VirtualFieldsBehavior classes.
 *
 * ```php
 * class VirtualFieldsBehaviorArticlesTable extends Table
 * {
 *      public function initialize(array $config)
 *      {
 *          parent::initialize($config);
 *
 *          //...
 *
 *          $this->addBehavior('GuardedAssociatedTables');
 *          $this->addBehavior('VirtualFields');
 *      }
 * }
 * ```
 *
 * @package LibricielCakephp3Database\Model\Behavior
 */
class GuardedAssociatedTablesBehavior extends AssociatedTablesBehavior
{
    /**
     * Constant for the prefix config key
     */
    public const PREFIX = 'prefix';

    /**
     * Constant for the delete config key
     */
    public const DELETE = 'delete';

    /**
     * Constant for the update config key
     */
    public const UPDATE = 'update';

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'prefix' => 'has_',
        'delete' => [
            'guardedAliases' => null,
            'unguardedAliases' => null,
        ],
        'update' => [
            'guardedFields' => null,
            'unguardedFields' => null,
            'guardedAliases' => null,
            'unguardedAliases' => null,
        ],
        'implementedMethods' => [
            'getGuardedAliases' => 'getGuardedAliases',
            'getIsLinkedSubquery' => 'getIsLinkedSubquery',
            'getIsUpdatableSubquery' => 'getIsUpdatableSubquery',
            'getIsDeletableSubquery' => 'getIsDeletableSubquery',
        ],
    ];

    /**
     * Is guarding effective for application rules ?
     *
     * @var bool
     */
    protected $_enabled = true;

    /**
     * Disable guarding for application rules.
     *
     * @return void
     */
    public function disable()
    {
        $this->_enabled = false;
    }

    /**
     * Enable guarding for application rules.
     *
     * @return void
     */
    public function enable()
    {
        $this->_enabled = true;
    }

    /**
     * Is guarding effective for application rules ?
     *
     * @return bool
     */
    public function enabled()
    {
        return $this->_enabled === true;
    }

    /**
     * Returns a subquery that checks for the existence of linked records to the current table in a linked "one to many"
     * table.
     * Linked tables can be "hasMany" or "belongsToMany", in which case the linked table will be the join model.
     *
     * @param string $alias The alias of relation
     * @return \Cake\Database\Expression\QueryExpression
     */
    private function getLinkedTableVirtualFieldSubquery($alias)
    {
        $query = $this->_table->find();
        $conditions = $this->getAssociationFullConditions($alias);

        $subQuery = $this->_table->{$alias}
            //->setAlias(Inflector::underscore($alias)) // alias ?
            ->find()
            ->select($this->_table->{$alias}->getPrimaryKey())
            ->where($conditions);

        return $query->expr()->exists($subQuery);
    }

    /**
     * Returns a list of linked "one to many" tables that should be guarded for a given operation, with respect to the
     * "guardedAliases" and "unguardedAliases" behavior configuration keys.
     * Linked tables can be "hasMany" or "belongsToMany", in which case the linked table will be the join model.
     *
     * @param string $type The config prefix ("update" or "delete")
     * @return array
     */
    public function getGuardedAliases($type)
    {
        $accepted = [static::DELETE, static::UPDATE];
        if (in_array($type, $accepted, true) === false) {
            $msgid = 'Invalid parameter "%s" for %s; accepted values are: %s';
            $message = sprintf($msgid, $type, __METHOD__, '"' . implode('"", "', $accepted) . '"');
            throw new UnexpectedValueException($message);
        }

        $result = [];
        $aliases = $this->getAllAssociatedTablesAliases();
        $guarded = $this->getConfig("{$type}.guardedAliases");
        $unguarded = $this->getConfig("{$type}.unguardedAliases");

        foreach ($aliases as $alias) {
            $get = (
                ($unguarded === null || in_array($alias, $unguarded, true) === false)
                && ($guarded === null || in_array($alias, $guarded, true) === true)
            );

            if ($get === true) {
                $result[] = $alias;
            }
        }

        return array_unique($result);
    }

    /**
     * Returns a query expression that checks that a record can be updated with respect to the guarded tables specified
     * in the behavior configuration for the "update" operation ("update.guardedAliases" and "update.unguardedAliases").
     *
     * @return \Cake\Database\Expression\QueryExpression
     */
    public function getIsLinkedSubquery()
    {
        $query = $this->_table->find();

        $ors = [];
        foreach ($this->getAllAssociatedTablesAliases() as $alias) {
            $ors[] = $this->getLinkedTableVirtualFieldSubquery($alias);
        }

        return $query->expr()->or($ors);
    }

    /**
     * Returns a query expression that checks that a record can be updated with respect to the guarded tables specified
     * in the behavior configuration for the "update" operation ("update.guardedAliases" and "update.unguardedAliases").
     *
     * @return \Cake\Database\Expression\QueryExpression
     */
    public function getIsUpdatableSubquery()
    {
        $query = $this->_table->find();

        $ors = [];
        foreach ($this->getGuardedAliases(static::UPDATE) as $alias) {
            $ors[] = $this->getLinkedTableVirtualFieldSubquery($alias);
        }

        return $query->expr()->not($query->expr()->or($ors));
    }

    /**
     * Returns a query expression that checks that a record can be updated with respect to the guarded tables specified
     * in the behavior configuration for the "delete" operation ("update.guardedAliases" and "update.unguardedAliases").
     *
     * @return \Cake\Database\Expression\QueryExpression
     */
    public function getIsDeletableSubquery()
    {
        $query = $this->_table->find();

        $ors = [];
        foreach ($this->getGuardedAliases(static::DELETE) as $alias) {
            $ors[] = $this->getLinkedTableVirtualFieldSubquery($alias);
        }

        return $query->expr()->not($query->expr()->or($ors));
    }

    /**
     * Returns a string containing the unprefixed table aliases to be used in validation error messages.
     * Prefix is defined in the behavior configuration under the "prefix" key
     *
     * @param array $tables A list of virtual fields ("<prefix>"underscored table alias)
     * @return string
     */
    protected function getGuardedTableAliases(array $tables)
    {
        return implode(
            ', ',
            array_map(
                function ($value) {
                    $underscored = preg_replace(
                        sprintf('/^%s/', preg_quote($this->getConfig(static::PREFIX), '/')),
                        '',
                        $value
                    );

                    return Inflector::camelize($underscored);
                },
                array_keys($tables)
            )
        );
    }

    /**
     * Returns a list of fields that can be updated (unguarded fields) in the current table when there oare linked
     * records.
     * See the behavior's "update.guardedFields" and "update.unguardedFields" configuration keys.
     *
     * @return array
     */
    protected function getUnguardedFields()
    {
        $result = [];
        $columns = $this->_table->getSchema()->columns();
        $guarded = $this->getConfig('update.guardedFields');
        $unguarded = $this->getConfig('update.unguardedFields');

        if (($guarded === null && $unguarded === null) === false) {
            foreach ($columns as $column) {
                $get = (
                    ($unguarded === null || in_array($column, $unguarded, true) === true)
                    && ($guarded === null || in_array($column, $guarded, true) === false)
                );

                if ($get === true) {
                    $result[] = $column;
                }
            }
        }

        sort($result);

        return $result;
    }

    /**
     * Adds delete and update rules to the application rules for the guarded one to many tables linked to the current
     * table with respected to the behavior configuration.
     *
     * @param \Cake\Event\Event $event The calling event
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(EventInterface $event, RulesChecker $rules)
    {
        if ($this->_enabled === true) {
            $primaryKey = $this->_table->getPrimaryKey();

            $rules->addDelete(
                function ($entity) use ($primaryKey) {
                    $query = $this->_table
                        ->find()
                        ->where([$primaryKey => $entity->{$primaryKey}])
                        ->enableHydration(false);

                    foreach ($this->getGuardedAliases(static::DELETE) as $alias) {
                        $fieldName = $this->getConfig(static::PREFIX) . Inflector::underscore($alias);
                        $query->select([$fieldName => $this->getLinkedTableVirtualFieldSubquery($alias)]);
                    }

                    $result = $query->first();
                    $found = array_search(true, $result, true) !== false;
                    $tables = array_filter($result, function ($value) {
                        return $value === true;
                    });

                    $result = ($found === false);

                    if ($result === false) {
                        $result = sprintf(
                            'Cannot delete a %s with dependant records in %s',
                            $this->_table->getAlias(),
                            $this->getGuardedTableAliases($tables)
                        );
                    }

                    return $result;
                },
                '_linkedIn',
                [
                    'errorField' => $primaryKey,
                ]
            );

            $accepted = $this->getUnguardedFields();
            $rules->addUpdate(
                function ($entity) use ($accepted, $primaryKey) {
                    $dirty = array_diff($entity->getDirty(), $accepted);
                    $result = true;
                    if (empty($dirty) === false) {
                        $query = $this->_table
                            ->find()
                            ->where([$primaryKey => $entity->{$primaryKey}])
                            ->enableHydration(false);

                        foreach ($this->getGuardedAliases(static::UPDATE) as $alias) {
                            $fieldName = $this->getConfig(static::PREFIX) . Inflector::underscore($alias);
                            $query->select([$fieldName => $this->getLinkedTableVirtualFieldSubquery($alias)]);
                        }

                        $result = $query->first();
                        $found = array_search(true, $result, true) !== false;
                        $tables = array_filter($result, function ($value) {
                            return $value === true;
                        });

                        $result = ($found === false);
                        if ($result === false) {
                            if (empty($accepted) === true) {
                                $result = sprintf(
                                    'Cannot update a %s fields with dependant records in %s',
                                    $this->_table->getAlias(),
                                    $this->getGuardedTableAliases($tables)
                                );
                            } else {
                                $result = sprintf(
                                    'Cannot update a %s fields except %s with dependant records in %s',
                                    $this->_table->getAlias(),
                                    implode(', ', $accepted),
                                    $this->getGuardedTableAliases($tables)
                                );
                            }
                        }
                    }

                    return $result;
                },
                '_linkedIn',
                [
                    'errorField' => $primaryKey,
                ]
            );
        }

        return $rules;
    }
}
