<?php
declare(strict_types=1);

namespace WrapperFlowable\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\Http\Client;

// @codingStandardsIgnoreFile
/**
 * The class FlowableShell makes it possible to initialiaze a Flowable instance with a deployment or get the list of
 * deployments.
 *
 * @package WrapperFlowable\Shell
 */
class FlowableDeploymentsShell extends Shell
{
    /**
     * Returns the deployment URL with respect to the parameters and the configuration.
     *
     * @return string
     */
    protected function _url()
    {
        $format = '%s://%s/flowable-rest/service/repository/deployments';

        return sprintf($format, $this->param('scheme'), $this->param('host'));
    }

    /**
     * Returns the flowable auth with respect to the parameters and the configuration.
     *
     * @return array
     */
    protected function _auth()
    {
        return [
            'username' => $this->param('username'),
            'password' => $this->param('password'),
        ];
    }

    /**
     * Returns the common options with respect to the parameters and the configuration.
     *
     * @return array
     */
    protected function _options()
    {
        return ['auth' => $this->_auth(), 'type' => 'json'];
    }

    /**
     * Add a deployment to the flowable server with a given deployment key, name and file.
     *
     * @return void
     */
    public function init()
    {
        $handle = fopen($this->param('file'), 'r');
        if (empty($handle) === true) {
            $success = false;
            $this->err(sprintf('Could not read file %s', $this->param('file')));
        } else {
            $client = new Client();

            $url = $this->_url();
            $data = [
                'deploymentKey' => $this->param('key'),
                'deploymentName' => $this->param('name'),
                'file' => $handle,
            ];
            $options = $this->_options();

            $response = $client->post($url, $data, $options);

            if ($response->getStatusCode() === 201) {
                $success = true;
                $deployment = $response->getJson();
                $msgid = '<success>Successfully</success> created a deployment with id <info>%s</info> and name <info>%s</info> at <info>%s</info>';
                $this->out(sprintf($msgid, $deployment['id'], $deployment['name'], $deployment['deploymentTime']));
            } else {
                $success = false;
                $msgid = 'Got reponse code %d from %s (%s)';
                $this->err(sprintf($msgid, $response->getStatusCode(), $url, $response->getBody()->getContents()));
            }

            if (empty($handle) === false) {
                fclose($handle);
            }
        }

        $this->_stop($success ? static::CODE_SUCCESS : static::CODE_ERROR);
    }

    /**
     * Prints the list of deployments available on the flowable server.
     *
     * @return void
     */
    public function index()
    {
        $client = new Client();
        $url = sprintf('%s?sort=name', $this->_url());
        $options = $this->_options();

        $response = $client->get($url, [], $options);
        if ($response->getStatusCode() === 200) {
            $success = true;
            $deployments = $response->getJson();

            $data = [['Id', 'Name', 'Created']];
            foreach ($deployments['data'] as $deployment) {
                $data[] = [
                    $deployment['id'],
                    $deployment['name'],
                    $deployment['deploymentTime'],
                ];
            }

            $msgstr = sprintf(
                'Showing <info>%d</info> records out of <info>%d</info> total, starting on record <info>%d</info>, ending on <info>%d</info>',
                min($deployments['total'], $deployments['size']),
                $deployments['total'],
                $deployments['start'],
                min($deployments['start'] + $deployments['size'], $deployments['total'])
            );
            $this->out($msgstr);
            $this->_io->helper('Table')->output($data);
        } else {
            $success = false;
            $msgid = 'Got reponse code %d from %s (%s)';
            $this->err(sprintf($msgid, $response->getStatusCode(), $url, $response->getBody()->getContents()));
        }

        $this->_stop($success ? static::CODE_SUCCESS : static::CODE_ERROR);
    }

    /**
     * Complete the option parser for this shell.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();

        $parser->setDescription(__('This shell makes it possible to initialiaze a Flowable instance with a deployment or get the list of deployments'));

        $parser->addSubcommands(
            [
                'init' => [
                    'help' => __('Add a deployment to the flowable server with a given deployment key, name and file'),
                ],
                'index' => [
                    'help' => __('Prints the list of deployments available on the flowable server.'),
                ],
            ]
        );

        $parser->addOptions(
            [
                'file' => [
                    'help' => 'The path to the file used for the initial deployment',
                    'short' => 'f',
                    'default' => ROOT . DS . 'docker-ressources/flowable/webActes_validation_simple.bpmn20.xml',
                ],
                'host' => [
                    'help' => 'The host name for the flowable service',
                    'short' => 'h',
                    'default' => 'flowable',
                ],
                'key' => [
                    'help' => 'The flowable deployment key',
                    'short' => 'k',
                    'default' => 'validation_simple',
                ],
                'name' => [
                    'help' => 'The flowable deployment name',
                    'short' => 'n',
                    'default' => 'validationSimple',
                ],
                'password' => [
                    'help' => 'The password for the flowable service',
                    'short' => 'p',
                    'default' => Configure::read('Flowable.auth.password'),
                ],
                'port' => [
                    'help' => 'The port for the flowable service',
                    'short' => 'P',
                    'default' => 8080,
                ],
                'scheme' => [
                    'help' => 'The scheme for the flowable service',
                    'short' => 's',
                    'default' => 'http',
                    'choices' => ['http', 'https'],
                ],
                'username' => [
                    'help' => 'The user name for the flowable service',
                    'short' => 'u',
                    'default' => Configure::read('Flowable.auth.username'),
                ],
            ]
        );

        return $parser;
    }
}
