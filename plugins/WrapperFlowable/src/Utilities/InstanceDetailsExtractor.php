<?php
declare(strict_types=1);

namespace WrapperFlowable\Utilities;

use Cake\Utility\Hash;
use DateTime;
use WrapperFlowable\Api\FlowableException;
use WrapperFlowable\Api\FlowableFacade;
use WrapperFlowable\Api\WrapperFactory;
use WrapperFlowable\Model\Entity\InstanciableInterface;

/**
 * Class InstanceDetailsExtractor
 *
 * @package WrapperFlowable\Utilities
 */
abstract class InstanceDetailsExtractor
{
    /**
     * @param \WrapperFlowable\Model\Entity\InstanciableInterface $entity entity
     * @param int $userId id du user pour déterminer s'il peut agir
     * @return array|null
     * @throws \Exception
     */
    public static function getInstanceDetails(InstanciableInterface $entity, int $userId = -1): ?array
    {
        // @fixme : sortir de la facade
        $instanceId = $entity->getIdFlowableInstance();

        if ($instanceId === null) {
            return null;
        }
        $facade = new FlowableFacade();

        $response = $facade->getInstanceHistoryByBusinessKey($instanceId);
        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }
        $finishedStatus = $response->getJson()['data'][0]['endActivityId'];

        if ($finishedStatus === null) {
            $instance = self::getInstanceDetailsCircuitRunning($entity, $userId);
        } else {
            $instance = self::getInstanceDetailsCircuitOver($entity);
            $instance['isRejected'] = $finishedStatus === FlowableFacade::STATUS_REJECTED;
        }

        return $instance;
    }

    /**
     * @param \WrapperFlowable\Model\Entity\InstanciableInterface $entity entity
     * @param int $userId user
     * @return array
     * @throws \Exception
     */
    protected static function getInstanceDetailsCircuitRunning(InstanciableInterface $entity, int $userId = -1): array
    {
        // @fixme : sortir de la facade
        $instanceKey = $entity->getIdFlowableInstance();

        /** @var \WrapperFlowable\Utilities\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();
        $steps = $wrapper->getSteps($entity->getWorkflow()->getProcessDefinitionKey());
        $tasks = $wrapper->getTaskHistoryByBusinessId($instanceKey);

        $currentStepIndex = null;
        $canPerformAnAction = false;

        foreach ($steps as $stepIdx => &$step) {
            // recuperation de la tache ayant le même nom que le step
            $matchingTask = Hash::extract($tasks, '{n}[name=/^' . preg_quote($step['name'], '/') . '$/]');

            if (count($matchingTask)) {
                $task = $matchingTask[0];

                $validation = Hash::extract($task['variables'], '{n}[name=/^userId$/]');

                if ($validation !== []) { // si on a une variable 'userId', c'est du validé (passé)
                    $step['actedUponBy'] = $validation[0]['value'];
                    $step['state'] = Hash::extract($task['variables'], '{n}[name=/^action$/]')[0]['value'];
                    $step['comment'] = Hash::extract($task['variables'], '{n}[name=/^comment$/]')[0]['value'] ?? null;
                    $date = new DateTime($task['endTime']);
                    $step['actedTime'] = $date->format(DATE_ATOM);
                } else {
                    $step['state'] = FlowableFacade::STATUS_CURRENT;
                    $currentStepIndex = $stepIdx;
                    if (in_array($userId, $step['validators'])) {
                        $canPerformAnAction = true;
                    }
                }
                $step['actionId'] = $task['id'];
            } else {
                $step['state'] = FlowableFacade::STATUS_PENDING;
            }
        }

        $instance = [
            'id' => $entity->getIdFlowableInstance(),
            'isOver' => false,
            'isRejected' => false,
            'currentStepIndex' => $currentStepIndex,
            'steps' => $steps,
            'canPerformAnAction' => $canPerformAnAction,
        ];

        return $instance;
    }

    /**
     * @param \WrapperFlowable\Model\Entity\InstanciableInterface $entity entity
     * @return array
     * @throws \Exception
     */
    protected static function getInstanceDetailsCircuitOver(InstanciableInterface $entity): array
    {
        // @fixme : sortir de la facade
        $instanceKey = $entity->getIdFlowableInstance();

        /** @var \WrapperFlowable\Utilities\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();
        $steps = $wrapper->getSteps($entity->getWorkflow()->getProcessDefinitionKey());
        $tasks = $wrapper->getTaskHistoryByBusinessId($instanceKey);
        $skip = false;
        $history = [];

        foreach ($steps as $stepIdx => $step) {
            // recuperation de la tache ayant le même nom que le step

            $matchingTask = Hash::extract($tasks, '{n}[name=/^' . preg_quote($step['name'], '/') . '$/]');

            if (count($matchingTask)) {
                $task = $matchingTask[0];

                $validation = Hash::extract($task['variables'], '{n}[name=/^userId$/]');

                if ($validation !== []) { // si on a une variable 'userId', c'est du validé (passé)
                    $step['actedUponBy'] = $validation[0]['value'];
                    $step['state'] = Hash::extract($task['variables'], '{n}[name=/^action$/]')[0]['value'];
                    $step['comment'] = Hash::extract($task['variables'], '{n}[name=/^comment$/]')[0]['value'] ?? null;
                    $date = new DateTime($task['endTime']);
                    $step['actedTime'] = $date->format(DATE_ATOM);
                    $step['actionId'] = $task['id'];
                } else {
                    $skip = true;
                    $adminTask = Hash::extract($tasks, '{n}[name=/^' . FlowableFacade::ACTION_SPECIALE . '$/]')[0];// @codingStandardsIgnoreLine
                    $step['actedUponBy'] = Hash::extract($adminTask['variables'], '{n}[name=/^userId$/]')[0]['value'];// @codingStandardsIgnoreLine
                    $step['state'] = FlowableFacade::STATUS_APPROVED_ADMIN;
                    $step['comment'] = Hash::extract($adminTask['variables'], '{n}[name=/^comment$/]')[0]['value'] ?? null;// @codingStandardsIgnoreLine
                    $date = new DateTime($adminTask['endTime']);
                    $step['actedTime'] = $date->format(DATE_ATOM);
                    $step['actionId'] = $adminTask['id'];
                }
            } else {
                $step['state'] = $skip ? FlowableFacade::STATUS_SKIPPED : FlowableFacade::STATUS_PENDING;
            }
            $history[] = $step;
        }

        $instance = [
            'id' => $entity->getIdFlowableInstance(),
            'isOver' => true,
            'currentStepIndex' => null,
            'steps' => $history,
            'canPerformAnAction' => false,
        ];

        return $instance;
    }
}
