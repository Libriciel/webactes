<?php
declare(strict_types=1);

namespace WrapperFlowable\Utilities;

use App\Model\Behavior\LettercaseFormattableBehavior;
use Cake\Utility\Hash;
use Ramsey\Uuid\Uuid;
use SimpleXMLElement;
use WrapperFlowable\Api\FlowableFacade;

abstract class XmlWorkflowConverter
{
    public const VALIDATION_SIMPLE = 'webActes_validation_simple';

    /**
     * @param \SimpleXMLElement $xml from file
     * @return array
     */
    public static function convertXmlToJson(SimpleXMLElement $xml): array
    {
        $steps = [];

        foreach ($xml->process->callActivity as $callActivity) {
            $userIds = '';
            foreach ($callActivity->extensionElements->children('flowable') as $in) {
                if (strval($in->attributes()['target']) === 'users_candidate') {
                    $userIds = strval($in->attributes()['sourceExpression']);
                    break;
                }
            }

            $steps[] = [
                'name' => strval($callActivity->attributes()['name']),
                'validators' => $userIds === 'NO_USER'
                    ? []
                    : array_map(
                        function ($i) {
                            return (int)$i;
                        },
                        explode(',', $userIds)
                    ),
            ];
        }

        return $steps;
    }

    /**
     * Generate the xml for a circuit
     *
     * @param array $circuit array circuit
     * @return \SimpleXMLElement
     */
    public static function convertJsonToXml(array $circuit): SimpleXMLElement
    {
        $nbSteps = count($circuit['steps']);
        $intForId = range(1, $nbSteps);

        $xml_data = new SimpleXMLElement(
            '<?xml version="1.0" encoding="UTF-8"?><definitions
            xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:flowable="http://flowable.org/bpmn"
            xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
            xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC"
            xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI"></definitions>'
        );
        $xml_data->addAttribute('typeLanguage', 'http://www.w3.org/2001/XMLSchema');
        $xml_data->addAttribute('expressionLanguage', 'http://www.w3.org/1999/XPath');
        $xml_data->addAttribute('targetNamespace', 'http://flowable.org/test');

        self::addSignal($xml_data);

        self::addProcess($circuit, $xml_data);

        // Add node "startEvent"
        $datas = [
            'id' => 'startEvent',
        ];
        self::addChildNodeProcess('startEvent', $datas, $xml_data);

        // Add node 'endEvent'
        self::addEndEvent($xml_data);

        // Add node 'userTask'
        $datas = [
            'id' => 'task_admin',
            'name' => FlowableFacade::ACTION_SPECIALE,
            'flowable:candidateGroups' => '${admin_groups}',
        ];
        self::addChildNodeProcess('userTask', $datas, $xml_data);

        // Add node 'parallelGateway'
        $datas = [
            'id' => 'parallel_gateway_admin_or_tasks',
        ];
        self::addChildNodeProcess('parallelGateway', $datas, $xml_data);

        // Add node 'exclusiveGateway'
        $datas = [
            'id' => 'exclusive_gateway_admin',
        ];
        self::addChildNodeProcess('exclusiveGateway', $datas, $xml_data);

        // Add node 'callActivity'
        self::addCallActivity($intForId, $circuit['steps'], $xml_data);

        self::addExclusiveGatewayWithDefault($intForId, $xml_data);

        self::addBoundaryEvent($intForId, $xml_data);

        self::addIntermediateThrowEvent($intForId, $xml_data);

        self::addSequenceFlow($intForId, $xml_data);

        return $xml_data;
    }

    /**
     * Ajout des noeuds "signal" avec les attribues
     *
     * @param \SimpleXMLElement $xml_data xml_data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addSignal(&$xml_data)
    {
        $datas = [
            'id' => 'cancel_validations',
            'name' => 'Cancel validations',
            'flowable:scope' => 'processInstance',
        ];

        $signal = self::addNewChild($xml_data, 'signal');
        self::formatForAddAttributeToChild($signal, $datas);
    }

    /**
     * Ajout des noeuds "process" avec les attribues
     *
     * @param array $circuit circuit
     * @param \SimpleXMLElement $xml_data xml_data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addProcess(array $circuit, &$xml_data)
    {
        $datas = [
            'id' => 'process_' . Uuid::uuid4() . '-' . time(),
            'name' => $circuit['name'],
            'isExecutable' => 'true',
        ];

        $process = self::addNewChild($xml_data, 'process');
        self::formatForAddAttributeToChild($process, $datas);
    }

    /**
     * Ajout des noeuds "endEvent" avec les attribues et les sous noeuds
     *
     * @param \SimpleXMLElement $xml_data xml_data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addEndEvent(&$xml_data)
    {
        $datas = [
            [
                'id' => 'rejected',
                'name' => 'Rejet',
                'terminateEventDefinition' => [
                    'flowable:terminateAll' => 'true',
                ],
            ],
            [
                'id' => 'approved',
                'name' => 'Validation',
                'terminateEventDefinition' => [],
            ],
            [
                'id' => 'cancelled',
                'name' => 'Cancel',
                'terminateEventDefinition' => [
                    'flowable:terminateAll' => 'true',
                ],
            ],
        ];

        foreach ($datas as $data) {
            self::addChildNodeProcess('endEvent', $data, $xml_data);
        }
    }

    /**
     * Ajout des noeuds enfant au noeud "process"
     *
     * @param string $nameNodeChild nom de la balise XML parent
     * @param array $data Tableau de valeur à ajouter en attribut à la balise parent
     * @param \SimpleXMLElement $xml_data xml_data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addChildNodeProcess(string $nameNodeChild, array $data, &$xml_data)
    {
        $addChild = $xml_data->process->addChild($nameNodeChild);
        self::formatForAddAttributeToChild($addChild, $data);
    }

    /**
     * Ajout des noeuds "callActivity" qui corresponde aux étapes ('steps')
     *
     * @param array $intForId tableau du nombre d'étapes du circuit
     * @param array $steps Etape de validation
     * @param \SimpleXMLElement $xml_data xml_data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addCallActivity(array $intForId, array $steps, &$xml_data)
    {
        foreach ($steps as $keyValue => $step) {
            $data = [
                'id' => 'task_validation_' . $intForId[$keyValue],
                'name' => $step['name'],
                'calledElement' => self::VALIDATION_SIMPLE,
                'flowable:calledElementType' => 'key',
                'flowable:inheritBusinessKey' => 'true',
                'flowable:inheritVariables' => 'true',
                'flowable:fallbackToDefaultTenant' => 'false',
                'extensionElements' => [
                    'flowable:in' => [
                        [
                            'sourceExpression' => $step['name'],
                            'target' => 'validation_name',
                        ],
                        [
                            'sourceExpression' => $step['users'],
                            'target' => 'users_candidate',
                        ],
                        [
                            'sourceExpression' => $step['groups'],
                            'target' => 'groups_candidate',
                        ],
                        [
                            'sourceExpression' => 'ADMIN_USER_ID',
                            'target' => 'admin_users',
                        ],
                        [
                            'sourceExpression' => 'ADMIN_GROUP_ID',
                            'target' => 'admin_groups',
                        ],
                        [
                            'sourceExpression' => '${business_id}',
                            'target' => 'business_id',
                        ],
                    ],
                    'flowable:out' => [
                        'sourceExpression' => '${action}',
                        'target' => 'action_' . $intForId[$keyValue] . '',
                    ],
                ],
            ];

            $callActivity = $xml_data->process->addChild('callActivity');

            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $extensionElements = self::addNewChild($callActivity, $key);

                    foreach ($value as $keyElements => $elements) {
                        if (self::multiArray($elements) === true) {
                            foreach ($elements as $element) {
                                $flowableIn = self::addNewChild($extensionElements, $keyElements);

                                self::formatForAddAttributeToChild($flowableIn, $element);
                            }
                        } else {
                            $flowableOut = self::addNewChild($extensionElements, $keyElements);

                            self::formatForAddAttributeToChild($flowableOut, $elements);
                        }
                    }
                } else {
                    self::addAttribute($callActivity, $key, $value);
                }
            }
        }
    }

    /**
     * Ajout des noeuds "exclusiveGateway" avec les attribues
     *
     * @param array $intForIds Tableau du nombre d'étapes du circuit
     * @param \SimpleXMLElement $xml_data xml_data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addExclusiveGatewayWithDefault($intForIds, &$xml_data)
    {
        foreach ($intForIds as $intForId) {
            $datas[] = [
                'id' => 'exclusive_gateway_' . $intForId,
                'default' => 'choice_approved_' . $intForId,
            ];
        }

        foreach ($datas as $data) {
            self::addChildNodeProcess('exclusiveGateway', $data, $xml_data);
        }
    }

    /**
     * Ajout des noeuds "sequenceFlow" avec les attribues et les sous noeuds
     *
     * @param array $intForIds tableau du nombre d'étapes du circuit
     * @param \SimpleXMLElement $xml_data xml_data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addSequenceFlow($intForIds, &$xml_data)
    {
        $nbSteps = count($intForIds);

        $datas = [];
        foreach ($intForIds as $intForId) {
            $datas[] = [
                'id' => 'task_validation_to_exclusive_gateway_' . $intForId,
                'sourceRef' => 'task_validation_' . $intForId,
                'targetRef' => 'exclusive_gateway_' . $intForId,
            ];

            $datas[] = [
                'id' => 'cancel_task_' . $intForId,
                'sourceRef' => 'catch_cancel_task_event_' . $intForId,
                'targetRef' => 'task_admin',
            ];

            $datas[] = [
                'id' => 'choice_goto_from_throw_cancel_' . $intForId,
                'sourceRef' => 'throw_cancel_task_event_' . $intForId,
                'targetRef' => 'task_validation_' . $intForId,
            ];

            $datas[] = [
                'id' => 'choice_rejected_' . $intForId,
                'sourceRef' => 'exclusive_gateway_' . $intForId,
                'targetRef' => 'rejected',
                'conditionExpression' => [
                    'xsi:type' => 'tFormalExpression',
                    '@value' => '${action_' . $intForId . ' == "reject"}',
                ],
            ];

            $datas[] = [
                'id' => 'choice_goto_from_admin_to_task_' . $intForId,
                'name' => 'Forcer',
                'sourceRef' => 'exclusive_gateway_admin',
                'targetRef' => 'throw_cancel_task_event_' . $intForId,
                'conditionExpression' => [
                    'xsi:type' => 'tFormalExpression',
                    '@value' => '${action == "to_step_' . $intForId . '"}',
                ],
            ];

            if ($intForId != $nbSteps) {
                $datas[] = [
                    'id' => 'choice_approved_' . $intForId,
                    'sourceRef' => 'exclusive_gateway_' . $intForId,
                    'targetRef' => 'task_validation_' . ($intForId + 1),
                ];
            } else {
                $datas[] = [
                    'id' => 'choice_approved_' . $intForId,
                    'sourceRef' => 'exclusive_gateway_' . $intForId,
                    'targetRef' => 'approved',
                ];
            }
        }

        $datas = array_merge($datas, self::choiceRollback($intForIds));

        $datas[] = [
            'id' => 'choice_approved_admin',
            'name' => 'Valider le circuit',
            'sourceRef' => 'exclusive_gateway_admin',
            'targetRef' => 'approved',
            'conditionExpression' => [
                'xsi:type' => 'tFormalExpression',
                '@value' => '${action == "approve"}',
            ],
        ];

        $datas[] = [
            'id' => 'task_admin_to_exclusive_gateway_admin',
            'sourceRef' => 'task_admin',
            'targetRef' => 'exclusive_gateway_admin',
        ];

        $datas[] = [
            'id' => 'choice_cancel_admin',
            'name' => 'Annuler le circuit',
            'sourceRef' => 'exclusive_gateway_admin',
            'targetRef' => 'cancelled',
            'conditionExpression' => [
                'xsi:type' => 'tFormalExpression',
                '@value' => '${action == "cancel"}',
            ],
        ];

        $datas[] = [
            'id' => 'choice_rejected_admin',
            'name' => 'Rejeter',
            'sourceRef' => 'exclusive_gateway_admin',
            'targetRef' => 'rejected',
            'conditionExpression' => [
                'xsi:type' => 'tFormalExpression',
                '@value' => '${action == "reject"}',
            ],
        ];

        $datas[] = [
            'id' => 'to_tasks',
            'name' => 'Initialisation',
            'sourceRef' => 'parallel_gateway_admin_or_tasks',
            'targetRef' => 'task_validation_1',
        ];

        $datas[] = [
            'id' => 'init_process',
            'name' => 'To initial state',
            'sourceRef' => 'startEvent',
            'targetRef' => 'parallel_gateway_admin_or_tasks',
        ];

        $datas[] = [
            'id' => 'to_admin_task',
            'sourceRef' => 'parallel_gateway_admin_or_tasks',
            'targetRef' => 'task_admin',
        ];

        foreach ($datas as $data) {
            self::addChildNodeProcess('sequenceFlow', $data, $xml_data);
        }
    }

    /**
     * @param array $intForIds tableau du nombre d'étapes du circuit
     * @return array
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function choiceRollback($intForIds)
    {
        $datas = [];
        foreach ($intForIds as $intForId) {
            if ($intForId != 1) {
                $datas = array_merge($datas, self::generateSomething($intForId));
            }
        }

        return $datas;
    }

    /**
     * @param array $intForId tableau du nombre d'étapes du circuit
     * @return array
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    private static function generateSomething($intForId)
    {
        $data = [];
        for ($i = $intForId - 1; $i > 0; --$i) {
            $data[] = [
                'id' => 'choice_rollback_from_' . $intForId . '_to_' . $i,
                'sourceRef' => 'exclusive_gateway_' . $intForId,
                'targetRef' => 'task_validation_' . $i,
                'conditionExpression' => [
                    'xsi:type' => 'tFormalExpression',
                    '@value' => '${action_' . $intForId . ' == "rollback_' . $i . '"}',
                ],
            ];
        }
        // rollback_system
        $data[] = [
            'id' => 'rollback_system_from_' . $intForId,
            'sourceRef' => 'exclusive_gateway_' . $intForId,
            'targetRef' => 'task_validation_' . ($intForId - 1),
            'conditionExpression' => [
                'xsi:type' => 'tFormalExpression',
                '@value' => '${action_' . $intForId . ' == "rollback_system"}',
            ],
        ];

        return $data;
    }

    /**
     * Ajout des noeuds "boundaryEvent" avec les attribues et les sous noeuds
     *
     * @param array $intForIds tableau du nombre d'étapes du circuit
     * @param \SimpleXMLElement $xml_data xml_data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addBoundaryEvent($intForIds, &$xml_data)
    {
        foreach ($intForIds as $intForId) {
            $datas[] = [
                'id' => 'catch_cancel_task_event_' . $intForId,
                'attachedToRef' => 'task_validation_' . $intForId,
                'cancelActivity' => 'true',
                'signalEventDefinition' => [
                    'signalRef' => 'cancel_validations',
                ],
            ];
        }

        foreach ($datas as $data) {
            self::addChildNodeProcess('boundaryEvent', $data, $xml_data);
        }
    }

    /**
     * Ajout des noeuds "intermediateThrowEvent" avec les attribues et les sous noeuds
     *
     * @param array $intForIds tableau du nombre d'étapes du circuit
     * @param \SimpleXMLElement $xml_data xml_data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addIntermediateThrowEvent($intForIds, &$xml_data)
    {
        foreach ($intForIds as $intForId) {
            $datas[] = [
                'id' => 'throw_cancel_task_event_' . $intForId,
                'signalEventDefinition' => [
                    'signalRef' => 'cancel_validations',
                ],
            ];
        }

        foreach ($datas as $data) {
            self::addChildNodeProcess('intermediateThrowEvent', $data, $xml_data);
        }
    }

    /**
     * Ajout du noeud "BPMNDiagram" pour l'utilisation de flowable avec l'interface graphique
     *
     * @param string $nameCircuit nom du circuit
     * @param \SimpleXMLElement $xml_data xml_data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @deprecated non utiliser avec l'utilisation de l'api de flowable
     * @version V0.0.9
     */
    protected static function addBpmndiDiagram($nameCircuit, $xml_data)
    {
        $bpmndiDiagramm = self::addNewChild($xml_data, 'bpmndi:BPMNDiagram');

        $idDiagramm = 'BPMNDiagram_' . self::formatForID($nameCircuit);
        self::addAttribute($bpmndiDiagramm, 'id', $idDiagramm);

        $bpmndiPlane = self::addNewChild($bpmndiDiagramm, 'bpmndi:BPMNPlane');
        self::addAttribute($bpmndiPlane, 'bpmnElement', self::formatForID($nameCircuit));
        self::addAttribute($bpmndiPlane, 'id', 'BPMNPlane_' . self::formatForID($nameCircuit));
    }

    /**
     * Détermine si de tableau reçu est un multi-tableau ou pas
     *
     * @param array $arr Tableau à vérifier si il est multidimension
     * @return bool
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function multiArray($arr)
    {
        rsort($arr);

        return isset($arr[0]) && is_array($arr[0]);
    }

    /**
     * @param string $key key
     * @return |null
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function checkNamespace(string $key)
    {
        $exploded = explode(':', $key);
        if (count($exploded) === 1) {
            return null;
        } else {
            return $exploded[0];
        }
    }

    /**
     * @param \SimpleXMLElement $child child
     * @param array $datas datas
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function formatForAddAttributeToChild($child, $datas)
    {
        foreach ($datas as $key => $data) {
            if (is_array($data) === true) {
                $value = null;

                if (array_key_exists('@value', $data)) {
                    $value = Hash::get($data, ['@value']);
                    unset($data['@value']);
                }

                $chidOfChild = self::addNewChild($child, $key, $value);

                foreach ($data as $keyValue => $value) {
                    self::addAttribute($chidOfChild, $keyValue, $value);
                }
            } else {
                self::addAttribute($child, $key, $data);
            }
        }
    }

    /**
     * @param \SimpleXMLElement $child child
     * @param string $key key
     * @param null $value value
     * @return mixed
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addNewChild($child, $key, $value = null)
    {
        $namespace = self::checkNamespace($key);
        $newChild = $child->addChild($key, null, $namespace);

        if ($value != null) {
            $child_node = dom_import_simplexml($newChild);
            $child_owner = $child_node->ownerDocument;
            $child_node->appendChild($child_owner->createCDATASection($value));
        }

        return $newChild;
    }

    /**
     * @param \SimpleXMLElement $child child
     * @param string $key key
     * @param string $data data
     * @return void
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function addAttribute(SimpleXMLElement $child, string $key, string $data): void
    {
        $namespace = self::checkNamespace($key);
        $child->addAttribute($key, $data, $namespace);
    }

    /**
     * @param string $value value
     * @return string
     * @access protected
     * @created 26/03/2019
     * @version V0.0.9
     */
    protected static function formatForID(string $value): string
    {
        $value = str_replace(' ', '', $value);
        $value = LettercaseFormattableBehavior::noAccents($value);
        $value = mb_convert_case($value, MB_CASE_LOWER);

        return $value;
    }
}
