<?php
declare(strict_types=1);

namespace WrapperFlowable\Api;

use WrapperFlowable\Model\Entity\InstanciableInterface;

/**
 * Interface FlowableWrapperInterface
 *
 * @package WrapperFlowable\Api
 */
interface FlowableWrapperInterface
{
    /**
     * @return array
     */
    public function getDeployments(): array;

    /**
     * @param string $deploymentId id
     * @return array
     */
    public function getDeployment(string $deploymentId): array;

    /**
     * @param array $data data
     * @return array
     */
    public function createDeployment(array $data): array;

    /**
     * @param string $deploymentId id
     * @return void
     */
    public function deleteDeployment(string $deploymentId): void;

    /**
     * @param string $processDefinitionKey key
     * @return array
     */
    public function createInstance(string $processDefinitionKey): array;

    /**
     * @param string $businessKey key
     * @return array
     */
    public function getInstanceHistoryByBusinessKey(string $businessKey): array;

    /**
     * @param \WrapperFlowable\Model\Entity\InstanciableInterface $entity entity
     * @param int $userId id
     * @return array
     */
    public function getInstanceDetails(InstanciableInterface $entity, int $userId = -1): array;

    /**
     * @param string $key key
     * @return void
     */
    public function deleteInstance(string $key): void;

    /**
     * @param int $userId id
     * @return array
     */
    public function getTasksByCandidateUser(int $userId): array;

    /**
     * @param string $taskId id
     * @return array
     */
    public function getTask(string $taskId): array;

    /**
     * @param string $taskId id
     * @return array
     */
    public function getTaskVariables(string $taskId): array;

    /**
     * @param string $taskId id
     * @return array
     */
    public function getHistoryTaskById(string $taskId): array;

    /**
     * @param string $businessId id
     * @return array
     */
    public function getTaskHistoryByBusinessId(string $businessId): array;

    /**
     * @param string $businessId id
     * @return array
     */
    public function getAdminTaskByBusinessId(string $businessId): array;

    /**
     * @param int $userId id
     * @param array $instanceIds ids
     * @return array
     */
    public function filterInstancesWhereUserIsCurrent(int $userId, array $instanceIds): array;

    /**
     * @param int $userId user
     * @param string $instanceId instance
     * @return bool
     */
    public function isUserCurrentForInstance(int $userId, string $instanceId): bool;

    /**
     * @param string $processDefinitionId id
     * @return array
     */
    public function getInstancesByProcessDefinitionId(string $processDefinitionId): array;

    /**
     * @param string $processDefinitionKey key
     * @return array
     */
    public function getSteps(string $processDefinitionKey): array;

    /**
     * @param array $data data
     * @return void
     */
    public function executeAnActionOnTask(array $data): void;

    /**
     * @param string $taskId id
     * @return void
     */
    public function executeAnActionForwardOnTask(string $taskId): void;
}
