<?php
declare(strict_types=1);

namespace WrapperFlowable\Api;

use App\Utilities\Api\Client as ApiClient;
use Cake\Core\Configure;
use Cake\Http\Client as CakeClient;
use Cake\Http\Client\Response;
use Cake\Log\Log;
use Cake\Utility\Hash;
use PDO;

/**
 * Class FlowableFacade
 *
 * @package WrapperFlowable\Api
 */
class FlowableFacade extends ApiClient
{
    //URL
    public const API_BASE = 'service';

    //STATUS
    public const STATUS_APPROVED = 'approved';
    public const STATUS_APPROVED_ADMIN = 'approved_admin';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_CANCELLED = 'cancelled';
    public const STATUS_CURRENT = 'current';
    public const STATUS_PENDING = 'pending';
    public const STATUS_SKIPPED = 'skipped';

    //ACTIONS
    public const ACTION_APPROVE = 'approve';
    public const ACTION_REJECT = 'reject';
    public const ACTION_FORWARD = 'forward';
    public const ACTION_APPROVE_FORWARD = 'approve_forward';
    public const ACTION_CANCEL = 'cancel'; // admin
    public const ACTION_BYPASS = 'bypass'; // admin

    public const ACTION_SPECIALE = 'Actions spéciales';

    /**
     * Base pagination for flowable, the idea is to never have a partial response from flowable
     * (a second request is made with the correct pagination count if needed)
     */
    public const PAGINATION_COUNT = 50;

    /**
     * @var \Cake\Http\Client client
     */
    private $client;

    /**
     * @var string
     */
    private $dbName;

    /**
     * @var string
     */
    private $dbHost;

    /**
     * @var string
     */
    private $dbUser;

    /**
     * @var string
     */
    private $dbPass;

    /**
     * FlowableFacade constructor.
     */
    public function __construct()
    {
        $config = Configure::read('Flowable');

        $this->client = new CakeClient(
            [
                'auth' => $config['auth'],
            ]
        );

        $url = 'http://' . env('FLOWABLE_HOST', $config['host']) . '/flowable-rest';

        $this->dbName = env('DB_FLOWABLE_DATABASE', 'flowable');
        $this->dbHost = env('DB_FLOWABLE_HOST', 'postgres-flow');
        $this->dbUser = env('DB_FLOWABLE_USER', 'flowable');
        $this->dbPass = env('POSTGRES_FLOWABLE_PASSWORD', 'flowable');

        parent::__construct($this->client, $url, self::API_BASE);
    }

    /**
     * @param array $urlParams params
     * @return \Cake\Http\Client\Response
     */
    public function getDeployments(array $urlParams = []): Response
    {
        $urlParams['size'] = self::PAGINATION_COUNT;

        $response = $this->get('repository/deployments', $urlParams);

        if (isset($response->getJson()['total']) && $response->getJson()['total'] > $urlParams['size']) {
            $urlParams['size'] = $response->getJson()['total'] + 1;
            $response = $this->get('repository/deployments', $urlParams);
        }

        return $response;
    }

//    /**
//     * @param string $deploymentName deploymentName
//     *
//     * @return mixed
//     */
//    public function getDeploymentByName(string $deploymentName): Response
//    {
//        return $this->get('repository/deployments', ['name' => $deploymentName]);
//    }

    /**
     * Get a deployment thanks to its id.
     *
     * @param string $deploymentId deploymentId
     * @return \Cake\Http\Client\Response
     */
    public function getDeployment(string $deploymentId): Response
    {
        $endpoint = 'repository/deployments/' . $deploymentId;

        return $this->get($endpoint, []);
    }

    /**
     * @param array $data data to post
     * @return \Cake\Http\Client\Response
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function createDeployment(array $data): Response
    {
        if (empty($data['deploymentName']) || empty($data['file']['path'])) {
            $response = new Response([], json_encode(['error' => 'invalid data']));

            return $response->withStatus(400);
        }

        $urlParams = ['deploymentName' => $data['deploymentName']];
        $file = fopen($data['file']['path'], 'r');

        if ($file === false) {
            $response = new Response([], json_encode([
                'error' => 'invalid file',
            ]));

            return $response->withStatus(400);
        }

        $bodyParams = ['file' => $file];
        $response = $this->post('repository/deployments', $urlParams, $bodyParams);

        if ($response->getStatusCode() !== 201) { // bypass si erreur
            return $response;
        }

        $deployment = $response->getJson();
        $processDefinitionKey = $this->getProcessDefinitionKeyByDeploymentId($deployment['id']);

        $deployment['process_definition_key'] = $processDefinitionKey;

        $headers = [];
        foreach (array_keys($response->getHeaders()) as $key) {
            $headers[] = sprintf('%s: %s', $key, $response->getHeaderLine($key));
        }
        $newResponse = new Response($headers, json_encode($deployment));

        return $newResponse->withStatus($response->getStatusCode());
    }

    /**
     * Delete a deployment by its id.
     *
     * @param string $deploymentId the deployment id
     * @return \Cake\Http\Client\Response
     */
    public function deleteDeployment(string $deploymentId): Response
    {
        $endpoint = 'repository/deployments/' . $deploymentId;

        return $this->delete($endpoint, []);
    }

    /**
     * Get the process definition Id of a deployment.
     *
     * @param string $deploymentId deploymentId
     * @return null|array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    private function getProcessDefinitionKeyByDeploymentId($deploymentId): string
    {
        $response = $this->get('repository/process-definitions', ['deploymentId' => $deploymentId]);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        return Hash::extract($response->getJson(), 'data.{n}.key')[0];
    }

    /**
     * Post a request to create an instance of a specific deployment.
     *
     * @param string $processDefinitionKey key
     * @param string $instanceId id
     * @return \Cake\Http\Client\Response
     */
    public function createInstance(string $processDefinitionKey, string $instanceId): Response
    {
        $instance = [
            'businessKey' => $instanceId,
            'processDefinitionKey' => $processDefinitionKey,
            'variables' => [
                [
                    'name' => 'business_id',
                    'value' => $instanceId,
                ],
            ],
        ];

        return $this->post('runtime/process-instances', [], json_encode($instance, JSON_PRETTY_PRINT));
    }

    /**
     * @param string $id id
     * @return \Cake\Http\Client\Response
     */
    public function getInstance(string $id): Response
    {
        return $this->get('runtime/process-instances/' . $id, []);
    }

    /**
     * @param string $key key
     * @return \Cake\Http\Client\Response
     */
    public function deleteInstanceByBusinessKey(string $key): Response
    {
        $result = $this->get('runtime/process-instances', ['businessKey' => $key])->getJson()['data'];

        if (count($result) !== 1) {
            $response = new Response();

            return $response->withStatus(404);
        }

        $id = $result[0]['id'];

        return $this->delete('runtime/process-instances/' . $id, []);
    }

    /**
     * @param string $processDefinitionKey key
     * @return \Cake\Http\Client\Response
     */
    public function getResourceData(string $processDefinitionKey): Response
    {
        try {
            $process = $this->getProcessByKey($processDefinitionKey)->getJson()['data'][0];
        } catch (FlowableException $e) {
            $response = new Response();

            return $response->withStatus(404);
        }

        $resourcePath = explode('/', $process['resource']);
        $resourceName = $resourcePath[count($resourcePath)-1];

        $url = 'repository/deployments/' . $process['deploymentId'] . '/resourcedata/' . $resourceName;

        return $response = $this->get($url, []);
    }

    /**
     * @param string $businessKey key
     * @return \Cake\Http\Client\Response
     */
    public function getInstanceHistoryByBusinessKey(string $businessKey): Response
    {
        return $this->get(
            'history/historic-process-instances',
            ['businessKey' => $businessKey, 'includeProcessVariables' => true]
        );
    }

    /**
     * @param string $businessId id
     * @param array $extraOptions extra options
     * @return \Cake\Http\Client\Response
     */
    public function getTaskHistoryByBusinessId(string $businessId, array $extraOptions = []): Response
    {
        $queryParam = array_merge($extraOptions, [
            'processVariables' => [
                [
                    'name' => 'business_id',
                    'operation' => 'equals',
                    'value' => $businessId,
                    'type' => 'string',
                ],
            ],
            'includeProcessVariables' => true]);
        $urlParams['size'] = self::PAGINATION_COUNT;

        $json = json_encode($queryParam);

        $response = $this->post('query/historic-task-instances', $urlParams, $json);

        if (isset($response->getJson()['total']) && $response->getJson()['total'] > $urlParams['size']) {
            $urlParams['size'] = $response->getJson()['total'] + 1;
            $response = $this->post('query/historic-task-instances', $urlParams, $json);
        }

        return $response;
    }

    /**
     * Récupère la tache de validation en cours
     *
     * @param string $businessId business id
     * @return int
     */
    public function getCurrentTaskValidationByBussinessId(string $businessId): Response
    {
        Log::write('info', 'le business id ' . $businessId, 'flowable');
        $optionsToGetTheCurrentValidator = [
            'taskDefinitionKey' => 'task_validation',
            'finished' => false,
        ];

        return $this->getTaskHistoryByBusinessId($businessId, $optionsToGetTheCurrentValidator);
    }

    /**
     * @param array $queryParam params
     * @return \Cake\Http\Client\Response
     */
    public function getTasks(array $queryParam): Response
    {
        $queryParam['size'] = self::PAGINATION_COUNT;

        $response = $this->get('runtime/tasks', $queryParam);

        if (isset($response->getJson()['total']) && $response->getJson()['total'] > $queryParam['size']) {
            $urlParams['size'] = $response->getJson()['total'] + 1;
            $response = $this->get('runtime/tasks', $queryParam);
        }

        return $response;
    }

    /**
     * @param array $bodyParams params
     * @return \Cake\Http\Client\Response
     */
    public function queryTasks(array $bodyParams): Response
    {
        $urlParams = ['size' => self::PAGINATION_COUNT];
        $json = json_encode($bodyParams);

        $response = $this->post('query/tasks', $urlParams, $json);

        if (isset($response->getJson()['total']) && $response->getJson()['total'] > $urlParams['size']) {
            $urlParams['size'] = $response->getJson()['total'] + 1;

            return $this->post('query/tasks', $urlParams, $json);
        }

        return $response;
    }

    /**
     * @param string $taskId id
     * @return \Cake\Http\Client\Response
     */
    public function getTask(string $taskId): Response
    {
        return $this->get('runtime/tasks/' . $taskId, []);
    }

//    /**
//     * @param int $businessId Identifiant du projet dans flowable.
//     * @return Response
//     */
//    public function getAdminTaskByBusinessId(string $businessId): Response
//    {
//        $queryParam = [
//            'business_id' => $businessId,
//            'taskDefinitionKey' => 'task_admin',
//            'includeProcessVariables' => true
//        ];
//
//        return $this->get('runtime/tasks', $queryParam);
//    }

    /**
     * @param array $urlParams should contain taskId
     * @return \Cake\Http\Client\Response
     */
    public function getHistoryTaskById($urlParams): Response
    {
        return $this->get('history/historic-task-instances', $urlParams);
    }

    /**
     * @param array $bodyParams params
     * @return \Cake\Http\Client\Response
     */
    public function getTasksByCandidateUser(array $bodyParams): Response
    {
        $urlParams = ['size' => self::PAGINATION_COUNT];

        $json = json_encode($bodyParams);

        $response = $this->post('query/tasks', $urlParams, $json);

        if (isset($response->getJson()['total']) && $response->getJson()['total'] > $urlParams['size']) {
            $urlParams['size'] = $response->getJson()['total'] + 1;

            return $this->post('query/tasks', $urlParams, $json);
        }

        return $response;
    }

    /**
     * Filtre une liste d'instance (businessKey) pour retourner celles pour lesquelles l'utilisateur est le validateur en cours
     *
     * @param int $userId id
     * @param string[] $instanceIds businessKeys
     * @return \Cake\Http\Client\Response
     */
    public function filterInstancesWhereUserIsCurrent(int $userId, array $instanceIds): Response
    {
        if (count($instanceIds) === 0) {
            $response = new Response([], json_encode([]));

            return $response->withStatus(200);
        }

        $db = new PDO("pgsql:dbname=$this->dbName;host=$this->dbHost", $this->dbUser, $this->dbPass);

        $ids = implode(',', array_map(
            function ($e) {
                return "'$e'";
            },
            $instanceIds
        ));

        $sql = /* @lang sql */
            "SELECT var_business.text_ FROM act_ru_variable var_user
            LEFT JOIN act_ru_variable var_business
                ON var_user.proc_inst_id_ = var_business.proc_inst_id_ AND var_business.name_ = 'business_id'
            WHERE var_user.name_ = 'users_candidate'
            AND var_business.text_ IN ($ids)
            AND (
                var_user.text_ = :reg1
                OR var_user.text_ LIKE :reg2
                OR var_user.text_ LIKE :reg3
                OR var_user.text_ LIKE :reg4
            )
        ";// @codingStandardsIgnoreLine
        $sth = $db->prepare($sql);
        $sth->execute([
            'reg1' => $userId, // only him
            'reg2' => "$userId,%", // him then others
            'reg3' => "%,$userId,%", // him amongst others
            'reg4' => "%,$userId", // others then him
        ]);

        $results = [];
        while ($row = $sth->fetch()) {
            $results[] = $row['text_'];
        }

        $response = new Response([], json_encode($results));

        return $response->withStatus(200);
    }

    /**
     * Indique si un utilisateur est validateur courant pour l'instance ou pas
     *
     * @param int $userId user
     * @param string $instanceId instance
     * @return \Cake\Http\Client\Response
     */
    public function isUserCurrentForInstance(int $userId, string $instanceId): Response
    {
        $db = new PDO(
            "pgsql:dbname=$this->dbName;host=$this->dbHost", $this->dbUser, $this->dbPass // @codingStandardsIgnoreLine
        );

        $sql = /* @lang sql */
            "SELECT COUNT(var_business.id_) FROM act_ru_variable var_user
            LEFT JOIN act_ru_variable var_business
                ON var_user.proc_inst_id_ = var_business.proc_inst_id_ AND var_business.name_ = 'business_id'
            WHERE var_user.name_ = 'users_candidate'
            AND var_business.text_ = :instance
            AND (
                var_user.text_ = :reg1
                OR var_user.text_ LIKE :reg2
                OR var_user.text_ LIKE :reg3
                OR var_user.text_ LIKE :reg4
            )
        ";
        $sth = $db->prepare($sql);
        $sth->execute([
            'instance' => $instanceId,
            'reg1' => $userId, // only him
            'reg2' => "$userId,%", // him then others
            'reg3' => "%,$userId,%", // him amongst others
            'reg4' => "%,$userId", // others then him
        ]);

        $result = $sth->fetch()['count'] !== 0;

        $response = new Response([], json_encode(['is_current' => $result]));

        return $response->withStatus(200);
    }

//    /**
//     * @param array $bodyParams parameters
//     *
//     * @return Response
//     */
//    public function getTasksByCandidateGroups($bodyParams): Response
//    {
//        if (array_key_exists('candidateGroups', $bodyParams)) {
//            $badRequest = new Response();
//            $badRequest->withStatus(400);
//
//            return $badRequest;
//        }
//        unset($bodyParams['candidateUser']);
//        $urlParams = ['size' => self::PAGINATION_COUNT];
//
//        $response = $this->post('query/tasks', $urlParams, $bodyParams);
//
//        if ($response->getStatusCode() === 200 && $response->getJson()['total'] > $response->getJson()['size']) {
//            $urlParams['size'] = $response->getJson()['total'] + 1;
//
//            $response = $this->post('query/tasks', $urlParams, $bodyParams);
//        }
//
//        return $response;
//    }

//    /**
//     * @param array $urlParams Url parameters
//     *
//     * @return Response
//     */
//    public function getTasksByCandidateGroupOrUser($urlParams): Response
//    {
//        if (array_key_exists('candidateGroups', $urlParams)) {
//            unset($urlParams['candidateUser']);
//
//            return $this->getTasksByCandidateGroups($urlParams);
//        }
//
//        if (array_key_exists('candidateUser', $urlParams)) {
//            unset($urlParams['candidateGroups']);
//
//            return $this->getTasksByCandidateUser($urlParams);
//        }
//    }

    /**
     * @param array $data task, action, userId
     * @return \Cake\Http\Client\Response
     */
    public function executeAnActionOnTask(array $data): Response
    {
        $dataToSend = [
            'action' => 'complete',
            'variables' => [
                [
                    'name' => 'action',
                    'value' => $data['action'],
                ],
                [
                    'name' => 'userId',
                    'value' => $data['userId'],
                ],
                [
                    'name' => 'comment',
                    'value' => $data['comment'] ?? null,
                ],
            ],
        ];

        return $this->post('runtime/tasks/' . $data['task'], [], json_encode($dataToSend));
    }

    /**
     * @param string $taskId id
     * @return \Cake\Http\Client\Response
     */
    public function executeAnActionForwardOnTask(string $taskId): Response
    {
        $variables = $this->getTasksVariables($taskId)->getJson();

        $vars = [];
        $vars['admin_groups'] = Hash::extract($variables, '{n}[name=/^admin_groups$/]');
        $vars['groups_candidate'] = Hash::extract($variables, '{n}[name=/^groups_candidate$/]');
        $vars['users_candidate'] = Hash::extract($variables, '{n}[name=/^users_candidate$/]');
        $vars['business_id'] = Hash::extract($variables, '{n}[name=/^business_id$/]');

        $data = [
            'action' => 'complete',
            'variables' => [
                [
                    'name' => 'action',
                    'value' => self::ACTION_FORWARD,
                ],
                [
                    'name' => 'forward_groups_candidate',
                    'type' => $vars['groups_candidate']['type'],
                    'value' => $vars['groups_candidate']['value'],
                    'scope' => $vars['groups_candidate']['scope'],
                ],
                [
                    'name' => 'forward_users_candidate',
                    'type' => $vars['users_candidate']['type'],
                    'value' => $vars['users_candidate']['value'],
                    'scope' => $vars['users_candidate']['scope'],
                ],
                [
                    'name' => 'forward_admin_group',
                    'type' => $vars['admin_groups']['type'],
                    'value' => $vars['admin_groups']['value'],
                    'scope' => $vars['admin_groups']['scope'],
                ],
                [
                    'name' => 'business_id',
                    'type' => $vars['business_id']['type'],
                    'value' => $vars['business_id']['value'],
                    'scope' => $vars['business_id']['scope'],
                ],
            ],
        ];

        return $this->post('query/tasks/' . $taskId, [], json_encode($data));
    }

    /**
     * @param string $flowableTaskId The task identifier
     * @return \Cake\Http\Client\Response
     */
    public function getTasksVariables(string $flowableTaskId): Response
    {
        $urlParams = ['size' => self::PAGINATION_COUNT];

        $response = $this->get('runtime/tasks/' . $flowableTaskId . '/' . 'variables', $urlParams);

//        if ($response->getStatusCode() === 200 && $response->getJson()['total'] > $response->getJson()['size']) {
//            $urlParams['size'] = $response->getJson()['total'] + 1;
//
//            $response = $this->get('runtime/tasks/' . $flowableTaskId . '/' . 'variables', $urlParams);
//        }

        return $response;
    }

//    /**
//     * Generate a string like organizationId_structureId_uuid.
//     *
//     * @param int $structureId the id of the structure
//     * @return void
//     */
//    private function generateDeploymentKeyName($structureId): void
//    {
//        $deploymentKeyName = $structureId . '_' . hash('sha256', Text::uuid());
////        $this->setDeploymentName($deploymentKeyName);
//    }

    /**
     * @param string $key key
     * @return mixed
     * @throws \WrapperFlowable\Api\FlowableException
     */
    private function getProcessByKey(string $key): Response
    {
        $response = $this->get('repository/process-definitions', ['key' => $key]);

        if (count($response->getJson()['data']) !== 1) {
            throw new FlowableException($response);
        }

        return $response;
    }

    /**
     * @param string $processDefinitionId identifiant du Process Definiton (model de circuit)
     * @return \Cake\Http\Client\Response
     */
    public function getInstancesByProcessDefinitionId(string $processDefinitionId): Response
    {
        $urlParams = [
            'processDefinitionKey' => $processDefinitionId,
            'size' => self::PAGINATION_COUNT,
        ];
        $response = $this->get('runtime/process-instances', $urlParams);

        if ($response->getStatusCode() === 200 && $response->getJson()['total'] > $response->getJson()['size']) {
            $urlParams['size'] = $response->getJson()['total'] + 1;

            $response = $this->get('runtime/process-instances', $urlParams);
        }

        return $response;
    }

    /**
     * Check if the specified worflows isFinished with the given status.
     *
     * @param string $businessKey identifiant du projet.
     * @param string $status The status to check.
     * @return \Cake\Http\Client\Response
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function isFinishedWithStatus($businessKey, $status): Response
    {
        $response = $this->getInstanceHistoryByBusinessKey($businessKey);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        $headers = [];
        foreach (array_keys($response->getHeaders()) as $key) {
            $headers[] = sprintf('%s: %s', $key, $response->getHeaderLine($key));
        }
        $newResponse = new Response(
            $headers,
            json_encode($response->getJson()['data'][0]['endActivityId'] === $status)
        );

        return $newResponse;
    }
}
