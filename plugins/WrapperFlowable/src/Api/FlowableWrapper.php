<?php
declare(strict_types=1);

namespace WrapperFlowable\Api;

use Cake\Utility\Hash;
use Ramsey\Uuid\Uuid;
use WrapperFlowable\Model\Entity\InstanciableInterface;
use WrapperFlowable\Utilities\InstanceDetailsExtractor;
use WrapperFlowable\Utilities\XmlWorkflowConverter;

class FlowableWrapper implements FlowableWrapperInterface
{
    /**
     * @var \WrapperFlowable\Api\FlowableFacade
     */
    protected $facade;

    /**
     * Wrapper constructor.
     */
    public function __construct()
    {
        $this->facade = new FlowableFacade();
    }

    /**
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getDeployments(): array
    {
        $response = $this->facade->getDeployments();

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        return $response->getJson()['data'];
    }

//    /**
//     * @param string $deploymentName name
//     * @return array|null
//     * @throws FlowableException
//     */
//    public function getDeploymentByName(string $deploymentName)
//    {
//        $response = $this->facade->getDeploymentByName($deploymentName);
//
//        if ($response->getStatusCode() !== 200) {
//            throw new FlowableException($response);
//        }
//
//        return $response->getJson();
//    }

    /**
     * Get a deployment thanks to its id
     *
     * @param string $deploymentId id
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getDeployment(string $deploymentId): array
    {
        $response = $this->facade->getDeployment($deploymentId);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        return $response->getJson();
    }

    /**
     * @param array $data d
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function createDeployment(array $data): array
    {
        $response = $this->facade->createDeployment($data);

        if ($response->getStatusCode() !== 201) {
            throw new FlowableException($response);
        }

        return $response->getJson();
    }

    /**
     * @param string $deploymentId id
     * @return void
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function deleteDeployment(string $deploymentId): void
    {
        $response = $this->facade->deleteDeployment($deploymentId);

        if ($response->getStatusCode() !== 204) {
            throw new FlowableException($response);
        }
    }

    /**
     * @param string $processDefinitionKey key
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function createInstance(string $processDefinitionKey): array
    {
        $instanceId = Uuid::uuid4()->toString();
        $response = $this->facade->createInstance($processDefinitionKey, $instanceId);

        if ($response->getStatusCode() !== 201) {
            throw new FlowableException($response);
        }

        return $response->getJson();
    }

    /**
     * @param string $businessKey key
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getInstanceHistoryByBusinessKey(string $businessKey): array
    {
        $response = $this->facade->getInstanceHistoryByBusinessKey($businessKey);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        $data = $response->getJson()['data'];

        if (count($data) !== 1) {
            throw new FlowableException($response); // @fixme : différencier 0 et > 1 ?
        }

        return $data[0];
    }

    /**
     * @param \WrapperFlowable\Model\Entity\InstanciableInterface $entity entity
     * @param int $userId id
     * @return array
     * @throws \Exception
     */
    public function getInstanceDetails(InstanciableInterface $entity, int $userId = -1): array
    {
        return InstanceDetailsExtractor::getInstanceDetails($entity, $userId);
    }

    /**
     * @param string $key businessKey
     * @return void
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function deleteInstance(string $key): void
    {
        $response = $this->facade->deleteInstanceByBusinessKey($key);

        if ($response->getStatusCode() !== 204) {
            throw new FlowableException($response);
        }
    }

    /**
     * @param int $userId id
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getTasksByCandidateUser(int $userId): array
    {
        $bodyParams = [
            'candidateUser' => $userId,
            'includeProcessVariables' => true,
            'withoutDeleteReason' => true, // ne pas récupérer les task d'annulation
        ];

        $response = $this->facade->getTasksByCandidateUser($bodyParams);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        return $response->getJson()['data'];
    }

    /**
     * @param string $businessKey id_flowable_instance => représente la businnesKey pour l'instantce du circuit
     * @return array
     */
    public function getCurrentUsersCadidatesIdsByBusinessKey(string $businessKey): array
    {
        $response = $this->facade->getCurrentTaskValidationByBussinessId($businessKey);

        if (!empty($response->getJson()['data'])) {
            $usersCadidatesIds = Hash::extract(
                $response->getJson()['data']['0']['variables'],
                '{n}[name=/^users_candidate$/]'
            );

            return strpos($usersCadidatesIds['0']['value'], ',') ?
                explode(',', $usersCadidatesIds['0']['value']) :
                [$usersCadidatesIds['0']['value']];
        }

        return [];
    }

    /**
     * @param string $taskId id
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getTask(string $taskId): array
    {
        $response = $this->facade->getTask($taskId);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        $data = $response->getJson();

        return $data;
    }

    /**
     * @param string $taskId id
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getTaskVariables(string $taskId): array
    {
        $response = $this->facade->getTasksVariables($taskId);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        return $response->getJson();
    }

    /**
     * @param string $taskId id
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getHistoryTaskById(string $taskId): array
    {
        $urlParams = [
            'taskId' => $taskId,
            'includeProcessVariables' => 'true',
        ];

        $response = $this->facade->getHistoryTaskById($urlParams);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        $data = $response->getJson()['data'];

        if (count($data) !== 1) {
            throw new FlowableException($response); // @fixme : différencier 0 et > 1 ?
        }

        return $data[0];
    }

    /**
     * @param string $businessId id
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getTaskHistoryByBusinessId(string $businessId): array
    {
        $response = $this->facade->getTaskHistoryByBusinessId($businessId);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        return $response->getJson()['data'];
    }

    /**
     * @param string $businessId id
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getAdminTaskByBusinessId(string $businessId): array
    {
        $queryParam = [
            'processInstanceVariables' => [
                [
                    'name' => 'business_id',
                    'operation' => 'equals',
                    'value' => $businessId,
                    'type' => 'string',
                ],
            ],
            'taskDefinitionKey' => 'task_admin',
            'includeProcessVariables' => true,
        ];

        $response = $this->facade->queryTasks($queryParam);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        return $response->getJson()['data'];
    }

    /**
     * @param array $data data
     * @return void
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function executeAnActionOnTask(array $data): void
    {
        $response = $this->facade->executeAnActionOnTask($data);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }
    }

    /**
     * @param string $taskId id
     * @return void
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function executeAnActionForwardOnTask(string $taskId): void
    {
        $response = $this->facade->executeAnActionForwardOnTask($taskId);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }
    }

    /**
     * Filtre une liste d'instance (businessKey) pour retourner celles pour lesquelles l'utilisateur est le validateur en cours
     *
     * @param int $userId id
     * @param array $instanceIds keys
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function filterInstancesWhereUserIsCurrent(int $userId, array $instanceIds): array
    {
        if (count($instanceIds) === 0) {
            return [];
        }

        $response = $this->facade->filterInstancesWhereUserIsCurrent($userId, $instanceIds);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        return $response->getJson();
    }

    /**
     * @param int $userId user
     * @param string $instanceId instance
     * @return bool
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function isUserCurrentForInstance(int $userId, string $instanceId): bool
    {
        $response = $this->facade->isUserCurrentForInstance($userId, $instanceId);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        return $response->getJson()['is_current'];
    }

    /**
     * @param string $processDefinitionId id
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getInstancesByProcessDefinitionId(string $processDefinitionId): array
    {
        $response = $this->facade->getInstancesByProcessDefinitionId($processDefinitionId);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        return $response->getJson()['data'];
    }

    /**
     * @param string $processDefinitionKey key
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getSteps(string $processDefinitionKey): array
    {
        $response = $this->facade->getResourceData($processDefinitionKey);

        if ($response->getStatusCode() !== 200) {
            throw new FlowableException($response);
        }

        $xml = simplexml_load_string($response->getStringBody());

        return XmlWorkflowConverter::convertXmlToJson($xml);
    }
}
