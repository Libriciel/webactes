<?php
declare(strict_types=1);

namespace WrapperFlowable\Api;

use Cake\Http\Client\Response;
use Exception;

/**
 * Class FlowableException
 * To throws when flowable return something unexpected
 *
 * @package WrapperFlowable\Api
 */
class FlowableException extends Exception
{
    /**
     * FlowableException constructor.
     *
     * @param \Cake\Http\Client\Response $response api response
     */
    public function __construct(Response $response)
    {
        parent::__construct($response->getStringBody(), $response->getStatusCode());
    }
}
