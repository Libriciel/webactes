<?php
declare(strict_types=1);

namespace WrapperFlowable\Api;

use Cake\Core\Configure;

/**
 * Class WrapperFactory : create a wrapper or a Mock based on configuration
 */
abstract class WrapperFactory
{
    /**
     * @return \WrapperFlowable\Api\FlowableWrapperInterface
     */
    public static function createWrapper(): FlowableWrapperInterface
    {
        $config = Configure::read('Flowable');

        return empty($config['wrapper_mock']) ? new FlowableWrapper() : $config['wrapper_mock']::getInstance();
    }
}
