<?php
declare(strict_types=1);

namespace WrapperFlowable\Model\Entity;

/**
 * Interface InstanciableInterface Things you can send to a workflow
 *
 * @package WrapperFlowable\Model\Entity
 */
interface InstanciableInterface
{
    /**
     * @return string|null
     * @throws \Exception
     */
    public function getIdFlowableInstance();

    /**
     * @return \WrapperFlowable\Model\Entity\WorkflowInterface|null
     */
    public function getWorkflow();

    /**
     * @return int|null
     */
    public function getWorkflowId();
}
