<?php
declare(strict_types=1);

namespace WrapperFlowable\Model\Entity;

interface WorkflowInterface
{
    /**
     * The structure of the workflow can be edited
     *
     * @fixme : will disappear with flowable versionning
     * @return bool
     */
    public function isEditable(): bool;

    /**
     * The workflow can be deleted
     *
     * @return bool
     */
    public function isDeletable(): bool;

    /**
     * The steps of the workflow
     *
     * @return array
     */
    public function getSteps(): array;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * flowable process definition id
     *
     * @return string|null
     */
    public function getProcessDefinitionKey();
}
