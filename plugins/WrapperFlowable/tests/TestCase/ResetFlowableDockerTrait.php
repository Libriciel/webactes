<?php

namespace WrapperFlowable\Test\TestCase;

use PDO;

trait ResetFlowableDockerTrait
{

    public function resetFlowableDocker()
    {
        $this->resetDB();
    }

    /**
     * Reset complet la db du docker flowable-rest
     */
    public function resetDB()
    {
        $dbName = env('DB_FLOWABLE_DATABASE', 'flowable');
        $dbHost = env('DB_FLOWABLE_HOST', 'postgres-flow');
        $dbUser = env('DB_FLOWABLE_USER', 'flowable');
        $dbPass = env('POSTGRES_FLOWABLE_PASSWORD', 'flowable');

        $db = new PDO("pgsql:dbname=$dbName;host=$dbHost", $dbUser, $dbPass);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
        $db->exec(file_get_contents(ROOT . DS . 'docker-ressources/drop-flowable.sql'));
        $db->exec(file_get_contents(ROOT . DS . 'docker-ressources/create-flowable.sql'));
        $db = null;
    }
}
