<?php

namespace WrapperFlowable\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use WrapperFlowable\Api\FlowableFacade;
use WrapperFlowable\Test\TestCase\ResetFlowableDockerTrait;

/**
 * App\Controller\WorkflowsController Test Case
 */
class FlowableFacadeTest extends TestCase
{
    use IntegrationTestTrait;
    use ResetFlowableDockerTrait;

    const ACTION_ID_REGEXP = '/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}/';
    const INSTANCE_ID_REGEXP = '/^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}/';
    const FLOWABLE_PROCESS_ID_REGEXP = '/^process[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}-[0-9]+/';
    const FLOWABLE_FILE_REGEXP = '/^[0-9]{10}_[0-9]{8}.bpmn20.xml/';

    /**
     * @var string
     */
    public $nameFile;

    /**
     * @var string
     */
    public $pathFile;

    public function setUp(): void
    {
        $this->nameFile = 'circuit_test.bpmn20.xml';
        $this->pathFile = ROOT . '/plugins/WrapperFlowable/tests/Fixture/files/xml/flowable_bpm/' . $this->nameFile;

        parent::setUp();
        $this->resetFlowableDocker();
    }

    public function testCreateDeployment()
    {
        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        $response = $facade->createDeployment($data);

        $deployment = $response->getJson();

        $this->assertEquals('Circuit Test', $deployment['name']);
        $this->assertEquals('circuittest', $deployment['process_definition_key']);
    }

    public function testCreateDeploymentFail()
    {
        $facade = new FlowableFacade();

        $response = $facade->createDeployment([]);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(['error' => 'invalid data'], $response->getJson());

        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => '_' . $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => '_' . $this->pathFile,
            ],
        ];

        $response = $facade->createDeployment($data);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(['error' => 'invalid file'], $response->getJson());
    }

    public function testMoreDeploymentThanPagination()
    {
        $n = FlowableFacade::PAGINATION_COUNT + 1;

        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        for ($i = 0; $i < $n; $i++) {
            $facade->createDeployment($data);
        }

        $response = $facade->getDeployments();

        $this->assertEquals(200, $response->getStatusCode());
        $actual = count($response->getJson()['data']);
        $expected = $n + 1; // +1 for the webActes_validation_simple
        $this->assertEquals($expected, $actual);
        $this->assertEquals($expected, $response->getJson()['total']);
        $this->assertEquals($expected, $response->getJson()['size']);
    }

    public function testGetDeployment()
    {
        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        $deployment = $facade->createDeployment($data)->getJson();

        $this->assertEquals('Circuit Test', $deployment['name']);
        $actual = $facade->getDeployment($deployment['id'])->getJson();

        unset($deployment['process_definition_key']); // only present on creation
        $this->assertEquals($deployment, $actual);
    }

    public function testDeleteDeployment()
    {
        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        $deployment = $facade->createDeployment($data)->getJson();

        $response = $facade->deleteDeployment($deployment['id']);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertNull($response->getJson());

        $response = $facade->getDeployment($deployment['id']);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testCreateInstance()
    {
        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        $response = $facade->createDeployment($data);

        $deployment = $response->getJson();

        $instanceId = 'testitest';
        $response = $facade->createInstance($deployment['process_definition_key'], $instanceId);
        $this->assertEquals(201, $response->getStatusCode());

        $instance = $response->getJson();

        $keys = [
            'id',
            'url',
            'businessKey',
            'suspended',
            'ended',
            'processDefinitionId',
            'processDefinitionName',
            'startTime',
            'variables',
            'completed',
        ];

        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $instance);
        }

        $this->assertEquals(200, $facade->getInstance($instance['id'])->getStatusCode());
    }

    public function testDeleteInstance()
    {
        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        $response = $facade->createDeployment($data);
        $deployment = $response->getJson();

        $response = $facade->createInstance($deployment['process_definition_key'], 'testitest');
        $instance = $response->getJson();

        $response = $facade->deleteInstanceByBusinessKey($instance['businessKey']);

        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEmpty($response->getJson());

        $response = $facade->getInstance($instance['id']);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testGetInstanceHistoryByBusinessKey()
    {
        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        $response = $facade->createDeployment($data);
        $deployment = $response->getJson();

        $response = $facade->createInstance($deployment['process_definition_key'], 'testitest');
        $instance = $response->getJson();

        $response = $facade->deleteInstanceByBusinessKey($instance['businessKey']);

        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEmpty($response->getJson());

        $response = $facade->getInstanceHistoryByBusinessKey($instance['businessKey']);
        $this->assertEquals(200, $response->getStatusCode());
        $data = $response->getJson()['data'];

        $this->assertEquals(1, count($data));
        $this->assertEquals($instance['businessKey'], $data[0]['businessKey']);
        $this->assertEquals($instance['id'], $data[0]['id']);
    }

    public function testGetInstancesMoreThanPagination()
    {
        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        $response = $facade->createDeployment($data);
        $deployment = $response->getJson();

        $n = FlowableFacade::PAGINATION_COUNT + 10;

        for ($i = 0; $i < $n; $i++) {
            $facade->createInstance($deployment['process_definition_key'], 'testitest');
        }

        $response = $facade->getInstancesByProcessDefinitionId($deployment['process_definition_key']);

        $this->assertEquals($n, count($response->getJson()['data']));
    }

    public function testGetResourceData()
    {
        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        $response = $facade->createDeployment($data);

        $deployment = $response->getJson();

        $response = $facade->getResourceData($deployment['process_definition_key']);
        $this->assertEquals(200, $response->getStatusCode());

        $expected = file_get_contents($this->pathFile);
        $actual = $response->getStringBody();
        $this->assertEquals($expected, $actual);

        $facade->deleteDeployment($deployment['id']);
        $response = $facade->getResourceData($deployment['process_definition_key']);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testGetResourceData404()
    {
        $facade = new FlowableFacade();
        $response = $facade->getResourceData('test');
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testGetInstanceHistoryByProjectInstanceId()
    {
        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        $response = $facade->createDeployment($data);

        $deployment = $response->getJson();

        $response = $facade->createInstance($deployment['process_definition_key'], 'testitest');
        $instance = $response->getJson();

        $response = $facade->getInstanceHistoryByBusinessKey($instance['businessKey']);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(1, count($response->getJson()['data']));
        $this->markTestIncomplete('vérifier les données');
    }

    public function getAdminTaskByBusinessId()
    {
        // todo
        $this->markTestIncomplete();
    }

    public function testFilterInstancesWhereUserIsCurrent()
    {
        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $this->nameFile,
                'type' => mime_content_type($this->pathFile),
                'path' => $this->pathFile,
            ],
        ];

        $facade = new FlowableFacade();

        $deployment = $facade->createDeployment($data)->getJson();

        $key = 'businessKey1';

        $facade->createInstance($deployment['process_definition_key'], $key);

        $response = $facade->filterInstancesWhereUserIsCurrent(4, [$key]);

        $this->assertEquals([$key], $response->getJson());

        $response = $facade->filterInstancesWhereUserIsCurrent(5, [$key]);

        $this->assertEquals([], $response->getJson());
    }

    public function testFilterInstancesWhereUserIsCurrentWithValidatorAnywhere()
    {
        $facade = new FlowableFacade();

        // 1,2
        $nameFile = 'circuit_test-1-2.bpmn20.xml';
        $pathFile = ROOT . '/plugins/WrapperFlowable/tests/Fixture/files/xml/flowable_bpm/' . $nameFile;

        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $nameFile,
                'type' => mime_content_type($pathFile),
                'path' => $pathFile,
            ],
        ];

        $deployment = $facade->createDeployment($data)->getJson();

        $facade->createInstance($deployment['process_definition_key'], 'key1');

        // 2,1
        $nameFile = 'circuit_test-2-1.bpmn20.xml';
        $pathFile = ROOT . '/plugins/WrapperFlowable/tests/Fixture/files/xml/flowable_bpm/' . $nameFile;

        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $nameFile,
                'type' => mime_content_type($pathFile),
                'path' => $pathFile,
            ],
        ];

        $deployment = $facade->createDeployment($data)->getJson();

        $facade->createInstance($deployment['process_definition_key'], 'key2');

        // 2,1,3
        $nameFile = 'circuit_test-2-1-3.bpmn20.xml';
        $pathFile = ROOT . '/plugins/WrapperFlowable/tests/Fixture/files/xml/flowable_bpm/' . $nameFile;

        $data = [
            'deploymentKey' => 'Circuit Test',
            'deploymentName' => 'Circuit Test',
            'file' => [
                'name' => $nameFile,
                'type' => mime_content_type($pathFile),
                'path' => $pathFile,
            ],
        ];

        $deployment = $facade->createDeployment($data)->getJson();

        $facade->createInstance($deployment['process_definition_key'], 'key3');

        $response = $facade->filterInstancesWhereUserIsCurrent(1, ['key1', 'key2', 'key3']);

        //test
        $this->assertEquals(['key1', 'key2', 'key3'], $response->getJson());
    }
}
