## Mock

```
Configure::write([
    'Flowable' => [
        'wrapper_mock' => null,
```
Mettre la classe de Mock pour les tests ('\App\Test\Mock\FlowableFacadeMock' par exemple).

## Classes principales :

- Api/Flowablexception : Levée lorsque soucis de communication avec flowable ou retour incorrect
- FlowableFacade : communique avec le container flowable, passe-plat
- FlowableWrapper : conversion des réponses de  FlowableFacade, levée d'exception si retour inattendu, unique point d'entrée du plugin
- WrapperFactory : utilisé pour permettre le Mock
- InstanciableInterface : ce qui peut être envoyé dans un circuit
- WorkflowInterface : les circuits sont définis dans l'app uniquement, ils doivent juste instancier cette interface

## TODO :

- actionForward
