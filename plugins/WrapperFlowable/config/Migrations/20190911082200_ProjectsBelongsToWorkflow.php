<?php

use Migrations\AbstractMigration;

class ProjectsBelongsToWorkflow extends AbstractMigration
{
    public function change()
    {
        $this->table('projects')
            ->addColumn('workflow_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addIndex(
                [
                    'workflow_id',
                ]
            )
            ->addForeignKey(
                'workflow_id',
                'workflows',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE',
                ]
            )
            ->update();
    }
}
