<?php
use Migrations\AbstractMigration;

class LogicDeletionForWorkflows extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('workflows')
            ->removeIndex([
                'structure_id',
                'name',
            ])
            ->addColumn('deleted', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();
    }
}
