<?php

use Migrations\AbstractMigration;

class WorkflowNameDescription extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('workflows')
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('description', 'string', [
                'default' => null,
                'limit' => 500,
                'null' => true,
            ])
            ->addColumn('file', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'structure_id',
                    'file',
                ],
                ['unique' => true]
            )
            ->update();
    }
}
