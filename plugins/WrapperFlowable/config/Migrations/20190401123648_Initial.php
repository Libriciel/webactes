<?php

use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        $this->table('workflows')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('process_definition_key', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('flowable_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addIndex(
                [
                    'flowable_id',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('projects')
            ->addColumn('key', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->update();

//        $this->table('workflows')->save();
//        $this->table('projects')->save();
    }
}
