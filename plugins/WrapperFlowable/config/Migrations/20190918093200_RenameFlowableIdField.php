<?php

use Migrations\AbstractMigration;

class RenameFlowableIdField extends AbstractMigration
{
    public function change()
    {
        $this->table('workflows')
            ->renameColumn('flowable_id', 'process_instance')
            ->update();
    }
}
