<?php

use Migrations\AbstractMigration;

class ProjectsKeyBecomeFlowableInstanceId extends AbstractMigration
{
    public function change()
    {
        $this->table('projects')
            ->renameColumn('key', 'id_flowable_instance')
            ->update();
    }
}
