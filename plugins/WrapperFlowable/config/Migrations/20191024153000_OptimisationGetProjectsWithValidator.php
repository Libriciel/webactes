<?php

use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class OptimisationGetProjectsWithValidator extends AbstractMigration
{
    // @fixme: devrait être dans l'app, mais le champs projects.id_flowable_instance a été altéré dans le plugin donc plus le choix
    // à changer lors de l'externalisation du plugin
    public function change()
    {
        $this->table('projects')
            ->addIndex('id_flowable_instance', [
                'unique' => true,
            ])
            ->save();
    }
}
