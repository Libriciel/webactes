<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use LibricielRecette\Model\Enum\GenerateTemplateType;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;

class LibricielRecetteStructure4Seed extends LibricielRecetteAbstractStructureSeed
{
    protected ?int $year;

    /**
     * @var \SplFileInfo[]
     */
    protected array $paths = [];

    protected function setupOrganizations(): void
    {
        $this->save(
            $this->organizations,
            [
                [
                    'name' => 'Recette WA structure 4',
                ],
            ]
        );
    }

    protected function setupStructures(): void
    {
        $this->save(
            $this->structures,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'business_name' => 'Recette WA S4',
                    'id_orchestration' => $this->env('LIBRICIEL_RECETTE_S4_PASTELL_ID_E', null),
                    'spinner' => 'ball-clip-rotate',
                ],
            ]
        );
    }

    protected function setupStructureSettings(): void
    {
        $this->save(
            $this->structureSettings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'act',
                    'value' => '{"generate_number":false,"act_generate":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'convocation',
                    'value' => '{"convocation_idelibre":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'sitting',
                    'value' => '{"sitting_enable":true,"sitting_generate_verbal_trial_digest":true,"sitting_generate_verbal_trial_full":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'project',
                    'value' => '{"project_workflow":true,"project_generate":false,"project_writing":false}',
                ],
            ]
        );
    }

    protected function setupOfficials(): void
    {
        $this->save(
            $this->officials,
            [
            ]
        );
    }

    protected function setupUsers(): void
    {
        $this->save(
            $this->users,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S4 ADMIN',
                    'firstname' => 'Raphaël',
                    'username' => 'admin@wa-s4',
                    'email' => 'wa-s4.admin@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S4 RÉDACTEUR 1',
                    'firstname' => 'Arthur',
                    'username' => 'redac-1@wa-s4',
                    'email' => 'wa-s4.redac-1@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme',
                    'lastname' => 'S4 VALIDEUR 1',
                    'firstname' => 'Sonia',
                    'username' => 'valid-1@wa-s4',
                    'email' => 'wa-s4.valid-1@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S4 VALIDEUR 2',
                    'firstname' => 'Alphonse',
                    'username' => 'valid-2@wa-s4',
                    'email' => 'wa-s4.valid-2@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme',
                    'lastname' => 'S4 VALIDEUR 3',
                    'firstname' => 'Elodie',
                    'username' => 'valid-3@wa-s4',
                    'email' => 'wa-s4.valid-3@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S4 VALIDEUR 4',
                    'firstname' => 'Patrick',
                    'username' => 'valid-4@wa-s4',
                    'email' => 'wa-s4.valid-4@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S4 ADMINFONC',
                    'firstname' => 'Lucas',
                    'username' => 'adminfonc@wa-s4',
                    'email' => 'wa-s4.adminfonc@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme',
                    'lastname' => 'S4 SECRETAIRE 1',
                    'firstname' => 'Lucie',
                    'username' => 'secretaire-1@wa-s4',
                    'email' => 'wa-s4.secretaire-1@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S4 RÉDACTEUR 4',
                    'firstname' => 'Adam',
                    'username' => 'redac-4@wa-s4',
                    'email' => 'wa-s4.redac-4@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme',
                    'lastname' => 'S4 SECRETAIRE 2',
                    'firstname' => 'Jade',
                    'username' => 'secretaire-2@wa-s4',
                    'email' => 'wa-s4.secretaire-2@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme',
                    'lastname' => 'S4 RÉDACTEUR 2',
                    'firstname' => 'Alice',
                    'username' => 'redac-2@wa-s4',
                    'email' => 'wa-s4.redac-2@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme',
                    'lastname' => 'S4 RÉDACTEUR 3',
                    'firstname' => 'Marjorie',
                    'username' => 'redac-3@wa-s4',
                    'email' => 'wa-s4.redac-3@' . $this->domain(),
                    'data_security_policy' => false,
                ],
            ]
        );
    }

    protected function setupStructuresUsers(): void
    {
        $this->save(
            $this->structuresUsers,
            [
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'admin@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'redac-1@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-1@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-2@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-3@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-4@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'adminfonc@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'secretaire-1@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'redac-4@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'secretaire-2@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'redac-2@wa-s4')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'redac-3@wa-s4')],
            ]
        );
    }

    protected function setupNotificationsUsers(): void
    {
        $this->save(
            $this->notificationsUsers,
            [
                ['user_id' => $this->record($this->users, 'admin@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'admin@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'admin@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'admin@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'admin@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'secretaire-1@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'secretaire-1@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'secretaire-1@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'secretaire-1@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'secretaire-1@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'redac-4@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'redac-4@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'redac-4@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'redac-4@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'redac-4@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'secretaire-2@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'secretaire-2@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'secretaire-2@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'secretaire-2@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'secretaire-2@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s4'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'redac-3@wa-s4'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'redac-3@wa-s4'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'redac-3@wa-s4'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'redac-3@wa-s4'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'redac-3@wa-s4'), 'notification_id' => 5],
            ]
        );
    }

    protected function setupRoles(): void
    {
        $this->save(
            $this->roles,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur Fonctionnel',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Rédacteur / Valideur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Secrétariat général',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Super Administrateur',
                ],
            ]
        );
    }

    protected function setupRolesUsers(): void
    {
        $this->save(
            $this->rolesUsers,
            [
                ['role_id' => $this->record($this->roles, 'Administrateur'), 'user_id' => $this->record($this->users, 'admin@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'redac-1@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-1@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-2@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-3@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-4@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Administrateur Fonctionnel'), 'user_id' => $this->record($this->users, 'adminfonc@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Secrétariat général'), 'user_id' => $this->record($this->users, 'secretaire-1@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'redac-4@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Secrétariat général'), 'user_id' => $this->record($this->users, 'secretaire-2@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'redac-2@wa-s4')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'redac-3@wa-s4')],
            ]
        );
    }

    protected function setupConnecteurs(): void
    {
        $env = getenv();
        $this->save(
            $this->connecteurs,
            [
                [
                    'name' => 'pastell',
                    'url' => "{$env["LIBRICIEL_RECETTE_S4_PASTELL_URL"]}/",
                    'tdt_url' => "{$env["LIBRICIEL_RECETTE_S4_PASTELL_TDT_URL"]}/modules/actes/actes_transac_post_confirm_api.php",
                    'username' => $env['LIBRICIEL_RECETTE_S4_PASTELL_USERNAME'],
                    'password' => $env['LIBRICIEL_RECETTE_S4_PASTELL_PASSWORD'],
                    'structure_id' => $this->current($this->structures),
                    'connector_type_id' => 1,
                    'connexion_option' => $env['LIBRICIEL_RECETTE_S4_PASTELL_CONNEXION_OPTION'],
                ],
            ]
        );
    }

    protected function setupPastellfluxtypes(): void
    {
        $this->save(
            $this->pastellfluxtypes,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'actes-generique',
                    'description' => 'Actes générique',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'mailsec',
                    'description' => 'Mail securisé',
                ],
            ]
        );
    }

    protected function setupGenerateTemplates(): void
    {
        $this->save(
            $this->generateTemplates,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::ACT,
                    'name' => 'Acte délib',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::ACT,
                    'name' => 'Acte arrêté',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::DELIBERATIONS_LIST,
                    'name' => 'Liste des délibérations',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::VERBAL_TRIAL,
                    'name' => 'Procès-verbal',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::EXECUTIVE_SUMMARY,
                    'name' => 'Note de synthèse',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::CONVOCATION,
                    'name' => 'Convocation',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::PROJECT,
                    'name' => 'Projet delib',
                ],
            ]
        );
    }

    protected function setupGenerateTemplatesFiles(): void
    {
        $this->save(
            $this->files,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Acte.odt',
                    'path' => $this->paths['S4 Acte']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S4 Acte']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Acte arrêté'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Deliberation.odt',
                    'path' => $this->paths['S4 Délibération']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S4 Délibération']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Acte délib'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Convocation.odt',
                    'path' => $this->paths['S4 Convocation']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S4 Convocation']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Convocation'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Note de synthese.odt',
                    'path' => $this->paths['S4 Note de synthèse']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S4 Note de synthèse']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Note de synthèse'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Projet delibération.odt',
                    'path' => $this->paths['S4 Projet délibération']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S4 Projet délibération']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Projet delib'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Proces-verbal.odt',
                    'path' => $this->paths['S4 Procès-verbal']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S4 Procès-verbal']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Liste des deliberations.odt',
                    'path' => $this->paths['S4 Liste des délibérations']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S4 Liste des délibérations']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Liste des délibérations'),
                ],
            ]
        );
    }

    protected function setupDpos(): void
    {
        $this->save(
            $this->dpos,
            [
            ]
        );
    }

    protected function setupThemes(): void
    {
        $this->save(
            $this->themes,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Informatique',
                    'position' => '01',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Système',
                    'parent_id' => $this->id('themes', 1),
                    'position' => '010',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Infrastructure',
                    'parent_id' => $this->id('themes', 1),
                    'position' => '011',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Ressources',
                    'position' => '02',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 RH',
                    'parent_id' => $this->id('themes', 4),
                    'position' => '020',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Finances',
                    'parent_id' => $this->id('themes', 4),
                    'position' => '021',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Comptabilité',
                    'parent_id' => $this->id('themes', 6),
                    'position' => '0210',
                ],
            ]
        );
    }

    protected function setupTypesittings(): void
    {
        $this->save(
            $this->typesittings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Conseil municipal',
                    'isdeliberating' => true,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Convocation'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Commission finance',
                    'isdeliberating' => false,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Convocation'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Conseil marjo bis',
                    'isdeliberating' => true,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Convocation'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Procès-verbal'),
                ],
            ]
        );
    }

    protected function setupSequences(): void
    {
        $this->save(
            $this->sequences,
            [
                [
                    'name' => 'S4 Arrêtés non TDT',
                    'comment' => 'Pour les arrêtés non télétransmissibles',
                    'sequence_num' => 0,
                    'structure_id' => $this->current($this->structures),
                ],
                [
                    'name' => 'S4 Arrêtés TDT',
                    'comment' => 'Pour les arrêtés télétransmissibles',
                    'sequence_num' => 0,
                    'structure_id' => $this->current($this->structures),
                ],
                [
                    'name' => 'S4 Délibérations',
                    'comment' => 'Pour les délibérations',
                    'sequence_num' => 0,
                    'structure_id' => $this->current($this->structures),
                ],
            ]
        );
    }

    protected function setupCounters(): void
    {
        $this->save(
            $this->counters,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Arrêtés non TDT',
                    'counter_def' => 'NONTDT#AA##MM##JJ##000#',
                    'sequence_id' => $this->record($this->sequences, 'S4 Arrêtés non TDT'),
                    'reinit_def' => '#MM#',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Arrêtés TDT',
                    'counter_def' => 'TDT#AA##MM##JJ##000#',
                    'sequence_id' => $this->record($this->sequences, 'S4 Arrêtés TDT'),
                    'reinit_def' => '#AAAA#',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Télétravail',
                    'counter_def' => '#SSSS#',
                    'sequence_id' => $this->record($this->sequences, 'S4 Délibérations'),
                    'reinit_def' => '#JJ#',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Délibération',
                    'counter_def' => '#SSSS#',
                    'sequence_id' => $this->record($this->sequences, 'S4 Délibérations'),
                    'reinit_def' => '#AAAA#',
                ],
            ]
        );
    }

    protected function setupTypesacts(): void
    {
        $this->save(
            $this->typesacts,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AR'),
                    'name' => 'S4 Arrêté TDT',
                    'istdt' => true,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'S4 Télétravail'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Acte arrêté'),
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'DE'),
                    'name' => 'S4 Délibérations',
                    'istdt' => true,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'S4 Délibération'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Acte délib'),
                    'isdeliberating' => true,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AI'),
                    'name' => 'S4 Arrêtés non TDT',
                    'istdt' => false,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'S4 Arrêtés TDT'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Acte arrêté'),
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AU'),
                    'name' => 'S4 Décision',
                    'istdt' => true,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'S4 Arrêtés non TDT'),
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'CC'),
                    'name' => 'S4 test sans modèle projet',
                    'istdt' => true,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'S4 Arrêtés non TDT'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Acte délib'),
                    'isdeliberating' => true,
                ],
            ]
        );
    }

    protected function setupTypesactsTypesittings(): void
    {
        $this->save(
            $this->typesactsTypesittings,
            [
                ['typesact_id' => $this->record($this->typesacts, 'S4 Délibérations'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil municipal')],
                ['typesact_id' => $this->record($this->typesacts, 'S4 Délibérations'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Commission finance')],
                ['typesact_id' => $this->record($this->typesacts, 'S4 test sans modèle projet'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil municipal')],
                ['typesact_id' => $this->record($this->typesacts, 'S4 Délibérations'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil marjo bis')],
            ]
        );
    }

    protected function setupActorGroups(): void
    {
        $this->save(
            $this->actorGroups,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Invités',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Majorité',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Opposition',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S4 Commission finance',
                ],
            ]
        );
    }

    protected function setupActors(): void
    {
        $this->save(
            $this->actors,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'active' => false,
                    'civility' => 'M',
                    'lastname' => 'S4 DÉSACTIVÉ',
                    'firstname' => 'Maël',
                    'email' => 'wa-s4.acteur.desactive@' . $this->domain(),
                    'title' => 'Adjoint maire',
                    'cellphone' => '0606060606',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M',
                    'lastname' => 'S4 LEROY',
                    'firstname' => 'Noémie',
                    'email' => 'wa-s4.acteur.leroy@' . $this->domain(),
                    'title' => 'Maire',
                    'cellphone' => '0606060606',
                    'rank' => 1,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M',
                    'lastname' => 'S4 BARBE',
                    'firstname' => 'Robert',
                    'email' => 'wa-s4.acteur.barbe@' . $this->domain(),
                    'cellphone' => '0708090909',
                    'rank' => 3,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mme',
                    'lastname' => 'S4 FERNANDES',
                    'firstname' => 'Valentine',
                    'email' => 'wa-s4.acteur.fernandes@' . $this->domain(),
                    'cellphone' => '0605040302',
                    'rank' => 2,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M',
                    'lastname' => 'S4 OPPOSITION',
                    'firstname' => 'Jérémie',
                    'email' => 'wa-s4.acteur.opposition@' . $this->domain(),
                    'title' => 'représente l\'opposition',
                    'cellphone' => '0604040404',
                    'rank' => 4,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mme',
                    'lastname' => 'S4 INVITÉE',
                    'firstname' => 'Ophélie',
                    'email' => 'wa-s4.acteur.invitee@' . $this->domain(),
                    'title' => 'journaliste',
                    'cellphone' => '0605050505',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'm',
                    'lastname' => 'S4 MASSE',
                    'firstname' => 'Henri',
                    'email' => 'wa-s4.acteur.masse@' . $this->domain(),
                    'title' => 'titre acteur6',
                    'cellphone' => '',
                    'rank' => 9,
                ],
            ]
        );
    }

    protected function setupActorsActorGroups(): void
    {
        $this->save(
            $this->actorsActorGroups,
            [
                ['actor_id' => $this->record($this->actors, 'wa-s4.acteur.desactive@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S4 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s4.acteur.leroy@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S4 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s4.acteur.barbe@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S4 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s4.acteur.fernandes@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S4 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s4.acteur.opposition@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S4 Opposition')],
                ['actor_id' => $this->record($this->actors, 'wa-s4.acteur.invitee@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S4 Invités')],
                ['actor_id' => $this->record($this->actors, 'wa-s4.acteur.masse@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S4 Opposition')],
                ['actor_id' => $this->record($this->actors, 'wa-s4.acteur.leroy@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S4 Commission finance')],
                ['actor_id' => $this->record($this->actors, 'wa-s4.acteur.barbe@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S4 Commission finance')],
            ]
        );
    }

    protected function setupActorGroupsTypesittings(): void
    {
        $this->save(
            $this->actorGroupsTypesittings,
            [
                ['actor_group_id' => $this->record($this->actorGroups, 'S4 Majorité'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil municipal')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S4 Opposition'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil municipal')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S4 Majorité'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil marjo bis')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S4 Opposition'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil marjo bis')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S4 Invités'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil marjo bis')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S4 Invités'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil municipal')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S4 Commission finance'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Commission finance')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S4 Invités'), 'typesitting_id' => $this->record($this->typesittings, 'S4 Commission finance')],
            ]
        );
    }

    protected function setupDraftTemplates(): void
    {
        $this->save(
            $this->draftTemplates,
            [
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 2, 'name' => 'S4 Gabarit acte délib'],
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 2, 'name' => 'S4 Gabarit acte arrêté'],
            ]
        );
    }

    protected function setupDraftTemplatesTypesacts(): void
    {
        $this->save(
            $this->draftTemplatesTypesacts,
            [
            ]
        );
    }

    protected function setupDraftTemplatesFiles(): void
    {
        $this->save(
            $this->files,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'gabarit_delibere_libriciel.odt',
                    'path' => $this->paths['gabarit_delibere_libriciel']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['gabarit_delibere_libriciel']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'S4 Gabarit acte délib'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'gabarit_decision_libriciel.odt',
                    'path' => $this->paths['gabarit_decision_libriciel']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['gabarit_decision_libriciel']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'S4 Gabarit acte arrêté'),
                ],
            ]
        );
    }

    protected function setupSittings(): void
    {
        $this->save(
            $this->sittings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil municipal'),
                    'date' => "{$this->year}-06-10 13:55:00",
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'S4 Conseil municipal'),
                    'date' => "{$this->year}-06-02 08:30:00",
                ],
            ]
        );
    }

    protected function setupSittingsStatesittings(): void
    {
        $this->save(
            $this->sittingsStatesittings,
            [
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-06-10 13:55:00"), 'statesitting_id' => 1],
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-06-02 08:30:00"), 'statesitting_id' => 1],
            ]
        );
    }

    protected function setupWorkflows(): void
    {
        parent::run();

        $this->apiConnect('admin@wa-s4');

        $this->apiAddWorkflow(
            [
                'name' => 'S4 Circuit avec secrétaire',
                'description' => '',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Valideur 1',
                        'users' => [$this->getUserId('valid-1@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Secrétariat 1',
                        'users' => [$this->getUserId('secretaire-1@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Valideur 2',
                        'users' => [$this->getUserId('valid-2@wa-s4')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S4 Étapes simples',
                'description' => '4 étapes simples successives',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => '1',
                        'users' => [$this->getUserId('valid-1@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => '2',
                        'users' => [$this->getUserId('valid-2@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => '3',
                        'users' => [$this->getUserId('valid-3@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => '4',
                        'users' => [$this->getUserId('valid-4@wa-s4')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S4 Simple concurrent simple',
                'description' => '3 étapes dont une concurrente',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Étape 1 simple',
                        'users' => [$this->getUserId('valid-1@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Étape 2 concurrent',
                        'users' => [$this->getUserId('valid-2@wa-s4'), $this->getUserId('valid-3@wa-s4')],
                    ],
                    [
                        'groups' => 'Étape 3 simple',
                        'name' => '3',
                        'users' => [$this->getUserId('valid-4@wa-s4')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S4 Une étape concurrente 1-2',
                'description' => '',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Étape concurrente',
                        'users' => [$this->getUserId('valid-1@wa-s4'), $this->getUserId('valid-2@wa-s4')],
                    ],
                ],
            ],
            true
        );
    }

    protected function setup(): void
    {
        $this->execute('BEGIN;');
        $this->setupOrganizations();
        $this->setupStructures();
        $this->setupStructureSettings();
        $this->setupOfficials();
        $this->setupUsers();
        $this->setupStructuresUsers();
        $this->setupNotificationsUsers();
        $this->setupRoles();
        $this->setupRolesUsers();
        $this->setupConnecteurs();
        $this->setupPastellfluxtypes();
        $this->setupGenerateTemplates();
        $this->setupGenerateTemplatesFiles();
        $this->setupDpos();
        $this->setupThemes();
        $this->setupNatures();
        $this->setupMatieres();
        $this->setupClassifications();
        $this->setupTypespiecesjointes();
        $this->setupTypesittings();
        $this->setupSequences();
        $this->setupCounters();
        $this->setupTypesacts();
        $this->setupTypesactsTypesittings();
        $this->setupActorGroups();
        $this->setupActors();
        $this->setupActorsActorGroups();
        $this->setupActorGroupsTypesittings();
        $this->setupDraftTemplates();
        $this->setupDraftTemplatesTypesacts();
        $this->setupDraftTemplatesFiles();
        $this->setupSittings();
        $this->setupSittingsStatesittings();
        $this->execute('COMMIT;');

        $this->setupWorkflows();
    }

    protected function createProjects(): void
    {
        $max = $this->env('LIBRICIEL_RECETTE_S4_NOMBRE_PROJETS', 10);
        for ($idx = 1; $idx <= $max; $idx++) {
            $this->addProjectWithoutGeneration(
                $this->current($this->organizations),
                $this->current($this->structures),
                $this->record($this->users, 'admin@wa-s4'),
                $this->record($this->themes, 'S4 Informatique'),
                $this->record($this->typesacts, 'S4 Délibérations'),
                sprintf('S4_%d_DE_%04d', date('Y'), $idx),
                dirname(__DIR__, 2) . DS . 'files' . DS . 'Projects' . DS . 'Projet S4.pdf'
            );
        }
    }

    public function run(): void
    {
        parent::run();

        $this->sequences();

        $this->year = (int)date('Y');

        $dirs = [
            'GenerateTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesGenerateTemplates' . DS,
            'DraftTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesDraftTemplates' . DS,
        ];
        $this->paths = [
            //'S4 Projet arrêté' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Projet arrete.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 1)),
            'S4 Délibération' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Deliberation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 1)),
            'S4 Acte' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Acte.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 2)),
            'S4 Liste des délibérations' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Liste des deliberations.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 3)),
            'S4 Procès-verbal' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Proces-verbal.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 4)),
            'S4 Note de synthèse' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Note de synthese.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 5)),
            'S4 Convocation' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Convocation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 6)),
            'S4 Projet délibération' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Projet deliberation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 7)),
            //------------------------------------------------------------------------------------------------------------------
            'gabarit_delibere_libriciel' => $this->createFile($dirs['DraftTemplates'] . 'gabarit_delibere_libriciel.odt', 'draft_template_id', 1, $this->id('structures', 1), $this->id('draft_templates', 1)),
            'gabarit_decision_libriciel' => $this->createFile($dirs['DraftTemplates'] . 'gabarit_decision_libriciel.odt', 'draft_template_id', 1, $this->id('structures', 1), $this->id('draft_templates', 2)),
        ];

        $this->setup();

        $this->sequences();
        $this->createProjects();
        $this->sequences();

        $this->permissions();
    }
}
