<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use Cake\Core\Plugin;
use LibricielRecette\Model\Enum\GenerateTemplateType;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;
use LibricielRecette\Seeds\LibricielRecetteMassiveDataSeedCommonTrait;
use LibricielRecette\Seeds\LibricielRecetteSeedGenerateTemplatesTrait;
use LibricielRecette\Seeds\LibricielRecetteSeedWorkflowTrait;

class LibricielRecetteMassiveDataSeed extends LibricielRecetteAbstractStructureSeed
{
    use LibricielRecetteMassiveDataSeedCommonTrait;
    use LibricielRecetteSeedGenerateTemplatesTrait;

    protected ?int $year;

    /**
     * @var \SplFileInfo[]
     */
    protected array $paths = [];

    protected ?int $organizationIdx = null;
    protected ?string $organizationName = null;

    protected ?int $structureIdx = null;
    protected ?string $structureName = null;

    protected ?string $pastellEnvPrefix = null;

    /**
     * Création du super-administrateur par défaut de la structure
     */
    protected function setupStructureSuperadmin(): void
    {
        $username = $this->getSuperadminUsername($this->organizationIdx, $this->structureIdx);

        $this->save(
            $this->users,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => sprintf('O-%05d S-%05d', $this->organizationIdx, $this->structureIdx),
                    'firstname' => 'Super-administratrice',
                    'username' => $username,
                    'email' => $this->getSuperadminEmail($this->organizationIdx, $this->structureIdx),
                ],
            ]
        );

        $this->save(
            $this->structuresUsers,
            [
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, $username)],
            ]
        );

        // NotificationsUsers (toutes désactivées)
        $data = [];
        for ($notificationId = 1; $notificationId <= 5; $notificationId++) {
            $data[] = [
                'user_id' => $this->record($this->users, $username),
                'notification_id' => $notificationId,
                'active' => false,
            ];
        }
        $this->save($this->notificationsUsers, $data);

        // RolesUsers
        $this->save(
            $this->rolesUsers,
            [
                [
                    'role_id' => $this->record($this->roles, 'Super Administrateur'),
                    'user_id' => $this->record($this->users, $username),
                ],
            ]
        );
    }

    protected function setupOrganizations(): void
    {
        $this->save(
            $this->organizations,
            [
                [
                    'active' => true,
                    'name' => $this->organizationName,
                ],
            ]
        );
    }

    protected function setupStructures(): void
    {
        $this->save(
            $this->structures,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'business_name' => $this->structureName,
                    'id_orchestration' => $env[$this->pastellEnvPrefix . '_ID_E'] ?? $this->structureIdx,
                ],
            ]
        );
    }

    protected function setupStructureSettings(): void
    {
        $this->save(
            $this->structureSettings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'project',
                    'value' => '{"project_generate":true,"project_workflow":true,"project_writing":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'act',
                    'value' => '{"act_generate":true,"generate_number":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'convocation',
                    'value' => '{"convocation_idelibre":true,"convocation_mailsec":false}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'sitting',
                    'value' => '{"sitting_enable":true,"sitting_generate_deliberations_list":true,"sitting_generate_verbal_trial":true}',
                ],
            ]
        );
    }

    protected function setupOfficials(): void
    {
        $this->save(
            $this->officials,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => $this->structureName,
                    'firstname' => 'Officiel',
                    'email' => 'contact@libriciel.coop',
                    'address' => '140 Rue Aglaonice de Thessalie',
                    'address_supplement' => '',
                    'post_code' => '34170',
                    'city' => 'Castelnau-le-Lez',
                    'phone' => '04 67 65 96 44',
                ],
            ]
        );
    }

    protected function setupRoles(): void
    {
        $this->save(
            $this->roles,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur Fonctionnel',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Rédacteur / Valideur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Secrétariat général',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Super Administrateur',
                ],
            ]
        );
    }

    protected function setupUsers(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        foreach ($this->records['roles'] as $roleName => $roleId) {
            for ($user = 1; $user <= $max['users']; $user++) {
                $data[] = [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => sprintf('O-%05d S-%05d', $this->organizationIdx, $this->structureIdx),
                    'firstname' => sprintf('%s %05d', $roleName, $user),
                    'username' => $this->getUsername($this->organizationIdx, $this->structureIdx, $roleName, $user),
                    'email' => $this->getUserEmail($this->organizationIdx, $this->structureIdx, $roleName, $user),
                ];
            }
        }

        $this->save($this->users, $data);
    }

    protected function setupStructuresUsers(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        foreach ($this->records['roles'] as $roleName => $roleId) {
            for ($user = 1; $user <= $max['users']; $user++) {
                $username = $this->getUsername($this->organizationIdx, $this->structureIdx, $roleName, $user);
                $data[] = [
                    'structure_id' => $this->current($this->structures),
                    'user_id' => $this->record($this->users, $username),
                ];
            }
        }

        $this->save($this->structuresUsers, $data);
    }

    protected function setupNotificationsUsers(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        foreach ($this->records['roles'] as $roleName => $roleId) {
            for ($user = 1; $user <= $max['users']; $user++) {
                $username = $this->getUsername($this->organizationIdx, $this->structureIdx, $roleName, $user);
                for ($notificationId = 1; $notificationId <= 5; $notificationId++) {
                    $data[] = [
                        'user_id' => $this->record($this->users, $username),
                        'notification_id' => $notificationId,
                        'active' => false,
                    ];
                }
            }
        }

        $this->save($this->notificationsUsers, $data);
    }

    protected function setupRolesUsers(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        foreach ($this->records['roles'] as $roleName => $roleId) {
            for ($user = 1; $user <= $max['users']; $user++) {
                $username = $this->getUsername($this->organizationIdx, $this->structureIdx, $roleName, $user);
                $data[] = [
                    'role_id' => $this->record($this->roles, $roleName),
                    'user_id' => $this->record($this->users, $username),
                ];
            }
        }

        $this->save($this->rolesUsers, $data);
    }

    protected function setupConnecteurs(): void
    {
        $env = getenv();
        $vars = [
            'URL' => $env[$this->pastellEnvPrefix . '_URL'] ?? 'https://pastell.partenaire.libriciel.fr',
            'TDT_URL' => $env[$this->pastellEnvPrefix . '_TDT_URL'] ?? 'https://s2low.partenaires.libriciel.fr',
            'USERNAME' => $env[$this->pastellEnvPrefix . '_USERNAME'] ?? 'username',
            'PASSWORD' => $env[$this->pastellEnvPrefix . '_PASSWORD'] ?? 'password',
            'CONNEXION_OPTION' => $env[$this->pastellEnvPrefix . '_CONNEXION_OPTION'] ?? 1,
        ];

        $data = [
            [
                'name' => 'pastell',
                'url' => "{$vars['URL']}/",
                'tdt_url' => "{$vars['TDT_URL']}/modules/actes/actes_transac_post_confirm_api.php",
                'username' => $vars['USERNAME'],
                'password' => $vars['PASSWORD'],
                'structure_id' => $this->current($this->structures),
                'connector_type_id' => 1,
                'connexion_option' => $vars['CONNEXION_OPTION'],
            ],
        ];

        $this->save($this->connecteurs, $data);
    }

    protected function setupPastellfluxtypes(): void
    {
        $this->save(
            $this->pastellfluxtypes,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'actes-generique',
                    'description' => 'Actes générique',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'mailsec',
                    'description' => 'Mail securisé',
                ],
            ]
        );
    }

    protected function setupGenerateTemplates(): void
    {
        $generateTemplates = [
            'Modèle de projet d\'arrêté' => GenerateTemplateType::PROJECT,
            'Modèle de projet de délibération' => GenerateTemplateType::PROJECT,
            'Modèle de convocation' => GenerateTemplateType::CONVOCATION,
            'Modèle de note de synthèse' => GenerateTemplateType::EXECUTIVE_SUMMARY,
            'Modèle de liste des délibérations' => GenerateTemplateType::DELIBERATIONS_LIST,
            'Modèle de procès-verbal' => GenerateTemplateType::VERBAL_TRIAL,
            'Modèle d\'arrêté' => GenerateTemplateType::ACT,
            'Modèle de délibération' => GenerateTemplateType::ACT,
        ];

        $data = [];
        foreach ($generateTemplates as $name => $generateTemplateType) {
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'generate_template_type_id' => $generateTemplateType,
                'name' => $name,
            ];
        }

        $this->save($this->generateTemplates, $data);
    }

    protected function setupGenerateTemplatesFiles(): void
    {
        $dir = Plugin::path('LibricielRecette') . DS . 'files' . DS . 'FilesGenerateTemplates' . DS;
        $generateTemplateFiles = [
            'Modèle de projet d\'arrêté' => 'Projet arrete',
            'Modèle de projet de délibération' => 'Projet deliberation',
            'Modèle de convocation' => 'Convocation',
            'Modèle de note de synthèse' => 'Note de synthese',
            'Modèle de liste des délibérations' => 'Liste des deliberations',
            'Modèle de procès-verbal' => 'Proces-verbal',
            'Modèle d\'arrêté' => 'Acte',
            'Modèle de délibération' => 'Deliberation',
        ];
        $data = [];
        foreach ($generateTemplateFiles as $name => $filename) {
            $file = $this->createFile(
                "{$dir}{$filename}.odt",
                'generate_template_id',
                $this->current($this->organizations),
                $this->current($this->structures),
                $this->record($this->generateTemplates, $name)
            );

            $data[] = [
                'structure_id' => $this->current($this->structures),
                'name' => "{$filename}.odt",
                'path' => $file->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $file->getSize(),
                'generate_template_id' => $this->record($this->generateTemplates, $name),
            ];
        }

        $this->save($this->files, $data);
    }

    protected function setupDpos(): void
    {
        $prefix = sprintf('O-%05d S-%05d', $this->organizationIdx, $this->structureIdx);
        $this->save(
            $this->dpos,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => "{$prefix} DPO",
                    'firstname' => 'Théo',
                    'email' => sprintf('wa-%s.dpo@%s', str_replace(' ', '-', mb_strtolower(trim($prefix))), $this->domain()),
                ],
            ]
        );
    }

    protected function setupThemes(): void
    {
        $max = $this->getConfiguration();

        // ($max['themes'])
        $data = [];
        for ($theme = 1; $theme <= $max['themes']; $theme++) {
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'name' => sprintf('Thème %d', $theme),
                'parent_id' => null,
                'position' => sprintf('%05d', $theme),
            ];
        }
        $this->save($this->themes, $data);

        // Sous-themes ($max['themes'], limité à une longueur de 60) pour le premier thème inséré
        if ($max['themes'] > 0) {
            $parentId = $this->record($this->themes, 'Thème 1');
            for ($theme = 1; $theme <= $max['themes']; $theme++) {
                $themeName = sprintf('Thème 1.%s', implode('.', array_fill(0, $theme, '1')));
                if (mb_strlen($themeName) <= 60) { // La longueur maximale du champ en base de données
                    $data = [
                        [
                            'structure_id' => $this->current($this->structures),
                            'name' => $themeName,
                            'parent_id' => $parentId,
                            'position' => sprintf('00001-%s', implode('-', array_fill(0, $theme, '1'))),
                        ],
                    ];
                    $this->save($this->themes, $data);
                    $parentId = $this->current($this->themes);
                }
            }
        }
    }

    protected function setupTypesittings(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($typesitting = 1; $typesitting <= $max['typesittings']; $typesitting++) {
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'name' => sprintf('Séance délibérante activée %05d', $typesitting),
                'isdeliberating' => true,
                'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
            ];
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'name' => sprintf('Séance non délibérante activée %05d', $typesitting),
                'isdeliberating' => false,
                'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
            ];
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'name' => sprintf('Séance délibérante désactivée %05d', $typesitting),
                'active' => false,
                'isdeliberating' => true,
                'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
            ];
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'name' => sprintf('Séance non délibérante désactivée %05d', $typesitting),
                'active' => false,
                'isdeliberating' => false,
                'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
            ];
        }

        $this->save($this->typesittings, $data);
    }

    protected function setupSequences(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($sequence = 1; $sequence <= $max['sequences']; $sequence++) {
            $data[] = [
                'name' => sprintf('Séquence %05d', $sequence),
                'comment' => '',
                'sequence_num' => 0,
                'structure_id' => $this->current($this->structures),
            ];
        }
        $this->save($this->sequences, $data);
    }

    protected function setupCounters(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($counter = 1; $counter <= $max['counters']; $counter++) {
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'name' => sprintf('Compteur %05d', $counter),
                'counter_def' => 'CNT_#SSSS#',
                'sequence_id' => $this->current($this->sequences),
                'reinit_def' => '#AAAA#',
            ];
        }
        $this->save($this->counters, $data);
    }

    protected function setupTypesacts(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($typeact = 1; $typeact <= $max['typesacts']; $typeact++) {
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'nature_id' => $this->id('natures', 5),
                'name' => sprintf('Délibération %05d', $typeact),
                'istdt' => true,
                'active' => true,
                'isdefault' => true,
                'counter_id' => $this->current($this->counters),
                'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet de délibération'),
                'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle de délibération'),
                'isdeliberating' => true,
            ];
        }
        $this->save($this->typesacts, $data);
    }

    protected function setupTypesactsTypesittings(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($typeact = 1; $typeact <= $max['typesacts']; $typeact++) {
            $data[] = [
                'typesact_id' => $this->record($this->typesacts, sprintf('Délibération %05d', $typeact)),
                'typesitting_id' => $this->current($this->typesittings),
            ];
        }
        $this->save($this->typesactsTypesittings, $data);
    }

    protected function setupActorGroups(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($actorGroup = 1; $actorGroup <= $max['actor_groups']; $actorGroup++) {
            $data[] = [
                'organization_id' => $this->current($this->organizations),
                'structure_id' => $this->current($this->structures),
                'name' => sprintf('Groupe d\'acteur %05d', $actorGroup),
                'active' => true,
            ];
        }
        $this->save($this->actorGroups, $data);
    }

    protected function setupActors(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($actor = 1; $actor <= $max['actors']; $actor++) {
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'active' => true,
                'civility' => 'M.',
                'lastname' => sprintf('O-%05d S-    %05d', $this->organizationIdx, $this->structureIdx),
                'firstname' => sprintf('Acteur %05d', $actor),
                'email' => $this->getActorEmail($this->organizationIdx, $this->structureIdx, $actor),
                'rank' => $actor,
            ];
        }
        $this->save($this->actors, $data);
    }

    protected function setupActorsActorGroups(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($actor = 1; $actor <= $max['actors']; $actor++) {
            $data[] = [
                'actor_id' => $this->record($this->actors, $this->getActorEmail($this->organizationIdx, $this->structureIdx, $actor)),
                'actor_group_id' => $this->current($this->actorGroups),
            ];
        }
        $this->save($this->actorsActorGroups, $data);
    }

    protected function setupActorGroupsTypesittings(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($actorGroup = 1; $actorGroup <= $max['actor_groups']; $actorGroup++) {
            $data[] = [
                'actor_group_id' => $this->record($this->actorGroups, sprintf('Groupe d\'acteur %05d', $actorGroup)),
                'typesitting_id' => $this->current($this->typesittings),
            ];
        }
        $this->save($this->actorGroupsTypesittings, $data);
    }

    protected function setupDraftTemplates(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($draftTemplate = 1; $draftTemplate <= $max['draft_templates']; $draftTemplate++) {
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'draft_template_type_id' => 1,
                'name' => sprintf('O%05d S%05d Projet %05d', $this->organizationIdx, $this->structureIdx, $draftTemplate),
            ];
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'draft_template_type_id' => 2,
                'name' => sprintf('O%05d S%05d Acte %05d', $this->organizationIdx, $this->structureIdx, $draftTemplate),
            ];
        }
        $this->save($this->draftTemplates, $data);
    }

    protected function setupDraftTemplatesTypesacts(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        for ($draftTemplate = 1; $draftTemplate <= $max['draft_templates']; $draftTemplate++) {
            $data[] = [
                'draft_template_id' => $this->record($this->draftTemplates, sprintf('O%05d S%05d Projet %05d', $this->organizationIdx, $this->structureIdx, $draftTemplate)),
                'typesact_id' => $this->current($this->typesacts),
            ];
            $data[] = [
                'draft_template_id' => $this->record($this->draftTemplates, sprintf('O%05d S%05d Acte %05d', $this->organizationIdx, $this->structureIdx, $draftTemplate)),
                'typesact_id' => $this->current($this->typesacts),
            ];
        }
        $this->save($this->draftTemplatesTypesacts, $data);
    }

    protected function setupDraftTemplatesFiles(): void
    {
        $max = $this->getConfiguration();
        $dir = Plugin::path('LibricielRecette') . DS . 'files' . DS . 'FilesDraftTemplates' . DS;

        $data = [];
        for ($draftTemplate = 1; $draftTemplate <= $max['draft_templates']; $draftTemplate++) {
            $files = [
                'texte_delib2' => $this->createFile(
                    $dir . 'texte_delib2.odt',
                    'draft_template_id',
                    $this->current($this->organizations),
                    $this->current($this->structures),
                    $this->record($this->draftTemplates, sprintf('O%05d S%05d Projet %05d', $this->organizationIdx, $this->structureIdx, $draftTemplate))
                ),
                'texte_projet' => $this->createFile(
                    $dir . 'texte_projet.odt',
                    'draft_template_id',
                    $this->current($this->organizations),
                    $this->current($this->structures),
                    $this->record($this->draftTemplates, sprintf('O%05d S%05d Acte %05d', $this->organizationIdx, $this->structureIdx, $draftTemplate))
                ),
            ];
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'name' => 'texte_delib2.odt',
                'path' => $files['texte_delib2']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['texte_delib2']->getSize(),
                'draft_template_id' => $this->record($this->draftTemplates, sprintf('O%05d S%05d Projet %05d', $this->organizationIdx, $this->structureIdx, $draftTemplate)),
            ];
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'name' => 'texte_projet.odt',
                'path' => $files['texte_projet']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['texte_projet']->getSize(),
                'draft_template_id' => $this->record($this->draftTemplates, sprintf('O%05d S%05d Acte %05d', $this->organizationIdx, $this->structureIdx, $draftTemplate)),
            ];
        }

        $this->save($this->files, $data);
    }

    protected function setupSittings(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        $start = mktime(0, 0, 0, 1, 1, (int)date('Y'));
        for ($sitting = 1; $sitting <= $max['sittings']; $sitting++) {
            $data[] = [
                'structure_id' => $this->current($this->structures),
                'typesitting_id' => $this->record($this->typesittings, 'Séance délibérante activée 00001'),
                'date' => date('Y-m-d H:i:s', $start + 60 * ($sitting - 1)),
            ];
        }

        $this->save($this->sittings, $data);
    }

    protected function setupSittingsStatesittings(): void
    {
        $max = $this->getConfiguration();

        $data = [];
        $start = mktime(0, 0, 0, 1, 1, (int)date('Y'));
        for ($sitting = 1; $sitting <= $max['sittings']; $sitting++) {
            $data[] = [
                'sitting_id' => $this->record($this->sittings, date('Y-m-d H:i:s', $start + 60 * ($sitting - 1))),
                'statesitting_id' => 1,
            ];
        }

        $this->save($this->sittingsStatesittings, $data);
    }

    protected function setupWorkflows(): void
    {
        $max = $this->getConfiguration();

        if ($max['workflows'] > 0) {
            $this->output->write(sprintf('Création des circuits pour la structure %s', $this->structureName), true);

            $username = $this->getSuperadminUsername($this->organizationIdx, $this->structureIdx);

            $this->apiConnect($username);
            for ($circuit = 1; $circuit <= $max['workflows']; $circuit++) {
                $name = sprintf('O-%05d S-%05d Circuit %05d', $this->organizationIdx, $this->structureIdx, $circuit);

                $this->apiAddWorkflow(
                    [
                        'name' => $name,
                        'description' => '',
                        'steps' => [
                            [
                                'groups' => '',
                                'name' => sprintf('Simple: %s', $username),
                                'users' => [$this->getUserId($username)],
                            ],
                        ],
                    ],
                    true
                );
            }
        }
    }

    protected function massive(): void
    {
        $max = $this->getConfiguration();

        for ($this->organizationIdx = 1; $this->organizationIdx <= $max['organizations']; $this->organizationIdx++) {
            // $this->records = []; // @todo: pas nécessaire ?

            $this->organizationName = sprintf('Organisation %05d', $this->organizationIdx);
            $this->execute('BEGIN;');
            $this->setupOrganizations();
            $this->execute('COMMIT;');

            for ($this->structureIdx = 1; $this->structureIdx <= $max['structures']; $this->structureIdx++) {
                $this->structureName = sprintf('Organisation %05d, structure %05d', $this->organizationIdx, $this->structureIdx);
                $this->output->write(sprintf('Création de la structure %s', $this->structureName), true);

                $this->loadIds();

                $this->execute('BEGIN;');

                $this->pastellEnvPrefix = sprintf('LIBRICIEL_RECETTE_MASSIVE_O_%05d_S_%05d_PASTELL', $this->organizationIdx, $this->structureIdx);
                $this->setupStructures();
                $this->setupStructureSettings();

                $this->setupOfficials();

                // @todo: changer l'ordre dans les générateur + S1 à S4
                $this->setupRoles();

                // 1. Création du superadministrateur par défaut de la structure
                $this->setupStructureSuperadmin();

                // 2. Création des utilisateurs de la structure
                if ($max['users'] > 0) {
                    $this->setupUsers();
                    $this->setupStructuresUsers();
                    $this->setupNotificationsUsers();
                    $this->setupRolesUsers();
                }

                $this->setupConnecteurs();
                $this->setupPastellfluxtypes();
                $this->setupGenerateTemplates();
                $this->setupGenerateTemplatesFiles();
                $this->setupDpos();
                $this->setupThemes();
                $this->setupNatures();
                $this->setupMatieres();
                // @fixme: bugge quand on le lance plusieurs fois (après avoir supprimé les données) ?
                $this->setupClassifications();
                $this->setupTypespiecesjointes();
                $this->setupTypesittings();
                $this->setupSequences();
                $this->setupCounters();
                $this->setupTypesacts();
                $this->setupTypesactsTypesittings();
                $this->setupActorGroups();
                $this->setupActors();
                $this->setupActorsActorGroups();
                $this->setupActorGroupsTypesittings();
                $this->setupDraftTemplates();
                $this->setupDraftTemplatesTypesacts();
                $this->setupDraftTemplatesFiles();
                $this->setupSittings();
                $this->setupSittingsStatesittings();

                $this->execute('COMMIT;');

                $this->sequences();

                $this->setupWorkflows();

                $this->createProjects();
            }
        }
    }

    protected function createProjects(): void
    {
        $max = $this->getConfiguration();

        for ($idx = 1; $idx <= $max['projects']; $idx++) {
            $this->addProjectWithGeneration(
                $this->current($this->organizations),
                $this->current($this->structures),
                $this->record($this->users, $this->getSuperadminUsername($this->organizationIdx, $this->structureIdx)),
                $this->record($this->themes, 'Thème 1'),
                $this->record($this->typesacts, 'Délibération 00001'),
                $this->record($this->draftTemplates, sprintf('O%05d S%05d Projet 00001', $this->organizationIdx, $this->structureIdx)),
                $this->record($this->draftTemplates, sprintf('O%05d S%05d Acte 00001', $this->organizationIdx, $this->structureIdx)),
            );
        }
    }

    public function run(): void
    {
        parent::run();

        $this->year = (int)date('Y');

        $this->sequences();
        $this->massive();
        $this->sequences();

        $this->permissions();
    }
}
