<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use Cake\Utility\Hash;
use LibricielRecette\Model\Enum\GenerateTemplateType;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;
use LibricielRecette\Seeds\LibricielRecetteSeedGenerateTemplatesTrait;

/**
 * @deprecated
 */
class LibricielRecetteOldStructure1Seed extends LibricielRecetteAbstractStructureSeed
{
    use LibricielRecetteSeedGenerateTemplatesTrait;

    /**
     * Crée le paramétrage de la structure 1
     *
     * @return void
     */
    protected function setup(): void
    {
        // @info: {$_ENV["..."]} not set
        $env = getenv();
        $year = date('Y');

        // Organizations
        $data = [
            [
                'id' => $this->id('organizations', 1),
                'name' => 'Recette WA structure 1',
            ],
        ];
        $this->organizations->insert($data);

        $dirs = [
            'GenerateTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesGenerateTemplates' . DS,
            'DraftTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesDraftTemplates' . DS,
        ];
        $files = [
            'S1 Projet arrêté' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Projet arrete.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 1)),
            'S1 Projet délibération' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Projet deliberation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 2)),
            'S1 Convocation' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Convocation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 3)),
            'S1 Note de synthèse' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Note de synthese.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 4)),
            'S1 Liste des délibérations' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Liste des deliberations.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 5)),
            'S1 Procès-verbal' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Proces-verbal.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 6)),
            'S1 Acte' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Acte.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 7)),
            'S1 Délibération' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Deliberation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 8)),
            //------------------------------------------------------------------------------------------------------------------
            'gabarit_arrete_libriciel' => $this->createFile($dirs['DraftTemplates'] . 'gabarit_arrete_libriciel.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 1)),
            'gabarit_projet_libriciel' => $this->createFile($dirs['DraftTemplates'] . 'gabarit_projet_libriciel.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 2)),
            'texte_delib2' => $this->createFile($dirs['DraftTemplates'] . 'texte_delib2.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 3)),
            'texte_projet' => $this->createFile($dirs['DraftTemplates'] . 'texte_projet.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 4)),
        ];

        // Structures
        $data = [
            [
                'id' => $this->id('structures', 1),
                'organization_id' => $this->id('organizations', 1),
                'business_name' => 'Recette WA S1',
                'id_orchestration' => $env['LIBRICIEL_RECETTE_S1_PASTELL_ID_E'],
            ],
        ];
        $this->structures->insert($data);

        // StructureSettings
        $data = [
            [
                'id' => $this->id('structure_settings', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'project',
                'value' => '{"project_generate":true,"project_workflow":true,"project_writing":true}',
            ],
            [
                'id' => $this->id('structure_settings', 2),
                'structure_id' => $this->id('structures', 1),
                'name' => 'act',
                'value' => '{"act_generate":true,"generate_number":true}',
            ],
            [
                'id' => $this->id('structure_settings', 3),
                'structure_id' => $this->id('structures', 1),
                'name' => 'convocation',
                'value' => '{"convocation_idelibre":true,"convocation_mailsec":false}',
            ],
            [
                'id' => $this->id('structure_settings', 4),
                'structure_id' => $this->id('structures', 1),
                'name' => 'sitting',
                'value' => '{"sitting_enable":true,"sitting_generate_deliberations_list":true,"sitting_generate_verbal_trial":true}',
            ],
        ];
        $this->structureSettings->insert($data);

        // Officials
        $data = [
            [
                'id' => $this->id('officials', 1),
                'structure_id' => $this->id('structures', 1),
                'civility' => 'M.',
                'lastname' => 'S1 LOSSERAND',
                'firstname' => 'Frédéric',
                'email' => 'contact@libriciel.coop',
                'address' => '140 Rue Aglaonice de Thessalie',
                'address_supplement' => '',
                'post_code' => '34170',
                'city' => 'Castelnau-le-Lez',
                'phone' => '04 67 65 96 44',
            ],
        ];
        $this->officials->insert($data);

        // Users
        $domain = env('LIBRICIEL_RECETTE_DEFAULT_EMAIL_DOMAIN', 'mailcatchall.libriciel.net');
        $data = [
            [
                'id' => $this->id('users', 1),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme.',
                'lastname' => 'S1 ADMIN',
                'firstname' => 'Josette',
                'username' => 'admin@wa-s1',
                'email' => 'wa-s1.admin@' . $domain,
            ],
            [
                'id' => $this->id('users', 2),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S1 ADMINFONC',
                'firstname' => 'Albert',
                'username' => 'adminfonc@wa-s1',
                'email' => 'wa-s1.adminfonc@' . $domain,
            ],
            [
                'id' => $this->id('users', 3),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S1 RÉDAC 1',
                'firstname' => 'Lucien',
                'username' => 'redac-1@wa-s1',
                'email' => 'wa-s1.redac-1@' . $domain,
            ],
            [
                'id' => $this->id('users', 4),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme.',
                'lastname' => 'S1 RÉDAC 2',
                'firstname' => 'Léonie',
                'username' => 'redac-2@wa-s1',
                'email' => 'wa-s1.redac-2@' . $domain,
            ],
            [
                'id' => $this->id('users', 5),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S1 VALIDEUR 1',
                'firstname' => 'Jean-Louis',
                'username' => 'valid-1@wa-s1',
                'email' => 'wa-s1.valid-1@' . $domain,
            ],
            [
                'id' => $this->id('users', 6),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S1 VALIDEUR 2',
                'firstname' => 'Paul-Emile',
                'username' => 'valid-2@wa-s1',
                'email' => 'wa-s1.valid-2@' . $domain,
            ],
            [
                'id' => $this->id('users', 7),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme.',
                'lastname' => 'S1 VALIDEUR 3',
                'firstname' => 'Marie-Odile',
                'username' => 'valid-3@wa-s1',
                'email' => 'wa-s1.valid-3@' . $domain,
            ],
            [
                'id' => $this->id('users', 8),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme.',
                'lastname' => 'S1 VALIDEUR 4',
                'firstname' => 'Juliette',
                'username' => 'valid-4@wa-s1',
                'email' => 'wa-s1.valid-4@' . $domain,
            ],
            [
                'id' => $this->id('users', 9),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme.',
                'lastname' => 'S1 VALIDEUR 5',
                'firstname' => 'Monique',
                'username' => 'valid-5@wa-s1',
                'email' => 'wa-s1.valid-5@' . $domain,
            ],
            [
                'id' => $this->id('users', 10),
                'organization_id' => $this->id('organizations', 1),
                'active' => false,
                'civility' => 'M.',
                'lastname' => 'S1 VALIDEUR 6',
                'firstname' => 'François',
                'username' => 'valid-6@wa-s1',
                'email' => 'wa-s1.valid-6@' . $domain,
            ],
            [
                'id' => $this->id('users', 11),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M',
                'lastname' => 'S1 SECRETAIRE',
                'firstname' => 'Jeannot',
                'username' => 'secretaire@wa-s1',
                'email' => 'wa-s1.secretaire-1@' . $domain,
            ],
        ];
        $this->users->insert($data);

        // StructuresUsers
        $data = [
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 1)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 2)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 3)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 4)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 5)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 6)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 7)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 8)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 9)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 10)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 11)],
        ];
        $this->structuresUsers->insert($data);

        // NotificationsUsers
        $data = [
            ['user_id' => $this->id('users', 1), 'notification_id' => 1],
            ['user_id' => $this->id('users', 1), 'notification_id' => 2],
            ['user_id' => $this->id('users', 1), 'notification_id' => 3],
            ['user_id' => $this->id('users', 1), 'notification_id' => 4],
            ['user_id' => $this->id('users', 1), 'notification_id' => 5],
            ['user_id' => $this->id('users', 2), 'notification_id' => 1],
            ['user_id' => $this->id('users', 2), 'notification_id' => 2],
            ['user_id' => $this->id('users', 2), 'notification_id' => 3],
            ['user_id' => $this->id('users', 2), 'notification_id' => 4],
            ['user_id' => $this->id('users', 2), 'notification_id' => 5],
            ['user_id' => $this->id('users', 3), 'notification_id' => 1],
            ['user_id' => $this->id('users', 3), 'notification_id' => 2],
            ['user_id' => $this->id('users', 3), 'notification_id' => 3],
            ['user_id' => $this->id('users', 3), 'notification_id' => 4],
            ['user_id' => $this->id('users', 3), 'notification_id' => 5],
            ['user_id' => $this->id('users', 4), 'notification_id' => 1],
            ['user_id' => $this->id('users', 4), 'notification_id' => 2],
            ['user_id' => $this->id('users', 4), 'notification_id' => 3],
            ['user_id' => $this->id('users', 4), 'notification_id' => 4],
            ['user_id' => $this->id('users', 4), 'notification_id' => 5],
            ['user_id' => $this->id('users', 5), 'notification_id' => 1],
            ['user_id' => $this->id('users', 5), 'notification_id' => 2],
            ['user_id' => $this->id('users', 5), 'notification_id' => 3],
            ['user_id' => $this->id('users', 5), 'notification_id' => 4],
            ['user_id' => $this->id('users', 5), 'notification_id' => 5],
            ['user_id' => $this->id('users', 6), 'notification_id' => 1],
            ['user_id' => $this->id('users', 6), 'notification_id' => 2],
            ['user_id' => $this->id('users', 6), 'notification_id' => 3],
            ['user_id' => $this->id('users', 6), 'notification_id' => 4],
            ['user_id' => $this->id('users', 6), 'notification_id' => 5],
            ['user_id' => $this->id('users', 7), 'notification_id' => 1],
            ['user_id' => $this->id('users', 7), 'notification_id' => 2],
            ['user_id' => $this->id('users', 7), 'notification_id' => 3],
            ['user_id' => $this->id('users', 7), 'notification_id' => 4],
            ['user_id' => $this->id('users', 7), 'notification_id' => 5],
            ['user_id' => $this->id('users', 8), 'notification_id' => 1],
            ['user_id' => $this->id('users', 8), 'notification_id' => 2],
            ['user_id' => $this->id('users', 8), 'notification_id' => 3],
            ['user_id' => $this->id('users', 8), 'notification_id' => 4],
            ['user_id' => $this->id('users', 8), 'notification_id' => 5],
            ['user_id' => $this->id('users', 9), 'notification_id' => 1],
            ['user_id' => $this->id('users', 9), 'notification_id' => 2],
            ['user_id' => $this->id('users', 9), 'notification_id' => 3],
            ['user_id' => $this->id('users', 9), 'notification_id' => 4],
            ['user_id' => $this->id('users', 9), 'notification_id' => 5],
            ['user_id' => $this->id('users', 10), 'notification_id' => 1],
            ['user_id' => $this->id('users', 10), 'notification_id' => 2],
            ['user_id' => $this->id('users', 10), 'notification_id' => 3],
            ['user_id' => $this->id('users', 10), 'notification_id' => 4],
            ['user_id' => $this->id('users', 10), 'notification_id' => 5],
            ['user_id' => $this->id('users', 11), 'notification_id' => 1],
            ['user_id' => $this->id('users', 11), 'notification_id' => 2],
            ['user_id' => $this->id('users', 11), 'notification_id' => 3],
            ['user_id' => $this->id('users', 11), 'notification_id' => 4],
            ['user_id' => $this->id('users', 11), 'notification_id' => 5],
        ];
        $this->notificationsUsers->insert($data);

        // Roles
        $data = [
            ['structure_id' => $this->id('structures', 1), 'name' => 'Administrateur'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Administrateur Fonctionnel'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Rédacteur / Valideur'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Secrétariat général'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Super Administrateur'],
        ];
        $roles = Hash::combine($this->roles->insert($data, ['id', 'name']), '{n}.name', '{n}.id');

        // RolesUsers
        $data = [
            ['role_id' => $roles['Administrateur'], 'user_id' => $this->id('users', 1)],
            ['role_id' => $roles['Administrateur Fonctionnel'], 'user_id' => $this->id('users', 2)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 3)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 4)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 5)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 6)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 7)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 8)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 9)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 10)],
            ['role_id' => $roles['Secrétariat général'], 'user_id' => $this->id('users', 11)],
        ];
        $this->rolesUsers->insert($data);

        // Connecteurs
        $data = [
            [
                'id' => $this->id('connecteurs', 1),
                'name' => 'pastell',
                'url' => "{$env['LIBRICIEL_RECETTE_S1_PASTELL_URL']}/",
                'tdt_url' => "{$env['LIBRICIEL_RECETTE_S1_PASTELL_TDT_URL']}/modules/actes/actes_transac_post_confirm_api.php",
                'username' => $env['LIBRICIEL_RECETTE_S1_PASTELL_USERNAME'],
                'password' => $env['LIBRICIEL_RECETTE_S1_PASTELL_PASSWORD'],
                'structure_id' => $this->id('structures', 1),
                'connector_type_id' => 1,
                'connexion_option' => $env['LIBRICIEL_RECETTE_S1_PASTELL_CONNEXION_OPTION'],
            ],
            [
                'id' => $this->id('connecteurs', 2),
                'name' => 'idelibre',
                'url' => $env['LIBRICIEL_RECETTE_S1_IDELIBRE_URL'],
                'tdt_url' => null,
                'username' => $env['LIBRICIEL_RECETTE_S1_IDELIBRE_USERNAME'],
                'password' => $env['LIBRICIEL_RECETTE_S1_IDELIBRE_PASSWORD'],
                'structure_id' => $this->id('structures', 1),
                'connector_type_id' => 2,
                'connexion_option' => $env['LIBRICIEL_RECETTE_S1_IDELIBRE_CONNEXION_OPTION'],
            ],
        ];
        $this->connecteurs->insert($data);

        // Pastellfluxtypes
        $data = [
            [
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'actes-generique',
                'description' => 'Actes générique',
            ],
            [
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'mailsec',
                'description' => 'Mail securisé',
            ],
        ];
        $this->pastellfluxtypes->insert($data);

        // GenerateTemplates
        $data = [
            [
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => GenerateTemplateType::PROJECT,
                'name' => 'Modèle de projet d\'arrêté',
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => GenerateTemplateType::PROJECT,
                'name' => 'Modèle de projet de délibération',
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => GenerateTemplateType::CONVOCATION,
                'name' => 'Modèle de convocation',
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => GenerateTemplateType::EXECUTIVE_SUMMARY,
                'name' => 'Modèle de note de synthèse',
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => GenerateTemplateType::DELIBERATIONS_LIST,
                'name' => 'Modèle de liste des délibérations',
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => GenerateTemplateType::VERBAL_TRIAL,
                'name' => 'Modèle de procès-verbal',
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => GenerateTemplateType::ACT,
                'name' => 'Modèle d\'arrêté',
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => GenerateTemplateType::ACT,
                'name' => 'Modèle de délibération',
            ],
        ];
        $generateTemplates = $this->generateTemplates->insert($data, ['id', 'name']);

        // Files (GenerateTemplates)
        $data = [
            [
                'structure_id' => $this->id('structures', 1),
                'name' => trim('S1 Projet arrete.odt'),
                'path' => $files['S1 Projet arrêté']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S1 Projet arrêté']->getSize(),
                'generate_template_id' => $generateTemplates[0]['id'],
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => trim('S1 Projet deliberation.odt'),
                'path' => $files['S1 Projet délibération']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S1 Projet délibération']->getSize(),
                'generate_template_id' => $generateTemplates[1]['id'],
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => trim('S1 Convocation.odt'),
                'path' => $files['S1 Convocation']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S1 Convocation']->getSize(),
                'generate_template_id' => $generateTemplates[2]['id'],
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => trim('S1 Note de synthese.odt'),
                'path' => $files['S1 Note de synthèse']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S1 Note de synthèse']->getSize(),
                'generate_template_id' => $generateTemplates[3]['id'],
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => trim('S1 Liste des deliberations.odt'),
                'path' => $files['S1 Liste des délibérations']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S1 Liste des délibérations']->getSize(),
                'generate_template_id' => $generateTemplates[4]['id'],
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => trim('S1 Proces-verbal.odt'),
                'path' => $files['S1 Procès-verbal']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S1 Procès-verbal']->getSize(),
                'generate_template_id' => $generateTemplates[5]['id'],
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => trim('S1 Acte.odt'),
                'path' => $files['S1 Acte']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S1 Acte']->getSize(),
                'generate_template_id' => $generateTemplates[6]['id'],
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => trim('S1 Deliberation.odt'),
                'path' => $files['S1 Délibération']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S1 Délibération']->getSize(),
                'generate_template_id' => $generateTemplates[7]['id'],
            ],
        ];
        $this->files->insert($data, ['id', 'name']);

        // Dpos
        $data = [
            [
                'structure_id' => $this->id('structures', 1),
                'civility' => 'M.',
                'lastname' => 'S1 DPO',
                'firstname' => 'Théo',
                'email' => sprintf('wa-s1.dpo@%s', $domain),
            ],
        ];
        $this->dpos->insert($data);

        // Themes
        $data = [
            [
                'id' => $this->id('themes', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Moyens généraux',
                'parent_id' => null,
                'position' => '1',
            ],
            [
                'id' => $this->id('themes', 2),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Finances',
                'parent_id' => $this->id('themes', 1),
                'position' => '10',
            ],
            [
                'id' => $this->id('themes', 3),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Comptabilité',
                'parent_id' => $this->id('themes', 2),
                'position' => '101',
            ],
            [
                'id' => $this->id('themes', 4),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Gestion',
                'parent_id' => $this->id('themes', 3),
                'position' => '1010',
            ],
            [
                'id' => $this->id('themes', 5),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Ressources humaines',
                'parent_id' => $this->id('themes', 1),
                'position' => '11',
            ],
            [
                'id' => $this->id('themes', 6),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 commande publique',
                'parent_id' => $this->id('themes', 1),
                'position' => '12',
            ],
            [
                'id' => $this->id('themes', 7),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Service à la population',
                'parent_id' => null,
                'position' => '2',
            ],
            [
                'id' => $this->id('themes', 8),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Techniques',
                'parent_id' => null,
                'position' => '3',
            ],
            [
                'id' => $this->id('themes', 9),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Environnement',
                'parent_id' => $this->id('themes', 8),
                'position' => '30',
            ],
            [
                'id' => $this->id('themes', 10),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Eau potable',
                'parent_id' => $this->id('themes', 9),
                'position' => '301',
            ],
            [
                'id' => $this->id('themes', 11),
                'structure_id' => $this->id('structures', 1),
                'active' => false,
                'name' => 'S1 Thème désactivé',
                'parent_id' => null,
                'position' => '4',
            ],
        ];
        $this->themes->insert($data);

        // Natures, Matieres, Classifications, Typespiecesjointes
        $this->setupS2low();

        // Typesittings
        $data = [
            [
                'id' => $this->id('typesittings', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Conseil Marjo',
                'isdeliberating' => true,
                'generate_template_convocation_id' => $this->id('generate_templates', 3),
                'generate_template_executive_summary_id' => $this->id('generate_templates', 4),
                'generate_template_deliberations_list_id' => $this->id('generate_templates', 5),
                'generate_template_verbal_trial_id' => $this->id('generate_templates', 6),
            ],
            [
                'id' => $this->id('typesittings', 2),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Commission finance',
                'isdeliberating' => false,
                'generate_template_convocation_id' => $this->id('generate_templates', 3),
                'generate_template_executive_summary_id' => $this->id('generate_templates', 4),
                'generate_template_deliberations_list_id' => $this->id('generate_templates', 5),
                'generate_template_verbal_trial_id' => $this->id('generate_templates', 6),
            ],
            [
                'id' => $this->id('typesittings', 3),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Commission environnement',
                'isdeliberating' => false,
                'generate_template_convocation_id' => $this->id('generate_templates', 3),
                'generate_template_executive_summary_id' => $this->id('generate_templates', 4),
                'generate_template_deliberations_list_id' => $this->id('generate_templates', 5),
                'generate_template_verbal_trial_id' => $this->id('generate_templates', 6),
            ],
            [
                'id' => $this->id('typesittings', 4),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Type séance désactivé',
                'active' => false,
                'isdeliberating' => true,
                'generate_template_convocation_id' => $this->id('generate_templates', 3),
                'generate_template_executive_summary_id' => $this->id('generate_templates', 4),
                'generate_template_deliberations_list_id' => $this->id('generate_templates', 5),
                'generate_template_verbal_trial_id' => $this->id('generate_templates', 6),
            ],
        ];
        $this->typesittings->insert($data);

        // Sequences
        $data = [
            [
                'id' => $this->id('sequences', 1),
                'name' => 'S1 Arrêtés',
                'comment' => 'Séquence pour les arrêtés',
                'sequence_num' => 2,
                'structure_id' => $this->id('structures', 1),
            ],
            [
                'id' => $this->id('sequences', 2),
                'name' => 'S1 Délibérations',
                'comment' => 'Séquences délibérations',
                'sequence_num' => 0,
                'structure_id' => $this->id('structures', 1),
            ],
        ];
        $this->sequences->insert($data);

        // Counters
        $data = [
            [
                'id' => $this->id('counters', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Arrêtés TDT',
                'comment' => 'Arrêtés télétransmissibles',
                'counter_def' => '1ATDT#000#',
                'sequence_id' => $this->id('sequences', 1),
                'reinit_def' => '#MM#',
            ],
            [
                'id' => $this->id('counters', 2),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Arrêtés non TDT',
                'comment' => 'Arrêtés non télétransmissibles',
                'counter_def' => '1ANTDT#00#',
                'sequence_id' => $this->id('sequences', 1),
                'reinit_def' => '#JJ#',
            ],
            [
                'id' => $this->id('counters', 3),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Délibérations',
                'comment' => '',
                'counter_def' => '1DEL#AA##MM##JJ##SSS#',
                'sequence_id' => $this->id('sequences', 2),
                'reinit_def' => '#AAAA#',
            ],
        ];
        $this->counters->insert($data);

        // Typesacts
        $data = [
            [
                'id' => $this->id('typesacts', 1),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 5),
                'name' => 'S1 Délibération',
                'istdt' => true,
                'active' => true,
                'isdefault' => true,
                'counter_id' => $this->id('counters', 3),
                'generate_template_project_id' => $this->id('generate_templates', 2),
                'generate_template_act_id' => $this->id('generate_templates', 8),
                'isdeliberating' => true,
            ],
            [
                'id' => $this->id('typesacts', 2),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 2),
                'name' => 'S1 Arrêté TDT',
                'istdt' => true,
                'active' => true,
                'isdefault' => false,
                'counter_id' => $this->id('counters', 1),
                'generate_template_project_id' => $this->id('generate_templates', 1),
                'generate_template_act_id' => $this->id('generate_templates', 7),
                'isdeliberating' => false,
            ],
            [
                'id' => $this->id('typesacts', 3),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 3),
                'name' => 'S1 Arrêté non TDT',
                'istdt' => false,
                'active' => true,
                'isdefault' => false,
                'counter_id' => $this->id('counters', 2),
                'generate_template_project_id' => $this->id('generate_templates', 1),
                'generate_template_act_id' => $this->id('generate_templates', 7),
                'isdeliberating' => false,
            ],
            [
                'id' => $this->id('typesacts', 4),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 4),
                'name' => 'S1 type acte désactivé',
                'istdt' => true,
                'active' => false,
                'isdefault' => false,
                'counter_id' => $this->id('counters', 3),
                'generate_template_project_id' => $this->id('generate_templates', 1),
                'generate_template_act_id' => $this->id('generate_templates', 7),
                'isdeliberating' => true,
            ],
        ];
        $this->typesacts->insert($data);

        // TypesactsTypesittings
        $data = [
            ['typesact_id' => $this->id('typesacts', 1), 'typesitting_id' => $this->id('typesittings', 1)],
            ['typesact_id' => $this->id('typesacts', 1), 'typesitting_id' => $this->id('typesittings', 2)],
            ['typesact_id' => $this->id('typesacts', 1), 'typesitting_id' => $this->id('typesittings', 3)],
            ['typesact_id' => $this->id('typesacts', 1), 'typesitting_id' => $this->id('typesittings', 4)],
        ];
        $this->typesactsTypesittings->insert($data);

        // ActorGroups
        $data = [
            [
                'id' => $this->id('actor_groups', 1),
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Majorité',
                'active' => true,
            ],
            [
                'id' => $this->id('actor_groups', 2),
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Opposition',
                'active' => true,
            ],
            [
                'id' => $this->id('actor_groups', 3),
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Comm environnement',
                'active' => true,
            ],
            [
                'id' => $this->id('actor_groups', 4),
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Comm finances',
                'active' => true,
            ],
            [
                'id' => $this->id('actor_groups', 5),
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Invités',
                'active' => true,
            ],
            [
                'id' => $this->id('actor_groups', 6),
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S1 Groupe désactivé',
                'active' => false,
            ],
        ];
        $this->actorGroups->insert($data);

        // Actors
        $domain = env('LIBRICIEL_RECETTE_DEFAULT_EMAIL_DOMAIN', 'mailcatchall.libriciel.net');
        $data = [
            [
                'id' => $this->id('actors', 1),
                'structure_id' => $this->id('structures', 1),
                'active' => true,
                'civility' => 'M.',
                'lastname' => 'S1 DOYON',
                'firstname' => 'Auguste',
                'birthday' => null,
                'email' => 'wa-s1.acteur.doyon@' . $domain,
                'title' => 'M. le maire',
                'cellphone' => '0605045748',
                'rank' => 1,
            ],
            [
                'id' => $this->id('actors', 2),
                'structure_id' => $this->id('structures', 1),
                'active' => true,
                'civility' => 'Mme.',
                'lastname' => 'S1 VARIEUR',
                'firstname' => 'Joséphine',
                'birthday' => null,
                'email' => 'wa-s1.acteur.varieur@' . $domain,
                'title' => 'Adjointe au maire',
                'cellphone' => '0708541239',
                'rank' => 2,
            ],
            [
                'id' => $this->id('actors', 3),
                'structure_id' => $this->id('structures', 1),
                'active' => true,
                'civility' => 'M.',
                'lastname' => 'S1 LOISEAU',
                'firstname' => 'Maurice',
                'birthday' => null,
                'email' => 'wa-s1.acteur.loiseau@' . $domain,
                'title' => 'Adjoint environnement',
                'cellphone' => null,
                'rank' => 3,
            ],
            [
                'id' => $this->id('actors', 4),
                'structure_id' => $this->id('structures', 1),
                'active' => true,
                'civility' => 'Mme.',
                'lastname' => 'S1 PELLAND',
                'firstname' => 'Rolande',
                'birthday' => null,
                'email' => 'wa-s1.acteur.pelland@' . $domain,
                'title' => 'Deuxième adjoint environnement',
                'cellphone' => null,
                'rank' => 4,
            ],
            [
                'id' => $this->id('actors', 5),
                'structure_id' => $this->id('structures', 1),
                'active' => false,
                'civility' => 'Mme.',
                'lastname' => 'S1 LEBEL',
                'firstname' => 'Harriette',
                'birthday' => null,
                'email' => 'wa-s1.acteur.lebel@' . $domain,
                'title' => 'Troisième adjointe',
                'cellphone' => null,
                'rank' => 5,
            ],
            [
                'id' => $this->id('actors', 6),
                'structure_id' => $this->id('structures', 1),
                'active' => true,
                'civility' => 'M.',
                'lastname' => 'S1 MANVILLE',
                'firstname' => 'Renaud',
                'birthday' => null,
                'email' => 'wa-s1.acteur.manville@' . $domain,
                'title' => '',
                'cellphone' => '0705040404',
                'rank' => null,
            ],
            [
                'id' => $this->id('actors', 7),
                'structure_id' => $this->id('structures', 1),
                'active' => true,
                'civility' => 'Mme.',
                'lastname' => 'S1 SACREY',
                'firstname' => 'Caroline',
                'birthday' => null,
                'email' => 'wa-s1.acteur.sacrey@' . $domain,
                'title' => '',
                'cellphone' => '',
                'rank' => null,
            ],
            [
                'id' => $this->id('actors', 8),
                'structure_id' => $this->id('structures', 1),
                'active' => true,
                'civility' => 'M.',
                'lastname' => 'S1 COURCELLE',
                'firstname' => 'Armand',
                'birthday' => null,
                'email' => 'wa-s1.acteur.courcelle@' . $domain,
                'title' => '',
                'cellphone' => '',
                'rank' => null,
            ],
            [
                'id' => $this->id('actors', 9),
                'structure_id' => $this->id('structures', 1),
                'active' => false,
                'civility' => 'Mme.',
                'lastname' => 'S1 DEVOST',
                'firstname' => 'Estelle',
                'birthday' => null,
                'email' => 'wa-s1.acteur.devost@' . $domain,
                'title' => '',
                'cellphone' => '',
                'rank' => null,
            ],
            [
                'id' => $this->id('actors', 10),
                'structure_id' => $this->id('structures', 1),
                'active' => true,
                'civility' => 'Mme.',
                'lastname' => 'S1 ROUZE',
                'firstname' => 'Diane',
                'birthday' => null,
                'email' => 'wa-s1.acteur.rouze@' . $domain,
                'title' => '',
                'cellphone' => '',
                'rank' => null,
            ],
        ];
        $this->actors->insert($data);

        // ActorsActorGroups
        $data = [
            ['id' => $this->id('actors_actor_groups', 1), 'actor_id' => $this->id('actors', 1), 'actor_group_id' => $this->id('actor_groups', 1)],
            ['id' => $this->id('actors_actor_groups', 2), 'actor_id' => $this->id('actors', 1), 'actor_group_id' => $this->id('actor_groups', 4)],
            ['id' => $this->id('actors_actor_groups', 3), 'actor_id' => $this->id('actors', 2), 'actor_group_id' => $this->id('actor_groups', 1)],
            ['id' => $this->id('actors_actor_groups', 4), 'actor_id' => $this->id('actors', 2), 'actor_group_id' => $this->id('actor_groups', 4)],
            ['id' => $this->id('actors_actor_groups', 5), 'actor_id' => $this->id('actors', 3), 'actor_group_id' => $this->id('actor_groups', 1)],
            ['id' => $this->id('actors_actor_groups', 6), 'actor_id' => $this->id('actors', 3), 'actor_group_id' => $this->id('actor_groups', 3)],
            ['id' => $this->id('actors_actor_groups', 7), 'actor_id' => $this->id('actors', 4), 'actor_group_id' => $this->id('actor_groups', 3)],
            ['id' => $this->id('actors_actor_groups', 8), 'actor_id' => $this->id('actors', 5), 'actor_group_id' => $this->id('actor_groups', 1)],
            ['id' => $this->id('actors_actor_groups', 9), 'actor_id' => $this->id('actors', 5), 'actor_group_id' => $this->id('actor_groups', 3)],
            ['id' => $this->id('actors_actor_groups', 10), 'actor_id' => $this->id('actors', 6), 'actor_group_id' => $this->id('actor_groups', 2)],
            ['id' => $this->id('actors_actor_groups', 11), 'actor_id' => $this->id('actors', 7), 'actor_group_id' => $this->id('actor_groups', 2)],
            ['id' => $this->id('actors_actor_groups', 12), 'actor_id' => $this->id('actors', 8), 'actor_group_id' => $this->id('actor_groups', 2)],
            ['id' => $this->id('actors_actor_groups', 13), 'actor_id' => $this->id('actors', 8), 'actor_group_id' => $this->id('actor_groups', 4)],
            ['id' => $this->id('actors_actor_groups', 14), 'actor_id' => $this->id('actors', 9), 'actor_group_id' => $this->id('actor_groups', 2)],
            ['id' => $this->id('actors_actor_groups', 15), 'actor_id' => $this->id('actors', 10), 'actor_group_id' => $this->id('actor_groups', 5)],
        ];
        $this->actorsActorGroups->insert($data);

        // ActorGroupsTypesittings
        $data = [
            ['actor_group_id' => $this->id('actor_groups', 1), 'typesitting_id' => $this->id('typesittings', 1)],
            ['actor_group_id' => $this->id('actor_groups', 2), 'typesitting_id' => $this->id('typesittings', 1)],
            ['actor_group_id' => $this->id('actor_groups', 5), 'typesitting_id' => $this->id('typesittings', 1)],
            ['actor_group_id' => $this->id('actor_groups', 4), 'typesitting_id' => $this->id('typesittings', 2)],
            ['actor_group_id' => $this->id('actor_groups', 3), 'typesitting_id' => $this->id('typesittings', 3)],
            ['actor_group_id' => $this->id('actor_groups', 2), 'typesitting_id' => $this->id('typesittings', 4)],
            ['actor_group_id' => $this->id('actor_groups', 3), 'typesitting_id' => $this->id('typesittings', 4)],
            ['actor_group_id' => $this->id('actor_groups', 4), 'typesitting_id' => $this->id('typesittings', 4)],
        ];
        $this->actorGroupsTypesittings->insert($data);

        // DraftTemplates
        $data = [
            ['id' => $this->id('draft_templates', 1), 'structure_id' => $this->id('structures', 1), 'draft_template_type_id' => 2, 'name' => 'S1 acte arrêté'],
            ['id' => $this->id('draft_templates', 2), 'structure_id' => $this->id('structures', 1), 'draft_template_type_id' => 1, 'name' => 'S1 projet arrêté'],
            ['id' => $this->id('draft_templates', 3), 'structure_id' => $this->id('structures', 1), 'draft_template_type_id' => 2, 'name' => 'S1 acte délibération'],
            ['id' => $this->id('draft_templates', 4), 'structure_id' => $this->id('structures', 1), 'draft_template_type_id' => 1, 'name' => 'S1 projet délibération'],
        ];
        $this->draftTemplates->insert($data);

        // DraftTemplatesTypesacts
        $data = [
            ['id' => $this->id('draft_templates_typesacts', 1), 'draft_template_id' => $this->id('draft_templates', 3), 'typesact_id' => $this->id('typesacts', 1)],
            ['id' => $this->id('draft_templates_typesacts', 2), 'draft_template_id' => $this->id('draft_templates', 4), 'typesact_id' => $this->id('typesacts', 1)],
            ['id' => $this->id('draft_templates_typesacts', 3), 'draft_template_id' => $this->id('draft_templates', 1), 'typesact_id' => $this->id('typesacts', 2)],
            ['id' => $this->id('draft_templates_typesacts', 4), 'draft_template_id' => $this->id('draft_templates', 2), 'typesact_id' => $this->id('typesacts', 2)],
            ['id' => $this->id('draft_templates_typesacts', 5), 'draft_template_id' => $this->id('draft_templates', 1), 'typesact_id' => $this->id('typesacts', 3)],
            ['id' => $this->id('draft_templates_typesacts', 6), 'draft_template_id' => $this->id('draft_templates', 2), 'typesact_id' => $this->id('typesacts', 4)],
            ['id' => $this->id('draft_templates_typesacts', 7), 'draft_template_id' => $this->id('draft_templates', 1), 'typesact_id' => $this->id('typesacts', 3)],
            ['id' => $this->id('draft_templates_typesacts', 8), 'draft_template_id' => $this->id('draft_templates', 2), 'typesact_id' => $this->id('typesacts', 4)],
        ];
        $this->draftTemplatesTypesacts->insert($data);

        // Files (FilesDraftTemplates)
        $data = [
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'gabarit_arrete_libriciel.odt',
                'path' => $files['gabarit_arrete_libriciel']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['gabarit_arrete_libriciel']->getSize(),
                'draft_template_id' => $this->id('draft_templates', 1),
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'gabarit_projet_libriciel.odt',
                'path' => $files['gabarit_projet_libriciel']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['gabarit_projet_libriciel']->getSize(),
                'draft_template_id' => $this->id('draft_templates', 2),
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'texte_delib2.odt',
                'path' => $files['texte_delib2']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['texte_delib2']->getSize(),
                'draft_template_id' => $this->id('draft_templates', 3),
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'texte_projet.odt',
                'path' => $files['texte_projet']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['texte_projet']->getSize(),
                'draft_template_id' => $this->id('draft_templates', 4),
            ],
        ];
        $this->files->insert($data);

        // Sittings
        $data = [
            [
                'id' => $this->id('sittings', 1),
                'structure_id' => $this->id('structures', 1),
                'typesitting_id' => $this->id('typesittings', 2),
                'date' => "{$year}-01-03 13:00:00",
                'president_id' => $this->id('actors', 1),
                'secretary_id' => $this->id('actors', 2),
            ],
            [
                'id' => $this->id('sittings', 2),
                'structure_id' => $this->id('structures', 1),
                'typesitting_id' => $this->id('typesittings', 3),
                'date' => "{$year}-01-04 14:00:00",
                'president_id' => $this->id('actors', 1),
                'secretary_id' => $this->id('actors', 2),
            ],
            [
                'id' => $this->id('sittings', 3),
                'structure_id' => $this->id('structures', 1),
                'typesitting_id' => $this->id('typesittings', 4),
                'date' => "{$year}-01-05 15:00:00",
                'president_id' => $this->id('actors', 1),
                'secretary_id' => $this->id('actors', 2),
            ],
            [
                'id' => $this->id('sittings', 4),
                'structure_id' => $this->id('structures', 1),
                'typesitting_id' => $this->id('typesittings', 1),
                'date' => "{$year}-01-06 16:00:00",
                'president_id' => $this->id('actors', 1),
                'secretary_id' => $this->id('actors', 2),
            ],
        ];
        $this->sittings->insert($data);

        // SittingsStatesittings
        $data = [
            ['sitting_id' => $this->id('sittings', 1), 'statesitting_id' => 1],
            ['sitting_id' => $this->id('sittings', 2), 'statesitting_id' => 1],
            ['sitting_id' => $this->id('sittings', 3), 'statesitting_id' => 1],
            ['sitting_id' => $this->id('sittings', 4), 'statesitting_id' => 1],
        ];
        $this->sittingsStatesittings->insert($data);
    }

    /**
     * Chargement des données de la structure 1 pour la recette
     *
     * @return void
     */
    public function run(): void
    {
        parent::run();

        $this->sequences();

        $this->execute('BEGIN;');
        $this->setup();
        $this->execute('COMMIT;');

        $this->sequences();
        $this->permissions();

        // Création de n (10 par défaut) projets de délibération avec 3 annexes chacun
        $ids = [
            'organization' => $this->id('organizations', 1),
            'structure' => $this->id('structures', 1),
            'user' => $this->id('users', 1),
            'theme' => $this->id('themes', 4),
            'typeact' => $this->id('typesacts', 1),
            'draftTemplateActe' => $this->id('draft_templates', 3),
            'draftTemplateProject' => $this->id('draft_templates', 4),
        ];

        $max = $this->env('LIBRICIEL_RECETTE_S1_NOMBRE_PROJETS', 10);
        for ($idx = 1; $idx <= $max; $idx++) {
            $this->addProjectWithGeneration(
                $ids['organization'],
                $ids['structure'],
                $ids['user'],
                $ids['theme'],
                $ids['typeact'],
                $ids['draftTemplateActe'],
                $ids['draftTemplateProject'],
            );
        }
    }
}
