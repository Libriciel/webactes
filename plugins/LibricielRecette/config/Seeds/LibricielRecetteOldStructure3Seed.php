<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use Cake\Utility\Hash;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;
use LibricielRecette\Seeds\LibricielRecetteSeedGenerateTemplatesTrait;

/**
 * @deprecated
 */
class LibricielRecetteOldStructure3Seed extends LibricielRecetteAbstractStructureSeed
{
    use LibricielRecetteSeedGenerateTemplatesTrait;

    /**
     * Crée le paramétrage de la structure 3
     *
     * @return void
     */
    protected function setup(): void
    {
        // @info: {$_ENV["..."]} not set
        $env = getenv();

        // Organizations
        $data = [
            [
                'id' => $this->id('organizations', 1),
                'name' => 'Recette WA structure 3',
            ],
        ];
        $this->organizations->insert($data);

        // Structures
        $data = [
            [
                'id' => $this->id('structures', 1),
                'organization_id' => $this->id('organizations', 1),
                'business_name' => 'Recette WA S3',
                'id_orchestration' => $env['LIBRICIEL_RECETTE_S3_PASTELL_ID_E'],
                'spinner' => 'ball-clip-rotate',
            ],
        ];
        $this->structures->insert($data);

        // StructureSettings
        $data = [
            [
                'id' => $this->id('structure_settings', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'project',
                'value' => '{"project_workflow":false,"project_writing":false,"project_generate":false}',
            ],
            [
                'id' => $this->id('structure_settings', 2),
                'structure_id' => $this->id('structures', 1),
                'name' => 'act',
                'value' => '{"generate_number":false,"act_generate":false}',
            ],
            [
                'id' => $this->id('structure_settings', 4),
                'structure_id' => $this->id('structures', 1),
                'name' => 'sitting',
                'value' => '{"sitting_enable":false,"sitting_generate_verbal_trial_digest":false}',
            ],
        ];
        $this->structureSettings->insert($data);

        // Officials (0)

        // Users
        $domain = env('LIBRICIEL_RECETTE_DEFAULT_EMAIL_DOMAIN', 'mailcatchall.libriciel.net');
        $data = [
            [
                'id' => $this->id('users', 1),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme',
                'lastname' => 'S3 ADMIN',
                'firstname' => 'Camille',
                'username' => 'admin@wa-s3',
                'email' => 'wa-s3.admin@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 2),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S3 RÉDACTEUR',
                'firstname' => 'Kévin',
                'username' => 'redac@wa-s3',
                'email' => 'wa-s3.redac@' . $domain,
                'data_security_policy' => false,
            ],
        ];
        $this->users->insert($data);

        // StructuresUsers
        $data = [
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 1)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 2)],
        ];
        $this->structuresUsers->insert($data);

        // NotificationsUsers
        $data = [
            ['user_id' => $this->id('users', 1), 'notification_id' => 1],
            ['user_id' => $this->id('users', 1), 'notification_id' => 2],
            ['user_id' => $this->id('users', 1), 'notification_id' => 3],
            ['user_id' => $this->id('users', 1), 'notification_id' => 4],
            ['user_id' => $this->id('users', 1), 'notification_id' => 5],
            ['user_id' => $this->id('users', 2), 'notification_id' => 1],
            ['user_id' => $this->id('users', 2), 'notification_id' => 2],
            ['user_id' => $this->id('users', 2), 'notification_id' => 3],
            ['user_id' => $this->id('users', 2), 'notification_id' => 4],
            ['user_id' => $this->id('users', 2), 'notification_id' => 5],
        ];
        $this->notificationsUsers->insert($data);

        // Roles
        $data = [
            ['structure_id' => $this->id('structures', 1), 'name' => 'Administrateur'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Administrateur Fonctionnel'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Rédacteur / Valideur'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Secrétariat général'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Super Administrateur'],
        ];
        $roles = Hash::combine($this->roles->insert($data, ['id', 'name']), '{n}.name', '{n}.id');

        // RolesUsers
        $data = [
            ['role_id' => $roles['Administrateur'], 'user_id' => $this->id('users', 1)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 2)],
        ];
        $this->rolesUsers->insert($data);

        // Connecteurs
        $data = [
            [
                'id' => $this->id('connecteurs', 1),
                'name' => 'pastell',
                'url' => "{$env["LIBRICIEL_RECETTE_S3_PASTELL_URL"]}/",
                'tdt_url' => "{$env["LIBRICIEL_RECETTE_S3_PASTELL_TDT_URL"]}/modules/actes/actes_transac_post_confirm_api.php",
                'username' => $env['LIBRICIEL_RECETTE_S3_PASTELL_USERNAME'],
                'password' => $env['LIBRICIEL_RECETTE_S3_PASTELL_PASSWORD'],
                'structure_id' => $this->id('structures', 1),
                'connector_type_id' => 1,
                'connexion_option' => $env['LIBRICIEL_RECETTE_S3_PASTELL_CONNEXION_OPTION'],
            ],
        ];
        $this->connecteurs->insert($data);

        // Pastellfluxtypes
        $data = [
            [
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'actes-generique',
                'description' => 'Actes générique',
            ],
            [
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'mailsec',
                'description' => 'Mail securisé',
            ],
        ];
        $this->pastellfluxtypes->insert($data);

        // GenerateTemplates (O)

        // Files (GenerateTemplates) (0)

        // Dpos (0)

        // Themes
        $data = [
            [
                'id' => $this->id('themes', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S3 Thème 1',
                'parent_id' => null,
                'position' => '1',
            ],
        ];
        $this->themes->insert($data);

        // Natures, Matieres, Classifications, Typespiecesjointes
        $this->setupS2low();

        // Typesittings (0)

        // Sequences (0)

        // Counters (0)

        // Typesacts
        $data = [
            [
                'id' => $this->id('typesacts', 1),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 2),
                'name' => 'Arrêtés TDT',
                'istdt' => true,
                'isdefault' => false,
                'isdeliberating' => false,
            ],
            [
                'id' => $this->id('typesacts', 2),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 3),
                'name' => 'Arrêtés non TDT',
                'istdt' => false,
                'isdefault' => false,
                'isdeliberating' => false,
            ],
        ];
        $this->typesacts->insert($data);

        // TypesactsTypesittings (0)

        // ActorGroups (O)

        // Actors (0)

        // ActorsActorGroups (0)

        // ActorGroupsTypesittings (0)

        // DraftTemplates (0)

        // DraftTemplatesTypesacts (0)

        // Files (DraftTemplates) (0)

        // Sittings (0)

        // SittingsStatesittings (0)
    }

    /**
     * Chargement des données de la structure 2 pour la recette
     *
     * @return void
     */
    public function run(): void
    {
        parent::run();

        $this->sequences();

        $this->execute('BEGIN;');
        $this->setup();
        $this->execute('COMMIT;');

        $this->sequences();
        $this->permissions();

        // Création de n (10 par défaut) projets de délibération avec 3 annexes chacun
        $ids = [
            'organization' => $this->id('organizations', 1),
            'structure' => $this->id('structures', 1),
            'user' => $this->id('users', 1),
            'theme' => $this->id('themes', 1),
            'typeact' => $this->id('typesacts', 1),
        ];

        $max = $this->env('LIBRICIEL_RECETTE_S3_NOMBRE_PROJETS', 10);
        for ($idx = 1; $idx <= $max; $idx++) {
            $this->addProjectWithoutGeneration(
                $ids['organization'],
                $ids['structure'],
                $ids['user'],
                $ids['theme'],
                $ids['typeact'],
                sprintf('S3_%d_AT_%04d', date('Y'), $idx),
                dirname(__DIR__, 2) . DS . 'files' . DS . 'Projects' . DS . 'Projet S3.pdf'
            );
        }
    }
}
