<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use LibricielRecette\Model\Enum\GenerateTemplateType;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;

/**
 * @deprecated
 */
class LibricielRecetteDemo1Seed extends LibricielRecetteAbstractStructureSeed
{
    protected ?int $year;

    /**
     * @var \SplFileInfo[]
     */
    protected array $paths = [];

    protected function setupOrganizations(): void
    {
        $record = $this->fetchRow("SELECT id, name FROM organizations WHERE name = 'LIBRICIEL SCOP';");
        if ($record) {
            if (!isset($this->records['organizations'])) {
                $this->records['organizations'] = [];
            }
            $this->records['organizations'][$record['name']] = $record['id'];
        } else {
            $this->save(
                $this->organizations,
                [
                    [
                        'name' => 'LIBRICIEL SCOP',
                    ],
                ]
            );
        }
    }

    protected function setupStructures(): void
    {
        $this->save(
            $this->structures,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'business_name' => 'LIBRICIEL SCOP',
                    'id_orchestration' => $this->env('LIBRICIEL_RECETTE_DEMO1_PASTELL_ID_E', 9),
                    'spinner' => 'ball-clip-rotate',
                ],
            ]
        );
    }

    protected function setupStructureSettings(): void
    {
        $this->save(
            $this->structureSettings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'project',
                    'value' => '{"project_generate":true,"project_workflow":true,"project_writing":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'act',
                    'value' => '{"act_generate":true,"generate_number":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'convocation',
                    'value' => '{"convocation_idelibre":true,"convocation_mailsec":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'sitting',
                    'value' => '{"sitting_enable":true,"sitting_generate_deliberations_list":true,"sitting_generate_verbal_trial":true}',
                ],
            ]
        );
    }

    protected function setupOfficials(): void
    {
        $this->save(
            $this->officials,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Me.',
                    'lastname' => 'Diallo',
                    'firstname' => 'Stéphane',
                    'email' => 'contact@libriciel.coop',
                    'address' => '836 Rue du Mas de Verchant',
                    'address_supplement' => 'Immeuble le TUCANO',
                    'post_code' => '34000',
                    'city' => 'Montpellier',
                    'phone' => '04 67 65 96 44',
                ],
            ]
        );
    }

    protected function setupUsers(): void
    {
        $this->save(
            $this->users,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'BLANC',
                    'firstname' => 'Sébastien',
                    'username' => 's.blanc',
                    'email' => 'sebastien.blanc@test.fr',
                    'phone' => '0467659645',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'VERT',
                    'firstname' => 'Mickael',
                    'username' => 'm.vert',
                    'email' => 'mickael.vert@test.fr',
                    'phone' => '0467659645',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'ORANGE',
                    'firstname' => 'Julien',
                    'username' => 'j.orange',
                    'email' => 'julien.orange@test.fr',
                    'phone' => '0467659645',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'VIOLET',
                    'firstname' => 'Rémi',
                    'username' => 'r.violet',
                    'email' => 'remi.violet@test.fr',
                    'phone' => '0467659645',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'Cyan',
                    'firstname' => 'Romain',
                    'username' => 'r.cyan',
                    'email' => 'romain.cyan@test.fr',
                ],
            ]
        );
    }

    protected function setupStructuresUsers(): void
    {
        $this->save(
            $this->structuresUsers,
            [
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 's.blanc')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'm.vert')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'j.orange')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'r.violet')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'r.cyan')],
            ]
        );
    }

    protected function setupNotificationsUsers(): void
    {
        $this->save(
            $this->notificationsUsers,
            [
            ]
        );
    }

    protected function setupRoles(): void
    {
        $this->save(
            $this->roles,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Rédacteur / Valideur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Secrétariat général',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur Fonctionnel',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Super Administrateur',
                ],
            ]
        );
    }

    protected function setupRolesUsers(): void
    {
        $this->save(
            $this->rolesUsers,
            [
                ['role_id' => $this->record($this->roles, 'Administrateur'), 'user_id' => $this->record($this->users, 's.blanc')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'm.vert')],
                ['role_id' => $this->record($this->roles, 'Secrétariat général'), 'user_id' => $this->record($this->users, 'j.orange')],
                ['role_id' => $this->record($this->roles, 'Administrateur Fonctionnel'), 'user_id' => $this->record($this->users, 'r.violet')],
                ['role_id' => $this->record($this->roles, 'Super Administrateur'), 'user_id' => $this->record($this->users, 'r.cyan')],
            ]
        );
    }

    protected function setupConnecteurs(): void
    {
        $env = getenv();
        $this->save(
            $this->connecteurs,
            [
                [
                    'name' => 'pastell',
                    'url' => "{$env['LIBRICIEL_RECETTE_DEMO1_PASTELL_URL']}/",
                    'tdt_url' => "{$env['LIBRICIEL_RECETTE_DEMO1_PASTELL_TDT_URL']}/modules/actes/actes_transac_post_confirm_api.php",
                    'username' => $env['LIBRICIEL_RECETTE_DEMO1_PASTELL_USERNAME'],
                    'password' => $env['LIBRICIEL_RECETTE_DEMO1_PASTELL_PASSWORD'],
                    'structure_id' => $this->current($this->structures),
                    'connector_type_id' => 1,
                    'connexion_option' => $env['LIBRICIEL_RECETTE_DEMO1_PASTELL_CONNEXION_OPTION'],
                ],
                [
                    'name' => 'idelibre',
                    'url' => $env['LIBRICIEL_RECETTE_DEMO1_IDELIBRE_URL'],
                    'tdt_url' => null,
                    'username' => $env['LIBRICIEL_RECETTE_DEMO1_IDELIBRE_USERNAME'],
                    'password' => $env['LIBRICIEL_RECETTE_DEMO1_IDELIBRE_PASSWORD'],
                    'structure_id' => $this->current($this->structures),
                    'connector_type_id' => 2,
                    'connexion_option' => $env['LIBRICIEL_RECETTE_DEMO1_IDELIBRE_CONNEXION_OPTION'],
                ],
            ]
        );
    }

    protected function setupPastellfluxtypes(): void
    {
        $this->save(
            $this->pastellfluxtypes,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'actes-generique',
                    'description' => 'Actes générique',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'mailsec',
                    'description' => 'Mail securisé',
                ],
            ]
        );
    }

    protected function setupGenerateTemplates(): void
    {
        $this->save(
            $this->generateTemplates,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::PROJECT,
                    'name' => 'Modèle de projet',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::ACT,
                    'name' => 'Modèle d\'acte',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::CONVOCATION,
                    'name' => 'Modèle de convocation d\'un acte',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::EXECUTIVE_SUMMARY,
                    'name' => 'Modèle de note de synthèse',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::DELIBERATIONS_LIST,
                    'name' => 'Modèle de liste des délibérations',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::VERBAL_TRIAL,
                    'name' => 'Modèle de procès-verbal',
                ],
            ]
        );
    }

    protected function setupGenerateTemplatesFiles(): void
    {
        $this->save(
            $this->files,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_projet_v1.odt',
                    'path' => $this->paths['modele_projet_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_projet_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de projet'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_acte_v1.odt',
                    'path' => $this->paths['modele_acte_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_acte_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle d\'acte'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_convocation_v1.odt',
                    'path' => $this->paths['modele_convocation_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_convocation_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de convocation d\'un acte'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_note_de_synthese_v1.odt',
                    'path' => $this->paths['modele_note_de_synthese_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_note_de_synthese_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_liste_des_deliberations_v1.odt',
                    'path' => $this->paths['modele_liste_des_deliberations_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_liste_des_deliberations_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_proces_verbal_v1.odt',
                    'path' => $this->paths['modele_proces_verbal_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_proces_verbal_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
            ]
        );
    }

    protected function setupDpos(): void
    {
        $this->save(
            $this->dpos,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'DPO',
                    'firstname' => 'Mickey',
                    'email' => 'mickey.dpo@test.fr',
                ],
            ]
        );
    }

    protected function setupThemes(): void
    {
        $this->save(
            $this->themes,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administration Générale',
                    'position' => 'AG100',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Ressources Humaines',
                    'parent_id' => $this->id('themes', 1),
                    'position' => 'AG110',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Affaires juridiques',
                    'parent_id' => $this->id('themes', 1),
                    'position' => 'AG120',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Urbanisme',
                    'position' => 'URB100',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Procès',
                    'parent_id' => $this->id('themes', 3),
                    'position' => 'AG121',
                ],
            ]
        );
    }

    protected function setupTypesittings(): void
    {
        $this->save(
            $this->typesittings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Conseil Municipal',
                    'isdeliberating' => true,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation d\'un acte'),
                    'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
            ]
        );
    }

    protected function setupSequences(): void
    {
        $this->save(
            $this->sequences,
            [
                [
                    'name' => 'Séquences_arrêtés',
                    'comment' => 'Une séquence spécifique pour les arrêtés',
                    'sequence_num' => 1,
                    'structure_id' => $this->current($this->structures),
                ],
                [
                    'name' => 'Séquences_commissions',
                    'comment' => 'Une séquence spécifique pour les commissions',
                    'sequence_num' => 1,
                    'structure_id' => $this->current($this->structures),
                ],
                [
                    'name' => 'Séquences_conventions',
                    'comment' => 'Une séquence spécifique pour les conventions',
                    'sequence_num' => 1,
                    'structure_id' => $this->current($this->structures),
                ],
                [
                    'name' => 'Séquences_délibérations',
                    'comment' => 'Une séquence spécifique pour les délibérations',
                    'sequence_num' => 1,
                    'structure_id' => $this->current($this->structures),
                ],
            ]
        );
    }

    protected function setupCounters(): void
    {
        $this->save(
            $this->counters,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Compteurs délib',
                    'comment' => 'Un système de compteur pour les délibérations',
                    'counter_def' => 'DELIB_#SS#',
                    'sequence_id' => $this->record($this->sequences, 'Séquences_délibérations'),
                    'reinit_def' => '#AAAA#',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Compteurs de commissions',
                    'comment' => 'Le compteur des commissions',
                    'counter_def' => 'DELIB_#MM#',
                    'sequence_id' => $this->record($this->sequences, 'Séquences_commissions'),
                    'reinit_def' => '#JJ#',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Compteur des arrêtés',
                    'comment' => 'Un compteur pour les arrêtés',
                    'counter_def' => 'DELIB_#0000#',
                    'sequence_id' => $this->record($this->sequences, 'Séquences_arrêtés'),
                    'reinit_def' => '#AAAA#',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Le compteur des conventions',
                    'comment' => 'Les conventions ont un système de compteur',
                    'counter_def' => 'DELIB_#JJ#',
                    'sequence_id' => $this->record($this->sequences, 'Séquences_conventions'),
                    'reinit_def' => '#AAAA#',
                ],
            ]
        );
    }

    protected function setupTypesacts(): void
    {
        $this->save(
            $this->typesacts,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'DE'),
                    'name' => 'Délibération',
                    'istdt' => true,
                    'isdefault' => true,
                    'counter_id' => $this->record($this->counters, 'Compteurs délib'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle d\'acte'),
                    'isdeliberating' => true,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AR'),
                    'name' => 'Arrêté réglementaire',
                    'istdt' => true,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'Compteur des arrêtés'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle d\'acte'),
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AI'),
                    'name' => 'Arrêté individuel',
                    'istdt' => false,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'Compteur des arrêtés'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle d\'acte'),
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'CC'),
                    'name' => 'Contrat et convention',
                    'istdt' => true,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'Le compteur des conventions'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle d\'acte'),
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AU'),
                    'name' => 'Autre',
                    'istdt' => false,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'Le compteur des conventions'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle d\'acte'),
                    'isdeliberating' => false,
                ],
            ]
        );
    }

    protected function setupTypesactsTypesittings(): void
    {
        $this->save(
            $this->typesactsTypesittings,
            [
                ['typesact_id' => $this->record($this->typesacts, 'Délibération'), 'typesitting_id' => $this->record($this->typesittings, 'Conseil Municipal')],
            ]
        );
    }

    protected function setupActorGroups(): void
    {
        $this->save(
            $this->actorGroups,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Louhans - Groupe 1',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Louhans - Groupe 2',
                ],
            ]
        );
    }

    protected function setupActors(): void
    {
        $this->save(
            $this->actors,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mlle',
                    'lastname' => 'Collin',
                    'firstname' => 'Charles',
                    'email' => 'vincent53@sfr.fr',
                    'title' => '',
                    'rank' => 1,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Pr.',
                    'lastname' => 'Weiss',
                    'firstname' => 'Dominique',
                    'email' => 'alves.daniel@brun.fr',
                    'title' => '',
                    'rank' => 2,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mlle',
                    'lastname' => 'Bouvet',
                    'firstname' => 'Stéphanie',
                    'email' => 'nicolas25@club-internet.fr',
                    'title' => 'Docteur',
                    'rank' => 3,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Pr.',
                    'lastname' => 'Bertrand',
                    'firstname' => 'Guy',
                    'email' => 'eric.petitjean@dupre.fr',
                    'title' => '',
                    'rank' => 4,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Dr.',
                    'lastname' => 'Mercier',
                    'firstname' => 'Bernard',
                    'email' => 'vrobert@wanadoo.fr',
                    'title' => '',
                    'rank' => 5,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Me.',
                    'lastname' => 'Pascal',
                    'firstname' => 'Madeleine',
                    'email' => 'begue.anne@dbmail.com',
                    'title' => '',
                    'rank' => 6,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Dr.',
                    'lastname' => 'Lucas',
                    'firstname' => 'Aimée',
                    'email' => 'richard.adam@auger.fr',
                    'title' => '',
                    'rank' => 7,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Me.',
                    'lastname' => 'Merle',
                    'firstname' => 'Guy',
                    'email' => 'diane.ollivier@tessier.net',
                    'title' => '',
                    'rank' => 8,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Dr.',
                    'lastname' => 'Lacombe',
                    'firstname' => 'David',
                    'email' => 'stephane06@club-internet.fr',
                    'title' => '',
                    'rank' => 9,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Pr.',
                    'lastname' => 'Boucher',
                    'firstname' => 'Jeannine',
                    'email' => 'etienne75@tele2.fr',
                    'title' => 'Docteur',
                    'rank' => 10,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Dr.',
                    'lastname' => 'Monnier',
                    'firstname' => 'Arthur',
                    'email' => 'eleonore86@free.fr',
                    'title' => 'Docteur',
                    'rank' => 11,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Dr.',
                    'lastname' => 'Leveque',
                    'firstname' => 'Astrid',
                    'email' => 'elodie27@live.com',
                    'title' => 'Docteur',
                    'rank' => 12,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Me.',
                    'lastname' => 'Remy',
                    'firstname' => 'Claudine',
                    'email' => 'lamy.anne@dbmail.com',
                    'title' => '',
                    'rank' => 13,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Dr.',
                    'lastname' => 'Bernard',
                    'firstname' => 'Jeanne',
                    'email' => 'monique86@live.com',
                    'title' => '',
                    'rank' => 14,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Pr.',
                    'lastname' => 'Denis',
                    'firstname' => 'Olivier',
                    'email' => 'jacques.remy@hotmail.fr',
                    'title' => 'Docteur',
                    'rank' => 15,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mlle',
                    'lastname' => 'Marin',
                    'firstname' => 'François',
                    'email' => 'martine.bodin@nicolas.com',
                    'title' => '',
                    'rank' => 16,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Pr.',
                    'lastname' => 'Coulon',
                    'firstname' => 'Adèle',
                    'email' => 'dominique.ramos@club-internet.fr',
                    'title' => '',
                    'rank' => 17,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Me.',
                    'lastname' => 'Lefevre',
                    'firstname' => 'Henri',
                    'email' => 'leroux.alfred@lefort.com',
                    'title' => 'Docteur',
                    'rank' => 18,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Pr.',
                    'lastname' => 'Colas',
                    'firstname' => 'Dominique',
                    'email' => 'anais.marques@orange.fr',
                    'title' => '',
                    'rank' => 19,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Me.',
                    'lastname' => 'Merle',
                    'firstname' => 'Céline',
                    'email' => 'roche.dominique@wanadoo.fr',
                    'title' => '',
                    'rank' => 20,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Dr.',
                    'lastname' => 'Leblanc',
                    'firstname' => 'Denis',
                    'email' => 'raymond16@dbmail.com',
                    'title' => '',
                    'rank' => 21,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mlle',
                    'lastname' => 'Bousquet',
                    'firstname' => 'Élise',
                    'email' => 'susanne.lefevre@garcia.com',
                    'title' => '',
                    'rank' => 22,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Me.',
                    'lastname' => 'Rey',
                    'firstname' => 'Denise',
                    'email' => 'marguerite82@gosselin.com',
                    'title' => '',
                    'rank' => 23,
                ],
            ]
        );
    }

    protected function setupActorsActorGroups(): void
    {
        $this->save(
            $this->actorsActorGroups,
            [
                ['actor_id' => $this->record($this->actors, 'vincent53@sfr.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'alves.daniel@brun.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'nicolas25@club-internet.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'eric.petitjean@dupre.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'vrobert@wanadoo.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'begue.anne@dbmail.com'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'richard.adam@auger.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'diane.ollivier@tessier.net'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'stephane06@club-internet.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'etienne75@tele2.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'eleonore86@free.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'elodie27@live.com'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'lamy.anne@dbmail.com'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'monique86@live.com'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'jacques.remy@hotmail.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'martine.bodin@nicolas.com'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1')],
                ['actor_id' => $this->record($this->actors, 'dominique.ramos@club-internet.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 2')],
                ['actor_id' => $this->record($this->actors, 'leroux.alfred@lefort.com'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 2')],
                ['actor_id' => $this->record($this->actors, 'anais.marques@orange.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 2')],
                ['actor_id' => $this->record($this->actors, 'roche.dominique@wanadoo.fr'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 2')],
                ['actor_id' => $this->record($this->actors, 'raymond16@dbmail.com'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 2')],
                ['actor_id' => $this->record($this->actors, 'susanne.lefevre@garcia.com'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 2')],
                ['actor_id' => $this->record($this->actors, 'marguerite82@gosselin.com'), 'actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 2')],
            ]
        );
    }

    protected function setupActorGroupsTypesittings(): void
    {
        $this->save(
            $this->actorGroupsTypesittings,
            [
                ['actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 1'), 'typesitting_id' => $this->record($this->typesittings, 'Conseil Municipal')],
                ['actor_group_id' => $this->record($this->actorGroups, 'Louhans - Groupe 2'), 'typesitting_id' => $this->record($this->typesittings, 'Conseil Municipal')],
            ]
        );
    }

    protected function setupDraftTemplates(): void
    {
        $this->save(
            $this->draftTemplates,
            [
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 1, 'name' => 'Gabarit de projet'],
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 2, 'name' => 'Gabarit d\'acte'],
            ]
        );
    }

    protected function setupDraftTemplatesTypesacts(): void
    {
        $this->save(
            $this->draftTemplatesTypesacts,
            [
                ['draft_template_id' => $this->record($this->draftTemplates, 'Gabarit de projet'), 'typesact_id' => $this->record($this->typesacts, 'Délibération')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'Gabarit d\'acte'), 'typesact_id' => $this->record($this->typesacts, 'Délibération')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'Gabarit d\'acte'), 'typesact_id' => $this->record($this->typesacts, 'Arrêté réglementaire')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'Gabarit d\'acte'), 'typesact_id' => $this->record($this->typesacts, 'Arrêté individuel')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'Gabarit d\'acte'), 'typesact_id' => $this->record($this->typesacts, 'Contrat et convention')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'Gabarit d\'acte'), 'typesact_id' => $this->record($this->typesacts, 'Autre')],
            ]
        );
    }

    protected function setupDraftTemplatesFiles(): void
    {
        $this->save(
            $this->files,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'gabarit_projet_v1.odt',
                    'path' => $this->paths['draft_template_project']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['draft_template_project']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'Gabarit de projet'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'gabarit_acte_v1.odt',
                    'path' => $this->paths['draft_template_act']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['draft_template_act']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'Gabarit d\'acte'),
                ],
            ]
        );
    }

    protected function setupSittings(): void
    {
        $this->save(
            $this->sittings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'Conseil Municipal'),
                    'date' => "{$this->year}-06-17 20:00:00",
                ],
            ]
        );
    }

    protected function setupSittingsStatesittings(): void
    {
        $this->save(
            $this->sittingsStatesittings,
            [
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-06-17 20:00:00"), 'statesitting_id' => 1],
            ]
        );
    }

    protected function setupWorkflows(): void
    {
    }

    protected function setup(): void
    {
        $this->execute('BEGIN;');
        $this->setupOrganizations();
        $this->setupStructures();
        $this->setupStructureSettings();
        $this->setupOfficials();
        $this->setupUsers();
        $this->setupStructuresUsers();
        $this->setupNotificationsUsers();
        $this->setupRoles();
        $this->setupRolesUsers();
        $this->setupConnecteurs();
        $this->setupPastellfluxtypes();
        $this->setupGenerateTemplates();
        $this->setupGenerateTemplatesFiles();
        $this->setupDpos();
        $this->setupThemes();
        $this->setupNatures();
        $this->setupMatieres();
        $this->setupClassifications();
        $this->setupTypespiecesjointes();
        $this->setupTypesittings();
        $this->setupSequences();
        $this->setupCounters();
        $this->setupTypesacts();
        $this->setupTypesactsTypesittings();
        $this->setupActorGroups();
        $this->setupActors();
        $this->setupActorsActorGroups();
        $this->setupActorGroupsTypesittings();
        $this->setupDraftTemplates();
        $this->setupDraftTemplatesTypesacts();
        $this->setupDraftTemplatesFiles();
        $this->setupSittings();
        $this->setupSittingsStatesittings();
        $this->execute('COMMIT;');

        $this->setupWorkflows();
    }

    protected function createProjects(): void
    {
        $max = $this->env('LIBRICIEL_RECETTE_DEMO1_NOMBRE_PROJETS', 0);
        for ($idx = 1; $idx <= $max; $idx++) {
            $this->addProjectWithGeneration(
                $this->current($this->organizations),
                $this->current($this->structures),
                $this->record($this->users, 's.blanc'),
                $this->record($this->themes, 'Affaires juridiques'),
                $this->record($this->typesacts, 'Délibération'),
                $this->record($this->draftTemplates, 'Gabarit d\'acte'),
                $this->record($this->draftTemplates, 'Gabarit de projet'),
            );
        }
    }

    public function run(): void
    {
        parent::run();

        $this->sequences();

        $this->year = (int)date('Y');

        $dirs = [
            'GenerateTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesGenerateTemplates' . DS,
            'DraftTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesDraftTemplates' . DS,
        ];
        $this->paths = [
            'modele_projet_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_project.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 1)),
            'modele_acte_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_act.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 2)),
            'modele_convocation_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_project.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 3)),
            'modele_note_de_synthese_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_project.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 4)),
            'modele_liste_des_deliberations_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_deliberations_list.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 5)),
            'modele_proces_verbal_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_verbal_trial.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 6)),
            //------------------------------------------------------------------------------------------------------------------
            'draft_template_project' => $this->createFile($dirs['DraftTemplates'] . 'draft_template_project.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 1)),
            'draft_template_act' => $this->createFile($dirs['DraftTemplates'] . 'draft_template_act.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 2)),
        ];

        $this->setup();

        $this->sequences();
        $this->createProjects();
        $this->sequences();

        $this->permissions();
    }
}
