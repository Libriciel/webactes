<?php
declare(strict_types=1);

use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;

/**
 * @deprecated
 */
class LibricielRecetteOldWorkflowsSeed extends LibricielRecetteAbstractStructureSeed
{
    /**
     * @inheritDoc
     */
    public function run(): void
    {
        parent::run();

        $this->call('LibricielRecette.LibricielRecetteOldWorkflowsStructure1Seed');
        $this->call('LibricielRecette.LibricielRecetteOldWorkflowsStructure2Seed');
        $this->call('LibricielRecette.LibricielRecetteOldWorkflowsStructure4Seed');
    }
}
