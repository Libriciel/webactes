<?php
declare(strict_types=1);

use LibricielRecette\Seeds\LibricielRecetteAbstractWorkflowSeed;

/**
 * @deprecated
 */
class LibricielRecetteOldWorkflowsStructure2Seed extends LibricielRecetteAbstractWorkflowSeed
{
    /**
     * @inheritDoc
     */
    public function run(): void
    {
        parent::run();

        $this->apiConnect('admin@wa-s2');

        $this->apiAddWorkflow(
            [
                'name' => 'S2 Une étape simple',
                'description' => 'Une seule étape simple',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Anthony S2Valideur1',
                        'users' => [$this->getUserId('valid-1@wa-s2')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S2 Validation rédacteur 1',
                'description' => 'Validation par le rédacteur1',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Christophe S2Rédac1',
                        'users' => [$this->getUserId('redac-1@wa-s2')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S2 Une étape concurrente',
                'description' => '',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Concurrent: [Anthony S2Valideur1] OU (Jeanne S2Valideur3)',
                        'users' => [
                            $this->getUserId('valid-1@wa-s2'),
                            $this->getUserId('valid-3@wa-s2'),
                        ],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S2 Deux étapes simples',
                'description' => 'Succession de deux étapes simples',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Anthony S2Valideur1',
                        'users' => [$this->getUserId('valid-1@wa-s2')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple : Maurice S2Valideur2',
                        'users' => [$this->getUserId('valid-2@wa-s2')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S2 Circuit désactivé',
                'description' => 'Circuit désactivé',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple : Maurice S2Valideur2',
                        'users' => [$this->getUserId('valid-2@wa-s2')],
                    ],
                ],
            ],
            false
        );
    }
}
