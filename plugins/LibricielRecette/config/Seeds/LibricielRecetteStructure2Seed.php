<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use LibricielRecette\Model\Enum\GenerateTemplateType;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;

class LibricielRecetteStructure2Seed extends LibricielRecetteAbstractStructureSeed
{
    protected ?int $year;

    /**
     * @var \SplFileInfo[]
     */
    protected array $paths = [];

    protected function setupOrganizations(): void
    {
        $this->save(
            $this->organizations,
            [
                [
                    'name' => 'Recette WA structure 2',
                ],
            ]
        );
    }

    protected function setupStructures(): void
    {
        $this->save(
            $this->structures,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'business_name' => 'Recette WA S2',
                    'id_orchestration' => $this->env('LIBRICIEL_RECETTE_S2_PASTELL_ID_E', null),
                    'spinner' => 'ball-clip-rotate',
                ],
            ]
        );
    }

    protected function setupStructureSettings(): void
    {
        $this->save(
            $this->structureSettings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'project',
                    'value' => '{"project_generate":true,"project_workflow":true,"project_writing":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'act',
                    'value' => '{"act_generate":true,"generate_number":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'convocation',
                    'value' => '{"convocation_idelibre":false,"convocation_mailsec":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'sitting',
                    'value' => '{"sitting_enable":true,"sitting_generate_deliberations_list":true,"sitting_generate_verbal_trial":true}',
                ],
            ]
        );
    }

    protected function setupOfficials(): void
    {
        $this->save(
            $this->officials,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S2 LOSSERAND',
                    'firstname' => 'Frédéric',
                    'email' => 'contact@libriciel.coop',
                    'address' => '140 Rue Aglaonice de Thessalie',
                    'address_supplement' => '',
                    'post_code' => '34170',
                    'city' => 'Castelnau-le-Lez',
                    'phone' => '04 67 65 96 44',
                ],
            ]
        );
    }

    protected function setupUsers(): void
    {
        $this->save(
            $this->users,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S2 ADMIN',
                    'firstname' => 'Henriette',
                    'username' => 'admin@wa-s2',
                    'email' => 'wa-s2.admin@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S2 ADMINFONC',
                    'firstname' => 'Ernest',
                    'username' => 'adminfonc@wa-s2',
                    'email' => 'wa-s2.adminfonc@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S2 RÉDACTEUR 1',
                    'firstname' => 'Christophe',
                    'username' => 'redac-1@wa-s2',
                    'email' => 'wa-s2.redac-1@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => 'S2 RÉDACTEUR 2',
                    'firstname' => 'Marie-Pierre',
                    'username' => 'redac-2@wa-s2',
                    'email' => 'wa-s2.redac-2@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S2 VALIDEUR 1',
                    'firstname' => 'Anthony',
                    'username' => 'valid-1@wa-s2',
                    'email' => 'wa-s2.valid-1@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S2 VALIDEUR 2',
                    'firstname' => 'Maurice',
                    'username' => 'valid-2@wa-s2',
                    'email' => 'wa-s2.valid-2@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => 'S2 VALIDEUR 3',
                    'firstname' => 'Jeanne',
                    'username' => 'valid-3@wa-s2',
                    'email' => 'wa-s2.valid-3@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => 'S2 VALIDEUR 4',
                    'firstname' => 'Julie',
                    'username' => 'valid-4@wa-s2',
                    'email' => 'wa-s2.valid-4@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => 'S2 VALIDEUR 5',
                    'firstname' => 'Béatrice',
                    'username' => 'valid-5@wa-s2',
                    'email' => 'wa-s2.valid-5@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S2 SECRETAIRE',
                    'firstname' => 'Jérôme',
                    'username' => 'secretaire@wa-s2',
                    'email' => 'wa-s2.secretaire@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => 'S2 SUPERADMIN',
                    'firstname' => 'Aria',
                    'username' => 'superadmin@wa-s2',
                    'email' => 'wa-s2.superadmin@' . $this->domain(),
                ],
            ]
        );
    }

    protected function setupStructuresUsers(): void
    {
        $this->save(
            $this->structuresUsers,
            [
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'admin@wa-s2')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'adminfonc@wa-s2')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'redac-1@wa-s2')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'redac-2@wa-s2')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-1@wa-s2')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-2@wa-s2')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-3@wa-s2')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-4@wa-s2')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-5@wa-s2')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'secretaire@wa-s2')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'superadmin@wa-s2')],
            ]
        );
    }

    protected function setupNotificationsUsers(): void
    {
        $this->save(
            $this->notificationsUsers,
            [
                ['user_id' => $this->record($this->users, 'admin@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'admin@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'admin@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'admin@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'admin@wa-s2'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s2'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s2'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s2'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s2'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s2'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s2'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s2'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-5@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-5@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-5@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-5@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-5@wa-s2'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'secretaire@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'secretaire@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'secretaire@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'secretaire@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'secretaire@wa-s2'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'superadmin@wa-s2'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'superadmin@wa-s2'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'superadmin@wa-s2'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'superadmin@wa-s2'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'superadmin@wa-s2'), 'notification_id' => 5],
            ]
        );
    }

    protected function setupRoles(): void
    {
        $this->save(
            $this->roles,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur Fonctionnel',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Rédacteur / Valideur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Secrétariat général',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Super Administrateur',
                ],
            ]
        );
    }

    protected function setupRolesUsers(): void
    {
        $this->save(
            $this->rolesUsers,
            [
                ['role_id' => $this->record($this->roles, 'Administrateur'), 'user_id' => $this->record($this->users, 'admin@wa-s2')],
                ['role_id' => $this->record($this->roles, 'Administrateur Fonctionnel'), 'user_id' => $this->record($this->users, 'adminfonc@wa-s2')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'redac-1@wa-s2')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'redac-2@wa-s2')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-1@wa-s2')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-2@wa-s2')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-3@wa-s2')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-4@wa-s2')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-5@wa-s2')],
                ['role_id' => $this->record($this->roles, 'Secrétariat général'), 'user_id' => $this->record($this->users, 'secretaire@wa-s2')],
                ['role_id' => $this->record($this->roles, 'Super Administrateur'), 'user_id' => $this->record($this->users, 'superadmin@wa-s2')],
            ]
        );
    }

    protected function setupConnecteurs(): void
    {
        $env = getenv();
        $this->save(
            $this->connecteurs,
            [
                [
                    'name' => 'pastell',
                    'url' => "{$env['LIBRICIEL_RECETTE_S2_PASTELL_URL']}/",
                    'tdt_url' => "{$env['LIBRICIEL_RECETTE_S2_PASTELL_TDT_URL']}/modules/actes/actes_transac_post_confirm_api.php",
                    'username' => $env['LIBRICIEL_RECETTE_S2_PASTELL_USERNAME'],
                    'password' => $env['LIBRICIEL_RECETTE_S2_PASTELL_PASSWORD'],
                    'structure_id' => $this->current($this->structures),
                    'connector_type_id' => 1,
                    'connexion_option' => $env['LIBRICIEL_RECETTE_S2_PASTELL_CONNEXION_OPTION'],
                ],
            ]
        );
    }

    protected function setupPastellfluxtypes(): void
    {
        $this->save(
            $this->pastellfluxtypes,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'actes-generique',
                    'description' => 'Actes générique',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'mailsec',
                    'description' => 'Mail securisé',
                ],
            ]
        );
    }

    protected function setupGenerateTemplates(): void
    {
        $this->save(
            $this->generateTemplates,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::PROJECT,
                    'name' => 'Modèle de projet d\'arrêté',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::PROJECT,
                    'name' => 'Modèle de projet de délibération',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::CONVOCATION,
                    'name' => 'Modèle de convocation',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::EXECUTIVE_SUMMARY,
                    'name' => 'Modèle de note de synthèse',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::DELIBERATIONS_LIST,
                    'name' => 'Modèle de liste des délibérations',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::VERBAL_TRIAL,
                    'name' => 'Modèle de procès-verbal',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::ACT,
                    'name' => 'Modèle d\'arrêté',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::ACT,
                    'name' => 'Modèle de délibération',
                ],
            ]
        );
    }

    protected function setupGenerateTemplatesFiles(): void
    {
        $this->save(
            $this->files,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S2 Projet arrete.odt'),
                    'path' => $this->paths['S2 Projet arrêté']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S2 Projet arrêté']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de projet d\'arrêté'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S2 Projet deliberation.odt'),
                    'path' => $this->paths['S2 Projet délibération']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S2 Projet délibération']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de projet de délibération'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S2 Convocation.odt'),
                    'path' => $this->paths['S2 Convocation']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S2 Convocation']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S2 Note de synthese.odt'),
                    'path' => $this->paths['S2 Note de synthèse']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S2 Note de synthèse']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S2 Liste des deliberations.odt'),
                    'path' => $this->paths['S2 Liste des délibérations']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S2 Liste des délibérations']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S2 Proces-verbal.odt'),
                    'path' => $this->paths['S2 Procès-verbal']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S2 Procès-verbal']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S2 Acte.odt'),
                    'path' => $this->paths['S2 Acte']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S2 Acte']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle d\'arrêté'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S2 Deliberation.odt'),
                    'path' => $this->paths['S2 Délibération']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S2 Délibération']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de délibération'),
                ],
            ]
        );
    }

    protected function setupDpos(): void
    {
        $this->save(
            $this->dpos,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S2 DPO',
                    'firstname' => 'Théo',
                    'email' => 'wa-s2.dpo@' . $this->domain(),
                ],
            ]
        );
    }

    protected function setupThemes(): void
    {
        $this->save(
            $this->themes,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Moyens généraux',
                    'position' => '1',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Finances',
                    'parent_id' => $this->id('themes', 1),
                    'position' => '10',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Comptabilité',
                    'parent_id' => $this->id('themes', 2),
                    'position' => '101',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Gestion',
                    'parent_id' => $this->id('themes', 3),
                    'position' => '1010',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Ressources humaines',
                    'parent_id' => $this->id('themes', 1),
                    'position' => '11',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 commande publique',
                    'parent_id' => $this->id('themes', 1),
                    'position' => '12',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Service à la population',
                    'position' => '2',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Techniques',
                    'position' => '3',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Environnement',
                    'parent_id' => $this->id('themes', 8),
                    'position' => '30',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Eau potable',
                    'parent_id' => $this->id('themes', 9),
                    'position' => '301',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'active' => false,
                    'name' => 'S2 Thème désactivé',
                    'position' => '4',
                ],
            ]
        );
    }

    protected function setupTypesittings(): void
    {
        $this->save(
            $this->typesittings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Conseil Marjo',
                    'isdeliberating' => true,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                    'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Commission urbanisme',
                    'isdeliberating' => false,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                    'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Conseil communautaire',
                    'isdeliberating' => false,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                    'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Conseil municipal',
                    'isdeliberating' => true,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                    'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Type séance désactivé',
                    'active' => false,
                    'isdeliberating' => true,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                    'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
            ]
        );
    }

    protected function setupSequences(): void
    {
        $this->save(
            $this->sequences,
            [
                [
                    'name' => 'S2 Arrêtés',
                    'comment' => 'Séquence pour les arrêtés',
                    'sequence_num' => 0,
                    'structure_id' => $this->current($this->structures),
                ],
                [
                    'name' => 'S2 Délibérations',
                    'comment' => 'Séquences délibérations',
                    'sequence_num' => 0,
                    'structure_id' => $this->current($this->structures),
                ],
            ]
        );
    }

    protected function setupCounters(): void
    {
        $this->save(
            $this->counters,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Arrêtés',
                    'counter_def' => 'S2_ARR_#SSSS#',
                    'sequence_id' => $this->record($this->sequences, 'S2 Arrêtés'),
                    'reinit_def' => '#AAAA#',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Délibérations',
                    'counter_def' => 'S2_DEL_#0000#',
                    'sequence_id' => $this->record($this->sequences, 'S2 Délibérations'),
                    'reinit_def' => '#AAAA#',
                ],
            ]
        );
    }

    protected function setupTypesacts(): void
    {
        $this->save(
            $this->typesacts,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'DE'),
                    'name' => 'S2 Délibération',
                    'istdt' => true,
                    'isdefault' => true,
                    'counter_id' => $this->record($this->counters, 'S2 Délibérations'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet de délibération'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle de délibération'),
                    'isdeliberating' => true,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AR'),
                    'name' => 'S2 Arrêté TDT',
                    'istdt' => true,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'S2 Arrêtés'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet d\'arrêté'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle d\'arrêté'),
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AI'),
                    'name' => 'S2 Arrêté non TDT',
                    'istdt' => false,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'S2 Arrêtés'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet d\'arrêté'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle d\'arrêté'),
                    'isdeliberating' => false,
                ],
            ]
        );
    }

    protected function setupTypesactsTypesittings(): void
    {
        $this->save(
            $this->typesactsTypesittings,
            [
                ['typesact_id' => $this->record($this->typesacts, 'S2 Délibération'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil Marjo')],
                ['typesact_id' => $this->record($this->typesacts, 'S2 Délibération'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Commission urbanisme')],
                ['typesact_id' => $this->record($this->typesacts, 'S2 Délibération'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil communautaire')],
                ['typesact_id' => $this->record($this->typesacts, 'S2 Délibération'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil municipal')],
                ['typesact_id' => $this->record($this->typesacts, 'S2 Délibération'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Type séance désactivé')],
            ]
        );
    }

    protected function setupActorGroups(): void
    {
        $this->save(
            $this->actorGroups,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Majorité',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Opposition',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Commission urbanisme',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Commission voirie',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Invités',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S2 Groupe désactivé',
                ],
            ]
        );
    }

    protected function setupActors(): void
    {
        $this->save(
            $this->actors,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S2 DODIN',
                    'firstname' => 'Louis',
                    'email' => 'wa-s2.acteur.dodin@' . $this->domain(),
                    'title' => 'Mr le président',
                    'cellphone' => '0645784568',
                    'rank' => 1,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mme',
                    'lastname' => 'S2 DAVIS',
                    'firstname' => 'Victoria',
                    'email' => 'wa-s2.acteur.davis@' . $this->domain(),
                    'title' => 'Adjointe au président',
                    'cellphone' => '0654721539',
                    'rank' => 2,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S2 MAROIS',
                    'firstname' => 'Chandler',
                    'email' => 'wa-s2.acteur.marois@' . $this->domain(),
                    'title' => 'Adjoint voirie',
                    'rank' => 3,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mme',
                    'lastname' => 'S2 PELLERIN',
                    'firstname' => 'Léonie',
                    'email' => 'wa-s2.acteur.pellerin@' . $this->domain(),
                    'title' => 'Deuxième adjoint voierie',
                    'rank' => 4,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'active' => false,
                    'civility' => 'Mme',
                    'lastname' => 'S2 CHAPIN',
                    'firstname' => 'Laurence',
                    'email' => 'wa-s2.acteur.chapin@' . $this->domain(),
                    'title' => 'Troisième adjointe voierie',
                    'rank' => 5,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S2 ALLAIN',
                    'firstname' => 'Alexandre',
                    'email' => 'wa-s2.acteur.allain@' . $this->domain(),
                    'title' => '',
                    'cellphone' => '0612245687',
                    'rank' => 7,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mme',
                    'lastname' => 'S2 LANOIE',
                    'firstname' => 'Gabrielle',
                    'email' => 'wa-s2.acteur.lanoie@' . $this->domain(),
                    'title' => 'Leader de l\'opposition',
                    'rank' => 6,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S2 THERRIAULT',
                    'firstname' => 'Martin',
                    'email' => 'wa-s2.acteur.therriault@' . $this->domain(),
                    'title' => '',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'active' => false,
                    'civility' => 'Mme',
                    'lastname' => 'S2 BOSSÉ',
                    'firstname' => 'France',
                    'email' => 'wa-s2.acteur.bosse@' . $this->domain(),
                    'title' => '',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mme',
                    'lastname' => 'S2 MOUET',
                    'firstname' => 'Aubert',
                    'email' => 'wa-s2.acteur.mouet@' . $this->domain(),
                    'title' => '',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S2 LAPOINTE',
                    'firstname' => 'Georges',
                    'email' => 'wa-s2.acteur.lapointe@' . $this->domain(),
                    'title' => 'Presse',
                    'cellphone' => '0705048542',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'active' => false,
                    'civility' => 'M.',
                    'lastname' => 'S2 ROSSIGNOL',
                    'firstname' => 'Stéphane',
                    'email' => 'wa-s2.acteur.rossignol@' . $this->domain(),
                    'title' => 'Presse',
                ],
            ]
        );
    }

    protected function setupActorsActorGroups(): void
    {
        $this->save(
            $this->actorsActorGroups,
            [
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.dodin@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.dodin@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Commission urbanisme')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.davis@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.davis@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Commission urbanisme')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.davis@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Commission voirie')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.marois@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.marois@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Commission voirie')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.pellerin@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Commission voirie')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.chapin@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.chapin@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Commission voirie')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.allain@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Opposition')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.allain@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Commission voirie')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.lanoie@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Opposition')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.therriault@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Opposition')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.therriault@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Commission voirie')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.bosse@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Opposition')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.mouet@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Invités')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.lapointe@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Invités')],
                ['actor_id' => $this->record($this->actors, 'wa-s2.acteur.rossignol@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S2 Invités')],
            ]
        );
    }

    protected function setupActorGroupsTypesittings(): void
    {
        $this->save(
            $this->actorGroupsTypesittings,
            [
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Majorité'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil Marjo')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Opposition'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil Marjo')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Invités'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil Marjo')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Commission urbanisme'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Commission urbanisme')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Majorité'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil communautaire')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Opposition'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil communautaire')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Majorité'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil municipal')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Opposition'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil municipal')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Opposition'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Type séance désactivé')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Commission urbanisme'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Type séance désactivé')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S2 Commission voirie'), 'typesitting_id' => $this->record($this->typesittings, 'S2 Type séance désactivé')],
            ]
        );
    }

    protected function setupDraftTemplates(): void
    {
        $this->save(
            $this->draftTemplates,
            [
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 2, 'name' => 'S2 acte arrêté'],
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 1, 'name' => 'S2 projet arrêté'],
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 2, 'name' => 'S2 acte délibération'],
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 1, 'name' => 'S2 projet délibération'],
            ]
        );
    }

    protected function setupDraftTemplatesTypesacts(): void
    {
        $this->save(
            $this->draftTemplatesTypesacts,
            [
                ['draft_template_id' => $this->record($this->draftTemplates, 'S2 acte délibération'), 'typesact_id' => $this->record($this->typesacts, 'S2 Délibération')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S2 projet délibération'), 'typesact_id' => $this->record($this->typesacts, 'S2 Délibération')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S2 acte arrêté'), 'typesact_id' => $this->record($this->typesacts, 'S2 Arrêté TDT')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S2 projet arrêté'), 'typesact_id' => $this->record($this->typesacts, 'S2 Arrêté TDT')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S2 acte arrêté'), 'typesact_id' => $this->record($this->typesacts, 'S2 Arrêté non TDT')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S2 projet arrêté'), 'typesact_id' => $this->record($this->typesacts, 'S2 Arrêté non TDT')],
            ]
        );
    }

    protected function setupDraftTemplatesFiles(): void
    {
        $this->save(
            $this->files,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'gabarit_arrete_libriciel.odt',
                    'path' => $this->paths['gabarit_arrete_libriciel']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['gabarit_arrete_libriciel']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'S2 acte arrêté'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'gabarit_projet_libriciel.odt',
                    'path' => $this->paths['gabarit_projet_libriciel']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['gabarit_projet_libriciel']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'S2 projet arrêté'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'texte_delib2.odt',
                    'path' => $this->paths['texte_delib2']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['texte_delib2']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'S2 acte délibération'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'texte_projet.odt',
                    'path' => $this->paths['texte_projet']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['texte_projet']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'S2 projet délibération'),
                ],
            ]
        );
    }

    protected function setupSittings(): void
    {
        $this->save(
            $this->sittings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'S2 Commission urbanisme'),
                    'date' => "{$this->year}-02-07 12:00:00",
                    'president_id' => $this->record($this->actors, 'wa-s2.acteur.dodin@' . $this->domain()),
                    'secretary_id' => $this->record($this->actors, 'wa-s2.acteur.davis@' . $this->domain()),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil communautaire'),
                    'date' => "{$this->year}-02-08 13:00:00",
                    'president_id' => $this->record($this->actors, 'wa-s2.acteur.dodin@' . $this->domain()),
                    'secretary_id' => $this->record($this->actors, 'wa-s2.acteur.davis@' . $this->domain()),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil municipal'),
                    'date' => "{$this->year}-02-09 14:00:00",
                    'president_id' => $this->record($this->actors, 'wa-s2.acteur.dodin@' . $this->domain()),
                    'secretary_id' => $this->record($this->actors, 'wa-s2.acteur.davis@' . $this->domain()),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'S2 Conseil Marjo'),
                    'date' => "{$this->year}-02-10 15:00:00",
                    'president_id' => $this->record($this->actors, 'wa-s2.acteur.dodin@' . $this->domain()),
                    'secretary_id' => $this->record($this->actors, 'wa-s2.acteur.davis@' . $this->domain()),
                ],
            ]
        );
    }

    protected function setupSittingsStatesittings(): void
    {
        $this->save(
            $this->sittingsStatesittings,
            [
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-02-07 12:00:00"), 'statesitting_id' => 1],
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-02-08 13:00:00"), 'statesitting_id' => 1],
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-02-09 14:00:00"), 'statesitting_id' => 1],
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-02-10 15:00:00"), 'statesitting_id' => 1],
            ]
        );
    }

    protected function setupWorkflows(): void
    {
        $this->apiConnect('admin@wa-s2');

        $this->apiAddWorkflow(
            [
                'name' => 'S2 Une étape simple',
                'description' => 'Une seule étape simple',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Anthony S2Valideur1',
                        'users' => [$this->getUserId('valid-1@wa-s2')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S2 Validation rédacteur 1',
                'description' => 'Validation par le rédacteur1',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Christophe S2Rédac1',
                        'users' => [$this->getUserId('redac-1@wa-s2')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S2 Une étape concurrente',
                'description' => '',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Concurrent: [Anthony S2Valideur1] OU (Jeanne S2Valideur3)',
                        'users' => [
                            $this->getUserId('valid-1@wa-s2'),
                            $this->getUserId('valid-3@wa-s2'),
                        ],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S2 Deux étapes simples',
                'description' => 'Succession de deux étapes simples',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Anthony S2Valideur1',
                        'users' => [$this->getUserId('valid-1@wa-s2')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple : Maurice S2Valideur2',
                        'users' => [$this->getUserId('valid-2@wa-s2')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S2 Circuit désactivé',
                'description' => 'Circuit désactivé',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple : Maurice S2Valideur2',
                        'users' => [$this->getUserId('valid-2@wa-s2')],
                    ],
                ],
            ],
            false
        );
    }

    protected function setup(): void
    {
        $this->execute('BEGIN;');
        $this->setupOrganizations();
        $this->setupStructures();
        $this->setupStructureSettings();
        $this->setupOfficials();
        $this->setupUsers();
        $this->setupStructuresUsers();
        $this->setupNotificationsUsers();
        $this->setupRoles();
        $this->setupRolesUsers();
        $this->setupConnecteurs();
        $this->setupPastellfluxtypes();
        $this->setupGenerateTemplates();
        $this->setupGenerateTemplatesFiles();
        $this->setupDpos();
        $this->setupThemes();
        $this->setupNatures();
        $this->setupMatieres();
        $this->setupClassifications();
        $this->setupTypespiecesjointes();
        $this->setupTypesittings();
        $this->setupSequences();
        $this->setupCounters();
        $this->setupTypesacts();
        $this->setupTypesactsTypesittings();
        $this->setupActorGroups();
        $this->setupActors();
        $this->setupActorsActorGroups();
        $this->setupActorGroupsTypesittings();
        $this->setupDraftTemplates();
        $this->setupDraftTemplatesTypesacts();
        $this->setupDraftTemplatesFiles();
        $this->setupSittings();
        $this->setupSittingsStatesittings();
        $this->execute('COMMIT;');

        $this->setupWorkflows();
    }

    protected function createProjects(): void
    {
        $max = $this->env('LIBRICIEL_RECETTE_S2_NOMBRE_PROJETS', 10);
        for ($idx = 1; $idx <= $max; $idx++) {
            $this->addProjectWithGeneration(
                $this->current($this->organizations),
                $this->current($this->structures),
                $this->record($this->users, 'admin@wa-s2'),
                $this->record($this->themes, 'S2 Gestion'),
                $this->record($this->typesacts, 'S2 Délibération'),
                $this->record($this->draftTemplates, 'S2 acte délibération'),
                $this->record($this->draftTemplates, 'S2 projet délibération'),
            );
        }
    }

    public function run(): void
    {
        parent::run();

        $this->sequences();

        $this->year = (int)date('Y');

        $dirs = [
            'GenerateTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesGenerateTemplates' . DS,
            'DraftTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesDraftTemplates' . DS,
        ];
        $this->paths = [
            'S2 Projet arrêté' => $this->createFile($dirs['GenerateTemplates'] . 'S2 Projet arrete.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 1)),
            'S2 Projet délibération' => $this->createFile($dirs['GenerateTemplates'] . 'S2 Projet deliberation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 2)),
            'S2 Convocation' => $this->createFile($dirs['GenerateTemplates'] . 'S2 Convocation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 3)),
            'S2 Note de synthèse' => $this->createFile($dirs['GenerateTemplates'] . 'S2 Note de synthese.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 4)),
            'S2 Liste des délibérations' => $this->createFile($dirs['GenerateTemplates'] . 'S2 Liste des deliberations.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 5)),
            'S2 Procès-verbal' => $this->createFile($dirs['GenerateTemplates'] . 'S2 Proces-verbal.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 6)),
            'S2 Acte' => $this->createFile($dirs['GenerateTemplates'] . 'S2 Acte.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 7)),
            'S2 Délibération' => $this->createFile($dirs['GenerateTemplates'] . 'S2 Deliberation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 8)),
            //------------------------------------------------------------------------------------------------------------------
            'gabarit_arrete_libriciel' => $this->createFile($dirs['DraftTemplates'] . 'gabarit_arrete_libriciel.odt', 'draft_template_id', 1, $this->id('structures', 1), $this->id('draft_templates', 1)),
            'gabarit_projet_libriciel' => $this->createFile($dirs['DraftTemplates'] . 'gabarit_projet_libriciel.odt', 'draft_template_id', 1, $this->id('structures', 1), $this->id('draft_templates', 2)),
            'texte_delib2' => $this->createFile($dirs['DraftTemplates'] . 'texte_delib2.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 3)),
            'texte_projet' => $this->createFile($dirs['DraftTemplates'] . 'texte_projet.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 4)),
        ];

        $this->setup();

        $this->sequences();
        $this->createProjects();
        $this->sequences();

        $this->permissions();
    }
}
