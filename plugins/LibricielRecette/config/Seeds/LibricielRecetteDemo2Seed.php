<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use LibricielRecette\Model\Enum\GenerateTemplateType;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;

/**
 * @deprecated
 */
class LibricielRecetteDemo2Seed extends LibricielRecetteAbstractStructureSeed
{
    protected ?int $year;

    /**
     * @var \SplFileInfo[]
     */
    protected array $paths = [];

    protected function setupOrganizations(): void
    {
        $record = $this->fetchRow("SELECT id, name FROM organizations WHERE name = 'LIBRICIEL SCOP';");
        if ($record) {
            if (!isset($this->records['organizations'])) {
                $this->records['organizations'] = [];
            }
            $this->records['organizations'][$record['name']] = $record['id'];
        } else {
            $this->save(
                $this->organizations,
                [
                    [
                        'name' => 'LIBRICIEL SCOP',
                    ],
                ]
            );
        }
    }

    protected function setupStructures(): void
    {
        $this->save(
            $this->structures,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'business_name' => 'TERNUM',
                    'id_orchestration' => $this->env('LIBRICIEL_RECETTE_DEMO2_PASTELL_ID_E', 11),
                    'spinner' => 'ball-clip-rotate',
                ],
            ]
        );
    }

    protected function setupStructureSettings(): void
    {
    }

    protected function setupOfficials(): void
    {
    }

    protected function setupUsers(): void
    {
        $this->save(
            $this->users,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'White',
                    'firstname' => 'Sébastien',
                    'username' => 's.white',
                    'email' => 'sebastien.white@test.fr',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'Green',
                    'firstname' => 'Mickael',
                    'username' => 'm.green',
                    'email' => 'mickael.green@test.fr',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'Yellow',
                    'firstname' => 'Valérie',
                    'username' => 'v.yellow',
                    'email' => 'valerie.yellow@test.fr',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'Purple',
                    'firstname' => 'Stéphane',
                    'username' => 's.purple',
                    'email' => 'stephane.purple@test.fr',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme',
                    'lastname' => 'Lime',
                    'firstname' => 'Emilie',
                    'username' => 'e.lime',
                    'email' => 'emilie.lime@test.fr',
                ],
            ]
        );
    }

    protected function setupStructuresUsers(): void
    {
        $this->save(
            $this->structuresUsers,
            [
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 's.white')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'm.green')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'v.yellow')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 's.purple')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'e.lime')],
            ]
        );
    }

    protected function setupNotificationsUsers(): void
    {
    }

    protected function setupRoles(): void
    {
        $this->save(
            $this->roles,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Rédacteur / Valideur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Secrétariat général',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur Fonctionnel',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Super Administrateur',
                ],
            ]
        );
    }

    protected function setupRolesUsers(): void
    {
        $this->save(
            $this->rolesUsers,
            [
                ['role_id' => $this->record($this->roles, 'Administrateur'), 'user_id' => $this->record($this->users, 's.white')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'm.green')],
                ['role_id' => $this->record($this->roles, 'Secrétariat général'), 'user_id' => $this->record($this->users, 'v.yellow')],
                ['role_id' => $this->record($this->roles, 'Administrateur Fonctionnel'), 'user_id' => $this->record($this->users, 's.purple')],
                ['role_id' => $this->record($this->roles, 'Super Administrateur'), 'user_id' => $this->record($this->users, 'e.lime')],
            ]
        );
    }

    protected function setupConnecteurs(): void
    {
        $env = getenv();
        $this->save(
            $this->connecteurs,
            [
                [
                    'name' => 'pastell',
                    'url' => "{$env['LIBRICIEL_RECETTE_DEMO2_PASTELL_URL']}/",
                    'tdt_url' => "{$env['LIBRICIEL_RECETTE_DEMO2_PASTELL_TDT_URL']}/modules/actes/actes_transac_post_confirm_api.php",
                    'username' => $env['LIBRICIEL_RECETTE_DEMO2_PASTELL_USERNAME'],
                    'password' => $env['LIBRICIEL_RECETTE_DEMO2_PASTELL_PASSWORD'],
                    'structure_id' => $this->current($this->structures),
                    'connector_type_id' => 1,
                    'connexion_option' => $env['LIBRICIEL_RECETTE_DEMO2_PASTELL_CONNEXION_OPTION'],
                ],
            ]
        );
    }

    protected function setupPastellfluxtypes(): void
    {
    }

    protected function setupGenerateTemplates(): void
    {
        $this->save(
            $this->generateTemplates,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::PROJECT,
                    'name' => 'Modèle de projet',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::ACT,
                    'name' => 'Modèle d\'acte',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::CONVOCATION,
                    'name' => 'Modèle de convocation d\'un acte',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::EXECUTIVE_SUMMARY,
                    'name' => 'Modèle de note de synthèse',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::DELIBERATIONS_LIST,
                    'name' => 'Modèle de liste des délibérations',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::VERBAL_TRIAL,
                    'name' => 'Modèle de procès-verbal',
                ],
            ]
        );
    }

    protected function setupGenerateTemplatesFiles(): void
    {
        $this->save(
            $this->files,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_projet_v1.odt',
                    'path' => $this->paths['modele_projet_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_projet_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de projet'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_acte_v1.odt',
                    'path' => $this->paths['modele_acte_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_acte_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle d\'acte'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_convocation_v1.odt',
                    'path' => $this->paths['modele_convocation_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_convocation_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de convocation d\'un acte'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_note_de_synthese_v1.odt',
                    'path' => $this->paths['modele_note_de_synthese_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_note_de_synthese_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_liste_des_deliberations_v1.odt',
                    'path' => $this->paths['modele_liste_des_deliberations_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_liste_des_deliberations_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'modele_proces_verbal_v1.odt',
                    'path' => $this->paths['modele_proces_verbal_v1']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['modele_proces_verbal_v1']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
            ]
        );
    }

    protected function setupDpos(): void
    {
    }

    protected function setupThemes(): void
    {
    }

    protected function setupTypesittings(): void
    {
        $this->save(
            $this->typesittings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Conseil Communautaire',
                    'isdeliberating' => true,
                ],
            ]
        );
    }

    protected function setupSequences(): void
    {
    }

    protected function setupCounters(): void
    {
    }

    protected function setupTypesacts(): void
    {
        $this->save(
            $this->typesacts,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'DE'),
                    'name' => 'Délibération',
                    'istdt' => true,
                    'isdefault' => true,
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AR'),
                    'name' => 'Arrêté réglementaire',
                    'istdt' => true,
                    'isdefault' => false,
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AI'),
                    'name' => 'Arrêté individuel',
                    'istdt' => false,
                    'isdefault' => false,
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'CC'),
                    'name' => 'Contrat et convention',
                    'istdt' => true,
                    'isdefault' => false,
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AU'),
                    'name' => 'Autre',
                    'istdt' => false,
                    'isdefault' => false,
                    'isdeliberating' => false,
                ],
            ]
        );
    }

    protected function setupTypesactsTypesittings(): void
    {
        $this->save(
            $this->typesactsTypesittings,
            [
                ['typesact_id' => $this->record($this->typesacts, 'Arrêté réglementaire'), 'typesitting_id' => $this->record($this->typesittings, 'Conseil Communautaire')],
            ]
        );
    }

    protected function setupActorGroups(): void
    {
        $this->save(
            $this->actorGroups,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Montceau 2 - Groupe 1',
                ],
            ]
        );
    }

    protected function setupActors(): void
    {
    }

    protected function setupActorsActorGroups(): void
    {
    }

    protected function setupActorGroupsTypesittings(): void
    {
    }

    protected function setupDraftTemplates(): void
    {
    }

    protected function setupDraftTemplatesTypesacts(): void
    {
    }

    protected function setupDraftTemplatesFiles(): void
    {
    }

    protected function setupSittings(): void
    {
    }

    protected function setupSittingsStatesittings(): void
    {
    }

    protected function setupWorkflows(): void
    {
    }

    protected function setup(): void
    {
        $this->execute('BEGIN;');
        $this->setupOrganizations();
        $this->setupStructures();
        $this->setupStructureSettings();
        $this->setupOfficials();
        $this->setupUsers();
        $this->setupStructuresUsers();
        $this->setupNotificationsUsers();
        $this->setupRoles();
        $this->setupRolesUsers();
        $this->setupConnecteurs();
        $this->setupPastellfluxtypes();
        $this->setupGenerateTemplates();
        $this->setupGenerateTemplatesFiles();
        $this->setupDpos();
        $this->setupThemes();
        $this->setupNatures();
        $this->setupMatieres();
        $this->setupClassifications();
        $this->setupTypespiecesjointes();
        $this->setupTypesittings();
        $this->setupSequences();
        $this->setupCounters();
        $this->setupTypesacts();
        $this->setupTypesactsTypesittings();
        $this->setupActorGroups();
        $this->setupActors();
        $this->setupActorsActorGroups();
        $this->setupActorGroupsTypesittings();
        $this->setupDraftTemplates();
        $this->setupDraftTemplatesTypesacts();
        $this->setupDraftTemplatesFiles();
        $this->setupSittings();
        $this->setupSittingsStatesittings();
        $this->execute('COMMIT;');

        $this->setupWorkflows();
    }

    protected function createProjects(): void
    {
        $max = $this->env('LIBRICIEL_RECETTE_DEMO2_NOMBRE_PROJETS', 0);
        for ($idx = 1; $idx <= $max; $idx++) {
            $this->addProjectWithoutGeneration(
                $this->current($this->organizations),
                $this->current($this->structures),
                $this->record($this->users, 's.white'),
                null,
                $this->record($this->typesacts, 'Délibération'),
                sprintf('D2_%d_D_%04d', $this->year, $idx),
                dirname(__DIR__, 2) . DS . 'files' . DS . 'Projects' . DS . 'Projet DEMO2.pdf'
            );
        }
    }

    public function run(): void
    {
        parent::run();

        $this->sequences();

        $this->year = (int)date('Y');

        $dirs = [
            'GenerateTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesGenerateTemplates' . DS,
            'DraftTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesDraftTemplates' . DS,
        ];
        $this->paths = [
            'modele_projet_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_project.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 1)),
            'modele_acte_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_act.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 2)),
            'modele_convocation_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_project.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 3)),
            'modele_note_de_synthese_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_project.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 4)),
            'modele_liste_des_deliberations_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_deliberations_list.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 5)),
            'modele_proces_verbal_v1' => $this->createFile($dirs['GenerateTemplates'] . 'generate_template_verbal_trial.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 6)),
        ];

        $this->setup();

        $this->sequences();
        $this->createProjects();
        $this->sequences();

        $this->permissions();
    }
}
