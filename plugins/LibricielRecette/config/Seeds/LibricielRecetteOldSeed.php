<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * @deprecated
 */
class LibricielRecetteOldSeed extends AbstractSeed
{
    /**
     * Chargement des données pour la recette
     *
     * @return void
     */
    public function run(): void
    {
        parent::run();

        $this->call('LibricielRecette.LibricielRecetteOldStructure1Seed');
        $this->call('LibricielRecette.LibricielRecetteOldStructure2Seed');
        $this->call('LibricielRecette.LibricielRecetteOldStructure3Seed');
        $this->call('LibricielRecette.LibricielRecetteOldStructure4Seed');
    }
}
