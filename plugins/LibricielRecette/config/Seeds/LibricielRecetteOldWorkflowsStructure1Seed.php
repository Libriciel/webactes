<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use LibricielRecette\Seeds\LibricielRecetteAbstractWorkflowSeed;

/**
 * @deprecated
 */
class LibricielRecetteOldWorkflowsStructure1Seed extends LibricielRecetteAbstractWorkflowSeed
{
    /**
     * @inheritDoc
     */
    public function run(): void
    {
        parent::run();

        $this->apiConnect('admin@wa-s1');

        $this->apiAddWorkflow(
            [
                'name' => 'S1 Etapes simples',
                'description' => '4 étapes simples successives',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Jean-Louis S1valideur1',
                        'users' => [$this->getUserId('valid-1@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Paul-Emile S1valideur2',
                        'users' => [$this->getUserId('valid-2@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Marie-Odile S1valideur3',
                        'users' => [$this->getUserId('valid-3@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Juliette S1valideur4',
                        'users' => [$this->getUserId('valid-4@wa-s1')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S1 Simple concurrent simple',
                'description' => '3 étapes dont une concurrente',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Jean-Louis S1valideur1',
                        'users' => [$this->getUserId('valid-1@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Concurrent: Marie-Odile S1valideur3 OU Juliette S1valideur4',
                        'users' => [
                            $this->getUserId('valid-3@wa-s1'),
                            $this->getUserId('valid-4@wa-s1'),
                        ],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Monique S1valideur5',
                        'users' => [$this->getUserId('valid-5@wa-s1')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S1 Admin fonc secretaire',
                'description' => 'Circuit avec intervention admin fonctionnel et secrétaire',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Jean-Louis S1valideur1',
                        'users' => [$this->getUserId('valid-1@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Albert S1Adminfonc',
                        'users' => [$this->getUserId('adminfonc@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Jeannot S1Secretaire1',
                        'users' => [$this->getUserId('secretaire@wa-s1')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S1 Une étape concurrente',
                'description' => '',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Concurrent: (Jean-Louis S1valideur1) / OU / [Paul-Emile S1valideur2] - OU_ Monique S1valideur5',
                        'users' => [
                            $this->getUserId('valid-1@wa-s1'),
                            $this->getUserId('valid-2@wa-s1'),
                            $this->getUserId('valid-5@wa-s1'),
                        ],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S1 Circuit désactivé',
                'description' => 'Circuit désactivé',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple : Monique S1valideur5',
                        'users' => [$this->getUserId('valid-5@wa-s1')],
                    ],
                ],
            ],
            false
        );
    }
}
