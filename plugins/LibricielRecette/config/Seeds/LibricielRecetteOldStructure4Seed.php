<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use Cake\Utility\Hash;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;
use LibricielRecette\Seeds\LibricielRecetteSeedGenerateTemplatesTrait;

/**
 * @deprecated
 */
class LibricielRecetteOldStructure4Seed extends LibricielRecetteAbstractStructureSeed
{
    use LibricielRecetteSeedGenerateTemplatesTrait;

    /**
     * Crée le paramétrage de la structure 4
     *
     * @return void
     */
    protected function setup(): void
    {
        // @info: {$_ENV["..."]} not set
        $env = getenv();
        $year = date('Y');

        // Organizations
        $data = [
            [
                'id' => $this->id('organizations', 1),
                'name' => 'Recette WA structure 4',
            ],
        ];
        $this->organizations->insert($data);

        $dirs = [
            'GenerateTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesGenerateTemplates' . DS,
            'DraftTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesDraftTemplates' . DS,
        ];
        $files = [
            //'S4 Projet arrêté' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Projet arrete.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 1)),
            'S4 Projet délibération' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Projet deliberation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 7)),
            'S4 Convocation' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Convocation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 6)),
            'S4 Note de synthèse' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Note de synthese.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 5)),
            'S4 Liste des délibérations' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Liste des deliberations.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 3)),
            'S4 Procès-verbal' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Proces-verbal.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 4)),
            'S4 Acte' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Acte.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 2)),
            'S4 Délibération' => $this->createFile($dirs['GenerateTemplates'] . 'S4 Deliberation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 1)),
            //------------------------------------------------------------------------------------------------------------------
            'gabarit_delibere_libriciel' => $this->createFile($dirs['DraftTemplates'] . 'gabarit_delibere_libriciel.odt', 'draft_template_id', 1, $this->id('structures', 1), $this->id('draft_templates', 1)),
            'gabarit_decision_libriciel' => $this->createFile($dirs['DraftTemplates'] . 'gabarit_decision_libriciel.odt', 'draft_template_id', 1, $this->id('structures', 1), $this->id('draft_templates', 2)),
        ];

        // Structures
        $data = [
            [
                'id' => $this->id('structures', 1),
                'organization_id' => $this->id('organizations', 1),
                'business_name' => 'Recette WA S4',
                'id_orchestration' => $env['LIBRICIEL_RECETTE_S4_PASTELL_ID_E'],
                'spinner' => 'ball-clip-rotate',
            ],
        ];
        $this->structures->insert($data);

        // StructureSettings
        $data = [
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'act',
                'value' => '{"generate_number":false,"act_generate":true}',
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'convocation',
                'value' => '{"convocation_idelibre":true}',
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'sitting',
                'value' => '{"sitting_enable":true,"sitting_generate_verbal_trial_digest":true,"sitting_generate_verbal_trial_full":true}',
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'project',
                'value' => '{"project_workflow":true,"project_generate":false,"project_writing":false}',
            ],
        ];
        $this->structureSettings->insert($data);

        // Officials (0)

        // Users
        $domain = env('LIBRICIEL_RECETTE_DEFAULT_EMAIL_DOMAIN', 'mailcatchall.libriciel.net');
        $data = [
            [
                'id' => $this->id('users', 1),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S4 ADMIN',
                'firstname' => 'Raphaël',
                'username' => 'admin@wa-s4',
                'email' => 'wa-s4.admin@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 2),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S4 RÉDACTEUR 1',
                'firstname' => 'Arthur',
                'username' => 'redac-1@wa-s4',
                'email' => 'wa-s4.redac-1@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 3),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme',
                'lastname' => 'S4 VALIDEUR 1',
                'firstname' => 'Sonia',
                'username' => 'valid-1@wa-s4',
                'email' => 'wa-s4.valid-1@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 4),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S4 VALIDEUR 2',
                'firstname' => 'Alphonse',
                'username' => 'valid-2@wa-s4',
                'email' => 'wa-s4.valid-2@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 5),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme',
                'lastname' => 'S4 VALIDEUR 3',
                'firstname' => 'Elodie',
                'username' => 'valid-3@wa-s4',
                'email' => 'wa-s4.valid-3@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 6),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S4 VALIDEUR 4',
                'firstname' => 'Patrick',
                'username' => 'valid-4@wa-s4',
                'email' => 'wa-s4.valid-4@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 7),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S4 ADMINFONC',
                'firstname' => 'Lucas',
                'username' => 'adminfonc@wa-s4',
                'email' => 'wa-s4.adminfonc@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 8),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme',
                'lastname' => 'S4 SECRETAIRE 1',
                'firstname' => 'Lucie',
                'username' => 'secretaire-1@wa-s4',
                'email' => 'wa-s4.secretaire-1@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 9),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'M.',
                'lastname' => 'S4 RÉDACTEUR 4',
                'firstname' => 'Adam',
                'username' => 'redac-4@wa-s4',
                'email' => 'wa-s4.redac-4@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 10),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme',
                'lastname' => 'S4 SECRETAIRE 2',
                'firstname' => 'Jade',
                'username' => 'secretaire-2@wa-s4',
                'email' => 'wa-s4.secretaire-2@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 11),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme',
                'lastname' => 'S4 RÉDACTEUR 2',
                'firstname' => 'Alice',
                'username' => 'redac-2@wa-s4',
                'email' => 'wa-s4.redac-2@' . $domain,
                'data_security_policy' => false,
            ],
            [
                'id' => $this->id('users', 12),
                'organization_id' => $this->id('organizations', 1),
                'civility' => 'Mme',
                'lastname' => 'S4 RÉDACTEUR 3',
                'firstname' => 'Marjorie',
                'username' => 'redac-3@wa-s4',
                'email' => 'wa-s4.redac-3@' . $domain,
                'data_security_policy' => false,
            ],
        ];
        $this->users->insert($data);

        // StructuresUsers
        $data = [
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 1)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 2)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 3)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 4)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 5)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 6)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 7)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 8)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 9)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 10)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 11)],
            ['structure_id' => $this->id('structures', 1), 'user_id' => $this->id('users', 12)],
        ];
        $this->structuresUsers->insert($data);

        // NotificationsUsers
        $data = [
            ['user_id' => $this->id('users', 1), 'notification_id' => 1],
            ['user_id' => $this->id('users', 1), 'notification_id' => 2],
            ['user_id' => $this->id('users', 1), 'notification_id' => 3],
            ['user_id' => $this->id('users', 1), 'notification_id' => 4],
            ['user_id' => $this->id('users', 1), 'notification_id' => 5],
            ['user_id' => $this->id('users', 2), 'notification_id' => 1],
            ['user_id' => $this->id('users', 2), 'notification_id' => 2],
            ['user_id' => $this->id('users', 2), 'notification_id' => 3],
            ['user_id' => $this->id('users', 2), 'notification_id' => 4],
            ['user_id' => $this->id('users', 2), 'notification_id' => 5],
            ['user_id' => $this->id('users', 3), 'notification_id' => 1],
            ['user_id' => $this->id('users', 3), 'notification_id' => 2],
            ['user_id' => $this->id('users', 3), 'notification_id' => 3],
            ['user_id' => $this->id('users', 3), 'notification_id' => 4],
            ['user_id' => $this->id('users', 3), 'notification_id' => 5],
            ['user_id' => $this->id('users', 4), 'notification_id' => 1],
            ['user_id' => $this->id('users', 4), 'notification_id' => 2],
            ['user_id' => $this->id('users', 4), 'notification_id' => 3],
            ['user_id' => $this->id('users', 4), 'notification_id' => 4],
            ['user_id' => $this->id('users', 4), 'notification_id' => 5],
            ['user_id' => $this->id('users', 5), 'notification_id' => 1],
            ['user_id' => $this->id('users', 5), 'notification_id' => 2],
            ['user_id' => $this->id('users', 5), 'notification_id' => 3],
            ['user_id' => $this->id('users', 5), 'notification_id' => 4],
            ['user_id' => $this->id('users', 5), 'notification_id' => 5],
            ['user_id' => $this->id('users', 6), 'notification_id' => 1],
            ['user_id' => $this->id('users', 6), 'notification_id' => 2],
            ['user_id' => $this->id('users', 6), 'notification_id' => 3],
            ['user_id' => $this->id('users', 6), 'notification_id' => 4],
            ['user_id' => $this->id('users', 6), 'notification_id' => 5],
            ['user_id' => $this->id('users', 7), 'notification_id' => 1],
            ['user_id' => $this->id('users', 7), 'notification_id' => 2],
            ['user_id' => $this->id('users', 7), 'notification_id' => 3],
            ['user_id' => $this->id('users', 7), 'notification_id' => 4],
            ['user_id' => $this->id('users', 7), 'notification_id' => 5],
            ['user_id' => $this->id('users', 8), 'notification_id' => 1],
            ['user_id' => $this->id('users', 8), 'notification_id' => 2],
            ['user_id' => $this->id('users', 8), 'notification_id' => 3],
            ['user_id' => $this->id('users', 8), 'notification_id' => 4],
            ['user_id' => $this->id('users', 8), 'notification_id' => 5],
            ['user_id' => $this->id('users', 9), 'notification_id' => 1],
            ['user_id' => $this->id('users', 9), 'notification_id' => 2],
            ['user_id' => $this->id('users', 9), 'notification_id' => 3],
            ['user_id' => $this->id('users', 9), 'notification_id' => 4],
            ['user_id' => $this->id('users', 9), 'notification_id' => 5],
            ['user_id' => $this->id('users', 10), 'notification_id' => 1],
            ['user_id' => $this->id('users', 10), 'notification_id' => 2],
            ['user_id' => $this->id('users', 10), 'notification_id' => 3],
            ['user_id' => $this->id('users', 10), 'notification_id' => 4],
            ['user_id' => $this->id('users', 10), 'notification_id' => 5],
            ['user_id' => $this->id('users', 11), 'notification_id' => 1],
            ['user_id' => $this->id('users', 11), 'notification_id' => 2],
            ['user_id' => $this->id('users', 11), 'notification_id' => 3],
            ['user_id' => $this->id('users', 11), 'notification_id' => 4],
            ['user_id' => $this->id('users', 11), 'notification_id' => 5],
            ['user_id' => $this->id('users', 12), 'notification_id' => 1],
            ['user_id' => $this->id('users', 12), 'notification_id' => 2],
            ['user_id' => $this->id('users', 12), 'notification_id' => 3],
            ['user_id' => $this->id('users', 12), 'notification_id' => 4],
            ['user_id' => $this->id('users', 12), 'notification_id' => 5],
        ];
        $this->notificationsUsers->insert($data);

        // Roles
        $data = [
            ['structure_id' => $this->id('structures', 1), 'name' => 'Administrateur'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Administrateur Fonctionnel'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Rédacteur / Valideur'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Secrétariat général'],
            ['structure_id' => $this->id('structures', 1), 'name' => 'Super Administrateur'],
        ];
        $roles = Hash::combine($this->roles->insert($data, ['id', 'name']), '{n}.name', '{n}.id');

        // RolesUsers
        $data = [
            ['role_id' => $roles['Administrateur'], 'user_id' => $this->id('users', 1)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 2)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 3)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 4)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 5)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 6)],
            ['role_id' => $roles['Administrateur Fonctionnel'], 'user_id' => $this->id('users', 7)],
            ['role_id' => $roles['Secrétariat général'], 'user_id' => $this->id('users', 8)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 9)],
            ['role_id' => $roles['Secrétariat général'], 'user_id' => $this->id('users', 10)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 11)],
            ['role_id' => $roles['Rédacteur / Valideur'], 'user_id' => $this->id('users', 12)],
        ];
        $this->rolesUsers->insert($data);

        // Connecteurs
        $data = [
            [
                'id' => $this->id('connecteurs', 1),
                'name' => 'pastell',
                'url' => "{$env["LIBRICIEL_RECETTE_S4_PASTELL_URL"]}/",
                'tdt_url' => "{$env["LIBRICIEL_RECETTE_S4_PASTELL_TDT_URL"]}/modules/actes/actes_transac_post_confirm_api.php",
                'username' => $env['LIBRICIEL_RECETTE_S4_PASTELL_USERNAME'],
                'password' => $env['LIBRICIEL_RECETTE_S4_PASTELL_PASSWORD'],
                'structure_id' => $this->id('structures', 1),
                'connector_type_id' => 1,
                'connexion_option' => $env['LIBRICIEL_RECETTE_S4_PASTELL_CONNEXION_OPTION'],
            ],
        ];
        $this->connecteurs->insert($data);

        // Pastellfluxtypes
        $data = [
            [
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'actes-generique',
                'description' => 'Actes générique',
            ],
            [
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'mailsec',
                'description' => 'Mail securisé',
            ],
        ];
        $this->pastellfluxtypes->insert($data);

        // GenerateTemplates
        $data = [
            [
                'id' => $this->id('generate_templates', 1),
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => 2,
                'name' => 'Acte délib',
            ],
            [
                'id' => $this->id('generate_templates', 2),
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => 2,
                'name' => 'Acte arrêté',
            ],
            [
                'id' => $this->id('generate_templates', 3),
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => 5,
                'name' => 'Liste des délibérations',
            ],
            [
                'id' => $this->id('generate_templates', 4),
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => 6,
                'name' => 'Procès-verbal',
            ],
            [
                'id' => $this->id('generate_templates', 5),
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => 4,
                'name' => 'Note de synthèse',
            ],
            [
                'id' => $this->id('generate_templates', 6),
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => 3,
                'name' => 'Convocation',
            ],
            [
                'id' => $this->id('generate_templates', 7),
                'structure_id' => $this->id('structures', 1),
                'generate_template_type_id' => 1,
                'name' => 'Projet delib',
            ],
        ];
        $generateTemplates = $this->generateTemplates->insert($data, ['id', 'name']);

        // Files (GenerateTemplates)
        $data = [
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Acte.odt',
                'path' => $files['S4 Acte']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S4 Acte']->getSize(),
                'generate_template_id' => $this->id('generate_templates', 2),
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Deliberation.odt',
                'path' => $files['S4 Délibération']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S4 Délibération']->getSize(),
                'generate_template_id' => $this->id('generate_templates', 1),
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Convocation.odt',
                'path' => $files['S4 Convocation']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S4 Convocation']->getSize(),
                'generate_template_id' => $this->id('generate_templates', 6),
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Note de synthese.odt',
                'path' => $files['S4 Note de synthèse']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S4 Note de synthèse']->getSize(),
                'generate_template_id' => $this->id('generate_templates', 5),
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Projet delibération.odt',
                'path' => $files['S4 Projet délibération']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S4 Projet délibération']->getSize(),
                'generate_template_id' => $this->id('generate_templates', 7),
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Proces-verbal.odt',
                'path' => $files['S4 Procès-verbal']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S4 Procès-verbal']->getSize(),
                'generate_template_id' => $this->id('generate_templates', 4),
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Liste des deliberations.odt',
                'path' => $files['S4 Liste des délibérations']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['S4 Liste des délibérations']->getSize(),
                'generate_template_id' => $this->id('generate_templates', 3),
            ],
        ];
        $this->files->insert($data);

        // Dpos (0)

        // Themes
        $data = [
            [
                'id' => $this->id('themes', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Informatique',
                'position' => '01',
            ],
            [
                'id' => $this->id('themes', 2),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Système',
                'parent_id' => $this->id('themes', 1),
                'position' => '010',
            ],
            [
                'id' => $this->id('themes', 3),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Infrastructure',
                'parent_id' => $this->id('themes', 1),
                'position' => '011',
            ],
            [
                'id' => $this->id('themes', 4),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Ressources',
                'position' => '02',
            ],
            [
                'id' => $this->id('themes', 5),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 RH',
                'parent_id' => $this->id('themes', 4),
                'position' => '020',
            ],
            [
                'id' => $this->id('themes', 6),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Finances',
                'parent_id' => $this->id('themes', 4),
                'position' => '021',
            ],
            [
                'id' => $this->id('themes', 7),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Comptabilité',
                'parent_id' => $this->id('themes', 6),
                'position' => '0210',
            ],
        ];
        $this->themes->insert($data);

        // Natures, Matieres, Classifications, Typespiecesjointes
        $this->setupS2low();

        // Typesittings
        $data = [
            [
                'id' => $this->id('typesittings', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Conseil municipal',
                'isdeliberating' => true,
                'generate_template_convocation_id' => $this->id('generate_templates', 6),
                'generate_template_schedule_id' => $this->id('generate_templates', 5),
                'generate_template_deliberations_list_id' => $this->id('generate_templates', 3),
                'generate_template_verbal_trial_id' => $this->id('generate_templates', 4),
            ],
            [
                'id' => $this->id('typesittings', 2),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Commission finance',
                'isdeliberating' => false,
                'generate_template_convocation_id' => $this->id('generate_templates', 6),
                'generate_template_schedule_id' => $this->id('generate_templates', 5),
                'generate_template_deliberations_list_id' => $this->id('generate_templates', 3),
                'generate_template_verbal_trial_id' => $this->id('generate_templates', 4),
            ],
            [
                'id' => $this->id('typesittings', 3),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Conseil marjo bis',
                'isdeliberating' => true,
                'generate_template_convocation_id' => $this->id('generate_templates', 6),
                'generate_template_schedule_id' => $this->id('generate_templates', 5),
                'generate_template_deliberations_list_id' => $this->id('generate_templates', 3),
                'generate_template_verbal_trial_id' => $this->id('generate_templates', 4),
            ],
        ];
        $this->typesittings->insert($data);

        // Sequences
        $data = [
            [
                'id' => $this->id('sequences', 1),
                'name' => 'S4 Arrêtés non TDT',
                'comment' => 'Pour les arrêtés non télétransmissibles',
                'sequence_num' => 0,
                'structure_id' => $this->id('structures', 1),
            ],
            [
                'id' => $this->id('sequences', 2),
                'name' => 'S4 Arrêtés TDT',
                'comment' => 'Pour les arrêtés télétransmissibles',
                'sequence_num' => 0,
                'structure_id' => $this->id('structures', 1),
            ],
            [
                'id' => $this->id('sequences', 3),
                'name' => 'S4 Délibérations',
                'comment' => 'Pour les délibérations',
                'sequence_num' => 0,
                'structure_id' => $this->id('structures', 1),
            ],
        ];
        $this->sequences->insert($data);

        // Counters
        $data = [
            [
                'id' => $this->id('counters', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Arrêtés non TDT',
                'counter_def' => 'NONTDT#AA##MM##JJ##000#',
                'sequence_id' => $this->id('sequences', 1),
                'reinit_def' => '#MM#',
            ],
            [
                'id' => $this->id('counters', 2),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Arrêtés TDT',
                'counter_def' => 'TDT#AA##MM##JJ##000#',
                'sequence_id' => $this->id('sequences', 2),
                'reinit_def' => '#AAAA#',
            ],
            [
                'id' => $this->id('counters', 3),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Télétravail',
                'counter_def' => '#SSSS#',
                'sequence_id' => $this->id('sequences', 3),
                'reinit_def' => '#JJ#',
            ],
            [
                'id' => $this->id('counters', 4),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Délibération',
                'counter_def' => '#SSSS#',
                'sequence_id' => $this->id('sequences', 3),
                'reinit_def' => '#AAAA#',
            ],
        ];
        $this->counters->insert($data);

        // Typesacts
        $data = [
            [
                'id' => $this->id('typesacts', 1),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 2),
                'name' => 'S4 Arrêté TDT',
                'istdt' => true,
                'isdefault' => false,
                'counter_id' => $this->id('counters', 3),
                'generate_template_act_id' => $this->id('generate_templates', 2),
                'isdeliberating' => false,
            ],
            [
                'id' => $this->id('typesacts', 2),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 5),
                'name' => 'S4 Délibérations',
                'istdt' => true,
                'isdefault' => false,
                'counter_id' => $this->id('counters', 4),
                'generate_template_act_id' => $this->id('generate_templates', 1),
                'isdeliberating' => true,
            ],
            [
                'id' => $this->id('typesacts', 3),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 3),
                'name' => 'S4 Arrêtés non TDT',
                'istdt' => false,
                'isdefault' => false,
                'counter_id' => $this->id('counters', 2),
                'generate_template_act_id' => $this->id('generate_templates', 2),
                'isdeliberating' => false,
            ],
            [
                'id' => $this->id('typesacts', 4),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 1),
                'name' => 'S4 Décision',
                'istdt' => true,
                'isdefault' => false,
                'counter_id' => $this->id('counters', 1),
                'isdeliberating' => false,
            ],
            [
                'id' => $this->id('typesacts', 5),
                'structure_id' => $this->id('structures', 1),
                'nature_id' => $this->id('natures', 4),
                'name' => 'S4 test sans modèle projet',
                'istdt' => true,
                'isdefault' => false,
                'counter_id' => $this->id('counters', 1),
                'generate_template_act_id' => $this->id('generate_templates', 1),
                'isdeliberating' => true,
            ],
        ];
        $this->typesacts->insert($data);

        // TypesactsTypesittings
        $data = [
            ['typesact_id' => $this->id('typesacts', 2), 'typesitting_id' => $this->id('typesittings', 1)],
            ['typesact_id' => $this->id('typesacts', 2), 'typesitting_id' => $this->id('typesittings', 2)],
            ['typesact_id' => $this->id('typesacts', 5), 'typesitting_id' => $this->id('typesittings', 1)],
            ['typesact_id' => $this->id('typesacts', 2), 'typesitting_id' => $this->id('typesittings', 3)],
        ];
        $this->typesactsTypesittings->insert($data);

        // ActorGroups
        $data = [
            [
                'id' => $this->id('actor_groups', 1),
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Invités',
            ],
            [
                'id' => $this->id('actor_groups', 2),
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Majorité',
            ],
            [
                'id' => $this->id('actor_groups', 3),
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Opposition',
            ],
            [
                'id' => $this->id('actor_groups', 4),
                'organization_id' => $this->id('organizations', 1),
                'structure_id' => $this->id('structures', 1),
                'name' => 'S4 Commission finance',
            ],
        ];
        $this->actorGroups->insert($data);

        // Actors
        $domain = env('LIBRICIEL_RECETTE_DEFAULT_EMAIL_DOMAIN', 'mailcatchall.libriciel.net');
        $data = [
            [
                'id' => $this->id('actors', 1),
                'structure_id' => $this->id('structures', 1),
                'active' => false,
                'civility' => 'M',
                'lastname' => 'S4 DÉSACTIVÉ',
                'firstname' => 'Maël',
                'email' => 'wa-s4.acteur.desactive@' . $domain,
                'title' => 'Adjoint maire',
                'cellphone' => '0606060606',
            ],
            [
                'id' => $this->id('actors', 2),
                'structure_id' => $this->id('structures', 1),
                'civility' => 'M',
                'lastname' => 'S4 LEROY',
                'firstname' => 'Noémie',
                'email' => 'wa-s4.acteur.leroy@' . $domain,
                'title' => 'Maire',
                'cellphone' => '0606060606',
                'rank' => 1,
            ],
            [
                'id' => $this->id('actors', 3),
                'structure_id' => $this->id('structures', 1),
                'civility' => 'M',
                'lastname' => 'S4 BARBE',
                'firstname' => 'Robert',
                'email' => 'wa-s4.acteur.barbe@' . $domain,
                'cellphone' => '0708090909',
                'rank' => 3,
            ],
            [
                'id' => $this->id('actors', 4),
                'structure_id' => $this->id('structures', 1),
                'civility' => 'Mme',
                'lastname' => 'S4 FERNANDES',
                'firstname' => 'Valentine',
                'email' => 'wa-s4.acteur.fernandes@' . $domain,
                'cellphone' => '0605040302',
                'rank' => 2,
            ],
            [
                'id' => $this->id('actors', 5),
                'structure_id' => $this->id('structures', 1),
                'civility' => 'M',
                'lastname' => 'S4 OPPOSITION',
                'firstname' => 'Jérémie',
                'email' => 'wa-s4.acteur.opposition@' . $domain,
                'title' => 'représente l\'opposition',
                'cellphone' => '0604040404',
                'rank' => 4,
            ],
            [
                'id' => $this->id('actors', 6),
                'structure_id' => $this->id('structures', 1),
                'civility' => 'Mme',
                'lastname' => 'S4 INVITÉE',
                'firstname' => 'Ophélie',
                'email' => 'wa-s4.acteur.invitee@' . $domain,
                'title' => 'journaliste',
                'cellphone' => '0605050505',
            ],
            [
                'id' => $this->id('actors', 7),
                'structure_id' => $this->id('structures', 1),
                'civility' => 'm',
                'lastname' => 'S4 MASSE',
                'firstname' => 'Henri',
                'email' => 'wa-s4.acteur.masse@' . $domain,
                'title' => 'titre acteur6',
                'cellphone' => '',
                'rank' => 9,
            ],
        ];
        $this->actors->insert($data);

        // ActorsActorGroups
        $data = [
            ['id' => $this->id('actors_actor_groups', 1), 'actor_id' => $this->id('actors', 1), 'actor_group_id' => $this->id('actor_groups', 2), ],
            ['id' => $this->id('actors_actor_groups', 2), 'actor_id' => $this->id('actors', 2), 'actor_group_id' => $this->id('actor_groups', 2), ],
            ['id' => $this->id('actors_actor_groups', 3), 'actor_id' => $this->id('actors', 3), 'actor_group_id' => $this->id('actor_groups', 2), ],
            ['id' => $this->id('actors_actor_groups', 4), 'actor_id' => $this->id('actors', 4), 'actor_group_id' => $this->id('actor_groups', 2), ],
            ['id' => $this->id('actors_actor_groups', 5), 'actor_id' => $this->id('actors', 5), 'actor_group_id' => $this->id('actor_groups', 3), ],
            ['id' => $this->id('actors_actor_groups', 6), 'actor_id' => $this->id('actors', 6), 'actor_group_id' => $this->id('actor_groups', 1), ],
            ['id' => $this->id('actors_actor_groups', 7), 'actor_id' => $this->id('actors', 7), 'actor_group_id' => $this->id('actor_groups', 3), ],
            ['id' => $this->id('actors_actor_groups', 10), 'actor_id' => $this->id('actors', 2), 'actor_group_id' => $this->id('actor_groups', 4), ],
            ['id' => $this->id('actors_actor_groups', 11), 'actor_id' => $this->id('actors', 3), 'actor_group_id' => $this->id('actor_groups', 4), ],
        ];
        $this->actorsActorGroups->insert($data);

        // ActorGroupsTypesittings
        $data = [
            ['actor_group_id' => $this->id('actor_groups', 2), 'typesitting_id' => $this->id('typesittings', 1), ],
            ['actor_group_id' => $this->id('actor_groups', 3), 'typesitting_id' => $this->id('typesittings', 1), ],
            ['actor_group_id' => $this->id('actor_groups', 2), 'typesitting_id' => $this->id('typesittings', 3), ],
            ['actor_group_id' => $this->id('actor_groups', 3), 'typesitting_id' => $this->id('typesittings', 3), ],
            ['actor_group_id' => $this->id('actor_groups', 1), 'typesitting_id' => $this->id('typesittings', 3), ],
            ['actor_group_id' => $this->id('actor_groups', 1), 'typesitting_id' => $this->id('typesittings', 1), ],
            ['actor_group_id' => $this->id('actor_groups', 4), 'typesitting_id' => $this->id('typesittings', 2), ],
            ['actor_group_id' => $this->id('actor_groups', 1), 'typesitting_id' => $this->id('typesittings', 2), ],
        ];
        $this->actorGroupsTypesittings->insert($data);

        // DraftTemplates
        $data = [
            ['id' => $this->id('draft_templates', 1), 'structure_id' => $this->id('structures', 1), 'draft_template_type_id' => 2, 'name' => 'S4 Gabarit acte délib', ],
            ['id' => $this->id('draft_templates', 2), 'structure_id' => $this->id('structures', 1), 'draft_template_type_id' => 2, 'name' => 'S4 Gabarit acte arrêté', ],
        ];
        $this->draftTemplates->insert($data);

        // DraftTemplatesTypesacts (0)

        // Files (DraftTemplates)
        $data = [
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'gabarit_delibere_libriciel.odt',
                'path' => $files['gabarit_delibere_libriciel']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['gabarit_delibere_libriciel']->getSize(),
                'draft_template_id' => $this->id('draft_templates', 1),
            ],
            [
                'structure_id' => $this->id('structures', 1),
                'name' => 'gabarit_decision_libriciel.odt',
                'path' => $files['gabarit_decision_libriciel']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $files['gabarit_decision_libriciel']->getSize(),
                'draft_template_id' => $this->id('draft_templates', 2),
            ],
        ];
        $this->files->insert($data);

        // Sittings
        $data = [
            [
                'id' => $this->id('sittings', 1),
                'structure_id' => $this->id('structures', 1),
                'typesitting_id' => $this->id('typesittings', 1),
                'date' => "{$year}-06-10 11:55:00",
            ],
            [
                'id' => $this->id('sittings', 2),
                'structure_id' => $this->id('structures', 1),
                'typesitting_id' => $this->id('typesittings', 1),
                'date' => "{$year}-06-02 06:30:00",
            ],
        ];
        $this->sittings->insert($data);

        // SittingsStatesittings
        $data = [
            ['sitting_id' => $this->id('sittings', 1), 'statesitting_id' => 1],
            ['sitting_id' => $this->id('sittings', 2), 'statesitting_id' => 1],
        ];
        $this->sittingsStatesittings->insert($data);
    }

    /**
     * Chargement des données de la structure 2 pour la recette
     *
     * @return void
     */
    public function run(): void
    {
        parent::run();

        $this->sequences();

        $this->execute('BEGIN;');
        $this->setup();
        $this->execute('COMMIT;');

        $this->sequences();
        $this->permissions();

        // Création de n (10 par défaut) projets de délibération avec 3 annexes chacun
        $ids = [
            'organization' => $this->id('organizations', 1),
            'structure' => $this->id('structures', 1),
            'user' => $this->id('users', 1),
            'theme' => $this->id('themes', 1),
            'typeact' => $this->id('typesacts', 2),
        ];

        $max = $this->env('LIBRICIEL_RECETTE_S4_NOMBRE_PROJETS', 10);
        for ($idx = 1; $idx <= $max; $idx++) {
            $this->addProjectWithoutGeneration(
                $ids['organization'],
                $ids['structure'],
                $ids['user'],
                $ids['theme'],
                $ids['typeact'],
                sprintf('S4_%d_DE_%04d', date('Y'), $idx),
                dirname(__DIR__, 2) . DS . 'files' . DS . 'Projects' . DS . 'Projet S4.pdf'
            );
        }
    }
}
