<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;

class LibricielRecetteStructure3Seed extends LibricielRecetteAbstractStructureSeed
{
    protected ?int $year;

    protected function setupOrganizations(): void
    {
        $this->save(
            $this->organizations,
            [
                [
                    'name' => 'Recette WA structure 3',
                ],
            ]
        );
    }

    protected function setupStructures(): void
    {
        $this->save(
            $this->structures,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'business_name' => 'Recette WA S3',
                    'id_orchestration' => getenv()['LIBRICIEL_RECETTE_S3_PASTELL_ID_E'],
                    'spinner' => 'ball-clip-rotate',
                ],
            ]
        );
    }

    protected function setupStructureSettings(): void
    {
        $this->save(
            $this->structureSettings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'project',
                    'value' => '{"project_workflow":false,"project_writing":false,"project_generate":false}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'act',
                    'value' => '{"generate_number":false,"act_generate":false}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'sitting',
                    'value' => '{"sitting_enable":false,"sitting_generate_verbal_trial_digest":false}',
                ],
            ]
        );
    }

    protected function setupOfficials(): void
    {
        $this->save(
            $this->officials,
            [
            ]
        );
    }

    protected function setupUsers(): void
    {
        $this->save(
            $this->users,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme',
                    'lastname' => 'S3 ADMIN',
                    'firstname' => 'Camille',
                    'username' => 'admin@wa-s3',
                    'email' => 'wa-s3.admin@' . $this->domain(),
                    'data_security_policy' => false,
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S3 RÉDACTEUR',
                    'firstname' => 'Kévin',
                    'username' => 'redac@wa-s3',
                    'email' => 'wa-s3.redac@' . $this->domain(),
                    'data_security_policy' => false,
                ],
            ]
        );
    }

    protected function setupStructuresUsers(): void
    {
        $this->save(
            $this->structuresUsers,
            [
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'admin@wa-s3')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'redac@wa-s3')],
            ]
        );
    }

    protected function setupNotificationsUsers(): void
    {
        $this->save(
            $this->notificationsUsers,
            [
                ['user_id' => $this->record($this->users, 'admin@wa-s3'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'admin@wa-s3'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'admin@wa-s3'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'admin@wa-s3'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'admin@wa-s3'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'redac@wa-s3'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'redac@wa-s3'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'redac@wa-s3'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'redac@wa-s3'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'redac@wa-s3'), 'notification_id' => 5],
            ]
        );
    }

    protected function setupRoles(): void
    {
        $this->save(
            $this->roles,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur Fonctionnel',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Rédacteur / Valideur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Secrétariat général',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Super Administrateur',
                ],
            ]
        );
    }

    protected function setupRolesUsers(): void
    {
        $this->save(
            $this->rolesUsers,
            [
                ['role_id' => $this->record($this->roles, 'Administrateur'), 'user_id' => $this->record($this->users, 'admin@wa-s3')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'redac@wa-s3')],
            ]
        );
    }

    protected function setupConnecteurs(): void
    {
        $env = getenv();
        $this->save(
            $this->connecteurs,
            [
                [
                    'name' => 'pastell',
                    'url' => "{$env["LIBRICIEL_RECETTE_S3_PASTELL_URL"]}/",
                    'tdt_url' => "{$env["LIBRICIEL_RECETTE_S3_PASTELL_TDT_URL"]}/modules/actes/actes_transac_post_confirm_api.php",
                    'username' => $env['LIBRICIEL_RECETTE_S3_PASTELL_USERNAME'],
                    'password' => $env['LIBRICIEL_RECETTE_S3_PASTELL_PASSWORD'],
                    'structure_id' => $this->current($this->structures),
                    'connector_type_id' => 1,
                    'connexion_option' => $env['LIBRICIEL_RECETTE_S3_PASTELL_CONNEXION_OPTION'],
                ],
            ]
        );
    }

    protected function setupPastellfluxtypes(): void
    {
        $this->save(
            $this->pastellfluxtypes,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'actes-generique',
                    'description' => 'Actes générique',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'mailsec',
                    'description' => 'Mail securisé',
                ],
            ]
        );
    }

    protected function setupThemes(): void
    {
        $this->save(
            $this->themes,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S3 Thème 1',
                    'position' => '1',
                ],
            ]
        );
    }

    protected function setupTypesacts(): void
    {
        $this->save(
            $this->typesacts,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AR'),
                    'name' => 'Arrêtés TDT',
                    'istdt' => true,
                    'isdefault' => false,
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AI'),
                    'name' => 'Arrêtés non TDT',
                    'istdt' => false,
                    'isdefault' => false,
                    'isdeliberating' => false,
                ],
            ]
        );
    }

    protected function setup(): void
    {
        $this->setupOrganizations();
        $this->setupStructures();
        $this->setupStructureSettings();
        $this->setupOfficials();
        $this->setupUsers();
        $this->setupStructuresUsers();
        $this->setupNotificationsUsers();
        $this->setupRoles();
        $this->setupRolesUsers();
        $this->setupConnecteurs();
        $this->setupPastellfluxtypes();
        $this->setupThemes();
        $this->setupNatures();
        $this->setupMatieres();
        $this->setupClassifications();
        $this->setupTypespiecesjointes();
        $this->setupTypesacts();
    }

    protected function createProjects(): void
    {
        $max = $this->env('LIBRICIEL_RECETTE_S3_NOMBRE_PROJETS', 10);
        for ($idx = 1; $idx <= $max; $idx++) {
            $this->addProjectWithoutGeneration(
                $this->current($this->organizations),
                $this->current($this->structures),
                $this->record($this->users, 'admin@wa-s3'),
                $this->record($this->themes, 'S3 Thème 1'),
                $this->record($this->typesacts, 'Arrêtés TDT'),
                sprintf('S3_%d_AT_%04d', $this->year, $idx),
                dirname(__DIR__, 2) . DS . 'files' . DS . 'Projects' . DS . 'Projet S3.pdf'
            );
        }
    }

    public function run(): void
    {
        parent::run();

        $this->sequences();

        $this->year = (int)date('Y');

        $this->execute('BEGIN;');
        $this->setup();
        $this->execute('COMMIT;');

        $this->sequences();
        $this->createProjects();
        $this->sequences();

        $this->permissions();
    }
}
