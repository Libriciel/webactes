<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

class LibricielRecetteSeed extends AbstractSeed
{
    /**
     * Chargement des données pour la recette
     *
     * @return void
     */
    public function run(): void
    {
        parent::run();

        $this->call('LibricielRecette.LibricielRecetteStructure1Seed');
        $this->call('LibricielRecette.LibricielRecetteStructure2Seed');
        $this->call('LibricielRecette.LibricielRecetteStructure3Seed');
        $this->call('LibricielRecette.LibricielRecetteStructure4Seed');
    }
}
