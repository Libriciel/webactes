<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use Cake\Core\Plugin;
use Cake\Utility\Hash;
use LibricielRecette\Model\Enum\GenerateTemplateType;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;
use LibricielRecette\Seeds\LibricielRecetteMassiveDataSeedCommonTrait;
use LibricielRecette\Seeds\LibricielRecetteSeedGenerateTemplatesTrait;

/**
 * @deprecated
 */
class LibricielRecetteOldMassiveDataSeed extends LibricielRecetteAbstractStructureSeed
{
    use LibricielRecetteMassiveDataSeedCommonTrait;
    use LibricielRecetteSeedGenerateTemplatesTrait;

    /**
     * Création du super-administrateur par défaut de la structure
     */
    protected function setupStructureSuperadmin(int $organizationIdx, int $structureIdx, string $organizationName, string $structureName, array $records): array
    {
        $username = $this->getSuperadminUsername($organizationIdx, $structureIdx);

        $data = [
            [
                'organization_id' => $records['organizations'][$organizationName],
                'civility' => 'Mme.',
                'lastname' => sprintf('O-%05d S-%05d', $organizationIdx, $structureIdx),
                'firstname' => 'Super-administratrice',
                'username' => $username,
                'email' => $this->getSuperadminEmail($organizationIdx, $structureIdx),
            ],
        ];

        $records['users'] = array_merge(
            $records['users'],
            Hash::combine($this->users->insert($data, ['id', 'username']), '{n}.username', '{n}.id')
        );

        // StructuresUsers
        $data = [
            ['structure_id' => $records['structures'][$structureName], 'user_id' => $records['users'][$username]],
        ];
        $this->structuresUsers->insert($data);

        // NotificationsUsers (toutes désactivées)
        $data = [];
        for ($notificationId = 1; $notificationId <= 5; $notificationId++) {
            $data[] = [
                'user_id' => $records['users'][$username],
                'notification_id' => $notificationId,
                'active' => false,
            ];
        }
        $this->notificationsUsers->insert($data);

        // Roles (en dur, mais à ajouter par structure)
        $data = [
            ['structure_id' => $records['structures'][$structureName], 'name' => 'Administrateur'],
            ['structure_id' => $records['structures'][$structureName], 'name' => 'Administrateur Fonctionnel'],
            ['structure_id' => $records['structures'][$structureName], 'name' => 'Rédacteur / Valideur'],
            ['structure_id' => $records['structures'][$structureName], 'name' => 'Secrétariat général'],
            ['structure_id' => $records['structures'][$structureName], 'name' => 'Super Administrateur'],
        ];
        $records['roles'] = Hash::combine($this->roles->insert($data, ['id', 'name']), '{n}.name', '{n}.id');

        // RolesUsers
        $data = [
            [
                'role_id' => $records['roles']['Super Administrateur'],
                'user_id' => $records['users'][$username],
            ],
        ];
        $this->rolesUsers->insert($data);

        return $records;
    }

    /**
     * Crée le paramétrage
     *
     * @return void
     */
    protected function setup(): void
    {
        $env = getenv();
        $max = $this->getConfiguration();
        $records = [
            'generate_templates' => [],
            'organizations' => [],
            'roles' => [],
            'structures' => [],
            'users' => [],
        ];
        $dirs = [
            'GenerateTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesGenerateTemplates' . DS,
            'DraftTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesDraftTemplates' . DS,
        ];

        for ($organizationIdx = 1; $organizationIdx <= $max['organizations']; $organizationIdx++) {
            $organizationName = sprintf('Organisation %05d', $organizationIdx);

            $data = [
                [
                    'active' => true,
                    'name' => $organizationName,
                ],
            ];

            $records['organizations'] = array_merge(
                $records['organizations'],
                Hash::combine($this->organizations->insert($data, ['id', 'name']), '{n}.name', '{n}.id')
            );

            for ($structureIdx = 1; $structureIdx <= $max['structures']; $structureIdx++) {
                $structureName = sprintf('Organisation %05d, structure %05d', $organizationIdx, $structureIdx);
                $this->output->write(sprintf('Création de la structure %s', $structureName), true);

                $this->loadIds();

                $pastellEnvPrefix = sprintf('LIBRICIEL_RECETTE_MASSIVE_O_%05d_S_%05d_PASTELL', $organizationIdx, $structureIdx);

                // Structures
                $data = [
                    [
                        'organization_id' => $records['organizations'][$organizationName],
                        'business_name' => $structureName,
                        'id_orchestration' => $env[$pastellEnvPrefix . '_ID_E'] ?? $structureIdx,
                    ],
                ];
                $records['structures'] = array_merge(
                    $records['structures'],
                    Hash::combine($this->structures->insert($data, ['id', 'business_name']), '{n}.business_name', '{n}.id')
                );

                // StructureSettings
                $data = [
                    [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => 'project',
                        'value' => '{"project_generate":true,"project_workflow":true,"project_writing":true}',
                    ],
                    [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => 'act',
                        'value' => '{"act_generate":true,"generate_number":true}',
                    ],
                    [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => 'convocation',
                        'value' => '{"convocation_idelibre":true,"convocation_mailsec":false}',
                    ],
                    [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => 'sitting',
                        'value' => '{"sitting_enable":true,"sitting_generate_deliberations_list":true,"sitting_generate_verbal_trial":true}',
                    ],
                ];
                $this->structureSettings->insert($data);

                // Officials
                $data = [
                    [
                        'structure_id' => $records['structures'][$structureName],
                        'civility' => 'M.',
                        'lastname' => $structureName,
                        'firstname' => 'Officiel',
                        'email' => 'contact@libriciel.coop',
                        'address' => '140 Rue Aglaonice de Thessalie',
                        'address_supplement' => '',
                        'post_code' => '34170',
                        'city' => 'Castelnau-le-Lez',
                        'phone' => '04 67 65 96 44',
                    ],
                ];
                $this->officials->insert($data);

                // Users

                // 1. Création du superadministrateur par défaut de la structure
                $records = $this->setupStructureSuperadmin($organizationIdx, $structureIdx, $organizationName, $structureName, $records);

                // 2. Création des utilisateurs de la structure
                if ($max['users'] > 0) {
                    $data = [];
                    foreach ($records['roles'] as $roleName => $roleId) {
                        for ($user = 1; $user <= $max['users']; $user++) {
                            $data[] = [
                                'organization_id' => $records['organizations'][$organizationName],
                                'civility' => 'Mme.',
                                'lastname' => sprintf('O-%05d S-%05d', $organizationIdx, $structureIdx),
                                'firstname' => sprintf('%s %05d', $roleName, $user),
                                'username' => $this->getUsername($organizationIdx, $structureIdx, $roleName, $user),
                                'email' => $this->getUserEmail($organizationIdx, $structureIdx, $roleName, $user),
                            ];
                        }
                    }
                    $records['users'] = array_merge(
                        $records['users'],
                        Hash::combine($this->users->insert($data, ['id', 'username']), '{n}.username', '{n}.id')
                    );

                    // StructuresUsers
                    $data = [];
                    foreach ($records['roles'] as $roleName => $roleId) {
                        for ($user = 1; $user <= $max['users']; $user++) {
                            $username = $this->getUsername($organizationIdx, $structureIdx, $roleName, $user);
                            $data[] = [
                                'structure_id' => $records['structures'][$structureName],
                                'user_id' => $records['users'][$username],
                            ];
                        }
                    }
                    $this->structuresUsers->insert($data);

                    // NotificationsUsers (toutes désactivées)
                    $data = [];
                    foreach ($records['roles'] as $roleName => $roleId) {
                        for ($user = 1; $user <= $max['users']; $user++) {
                            $username = $this->getUsername($organizationIdx, $structureIdx, $roleName, $user);
                            for ($notificationId = 1; $notificationId <= 5; $notificationId++) {
                                $data[] = [
                                    'user_id' => $records['users'][$username],
                                    'notification_id' => $notificationId,
                                    'active' => false,
                                ];
                            }
                        }
                    }
                    $this->notificationsUsers->insert($data);

                    // RolesUsers
                    $data = [];
                    foreach ($records['roles'] as $roleName => $roleId) {
                        for ($user = 1; $user <= $max['users']; $user++) {
                            $username = $this->getUsername($organizationIdx, $structureIdx, $roleName, $user);
                            $data[] = [
                                'role_id' => $records['roles'][$roleName],
                                'user_id' => $records['users'][$username],
                            ];
                        }
                    }
                    $this->rolesUsers->insert($data);
                }

                // -----------------------------------------------------------------------------------------------------

                // Connecteurs
                $vars = [
                    'URL' => $env[$pastellEnvPrefix . '_URL'] ?? 'https://pastell.partenaire.libriciel.fr',
                    'TDT_URL' => $env[$pastellEnvPrefix . '_TDT_URL'] ?? 'https://s2low.partenaires.libriciel.fr',
                    'USERNAME' => $env[$pastellEnvPrefix . '_USERNAME'] ?? 'username',
                    'PASSWORD' => $env[$pastellEnvPrefix . '_PASSWORD'] ?? 'password',
                    'CONNEXION_OPTION' => $env[$pastellEnvPrefix . '_CONNEXION_OPTION'] ?? 1,
                ];

                $data = [
                    [
                        'name' => 'pastell',
                        'url' => "{$vars['URL']}/",
                        'tdt_url' => "{$vars['TDT_URL']}/modules/actes/actes_transac_post_confirm_api.php",
                        'username' => $vars['USERNAME'],
                        'password' => $vars['PASSWORD'],
                        'structure_id' => $records['structures'][$structureName],
                        'connector_type_id' => 1,
                        'connexion_option' => $vars['CONNEXION_OPTION'],
                    ],
                ];
                $this->connecteurs->insert($data);

                // Pastellfluxtypes
                $data = [
                    [
                        'organization_id' => $records['organizations'][$organizationName],
                        'structure_id' => $records['structures'][$structureName],
                        'name' => 'actes-generique',
                        'description' => 'Actes générique',
                    ],
                    [
                        'organization_id' => $records['organizations'][$organizationName],
                        'structure_id' => $records['structures'][$structureName],
                        'name' => 'mailsec',
                        'description' => 'Mail securisé',
                    ],
                ];
                $this->pastellfluxtypes->insert($data);

                // GenerateTemplates (par défaut)
                $generateTemplates = [
                    'Modèle de projet d\'arrêté' => GenerateTemplateType::PROJECT,
                    'Modèle de projet de délibération' => GenerateTemplateType::PROJECT,
                    'Modèle de convocation' => GenerateTemplateType::CONVOCATION,
                    'Modèle de note de synthèse' => GenerateTemplateType::EXECUTIVE_SUMMARY,
                    'Modèle de liste des délibérations' => GenerateTemplateType::DELIBERATIONS_LIST,
                    'Modèle de procès-verbal' => GenerateTemplateType::VERBAL_TRIAL,
                    'Modèle d\'arrêté' => GenerateTemplateType::ACT,
                    'Modèle de délibération' => GenerateTemplateType::ACT,
                ];
                foreach ($generateTemplates as $name => $generateTemplateType) {
                    $records['generate_templates'][$name] = $this->setupGenerateTemplate(
                        $records['structures'][$structureName],
                        $generateTemplateType,
                        $name
                    );
                }

                // GenerateTemplatesFiles (par défaut)
                $dir = Plugin::path('LibricielRecette') . DS . 'files' . DS . 'FilesGenerateTemplates' . DS;
                $generateTemplateFiles = [
                    'Modèle de projet d\'arrêté' => 'Projet arrete',
                    'Modèle de projet de délibération' => 'Projet deliberation',
                    'Modèle de convocation' => 'Convocation',
                    'Modèle de note de synthèse' => 'Note de synthese',
                    'Modèle de liste des délibérations' => 'Liste des deliberations',
                    'Modèle de procès-verbal' => 'Proces-verbal',
                    'Modèle d\'arrêté' => 'Acte',
                    'Modèle de délibération' => 'Deliberation',
                ];
                foreach ($generateTemplateFiles as $name => $filename) {
                    $this->setupGenerateTemplateFile(
                        $records['structures'][$structureName],
                        $records['generate_templates'][$name],
                        "{$dir}{$filename}.odt",
                        "{$filename}.odt"
                    );
                }

                // GenerateTemplates et GenerateTemplatesFiles ($max['generate_templates'])
                for ($generateTemplate = 1; $generateTemplate <= $max['generate_templates']; $generateTemplate++) {
                    $name = sprintf('Modèle de projet de délibération %05d', $generateTemplate);
                    $records['generate_templates'][$name] = $this->setupGenerateTemplate(
                        $records['structures'][$structureName],
                        GenerateTemplateType::PROJECT,
                        $name
                    );
                    $this->setupGenerateTemplateFile(
                        $records['structures'][$structureName],
                        $records['generate_templates'][$name],
                        "{$dir}{$filename}.odt",
                        sprintf('Projet de délibération %05d.odt', $generateTemplate)
                    );
                }

                // Dpos
                $data = [
                    [
                        'structure_id' => $records['structures'][$structureName],
                        'civility' => 'M.',
                        'lastname' => sprintf('O-%05d S-%05d DPO', $organizationIdx, $structureIdx),
                        'firstname' => 'Théo',
                        'email' => sprintf('wa-o-%05d-s-%05d.dpo@%s', $organizationIdx, $structureIdx, $this->getDomain()),
                    ],
                ];
                $this->dpos->insert($data);

                // Themes ($max['themes'])
                $data = [];
                for ($theme = 1; $theme <= $max['themes']; $theme++) {
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => sprintf('Thème %d', $theme),
                        'parent_id' => null,
                        'position' => sprintf('%05d', $theme),
                    ];
                }
                $records['themes'] = Hash::combine($this->themes->insert($data, ['id', 'name']), '{n}.name', '{n}.id');

                // Sous-themes ($max['themes'], limité à une longueur de 60) pour le premier thème inséré
                if (isset($records['themes']['Thème 1'])) {
                    $parentId = $records['themes']['Thème 1'];
                    for ($theme = 1; $theme <= $max['themes']; $theme++) {
                        $themeName = sprintf('Thème 1.%s', implode('.', array_fill(0, $theme, '1')));
                        if (mb_strlen($themeName) <= 60) { // La longueur maximale du champ en base de données
                            $data = [
                                [
                                    'structure_id' => $records['structures'][$structureName],
                                    'name' => $themeName,
                                    'parent_id' => $parentId,
                                    'position' => sprintf('00001-%s', implode('-', array_fill(0, $theme, '1'))),
                                ],
                            ];
                            $insertedThemes = Hash::combine($this->themes->insert($data, ['id', 'name']), '{n}.name', '{n}.id');
                            $parentId = $insertedThemes[$themeName];
                            $records['themes'] = array_merge($records['themes'], $insertedThemes);
                        }
                    }
                }

                $this->setupS2low($records['structures'][$structureName]);

                // Typesittings
                $data = [];
                for ($typesitting = 1; $typesitting <= $max['typesittings']; $typesitting++) {
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => sprintf('Séance délibérante activée %05d', $typesitting),
                        'isdeliberating' => true,
                        'generate_template_convocation_id' => $records['generate_templates']['Modèle de convocation'],
                        'generate_template_executive_summary_id' => $records['generate_templates']['Modèle de note de synthèse'],
                        'generate_template_deliberations_list_id' => $records['generate_templates']['Modèle de liste des délibérations'],
                        'generate_template_verbal_trial_id' => $records['generate_templates']['Modèle de note de synthèse'],
                    ];
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => sprintf('Séance non délibérante activée %05d', $typesitting),
                        'isdeliberating' => false,
                        'generate_template_convocation_id' => $records['generate_templates']['Modèle de convocation'],
                        'generate_template_executive_summary_id' => $records['generate_templates']['Modèle de note de synthèse'],
                        'generate_template_deliberations_list_id' => $records['generate_templates']['Modèle de liste des délibérations'],
                        'generate_template_verbal_trial_id' => $records['generate_templates']['Modèle de note de synthèse'],
                    ];
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => sprintf('Séance délibérante désactivée %05d', $typesitting),
                        'active' => false,
                        'isdeliberating' => true,
                        'generate_template_convocation_id' => $records['generate_templates']['Modèle de convocation'],
                        'generate_template_executive_summary_id' => $records['generate_templates']['Modèle de note de synthèse'],
                        'generate_template_deliberations_list_id' => $records['generate_templates']['Modèle de liste des délibérations'],
                        'generate_template_verbal_trial_id' => $records['generate_templates']['Modèle de note de synthèse'],
                    ];
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => sprintf('Séance non délibérante désactivée %05d', $typesitting),
                        'active' => false,
                        'isdeliberating' => false,
                        'generate_template_convocation_id' => $records['generate_templates']['Modèle de convocation'],
                        'generate_template_executive_summary_id' => $records['generate_templates']['Modèle de note de synthèse'],
                        'generate_template_deliberations_list_id' => $records['generate_templates']['Modèle de liste des délibérations'],
                        'generate_template_verbal_trial_id' => $records['generate_templates']['Modèle de note de synthèse'],
                    ];
                }
                $typesittings = $this->typesittings->insert($data, ['id', 'name']);

                // Sequences
                $data = [];
                for ($sequence = 1; $sequence <= $max['sequences']; $sequence++) {
                    $data[] = [
                        'name' => sprintf('Séquence %05d', $sequence),
                        'comment' => '',
                        'sequence_num' => 0,
                        'structure_id' => $records['structures'][$structureName],
                    ];
                }
                $sequences = $this->sequences->insert($data, ['id', 'name']);

                // Counters
                $data = [];
                for ($counter = 1; $counter <= $max['counters']; $counter++) {
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => sprintf('Compteur %05d', $counter),
                        'counter_def' => 'CNT_#SSSS#',
                        'sequence_id' => $sequences[0]['id'],
                        'reinit_def' => '#AAAA#',
                    ];
                }
                $counters = $this->counters->insert($data, ['id', 'name']);

                // Typesacts
                $data = [];
                for ($typeact = 1; $typeact <= $max['typesacts']; $typeact++) {
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'nature_id' => $this->id('natures', 5),
                        'name' => sprintf('Délibération %05d', $typeact),
                        'istdt' => true,
                        'active' => true,
                        'isdefault' => true,
                        'counter_id' => $counters[0]['id'],
                        'generate_template_project_id' => $records['generate_templates']['Modèle de projet de délibération'],
                        'generate_template_act_id' => $records['generate_templates']['Modèle de délibération'],
                        'isdeliberating' => true,
                    ];
                }
                $typesacts = $this->typesacts->insert($data, ['id', 'name']);

                // TypesactsTypesittings
                $data = [];
                for ($typeact = 1; $typeact <= $max['typesacts']; $typeact++) {
                    $data[] = [
                        'typesact_id' => $typesacts[$typeact - 1]['id'],
                        'typesitting_id' => $typesittings[0]['id'],
                    ];
                }
                $this->typesactsTypesittings->insert($data);

                // ActorGroups
                $data = [];
                for ($actorGroup = 1; $actorGroup <= $max['actor_groups']; $actorGroup++) {
                    $data[] = [
                        'organization_id' => $records['organizations'][$organizationName],
                        'structure_id' => $records['structures'][$structureName],
                        'name' => sprintf('Groupe d\'acteur %05d', $actorGroup),
                        'active' => true,
                    ];
                }
                $actorGroups = $this->actorGroups->insert($data, ['id', 'name']);

                // Actors
                $data = [];
                for ($actor = 1; $actor <= $max['actors']; $actor++) {
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'active' => true,
                        'civility' => 'M.',
                        'lastname' => sprintf('O-%05d S-    %05d', $organizationIdx, $structureIdx),
                        'firstname' => sprintf('Acteur %05d', $actor),
                        'email' => $this->getActorEmail($organizationIdx, $structureIdx, $actor),
                        'rank' => $actor,
                    ];
                }
                $actors = $this->actors->insert($data, ['id', 'email']);

                // ActorsActorGroups
                $data = [];
                for ($actor = 1; $actor <= $max['actors']; $actor++) {
                    $data[] = ['actor_id' => $actors[$actor - 1]['id'], 'actor_group_id' => $actorGroups[0]['id']];
                }
                $this->actorsActorGroups->insert($data);

                // ActorGroupsTypesittings
                $data = [];
                for ($actorGroup = 1; $actorGroup <= $max['actor_groups']; $actorGroup++) {
                    $data[] = [
                        'actor_group_id' => $actorGroups[$actorGroup - 1]['id'],
                        'typesitting_id' => $typesittings[0]['id'],
                    ];
                }
                $this->actorGroupsTypesittings->insert($data);

                // DraftTemplates
                $data = [];
                for ($draftTemplate = 1; $draftTemplate <= $max['draft_templates']; $draftTemplate++) {
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'draft_template_type_id' => 1,
                        'name' => sprintf('O%05d S%05d Projet %05d', $organizationIdx, $structureIdx, $draftTemplate),
                    ];
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'draft_template_type_id' => 2,
                        'name' => sprintf('O%05d S%05d Acte %05d', $organizationIdx, $structureIdx, $draftTemplate),
                    ];
                }
                $draftTemplates = $this->draftTemplates->insert($data, ['id', 'draft_template_type_id']);

                // DraftTemplatesTypesacts
                $data = [];
                for ($draftTemplate = 1; $draftTemplate <= $max['draft_templates']; $draftTemplate++) {
                    $data[] = [
                        'draft_template_id' => $draftTemplates[($draftTemplate - 1) * 2]['id'],
                        'typesact_id' => $typesacts[0]['id'],
                    ];
                    $data[] = [
                        'draft_template_id' => $draftTemplates[($draftTemplate - 1) * 2 + 1]['id'],
                        'typesact_id' => $typesacts[0]['id'],
                    ];
                }
                $this->draftTemplatesTypesacts->insert($data);

                // Files (FilesDraftTemplates)
                $data = [];
                for ($draftTemplate = 1; $draftTemplate <= $max['draft_templates']; $draftTemplate++) {
                    $files = [
                        'texte_delib2' => $this->createFile(
                            $dirs['DraftTemplates'] . 'texte_delib2.odt',
                            'draft_template_id',
                            $records['organizations'][$organizationName],
                            $records['structures'][$structureName],
                            $draftTemplates[($draftTemplate - 1) * 2]['id']
                        ),
                        'texte_projet' => $this->createFile(
                            $dirs['DraftTemplates'] . 'texte_projet.odt',
                            'draft_template_id',
                            $records['organizations'][$organizationName],
                            $records['structures'][$structureName],
                            $draftTemplates[($draftTemplate - 1) * 2 + 1]['id']
                        ),
                    ];
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => 'texte_delib2.odt',
                        'path' => $files['texte_delib2']->getRealPath(),
                        'mimetype' => 'application/vnd.oasis.opendocument.text',
                        'size' => $files['texte_delib2']->getSize(),
                        'draft_template_id' => $draftTemplates[($draftTemplate - 1) * 2]['id'],
                    ];
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'name' => 'texte_projet.odt',
                        'path' => $files['texte_projet']->getRealPath(),
                        'mimetype' => 'application/vnd.oasis.opendocument.text',
                        'size' => $files['texte_projet']->getSize(),
                        'draft_template_id' => $draftTemplates[($draftTemplate - 1) * 2 + 1]['id'],
                    ];
                }
                $this->files->insert($data, ['id']);

                // Sittings
                $data = [];
                $start = mktime(0, 0, 0, 1, 1, (int)date('Y'));
                for ($sitting = 1; $sitting <= $max['sittings']; $sitting++) {
                    $data[] = [
                        'structure_id' => $records['structures'][$structureName],
                        'typesitting_id' => $typesittings[0]['id'],
                        'date' => date('Y-m-d H:i:s', $start + 60 * ($sitting - 1)),
                    ];
                }
                $sittings = $this->sittings->insert($data, ['id']);

                // SittingsStatesittings
                $data = [];
                foreach ($sittings as $sitting) {
                    $data[] = ['sitting_id' => $sitting['id'], 'statesitting_id' => 1];
                }
                $this->sittingsStatesittings->insert($data);

                // Création de n (10 par défaut) projets de délibération avec 3 annexes chacun
                for ($idx = 1; $idx <= $max['projects']; $idx++) {
                    $this->addProjectWithGeneration(
                        $records['organizations'][$organizationName],
                        $records['structures'][$structureName],
                        $records['users'][$this->getSuperadminUsername($organizationIdx, $structureIdx)],
                        $records['themes']['Thème 1'],
                        $typesacts[0]['id'],
                        $draftTemplates[0]['id'],
                        $draftTemplates[1]['id']
                    );
                }
            }
        }
    }

    /**
     * Chargement des données pour la recette
     *
     * @return void
     * @throws \Throwable
     */
    public function run(): void
    {
        parent::run();
        $this->sequences();

        $this->execute('BEGIN;');
        $this->setup();
        $this->execute('COMMIT;');

        $this->sequences();
        $this->permissions();
    }
}
