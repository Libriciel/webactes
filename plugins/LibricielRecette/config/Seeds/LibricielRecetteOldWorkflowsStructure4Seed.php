<?php
declare(strict_types=1);

use LibricielRecette\Seeds\LibricielRecetteAbstractWorkflowSeed;

/**
 * @deprecated
 */
class LibricielRecetteOldWorkflowsStructure4Seed extends LibricielRecetteAbstractWorkflowSeed
{
    /**
     * @inheritDoc
     */
    public function run(): void
    {
        parent::run();

        $this->apiConnect('admin@wa-s4');

        $this->apiAddWorkflow(
            [
                'name' => 'S4 Circuit avec secrétaire',
                'description' => '',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Valideur 1',
                        'users' => [$this->getUserId('valid-1@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Secrétariat 1',
                        'users' => [$this->getUserId('secretaire-1@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Valideur 2',
                        'users' => [$this->getUserId('valid-2@wa-s4')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S4 Étapes simples',
                'description' => '4 étapes simples successives',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => '1',
                        'users' => [$this->getUserId('valid-1@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => '2',
                        'users' => [$this->getUserId('valid-2@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => '3',
                        'users' => [$this->getUserId('valid-3@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => '4',
                        'users' => [$this->getUserId('valid-4@wa-s4')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S4 Simple concurrent simple',
                'description' => '3 étapes dont une concurrente',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Étape 1 simple',
                        'users' => [$this->getUserId('valid-1@wa-s4')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Étape 2 concurrent',
                        'users' => [$this->getUserId('valid-2@wa-s4'), $this->getUserId('valid-3@wa-s4')],
                    ],
                    [
                        'groups' => 'Étape 3 simple',
                        'name' => '3',
                        'users' => [$this->getUserId('valid-4@wa-s4')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S4 Une étape concurrente 1-2',
                'description' => '',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Étape concurrente',
                        'users' => [$this->getUserId('valid-1@wa-s4'), $this->getUserId('valid-2@wa-s4')],
                    ],
                ],
            ],
            true
        );
    }
}
