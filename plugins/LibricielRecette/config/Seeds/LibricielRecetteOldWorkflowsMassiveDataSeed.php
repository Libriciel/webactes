<?php
declare(strict_types=1);

use LibricielRecette\Seeds\LibricielRecetteAbstractWorkflowSeed;
use LibricielRecette\Seeds\LibricielRecetteMassiveDataSeedCommonTrait;

/**
 * @deprecated
 */
class LibricielRecetteOldWorkflowsMassiveDataSeed extends LibricielRecetteAbstractWorkflowSeed
{
    use LibricielRecetteMassiveDataSeedCommonTrait;

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        parent::run();

        $max = $this->getConfiguration();

        for ($organizationIdx = 1; $organizationIdx <= $max['organizations']; $organizationIdx++) {
            for ($structureIdx = 1; $structureIdx <= $max['structures']; $structureIdx++) {
                $structureName = sprintf('Organisation %05d, structure %05d', $organizationIdx, $structureIdx);
                $this->output->write(sprintf('Création des circuits pour la structure %s', $structureName), true);

                $username = $this->getSuperadminUsername($organizationIdx, $structureIdx);

                $this->apiConnect($username);
                for ($circuit = 1; $circuit <= $max['workflows']; $circuit++) {
                    $name = sprintf('O-%05d S-%05d Circuit %05d', $organizationIdx, $structureIdx, $circuit);

                    $this->apiAddWorkflow(
                        [
                            'name' => $name,
                            'description' => '',
                            'steps' => [
                                [
                                    'groups' => '',
                                    'name' => sprintf('Simple: %s', $username),
                                    'users' => [$this->getUserId($username)],
                                ],
                            ],
                        ],
                        true
                    );
                }
            }
        }
    }
}
