<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use LibricielRecette\Model\Enum\GenerateTemplateType;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;

class LibricielRecetteStructure1Seed extends LibricielRecetteAbstractStructureSeed
{
    protected ?int $year;

    /**
     * @var \SplFileInfo[]
     */
    protected array $paths = [];

    protected function setupOrganizations(): void
    {
        $this->save(
            $this->organizations,
            [
                [
                    'name' => 'Recette WA structure 1',
                ],
            ]
        );
    }

    protected function setupStructures(): void
    {
        $this->save(
            $this->structures,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'business_name' => 'Recette WA S1',
                    'id_orchestration' => $this->env('LIBRICIEL_RECETTE_S1_PASTELL_ID_E', null),
                ],
            ]
        );
    }

    protected function setupStructureSettings(): void
    {
        $this->save(
            $this->structureSettings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'project',
                    'value' => '{"project_generate":true,"project_workflow":true,"project_writing":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'act',
                    'value' => '{"act_generate":true,"generate_number":true}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'convocation',
                    'value' => '{"convocation_idelibre":true,"convocation_mailsec":false}',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'sitting',
                    'value' => '{"sitting_enable":true,"sitting_generate_deliberations_list":true,"sitting_generate_verbal_trial":true}',
                ],
            ]
        );
    }

    protected function setupOfficials(): void
    {
        $this->save(
            $this->officials,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S1 LOSSERAND',
                    'firstname' => 'Frédéric',
                    'email' => 'contact@libriciel.coop',
                    'address' => '140 Rue Aglaonice de Thessalie',
                    'address_supplement' => '',
                    'post_code' => '34170',
                    'city' => 'Castelnau-le-Lez',
                    'phone' => '04 67 65 96 44',
                ],
            ]
        );
    }

    protected function setupUsers(): void
    {
        $this->save(
            $this->users,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => 'S1 ADMIN',
                    'firstname' => 'Josette',
                    'username' => 'admin@wa-s1',
                    'email' => 'wa-s1.admin@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S1 ADMINFONC',
                    'firstname' => 'Albert',
                    'username' => 'adminfonc@wa-s1',
                    'email' => 'wa-s1.adminfonc@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S1 RÉDAC 1',
                    'firstname' => 'Lucien',
                    'username' => 'redac-1@wa-s1',
                    'email' => 'wa-s1.redac-1@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => 'S1 RÉDAC 2',
                    'firstname' => 'Léonie',
                    'username' => 'redac-2@wa-s1',
                    'email' => 'wa-s1.redac-2@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S1 VALIDEUR 1',
                    'firstname' => 'Jean-Louis',
                    'username' => 'valid-1@wa-s1',
                    'email' => 'wa-s1.valid-1@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M.',
                    'lastname' => 'S1 VALIDEUR 2',
                    'firstname' => 'Paul-Emile',
                    'username' => 'valid-2@wa-s1',
                    'email' => 'wa-s1.valid-2@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => 'S1 VALIDEUR 3',
                    'firstname' => 'Marie-Odile',
                    'username' => 'valid-3@wa-s1',
                    'email' => 'wa-s1.valid-3@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => 'S1 VALIDEUR 4',
                    'firstname' => 'Juliette',
                    'username' => 'valid-4@wa-s1',
                    'email' => 'wa-s1.valid-4@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'Mme.',
                    'lastname' => 'S1 VALIDEUR 5',
                    'firstname' => 'Monique',
                    'username' => 'valid-5@wa-s1',
                    'email' => 'wa-s1.valid-5@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'active' => false,
                    'civility' => 'M.',
                    'lastname' => 'S1 VALIDEUR 6',
                    'firstname' => 'François',
                    'username' => 'valid-6@wa-s1',
                    'email' => 'wa-s1.valid-6@' . $this->domain(),
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'civility' => 'M',
                    'lastname' => 'S1 SECRETAIRE',
                    'firstname' => 'Jeannot',
                    'username' => 'secretaire@wa-s1',
                    'email' => 'wa-s1.secretaire-1@' . $this->domain(),
                ],
            ]
        );
    }

    protected function setupStructuresUsers(): void
    {
        $this->save(
            $this->structuresUsers,
            [
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'admin@wa-s1')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'adminfonc@wa-s1')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'redac-1@wa-s1')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'redac-2@wa-s1')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-1@wa-s1')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-2@wa-s1')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-3@wa-s1')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-4@wa-s1')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-5@wa-s1')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'valid-6@wa-s1')],
                ['structure_id' => $this->current($this->structures), 'user_id' => $this->record($this->users, 'secretaire@wa-s1')],
            ]
        );
    }

    protected function setupNotificationsUsers(): void
    {
        $this->save(
            $this->notificationsUsers,
            [
                ['user_id' => $this->record($this->users, 'admin@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'admin@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'admin@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'admin@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'admin@wa-s1'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'adminfonc@wa-s1'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'redac-1@wa-s1'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'redac-2@wa-s1'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-1@wa-s1'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-2@wa-s1'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-3@wa-s1'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-4@wa-s1'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-5@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-5@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-5@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-5@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-5@wa-s1'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'valid-6@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'valid-6@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'valid-6@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'valid-6@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'valid-6@wa-s1'), 'notification_id' => 5],
                ['user_id' => $this->record($this->users, 'secretaire@wa-s1'), 'notification_id' => 1],
                ['user_id' => $this->record($this->users, 'secretaire@wa-s1'), 'notification_id' => 2],
                ['user_id' => $this->record($this->users, 'secretaire@wa-s1'), 'notification_id' => 3],
                ['user_id' => $this->record($this->users, 'secretaire@wa-s1'), 'notification_id' => 4],
                ['user_id' => $this->record($this->users, 'secretaire@wa-s1'), 'notification_id' => 5],
            ]
        );
    }

    protected function setupRoles(): void
    {
        $this->save(
            $this->roles,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Administrateur Fonctionnel',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Rédacteur / Valideur',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Secrétariat général',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'Super Administrateur',
                ],
            ]
        );
    }

    protected function setupRolesUsers(): void
    {
        $this->save(
            $this->rolesUsers,
            [
                ['role_id' => $this->record($this->roles, 'Administrateur'), 'user_id' => $this->record($this->users, 'admin@wa-s1')],
                ['role_id' => $this->record($this->roles, 'Administrateur Fonctionnel'), 'user_id' => $this->record($this->users, 'adminfonc@wa-s1')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'redac-1@wa-s1')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'redac-2@wa-s1')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-1@wa-s1')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-2@wa-s1')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-3@wa-s1')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-4@wa-s1')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-5@wa-s1')],
                ['role_id' => $this->record($this->roles, 'Rédacteur / Valideur'), 'user_id' => $this->record($this->users, 'valid-6@wa-s1')],
                ['role_id' => $this->record($this->roles, 'Secrétariat général'), 'user_id' => $this->record($this->users, 'secretaire@wa-s1')],
            ]
        );
    }

    protected function setupConnecteurs(): void
    {
        $env = getenv();
        $this->save(
            $this->connecteurs,
            [
                [
                    'name' => 'pastell',
                    'url' => "{$env['LIBRICIEL_RECETTE_S1_PASTELL_URL']}/",
                    'tdt_url' => "{$env['LIBRICIEL_RECETTE_S1_PASTELL_TDT_URL']}/modules/actes/actes_transac_post_confirm_api.php",
                    'username' => $env['LIBRICIEL_RECETTE_S1_PASTELL_USERNAME'],
                    'password' => $env['LIBRICIEL_RECETTE_S1_PASTELL_PASSWORD'],
                    'structure_id' => $this->current($this->structures),
                    'connector_type_id' => 1,
                    'connexion_option' => $env['LIBRICIEL_RECETTE_S1_PASTELL_CONNEXION_OPTION'],
                ],
                [
                    'name' => 'idelibre',
                    'url' => $env['LIBRICIEL_RECETTE_S1_IDELIBRE_URL'],
                    'tdt_url' => null,
                    'username' => $env['LIBRICIEL_RECETTE_S1_IDELIBRE_USERNAME'],
                    'password' => $env['LIBRICIEL_RECETTE_S1_IDELIBRE_PASSWORD'],
                    'structure_id' => $this->current($this->structures),
                    'connector_type_id' => 2,
                    'connexion_option' => $env['LIBRICIEL_RECETTE_S1_IDELIBRE_CONNEXION_OPTION'],
                ],
            ]
        );
    }

    protected function setupPastellfluxtypes(): void
    {
        $this->save(
            $this->pastellfluxtypes,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'actes-generique',
                    'description' => 'Actes générique',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'mailsec',
                    'description' => 'Mail securisé',
                ],
            ]
        );
    }

    protected function setupGenerateTemplates(): void
    {
        $this->save(
            $this->generateTemplates,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::PROJECT,
                    'name' => 'Modèle de projet d\'arrêté',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::PROJECT,
                    'name' => 'Modèle de projet de délibération',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::CONVOCATION,
                    'name' => 'Modèle de convocation',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::EXECUTIVE_SUMMARY,
                    'name' => 'Modèle de note de synthèse',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::DELIBERATIONS_LIST,
                    'name' => 'Modèle de liste des délibérations',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::VERBAL_TRIAL,
                    'name' => 'Modèle de procès-verbal',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::ACT,
                    'name' => 'Modèle d\'arrêté',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'generate_template_type_id' => GenerateTemplateType::ACT,
                    'name' => 'Modèle de délibération',
                ],
            ]
        );
    }

    protected function setupGenerateTemplatesFiles(): void
    {
        $this->save(
            $this->files,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S1 Projet arrete.odt'),
                    'path' => $this->paths['S1 Projet arrêté']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S1 Projet arrêté']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de projet d\'arrêté'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S1 Projet deliberation.odt'),
                    'path' => $this->paths['S1 Projet délibération']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S1 Projet délibération']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de projet de délibération'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S1 Convocation.odt'),
                    'path' => $this->paths['S1 Convocation']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S1 Convocation']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S1 Note de synthese.odt'),
                    'path' => $this->paths['S1 Note de synthèse']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S1 Note de synthèse']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S1 Liste des deliberations.odt'),
                    'path' => $this->paths['S1 Liste des délibérations']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S1 Liste des délibérations']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S1 Proces-verbal.odt'),
                    'path' => $this->paths['S1 Procès-verbal']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S1 Procès-verbal']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S1 Acte.odt'),
                    'path' => $this->paths['S1 Acte']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S1 Acte']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle d\'arrêté'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => trim('S1 Deliberation.odt'),
                    'path' => $this->paths['S1 Délibération']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['S1 Délibération']->getSize(),
                    'generate_template_id' => $this->record($this->generateTemplates, 'Modèle de délibération'),
                ],
            ]
        );
    }

    protected function setupDpos(): void
    {
        $this->save(
            $this->dpos,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S1 DPO',
                    'firstname' => 'Théo',
                    'email' => 'wa-s1.dpo@' . $this->domain(),
                ],
            ]
        );
    }

    protected function setupThemes(): void
    {
        $this->save(
            $this->themes,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Moyens généraux',
                    'position' => '1',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Finances',
                    'parent_id' => $this->id('themes', 1),
                    'position' => '10',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Comptabilité',
                    'parent_id' => $this->id('themes', 2),
                    'position' => '101',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Gestion',
                    'parent_id' => $this->id('themes', 3),
                    'position' => '1010',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Ressources humaines',
                    'parent_id' => $this->id('themes', 1),
                    'position' => '11',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 commande publique',
                    'parent_id' => $this->id('themes', 1),
                    'position' => '12',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Service à la population',
                    'position' => '2',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Techniques',
                    'position' => '3',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Environnement',
                    'parent_id' => $this->id('themes', 8),
                    'position' => '30',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Eau potable',
                    'parent_id' => $this->id('themes', 9),
                    'position' => '301',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'active' => false,
                    'name' => 'S1 Thème désactivé',
                    'position' => '4',
                ],
            ]
        );
    }

    protected function setupTypesittings(): void
    {
        $this->save(
            $this->typesittings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Conseil Marjo',
                    'isdeliberating' => true,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                    'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Commission finance',
                    'isdeliberating' => false,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                    'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Commission environnement',
                    'isdeliberating' => false,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                    'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Type séance désactivé',
                    'active' => false,
                    'isdeliberating' => true,
                    'generate_template_convocation_id' => $this->record($this->generateTemplates, 'Modèle de convocation'),
                    'generate_template_executive_summary_id' => $this->record($this->generateTemplates, 'Modèle de note de synthèse'),
                    'generate_template_deliberations_list_id' => $this->record($this->generateTemplates, 'Modèle de liste des délibérations'),
                    'generate_template_verbal_trial_id' => $this->record($this->generateTemplates, 'Modèle de procès-verbal'),
                ],
            ]
        );
    }

    protected function setupSequences(): void
    {
        $this->save(
            $this->sequences,
            [
                [
                    'name' => 'S1 Arrêtés',
                    'comment' => 'Séquence pour les arrêtés',
                    'sequence_num' => 0,
                    'structure_id' => $this->current($this->structures),
                ],
                [
                    'name' => 'S1 Délibérations',
                    'comment' => 'Séquences délibérations',
                    'sequence_num' => 0,
                    'structure_id' => $this->current($this->structures),
                ],
            ]
        );
    }

    protected function setupCounters(): void
    {
        $this->save(
            $this->counters,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Arrêtés TDT',
                    'comment' => 'Arrêtés télétransmissibles',
                    'counter_def' => '1ATDT#000#',
                    'sequence_id' => $this->record($this->sequences, 'S1 Arrêtés'),
                    'reinit_def' => '#MM#',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Arrêtés non TDT',
                    'comment' => 'Arrêtés non télétransmissibles',
                    'counter_def' => '1ANTDT#00#',
                    'sequence_id' => $this->record($this->sequences, 'S1 Arrêtés'),
                    'reinit_def' => '#JJ#',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Délibérations',
                    'comment' => '',
                    'counter_def' => '1DEL#AA##MM##JJ##SSS#',
                    'sequence_id' => $this->record($this->sequences, 'S1 Délibérations'),
                    'reinit_def' => '#AAAA#',
                ],
            ]
        );
    }

    protected function setupTypesacts(): void
    {
        $this->save(
            $this->typesacts,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'DE'),
                    'name' => 'S1 Délibération',
                    'istdt' => true,
                    'isdefault' => true,
                    'counter_id' => $this->record($this->counters, 'S1 Délibérations'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet de délibération'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle de délibération'),
                    'isdeliberating' => true,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AR'),
                    'name' => 'S1 Arrêté TDT',
                    'istdt' => true,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'S1 Arrêtés TDT'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet d\'arrêté'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle d\'arrêté'),
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'AI'),
                    'name' => 'S1 Arrêté non TDT',
                    'istdt' => false,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'S1 Arrêtés non TDT'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet d\'arrêté'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle d\'arrêté'),
                    'isdeliberating' => false,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'nature_id' => $this->record($this->natures, 'CC'),
                    'name' => 'S1 type acte désactivé',
                    'istdt' => true,
                    'active' => false,
                    'isdefault' => false,
                    'counter_id' => $this->record($this->counters, 'S1 Délibérations'),
                    'generate_template_project_id' => $this->record($this->generateTemplates, 'Modèle de projet d\'arrêté'),
                    'generate_template_act_id' => $this->record($this->generateTemplates, 'Modèle d\'arrêté'),
                    'isdeliberating' => true,
                ],
            ]
        );
    }

    protected function setupTypesactsTypesittings(): void
    {
        $this->save(
            $this->typesactsTypesittings,
            [
                ['typesact_id' => $this->record($this->typesacts, 'S1 Délibération'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Conseil Marjo')],
                ['typesact_id' => $this->record($this->typesacts, 'S1 Délibération'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Commission finance')],
                ['typesact_id' => $this->record($this->typesacts, 'S1 Délibération'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Commission environnement')],
                ['typesact_id' => $this->record($this->typesacts, 'S1 Délibération'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Type séance désactivé')],
            ]
        );
    }

    protected function setupActorGroups(): void
    {
        $this->save(
            $this->actorGroups,
            [
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Majorité',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Opposition',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Comm environnement',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Comm finances',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Invités',
                ],
                [
                    'organization_id' => $this->current($this->organizations),
                    'structure_id' => $this->current($this->structures),
                    'name' => 'S1 Groupe désactivé',
                    'active' => false,
                ],
            ]
        );
    }

    protected function setupActors(): void
    {
        $this->save(
            $this->actors,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S1 DOYON',
                    'firstname' => 'Auguste',
                    'email' => 'wa-s1.acteur.doyon@' . $this->domain(),
                    'title' => 'M. le maire',
                    'cellphone' => '0605045748',
                    'rank' => 1,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mme.',
                    'lastname' => 'S1 VARIEUR',
                    'firstname' => 'Joséphine',
                    'email' => 'wa-s1.acteur.varieur@' . $this->domain(),
                    'title' => 'Adjointe au maire',
                    'cellphone' => '0708541239',
                    'rank' => 2,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S1 LOISEAU',
                    'firstname' => 'Maurice',
                    'email' => 'wa-s1.acteur.loiseau@' . $this->domain(),
                    'title' => 'Adjoint environnement',
                    'rank' => 3,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mme.',
                    'lastname' => 'S1 PELLAND',
                    'firstname' => 'Rolande',
                    'email' => 'wa-s1.acteur.pelland@' . $this->domain(),
                    'title' => 'Deuxième adjoint environnement',
                    'rank' => 4,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'active' => false,
                    'civility' => 'Mme.',
                    'lastname' => 'S1 LEBEL',
                    'firstname' => 'Harriette',
                    'email' => 'wa-s1.acteur.lebel@' . $this->domain(),
                    'title' => 'Troisième adjointe',
                    'rank' => 5,
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S1 MANVILLE',
                    'firstname' => 'Renaud',
                    'email' => 'wa-s1.acteur.manville@' . $this->domain(),
                    'title' => '',
                    'cellphone' => '0705040404',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mme.',
                    'lastname' => 'S1 SACREY',
                    'firstname' => 'Caroline',
                    'email' => 'wa-s1.acteur.sacrey@' . $this->domain(),
                    'title' => '',
                    'cellphone' => '',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'M.',
                    'lastname' => 'S1 COURCELLE',
                    'firstname' => 'Armand',
                    'email' => 'wa-s1.acteur.courcelle@' . $this->domain(),
                    'title' => '',
                    'cellphone' => '',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'active' => false,
                    'civility' => 'Mme.',
                    'lastname' => 'S1 DEVOST',
                    'firstname' => 'Estelle',
                    'email' => 'wa-s1.acteur.devost@' . $this->domain(),
                    'title' => '',
                    'cellphone' => '',
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'civility' => 'Mme.',
                    'lastname' => 'S1 ROUZE',
                    'firstname' => 'Diane',
                    'email' => 'wa-s1.acteur.rouze@' . $this->domain(),
                    'title' => '',
                    'cellphone' => '',
                ],
            ]
        );
    }

    protected function setupActorsActorGroups(): void
    {
        $this->save(
            $this->actorsActorGroups,
            [
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.doyon@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.doyon@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Comm finances')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.varieur@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.varieur@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Comm finances')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.loiseau@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.loiseau@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Comm environnement')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.pelland@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Comm environnement')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.lebel@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Majorité')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.lebel@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Comm environnement')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.manville@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Opposition')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.sacrey@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Opposition')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.courcelle@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Opposition')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.courcelle@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Comm finances')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.devost@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Opposition')],
                ['actor_id' => $this->record($this->actors, 'wa-s1.acteur.rouze@' . $this->domain()), 'actor_group_id' => $this->record($this->actorGroups, 'S1 Invités')],
            ]
        );
    }

    protected function setupActorGroupsTypesittings(): void
    {
        $this->save(
            $this->actorGroupsTypesittings,
            [
                ['actor_group_id' => $this->record($this->actorGroups, 'S1 Majorité'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Conseil Marjo')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S1 Opposition'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Conseil Marjo')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S1 Invités'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Conseil Marjo')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S1 Comm finances'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Commission finance')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S1 Comm environnement'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Commission environnement')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S1 Opposition'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Type séance désactivé')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S1 Comm environnement'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Type séance désactivé')],
                ['actor_group_id' => $this->record($this->actorGroups, 'S1 Comm finances'), 'typesitting_id' => $this->record($this->typesittings, 'S1 Type séance désactivé')],
            ]
        );
    }

    protected function setupDraftTemplates(): void
    {
        $this->save(
            $this->draftTemplates,
            [
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 2, 'name' => 'S1 acte arrêté'],
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 1, 'name' => 'S1 projet arrêté'],
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 2, 'name' => 'S1 acte délibération'],
                ['structure_id' => $this->current($this->structures), 'draft_template_type_id' => 1, 'name' => 'S1 projet délibération'],
            ]
        );
    }

    protected function setupDraftTemplatesTypesacts(): void
    {
        $this->save(
            $this->draftTemplatesTypesacts,
            [
                ['draft_template_id' => $this->record($this->draftTemplates, 'S1 acte délibération'), 'typesact_id' => $this->record($this->typesacts, 'S1 Délibération')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S1 projet délibération'), 'typesact_id' => $this->record($this->typesacts, 'S1 Délibération')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S1 acte arrêté'), 'typesact_id' => $this->record($this->typesacts, 'S1 Arrêté TDT')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S1 projet arrêté'), 'typesact_id' => $this->record($this->typesacts, 'S1 Arrêté TDT')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S1 acte arrêté'), 'typesact_id' => $this->record($this->typesacts, 'S1 Arrêté non TDT')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S1 projet arrêté'), 'typesact_id' => $this->record($this->typesacts, 'S1 type acte désactivé')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S1 acte arrêté'), 'typesact_id' => $this->record($this->typesacts, 'S1 Arrêté non TDT')],
                ['draft_template_id' => $this->record($this->draftTemplates, 'S1 projet arrêté'), 'typesact_id' => $this->record($this->typesacts, 'S1 type acte désactivé')],
            ]
        );
    }

    protected function setupDraftTemplatesFiles(): void
    {
        $this->save(
            $this->files,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'gabarit_arrete_libriciel.odt',
                    'path' => $this->paths['gabarit_arrete_libriciel']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['gabarit_arrete_libriciel']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'S1 acte arrêté'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'gabarit_projet_libriciel.odt',
                    'path' => $this->paths['gabarit_projet_libriciel']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['gabarit_projet_libriciel']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'S1 projet arrêté'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'texte_delib2.odt',
                    'path' => $this->paths['texte_delib2']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['texte_delib2']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'S1 acte délibération'),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'name' => 'texte_projet.odt',
                    'path' => $this->paths['texte_projet']->getRealPath(),
                    'mimetype' => 'application/vnd.oasis.opendocument.text',
                    'size' => $this->paths['texte_projet']->getSize(),
                    'draft_template_id' => $this->record($this->draftTemplates, 'S1 projet délibération'),
                ],
            ]
        );
    }

    protected function setupSittings(): void
    {
        $this->save(
            $this->sittings,
            [
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'S1 Commission finance'),
                    'date' => "{$this->year}-01-03 13:00:00",
                    'president_id' => $this->record($this->actors, 'wa-s1.acteur.doyon@' . $this->domain()),
                    'secretary_id' => $this->record($this->actors, 'wa-s1.acteur.varieur@' . $this->domain()),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'S1 Commission environnement'),
                    'date' => "{$this->year}-01-04 14:00:00",
                    'president_id' => $this->record($this->actors, 'wa-s1.acteur.doyon@' . $this->domain()),
                    'secretary_id' => $this->record($this->actors, 'wa-s1.acteur.varieur@' . $this->domain()),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'S1 Type séance désactivé'),
                    'date' => "{$this->year}-01-05 15:00:00",
                    'president_id' => $this->record($this->actors, 'wa-s1.acteur.doyon@' . $this->domain()),
                    'secretary_id' => $this->record($this->actors, 'wa-s1.acteur.varieur@' . $this->domain()),
                ],
                [
                    'structure_id' => $this->current($this->structures),
                    'typesitting_id' => $this->record($this->typesittings, 'S1 Conseil Marjo'),
                    'date' => "{$this->year}-01-06 16:00:00",
                    'president_id' => $this->record($this->actors, 'wa-s1.acteur.doyon@' . $this->domain()),
                    'secretary_id' => $this->record($this->actors, 'wa-s1.acteur.varieur@' . $this->domain()),
                ],
            ]
        );
    }

    protected function setupSittingsStatesittings(): void
    {
        $this->save(
            $this->sittingsStatesittings,
            [
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-01-03 13:00:00"), 'statesitting_id' => 1],
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-01-04 14:00:00"), 'statesitting_id' => 1],
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-01-05 15:00:00"), 'statesitting_id' => 1],
                ['sitting_id' => $this->record($this->sittings, "{$this->year}-01-06 16:00:00"), 'statesitting_id' => 1],
            ]
        );
    }

    protected function setupWorkflows(): void
    {
        $this->apiConnect('admin@wa-s1');

        $this->apiAddWorkflow(
            [
                'name' => 'S1 Etapes simples',
                'description' => '4 étapes simples successives',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Jean-Louis S1valideur1',
                        'users' => [$this->getUserId('valid-1@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Paul-Emile S1valideur2',
                        'users' => [$this->getUserId('valid-2@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Marie-Odile S1valideur3',
                        'users' => [$this->getUserId('valid-3@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Juliette S1valideur4',
                        'users' => [$this->getUserId('valid-4@wa-s1')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S1 Simple concurrent simple',
                'description' => '3 étapes dont une concurrente',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Jean-Louis S1valideur1',
                        'users' => [$this->getUserId('valid-1@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Concurrent: Marie-Odile S1valideur3 OU Juliette S1valideur4',
                        'users' => [
                            $this->getUserId('valid-3@wa-s1'),
                            $this->getUserId('valid-4@wa-s1'),
                        ],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Monique S1valideur5',
                        'users' => [$this->getUserId('valid-5@wa-s1')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S1 Admin fonc secretaire',
                'description' => 'Circuit avec intervention admin fonctionnel et secrétaire',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple: Jean-Louis S1valideur1',
                        'users' => [$this->getUserId('valid-1@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Albert S1Adminfonc',
                        'users' => [$this->getUserId('adminfonc@wa-s1')],
                    ],
                    [
                        'groups' => '',
                        'name' => 'Simple: Jeannot S1Secretaire1',
                        'users' => [$this->getUserId('secretaire@wa-s1')],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S1 Une étape concurrente',
                'description' => '',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Concurrent: (Jean-Louis S1valideur1) / OU / [Paul-Emile S1valideur2] - OU_ Monique S1valideur5',
                        'users' => [
                            $this->getUserId('valid-1@wa-s1'),
                            $this->getUserId('valid-2@wa-s1'),
                            $this->getUserId('valid-5@wa-s1'),
                        ],
                    ],
                ],
            ],
            true
        );

        $this->apiAddWorkflow(
            [
                'name' => 'S1 Circuit désactivé',
                'description' => 'Circuit désactivé',
                'steps' => [
                    [
                        'groups' => '',
                        'name' => 'Simple : Monique S1valideur5',
                        'users' => [$this->getUserId('valid-5@wa-s1')],
                    ],
                ],
            ],
            false
        );
    }

    protected function setup(): void
    {
        $this->execute('BEGIN;');
        $this->setupOrganizations();
        $this->setupStructures();
        $this->setupStructureSettings();
        $this->setupOfficials();
        $this->setupUsers();
        $this->setupStructuresUsers();
        $this->setupNotificationsUsers();
        $this->setupRoles();
        $this->setupRolesUsers();
        $this->setupConnecteurs();
        $this->setupPastellfluxtypes();
        $this->setupGenerateTemplates();
        $this->setupGenerateTemplatesFiles();
        $this->setupDpos();
        $this->setupThemes();
        $this->setupNatures();
        $this->setupMatieres();
        $this->setupClassifications();
        $this->setupTypespiecesjointes();
        $this->setupTypesittings();
        $this->setupSequences();
        $this->setupCounters();
        $this->setupTypesacts();
        $this->setupTypesactsTypesittings();
        $this->setupActorGroups();
        $this->setupActors();
        $this->setupActorsActorGroups();
        $this->setupActorGroupsTypesittings();
        $this->setupDraftTemplates();
        $this->setupDraftTemplatesTypesacts();
        $this->setupDraftTemplatesFiles();
        $this->setupSittings();
        $this->setupSittingsStatesittings();
        $this->execute('COMMIT;');

        $this->setupWorkflows();
    }

    protected function createProjects(): void
    {
        $max = $this->env('LIBRICIEL_RECETTE_S1_NOMBRE_PROJETS', 10);
        for ($idx = 1; $idx <= $max; $idx++) {
            $this->addProjectWithGeneration(
                $this->current($this->organizations),
                $this->current($this->structures),
                $this->record($this->users, 'admin@wa-s1'),
                $this->record($this->themes, 'S1 Gestion'),
                $this->record($this->typesacts, 'S1 Délibération'),
                $this->record($this->draftTemplates, 'S1 acte délibération'),
                $this->record($this->draftTemplates, 'S1 projet délibération'),
            );
        }
    }

    public function run(): void
    {
        parent::run();

        $this->sequences();

        $this->year = (int)date('Y');

        $dirs = [
            'GenerateTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesGenerateTemplates' . DS,
            'DraftTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesDraftTemplates' . DS,
        ];
        $this->paths = [
            'S1 Projet arrêté' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Projet arrete.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 1)),
            'S1 Projet délibération' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Projet deliberation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 2)),
            'S1 Convocation' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Convocation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 3)),
            'S1 Note de synthèse' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Note de synthese.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 4)),
            'S1 Liste des délibérations' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Liste des deliberations.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 5)),
            'S1 Procès-verbal' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Proces-verbal.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 6)),
            'S1 Acte' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Acte.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 7)),
            'S1 Délibération' => $this->createFile($dirs['GenerateTemplates'] . 'S1 Deliberation.odt', 'generate_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('generate_templates', 8)),
            //------------------------------------------------------------------------------------------------------------------
            'gabarit_arrete_libriciel' => $this->createFile($dirs['DraftTemplates'] . 'gabarit_arrete_libriciel.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 1)),
            'gabarit_projet_libriciel' => $this->createFile($dirs['DraftTemplates'] . 'gabarit_projet_libriciel.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 2)),
            'texte_delib2' => $this->createFile($dirs['DraftTemplates'] . 'texte_delib2.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 3)),
            'texte_projet' => $this->createFile($dirs['DraftTemplates'] . 'texte_projet.odt', 'draft_template_id', $this->id('organizations', 1), $this->id('structures', 1), $this->id('draft_templates', 4)),
        ];

        $this->setup();

        $this->sequences();
        $this->createProjects();
        $this->sequences();

        $this->permissions();
    }
}
