<?php
declare(strict_types=1);

namespace LibricielRecette\Test\TestCase\Service;

use Cake\TestSuite\TestCase;
use InvalidArgumentException;
use LibricielRecette\Service\EnvvarService;

class EnvvarServiceTest extends TestCase
{
    /**
     * @dataProvider dataProviderEscapeVarname
     * @dataProvider dataProviderEscapeVarnameUsersStructure1
     * @dataProvider dataProviderEscapeVarnameUsersStructure2
     */
    public function testEscapeVarname(string $string, string $expected): void
    {
        $this->assertEquals($expected, EnvvarService::escapeVarname($string));
    }

    public function dataProviderEscapeVarname(): array
    {
        return [
            ['foo.bar@baz.com', 'FOO_DOT_BAR_AT_BAZ_DOT_COM'],
            ['https://baz.com', 'HTTPS_COLON__SLASH__SLASH_BAZ_DOT_COM'],
        ];
    }

    public function dataProviderEscapeVarnameUsersStructure1(): array
    {
        return [
            ['admin@wa-s1', 'ADMIN_AT_WA_DASH_S1'],
            ['adminfonc@wa-s1', 'ADMINFONC_AT_WA_DASH_S1'],
            ['redac-1@wa-s1', 'REDAC_DASH_1_AT_WA_DASH_S1'],
            ['redac-2@wa-s1', 'REDAC_DASH_2_AT_WA_DASH_S1'],
            ['valid-1@wa-s1', 'VALID_DASH_1_AT_WA_DASH_S1'],
            ['valid-2@wa-s1', 'VALID_DASH_2_AT_WA_DASH_S1'],
            ['valid-3@wa-s1', 'VALID_DASH_3_AT_WA_DASH_S1'],
            ['valid-4@wa-s1', 'VALID_DASH_4_AT_WA_DASH_S1'],
            ['valid-5@wa-s1', 'VALID_DASH_5_AT_WA_DASH_S1'],
            ['valid-6@wa-s1', 'VALID_DASH_6_AT_WA_DASH_S1'],
            ['secretaire@wa-s1', 'SECRETAIRE_AT_WA_DASH_S1'],
        ];
    }

    public function dataProviderEscapeVarnameUsersStructure2(): array
    {
        return [
            ['admin@wa-s2', 'ADMIN_AT_WA_DASH_S2'],
            ['adminfonc@wa-s2', 'ADMINFONC_AT_WA_DASH_S2'],
            ['redac-1@wa-s2', 'REDAC_DASH_1_AT_WA_DASH_S2'],
            ['redac-2@wa-s2', 'REDAC_DASH_2_AT_WA_DASH_S2'],
            ['valid-1@wa-s2', 'VALID_DASH_1_AT_WA_DASH_S2'],
            ['valid-2@wa-s2', 'VALID_DASH_2_AT_WA_DASH_S2'],
            ['valid-3@wa-s2', 'VALID_DASH_3_AT_WA_DASH_S2'],
            ['valid-4@wa-s2', 'VALID_DASH_4_AT_WA_DASH_S2'],
            ['valid-5@wa-s2', 'VALID_DASH_5_AT_WA_DASH_S2'],
            ['secretaire@wa-s2', 'SECRETAIRE_AT_WA_DASH_S2'],
            ['superadmin@wa-s2', 'SUPERADMIN_AT_WA_DASH_S2'],
        ];
    }

    public function testEscapeVarnameNonAscii(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The string "éléphant" contains characters that are not ASCII.');

        EnvvarService::escapeVarname('éléphant');
    }

    public function testReplacementsToMarkdown(): void
    {
        $actual = EnvvarService::replacementsToMarkdown();
        $expected = '| `!` | `"` | `#` | `$` | `%` |
| :--- | :--- | :--- | :--- | :--- |
| `_EXCLAMATION_` | `_QUOTE_` | `_HASH_` | `_DOLLAR_` | `_PERCENT_` |

| `&` | `\'` | `(` | `)` | `*` |
| :--- | :--- | :--- | :--- | :--- |
| `_AMPERSAND_` | `_APOSTROPHE_` | `_LEFT_PARENTHESIS_` | `_RIGHT_PARENTHESIS_` | `_ASTERISK_` |

| `+` | `,` | `-` | `.` | `/` |
| :--- | :--- | :--- | :--- | :--- |
| `_PLUS_` | `_COMMA_` | `_DASH_` | `_DOT_` | `_SLASH_` |

| `:` | `;` | `<` | `=` | `>` |
| :--- | :--- | :--- | :--- | :--- |
| `_COLON_` | `_SEMICOLON_` | `_LESS_THAN_` | `_EQUALS_` | `_GREATER_THAN_` |

| `?` | `@` | `[` | `\` | `]` |
| :--- | :--- | :--- | :--- | :--- |
| `_QUESTION_` | `_AT_` | `_LEFT_SQUARE_BRACKET_` | `_BACKSLASH_` | `_RIGHT_SQUARE_BRACKET_` |

| `^` | `` ` `` | `{` | `\\|` | `}` |
| :--- | :--- | :--- | :--- | :--- |
| `_CIRCUMFLEX_` | `_GRAVE_` | `_LEFT_BRACE_` | `_PIPE_` | `_RIGHT_BRACE_` |

| `~` |
| :--- |
| `_TILDE_` |

';
        $this->assertEquals($expected, $actual);
    }
}
