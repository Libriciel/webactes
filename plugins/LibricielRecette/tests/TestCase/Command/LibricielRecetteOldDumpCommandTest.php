<?php
declare(strict_types=1);

namespace LibricielRecette\Test\TestCase\Command;

use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * @deprecated
 */
class LibricielRecetteOldDumpCommandTest extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'app.Organizations',
        'app.Structures',
        'app.StructureSettings',
        'app.Officials',
        'app.Users',
        'app.StructuresUsers',
        'app.Notifications',
        'app.NotificationsUsers',
        'app.Roles',
        'app.RolesUsers',
        'app.ConnectorTypes',
        'app.Connecteurs',
        'app.Pastellfluxtypes',
        'app.GenerateTemplates',
        'app.Dpos',
        'app.Themes',
        'app.Natures',
        'app.Matieres',
        'app.Classifications',
        'app.Typespiecesjointes',
        'app.Typesittings',
        'app.Sequences',
        'app.Counters',
        'app.Typesacts',
        'app.TypesactsTypesittings',
        'app.Templates',
        'app.Workflows',
        'app.Projects',
        'app.Containers',
        'app.Maindocuments',
        'app.DraftTemplates',
        'app.DraftTemplatesTypesacts',
        'app.ProjectTexts',
        'app.Files',
        'app.ActorGroups',
        'app.Actors',
        'app.ActorsActorGroups',
        'app.ActorGroupsTypesittings',
        'app.Sittings',
        'app.Statesittings',
        'app.SittingsStatesittings',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->useCommandRunner();
    }

    public function testOutputStructure1()
    {
        $this->fetchTable('Connecteurs')->updateAll(['username' => 'username', 'password' => 'password'], ['1 = 1']);

        $this->exec('libriciel_recette_old_dump 1');
        $path = dirname(__DIR__, 2) . DS . 'Expectation' . DS . 'LibricielRecetteOldDumpCommand' . DS . 'Structure1.txt';
        $actual = preg_replace(
            '#(/data/workspace/1/1)/([^/]+)/([0-9]+)/([^/\']+)#m',
            '\1/\2/\3/\3',
            $this->_out->output()
        );
        $actual = preg_replace('/[ \t]+$/m', '', $actual);
        $this->assertEquals(trim(file_get_contents($path)), trim($actual));
        $this->assertExitCode(0);
    }
}
