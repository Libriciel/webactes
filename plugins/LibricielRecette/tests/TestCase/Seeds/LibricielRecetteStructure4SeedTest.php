<?php
declare(strict_types=1);

namespace LibricielRecette\Test\TestCase\Seeds;

use LibricielRecette\TestSuite\AbstractLibricielRecetteSeedTestCase;

class LibricielRecetteStructure4SeedTest extends AbstractLibricielRecetteSeedTestCase
{
    protected function setupEnv(): void
    {
        putenv('LIBRICIEL_RECETTE_S4_PASTELL_ID_E=4');
        putenv('LIBRICIEL_RECETTE_S4_PASTELL_URL=https://pastell.libriciel.fr');
        putenv('LIBRICIEL_RECETTE_S4_PASTELL_TDT_URL=https://s2low.libriciel.fr');
        putenv('LIBRICIEL_RECETTE_S4_PASTELL_USERNAME=username');
        putenv('LIBRICIEL_RECETTE_S4_PASTELL_PASSWORD=password');
        putenv('LIBRICIEL_RECETTE_S4_PASTELL_CONNEXION_OPTION=1');

        putenv('LIBRICIEL_RECETTE_S4_NOMBRE_PROJETS=0');
    }

    public function testOutputStructure4()
    {
        $this->setupEnv();

        $this->exec('migrations seed --plugin=LibricielRecette --seed LibricielRecetteStructure4Seed --connection=test --quiet');
        $this->assertOutputEmpty();
        $this->assertExitCode(0);

        $expected = [
            'actor_groups' => 4,
            'actor_groups_typesittings' => 8,
            'actors' => 7,
            'actors_actor_groups' => 9,
            'classifications' => 1,
            'connecteurs' => 1,
            'connector_types' => 2,
            'counters' => 4,
            'draft_templates' => 2,
            'files' => 9,
            'generate_templates' => 7,
            'matieres' => 66,
            'natures' => 5,
            'notifications' => 5,
            'notifications_users' => 60,
            'organizations' => 1,
            'pastellfluxtypes' => 2,
            'roles' => 5,
            'roles_users' => 12,
            'sequences' => 3,
            'sittings' => 2,
            'sittings_statesittings' => 2,
            'stateacts' => 20,
            'statesittings' => 2,
            'structure_settings' => 4,
            'structures' => 1,
            'structures_users' => 12,
            'themes' => 7,
            'typesacts' => 5,
            'typesacts_typesittings' => 4,
            'typesittings' => 3,
            'typespiecesjointes' => 100,
            'users' => 12,
        ];
        ksort($expected);
        $this->assertEquals($expected, array_filter($this->getTablesCount()));
    }
}
