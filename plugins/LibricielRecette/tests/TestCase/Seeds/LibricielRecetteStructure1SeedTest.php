<?php
declare(strict_types=1);

namespace LibricielRecette\Test\TestCase\Seeds;

use LibricielRecette\TestSuite\AbstractLibricielRecetteSeedTestCase;

class LibricielRecetteStructure1SeedTest extends AbstractLibricielRecetteSeedTestCase
{
    protected function setupEnv(): void
    {
        putenv('LIBRICIEL_RECETTE_S1_PASTELL_ID_E=1');
        putenv('LIBRICIEL_RECETTE_S1_PASTELL_URL=https://pastell.libriciel.fr');
        putenv('LIBRICIEL_RECETTE_S1_PASTELL_TDT_URL=https://s2low.libriciel.fr');
        putenv('LIBRICIEL_RECETTE_S1_PASTELL_USERNAME=username');
        putenv('LIBRICIEL_RECETTE_S1_PASTELL_PASSWORD=password');
        putenv('LIBRICIEL_RECETTE_S1_PASTELL_CONNEXION_OPTION=1');

        putenv('LIBRICIEL_RECETTE_S1_IDELIBRE_URL=https://idelibre.libriciel.fr');
        putenv('LIBRICIEL_RECETTE_S1_IDELIBRE_USERNAME=username');
        putenv('LIBRICIEL_RECETTE_S1_IDELIBRE_PASSWORD=password');
        putenv('LIBRICIEL_RECETTE_S1_IDELIBRE_CONNEXION_OPTION=1');

        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMINFONC_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_1_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_2_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_1_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_2_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_3_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_4_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_5_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_6_AT_WA_DASH_S1=password');
        putenv('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SECRETAIRE_AT_WA_DASH_S1=password');

        putenv('LIBRICIEL_RECETTE_S1_NOMBRE_PROJETS=0');
    }

    public function testOutputStructure1()
    {
        $this->setupEnv();

        $this->exec('migrations seed --plugin=LibricielRecette --seed LibricielRecetteStructure1Seed --connection=test --quiet');
        $this->assertOutputEmpty();
        $this->assertExitCode(0);

        $expected = [
            'actor_groups' => 6,
            'actor_groups_typesittings' => 8,
            'actors' => 10,
            'actors_actor_groups' => 15,
            'classifications' => 1,
            'connecteurs' => 2,
            'connector_types' => 2,
            'counters' => 3,
            'dpos' => 1,
            'draft_templates' => 4,
            'draft_templates_typesacts' => 8,
            'files' => 12,
            'generate_templates' => 8,
            'matieres' => 66,
            'natures' => 5,
            'notifications' => 5,
            'notifications_users' => 55,
            'officials' => 1,
            'organizations' => 1,
            'pastellfluxtypes' => 2,
            'roles' => 5,
            'roles_users' => 11,
            'sequences' => 2,
            'sittings' => 4,
            'sittings_statesittings' => 4,
            'stateacts' => 20,
            'statesittings' => 2,
            'structure_settings' => 4,
            'structures' => 1,
            'structures_users' => 11,
            'themes' => 11,
            'typesacts' => 4,
            'typesacts_typesittings' => 4,
            'typesittings' => 4,
            'typespiecesjointes' => 100,
            'users' => 11,
        ];
        ksort($expected);
        $this->assertEquals($expected, array_filter($this->getTablesCount()));
    }
}
