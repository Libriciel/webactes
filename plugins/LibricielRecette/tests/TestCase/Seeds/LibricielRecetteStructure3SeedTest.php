<?php
declare(strict_types=1);

namespace LibricielRecette\Test\TestCase\Seeds;

use LibricielRecette\TestSuite\AbstractLibricielRecetteSeedTestCase;

class LibricielRecetteStructure3SeedTest extends AbstractLibricielRecetteSeedTestCase
{
    protected function setupEnv(): void
    {
        putenv('LIBRICIEL_RECETTE_S3_PASTELL_ID_E=3');
        putenv('LIBRICIEL_RECETTE_S3_PASTELL_URL=https://pastell.libriciel.fr');
        putenv('LIBRICIEL_RECETTE_S3_PASTELL_TDT_URL=https://s2low.libriciel.fr');
        putenv('LIBRICIEL_RECETTE_S3_PASTELL_USERNAME=username');
        putenv('LIBRICIEL_RECETTE_S3_PASTELL_PASSWORD=password');
        putenv('LIBRICIEL_RECETTE_S3_PASTELL_CONNEXION_OPTION=1');

        putenv('LIBRICIEL_RECETTE_S3_NOMBRE_PROJETS=0');
    }

    public function testOutputStructure3()
    {
        $this->setupEnv();

        $this->exec('migrations seed --plugin=LibricielRecette --seed LibricielRecetteStructure3Seed --connection=test --quiet');
        $this->assertOutputEmpty();
        $this->assertExitCode(0);

        $expected = [
            'classifications' => 1,
            'connecteurs' => 1,
            'connector_types' => 2,
            'matieres' => 66,
            'natures' => 5,
            'notifications' => 5,
            'notifications_users' => 10,
            'organizations' => 1,
            'pastellfluxtypes' => 2,
            'roles' => 5,
            'roles_users' => 2,
            'stateacts' => 20,
            'statesittings' => 2,
            'structure_settings' => 3,
            'structures' => 1,
            'structures_users' => 2,
            'themes' => 1,
            'typesacts' => 2,
            'typespiecesjointes' => 100,
            'users' => 2,
        ];
        ksort($expected);
        $this->assertEquals($expected, array_filter($this->getTablesCount()));
    }
}
