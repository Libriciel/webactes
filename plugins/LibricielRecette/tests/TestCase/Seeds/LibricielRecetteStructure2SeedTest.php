<?php
declare(strict_types=1);

namespace LibricielRecette\Test\TestCase\Seeds;

use LibricielRecette\TestSuite\AbstractLibricielRecetteSeedTestCase;

class LibricielRecetteStructure2SeedTest extends AbstractLibricielRecetteSeedTestCase
{
    protected function setupEnv(): void
    {
        putenv('LIBRICIEL_RECETTE_S2_PASTELL_ID_E=2');
        putenv('LIBRICIEL_RECETTE_S2_PASTELL_URL=https://pastell.libriciel.fr');
        putenv('LIBRICIEL_RECETTE_S2_PASTELL_TDT_URL=https://s2low.libriciel.fr');
        putenv('LIBRICIEL_RECETTE_S2_PASTELL_USERNAME=username');
        putenv('LIBRICIEL_RECETTE_S2_PASTELL_PASSWORD=password');
        putenv('LIBRICIEL_RECETTE_S2_PASTELL_CONNEXION_OPTION=1');

        putenv('LIBRICIEL_RECETTE_S2_NOMBRE_PROJETS=0');
    }

    public function testOutputStructure2()
    {
        $this->setupEnv();

        $this->exec('migrations seed --plugin=LibricielRecette --seed LibricielRecetteStructure2Seed --connection=test --quiet');
        $this->assertOutputEmpty();
        $this->assertExitCode(0);

        $expected = [
            'actor_groups' => 6,
            'actor_groups_typesittings' => 11,
            'actors' => 12,
            'actors_actor_groups' => 19,
            'classifications' => 1,
            'connecteurs' => 1,
            'connector_types' => 2,
            'counters' => 2,
            'dpos' => 1,
            'draft_templates' => 4,
            'draft_templates_typesacts' => 6,
            'files' => 12,
            'generate_templates' => 8,
            'matieres' => 66,
            'natures' => 5,
            'notifications' => 5,
            'notifications_users' => 55,
            'officials' => 1,
            'organizations' => 1,
            'pastellfluxtypes' => 2,
            'roles' => 5,
            'roles_users' => 11,
            'sequences' => 2,
            'sittings' => 4,
            'sittings_statesittings' => 4,
            'stateacts' => 20,
            'statesittings' => 2,
            'structure_settings' => 4,
            'structures' => 1,
            'structures_users' => 11,
            'themes' => 11,
            'typesacts' => 3,
            'typesacts_typesittings' => 5,
            'typesittings' => 5,
            'typespiecesjointes' => 100,
            'users' => 11,
        ];
        ksort($expected);
        $this->assertEquals($expected, array_filter($this->getTablesCount()));
    }
}
