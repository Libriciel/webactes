# Paramétrage automatisé

## Paramétrage de recette pour les structures S1, S2, S3 et S4

| Structure   | Acte : num. auto. | Acte : génération | Convocation : idelibre | Convocation : mail sécurisé | Séance : projets | Séance : génération listes des délibs. | Séance : génération des PV | Projet : génération | Projet : rédaction collabora | Projet : circuit de validation | Signature |
|:------------|:------------------|:------------------|:-----------------------|:----------------------------|:-----------------|:---------------------------------------|:---------------------------|:--------------------|:-----------------------------|:-------------------------------|:----------|
| Structure 1 | __Oui__           | __Oui__           | __Oui__                | Non                         | __Oui__          | Non                                    | Non                        | __Oui__             | __Oui__                      | __Oui__                        | IP 4      |
| Structure 2 | __Oui__           | __Oui__           | Non                    | __Oui__                     | __Oui__          | __Oui__                                | __Oui__                    | __Oui__             | __Oui__                      | __Oui__                        | IP 5      |
| Structure 3 | Non               | Non               | Non                    | Non                         | Non              | Non                                    | Non                        | Non                 | Non                          | Non                            | IP 5      |
| Structure 4 | Non               | __Oui__           | __Oui__                | Non                         | __Oui__          | Non                                    | Non                        | Non                 | Non                          | __Oui__                        | IP 5      |

### Avant de lancer la commande

- [Variables d'environnement](docs/env.md)
- [Liste des acteurs et des utilisateurs par collectivité et par structure](docs/listes.md)

### Commande

```bash
bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteSeed \
&& bin/cake-wrapper.sh trees_recovery
```

#### Ancienne version (dépréciée)

```bash
bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteOldSeed \
&& bin/cake-wrapper.sh trees_recovery \
&& bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteOldWorkflowsSeed
```

### Suppression des enregistrements

#### En base de données

```sql
DELETE FROM actors_actor_groups; DELETE FROM actor_groups_typesittings; DELETE FROM notifications_users; DELETE FROM organizations ;
```

#### Sur disque

```bash
rm -rf /data/workspace/* && find tmp/ logs/ -type f -exec rm {} \;
```

## Paramétrages pour avoir une masse de données

Voir la [partie dédiée](docs/massive.md).

## Export en console des _seeds_ pour les données en base de données

```bash
# Pour n'exporter que la structure d'identifiant 1: bin/cake-wrapper.sh libriciel_recette_dump 1
bin/cake-wrapper.sh libriciel_recette_dump
```

## Affichage en console des acteurs et des utilisateurs

```bash
# Pour n'exporter que la structure d'identifiant 1: bin/cake-wrapper.sh libriciel_recette_summary 1
bin/cake-wrapper.sh libriciel_recette_summary
```

## Paramétrage des 2 structures de démo (déprécié)

```bash
bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteDemo1Seed \
&& bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteDemo2Seed \
&& bin/cake-wrapper.sh trees_recovery
```

```dotenv
#-----------------------------------------------------------------------------------------------------------------------
# @deprecated (DEMO1)
#-----------------------------------------------------------------------------------------------------------------------
LIBRICIEL_RECETTE_DEMO1_PASTELL_ID_E=9
LIBRICIEL_RECETTE_DEMO1_PASTELL_URL=https://pastell.partenaire.libriciel.fr
LIBRICIEL_RECETTE_DEMO1_PASTELL_TDT_URL=https://s2low.formations.libriciel.fr
LIBRICIEL_RECETTE_DEMO1_PASTELL_USERNAME=@see PASTELL_TEST_USER
LIBRICIEL_RECETTE_DEMO1_PASTELL_PASSWORD=@see PASTELL_TEST_PASSWORD
LIBRICIEL_RECETTE_DEMO1_PASTELL_CONNEXION_OPTION=1

LIBRICIEL_RECETTE_DEMO1_IDELIBRE_URL=https://idelibre.adullact.org
LIBRICIEL_RECETTE_DEMO1_IDELIBRE_USERNAME=@see IDELIBRE_TEST_USER
LIBRICIEL_RECETTE_DEMO1_IDELIBRE_PASSWORD=@see IDELIBRE_TEST_PASSWORD
LIBRICIEL_RECETTE_DEMO1_IDELIBRE_CONNEXION_OPTION=@see IDELIBRE_TEST_CONNEXION

LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_S_DOT_BLANC=@todo set password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_M_DOT_VERT=@todo set password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_J_DOT_ORANGE=@todo set password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_R_DOT_VIOLET=@todo set password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_R_DOT_CYAN=@todo set password

LIBRICIEL_RECETTE_DEMO1_NOMBRE_PROJETS=0

#-----------------------------------------------------------------------------------------------------------------------
# @deprecated (DEMO2)
#-----------------------------------------------------------------------------------------------------------------------
LIBRICIEL_RECETTE_DEMO2_PASTELL_ID_E=11
LIBRICIEL_RECETTE_DEMO2_PASTELL_URL=https://pastell.partenaire.libriciel.fr
LIBRICIEL_RECETTE_DEMO2_PASTELL_TDT_URL=https://s2low.formations.libriciel.fr
LIBRICIEL_RECETTE_DEMO2_PASTELL_USERNAME=@see PASTELL_TEST_USER
LIBRICIEL_RECETTE_DEMO2_PASTELL_PASSWORD=@see PASTELL_TEST_PASSWORD
LIBRICIEL_RECETTE_DEMO2_PASTELL_CONNEXION_OPTION=1

LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_S_DOT_WHITE=@todo set password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_M_DOT_GREEN=@todo set password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_V_DOT_YELLOW=@todo set password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_S_DOT_PURPLE=@todo set password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_E_DOT_LIME=@todo set password

LIBRICIEL_RECETTE_DEMO2_NOMBRE_PROJETS=0
```
