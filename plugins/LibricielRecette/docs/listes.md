# Liste des acteurs et des utilisateurs par collectivité et par structure

Ces listes ont été générées automatiquement au moyen de la commande `bin/cake-wrapper.sh libriciel_recette_summary`.

## Collectivité: Recette WA structure 1

### Structure: Recette WA S1

#### Utilisateurs

| Actif | Identifiant      | Civilité | Nom           | Prénom      | E-mail                                        | Rôle                       | Mot de passe                                                        |
|-------|------------------|----------|---------------|-------------|-----------------------------------------------|----------------------------|---------------------------------------------------------------------|
| Oui   | adminfonc@wa-s1  | M.       | S1 ADMINFONC  | Albert      | wa-s1.adminfonc@mailcatchall.libriciel.net    | Administrateur Fonctionnel | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMINFONC_AT_WA_DASH_S1    |
| Oui   | admin@wa-s1      | Mme.     | S1 ADMIN      | Josette     | wa-s1.admin@mailcatchall.libriciel.net        | Administrateur             | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_AT_WA_DASH_S1        |
| Oui   | redac-1@wa-s1    | M.       | S1 RÉDAC 1    | Lucien      | wa-s1.redac-1@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_1_AT_WA_DASH_S1 |
| Oui   | redac-2@wa-s1    | Mme.     | S1 RÉDAC 2    | Léonie      | wa-s1.redac-2@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_2_AT_WA_DASH_S1 |
| Oui   | secretaire@wa-s1 | M        | S1 SECRETAIRE | Jeannot     | wa-s1.secretaire-1@mailcatchall.libriciel.net | Secrétariat général        | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SECRETAIRE_AT_WA_DASH_S1   |
| Oui   | valid-1@wa-s1    | M.       | S1 VALIDEUR 1 | Jean-Louis  | wa-s1.valid-1@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_1_AT_WA_DASH_S1 |
| Oui   | valid-2@wa-s1    | M.       | S1 VALIDEUR 2 | Paul-Emile  | wa-s1.valid-2@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_2_AT_WA_DASH_S1 |
| Oui   | valid-3@wa-s1    | Mme.     | S1 VALIDEUR 3 | Marie-Odile | wa-s1.valid-3@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_3_AT_WA_DASH_S1 |
| Oui   | valid-4@wa-s1    | Mme.     | S1 VALIDEUR 4 | Juliette    | wa-s1.valid-4@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_4_AT_WA_DASH_S1 |
| Oui   | valid-5@wa-s1    | Mme.     | S1 VALIDEUR 5 | Monique     | wa-s1.valid-5@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_5_AT_WA_DASH_S1 |
| Non   | valid-6@wa-s1    | M.       | S1 VALIDEUR 6 | François    | wa-s1.valid-6@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_6_AT_WA_DASH_S1 |

#### Acteurs

| Actif | Civilité | Nom          | Prénom    | E-mail                                            | Groupes                            |
|-------|----------|--------------|-----------|---------------------------------------------------|------------------------------------|
| Oui   | M.       | S1 COURCELLE | Armand    | wa-s1.acteur.courcelle@mailcatchall.libriciel.net | S1 Comm finances, S1 Opposition    |
| Non   | Mme.     | S1 DEVOST    | Estelle   | wa-s1.acteur.devost@mailcatchall.libriciel.net    | S1 Opposition                      |
| Oui   | M.       | S1 DOYON     | Auguste   | wa-s1.acteur.doyon@mailcatchall.libriciel.net     | S1 Comm finances, S1 Majorité      |
| Non   | Mme.     | S1 LEBEL     | Harriette | wa-s1.acteur.lebel@mailcatchall.libriciel.net     | S1 Comm environnement, S1 Majorité |
| Oui   | M.       | S1 LOISEAU   | Maurice   | wa-s1.acteur.loiseau@mailcatchall.libriciel.net   | S1 Comm environnement, S1 Majorité |
| Oui   | M.       | S1 MANVILLE  | Renaud    | wa-s1.acteur.manville@mailcatchall.libriciel.net  | S1 Opposition                      |
| Oui   | Mme.     | S1 PELLAND   | Rolande   | wa-s1.acteur.pelland@mailcatchall.libriciel.net   | S1 Comm environnement              |
| Oui   | Mme.     | S1 ROUZE     | Diane     | wa-s1.acteur.rouze@mailcatchall.libriciel.net     | S1 Invités                         |
| Oui   | Mme.     | S1 SACREY    | Caroline  | wa-s1.acteur.sacrey@mailcatchall.libriciel.net    | S1 Opposition                      |
| Oui   | Mme.     | S1 VARIEUR   | Joséphine | wa-s1.acteur.varieur@mailcatchall.libriciel.net   | S1 Comm finances, S1 Majorité      |

## Collectivité: Recette WA structure 2

### Structure: Recette WA S2

#### Utilisateurs

| Actif | Identifiant      | Civilité | Nom            | Prénom       | E-mail                                      | Rôle                       | Mot de passe                                                        |
|-------|------------------|----------|----------------|--------------|---------------------------------------------|----------------------------|---------------------------------------------------------------------|
| Oui   | adminfonc@wa-s2  | M.       | S2 ADMINFONC   | Ernest       | wa-s2.adminfonc@mailcatchall.libriciel.net  | Administrateur Fonctionnel | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMINFONC_AT_WA_DASH_S2    |
| Oui   | admin@wa-s2      | M.       | S2 ADMIN       | Henriette    | wa-s2.admin@mailcatchall.libriciel.net      | Administrateur             | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_AT_WA_DASH_S2        |
| Oui   | redac-1@wa-s2    | M.       | S2 RÉDACTEUR 1 | Christophe   | wa-s2.redac-1@mailcatchall.libriciel.net    | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_1_AT_WA_DASH_S2 |
| Oui   | redac-2@wa-s2    | Mme.     | S2 RÉDACTEUR 2 | Marie-Pierre | wa-s2.redac-2@mailcatchall.libriciel.net    | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_2_AT_WA_DASH_S2 |
| Oui   | secretaire@wa-s2 | M.       | S2 SECRETAIRE  | Jérôme       | wa-s2.secretaire@mailcatchall.libriciel.net | Secrétariat général        | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SECRETAIRE_AT_WA_DASH_S2   |
| Oui   | superadmin@wa-s2 | Mme.     | S2 SUPERADMIN  | Aria         | wa-s2.superadmin@mailcatchall.libriciel.net | Super Administrateur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_WA_DASH_S2   |
| Oui   | valid-1@wa-s2    | M.       | S2 VALIDEUR 1  | Anthony      | wa-s2.valid-1@mailcatchall.libriciel.net    | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_1_AT_WA_DASH_S2 |
| Oui   | valid-2@wa-s2    | M.       | S2 VALIDEUR 2  | Maurice      | wa-s2.valid-2@mailcatchall.libriciel.net    | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_2_AT_WA_DASH_S2 |
| Oui   | valid-3@wa-s2    | Mme.     | S2 VALIDEUR 3  | Jeanne       | wa-s2.valid-3@mailcatchall.libriciel.net    | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_3_AT_WA_DASH_S2 |
| Oui   | valid-4@wa-s2    | Mme.     | S2 VALIDEUR 4  | Julie        | wa-s2.valid-4@mailcatchall.libriciel.net    | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_4_AT_WA_DASH_S2 |
| Oui   | valid-5@wa-s2    | Mme.     | S2 VALIDEUR 5  | Béatrice     | wa-s2.valid-5@mailcatchall.libriciel.net    | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_5_AT_WA_DASH_S2 |


#### Acteurs

| Actif | Civilité | Nom           | Prénom    | E-mail                                             | Groupes                                                    |
|-------|----------|---------------|-----------|----------------------------------------------------|------------------------------------------------------------|
| Oui   | M.       | S2 ALLAIN     | Alexandre | wa-s2.acteur.allain@mailcatchall.libriciel.net     | S2 Commission voirie, S2 Opposition                        |
| Non   | Mme      | S2 BOSSÉ      | France    | wa-s2.acteur.bosse@mailcatchall.libriciel.net      | S2 Opposition                                              |
| Non   | Mme      | S2 CHAPIN     | Laurence  | wa-s2.acteur.chapin@mailcatchall.libriciel.net     | S2 Commission voirie, S2 Majorité                          |
| Oui   | Mme      | S2 DAVIS      | Victoria  | wa-s2.acteur.davis@mailcatchall.libriciel.net      | S2 Commission urbanisme, S2 Commission voirie, S2 Majorité |
| Oui   | M.       | S2 DODIN      | Louis     | wa-s2.acteur.dodin@mailcatchall.libriciel.net      | S2 Commission urbanisme, S2 Majorité                       |
| Oui   | Mme      | S2 LANOIE     | Gabrielle | wa-s2.acteur.lanoie@mailcatchall.libriciel.net     | S2 Opposition                                              |
| Oui   | M.       | S2 LAPOINTE   | Georges   | wa-s2.acteur.lapointe@mailcatchall.libriciel.net   | S2 Invités                                                 |
| Oui   | M.       | S2 MAROIS     | Chandler  | wa-s2.acteur.marois@mailcatchall.libriciel.net     | S2 Commission voirie, S2 Majorité                          |
| Oui   | Mme      | S2 MOUET      | Aubert    | wa-s2.acteur.mouet@mailcatchall.libriciel.net      | S2 Invités                                                 |
| Oui   | Mme      | S2 PELLERIN   | Léonie    | wa-s2.acteur.pellerin@mailcatchall.libriciel.net   | S2 Commission voirie                                       |
| Non   | M.       | S2 ROSSIGNOL  | Stéphane  | wa-s2.acteur.rossignol@mailcatchall.libriciel.net  | S2 Invités                                                 |
| Oui   | M.       | S2 THERRIAULT | Martin    | wa-s2.acteur.therriault@mailcatchall.libriciel.net | S2 Commission voirie, S2 Opposition                        |

## Collectivité: Recette WA structure 3

### Structure: Recette WA S3

#### Utilisateurs

| Actif | Identifiant | Civilité | Nom          | Prénom  | E-mail                                 | Rôle                 | Mot de passe                                                 |
|-------|-------------|----------|--------------|---------|----------------------------------------|----------------------|--------------------------------------------------------------|
| Oui   | admin@wa-s3 | Mme      | S3 ADMIN     | Camille | wa-s3.admin@mailcatchall.libriciel.net | Administrateur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_AT_WA_DASH_S3 |
| Oui   | redac@wa-s3 | M.       | S3 RÉDACTEUR | Kévin   | wa-s3.redac@mailcatchall.libriciel.net | Rédacteur / Valideur | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_AT_WA_DASH_S3 |

#### Acteurs

Cette structure ne comporte aucun acteur.

## Collectivité: Recette WA structure 4

### Structure: Recette WA S4

#### Utilisateurs


| Actif | Identifiant        | Civilité | Nom             | Prénom   | E-mail                                        | Rôle                       | Mot de passe                                                             |
|-------|--------------------|----------|-----------------|----------|-----------------------------------------------|----------------------------|--------------------------------------------------------------------------|
| Oui   | adminfonc@wa-s4    | M.       | S4 ADMINFONC    | Lucas    | wa-s4.adminfonc@mailcatchall.libriciel.net    | Administrateur Fonctionnel | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMINFONC_AT_WA_DASH_S4         |
| Oui   | admin@wa-s4        | M.       | S4 ADMIN        | Raphaël  | wa-s4.admin@mailcatchall.libriciel.net        | Administrateur             | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_AT_WA_DASH_S4             |
| Oui   | redac-1@wa-s4      | M.       | S4 RÉDACTEUR 1  | Arthur   | wa-s4.redac-1@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_1_AT_WA_DASH_S4      |
| Oui   | redac-2@wa-s4      | Mme      | S4 RÉDACTEUR 2  | Alice    | wa-s4.redac-2@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_2_AT_WA_DASH_S4      |
| Oui   | redac-3@wa-s4      | Mme      | S4 RÉDACTEUR 3  | Marjorie | wa-s4.redac-3@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_3_AT_WA_DASH_S4      |
| Oui   | redac-4@wa-s4      | M.       | S4 RÉDACTEUR 4  | Adam     | wa-s4.redac-4@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_4_AT_WA_DASH_S4      |
| Oui   | secretaire-1@wa-s4 | Mme      | S4 SECRETAIRE 1 | Lucie    | wa-s4.secretaire-1@mailcatchall.libriciel.net | Secrétariat général        | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SECRETAIRE_DASH_1_AT_WA_DASH_S4 |
| Oui   | secretaire-2@wa-s4 | Mme      | S4 SECRETAIRE 2 | Jade     | wa-s4.secretaire-2@mailcatchall.libriciel.net | Secrétariat général        | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SECRETAIRE_DASH_2_AT_WA_DASH_S4 |
| Oui   | valid-1@wa-s4      | Mme      | S4 VALIDEUR 1   | Sonia    | wa-s4.valid-1@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_1_AT_WA_DASH_S4      |
| Oui   | valid-2@wa-s4      | M.       | S4 VALIDEUR 2   | Alphonse | wa-s4.valid-2@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_2_AT_WA_DASH_S4      |
| Oui   | valid-3@wa-s4      | Mme      | S4 VALIDEUR 3   | Elodie   | wa-s4.valid-3@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_3_AT_WA_DASH_S4      |
| Oui   | valid-4@wa-s4      | M.       | S4 VALIDEUR 4   | Patrick  | wa-s4.valid-4@mailcatchall.libriciel.net      | Rédacteur / Valideur       | LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_4_AT_WA_DASH_S4      |

#### Acteurs

| Actif | Civilité | Nom           | Prénom    | E-mail                                             | Groupes                            |
|-------|----------|---------------|-----------|----------------------------------------------------|------------------------------------|
| Oui   | M        | S4 BARBE      | Robert    | wa-s4.acteur.barbe@mailcatchall.libriciel.net      | S4 Commission finance, S4 Majorité |
| Non   | M        | S4 DÉSACTIVÉ  | Maël      | wa-s4.acteur.desactive@mailcatchall.libriciel.net  | S4 Majorité                        |
| Oui   | Mme      | S4 FERNANDES  | Valentine | wa-s4.acteur.fernandes@mailcatchall.libriciel.net  | S4 Majorité                        |
| Oui   | Mme      | S4 INVITÉE    | Ophélie   | wa-s4.acteur.invitee@mailcatchall.libriciel.net    | S4 Invités                         |
| Oui   | M        | S4 LEROY      | Noémie    | wa-s4.acteur.leroy@mailcatchall.libriciel.net      | S4 Commission finance, S4 Majorité |
| Oui   | m        | S4 MASSE      | Henri     | wa-s4.acteur.masse@mailcatchall.libriciel.net      | S4 Opposition                      |
| Oui   | M        | S4 OPPOSITION | Jérémie   | wa-s4.acteur.opposition@mailcatchall.libriciel.net | S4 Opposition                      |
