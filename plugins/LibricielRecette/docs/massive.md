# Paramétrages pour avoir une masse de données

Les paramétrages ci-dessous ont été testés avec un [_Intel(R) Core(TM)_ i7-9850H CPU @ 2.60GHz](https://ark.intel.com/content/www/fr/fr/ark/products/191047/intel-core-i7-9850h-processor-12m-cache-up-to-4-60-ghz.html) ayant 32 Go de RAM.

@fixme: nouveaux temps

## Limitation connue

La création de dossiers est buggée; elle conduit à une erreur SQL au bout de la création de plusieurs centaines de dossiers.
Le mieux est de ne pas créer des dossiers lorsqu'on crée plusieurs organisations et structures ou de créer beaucoup de dossiers au sein d'une organisation unique et d'une structure unique.

## Structures créées

Pour chaque structure créée, un utilisateur superadmin est créé aussi.

Son identifiant correspond au numéro de l'organisation et de la structure, son mot de passe est à définir obligatoirement dans le `.env`.

| Organisation | Structure | Identifiant                          | `.env`                                                                                               |
|-------------:|----------:|--------------------------------------|------------------------------------------------------------------------------------------------------|
|            1 |         1 | `superadmin@massive-o-00001-s-00001` | `LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00001` |
|            1 |         2 | `superadmin@massive-o-00001-s-00002` | `LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00002` |
|            2 |         1 | `superadmin@massive-o-00002-s-00001` | `LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_MASSIVE_DASH_O_DASH_00002_DASH_S_DASH_00001` |

De même, pour chaque structure créée, un paramétrage du connecteur Pastell est créé à partir des variables d'environnement.

| Organisation | Structure | Préfixe `.env`                                      |
|-------------:|----------:|-----------------------------------------------------|
|            1 |         1 | `LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL` |
|            1 |         2 | `LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00002_PASTELL` |
|            2 |         1 | `LIBRICIEL_RECETTE_MASSIVE_O_00002_S_00001_PASTELL` |

Exemple, les variables d'environnement pour la configuration Pastell de la structure 1 de l'organisation 1.

| Libellé dans l'interface                   | `.env`                                                               | Valeur par défaut                         |
|--------------------------------------------|----------------------------------------------------------------------|-------------------------------------------|
| Url de Pastell                             | `LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_URL`              | `https://pastell.partenaire.libriciel.fr` |
| Identifiant de connexion Pastell           | `LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_USERNAME`         | `username`                                |
| Mot de passe                               | `LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_PASSWORD`         | `password`                                |
| Identifiant de collectivité Pastell (id_e) | `LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_ID_E`             | Le numéro de la structure                 |
|                                            | `LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_CONNEXION_OPTION` | `1`                                       |
| Url de Slow                                | `LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_TDT_URL`          | `https://s2low.partenaires.libriciel.fr`  |

### Configuration des structures

| Catégorie   | Configuration                                 | Valeur |
|-------------|-----------------------------------------------|--------|
| Acte        | Numérotation automatique des actes            | Oui    |
| Acte        | Génération des actes                          | Oui    |
| Convocation | Envoi des convocations via I-délibRE          | Oui    |
| Convocation | Envoi des convocations via mail sécurisé      | -      |
| Séance      | Mise en séance des projets                    | Oui    |
| Séance      | Génération des listes des délibérations       | Oui    |
| Séance      | Génération des procès-verbaux                 | Oui    |
| Projet      | Génération des projets                        | Oui    |
| Projet      | Rédaction des textes avec Collabora online    | Oui    |
| Projet      | Circuit de validation                         | Oui    |


## Avec les valeurs par défaut

### Nombre d'enregistrements

| table           | enregistrements |
|-----------------|-----------------|
| actors          | 8000            |
| actor_groups    | 8000            |
| counters        | 8000            |
| draft_templates | 16000           |
| organizations   | 20              |
| projects        | 0               |
| sequences       | 8000            |
| sittings        | 8000            |
| structures      | 400             |
| themes          | 16000           |
| users           | 2400            |
| typesacts       | 8000            |
| typesittings    | 32000           |
| workflows       | 8000            |

### `.env`

```
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_ACTORS=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_ACTOR_GROUPS=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_COUNTERS=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_DRAFT_TEMPLATES=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_ORGANIZATIONS=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_PROJECTS=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_SEQUENCES=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_SITTINGS=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_STRUCTURES=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_THEMES=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_USERS=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_TYPESACTS=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_TYPESITTINGS=20
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_WORKFLOWS=20

# Configuration du connecteur Pastell pour la structure 1, organisation 1
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_URL=https://pastell.partenaire.libriciel.fr
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_USERNAME=username
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_PASSWORD=password
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_ID_E=1
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_CONNEXION_OPTION=1
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_TDT_URL=https://s2low.partenaires.libriciel.fr

# Identifiant superadmin@massive-o-00001-s-00001
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00001=password
# Identifiant superadmin@massive-o-00001-s-00002
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00002=password
...
# Identifiant superadmin@massive-o-00020-s-00020
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_MASSIVE_DASH_O_DASH_00020_DASH_S_DASH_00020=password
```

### Temps d'exécution

| Commande                                                                                                |  Temps (s) |
|:--------------------------------------------------------------------------------------------------------|-----------:|
| `bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteMassiveDataSeed`  |       6846 |
| `bin/cake-wrapper.sh trees_recovery`                                                                    |        189 |

### Commande à lancer

```bash
chown -R www-data:www-data /var/lib/php/sessions \
&& bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteMassiveDataSeed \
&& bin/cake-wrapper.sh trees_recovery
```

#### Ancienne version (dépréciée)

```bash
chown -R www-data:www-data /var/lib/php/sessions \
&& bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteOldMassiveDataSeed \
&& bin/cake-wrapper.sh trees_recovery \
&& bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteOldWorkflowsMassiveDataSeed
```

## Configurations typiques

### Organisations et structures

50 organisations, 50 structures par organisation, 1 superadmin par structure.

Cette configuration permet de tester les parties réservées au superadmin: _Structures_ et _Tous les utilisateurs_.

#### Nombre d'enregistrements

| table           | enregistrements |
|-----------------|-----------------|
| actors          | 0               |
| actor_groups    | 0               |
| counters        | 0               |
| draft_templates | 0               |
| organizations   | 50              |
| projects        | 0               |
| sequences       | 0               |
| sittings        | 0               |
| structures      | 2500            |
| themes          | 0               |
| users           | 2500            |
| typesacts       | 0               |
| typesittings    | 0               |
| workflows       | 0               |

#### Temps d'exécution

| Commande                                                                                               | Temps (s) |
|:-------------------------------------------------------------------------------------------------------|----------:|
| `bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteMassiveDataSeed` |       845 |
| `bin/cake-wrapper.sh trees_recovery` (cette commande ne sert pas vraiment, autant ne pas la lancer)    |      2442 |

#### `.env`

```
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_ACTORS=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_ACTOR_GROUPS=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_COUNTERS=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_DRAFT_TEMPLATES=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_GENERATE_TEMPLATES=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_ORGANIZATIONS=50
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_PROJECTS=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_SEQUENCES=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_SITTINGS=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_STRUCTURES=50
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_THEMES=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_USERS=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_TYPESACTS=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_TYPESITTINGS=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_WORKFLOWS=0

# Configuration du connecteur Pastell pour la structure 1, organisation 1
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_URL=https://pastell.partenaire.libriciel.fr
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_USERNAME=username
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_PASSWORD=password
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_ID_E=1
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_CONNEXION_OPTION=1
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_TDT_URL=https://s2low.partenaires.libriciel.fr

# Identifiant superadmin@massive-o-00001-s-00001
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00001=password
# Identifiant superadmin@massive-o-00001-s-00002
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00002=password
...
# Identifiant superadmin@massive-o-00050-s-00050
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_MASSIVE_DASH_O_DASH_00050_DASH_S_DASH_00050=password
```

#### Commande à lancer

```bash
chown -R www-data:www-data /var/lib/php/sessions \
&& bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteMassiveDataSeed
```

##### Ancienne version (dépréciée)

```bash
chown -R www-data:www-data /var/lib/php/sessions \
&& bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteOldMassiveDataSeed
```

### Utilisateurs

1 organisation, 1 structure, 500 utilisateurs (100 par rôle), 100 éléments de paramétrage, 0 projet.

Cette configuration permet de tester au sein d'une structure avec beaucoup d'enregistrements.

#### Nombre d'enregistrements

| table           | enregistrements |
|-----------------|-----------------|
| actors          | 0               |
| actor_groups    | 0               |
| counters        | 0               |
| draft_templates | 0               |
| organizations   | 1               |
| projects        | 0               |
| sequences       | 0               |
| sittings        | 0               |
| structures      | 1               |
| themes          | 0               |
| users           | 501             |
| typesacts       | 0               |
| typesittings    | 0               |
| workflows       | 0               |

#### Temps d'exécution

| Commande                                                                                                           | Temps (s) |
|:-------------------------------------------------------------------------------------------------------------------|----------:|
| `bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteMassiveDataSeed`             |        62 |
| `bin/cake-wrapper.sh trees_recovery`                                                                               |         1 |

#### `.env`

```
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_ACTORS=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_ACTOR_GROUPS=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_COUNTERS=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_DRAFT_TEMPLATES=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_GENERATE_TEMPLATES=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_ORGANIZATIONS=1
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_PROJECTS=0
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_SEQUENCES=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_SITTINGS=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_STRUCTURES=1
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_THEMES=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_USERS=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_TYPESACTS=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_TYPESITTINGS=100
LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_WORKFLOWS=100

# Configuration du connecteur Pastell pour la structure 1, organisation 1
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_URL=https://pastell.partenaire.libriciel.fr
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_USERNAME=username
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_PASSWORD=password
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_ID_E=1
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_CONNEXION_OPTION=1
LIBRICIEL_RECETTE_MASSIVE_O_00001_S_00001_PASTELL_TDT_URL=https://s2low.partenaires.libriciel.fr

# Identifiant superadmin@massive-o-00001-s-00001
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00001=password
# Identifiant admin-00001@massive-o-00001-s-00001
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_DASH_00001_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00001=password
# Identifiant adminfonc-00001@massive-o-00001-s-00001
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMINFONC_DASH_00001_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00001=password
# Identifiant valid-00001@massive-o-00001-s-00001
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_00001_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00001=password
# Identifiant secretariat-00001@massive-o-00001-s-00001
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SECRETARIAT_DASH_00001_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00001=password
# Identifiant superadmin-00001@massive-o-00001-s-00001
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_DASH_00001_AT_MASSIVE_DASH_O_DASH_00001_DASH_S_DASH_00001=password
```

#### Commande à lancer

```bash
chown -R www-data:www-data /var/lib/php/sessions \
&& bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteMassiveDataSeed \
&& bin/cake-wrapper.sh trees_recovery
```

##### Ancienne version (dépréciée)

```bash
chown -R www-data:www-data /var/lib/php/sessions \
&& bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteOldMassiveDataSeed \
&& bin/cake-wrapper.sh trees_recovery \
&& bin/cake-wrapper.sh migrations seed --plugin=LibricielRecette --seed LibricielRecetteOldWorkflowsMassiveDataSeed
```
