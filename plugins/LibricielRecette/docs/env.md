# Variables d'environnement

## Variables à ajouter au `.env`

```
#=======================================================================================================================
# Variables d'environnement pour la recette chez Libriciel SCOP
#=======================================================================================================================

# Domaine pour les adresses email
LIBRICIEL_RECETTE_DEFAULT_EMAIL_DOMAIN=mailcatchall.libriciel.net

#-----------------------------------------------------------------------------------------------------------------------
# Structure 1
#-----------------------------------------------------------------------------------------------------------------------
LIBRICIEL_RECETTE_S1_PASTELL_ID_E=6661
LIBRICIEL_RECETTE_S1_PASTELL_URL=https://pastell.partenaire.libriciel.fr
LIBRICIEL_RECETTE_S1_PASTELL_TDT_URL=https://s2low.partenaires.libriciel.fr
LIBRICIEL_RECETTE_S1_PASTELL_USERNAME=username-wa-s1
LIBRICIEL_RECETTE_S1_PASTELL_PASSWORD=password
LIBRICIEL_RECETTE_S1_PASTELL_CONNEXION_OPTION=1

LIBRICIEL_RECETTE_S1_IDELIBRE_URL=https://idelibre.formation.libriciel.fr
LIBRICIEL_RECETTE_S1_IDELIBRE_USERNAME=username-seance
LIBRICIEL_RECETTE_S1_IDELIBRE_PASSWORD=password
LIBRICIEL_RECETTE_S1_IDELIBRE_CONNEXION_OPTION=username-admin

# Nombre de projets à créer automatiquement (10 par défaut)
LIBRICIEL_RECETTE_S1_NOMBRE_PROJETS=10

#-----------------------------------------------------------------------------------------------------------------------
# Structure 2
#-----------------------------------------------------------------------------------------------------------------------
LIBRICIEL_RECETTE_S2_PASTELL_ID_E=6662
LIBRICIEL_RECETTE_S2_PASTELL_URL=https://pastell.partenaire.libriciel.fr
LIBRICIEL_RECETTE_S2_PASTELL_TDT_URL=https://s2low.partenaires.libriciel.fr
LIBRICIEL_RECETTE_S2_PASTELL_USERNAME=username-wa-s2
LIBRICIEL_RECETTE_S2_PASTELL_PASSWORD=password
LIBRICIEL_RECETTE_S2_PASTELL_CONNEXION_OPTION=1

# Nombre de projets à créer automatiquement (10 par défaut)
LIBRICIEL_RECETTE_S2_NOMBRE_PROJETS=10

#-----------------------------------------------------------------------------------------------------------------------
# Structure 3
#-----------------------------------------------------------------------------------------------------------------------
LIBRICIEL_RECETTE_S3_PASTELL_ID_E=6663
LIBRICIEL_RECETTE_S3_PASTELL_URL=https://pastell.partenaire.libriciel.fr
LIBRICIEL_RECETTE_S3_PASTELL_TDT_URL=https://s2low.partenaires.libriciel.fr
LIBRICIEL_RECETTE_S3_PASTELL_USERNAME=username-wa-s3
LIBRICIEL_RECETTE_S3_PASTELL_PASSWORD=password
LIBRICIEL_RECETTE_S3_PASTELL_CONNEXION_OPTION=1

# Nombre de projets à créer automatiquement (10 par défaut)
LIBRICIEL_RECETTE_S3_NOMBRE_PROJETS=10

#-----------------------------------------------------------------------------------------------------------------------
# Structure 4
#-----------------------------------------------------------------------------------------------------------------------
LIBRICIEL_RECETTE_S4_PASTELL_ID_E=6664
LIBRICIEL_RECETTE_S4_PASTELL_URL=https://pastell.partenaire.libriciel.fr
LIBRICIEL_RECETTE_S4_PASTELL_TDT_URL=https://s2low.partenaires.libriciel.fr
LIBRICIEL_RECETTE_S4_PASTELL_USERNAME=username-wa-s4
LIBRICIEL_RECETTE_S4_PASTELL_PASSWORD=password
LIBRICIEL_RECETTE_S4_PASTELL_CONNEXION_OPTION=1

# Nombre de projets à créer automatiquement (10 par défaut)
LIBRICIEL_RECETTE_S4_NOMBRE_PROJETS=10

#-----------------------------------------------------------------------------------------------------------------------
# Mots de passe pour les utilisateurs du plugin LibricielRecette
#-----------------------------------------------------------------------------------------------------------------------

# Structure 1
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_AT_WA_DASH_S1=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMINFONC_AT_WA_DASH_S1=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_1_AT_WA_DASH_S1=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_2_AT_WA_DASH_S1=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SECRETAIRE_AT_WA_DASH_S1=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_1_AT_WA_DASH_S1=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_2_AT_WA_DASH_S1=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_3_AT_WA_DASH_S1=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_4_AT_WA_DASH_S1=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_5_AT_WA_DASH_S1=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_6_AT_WA_DASH_S1=password

# Structure 2
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_AT_WA_DASH_S2=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMINFONC_AT_WA_DASH_S2=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_1_AT_WA_DASH_S2=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_2_AT_WA_DASH_S2=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SECRETAIRE_AT_WA_DASH_S2=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SUPERADMIN_AT_WA_DASH_S2=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_1_AT_WA_DASH_S2=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_2_AT_WA_DASH_S2=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_3_AT_WA_DASH_S2=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_4_AT_WA_DASH_S2=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_5_AT_WA_DASH_S2=password

# Structure 3
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_AT_WA_DASH_S3=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_AT_WA_DASH_S3=password

# Structure 4
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMIN_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_ADMINFONC_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_1_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_2_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_3_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_REDAC_DASH_4_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SECRETAIRE_DASH_1_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_SECRETAIRE_DASH_2_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_1_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_2_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_3_AT_WA_DASH_S4=password
LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_4_AT_WA_DASH_S4=password
```

## Liste des symboles remplacés

Pour spécifier les mots de passe des utilisateurs, il faut remplir des variables d'environnement.

Pour chaque utilisateur, la variable d'environnement sera composée de `LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_` suivi de l'identifiant
mis en majuscule et dont les caractères spéciaux sont remplacés par les chaînes de caractères ci-dessous.

Par exemple, pour l'identifiant `valid-1@wa-s1`, la variable d'environnement sera `LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_VALID_DASH_1_AT_WA_DASH_S1`.

Si la variable d'environnement permettant de définir le mot de passe d'un utilisateur n'est pas définie, alors un mot de passe aléatoire sera utilisé.

| `!`             | `"`       | `#`      | `$`        | `%`         |
|:----------------|:----------|:---------|:-----------|:------------|
| `_EXCLAMATION_` | `_QUOTE_` | `_HASH_` | `_DOLLAR_` | `_PERCENT_` |

| `&`           | `'`            | `(`                  | `)`                   | `*`          |
|:--------------|:---------------|:---------------------|:----------------------|:-------------|
| `_AMPERSAND_` | `_APOSTROPHE_` | `_LEFT_PARENTHESIS_` | `_RIGHT_PARENTHESIS_` | `_ASTERISK_` |

| `+`      | `,`       | `-`      | `.`     | `/`       |
|:---------|:----------|:---------|:--------|:----------|
| `_PLUS_` | `_COMMA_` | `_DASH_` | `_DOT_` | `_SLASH_` |

| `:`       | `;`           | `<`           | `=`        | `>`              |
|:----------|:--------------|:--------------|:-----------|:-----------------|
| `_COLON_` | `_SEMICOLON_` | `_LESS_THAN_` | `_EQUALS_` | `_GREATER_THAN_` |

| `?`          | `@`    | `[`                     | `\`           | `]`                      |
|:-------------|:-------|:------------------------|:--------------|:-------------------------|
| `_QUESTION_` | `_AT_` | `_LEFT_SQUARE_BRACKET_` | `_BACKSLASH_` | `_RIGHT_SQUARE_BRACKET_` |

| `^`            | `` ` ``   | `{`            | `\|`     | `}`             |
|:---------------|:----------|:---------------|:---------|:----------------|
| `_CIRCUMFLEX_` | `_GRAVE_` | `_LEFT_BRACE_` | `_PIPE_` | `_RIGHT_BRACE_` |

| `~`       |
|:----------|
| `_TILDE_` |

