<?php
declare(strict_types=1);

namespace LibricielRecette\Seeds;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

trait LibricielRecetteSeedWorkflowTrait
{
    /**
     * @var ?\GuzzleHttp\Client
     */
    protected $apiClient;

    /**
     * @var ?string
     */
    protected $apiAccessToken;

    /**
     * @var ?string
     */
    protected $apiTokenType;

    /**
     * @var ?array
     */
    protected $apiOptions;

    protected function getCleanedResponse(ResponseInterface $response): array
    {
        try {
            return json_decode(
                (string)$response->getBody()->getContents(),
                true,
                512,
                JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE
            );
        } catch (\Throwable $exc) {
            debug($response);
            debug((string)$response->getBody()->getContents());
            throw $exc;
        }
    }

    /**
     * Méthode utilitaire permettant d'utiliser l'API de webactes.
     * Il faudra en général s'identifier au préalable au moyen de la méthode apiConnect.
     *
     * @param string $method La méthode HTTP à utiliser (GET, POST, ...)
     * @param string $uri L'URL du endpoint d'API
     * @param array $options Les options à passer à la requête HTTP
     * @param array|int $expectedStatus Le ou les statuts HTTP attendus en cas de succès
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function apiRequest(string $method, string $uri, array $options, $expectedStatus)
    {
        $expectedStatus = (array)$expectedStatus;
        $response = $this->apiClient->request($method, $uri, $options);

        if (in_array($response->getStatusCode(), $expectedStatus, true) === false) {
            $message = sprintf(
                '%s call to %s led to a %d HTTP status code (%s expected)',
                $method,
                $uri,
                $response->getStatusCode(),
                implode(', ', $expectedStatus)
            );
            throw new RuntimeException($message);
        }

        return $response;
    }

    /**
     * Permet de se connecter à webactes pour pouvoir ensuite utiliser son API.
     * Le mot de passe est lu à partir de la variable d'environnement correspondant à l'identifiant de l'utilisateur.
     *
     * @param string $username L'identifiant de l'utilisateur
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function apiConnect(string $username)
    {
        if ($this->getInput()->getOption('connection') === 'test') {
            return;
        }

        $env = getenv();
        $key = $this->users->getUserPasswordEnvVar($username);
        if (!array_key_exists($key, $env)) {
            $message = sprintf(
                'Environment variable %s not set, for username %s',
                $key,
                $username
            );
            $this->getOutput()->writeln($message);
        }
        $password = $env[$key] ?? '';

        // Début connexion
        $this->apiClient = new Client([
            'base_uri' => 'https://reverse/',
            'cookies' => true,
            'headers' => [
                'Accept' => 'application/json',
            ],
            'verify' => false,
        ]);

        $this->apiRequest('GET', '/', [], 200);

        $response = $this->apiRequest(
            'POST',
            '/auth/realms/realmWA/protocol/openid-connect/token',
            [
                'form_params' => [
                    'client_id' => 'web-client',
                    'username' => $username,
                    'password' => $password,
                    'grant_type' => 'password',
                ],
            ],
            200
        );
        $json = $this->getCleanedResponse($response);

        $this->apiAccessToken = $json['access_token'];
        $this->apiTokenType = $json['token_type'];
        $this->apiOptions = [
            'headers' => [
                'Authorization' => $this->apiTokenType . ' ' . $this->apiAccessToken,
            ],
        ];
        $this->apiRequest('POST', '/api/v1/users/loginStructure', $this->apiOptions, 200);
    }

    /**
     * Ajout d'une définition de circuit par API.
     *
     * @param array $workflow Les étapes du circuit
     * @param bool $active Le circuit est-il actif ?
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function apiAddWorkflow(array $workflow, bool $active)
    {
        if ($this->getInput()->getOption('connection') === 'test') {
            return;
        }

        // Ajout d'un circuit
        $response = $this->apiRequest(
            'POST',
            '/api/v1/circuits',
            array_merge(
                $this->apiOptions,
                [
                    RequestOptions::JSON => $workflow,
                ]
            ),
            [200, 201]
        );

        // Activation du circuit le cas échéant
        if ($active === true) {
            $json = $this->getCleanedResponse($response);

            $this->apiRequest(
                'POST',
                sprintf('/api/v1/circuits/%d/activate', $json['circuit']['id']),
                $this->apiOptions,
                [200, 201]
            );
        }
    }

    /**
     * Récupère l'id d'un utilisateur à partir de son identifiant.
     *
     * @param string $username L'identifiant de l'utilisateur
     * @return mixed
     */
    protected function getUserId(string $username)
    {
        $sql = "SELECT id FROM users WHERE username = '" . $username . "';";
        $row = $this->fetchRow($sql);
        if (is_array($row) === false || array_key_exists('id', $row) === false) {
            $message = sprintf('Could not find user with username "%s": %s', $username, $sql);
            throw new RuntimeException($message);
        }

        return $row['id'];
    }
}
