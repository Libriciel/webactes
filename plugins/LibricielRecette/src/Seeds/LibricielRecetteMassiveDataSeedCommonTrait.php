<?php
declare(strict_types=1);

namespace LibricielRecette\Seeds;

use App\Model\Table\RolesTable;
use Cake\Utility\Inflector;
use RuntimeException;

trait LibricielRecetteMassiveDataSeedCommonTrait
{
    protected function getConfiguration(): array
    {
        $prefix = 'LIBRICIEL_RECETTE_MASSIVE_DATA_SEED_';

        $config = [
            'actors' => env($prefix . 'ACTORS', 20),
            'actor_groups' => env($prefix . 'ACTOR_GROUPS', 20),
            'counters' => env($prefix . 'COUNTERS', 20),
            'draft_templates' => env($prefix . 'DRAFT_TEMPLATES', 20),
            'generate_templates' => env($prefix . 'GENERATE_TEMPLATES', 20),
            'organizations' => env($prefix . 'ORGANIZATIONS', 20),
            'projects' => env($prefix . 'PROJECTS', 0),
            'sequences' => env($prefix . 'SEQUENCES', 20),
            'sittings' => env($prefix . 'SITTINGS', 20),
            'structures' => env($prefix . 'STRUCTURES', 20),
            'themes' => env($prefix . 'THEMES', 20),
            'users' => env($prefix . 'USERS', 20),
            'typesacts' => env($prefix . 'TYPESACTS', 20),
            'typesittings' => env($prefix . 'TYPESITTINGS', 20),
            'workflows' => env($prefix . 'WORKFLOWS', 20),
        ];

        // On s'assure que les mots de passe du super-administrateur par défaut de la structure soient bien définis
        // par des variables d'environnement
        $env = getenv();
        $missing = [];
        for ($organization = 1; $organization <= $config['organizations']; $organization++) {
            for ($structure = 1; $structure <= $config['structures']; $structure++) {
                $username = $this->getSuperadminUsername($organization, $structure);
                $key = $this->users->getUserPasswordEnvVar($username);
                if (!array_key_exists($key, $env)) {
                    $missing[] = sprintf('Environment variable %s not set, for username %s', $key, $username);
                }
            }
        }

        if (!empty($missing)) {
            foreach ($missing as $message) {
                fwrite(STDERR, "{$message}\n");
            }
            $message = sprintf(
                'Merci de renseigner les %d variables d\'environnement manquantes ci-dessus.',
                count($missing)
            );
            throw new RuntimeException($message);
        }

        return $config;
    }

    protected function getDomain(): string
    {
        return env('LIBRICIEL_RECETTE_DEFAULT_EMAIL_DOMAIN', 'mailcatchall.libriciel.net');
    }

    protected function getSuperadminUsername(int $organizationIdx, int $structureIdx): string
    {
        return sprintf('superadmin@massive-o-%05d-s-%05d', $organizationIdx, $structureIdx);
    }

    protected function getSuperadminEmail(int $organizationIdx, int $structureIdx): string
    {
        return sprintf(
            'wa-massive-o-%05d-s-%05d.superadmin@%s',
            $organizationIdx,
            $structureIdx,
            $this->getDomain()
        );
    }

    protected function getShortRoleName(string $roleName): string
    {
        switch ($roleName) {
            case RolesTable::SUPER_ADMINISTRATOR:
                return 'superadmin';
            case RolesTable::ADMINISTRATOR:
                return 'admin';
            case RolesTable::FUNCTIONNAL_ADMINISTRATOR:
                return 'adminfonc';
            case RolesTable::VALIDATOR_REDACTOR_NAME:
                return 'valid';
            case RolesTable::GENERAL_SECRETARIAT:
                return 'secretariat';
            default:
                throw new \RuntimeException(sprintf('Le rôle "%s" n\'est pas valide.', $roleName));
        }
    }

    protected function getCleanedUsername(string $string): string
    {
        // @see https://stackoverflow.com/a/11743977
        $string = strtr(
            utf8_decode($string),
            utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'),
            'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'
        );
        $string = preg_replace('/[^A-Za-z0-9]+/', '-', $string);

        return Inflector::dasherize($string);
    }

    protected function getUsername(int $organizationIdx, int $structureIdx, string $roleName, int $userIdx): string
    {
        return sprintf(
            '%s-%05d@massive-o-%05d-s-%05d',
            $this->getShortRoleName($roleName),
            $userIdx,
            $organizationIdx,
            $structureIdx
        );
    }

    protected function getUserEmail(int $organizationIdx, int $structureIdx, string $roleName, int $userIdx): string
    {
        return sprintf(
            'wa-massive-o-%05d-s-%05d.%s-%05d@%s',
            $organizationIdx,
            $structureIdx,
            $this->getShortRoleName($roleName),
            $userIdx,
            $this->getDomain()
        );
    }

    protected function getActorEmail(int $organizationIdx, int $structureIdx, int $actorIdx): string
    {
        return sprintf(
            'wa-massive-o-%05d-s-%05d.acteur-%05d@%s',
            $organizationIdx,
            $structureIdx,
            $actorIdx,
            $this->getDomain()
        );
    }
}
