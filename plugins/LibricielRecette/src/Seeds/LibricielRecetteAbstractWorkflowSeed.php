<?php
declare(strict_types=1);

namespace LibricielRecette\Seeds;

use LibricielRecette\Seeder\UsersSeeder;
use Migrations\AbstractSeed;

/**
 * Méthodes utilitaires pour les classes de données de recette concernant les circuits de validation (workflows).
 */
abstract class LibricielRecetteAbstractWorkflowSeed extends AbstractSeed
{
    use LibricielRecetteSeedWorkflowTrait;

    protected ?UsersSeeder $users;

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        parent::run();

        $connection = $this->getAdapter()->getConnection();
        $this->users = new UsersSeeder($connection);
    }
}
