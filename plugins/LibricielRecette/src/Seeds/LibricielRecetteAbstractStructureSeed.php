<?php
// phpcs:disable Generic.Files.LineLength.TooLong

declare(strict_types=1);

namespace LibricielRecette\Seeds;

use App\Utilities\Common\FilesSeederTrait;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventDispatcherTrait;
use Cake\Utility\Hash;
use LibricielRecette\Seeder\AbstractSeeder;
use LibricielRecette\Seeder\ActorGroupsSeeder;
use LibricielRecette\Seeder\ActorGroupsTypesittingsSeeder;
use LibricielRecette\Seeder\ActorsActorGroupsSeeder;
use LibricielRecette\Seeder\ActorsSeeder;
use LibricielRecette\Seeder\AnnexesSeeder;
use LibricielRecette\Seeder\ClassificationsSeeder;
use LibricielRecette\Seeder\ConnecteursSeeder;
use LibricielRecette\Seeder\ContainersSeeder;
use LibricielRecette\Seeder\ContainersStateactsSeeder;
use LibricielRecette\Seeder\CountersSeeder;
use LibricielRecette\Seeder\DposSeeder;
use LibricielRecette\Seeder\DraftTemplatesSeeder;
use LibricielRecette\Seeder\DraftTemplatesTypesactsSeeder;
use LibricielRecette\Seeder\FilesSeeder;
use LibricielRecette\Seeder\GenerateTemplatesSeeder;
use LibricielRecette\Seeder\HistoriesSeeder;
use LibricielRecette\Seeder\MaindocumentsSeeder;
use LibricielRecette\Seeder\MatieresSeeder;
use LibricielRecette\Seeder\NaturesSeeder;
use LibricielRecette\Seeder\NotificationsUsersSeeder;
use LibricielRecette\Seeder\OfficialsSeeder;
use LibricielRecette\Seeder\OrganizationsSeeder;
use LibricielRecette\Seeder\PastellfluxtypesSeeder;
use LibricielRecette\Seeder\ProjectsSeeder;
use LibricielRecette\Seeder\ProjectTextsSeeder;
use LibricielRecette\Seeder\RolesSeeder;
use LibricielRecette\Seeder\RolesUsersSeeder;
use LibricielRecette\Seeder\SequencesSeeder;
use LibricielRecette\Seeder\SittingsSeeder;
use LibricielRecette\Seeder\SittingsStatesittingsSeeder;
use LibricielRecette\Seeder\StructureSettingsSeeder;
use LibricielRecette\Seeder\StructuresSeeder;
use LibricielRecette\Seeder\StructuresUsersSeeder;
use LibricielRecette\Seeder\SystemlogsSeeder;
use LibricielRecette\Seeder\ThemesSeeder;
use LibricielRecette\Seeder\TypesactsSeeder;
use LibricielRecette\Seeder\TypesactsTypesittingsSeeder;
use LibricielRecette\Seeder\TypesittingsSeeder;
use LibricielRecette\Seeder\TypespiecesjointesSeeder;
use LibricielRecette\Seeder\UsersSeeder;
use Migrations\AbstractSeed;
use RuntimeException;

/**
 * Méthodes utilitaires pour les classes de données de recette.
 */
abstract class LibricielRecetteAbstractStructureSeed extends AbstractSeed
{
    use EventDispatcherTrait;
    use FilesSeederTrait;
    use LibricielRecetteSeedS2lowTrait;
    use LibricielRecetteSeedWorkflowTrait;

    /**
     * @var array
     */
    protected $ids = null;

    protected ?ActorGroupsSeeder $actorGroups;
    protected ?ActorsActorGroupsSeeder $actorsActorGroups;
    protected ?ActorGroupsTypesittingsSeeder $actorGroupsTypesittings;
    protected ?ActorsSeeder $actors;
    protected ?AnnexesSeeder $annexes;
    protected ?ClassificationsSeeder $classifications;
    protected ?ConnecteursSeeder $connecteurs;
    protected ?ContainersSeeder $containers;
    protected ?ContainersStateactsSeeder $containersStateacts;
    protected ?CountersSeeder $counters;
    protected ?DposSeeder $dpos;
    protected ?DraftTemplatesSeeder $draftTemplates;
    protected ?DraftTemplatesTypesactsSeeder $draftTemplatesTypesacts;
    protected ?FilesSeeder $files;
    protected ?GenerateTemplatesSeeder $generateTemplates;
    protected ?HistoriesSeeder $histories;
    protected ?MaindocumentsSeeder $maindocuments;
    protected ?MatieresSeeder $matieres;
    protected ?NaturesSeeder $natures;
    protected ?NotificationsUsersSeeder $notificationsUsers;
    protected ?PastellfluxtypesSeeder $pastellfluxtypes;
    protected ?RolesSeeder $roles;
    protected ?RolesUsersSeeder $rolesUsers;
    protected ?OfficialsSeeder $officials;
    protected ?OrganizationsSeeder $organizations;
    protected ?ProjectsSeeder $projects;
    protected ?ProjectTextsSeeder $projectTexts;
    protected ?SequencesSeeder $sequences;
    protected ?SittingsSeeder $sittings;
    protected ?SittingsStatesittingsSeeder $sittingsStatesittings;
    protected ?StructuresSeeder $structures;
    protected ?StructureSettingsSeeder $structuresSettings;
    protected ?StructuresUsersSeeder $structuresUsers;
    protected ?SystemlogsSeeder $systemlogs;
    protected ?ThemesSeeder $themes;
    protected ?TypesactsSeeder $typesacts;
    protected ?TypesactsTypesittingsSeeder $typesactsTypesittings;
    protected ?TypesittingsSeeder $typesittings;
    protected ?TypespiecesjointesSeeder $typespiecesjointes;
    protected ?UsersSeeder $users;

    protected array $records = [];

    protected function save(AbstractSeeder $seeder, array $data): void
    {
        if (!isset($this->records[$seeder->table])) {
            $this->records[$seeder->table] = [];
        }
        $insert = $seeder->insert($data, [$seeder->id, $seeder->label], true);
        $this->records[$seeder->table] = array_merge(
            $this->records[$seeder->table],
            Hash::combine(
                $insert,
                "{n}.{$seeder->label}",
                "{n}.{$seeder->id}"
            )
        );
    }

    protected function record(AbstractSeeder $seeder, mixed $label): mixed
    {
        return $this->records[$seeder->table][$label];
    }

    protected function current(AbstractSeeder $seeder): mixed
    {
        $keys = array_keys($this->records[$seeder->table]);

        return $this->records[$seeder->table][$keys[count($keys) - 1]];
    }

    protected function domain(): string
    {
        return env('LIBRICIEL_RECETTE_DEFAULT_EMAIL_DOMAIN', 'mailcatchall.libriciel.net');
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        parent::run();

        $connection = $this->getAdapter()->getConnection();

        $this->actorGroups = new ActorGroupsSeeder($connection);
        $this->actorsActorGroups = new ActorsActorGroupsSeeder($connection);
        $this->actorGroupsTypesittings = new ActorGroupsTypesittingsSeeder($connection);
        $this->actors = new ActorsSeeder($connection);
        $this->annexes = new AnnexesSeeder($connection);
        $this->classifications = new ClassificationsSeeder($connection);
        $this->connecteurs = new ConnecteursSeeder($connection);
        $this->containers = new ContainersSeeder($connection);
        $this->containersStateacts = new ContainersStateactsSeeder($connection);
        $this->counters = new CountersSeeder($connection);
        $this->dpos = new DposSeeder($connection);
        $this->draftTemplates = new DraftTemplatesSeeder($connection);
        $this->draftTemplatesTypesacts = new DraftTemplatesTypesactsSeeder($connection);
        $this->files = new FilesSeeder($connection);
        $this->generateTemplates = new GenerateTemplatesSeeder($connection);
        $this->histories = new HistoriesSeeder($connection);
        $this->maindocuments = new MaindocumentsSeeder($connection);
        $this->matieres = new MatieresSeeder($connection);
        $this->natures = new NaturesSeeder($connection);
        $this->notificationsUsers = new NotificationsUsersSeeder($connection);
        $this->officials = new OfficialsSeeder($connection);
        $this->organizations = new OrganizationsSeeder($connection);
        $this->pastellfluxtypes = new PastellfluxtypesSeeder($connection);
        $this->projects = new ProjectsSeeder($connection);
        $this->projectTexts = new ProjectTextsSeeder($connection);
        $this->roles = new RolesSeeder($connection);
        $this->rolesUsers = new RolesUsersSeeder($connection);
        $this->sequences = new SequencesSeeder($connection);
        $this->sittings = new SittingsSeeder($connection);
        $this->sittingsStatesittings = new SittingsStatesittingsSeeder($connection);
        $this->structures = new StructuresSeeder($connection);
        $this->structureSettings = new StructureSettingsSeeder($connection);
        $this->structuresUsers = new StructuresUsersSeeder($connection);
        $this->systemlogs = new SystemlogsSeeder($connection);
        $this->themes = new ThemesSeeder($connection);
        $this->typesacts = new TypesactsSeeder($connection);
        $this->typesactsTypesittings = new TypesactsTypesittingsSeeder($connection);
        $this->typesittings = new TypesittingsSeeder($connection);
        $this->typespiecesjointes = new TypespiecesjointesSeeder($connection);
        $this->users = new UsersSeeder($connection);
    }

    /**
     * Retourne la valeur de la variable d'environnement ou la valeur par défaut.
     *
     * @param string $key Le nom de la varibale d'environnement
     * @param mixed $default La valeur par défaut
     * @return mixed
     */
    protected function env(string $key, $default)
    {
        $env = getenv();

        return $env[$key] ?? $default;
    }

    /**
     * Affiche la requête complète en cas d'exception.
     *
     * @param string $sql La requête SQL
     * @param array $params Les paramètres à utiliser pour la requête
     * @return int
     * @throws \Throwable
     */
    public function execute(string $sql, array $params = [])
    {
        try {
            return parent::execute($sql);
        } catch (\Throwable $exc) {
            print_r("\n{$sql}\n");
            throw $exc;
        }
    }

    /**
     * Mise à jour des séquences postgresql vis-à-vis de la dernière valeur des clés primaires.
     *
     * @return void
     */
    protected function sequences()
    {
        $this->execute('BEGIN;');
        $sql = "SELECT table_name AS \"table\",
						column_name AS \"column\",
						column_default AS \"sequence\",
						is_identity AS \"is_identity\"
						FROM information_schema.columns
						WHERE table_schema = 'public'
							AND (column_default LIKE 'nextval(%::regclass)' OR is_identity = 'YES' )
						ORDER BY table_name, column_name";
        foreach ($this->fetchAll($sql) as $row) {
            if (!empty($row['sequence'])) {
                $sequence = preg_replace('/^nextval\(\'(.*)\'.*\)$/', '\1', $row['sequence']);
                $sql = "SELECT setval('{$sequence}', COALESCE(MAX({$row['column']}),0)+1, false) FROM {$row['table']};";
                $this->execute($sql);
            } else {
                $sql = "SELECT setval(pg_get_serial_sequence('{$row['table']}', '{$row['column']}'), (SELECT MAX({$row['column']}) FROM {$row['table']}));";
                $this->execute($sql);
            }
        }
        $this->execute('COMMIT;');
    }

    /**
     * Peuple l'attribut $sequences avec la dernière valeur de la séquence pour chacune des tables.
     *
     * @return void
     */
    protected function loadIds()
    {
        $sql = "SELECT table_name AS \"table\",
						column_name AS \"column\",
						column_default AS \"sequence\",
						is_identity AS \"is_identity\"
						FROM information_schema.columns
						WHERE table_schema = 'public'
							AND (column_default LIKE 'nextval(%::regclass)' OR is_identity = 'YES' )
						ORDER BY table_name, column_name";
        $results = [];

        foreach ($this->fetchAll($sql) as $row) {
            $possible = [0];
            if (!empty($row['sequence'])) {
                //@info: connecteurs, groups_users, notifications_users
                $sequence = preg_replace('/^nextval\(\'(.*)\'.*\)$/', '\1', $row['sequence']);
                $possible[] = $this->fetchRow("SELECT last_value FROM {$sequence};")['last_value'] - 1;
            } elseif ($row['is_identity'] === 'YES') {
                $serial = $this->fetchRow("SELECT pg_get_serial_sequence('{$row['table']}', '{$row['column']}')");
                $possible[] = $this->fetchRow("SELECT last_value FROM {$serial['pg_get_serial_sequence']};")['last_value'] - 1;
            }
            $possible[] = $this->fetchRow("SELECT MAX({$row['column']}) FROM {$row['table']};")['max'] ?? 0;
            $results[$row['table']] = max($possible);
        }

        $this->ids = $results;
    }

    /**
     * Retourne l'id relatif par-rapport à la dernière valeur de la séquence ou de la clé primare, ce qui permet
     * d'utiliser des id relatifs au sein de chaque seed.
     *
     * @param string $table Le nom de la table
     * @param int $offset Le décalage des ids
     * @return int
     */
    public function id(string $table, int $offset)
    {
        if ($this->ids === null) {
            $this->loadIds();
        }

        return $this->ids[$table] + $offset;
    }

    /**
     * Remet les bonnes permissions sur le dossier WORKSPACE (/data/workspace) et ses sous-dossiers.
     *
     * @return void
     */
    protected function permissions()
    {
        $cmd = sprintf('chown -R www-data:www-data %s;', WORKSPACE);
        exec($cmd);
    }

    /**
     * Crée un fichier de projet à partir d'un fichier original.
     *
     * Exemple: projet 1: /data/workspace/1/3/2023/1/<f873d23b-3d4e-4bcb-8d80-45d4b3bd26f4>
     *
     * @param string $pathFile Le chemin vers le fichier original
     * @param int $organizationId L'id de l'organization
     * @param int $structureId L'id de la structure
     * @param int $year L'année sur 4 chiffres
     * @param int $id L'identifiant du projet
     * @return \SplFileInfo
     */
    protected function createProjectFile(
        string $pathFile,
        int $organizationId,
        int $structureId,
        int $year,
        int $id
    ): \SplFileInfo {
        $file = $this->putFilePath(
            $pathFile,
            WORKSPACE . DS . $organizationId . DS . $structureId . DS . $year . DS . $id . DS
        );

        return $this->fileInfo($file);
    }

    /**
     * Crée un fichier d'annexe d'un projet à partir d'un fichier original.
     *
     * Exemple: annexe du projet 1: /data/workspace/1/3/2023/1/<f873d23b-3d4e-4bcb-8d80-45d4b3bd26f4>
     *
     * @param string $pathFile Le chemin vers le fichier original
     * @param int $organizationId L'id de l'organization
     * @param int $structureId L'id de la structure
     * @param int $year L'année sur 4 chiffres
     * @param int $id L'identifiant du projet
     * @return \SplFileInfo
     */
    protected function createProjectAnnexeFile(
        string $pathFile,
        int $organizationId,
        int $structureId,
        int $year,
        int $id
    ): \SplFileInfo {
        $file = $this->putFilePath(
            $pathFile,
            WORKSPACE . DS . $organizationId . DS . $structureId . DS . $year . DS . $id . DS
        );

        return $this->fileInfo($file);
    }

    /**
     * Crée un fichier de gabarit d'un projet à partir d'un fichier original.
     *
     * gabarits du projet 1: /data/workspace/1/3/2023/project_text_id/1/<2539b0f3-23ce-4b8d-a108-305c4152ac59>
     *
     * @param string $pathFile Le chemin vers le fichier original
     * @param int $organizationId L'id de l'organization
     * @param int $structureId L'id de la structure
     * @param int $year L'année sur 4 chiffres
     * @param int $id L'identifiant du projet
     * @return \SplFileInfo
     */
    protected function createProjectGabaritFile(
        string $pathFile,
        int $organizationId,
        int $structureId,
        int $year,
        int $id
    ): \SplFileInfo {
        $file = $this->putFilePath(
            $pathFile,
            WORKSPACE . DS . $organizationId . DS . $structureId . DS . $year . DS . 'project_text_id' . DS . $id . DS
        );

        return $this->fileInfo($file);
    }

    /**
     * Création du projet sur Pastell.
     *
     * @param int $organizationId L'id de l'organization
     * @param int $structureId L'id de la structure
     * @param int $projectId L'id du projet
     * @param int $containerId L'id du container du projet
     * @return void
     */
    protected function prepareDocumentOnPastell(int $organizationId, int $structureId, int $projectId, int $containerId)
    {
        if ($containerId !== null) {
            $this->getEventManager()->dispatch(
                new Event('Model.Project.afterSave.generateAct', $this, ['container_id' => $containerId])
            );
        }

        $event = new Event(
            'Model.Project.afterSaveState',
            $this,
            [
                'id_d' => $projectId,
                'flux' => Configure::read('Flux.actes-generique.name'),
                'flux_name' => Configure::read('Flux.actes-generique.name'),
                'structureId' => $structureId,
                'organizationId' => $organizationId,
            ]
        );
        $this->getEventManager()->dispatch($event);
    }

    /**
     * Lance la conversion d'annexes.
     *
     * @param array $ids Les identifiants des annexes à convertir
     * @return void
     */
    protected function convertFileAnnex(array $ids)
    {
        $this->getEventManager()->dispatch(
            new Event('Model.Annexe.afterSave.convertFile', $this, ['ids' => $ids])
        );
    }

    /**
     * Raccourci à l'utilisation de convertFileAnnex et prepareDocumentOnPastell
     *
     * @param array $ids Les id des projets
     * @return void
     */
    protected function initializeProjects(array $ids)
    {
        $sqlProjects = 'SELECT
                            structures.organization_id,
                            projects.structure_id,
                            projects.id
                            FROM projects
                            INNER JOIN structures ON (projects.structure_id = structures.id)
                            WHERE projects.id IN (' . implode(', ', $ids) . ');';
        foreach ($this->fetchAll($sqlProjects) as $project) {
            $sqlContainers = 'SELECT id FROM containers WHERE project_id = ' . $project['id'] . ';';
            foreach ($this->fetchAll($sqlContainers) as $container) {
                $sqlAnnexes = 'SELECT id FROM annexes WHERE container_id = ' . $container['id'] . ';';
                $annexesIds = [];
                foreach ($this->fetchAll($sqlAnnexes) as $annexe) {
                    $annexesIds[] = $annexe['id'];
                }
                if (!empty($annexesIds)) {
                    $this->convertFileAnnex($annexesIds);
                }
                $this->prepareDocumentOnPastell($project['organization_id'], $project['structure_id'], $project['id'], $container['id']);
            }
        }
    }

    /**
     * Création d'un projet d'acte lorsque la génération est activée.
     *
     * @param int $organizationId L'id de l'organization
     * @param int $structureId L'id de la structure
     * @param int $userId L'id de l'utilisateur créateur
     * @param int $themeId L'id du thème
     * @param int $typeactId L'id du type d'acte
     * @param int $draftTemplateActeId L'identifiant du texte de projet
     * @param int $draftTemplateProjectId L'identifiant du texte d'acte
     * @return void
     * @throws \Throwable
     */
    protected function addProjectWithGeneration(
        int $organizationId,
        int $structureId,
        int $userId,
        int $themeId,
        int $typeactId,
        int $draftTemplateActeId,
        int $draftTemplateProjectId
    ): void {
        $this->sequences();

        $this->loadIds();

        $this->execute('BEGIN;');

        $tables = ['annexes', 'containers', 'containers_stateacts', 'files', 'histories', 'maindocuments', 'project_texts', 'projects', 'systemlogs'];
        $sql = sprintf('LOCK TABLE "%s" IN ACCESS EXCLUSIVE MODE;', implode('", "', $tables));
        $this->execute($sql);

        $sql = "SELECT business_name FROM structures WHERE id = {$structureId}";
        $structure = preg_replace('/^.* ([^ ]+)$/', '\1', $this->fetchAll($sql)[0]['business_name']);

        $sql = "SELECT keycloak_id FROM users WHERE id = {$userId}";
        $keycloakId = $this->fetchAll($sql)[0]['keycloak_id'];

        $sql = "SELECT COUNT(*) AS \"count\" FROM projects WHERE structure_id = {$structureId};";
        $projectCounter = sprintf('%03d', $this->fetchAll($sql)[0]['count'] + 1);

        $dirs = [
            'Annexes' => dirname(__DIR__, 2) . DS . 'files' . DS . 'Annexes' . DS,
            'GenerateTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesGenerateTemplates' . DS,
            'DraftTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesDraftTemplates' . DS,
        ];

        $records = [];
        // Projects
        $data = [
            [
                'structure_id' => $structureId,
                'theme_id' => $themeId,
                'name' => "{$structure} Délibération {$projectCounter}",
                'codematiere' => '1.4',
            ],
        ];
        $records['projects'] = $this->projects->insert($data, ['id']);

        // Containers
        $data = [
            [
                'structure_id' => $structureId,
                'project_id' => $records['projects'][0]['id'],
                'typesact_id' => $typeactId,
            ],
        ];
        $records['containers'] = $this->containers->insert($data, ['id']);

        // Annexes
        $data = [
            [
                'structure_id' => $structureId,
                'container_id' => $records['containers'][0]['id'],
                'rank' => 1,
                'codetype' => '21_DA',
                'is_generate' => true,
            ],
            [
                'structure_id' => $structureId,
                'container_id' => $records['containers'][0]['id'],
                'rank' => 2,
                'codetype' => '21_DB',
                'is_generate' => true,
            ],
            [
                'structure_id' => $structureId,
                'container_id' => $records['containers'][0]['id'],
                'rank' => 3,
                'codetype' => '21_EP',
                'is_generate' => true,
            ],
        ];
        $records['annexes'] = $this->annexes->insert($data, ['id']);

        // ContainersStateacts
        $data = [
            [
                'container_id' => $records['containers'][0]['id'],
                'stateact_id' => 1,
            ],
        ];
        $records['containers_stateacts'] = $this->containersStateacts->insert($data, ['id']);

        // Maindocuments
        $data = [
            [
                'structure_id' => $structureId,
                'container_id' => $records['containers'][0]['id'],
                'name' => "{$structure} Délibération {$projectCounter}",
                'codetype' => '99_DE',
            ],
        ];
        $records['maindocuments'] = $this->maindocuments->insert($data, ['id']);

        // ProjectTexts
        $data = [
            [
                'structure_id' => $structureId,
                'project_id' => $records['projects'][0]['id'],
                'draft_template_id' => $draftTemplateActeId,
            ],
            [
                'structure_id' => $structureId,
                'project_id' => $records['projects'][0]['id'],
                'draft_template_id' => $draftTemplateProjectId,
            ],
        ];
        $records['project_texts'] = $this->projectTexts->insert($data, ['id']);

        // Files
        $annexes = [
            '1' => $this->createProjectAnnexeFile($dirs['Annexes'] . 'annexe1.pdf', $organizationId, $structureId, (int)date('Y'), $records['annexes'][0]['id']),
            '2' => $this->createProjectAnnexeFile($dirs['Annexes'] . 'annexe2.pdf', $organizationId, $structureId, (int)date('Y'), $records['annexes'][1]['id']),
            '3' => $this->createProjectAnnexeFile($dirs['Annexes'] . 'annexe3.pdf', $organizationId, $structureId, (int)date('Y'), $records['annexes'][2]['id']),
        ];
        $gabarits = [
            'texte_delib2' => $this->createProjectGabaritFile($dirs['DraftTemplates'] . 'texte_delib2.odt', $organizationId, $structureId, (int)date('Y'), $records['project_texts'][0]['id']),
            'texte_projet' => $this->createProjectGabaritFile($dirs['DraftTemplates'] . 'texte_projet.odt', $organizationId, $structureId, (int)date('Y'), $records['project_texts'][1]['id']),
        ];
        $data = [
            // Annexes
            [
                'structure_id' => $structureId,
                'annex_id' => $records['annexes'][0]['id'],
                'name' => 'annexe1.pdf',
                'path' => $annexes['1']->getRealPath(),
                'mimetype' => 'application/pdf',
                'size' => $annexes['1']->getSize(),
            ],
            [
                'structure_id' => $structureId,
                'annex_id' => $records['annexes'][1]['id'],
                'name' => 'annexe2.pdf',
                'path' => $annexes['2']->getRealPath(),
                'mimetype' => 'application/pdf',
                'size' => $annexes['2']->getSize(),
            ],
            [
                'structure_id' => $structureId,
                'annex_id' => $records['annexes'][2]['id'],
                'name' => 'annexe3.pdf',
                'path' => $annexes['3']->getRealPath(),
                'mimetype' => 'application/pdf',
                'size' => $annexes['2']->getSize(),
            ],
            // ProjectTexts
            [
                'structure_id' => $structureId,
                'project_text_id' => $records['project_texts'][0]['id'],
                'name' => 'texte_delib2.odt',
                'path' => $gabarits['texte_delib2']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $gabarits['texte_delib2']->getSize(),
            ],
            [
                'structure_id' => $structureId,
                'project_text_id' => $records['project_texts'][1]['id'],
                'name' => 'texte_projet.odt',
                'path' => $gabarits['texte_projet']->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $gabarits['texte_projet']->getSize(),
            ],
        ];
        $records['files'] = $this->files->insert($data, ['id']);

        // Histories
        $data = [
            [
                'structure_id' => $structureId,
                'container_id' => $records['containers'][0]['id'],
                'user_id' => $userId,
                'comment' => 'Création de projet',
            ],
        ];
        $records['histories'] = $this->histories->insert($data, ['id']);

        // Systemlogs
        $data = [
            [
                'structure_id' => $structureId,
                'keycloak_id' => $keycloakId,
                'controller' => 'Projects',
                'action' => 'add',
                'pass' => 'a:0:{}',
                'code' => 201,
                'message' => "Ajout du projet \"{$structure} Délibération {$projectCounter}\" ({$records['projects'][0]['id']}) par \"{$keycloakId}\" ({$userId})",
                'extra' => "a:1:{i:0;i:{$records['projects'][0]['id']};}",
            ],
        ];
        $records['systemlogs'] = $this->systemlogs->insert($data, ['id']);

        $this->execute('COMMIT;');

        $this->sequences();
        $this->permissions();

        $this->initializeProjects([$records['projects'][0]['id']]);

        $this->sequences();
        $this->permissions();
    }

    /**
     * Création d'un projet d'acte lorsque la génération n'est pas activée + Numéro de l'acte (projects.code_act)
     *
     * @param int $organizationId L'id de l'organization
     * @param int $structureId L'id de la structure
     * @param int $userId L'id de l'utilisateur créateur
     * @param int $themeId L'id du thème
     * @param int $typeactId L'id du type d'acte
     * @param string $codeAct
     * @return void
     * @throws \Throwable
     */
    protected function addProjectWithoutGeneration(
        int $organizationId,
        int $structureId,
        int $userId,
        ?int $themeId,
        int $typeactId,
        string $codeAct,
        string $projectFilePath
    ): void {
        $this->sequences();

        $this->execute('BEGIN;');

        $tables = ['annexes', 'containers', 'containers_stateacts', 'files', 'histories', 'maindocuments', 'project_texts', 'projects', 'systemlogs'];
        $sql = sprintf('LOCK TABLE "%s" IN ACCESS EXCLUSIVE MODE;', implode('", "', $tables));
        $this->execute($sql);

        $sql = "SELECT business_name FROM structures WHERE id = {$structureId}";
        $structure = preg_replace('/^.* ([^ ]+)$/', '\1', $this->fetchAll($sql)[0]['business_name']);

        $sql = "SELECT keycloak_id FROM users WHERE id = {$userId}";
        $keycloakId = $this->fetchAll($sql)[0]['keycloak_id'];

        $sql = "SELECT COUNT(*) AS \"count\" FROM projects WHERE structure_id = {$structureId};";
        $projectCounter = sprintf('%03d', $this->fetchAll($sql)[0]['count'] + 1);

        $dirs = [
            'Annexes' => dirname(__DIR__, 2) . DS . 'files' . DS . 'Annexes' . DS,
        ];
        $basename = basename($projectFilePath);
        $mimetype = mime_content_type($projectFilePath);

        $records = [];
        // Projects
        $data = [
            [
                'structure_id' => $structureId,
                'theme_id' => $themeId,
                'name' => "{$structure} Délibération {$projectCounter}",
                'code_act' => $codeAct,
                'codematiere' => '1.4',
            ],
        ];
        $records['projects'] = $this->projects->insert($data, ['id']);

        // Containers
        $data = [
            [
                'structure_id' => $structureId,
                'project_id' => $records['projects'][0]['id'],
                'typesact_id' => $typeactId,
            ],
        ];
        $records['containers'] = $this->containers->insert($data, ['id']);

        // Annexes
        $data = [
            [
                'structure_id' => $structureId,
                'container_id' => $records['containers'][0]['id'],
                'rank' => 1,
                'codetype' => '21_DA',
                'is_generate' => true,
            ],
            [
                'structure_id' => $structureId,
                'container_id' => $records['containers'][0]['id'],
                'rank' => 2,
                'codetype' => '21_DB',
                'is_generate' => true,
            ],
            [
                'structure_id' => $structureId,
                'container_id' => $records['containers'][0]['id'],
                'rank' => 3,
                'codetype' => '21_EP',
                'is_generate' => true,
            ],
        ];
        $records['annexes'] = $this->annexes->insert($data, ['id']);

        // ContainersStateacts
        $data = [
            [
                'container_id' => $records['containers'][0]['id'],
                'stateact_id' => 1,
            ],
        ];
        $records['containers_stateacts'] = $this->containersStateacts->insert($data, ['id']);

        // Maindocuments
        $data = [
            [
                'structure_id' => $structureId,
                'container_id' => $records['containers'][0]['id'],
                'name' => "{$structure} Délibération {$projectCounter}",
                'codetype' => '99_DE',
            ],
        ];
        $records['maindocuments'] = $this->maindocuments->insert($data, ['id']);

        // Files
        $project = $this->createProjectFile($projectFilePath, $organizationId, $structureId, (int)date('Y'), $this->id('projects', 1));
        $annexes = [
            '1' => $this->createProjectAnnexeFile($dirs['Annexes'] . 'annexe1.pdf', $organizationId, $structureId, (int)date('Y'), $this->id('annexes', 1)),
            '2' => $this->createProjectAnnexeFile($dirs['Annexes'] . 'annexe2.pdf', $organizationId, $structureId, (int)date('Y'), $this->id('annexes', 2)),
            '3' => $this->createProjectAnnexeFile($dirs['Annexes'] . 'annexe3.pdf', $organizationId, $structureId, (int)date('Y'), $this->id('annexes', 3)),
        ];
        $data = [
            // Maindocumens
            [
                'structure_id' => $structureId,
                'maindocument_id' => $records['maindocuments'][0]['id'],
                'name' => $basename,
                'path' => $project->getRealPath(),
                'mimetype' => $mimetype,
                'size' => $project->getSize(),
            ],
            // Annexes
            [
                'structure_id' => $structureId,
                'annex_id' => $records['annexes'][0]['id'],
                'name' => 'annexe1.pdf',
                'path' => $annexes['1']->getRealPath(),
                'mimetype' => 'application/pdf',
                'size' => $annexes['1']->getSize(),
            ],
            [
                'structure_id' => $structureId,
                'annex_id' => $records['annexes'][1]['id'],
                'name' => 'annexe2.pdf',
                'path' => $annexes['2']->getRealPath(),
                'mimetype' => 'application/pdf',
                'size' => $annexes['2']->getSize(),
            ],
            [
                'structure_id' => $structureId,
                'annex_id' => $records['annexes'][2]['id'],
                'name' => 'annexe3.pdf',
                'path' => $annexes['3']->getRealPath(),
                'mimetype' => 'application/pdf',
                'size' => $annexes['2']->getSize(),
            ],
        ];
        $records['files'] = $this->files->insert($data, ['id']);

        // Histories
        $data = [
            [
                'structure_id' => $structureId,
                'container_id' => $records['containers'][0]['id'],
                'user_id' => $userId,
                'comment' => 'Création de projet',
            ],
        ];
        $records['histories'] = $this->histories->insert($data, ['id']);

        // Systemlogs
        $data = [
            [
                'structure_id' => $structureId,
                'keycloak_id' => $keycloakId,
                'controller' => 'Projects',
                'action' => 'add',
                'pass' => 'a:0:{}',
                'code' => 201,
                'message' => "Ajout du projet \"{$structure} Délibération {$projectCounter}\" ({$records['projects'][0]['id']}) par \"{$keycloakId}\" ({$userId})",
                'extra' => "a:1:{i:0;i:{$records['projects'][0]['id']};}",
            ],
        ];
        $records['systemlogs'] = $this->systemlogs->insert($data, ['id']);

        $this->execute('COMMIT;');

        $this->sequences();
        $this->permissions();

        $this->initializeProjects([$records['projects'][0]['id']]);

        $this->sequences();
        $this->permissions();
    }

    /**
     * Récupère l'id d'une organization (structures.organization_id) à partir de l'identifiant de la stucture
     * (structures.id).
     *
     * @param int $structureId L'identifiant de la structure
     * @return mixed
     */
    protected function getOrganizationId(int $structureId)
    {
        $sql = 'SELECT organization_id FROM structures WHERE id = ' . $structureId . ';';
        $row = $this->fetchRow($sql);
        if (is_array($row) === false || array_key_exists('organization_id', $row) === false) {
            $message = sprintf('Could not find structure with id "%d": %s', $structureId, $sql);
            throw new RuntimeException($message);
        }

        return $row['organization_id'];
    }
}
