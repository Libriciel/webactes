<?php
// phpcs:disable Generic.Files.LineLength.TooLong

declare(strict_types=1);

namespace LibricielRecette\Seeds;

use InvalidArgumentException;
use LibricielRecette\Model\Enum\GenerateTemplateType;

trait LibricielRecetteSeedGenerateTemplatesTrait
{
    /**
     * Paramétrage d'un modèle de document
     *
     * @param ?int $structureId L'identifiant de la structure (par défaut: $structureId)
     * @param int $generateTemplateTypeId La valeur de generate_template_type_id (voir LibricielRecette\Model\Enum\GenerateTemplateType)
     * @param string|null $name
     * @return int
     */
    protected function setupGenerateTemplate(?int $structureId, int $generateTemplateTypeId, ?string $name = null): int
    {
        $structureId ??= $this->id('structures', 1);

        if ($name === null) {
            if ($generateTemplateTypeId === GenerateTemplateType::PROJECT) {
                $name = 'Modèle de projet';
            } elseif ($generateTemplateTypeId === GenerateTemplateType::ACT) {
                $name = 'Modèle d\'acte';
            } elseif ($generateTemplateTypeId === GenerateTemplateType::CONVOCATION) {
                $name = 'Modèle de convocation';
            } elseif ($generateTemplateTypeId === GenerateTemplateType::EXECUTIVE_SUMMARY) {
                $name = 'Modèle de note de synthèse';
            } elseif ($generateTemplateTypeId === GenerateTemplateType::DELIBERATIONS_LIST) {
                $name = 'Modèle de liste des délibérations';
            } elseif ($generateTemplateTypeId === GenerateTemplateType::VERBAL_TRIAL) {
                $name = 'Modèle de procès-verbal';
            } else {
                $message = sprintf('The $generateTemplateTypeId "%d" is unknown.', $generateTemplateTypeId);
                throw new InvalidArgumentException($message);
            }
        }

        $data = [
            [
                'structure_id' => $structureId,
                'generate_template_type_id' => $generateTemplateTypeId,
                'name' => $name,
            ],
         ];
        $rows = $this->generateTemplates->insert($data, ['id']);

        return $rows[0]['id'];
    }

    public function setupGenerateTemplateFile(?int $structureId, int $generateTemplateId, string $filename, string $name): int
    {
        $structureId ??= $this->id('structures', 1);
        $organizationId = $this->getOrganizationId($structureId);

        $file = $this->createFile(
            $filename,
            'generate_template_id',
            $organizationId,
            $structureId,
            $generateTemplateId
        );

        $data = [
            [
                'structure_id' => $structureId,
                'name' => $name,
                'path' => $file->getRealPath(),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $file->getSize(),
                'generate_template_id' => $generateTemplateId,
            ],
        ];
        $rows = $this->files->insert($data, ['id']);

        return $rows[0]['id'];
    }
}
