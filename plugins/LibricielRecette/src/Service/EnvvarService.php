<?php
declare(strict_types=1);

namespace LibricielRecette\Service;

use InvalidArgumentException;

class EnvvarService
{
    /**
     * La liste des remplacements pour les symboles
     *
     * @see static::$symbols
     * @var string[]
     */
    protected static $replacements = [
        '_EXCLAMATION_',
        '_QUOTE_',
        '_HASH_',
        '_DOLLAR_',
        '_PERCENT_',
        '_AMPERSAND_',
        '_APOSTROPHE_',
        '_LEFT_PARENTHESIS_',
        '_RIGHT_PARENTHESIS_',
        '_ASTERISK_',
        '_PLUS_',
        '_COMMA_',
        '_DASH_',
        '_DOT_',
        '_SLASH_',
        '_COLON_',
        '_SEMICOLON_',
        '_LESS_THAN_',
        '_EQUALS_',
        '_GREATER_THAN_',
        '_QUESTION_',
        '_AT_',
        '_LEFT_SQUARE_BRACKET_',
        '_BACKSLASH_',
        '_RIGHT_SQUARE_BRACKET_',
        '_CIRCUMFLEX_',
        '_GRAVE_',
        '_LEFT_BRACE_',
        '_PIPE_',
        '_RIGHT_BRACE_',
        '_TILDE_',
    ];

    /**
     * La liste des symboles à remplacer
     *
     * @see static::$replacements
     * @var string[]
     */
    protected static $symbols = [
        '!',
        '"',
        '#',
        '$',
        '%',
        '&',
        '\'',
        '(',
        ')',
        '*',
        '+',
        ',',
        '-',
        '.',
        '/',
        ':',
        ';',
        '<',
        '=',
        '>',
        '?',
        '@',
        '[',
        '\\',
        ']',
        '^',
        '`',
        '{',
        '|',
        '}',
        '~',
    ];

    /**
     * Remplace, par leur équivalent textuel, dans une chaîne de caractères ASCII les caractères qui ne peuvent pas
     * figurer dans le nom d'une variable d'environnement.
     *
     * Les caractères en minuscule seront transformés en majuscules.
     * Les symboles seront remplacés par leur "nom", entouré d'underscores.
     *
     * Par exemple, "" retournera "".
     *
     * @see static::$replacements, static::$symbols
     * @param string $string La chaîne de caractères à transformer
     * @return string
     */
    public static function escapeVarname(string $string): string
    {
        if (!mb_detect_encoding($string, 'ASCII', true)) {
            $message = sprintf('The string "%s" contains characters that are not ASCII.', $string);
            throw new InvalidArgumentException($message);
        }

        return strtoupper(str_replace(static::$symbols, static::$replacements, $string));
    }

    /**
     * Retourne le code markdown décrivant les remplacements effectués.
     *
     * @param int $max Le nombre maximum de colonnes à afficher par ligne
     * @return string
     */
    public static function replacementsToMarkdown(int $max = 5): string
    {
        $result = '';
        $lines = [];
        foreach (static::$symbols as $idx => $symbol) {
            $line = (ceil(($idx + 1) / $max)) * 2;
            if ($symbol === '|') {
                $symbol = '\\|';
            } elseif ($symbol === '`') {
                $symbol = '` ` `';
            }
            $lines[$line][] = $symbol;
            $lines[$line + 1][] = static::$replacements[$idx];
        }

        foreach ($lines as $idx => $line) {
            $result .= '| `' . implode('` | `', $line) . "` |\n";
            if ($idx % 2 === 0) {
                $result .= '| ' . implode(' | ', array_fill(0, count($line), ':---')) . " |\n";
            } else {
                $result .= "\n";
            }
        }

        return $result;
    }
}
