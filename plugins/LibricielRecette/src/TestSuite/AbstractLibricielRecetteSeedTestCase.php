<?php
declare(strict_types=1);

namespace LibricielRecette\TestSuite;

use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\Utility\Hash;

class AbstractLibricielRecetteSeedTestCase extends TestCase
{
    use ConsoleIntegrationTestTrait;

    public $fixtures = [
        'app.ConnectorTypes',
        'app.Notifications',
        'app.Stateacts',
        'app.Statesittings',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->connection = $this->fetchTable('Users')->getConnection();
        $this->useCommandRunner();
        $this->cleanup();
    }

    protected function cleanup(): void
    {
        $sql = 'SELECT table_name
                    FROM information_schema.tables
                    WHERE
                        table_schema=\'public\'
                        AND table_name NOT LIKE \'%_phinxlog\'
                        AND table_name NOT IN (\'connector_types\', \'notifications\', \'stateacts\', \'statesittings\')
                    ORDER BY table_name;';
        $tables = Hash::extract($this->connection->execute($sql)->fetchAll(\PDO::FETCH_ASSOC), '{n}.table_name');
        $this->connection->execute('TRUNCATE ' . implode(', ', $tables) . ' RESTART IDENTITY;');
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->cleanup();
    }

    protected function getTablesCount(array $tables = []): array
    {
        if (empty($tables)) {
            $conditions = "AND TABLE_NAME not like 'pg_%'
                           AND TABLE_NAME not like '%phinxlog%'
                           AND TABLE_NAME not like 'beanstalk_%'";
        } else {
            $conditions = 'AND TABLE_NAME in (\'' . implode("', '", $tables) . '\')';
        }
        // @see https://stackoverflow.com/a/2611745
        $sql = <<<SQL
WITH table_count AS (
    SELECT table_schema, TABLE_NAME
    FROM information_schema.tables
    WHERE
        table_schema in ('public')
        {$conditions}
)
SELECT TABLE_NAME,
       (
           xpath(
            '/row/c/text()',
            query_to_xml(format('select count(*) as c from %I.%I', table_schema, TABLE_NAME), FALSE, TRUE, '')
            )
       )[1]::text::int AS rows_count
FROM table_count
ORDER BY TABLE_NAME ASC;
SQL;
        $rows = $this->connection->execute($sql)->fetchAll(\PDO::FETCH_ASSOC);

        return Hash::combine($rows, '{n}.table_name', '{n}.rows_count');
    }
}
