<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use DateTime;
use PDO;

/**
 * ProjectTexts
 */
class ProjectTextsSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'project_texts';
        $this->id = 'id';
        $this->label = 'id';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'structure_id' => null,
            'project_id' => null,
            'draft_template_id' => null,
            'created' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'modified' => (new DateTime())->format('Y-m-d H:i:s.u'),
        ];
    }
}
