<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use DateTime;
use PDO;

/**
 * Files
 */
class FilesSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'files';
        $this->id = 'id';
        $this->label = 'name';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'structure_id' => null,
            'maindocument_id' => null,
            'annex_id' => null,
            'name' => null,
            'path' => null,
            'mimetype' => null,
            'size' => null,
            'generate_template_id' => null,
            'draft_template_id' => null,
            'created' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'modified' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'project_text_id' => null,
            'attachment_summon_id' => null,
        ];
    }
}
