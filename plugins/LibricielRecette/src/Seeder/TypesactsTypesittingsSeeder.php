<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use PDO;

/**
 * TypesactsTypesittings
 */
class TypesactsTypesittingsSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'typesacts_typesittings';
        $this->id = 'id';
        $this->label = 'id';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'typesact_id' => null,
            'typesitting_id' => null,
        ];
    }
}
