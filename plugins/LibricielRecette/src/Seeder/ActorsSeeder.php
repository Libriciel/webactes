<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use DateTime;
use PDO;

/**
 * Actors
 */
class ActorsSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'actors';
        $this->id = 'id';
        $this->label = 'email';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'structure_id' => null,
            'active' => true,
            'civility' => null,
            'lastname' => null,
            'firstname' => null,
            'birthday' => null,
            'email' => null,
            'title' => null,
            'address' => null,
            'address_supplement' => null,
            'post_code' => null,
            'city' => null,
            'phone' => null,
            'cellphone' => null,
            'rank' => null,
            'deleted' => null,
            'created' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'modified' => (new DateTime())->format('Y-m-d H:i:s.u'),
        ];
    }
}
