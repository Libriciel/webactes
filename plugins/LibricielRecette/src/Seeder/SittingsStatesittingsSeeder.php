<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use PDO;

/**
 * SittingsStatesittings
 *
 * @info: statesitting_id: 1=ouverture_seance, 2=seance_close
 */
class SittingsStatesittingsSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'sittings_statesittings';
        $this->id = 'id';
        $this->label = 'id';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'sitting_id' => null,
            'statesitting_id' => null,
        ];
    }
}
