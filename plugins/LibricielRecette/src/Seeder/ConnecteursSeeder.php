<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use PDO;

/**
 * Connecteurs
 *
 * @info: connector_type_id: 1=pastell, 2=idelibre
 */
class ConnecteursSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'connecteurs';
        $this->id = 'id';
        $this->label = 'name';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'name' => null,
            'url' => null,
            'tdt_url' => null,
            'username' => null,
            'password' => null,
            'structure_id' => null,
            'connector_type_id' => null,
            'connexion_option' => null,
        ];
    }
}
