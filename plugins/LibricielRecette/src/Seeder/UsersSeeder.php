<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use App\Model\Entity\Auth;
use Cake\ORM\TableRegistry;
use DateTime;
use Libriciel\Password\Service\PasswordGeneratorAnssi;
use LibricielRecette\Service\EnvvarService;
use LibricielRecette\Utility\ReflectionUtility;
use PDO;
use RuntimeException;

class UsersSeeder extends AbstractSeeder
{
    /**
     * @var \Libriciel\Password\Service\PasswordGeneratorAnssi
     */
    protected $generator;

    /**
     * @var \App\Model\Entity\Auth
     */
    protected ?Auth $keycloakAuth = null;

    /**
     * @inheritDoc
     */
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    /**
     * Est-on en train d'exécuter les tests unitaires ?
     *
     * @var bool
     */
    public readonly bool $inTests;

    /**
     * Constructeur.
     *
     * @param \PDO $connection La connexion PDO à utiliser
     */
    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $config = ReflectionUtility::getProtected(TableRegistry::get('Users')->getConnection(), '_config');
        $this->inTests = $config['name'] === 'test';

        $this->generator = new PasswordGeneratorAnssi();
        $this->table = 'users';
        $this->id = 'id';
        $this->label = 'username';
    }

    /**
     * Récupération de l'authentification pour keycloak (création le cas échéant).
     *
     * @return \App\Model\Entity\Auth
     */
    protected function getKeycloakAuth(): Auth
    {
        if ($this->keycloakAuth === null) {
            $auth = new Auth();
            $token = $auth->getToken(
                env('KEYCLOAK_USER', 'adminwa'),
                env('KEYCLOAK_PASSWORD', 'password')
            );
            $auth->setToken($token);
            $this->keycloakAuth = $auth;
        }

        return $this->keycloakAuth;
    }

    /**
     * Retourne le nom de la variable d'environnement permettant de spécifier le mot de passe d'un utilisateur.
     *
     * @param string $username L'identifiant de l'utilisateur
     * @return string
     */
    public function getUserPasswordEnvVar(string $username): string
    {
        return 'LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_' . EnvvarService::escapeVarname($username);
    }

    /**
     * Création d'un utilisateur sur keycloak via l'API.
     *
     * @param array $record Les données de l'utilisateur à créér
     * @return string
     * @throws \Exception
     */
    protected function addKeycloakUser(array $record): string
    {
        if ($this->inTests) {
            return md5($record['username']);
        }

        $env = getenv();
        $key = $this->getUserPasswordEnvVar($record['username']);
        if (!array_key_exists($key, $env)) {
            printf(
                "Environment variable %s not set, generating a random password for username %s\n",
                $key,
                $record['username']
            );
        }
        $password = $env[$key] ?? $this->generator->generate(80);

        $auth = $this->getKeycloakAuth();
        $this->deleteKeycloakUser($record['username']);

        $data = [
            'firstName' => $record['firstname'],
            'lastName' => $record['lastname'],
            'username' => $record['username'],
            'email' => $record['email'],
            'enabled' => 'true',
            'credentials' => [[
                'type' => 'password',
                'value' => $password,
                'temporary' => false,
            ]],
        ];

        $res = $auth->addUser($data);
        if (!is_array($res) || !array_key_exists('code', $res)) {
            $message = sprintf(
                'Unknown return value from keycloak while creating user %s: %s',
                json_encode($data),
                json_encode($res)
            );
            throw new RuntimeException($message);
        }
        if ($res['code'] !== 201) {
            $message = sprintf(
                'Wrong return status code from keycloak while creating user %s (expected 201, got %d): %s',
                json_encode($data),
                $res['code'],
                json_encode($res)
            );
            throw new RuntimeException($message);
        }

        return $res['id'];
    }

    protected function deleteKeycloakUser(string $username): void
    {
        $auth = $this->getKeycloakAuth();
        $users = $auth->getUsersQuery('?username=' . $username . '&exact=true');
        if (empty($users) === false) {
            $auth->deleteUser($users[0]['id']);
        }
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'organization_id' => null,
            'keycloak_id' => null,
            'active' => true,
            'civility' => null,
            'lastname' => null,
            'firstname' => null,
            'username' => null,
            'email' => null,
            'phone' => null,
            'data_security_policy' => true,
            'created' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'modified' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'id_external' => null,
            'deleted' => null,
        ];
    }

    /**
     * Création des utilisateurs sur keycloak, préparation et insertion des données via une commande SQL.
     *
     * @param array $data Les données à insérer
     * @param array $returning Les champs à retourner
     * @param bool $lock Doit-on verrouiller l'accès à la table ?
     * @return array
     */
    public function insert(array $data, array $returning = [], bool $lock = false): array
    {
        // On force le renouvellement du token
        $this->keycloakAuth = null;

        foreach ($data as $idx => $row) {
            if (!array_key_exists('keycloak_id', $data[$idx]) || $data[$idx]['keycloak_id'] === null) {
                $data[$idx]['keycloak_id'] = $this->addKeycloakUser([
                    'username' => $row['username'],
                    'firstname' => $row['firstname'],
                    'lastname' => $row['lastname'],
                    'email' => $row['email'],
                ]);
            }
        }

        return parent::insert($data, $returning, $lock);
    }
}
