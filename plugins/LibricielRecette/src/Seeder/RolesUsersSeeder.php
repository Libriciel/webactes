<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use PDO;

/**
 * RolesUsers
 */
class RolesUsersSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'roles_users';
        $this->id = 'id';
        $this->label = 'id';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'role_id' => null,
            'user_id' => null,
        ];
    }
}
