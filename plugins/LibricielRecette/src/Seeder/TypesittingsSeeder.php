<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use DateTime;
use PDO;

class TypesittingsSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'typesittings';
        $this->id = 'id';
        $this->label = 'name';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'structure_id' => null,
            'name' => null,
            'created' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'modified' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'active' => true,
            'isdeliberating' => null,
            'generate_template_convocation_id' => null,
            'generate_template_executive_summary_id' => null,
            'generate_template_deliberations_list_id' => null,
            'generate_template_verbal_trial_id' => null,
        ];
    }
}
