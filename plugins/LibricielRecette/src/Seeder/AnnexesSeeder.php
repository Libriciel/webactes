<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use PDO;

/**
 * Annexes
 */
class AnnexesSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'annexes';
        $this->id = 'id';
        $this->label = 'id';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'structure_id' => null,
            'container_id' => null,
            'rank' => null,
            'istransmissible' => true,
            'current' => true,
            'codetype' => null,
            'is_generate' => false,
        ];
    }
}
