<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use DateTime;
use PDO;

/**
 * Natures (données remontées)
 */
class NaturesSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'natures';
        $this->id = 'id';
        $this->label = 'typeabrege';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'structure_id' => null,
            'created' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'modified' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'codenatureacte' => null,
            'libelle' => null,
            'typeabrege' => null,
        ];
    }
}
