<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use PDO;

/**
 * DraftTemplatesTypesacts
 */
class DraftTemplatesTypesactsSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'draft_templates_typesacts';
        $this->id = 'id';
        $this->label = 'id';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'draft_template_id' => null,
            'typesact_id' => null,
        ];
    }
}
