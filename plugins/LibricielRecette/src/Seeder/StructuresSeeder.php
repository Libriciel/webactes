<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use DateTime;
use PDO;

/**
 * Structures
 */
class StructuresSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'structures';
        $this->id = 'id';
        $this->label = 'business_name';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'organization_id' => null,
            'active' => true,
            'logo' => null,
            'business_name' => null,
            'created' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'modified' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'id_orchestration' => null,
            'customname' => null,
            'id_external' => null,
        ];
    }
}
