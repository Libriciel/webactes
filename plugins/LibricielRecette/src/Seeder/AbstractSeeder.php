<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use PDO;

abstract class AbstractSeeder
{
    /**
     * La connexion PDO à utiliser
     *
     * @var \PDO
     */
    protected PDO $connection;

    /**
     * Le nom de la table.
     *
     * @var string
     */
    public readonly string $table;

    /**
     * Le nom du champ d'identifiant.
     *
     * @var string
     */
    public readonly mixed $id;

    /**
     * Le nom du champ de libellé (unique).
     *
     * @var string
     */
    public readonly mixed $label;

    /**
     * Constructeur.
     *
     * @param \PDO $connection La connexion PDO à utiliser
     */
    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Ajout des valeurs par défaut et échappement des données pour insertion via commandes SQL.
     *
     * @param array $data Les données à échapper
     * @return array
     */
    protected function escape(array $data): array
    {
        foreach ($data as $idx => $row) {
            $defaults = $this->defaults();
            $data[$idx] = $row + $defaults;
            foreach ($data[$idx] as $key => $value) {
                switch (gettype($value)) {
                    case 'boolean':
                        $data[$idx][$key] = $value ? 'true' : 'false';
                        break;
                    case 'double':
                    case 'integer':
                        break;
                    case 'NULL':
                        $data[$idx][$key] = 'NULL';
                        break;
                    default:
                        $type = PDO::PARAM_STR;
                        $value = (string)$value;
                        $data[$idx][$key] = $this->connection->quote($value, $type);
                }
            }
            $data[$idx] = array_intersect_key($data[$idx], $defaults);
        }

        return $data;
    }

    /**
     * Normalisation des données pour ordonner les clés pour insertion via commandes SQL.
     *
     * @param array $data Les données à normaliser
     * @return array
     */
    protected function normalize(array $data): array
    {
        $normalized = [];
        foreach ($data as $idx => $row) {
            $normalized[$idx] = [];
            foreach (array_keys($this->defaults()) as $key) {
                $normalized[$idx][$key] = $row[$key];
            }
        }

        return $normalized;
    }

    /**
     * Retourne les noms des champs et leur valeur par défaut.
     *
     * @return array
     */
    abstract public function defaults(): array;

    /**
     * Préparation et insertion des données via une commande SQL.
     *
     * @param array $data Les données à insérer
     * @param array $returning Les champs à retourner
     * @param bool $lock Doit-on verrouiller l'accès à la table ?
     * @return array
     */
    public function insert(array $data, array $returning = [], bool $lock = false): array
    {
        if (!empty($data)) {
            $fields = array_keys($this->defaults());
            $data = $this->prepare($data);

            $idIdx = array_search('id', $fields);
            if (($idIdx !== false) && mb_strtolower((string)$data[0]['id']) === 'null') {
                unset($fields[$idIdx]);
                foreach ($data as $key => $values) {
                    unset($data[$key]['id']);
                }
            }

            if ($lock) {
                $sql = sprintf('LOCK TABLE "%s" IN ACCESS EXCLUSIVE MODE;', $this->table);
                $this->connection->query($sql)->execute();
            }

            return $this->insertSqlData("public.{$this->table}", $fields, $data, $returning);
        }

        return [];
    }

    /**
     * Préparation données pour insertion via une commande SQL.
     *
     * @param array $data Les données à préparer
     * @return array
     */
    protected function prepare(array $data): array
    {
        return $this->normalize($this->escape($data));
    }

    /**
     * Insertion des données via une commande SQL.
     *
     * @param string $table La table dans laquelle insérer les données
     * @param array $fields Les champs des données à insérer
     * @param array $data Les données à insérer
     * @param array $returning Les champs à retourner
     * @return array
     * @throws \Throwable
     */
    protected function insertSqlData(string $table, array $fields, array $data, array $returning = []): array
    {
        $sql = [];
        foreach ($data as $idx => $row) {
            $sql[$idx] = '(' . implode(', ', $row) . ')';
        }

        if (!empty($returning)) {
            $returning = sprintf(' RETURNING %s', implode(', ', $returning));
        } else {
            $returning = '';
        }

        if (in_array('id', $fields)) {
            $overriding = 'OVERRIDING SYSTEM VALUE';
        } else {
            $overriding = '';
        }

        $sql = "INSERT INTO {$table} (\""
            . implode('", "', $fields)
            . "\") {$overriding} VALUES \n"
            . implode(",\n", $sql)
            . $returning . ';';

        try {
            return $this->connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        } catch (\Throwable $exc) {
            echo "\n{$sql}\n";
            throw $exc;
        }
    }
}
