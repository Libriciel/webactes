<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use PDO;

/**
 * StructuresUsers
 */
class StructuresUsersSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'structures_users';
        $this->id = 'id';
        $this->label = 'id';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'structure_id' => null,
            'user_id' => null,
        ];
    }
}
