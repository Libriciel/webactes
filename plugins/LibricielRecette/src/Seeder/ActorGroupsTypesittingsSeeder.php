<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use PDO;

/**
 * ActorGroupsTypesittings
 */
class ActorGroupsTypesittingsSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'actor_groups_typesittings';
        $this->id = 'id';
        $this->label = 'id';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'actor_group_id' => null,
            'typesitting_id' => null,
        ];
    }
}
