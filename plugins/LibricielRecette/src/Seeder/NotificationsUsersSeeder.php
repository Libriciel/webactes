<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use PDO;

/**
 * 1 | Un projet d'acte est en attente de validation à mon étape     | validation_to_do      | f
 * 2 | Mon projet d'acte a été validé / refusé                       | Envoi dans un circuit | f
 * 3 | Mon acte non télétransmissible a été signé                    | signed                | f
 * 4 | Mon acte a été signé et est en attente de dépôt sur le TdT    | Transmission TDT      | f
 * 5 | Mon projet d'acte a été validé et est en attente de signature | Validé                | f
 */
class NotificationsUsersSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'notifications_users';
        $this->id = 'id';
        $this->label = 'id';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'user_id' => null,
            'notification_id' => null,
            'active' => true,
        ];
    }
}
