<?php
declare(strict_types=1);

namespace LibricielRecette\Seeder;

use DateTime;
use PDO;

/**
 * Typesacts
 */
class TypesactsSeeder extends AbstractSeeder
{
    public readonly string $table;
    public readonly mixed $id;
    public readonly mixed $label;

    public function __construct(PDO $connection)
    {
        parent::__construct($connection);

        $this->table = 'typesacts';
        $this->id = 'id';
        $this->label = 'name';
    }

    /**
     * @inheritDoc
     */
    public function defaults(): array
    {
        return [
            'id' => null,
            'structure_id' => null,
            'nature_id' => null,
            'name' => null,
            'created' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'modified' => (new DateTime())->format('Y-m-d H:i:s.u'),
            'istdt' => null,
            'active' => true,
            'isdefault' => null,
            'counter_id' => null,
            'generate_template_project_id' => null,
            'generate_template_act_id' => null,
            'isdeliberating' => null,
        ];
    }
}
