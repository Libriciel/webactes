<?php
declare(strict_types=1);

namespace LibricielRecette\Utility;

class ReflectionUtility
{
    public static function getProtected($obj, $prop)
    {
        $reflection = new \ReflectionClass($obj);
        $property = $reflection->getProperty($prop);
        //@info: il faut garder cette ligne, quoi qu'en dise l'IDE
        $property->setAccessible(true);

        return $property->getValue($obj);
    }
}
