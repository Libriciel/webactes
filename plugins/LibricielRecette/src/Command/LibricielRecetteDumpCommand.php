<?php
// phpcs:disable Generic.Files.LineLength.TooLong

declare(strict_types=1);

namespace LibricielRecette\Command;

use App\Model\Entity\Workflow;
use App\Utilities\Common\RealTimezone;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Database\ExpressionInterface;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Inflector;
use LibricielRecette\Model\Enum\GenerateTemplateType;
use LibricielRecette\Utility\ReflectionUtility;
use RuntimeException;

/**
 * bin/cake-wrapper.sh libriciel_recette_dump
 */
class LibricielRecetteDumpCommand extends Command
{
    use LocatorAwareTrait;

    /**
     * @var \Cake\Database\Connection
     */
    protected $connection;

    /**
     * @var \Cake\Console\ConsoleIo
     */
    protected $io;

    /**
     * @var int|null L'identifiant de la structure à traiter si on limite par structure.
     */
    protected ?int $structureId;

    /**
     * Liste des champs qui ne sont pas de réelles clés étrangères.
     *
     * @var array
     */
    protected $falseForeignKeys = [
        'connector_type_id',
        'generate_template_type_id',
        'notification_id',
        'statesitting_id',
    ];

    protected $forgottenForeignKeys = [
        'counters' => 'counter_id',
        'organizations' => 'organization_id',
        'sequences' => 'sequence_id',
        'structures' => 'structure_id',
    ];

    /**
     * Échappement d'une valeur pour insertion via commandes SQL.
     *
     * @param mixed $value La valeur à échapper
     * @return array
     */
    protected function escape($value): string
    {
        if (is_null($value)) {
            return 'NULL';
        }

        switch (gettype($value)) {
            case 'boolean':
                return $value ? 'true' : 'false';
            case 'double':
            case 'integer':
                return (string)$value;
            case 'object':
                if ($value instanceof \Cake\I18n\FrozenTime) {
                    return "'" . RealTimezone::format($value, 'Y-m-d H:i:s') . "'";
                }

                if ($value instanceof \Cake\I18n\FrozenDate) {
                    return "'" . RealTimezone::format($value, 'Y-m-d') . "'";
                }

                return "'" . str_replace("'", "\\'", (string)$value) . "'";
            default:
                return "'" . str_replace("'", "\\'", (string)$value) . "'";
        }
    }

    /**
     * Retourne la liste des champs d'une table concernés par une clé primaire ou une clé étrangère.
     * Pour chaque champ, on aura un array avec en clé 0 le nom de la table et en clé 1 le nom du champ.
     * Ne fonctionne pas en cas de clés ayant des champs mutiples.
     *
     * @param string $table Le nom de la table
     * @return array
     */
    protected function keys(string $table): array
    {
        $details = $this->schemaCollection->describe($table);

        $keys = [];
        foreach ($details->constraints() as $name) {
            $constraint = $details->getConstraint($name);
            if ($constraint['type'] === 'primary') {
                $keys[$constraint['columns'][0]] = [$table, $constraint['columns'][0]];
            } elseif ($constraint['type'] === 'foreign') {
                $keys[$constraint['columns'][0]] = $constraint['references'];
            }
        }

        return $keys;
    }

    protected function getGenerateTemplateTypeConstant(mixed $id): string
    {
        switch ((int)$id) {
            case GenerateTemplateType::PROJECT:
                return 'GenerateTemplateType::PROJECT';
            case GenerateTemplateType::ACT:
                return 'GenerateTemplateType::ACT';
            case GenerateTemplateType::CONVOCATION:
                return 'GenerateTemplateType::CONVOCATION';
            case GenerateTemplateType::EXECUTIVE_SUMMARY:
                return 'GenerateTemplateType::EXECUTIVE_SUMMARY';
            case GenerateTemplateType::DELIBERATIONS_LIST:
                return 'GenerateTemplateType::DELIBERATIONS_LIST';
            case GenerateTemplateType::VERBAL_TRIAL:
                return 'GenerateTemplateType::VERBAL_TRIAL';
            default:
                $message = sprintf('Could not find enum value for %d', (int)$id);
                throw new RuntimeException($message);
        }
    }

    /**
     * Export dans la console du code PHP à mettre dans le seeder.
     *
     * @param string $method Le nom de la méthode servant à importer les données
     * @param string $table Le nom de la table dont on veut exporter les données
     * @param array|\Cake\Database\ExpressionInterface|\Closure|null|string $conditions Des conditions supplémentaires
     * @param bool $inline Doit-on écrire sur une ligne pour chaque enregistrement ?
     * @param array $ignore Les champs à ne pas inclure dans les données exportées (en plus de created, modified, lft, rght et keycloak_id)
     * @return void
     */
    protected function export(
        string $method,
        string $table,
        array|ExpressionInterface|\Closure|null|string $conditions = null,
        bool $inline = false,
        array $ignore = []
    ): void {
        $ignore = array_unique(array_merge($ignore, ['id', 'created', 'modified', 'lft', 'rght', 'keycloak_id']));

        $keys = $this->keys($table);
        $model = $this->fetchTable($table);

        if ($model->hasBehavior('SoftDeletion')) {
            $model->removeBehavior('SoftDeletion');
        }

        $records = $model
            ->find()
            ->where($conditions)
            ->orderAsc('id')
            ->disableAutoFields()
            ->enableHydration(false)
            ->toArray();

        $classname = sprintf('\LibricielRecette\Seeder\%sSeeder', Inflector::camelize($table));
        $seeder = new $classname($this->connection->getDriver()->getConnection());
        $defaults = $seeder->defaults();

        $this->io->out("protected function {$method}(): void");
        $this->io->out('{');
        $this->io->out('    $this->save(');
        $this->io->out(sprintf('        $this->%s,', Inflector::variable($seeder->table)));

        $this->io->out('        [');
        foreach ($records as $record) {
            $fields = [];
            if (!$inline) {
                $this->io->out('            [');
            } else {
                $this->io->out('            [', 0);
            }
            foreach ($record as $field => $value) {
                if (
                    !in_array($field, $ignore)
                    && (!array_key_exists($field, $defaults) || $defaults[$field] !== $value)
                ) {
                    if (is_array($value)) {
                        $value = $this->escape(json_encode($value));
                    } else {
                        if ($field === 'generate_template_type_id') {
                            $value = $this->getGenerateTemplateTypeConstant($value);
                        } else {
                            $value = $this->escape($value);
                        }

                        if (
                            (array_key_exists($field, $keys) || in_array($field, $this->forgottenForeignKeys))
                            && $value !== 'null'
                            && !in_array($field, $this->falseForeignKeys)
                        ) {
                            $foreignTable = $keys[$field][0] ?? array_search($field, $this->forgottenForeignKeys);

                            // @fixme: wrong offset (themes, ...)
                            if ($field !== 'parent_id') {
                                $classname = sprintf(
                                    '\LibricielRecette\Seeder\%sSeeder',
                                    Inflector::camelize($foreignTable)
                                );
                                $seeder = new $classname($this->connection->getDriver()->getConnection());

                                $sql = "SELECT {$seeder->label} FROM {$seeder->table} WHERE {$seeder->id} = $value";
                                $value = $this->connection->execute($sql)->fetch()[0];

                                // Special care for dates
                                if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $value)) {
                                    $value = RealTimezone::format(new FrozenDate($value), 'Y-m-d');
                                } elseif (
                                    preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/', $value)
                                ) {
                                    $value = RealTimezone::format(new FrozenTime($value), 'Y-m-d H:i:s');
                                }

                                $value = sprintf(
                                    '$this->record($this->%s, \'%s\')',
                                    Inflector::variable($seeder->table),
                                    str_replace('\'', '\\\'', $value)
                                );
                            } else {
                                $value = sprintf('$this->id(\'%s\', %d)', $foreignTable, $value);
                            }
                        }
                    }

                    $value = preg_replace('/@mailcatchall.libriciel.net\'/', '@\' . $this->domain()', $value);

                    if (!empty($this->structureId)) {
                        $value = preg_replace(
                            '/\$this->record\(\$this->(organizations|structures), .*\)/',
                            '\$this->current(\$this->\1)',
                            $value
                        );
                    }

                    if (!$inline) {
                        $fields[] = sprintf("                '%s' => %s", $field, $value);
                    } else {
                        $fields[] = sprintf("'%s' => %s", $field, $value);
                    }
                }
            }
            if (!$inline) {
                $this->io->out(implode(",\n", $fields) . ",\n", 0);
                $this->io->out('            ],');
            } else {
                $this->io->out(implode(', ', $fields), 0);
                $this->io->out('],');
            }
        }
        $this->io->out('        ]');
        $this->io->out('    );');
        $this->io->out('}', 2);
    }

    protected function exportBegin(): void
    {
        $content = <<<EOL
<?php
// phpcs:disable Generic.Files.LineLength.TooLong
declare(strict_types=1);

use LibricielRecette\Model\Enum\GenerateTemplateType;
use LibricielRecette\Seeds\LibricielRecetteAbstractStructureSeed;

// @fixme: change class name
class LibricielRecetteStructureXSeed extends LibricielRecetteAbstractStructureSeed
{
    protected ?int \$year;

    /**
     * @var \SplFileInfo[]
     */
    protected array \$paths = [];

EOL;
        $this->io->out($content);
    }

    protected function exportWorkflows(array|ExpressionInterface|\Closure|null|string $conditions = null): void
    {
        $records = $this->fetchTable('Workflows')
            ->find()
            ->where($conditions)
            ->orderAsc('structure_id')
            ->orderAsc('id')
            ->disableAutoFields()
            ->toArray();

        foreach (array_keys($records) as $idx) {
            try {
                $steps = $records[$idx]->getSteps();
                Workflow::completeUserData($steps);
            } catch (\Throwable $exc) {
                $config = ReflectionUtility::getProtected($this->fetchTable('Workflows')->getConnection(), '_config');
                if ($config['name'] !== 'test') {
                    throw $exc;
                }
                $steps = [];
            }
            $records[$idx]->steps = $steps;
        }

        $admins = [];

        $this->io->out('    protected function setupWorkflows(): void');
        $this->io->out('    {');
        foreach ($records as $record) {
            if (!array_key_exists($record->structure_id, $admins)) {
                $admins[$record->structure_id] = $this->fetchTable('Users')
                    ->find()
                    ->innerJoin('structures_users', ['Users.id = structures_users.user_id'])
                    ->innerJoin('roles_users', ['Users.id = roles_users.user_id'])
                    ->innerJoin('roles', ['roles.id = roles_users.role_id'])
                    ->where([
                        'structures_users.structure_id' => $record->structure_id,
                        'roles.name' => 'Administrateur',
                    ])
                    ->firstOrFail()
                    ->get('username');

                $this->io->out('        $this->apiConnect(' . $this->escape($admins[$record->structure_id]) . ');', 2);
            }

            $this->io->out('        $this->apiAddWorkflow(');
            $this->io->out('            [');
            $this->io->out('                \'name\' => ' . $this->escape($record->name) . ',');
            $this->io->out('                \'description\' => ' . $this->escape($record->description) . ',');
            $this->io->out('                \'steps\' => [');
            foreach ($record->steps as $step) {
                $validators = [];
                foreach ($step['validators'] as $validator) {
                    $username = $this->fetchTable('Users')->find()->where(['id' => $validator['id']])->firstOrFail()->get('username');
                    $validators[] = '$this->getUserId(' . $this->escape($username) . ')';
                }
                $this->io->out('                    [');
                $this->io->out('                        \'groups\' => \'\',');
                $this->io->out('                        \'name\' => ' . $this->escape($step['name']) . ',');
                $this->io->out('                        \'users\' => [', count($validators) > 1 ? 1 : 0);
                $this->io->out(
                    (count($validators) > 1 ? '                            ' : '')
                    . implode(",\n                            ", $validators) . (count($validators) > 1 ? ',' : ''),
                    count($validators) > 1 ? 1 : 0
                );
                $this->io->out(count($validators) > 1 ? '                        ],' : '],');
                $this->io->out('                    ],');
            }
            $this->io->out('                ],');
            $this->io->out('            ],');
            $this->io->out('            ' . $this->escape($record->active));
            $this->io->out('        );', 2);
        }
        $this->io->out('    }', 2);
    }

    protected function exportEnd(): void
    {
        $content = <<<EOL
    protected function setup(): void
    {
        \$this->execute('BEGIN;');
        \$this->setupOrganizations();
        \$this->setupStructures();
        \$this->setupStructureSettings();
        \$this->setupOfficials();
        \$this->setupUsers();
        \$this->setupStructuresUsers();
        \$this->setupNotificationsUsers();
        \$this->setupRoles();
        \$this->setupRolesUsers();
        \$this->setupConnecteurs();
        \$this->setupPastellfluxtypes();
        \$this->setupGenerateTemplates();
        \$this->setupGenerateTemplatesFiles();
        \$this->setupDpos();
        \$this->setupThemes();
        \$this->setupNatures();
        \$this->setupMatieres();
        \$this->setupClassifications();
        \$this->setupTypespiecesjointes();
        \$this->setupTypesittings();
        \$this->setupSequences();
        \$this->setupCounters();
        \$this->setupTypesacts();
        \$this->setupTypesactsTypesittings();
        \$this->setupActorGroups();
        \$this->setupActors();
        \$this->setupActorsActorGroups();
        \$this->setupActorGroupsTypesittings();
        \$this->setupDraftTemplates();
        \$this->setupDraftTemplatesTypesacts();
        \$this->setupDraftTemplatesFiles();
        \$this->setupSittings();
        \$this->setupSittingsStatesittings();
        \$this->execute('COMMIT;');

        \$this->setupWorkflows();
    }

    // @fixme: env var + sx
    protected function createProjects(): void
    {
        \$max = \$this->env('LIBRICIEL_RECETTE_SX_NOMBRE_PROJETS', 10);
        for (\$idx = 1; \$idx <= \$max; \$idx++) {
            \$this->addProjectWithGeneration(
                \$this->current(\$this->organizations),
                \$this->current(\$this->structures),
                \$this->record(\$this->users, 'admin@wa-sx'),
                \$this->record(\$this->themes, 'SX Gestion'),
                \$this->record(\$this->typesacts, 'SX Délibération'),
                \$this->record(\$this->draftTemplates, 'SX acte délibération'),
                \$this->record(\$this->draftTemplates, 'SX projet délibération'),
            );
        }
    }

    // @fixme: paths + id
    public function run(): void
    {
        parent::run();

        \$this->sequences();

        \$this->year = (int)date('Y');

        \$dirs = [
            'GenerateTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesGenerateTemplates' . DS,
            'DraftTemplates' => dirname(__DIR__, 2) . DS . 'files' . DS . 'FilesDraftTemplates' . DS,
        ];
        \$this->paths = [
            'SX Projet arrêté' => \$this->createFile(\$dirs['GenerateTemplates'] . 'SX Projet arrete.odt', 'generate_template_id', \$this->id('organizations', 1), \$this->id('structures', 1), \$this->id('generate_templates', 1)),
            'SX Projet délibération' => \$this->createFile(\$dirs['GenerateTemplates'] . 'SX Projet deliberation.odt', 'generate_template_id', \$this->id('organizations', 1), \$this->id('structures', 1), \$this->id('generate_templates', 2)),
            'SX Convocation' => \$this->createFile(\$dirs['GenerateTemplates'] . 'SX Convocation.odt', 'generate_template_id', \$this->id('organizations', 1), \$this->id('structures', 1), \$this->id('generate_templates', 3)),
            'SX Note de synthèse' => \$this->createFile(\$dirs['GenerateTemplates'] . 'SX Note de synthese.odt', 'generate_template_id', \$this->id('organizations', 1), \$this->id('structures', 1), \$this->id('generate_templates', 4)),
            'SX Liste des délibérations' => \$this->createFile(\$dirs['GenerateTemplates'] . 'SX Liste des deliberations.odt', 'generate_template_id', \$this->id('organizations', 1), \$this->id('structures', 1), \$this->id('generate_templates', 5)),
            'SX Procès-verbal' => \$this->createFile(\$dirs['GenerateTemplates'] . 'SX Proces-verbal.odt', 'generate_template_id', \$this->id('organizations', 1), \$this->id('structures', 1), \$this->id('generate_templates', 6)),
            'SX Acte' => \$this->createFile(\$dirs['GenerateTemplates'] . 'SX Acte.odt', 'generate_template_id', \$this->id('organizations', 1), \$this->id('structures', 1), \$this->id('generate_templates', 7)),
            'SX Délibération' => \$this->createFile(\$dirs['GenerateTemplates'] . 'SX Deliberation.odt', 'generate_template_id', \$this->id('organizations', 1), \$this->id('structures', 1), \$this->id('generate_templates', 8)),
            //------------------------------------------------------------------------------------------------------------------
            'gabarit_arrete_libriciel' => \$this->createFile(\$dirs['DraftTemplates'] . 'gabarit_arrete_libriciel.odt', 'draft_template_id', 1, \$this->id('structures', 1), \$this->id('draft_templates', 1)),
            'gabarit_projet_libriciel' => \$this->createFile(\$dirs['DraftTemplates'] . 'gabarit_projet_libriciel.odt', 'draft_template_id', 1, \$this->id('structures', 1), \$this->id('draft_templates', 2)),
            'texte_delib2' => \$this->createFile(\$dirs['DraftTemplates'] . 'texte_delib2.odt', 'draft_template_id', \$this->id('organizations', 1), \$this->id('structures', 1), \$this->id('draft_templates', 3)),
            'texte_projet' => \$this->createFile(\$dirs['DraftTemplates'] . 'texte_projet.odt', 'draft_template_id', \$this->id('organizations', 1), \$this->id('structures', 1), \$this->id('draft_templates', 4)),
        ];

        \$this->setup();

        \$this->sequences();
        \$this->createProjects();
        \$this->sequences();

        \$this->permissions();
    }
}

EOL;

        $this->io->out($content);
    }

    /**
     * @inheritDoc
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->setLoggers(false);

        $this->io = $io;
        $this->connection = $this->fetchTable('Users')->getConnection();
        $this->schemaCollection = $this->connection->getSchemaCollection();

        $structureId = $args->getArgument('structure_id');
        $this->structureId = empty($structureId) ? null : (int)$structureId;

        $this->exportBegin();

        $this->export(
            'setupOrganizations',
            'organizations',
            !$this->structureId
                ? null
                : [
                'id IN' => $this->fetchTable('Structures')
                    ->find()
                    ->select(['organization_id'])
                    ->where(['id' => $this->structureId]),
            ]
        );

        $this->io->out('    // @fixme: use $env = getenv();');
        $this->export(
            'setupStructures',
            'structures',
            !$this->structureId ? null : ['id' => $this->structureId]
        );

        $this->export(
            'setupStructureSettings',
            'structure_settings',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupOfficials',
            'officials',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupUsers',
            'users',
            !$this->structureId
                ? null
                : ['id IN' => $this->fetchTable('StructuresUsers')
                ->find()
                ->select(['user_id'])
                ->where(['structure_id' => $this->structureId]),
            ]
        );

        $this->export(
            'setupStructuresUsers',
            'structures_users',
            !$this->structureId ? null : ['structure_id' => $this->structureId],
            true,
            ['id']
        );

        $this->export(
            'setupNotificationsUsers',
            'notifications_users',
            !$this->structureId
                ? null
                : [
                'user_id IN' => $this->fetchTable('StructuresUsers')
                    ->find()
                    ->select(['user_id'])
                    ->where(['structure_id' => $this->structureId]),
            ],
            true,
            ['id']
        );

        $this->export(
            'setupRoles',
            'roles',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupRolesUsers',
            'roles_users',
            !$this->structureId
                ? null
                : [
                'role_id IN' => $this->fetchTable('Roles')
                    ->find()
                    ->select(['id'])
                    ->where(['structure_id' => $this->structureId]),
            ],
            true,
            ['id']
        );

        $this->io->out('    // @fixme: use $env = getenv();');
        $this->export(
            'setupConnecteurs',
            'connecteurs',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupPastellfluxtypes',
            'pastellfluxtypes',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupGenerateTemplates',
            'generate_templates',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->io->out('    // @fixme: use $this->paths[...]->getRealPath()/$this->paths[...]->getSize()');
        $condition = 'generate_template_id IS NOT NULL';
        $this->export(
            'setupGenerateTemplatesFiles',
            'files',
            !$this->structureId ? [$condition] : ['structure_id' => $this->structureId, $condition]
        );

        $this->export(
            'setupDpos',
            'dpos',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupThemes',
            'themes',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        /*
        // @todo: param to force
        $this->export(
            'setupNatures',
            'natures',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        // @todo: param to force
        $this->export(
            'setupMatieres',
            'matieres',
            !$this->structureId ? null : ['structure_id' => $this->structureId],
            true
        );

        // @todo: param to force
        $this->export(
            'setupClassifications',
            'classifications',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        // @todo: param to force
        $this->export(
            'setupTypespiecesjointes',
            'typespiecesjointes',
            !$this->structureId ? null : ['structure_id' => $this->structureId],
            true
        );
        */

        $this->export(
            'setupTypesittings',
            'typesittings',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupSequences',
            'sequences',
            !$this->structureId
                ? null
                : [
                'id IN' => $this->fetchTable('Counters')
                    ->find()
                    ->select(['sequence_id'])
                    ->where(['structure_id' => $this->structureId]),
            ]
        );

        $this->export(
            'setupCounters',
            'counters',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupTypesacts',
            'typesacts',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupTypesactsTypesittings',
            'typesacts_typesittings',
            !$this->structureId
                ? null
                : [
                'typesact_id IN' => $this->fetchTable('Typesacts')
                    ->find()
                    ->select(['id'])
                    ->where(['structure_id' => $this->structureId]),
            ],
            true,
            ['id']
        );

        $this->export(
            'setupActorGroups',
            'actor_groups',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupActors',
            'actors',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->export(
            'setupActorsActorGroups',
            'actors_actor_groups',
            !$this->structureId
            ? null
            : ['actor_id IN' => $this->fetchTable('Actors')
                ->find()
                ->select(['id'])
                ->where(['structure_id' => $this->structureId])],
            true
        );

        $this->export(
            'setupActorGroupsTypesittings',
            'actor_groups_typesittings',
            !$this->structureId
            ? null
            : ['typesitting_id IN' => $this->fetchTable('Typesittings')
                ->find()
                ->select(['id'])
                ->where(['structure_id' => $this->structureId])],
            true,
            ['id']
        );

        $this->export(
            'setupDraftTemplates',
            'draft_templates',
            !$this->structureId ? null : ['structure_id' => $this->structureId],
            true
        );

        $this->export(
            'setupDraftTemplatesTypesacts',
            'draft_templates_typesacts',
            !$this->structureId
            ? null
            : ['draft_template_id IN' => $this->fetchTable('DraftTemplates')
                ->find()
                ->select(['id'])
                ->where(['structure_id' => $this->structureId])],
            true
        );

        $this->io->out('    // @fixme: use $this->paths[...]->getRealPath()/$this->paths[...]->getSize()');
        $condition = 'draft_template_id IS NOT NULL';
        $this->export(
            'setupDraftTemplatesFiles',
            'files',
            !$this->structureId ? [$condition] : ['structure_id' => $this->structureId, $condition]
        );

        $this->io->out('    // @fixme: use $this->year');
        $this->export(
            'setupSittings',
            'sittings',
            !$this->structureId ? null : ['structure_id' => $this->structureId]
        );

        $this->io->out('    // @fixme: use $this->year');
        $this->export(
            'setupSittingsStatesittings',
            'sittings_statesittings',
            !$this->structureId
                ? null
                : [
                'sitting_id IN' => $this->fetchTable('Sittings')
                    ->find()
                    ->select(['id'])
                    ->where(['structure_id' => $this->structureId]),
            ],
            true,
            ['id']
        );

        $this->exportWorkflows(!$this->structureId ? null : ['structure_id' => $this->structureId]);

        $this->exportEnd();

        return static::CODE_SUCCESS;
    }

    /**
     * @inheritDoc
     */
    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription([
            'Commande permettant d\'exporter en console les seeds de l\'ensemble des structures'
            . ' ou d\'une structure en particulier',
            '',
            'Pour exporter les seeds d\'une structure en particulier, il faut passer son identifiant en paramètre.',
        ]);

        $parser->addArgument(
            'structure_id',
            [
                'help' => 'L\'identifiant de la structure (par défaut null, toutes structures)',
            ],
        );

        return $parser;
    }
}
