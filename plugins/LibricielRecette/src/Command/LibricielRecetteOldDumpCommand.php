<?php
declare(strict_types=1);

namespace LibricielRecette\Command;

use App\Utilities\Common\RealTimezone;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Database\ExpressionInterface;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Inflector;

/**
 * @deprecated
 */
class LibricielRecetteOldDumpCommand extends Command
{
    use LocatorAwareTrait;

    /**
     * @var \Cake\Database\Connection
     */
    protected $connection;

    /**
     * @var \Cake\Console\ConsoleIo
     */
    protected $io;

    /**
     * Liste des champs qui ne sont pas de réelles clés étrangères.
     *
     * @var array
     */
    protected $falseForeignKeys = [
        'connector_type_id',
        'generate_template_type_id',
        'notification_id',
        'statesitting_id',
    ];

    /**
     * Échappement d'une valeur pour insertion via commandes SQL.
     *
     * @param mixed $value La valeur à échapper
     * @return array
     */
    protected function escape($value): string
    {
        if (is_null($value)) {
            return 'NULL';
        }

        switch (gettype($value)) {
            case 'boolean':
                return $value ? 'true' : 'false';
            case 'double':
            case 'integer':
                return (string)$value;
            case 'object':
                if ($value instanceof \Cake\I18n\FrozenTime) {
                    return "'" . RealTimezone::format($value, 'Y-m-d H:i:s.u') . "'";
                }

                if ($value instanceof \Cake\I18n\FrozenDate) {
                    return "'" . RealTimezone::format($value, 'Y-m-d') . "'";
                }

                return "'" . str_replace("'", "\\'", (string)$value) . "'";
            default:
                return "'" . str_replace("'", "\\'", (string)$value) . "'";
        }
    }

    /**
     * Retourne la liste des champs d'une table concernés par une clé primaire ou une clé étrangère.
     * Pour chaque champ, on aura un array avec en clé 0 le nom de la table et en clé 1 le nom du champ.
     * Ne fonctionne pas en cas de clés ayant des champs mutiples.
     *
     * @param string $table Le nom de la table
     * @return array
     */
    protected function keys(string $table): array
    {
        $details = $this->schemaCollection->describe($table);

        $keys = [];
        foreach ($details->constraints() as $name) {
            $constraint = $details->getConstraint($name);
            if ($constraint['type'] === 'primary') {
                $keys[$constraint['columns'][0]] = [$table, $constraint['columns'][0]];
            } elseif ($constraint['type'] === 'foreign') {
                $keys[$constraint['columns'][0]] = $constraint['references'];
            }
        }

        return $keys;
    }

    /**
     * Export dans la console du code PHP à mettre dans le seeder.
     *
     * @param string $table Le nom de la table dont on veut exporter les données
     * @param array|\Cake\Database\ExpressionInterface|\Closure|null|string $conditions Des conditions supplémentaires
     * @param bool $inline Doit-on écrire sur une ligne pour chaque enregistrement ?
     * @param array $ignore Les champs à ne pas inclure dans les données exportées (en plus de created, modified, lft, rght et keycloak_id)
     * @return void
     */
    protected function export(
        string $table,
        array|ExpressionInterface|\Closure|null|string $conditions = null,
        bool $inline = false,
        array $ignore = []
    ): void {
        $ignore = array_unique(array_merge($ignore, ['created', 'modified', 'lft', 'rght', 'keycloak_id']));

        $keys = $this->keys($table);
        $model = $this->fetchTable($table);

        if ($model->hasBehavior('SoftDeletion')) {
            $model->removeBehavior('SoftDeletion');
        }

        $records = $model
            ->find()
            ->where($conditions)
            ->orderAsc('id')
            ->disableAutoFields()
            ->enableHydration(false)
            ->toArray();

        $classname = sprintf('\LibricielRecette\Seeder\%sSeeder', Inflector::camelize($table));
        $defaults = (new $classname($this->connection->getDriver()->getConnection()))->defaults();

        $this->io->out('$data = [');
        foreach ($records as $record) {
            if (!$inline) {
                $this->io->out('    [');
            } else {
                $this->io->out('    [', 0);
            }
            foreach ($record as $field => $value) {
                if (
                    !in_array($field, $ignore)
                    && (!array_key_exists($field, $defaults) || $defaults[$field] !== $value)
                ) {
                    if (is_array($value)) {
                        $value = $this->escape(json_encode($value));
                    } else {
                        $value = $this->escape($value);
                        if (
                            array_key_exists($field, $keys)
                            && $value !== 'null'
                            && !in_array($field, $this->falseForeignKeys)
                        ) {
                            $value = sprintf('$this->id(\'%s\', %d)', $keys[$field][0], $value);
                        }
                    }
                    if (!$inline) {
                        $this->io->out(sprintf("        '%s' => %s,", $field, $value));
                    } else {
                        $this->io->out(sprintf("'%s' => %s, ", $field, $value), 0);
                    }
                }
            }
            if (!$inline) {
                $this->io->out('    ],');
            } else {
                $this->io->out('],');
            }
        }
        $this->io->out('];');
        $this->io->out(sprintf('$this->%s->insert($data);', Inflector::variable($table)), 2);
    }

    /**
     * @inheritDoc
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->setLoggers(false);

        $this->io = $io;
        $this->connection = $this->fetchTable('Users')->getConnection();
        $this->schemaCollection = $this->connection->getSchemaCollection();

        $structureId = $args->getArgument('structure_id');

        $this->io->out('// Organizations');
        $this->export('organizations', !$structureId
            ? null
            : [
                'id IN' => $this->fetchTable('Structures')
                    ->find()
                    ->select(['organization_id'])
                    ->where(['id' => $structureId]),
            ]);

        $this->io->out('// Structures');
        $this->export('structures', !$structureId ? null : ['id' => $structureId]);

        $this->io->out('// StructureSettings');
        $this->export('structure_settings', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Officials');
        $this->export('officials', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Users');
        $this->export('users', !$structureId
            ? null
            : ['id IN' => $this->fetchTable('StructuresUsers')
                ->find()
                ->select(['user_id'])
                ->where(['structure_id' => $structureId]),
            ]);

        $this->io->out('// StructuresUsers');
        $this->export(
            'structures_users',
            !$structureId ? null : ['structure_id' => $structureId],
            true,
            ['id']
        );

        $this->io->out('// NotificationsUsers');
        $this->export(
            'notifications_users',
            !$structureId
                ? null
                : [
                'user_id IN' => $this->fetchTable('StructuresUsers')
                    ->find()
                    ->select(['user_id'])
                    ->where(['structure_id' => $structureId]),
            ],
            true,
            ['id']
        );

        $this->io->out('// Roles');
        $this->export('roles', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// RolesUsers');
        $this->export(
            'roles_users',
            !$structureId
                ? null
                : [
                'role_id IN' => $this->fetchTable('Roles')
                    ->find()
                    ->select(['id'])
                    ->where(['structure_id' => $structureId]),
            ],
            true,
            ['id']
        );

        $this->io->out('// Connecteurs');
        $this->export('connecteurs', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Pastellfluxtypes');
        $this->export('pastellfluxtypes', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// GenerateTemplates');
        $this->export('generate_templates', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Files (GenerateTemplates)');
        $condition = 'generate_template_id IS NOT NULL';
        $this->export('files', !$structureId ? [$condition] : ['structure_id' => $structureId, $condition]);

        $this->io->out('// Dpos');
        $this->export('dpos', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Themes');
        $this->export('themes', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Natures');
        $this->export('natures', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Matieres');
        $this->export('matieres', !$structureId ? null : ['structure_id' => $structureId], true);

        $this->io->out('// Classifications');
        $this->export('classifications', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Typespiecesjointes');
        $this->export('typespiecesjointes', !$structureId ? null : ['structure_id' => $structureId], true);

        $this->io->out('// Typesittings');
        $this->export('typesittings', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Sequences');
        $this->export('sequences', !$structureId
            ? null
            : ['id IN' => $this->fetchTable('Counters')
                ->find()
                ->select(['sequence_id'])
                ->where(['structure_id' => $structureId])]);

        $this->io->out('// Counters');
        $this->export('counters', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Typesacts');
        $this->export('typesacts', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// TypesactsTypesittings');
        $this->export(
            'typesacts_typesittings',
            !$structureId
                ? null
                : [
                'typesact_id IN' => $this->fetchTable('Typesacts')
                    ->find()
                    ->select(['id'])
                    ->where(['structure_id' => $structureId]),
            ],
            true,
            ['id']
        );

        $this->io->out('// ActorGroups');
        $this->export('actor_groups', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// Actors');
        $this->export('actors', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// ActorsActorGroups');
        $this->export(
            'actors_actor_groups',
            !$structureId
            ? null
            : ['actor_id IN' => $this->fetchTable('Actors')
                ->find()
                ->select(['id'])
                ->where(['structure_id' => $structureId])],
            true
        );

        $this->io->out('// ActorGroupsTypesittings');
        $this->export(
            'actor_groups_typesittings',
            !$structureId
            ? null
            : ['typesitting_id IN' => $this->fetchTable('Typesittings')
                ->find()
                ->select(['id'])
                ->where(['structure_id' => $structureId])],
            true,
            ['id']
        );

        $this->io->out('// DraftTemplates');
        $this->export('draft_templates', !$structureId ? null : ['structure_id' => $structureId], true);

        $this->io->out('// DraftTemplatesTypesacts');
        $this->export(
            'draft_templates_typesacts',
            !$structureId
            ? null
            : ['draft_template_id IN' => $this->fetchTable('DraftTemplates')
                ->find()
                ->select(['id'])
                ->where(['structure_id' => $structureId])],
            true
        );

        $this->io->out('// Files (DraftTemplates)');
        $condition = 'draft_template_id IS NOT NULL';
        $this->export('files', !$structureId ? [$condition] : ['structure_id' => $structureId, $condition]);

        $this->io->out('// Sittings');
        $this->export('sittings', !$structureId ? null : ['structure_id' => $structureId]);

        $this->io->out('// SittingsStatesittings');
        $this->export(
            'sittings_statesittings',
            !$structureId
                ? null
                : [
                'sitting_id IN' => $this->fetchTable('Sittings')
                    ->find()
                    ->select(['id'])
                    ->where(['structure_id' => $structureId]),
            ],
            false,
            ['id']
        );

        return static::CODE_SUCCESS;
    }

    /**
     * @inheritDoc
     */
    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription([
            'Commande permettant d\'exporter en console les seeds de l\'ensemble des structures'
            . ' ou d\'une structure en particulier',
            '',
            'Pour exporter les seeds d\'une structure en particulier, il faut passer son identifiant en paramètre.',
        ]);

        $parser->addArgument(
            'structure_id',
            [
                'help' => 'L\'identifiant de la structure (par défaut null, toutes structures)',
            ],
        );

        return $parser;
    }
}
