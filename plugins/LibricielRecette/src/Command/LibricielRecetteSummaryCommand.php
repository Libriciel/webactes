<?php
declare(strict_types=1);

namespace LibricielRecette\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Hash;
use LibricielRecette\Service\EnvvarService;

/**
 * bin/cake-wrapper.sh libriciel_recette_summary
 */
class LibricielRecetteSummaryCommand extends Command
{
    use LocatorAwareTrait;

    /**
     * @var \Cake\Console\ConsoleIo
     */
    protected $io;

    /**
     * Affiche la liste des acteurs de la structure sous forme de tableau.
     *
     * @param int $structureId L'identifiant de la structure (structures.id)
     * @return void
     */
    protected function actors(int $structureId): void
    {
        $actors = $this->fetchTable('Actors')
            ->find()
            ->select([
                'Actors.id',
                'Actors.active',
                'Actors.civility',
                'Actors.lastname',
                'Actors.firstname',
                'Actors.email',
            ])
            ->innerJoinWith('Structures')
            ->innerJoinWith('Structures.Organizations')
            ->contain([
                'ActorGroups' => [
                    'fields' => ['ActorGroups.name'],
                    'sort' => ['ActorGroups.name ASC'],
                ],
            ])
            ->where(['Actors.structure_id' => $structureId])
            ->orderAsc('Actors.email')
            ->enableHydration(false)
            ->toArray();

        if (empty($actors)) {
            $this->io->out('Cette structure ne comporte aucun acteur.');
        } else {
            $data = [
                ['Actif', 'Civilité', 'Nom', 'Prénom', 'E-mail', 'Groupes'],
            ];
            foreach ($actors as $actor) {
                $data[] = [
                    $actor['active'] ? 'Oui' : 'Non',
                    $actor['civility'],
                    $actor['lastname'],
                    $actor['firstname'],
                    $actor['email'],
                    implode(', ', Hash::extract($actor, 'actor_groups.{n}.name')),
                ];
            }
            $this->io->helper('Table')->output($data);
        }
    }

    /**
     * Affiche la liste des utilisateurs de la structure sous forme de tableau.
     *
     * @param int $structureId L'identifiant de la structure (structures.id)
     * @return void
     */
    protected function users(int $structureId): void
    {
        $users = $this->fetchTable('Users')
            ->find()
            ->select([
                'Roles.id',
                'Roles.name',
                'Users.id',
                'Users.active',
                'Users.civility',
                'Users.lastname',
                'Users.firstname',
                'Users.username',
                'Users.email',
            ])
            ->innerJoinWith('Organizations')
            ->innerJoinWith('Roles')
            ->innerJoinWith('Roles.Structures')
            ->where(['Structures.id' => $structureId])
            ->orderAsc('Users.email')
            ->enableHydration(false)
            ->toArray();

        if (empty($users)) {
            $this->io->out('Cette structure ne comporte aucun utilisateur.');
        } else {
            $data = [
                ['Actif', 'Identifiant', 'Civilité', 'Nom', 'Prénom', 'E-mail', 'Rôle', 'Mot de passe'],
            ];
            foreach ($users as $user) {
                $data[] = [
                    $user['active'] ? 'Oui' : 'Non',
                    $user['username'],
                    $user['civility'],
                    $user['lastname'],
                    $user['firstname'],
                    $user['email'],
                    $user['_matchingData']['Roles']['name'],
                    EnvvarService::escapeVarname('LIBRICIEL_RECETTE_KEYCLOAK_USER_PASSWORD_' . $user['username']),
                ];
            }
            $this->io->helper('Table')->output($data);
        }
    }

    /**
     * @inheritDoc
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->setLoggers(false);
        $this->io = $io;

        $query = $this->fetchTable('Organizations')
            ->find()
            ->select(['Organizations.id', 'Organizations.name'])
            ->orderAsc('Organizations.name');

        $structureId = $args->getArgument('structure_id');
        if (!empty($structureId)) {
            $query->matching('Structures', function ($q) use ($structureId) {
                return $q->where(['Structures.id' => $structureId]);
            });
        }

        $organizations = $query
            ->enableHydration(false)
            ->toArray();

        foreach ($organizations as $organization) {
            $this->io->out('');
            $this->io->out(sprintf('# Collectivité: %s', $organization['name']));

            $structures = $this->fetchTable('Structures')
                ->find()
                ->select(['Structures.id', 'Structures.business_name'])
                ->where([
                    'Structures.organization_id' => $organization['id'],
                    !empty($structureId)
                        ? ['Structures.id' => $structureId]
                        : '',
                ])
                ->orderAsc('Structures.business_name')
                ->enableHydration(false)
                ->toArray();

            foreach ($structures as $structure) {
                $this->io->out('');
                $this->io->out(sprintf('## Structure: %s', $structure['business_name']));

                $this->io->out('');
                $this->io->out('### Utilisateurs', 2);
                $this->users($structure['id']);

                $this->io->out('');
                $this->io->out('### Acteurs', 2);
                $this->actors($structure['id']);
            }
        }

        return static::CODE_SUCCESS;
    }

    /**
     * @inheritDoc
     */
    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription([
            'Commande permettant d\'afficher en console la liste des acteurs et la liste des utilisateurs de'
            . ' l\'ensemble des structures ou d\'une structure en particulier',
            '',
            'Pour traiter une structure en particulier, il faut passer son identifiant en paramètre.'
            ,
        ]);

        $parser->addArgument(
            'structure_id',
            [
                'help' => 'L\'identifiant de la structure (par défaut null, toutes structures)',
            ],
        );

        return $parser;
    }
}
