<?php
declare(strict_types=1);

namespace LibricielRecette\Model\Enum;

enum GenerateTemplateType
{
    public const ACT = 2;
    public const PROJECT = 1;
    public const CONVOCATION = 3;
    public const DELIBERATIONS_LIST = 5;
    public const VERBAL_TRIAL = 6;
    public const EXECUTIVE_SUMMARY = 4;
}
