Préambule
=========

Ce document décrit la procédure d'installation de l'application webACTES en production.
Ce guide d'installation-type est à destination de techniciens expérimentés.

Pré-Requis logiciels
====================

Les distributions Linux de référence sont :

Pour le serveur virtuel qui compose la plate-forme, Ubuntu server 18.04 LTS (plate-forme supportée et support long terme par la société Canonical Ltd.).
La configuration citée dans cette documentation doit être modifiée et adapté à vos choix.
Pendant l'installation, le serveur doit-être connecté sur un réseau relié à Internet afin de récupérer et installer les dernières mises-à-jour de composants logiciels disponibles.

Installation et configuration du serveur Applicatif
===================================================

Configuration des Locales
------------------------
TODO

Création des repertoires
----------------------
```shell
sudo mkdir /data
sudo mkdir /data/workspace
```

Mise en place de PostgreSQL
--------------
```shell
sudo apt-get install postgresql-10 postgresql-contrib
```

déplacer la bdd vers /data/postgresql au lieu de /var/lib/postgresql/..
```shell
sudo service postgresql stop
sudo cp -a /var/lib/postgresql /data
```

La faire pointer sur le nouveau répertoire
```shell
sudo vim /etc/postgresql/10/main/postgresql.conf

data_directory = '/data/postgresql/10/main'
```
Ecouter sur toutes les interfaces (devrait etre plus limité)
```shell
sudo vim /etc/postgresql/10/main/pg_hba.conf

host    all             all             0.0.0.0/0               md5
```

```shell
sudo vim /etc/postgresql/10/main/postgresql.conf

listen_addresses = '*'
```

Redémarer postgresl
```shell
sudo service postgresql restart
```

Création des bdd
```shell
sudo su postgres
psql -c "create user webactes"
psql -c "alter user webactes with login superuser createdb encrypted password 'secret'"
psql -c "create database webactes owner webactes"
psql -c "create database keycloak owner webactes"
psql -c "create database flowable owner webactes"
exit
```

Rotation des fichiers de Log
-----------
Mise en place des scriptes de rotation des logs dockers
```shell
sudo cp installation_ressources/logrotate/* /etc/logrotate.d/
```

Installation de docker et docker-compose
----------------

###Installer docker
```shell
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
sudo apt install docker-ce
```

Vérification de l'installation

```shell
sudo systemctl status docker
```

###Installer docker-compose
```shell
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

Vérification de l'installation
```shell
sudo docker-compose --version
```

[optionnel]Génération de certificat via let's encrypted
--------------------------------------
TODO Détail

docker run -it --rm --name certbot \
-v "/data/certbot/conf:/etc/letsencrypt" \
-v "/data/certbot/www:/var/www/certbot" \
-p 80:80 -p443:443 \
certbot/certbot


Récupération des sources
--------------------
Récupération des sources depuis ressource.libriciel.coop
```shell
sudo cd /data
sudo wget https://AdresseSourceWebActes.tar.gz
sudo tar -zxvf Installation.tar.gz 
```
Configuration de l'application avant démarrage
---------------------------------------------
#créer le .env
création du fichier d'environement et configuration

```shell
cp .env.dist .env
vim .env
```

Lancement des dockers
------------------------
Se logger au répository pour pouvoir télécharger les images
```shell
docker login gitlab.libriciel.fr:4567
```

Lancer le docker-compose
```shell
docker-compose up -d
```

Vérification du bon fonctionnement
```shell
docker-compose ps
```
Ne pas oublier de se deconnecter une fois les images docker récupérées
```shell
docker logout gitlab.libriciel.fr:4567
```

Ports de communication utilisés
----------------------------
L'application nécessite l'accès à certains ports situés à l'extérieur.

Voici la liste des autorisations a effectuer en cas d'utilisations de firewall  :

- HTTP 80 TCP - en entrée
- HTTPS 443 TCP - en entrée et sortie vers certains connecteurs
- SMTP 25 TCP -en sortie - envoi de mail
- SSH 22 TCP En entrée et sortie
