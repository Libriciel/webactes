FROM ubuntu:22.04 AS stage-apache

SHELL [ \
    "/bin/bash", \
    "-o", "errexit", \
    "-o", "nounset", \
    "-o", "pipefail", \
    "-c" \
]

# info instead of source /etc/apache2/envvars
ENV APACHE_CONFDIR=/etc/apache2
ENV APACHE_DOCUMENT_ROOT=/var/www/webACTES/webroot
ENV APACHE_ENVVARS=/etc/apache2/envvars
ENV APACHE_LOCK_DIR=/var/lock/apache2
ENV APACHE_LOG_DIR=/var/log/apache2
ENV APACHE_PID_FILE=/var/run/apache2/apache2.pid
ENV APACHE_RUN_DIR=/var/run/apache2
ENV APACHE_RUN_GROUP=www-data
ENV APACHE_RUN_USER=www-data

ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_VERSION=2.2.21
ENV DEB_PKG_SIEGFRIED=siegfried_1.9.1-1_amd64.deb
ENV DEBIAN_FRONTEND=noninteractive
ENV DIR_WA=/var/www/webACTES
ENV DIR_WORKSPACE=/data/workspace
ENV HTTP_USER=www-data
ENV HTTP_GROUP=www-data

ENV APACHE_DOCUMENT_ROOT "${DIR_WA}/webroot"

# @see docker-ressources/apt-policies.sh pour récupérer la liste exacte des versions des paquets
RUN apt-get update \
        --assume-yes \
        --quiet=2 \
    && apt-get install \
        --assume-yes \
        --no-install-recommends \
        --quiet=2 \
        apache2=2.4.52-1ubuntu4.9 \
        ca-certificates=20230311ubuntu0.22.04.1 \
        cron=3.0pl1-137ubuntu3 \
        curl=7.81.0-1ubuntu1.16 \
        git=1:2.34.1-1ubuntu1.11 \
        gnupg2=2.2.27-3ubuntu2.1 \
        libfreetype6-dev=2.11.1+dfsg-1ubuntu0.2 \
        libicu-dev=70.1-2 \
        libjpeg-dev=8c-2ubuntu10 \
        libonig-dev=6.9.7.1-2build1 \
        libpq-dev=14.12-0ubuntu0.22.04.1 \
        libssl-dev=3.0.2-0ubuntu1.16 \
        libxml2-dev=2.9.13+dfsg-1ubuntu0.4 \
        libzip-dev=1.7.3-1ubuntu2 \
        lsb-release=11.1.0ubuntu4 \
        nano=6.2-1 \
        php8.1-curl=8.1.2-1ubuntu2.18 \
        php8.1-intl=8.1.2-1ubuntu2.18 \
        php8.1-mbstring=8.1.2-1ubuntu2.18 \
        php8.1-pgsql=8.1.2-1ubuntu2.18 \
        php8.1-redis=5.3.5+4.3.0-5.1 \
        php8.1-soap=8.1.2-1ubuntu2.18 \
        php8.1-xdebug=3.1.2+2.9.8+2.8.1+2.5.5-4 \
        php8.1-xml=8.1.2-1ubuntu2.18 \
        php8.1-zip=8.1.2-1ubuntu2.18 \
        php8.1=8.1.2-1ubuntu2.18 \
        poppler-utils=22.02.0-2ubuntu0.4 \
        postgresql-client-14=14.12-0ubuntu0.22.04.1 \
        sudo=1.9.9-1ubuntu2.4 \
        supervisor=4.2.1-1ubuntu1 \
        unzip=6.0-26ubuntu3.2 \
        wget=1.21.2-2ubuntu1.1 \
        zip=3.0-12build2 \
        zlib1g-dev=1:1.2.11.dfsg-2ubuntu9.2 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && wget --no-check-certificate --no-verbose --quiet https://ressources.libriciel.fr/deploiement/s/${DEB_PKG_SIEGFRIED} --output-document=/tmp/${DEB_PKG_SIEGFRIED} \
    && dpkg -i /tmp/${DEB_PKG_SIEGFRIED} \
    && wget --no-check-certificate --no-verbose --quiet https://getcomposer.org/installer --output-document=/tmp/composer-installer.php \
    && php /tmp/composer-installer.php --filename composer --install-dir=/usr/local/bin --no-ansi --version ${COMPOSER_VERSION} \
    && echo "xdebug.mode = off" >> /etc/php/8.1/mods-available/xdebug.ini

RUN mkdir -p "${DIR_WORKSPACE}" \
    && chown -R ${HTTP_USER}:${HTTP_GROUP} "${DIR_WORKSPACE}"

# Si l'image est utilisée comme environnement de dev,
# On ne s'occupe pas des sources.

FROM stage-apache AS stage-no-entrypoint

RUN mkdir "${DIR_WA}"
COPY ./composer.json "${DIR_WA}"
COPY ./composer.lock "${DIR_WA}"

WORKDIR "${DIR_WA}"

COPY --chown=${HTTP_USER}:${HTTP_GROUP} . "${DIR_WA}"

RUN chmod +x "${DIR_WA}"/docker-ressources/*.sh \
    && "${DIR_WA}/docker-ressources/composer-install.sh"

COPY docker-ressources/apache-api.vhost /etc/apache2/sites-enabled/000-default.conf
RUN sed -i "1s/^/ServerName localhost\n/" /etc/apache2/apache2.conf \
    && sed -i ':a;N;$!ba; s/AllowOverride None/AllowOverride All/3' /etc/apache2/apache2.conf \
    && a2enmod rewrite

RUN chmod 777 "${DIR_WA}/bin/cake.php"

COPY docker-ressources/supervisord.conf /etc/supervisord.conf
COPY docker-ressources/supervisor/* /etc/supervisord.d/

COPY docker-ressources/webactes-apache-php.ini /etc/php/8.1/mods-available/
COPY docker-ressources/webactes-cli-php.ini /etc/php/8.1/mods-available/

RUN phpenmod -s apache2 webactes-apache-php \
    && phpenmod -s cli webactes-cli-php

FROM stage-no-entrypoint

ENTRYPOINT [ \
    "/var/www/webACTES/docker-ressources/initbdd.sh" , \
    "--composer-install", \
    "--deploy-flowable", \
    "--start", \
    "--useMain_URL" \
]
