# webACTES

## Installation webACTES client :

Si npm et nodejs avaient été déjà installés sur la machine, les désinstaller et installer les versions suivantes :
NodeJS v14.18 et npm v6.14 :

- ```sudo apt install curl```
- ```curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -```
- ```sudo apt install -y nodejs```
- ```node -v```
- ```sudo npm install npm --global```
- ```npm -v```

Angular cli v13.3.11 :

- ```npm i @angular/cli@13.3.11```

Cloner le projet : ```git clone git@gitlab.libriciel.fr:webACTES/angular-client.git```
Récupérer les dépendances : ```npm install```
S'il y a encore des soucis de version de node, supprimer le dossier node_modules du répertoire source et refaire un ```npm install```





