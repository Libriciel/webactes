(function(window) {
  window.env = window.env || {};

  // Environment variables
  window["env"]["apiUrl"] = "${API_URL}";
  window["env"]["debug"] = "${DEBUG}";
  window["env"]["keycloak.url"] = "${KEYCLOAK_URL}";
  window["env"]["keycloak.realm"] = "${KEYCLOAK_REALM}";
  window["env"]["keycloak.clientId"] = "${KEYCLOAK_CLIENT_ID}";
  window["env"]["keycloak.credentials.secret"] = "${KEYCLOAK_CREDENTIALS_SECRET}";
})(this);


