export const environment = {
  production: false,
  debug: true,
  apiUrl: '/api/v1'
};
