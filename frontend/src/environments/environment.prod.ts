const keycloakConfig: Keycloak.KeycloakConfig = {
  url: window['env']['keycloak.url'],
  realm: window['env']['keycloak.realm'],
  clientId: window['env']['keycloak.clientId'],
  // 'credentials': {
  //   'secret': window['env']['keycloak.credentials.secret']
  // }
};

export const environment = {
  production: true,
  debug: false,
  apiUrl: window['env']['apiUrl'] || '/api/v1',
  keycloak: keycloakConfig,
};
