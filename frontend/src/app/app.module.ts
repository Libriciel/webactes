import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { HttpClientModule } from '@angular/common/http';

import { NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFrenchParserFormatter } from './utility/NgbDateCustomParserFormatter';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { SittingsModule } from './sittings/sittings.module';
import { FormsModule } from '@angular/forms';
import { LogService } from './ls-common/services/http-services/log-service.service';
import { ICreateSittingService } from './sittings/services/iCreateSittingService';
import { CreateSittingService } from './ls-common/services/create-sitting.service';
import { IManageProjectsService } from './projects/services/i-manage-projects.service';
import { ManageProjectService } from './ls-common/services/manage-project.service';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IManageSittingService } from './sittings/services/iManageSittingService';
import { ManageSittingService } from './ls-common/services/manage-sitting.service';
import { LsComposantsModule } from '@libriciel/ls-composants';
import { WaCommonModule } from './wa-common/wa-common.module';
import { ActsModule } from './acts/acts.module';
import { ProjectsModule } from './projects/projects.module';
import { CircuitsModule } from './admin/circuits/circuits.module';
import { AdminModule } from './admin/admin.module';
import { IOrganizationsAndStructuresService } from './admin/organizations/serviceInterfaces/iorganizations-and-structures-service';
import { OrganizationsAndStructuresService } from './ls-common/services/organizations-and-structures.service';
import { keycloakInitialize } from './wa-common/initializers/keycloak-initializer';
import { ICreateCircuitService } from './admin/circuits/services/ICreateCircuitService';
import { ICreateStepService } from './admin/circuits/services/ICreateStepService';
import { CreateStepService } from './ls-common/services/create-step.service';
import { TypeActsModule } from './admin/type-acts/type-acts.module';
import { IManageTypeActService } from './admin/type-acts/services/IManageTypeActService';
import { RoleModule } from './admin/role/role.module';
import { IManageRoleService } from './admin/role/services/IManageRoleService';
import { ManageRoleService } from './ls-common/services/manage-role.service';
import { UsersModule } from './admin/users/users.module';
import { ManageUserService } from './ls-common/services/manage-user.service';
import { LoginModule } from './login/login.module';
import { ManageTypeActService } from './ls-common/services/manage-type-act.service';
import { IManageActsService } from './acts/services/imanage-acts-service';
import { ManageActsService } from './ls-common/services/manage-acts.service';
import { IManageCircuitService } from './admin/circuits/services/IManageCircuitService';
import { environment } from '../environments/environment';
import { IManageCircuitInstanceService } from './projects/services/i-manage-circuit-instances';
import { UserService } from './ls-common/services/http-services/user/User.service';
import { IManageStructureInformationService } from './admin/organization/services/i-manage-structure-information-service';
import { ManageStructureInformationService } from './ls-common/services/manage-structure-information-service';
import { IManageClassificationService } from './classification/services/i-manage-classification-service';
import { ManageClassificationService } from './ls-common/services/manage-classification.service';
import { CreateCircuitService } from './ls-common/services/create-circuit.service';
import { ManageCircuitInstanceService } from './ls-common/services/manage-circuit-instance.service';
import { IManageConnexionAdministrationService } from './admin/connexionPastell/services/imanage-connexion-administration-service';
import { ManageConnexionAdministrationService } from './ls-common/services/manage-connexion-administration.service';
import { IManageConnexionIdelibreService } from './admin/connexionIdelibre/services/imanage-connexion-idelibre-service';
import { ManageConnexionIdelibreService } from './ls-common/services/manage-connexion-idelibre.service';
import { IIParapheurService } from './projects/services/i-iparapheur-service';
import { ManageIParapheurService } from './ls-common/services/i-parapheur/manage-i-parapheur.service';
import { IManageUserService } from './admin/users/i-manage-user';
import { ErrorsService } from './errors/errors-service';
import { IManageNotificationsOptionsService } from './admin/notifications-options/services/imanage-notifications-options-service';
import { ManageNotificationsOptionsService } from './ls-common/services/notification-options/manage-notifications-options.service';
import { PreferencesModule } from './preferences/preferences.module';
import { AboutModule } from './about/about.module';
import { IManageApplicationVersionService } from './wa-common/application-about/services/imanage-application-version-service';
import { ManageApplicationVersionService } from './ls-common/services/manage-application-version.service';
import { ThemesModule } from './admin/theme/themes.module';
import { ManageThemeService } from './ls-common/services/manage-theme.service';
import { IManageThemeService } from './admin/theme/services/IManageThemeService';
import { IManageTypeSittingService } from './admin/type-sitting/services/IManageTypeSittingService';
import { TypeSittingModule } from './admin/type-sitting/type-sitting.module';
import { ManageTypeSittingsService } from './ls-common/services/manage-type-sittings-service';
import { SharedModule } from './shared';
import { CoreModule } from './core';
import { SequenceModule } from './admin/sequences/sequence.module';
import { IManageSequenceService } from './admin/sequences/services/IManageSequenceService';
import { ManageSequenceService } from './ls-common/services/manage-sequence.service';
import { CountersModule } from './admin/counters/counters.module';
import { IManageCountersService } from './admin/counters/services/IManageCountersService';
import { ManageCountersService } from './ls-common/services/manage-counter-service';
import { IManageNatureService } from './admin/type-acts/services/IManageNatureService';
import { NatureService } from './ls-common/services/http-services/nature.service';
import { SearchModule } from './search/search.module';
import { IManageSearchService } from './search/services/IManageSearchService';
import { ManageSearchService } from './ls-common/services/manage-search.service';
import { IManageDraftTemplateService } from './admin/type-acts/services/IManageDraftTemplateService';
import { IManageGenerateTemplateService } from './admin/type-acts/services/IManageGenerateTemplateService';
import { DraftTemplateService } from './ls-common/services/http-services/draft-template.service';
import { IManageVotesService } from './projects/services/i-manage-votes.service';
import { ManageVotesService } from './ls-common/services/manage-votes.service';
import { GenerateTemplateService } from './ls-common/services/http-services/generate-template.service';
import { IManageSummonService } from './model/summon/imanage-summon-service';
import { SummonService } from './ls-common/services/http-services/summon/summon-service';
import { IManageContainersSittingService } from './sittings/services/iManageContainersSittingService';
import { ManageContainerSittingService } from './ls-common/services/manage-container-sitting.service';
import { VoteModule } from './vote/vote.module';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    SharedModule,
    CoreModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    KeycloakAngularModule,
    SittingsModule,
    TypeSittingModule,
    ProjectsModule,
    ActsModule,
    CircuitsModule,
    AdminModule,
    PreferencesModule,
    AboutModule,
    TypeActsModule,
    RoleModule,
    UsersModule,
    LoginModule,
    NgbModule,
    BrowserAnimationsModule,
    LsComposantsModule,
    ThemesModule,
    SequenceModule,
    CountersModule,
    SearchModule,
    VoteModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      iconClasses: {
        error: 'toast-error',
        info: 'toast-info',
        success: 'toast-success',
        warning: 'toast-warning'
      },
      maxOpened: 5,
      //   preventDuplicates: true,
    }),
    AppRoutingModule,
    WaCommonModule,
  ],
  providers: [
    environment.debug ?
      []
      :
      {
        provide: APP_INITIALIZER,
        useFactory: keycloakInitialize,
        multi: true,
        deps: [KeycloakService, UserService]
      },
    {provide: LOCALE_ID, useValue: 'fr'},
    {provide: ICreateSittingService, useClass: CreateSittingService},
    {provide: IManageProjectsService, useClass: ManageProjectService},
    {provide: IManageSittingService, useClass: ManageSittingService},
    {provide: ICreateCircuitService, useClass: CreateCircuitService},
    {provide: IManageCircuitService, useExisting: ICreateCircuitService},
    {provide: IManageCircuitInstanceService, useClass: ManageCircuitInstanceService},
    {provide: ICreateStepService, useClass: CreateStepService},
    {provide: IManageActsService, useClass: ManageActsService},
    {provide: IOrganizationsAndStructuresService, useClass: OrganizationsAndStructuresService},
    {provide: IManageTypeActService, useClass: ManageTypeActService},
    {provide: IManageStructureInformationService, useClass: ManageStructureInformationService},
    {provide: NgbDateParserFormatter, useClass: NgbDateFrenchParserFormatter},
    {provide: IManageRoleService, useClass: ManageRoleService},
    {provide: IManageUserService, useClass: ManageUserService},
    {provide: NgbDateParserFormatter, useClass: NgbDateFrenchParserFormatter},
    {provide: IManageClassificationService, useClass: ManageClassificationService},
    {provide: IManageConnexionAdministrationService, useClass: ManageConnexionAdministrationService},
    {provide: IManageConnexionIdelibreService, useClass: ManageConnexionIdelibreService},
    {provide: IIParapheurService, useClass: ManageIParapheurService},
    {provide: IManageNotificationsOptionsService, useClass: ManageNotificationsOptionsService},
    {provide: IManageApplicationVersionService, useClass: ManageApplicationVersionService},
    {provide: IManageThemeService, useClass: ManageThemeService},
    {provide: IManageTypeSittingService, useClass: ManageTypeSittingsService},
    {provide: IManageSequenceService, useClass: ManageSequenceService},
    {provide: IManageCountersService, useClass: ManageCountersService},
    {provide: IManageNatureService, useClass: NatureService},
    {provide: IManageDraftTemplateService, useClass: DraftTemplateService},
    {provide: IManageGenerateTemplateService, useClass: GenerateTemplateService},
    {provide: IManageSearchService, useClass: ManageSearchService},
    {provide: IManageVotesService, useClass: ManageVotesService},
    {provide: IManageSummonService, useClass: SummonService},
    {provide: IManageContainersSittingService, useClass: ManageContainerSittingService},
    LogService,
    ErrorsService,
  ],
  bootstrap:
    [AppComponent],
  exports:
    []
})

export class AppModule {
}
