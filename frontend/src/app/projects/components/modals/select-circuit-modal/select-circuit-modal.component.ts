import { Component, Input, OnInit } from '@angular/core';
import { IManageCircuitService } from 'src/app/admin/circuits/services/IManageCircuitService';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Circuit } from 'src/app/model/circuit/circuit';
import { first } from 'rxjs/operators';
import { SelectCircuitModalMessages } from './select-circuit-modal-messages';
import { Project } from 'src/app/model/project/project';
import { IManageCircuitInstanceService } from '../../../services/i-manage-circuit-instances';
import { Weight } from '../../../../wa-common/weight.enum';
import { CommonMessages } from '../../../../ls-common/i18n/common-messages';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { ProjectsMessages } from '../../../i18n/projects-messages';

@Component({
  selector: 'wa-select-circuit-modal',
  templateUrl: './select-circuit-modal.component.html',
  styleUrls: ['./select-circuit-modal.component.scss']
})
export class SelectCircuitModalComponent implements OnInit {

  @Input() project: Project;
  messages = SelectCircuitModalMessages;
  commonMessages = CommonMessages;
  waCommonMessages = WACommonMessages;
  projectsMessages = ProjectsMessages;
  weight = Weight;
  circuitList: Circuit[] = [];
  selectedCircuit: Circuit;

  isProcessing = false;

  constructor(
    public activeModal: NgbActiveModal,
    protected circuitService: IManageCircuitService,
    protected circuitInstanceService: IManageCircuitInstanceService
  ) {
  }

  ngOnInit() {
    this.circuitService.getAllActiveCircuits().pipe(first()).subscribe(
      circuitsResult => {
        this.circuitList = circuitsResult.circuits;
        if (this.circuitList.length > 0) {
          this.selectedCircuit = this.circuitList[0];
        }
      }
    );
  }

  close(): void {
    this.activeModal.dismiss({isError: false});
  }

  selectCurrentCircuit() {
    if (this.selectedCircuit) {
      this.isProcessing = true;
      this.circuitInstanceService.sendProjectIntoCircuit(this.project, this.selectedCircuit)
        .subscribe(
          result => this.activeModal.close(result),
          err => this.activeModal.dismiss({isError: true, message: err})
        ).add(
        () => this.isProcessing = false
      );
    }
  }

}
