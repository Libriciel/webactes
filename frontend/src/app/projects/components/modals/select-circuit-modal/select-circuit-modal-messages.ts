export class SelectCircuitModalMessages {
  static TITLE = 'Sélectionner un circuit pour le projet';
  static SEND_TO_CIRCUIT_BTN_TXT = 'Envoyer dans le circuit';
  static SELECT_CIRCUIT_LABEL = 'Sélectionnez un circuit';
}
