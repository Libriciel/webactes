import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IIParapheurService } from '../../../../../services/i-iparapheur-service';
import { Project } from '../../../../../../model/project/project';
import { IParapheurCircuit } from '../../../../../../model/iparapheur/iparapheur-circuit';
import { ProjectsMessages } from '../../../../../i18n/projects-messages';
import { WACommonMessages } from '../../../../../../wa-common/i18n/wa-common-messages';
import { CommonMessages } from '../../../../../../ls-common/i18n/common-messages';
import { Style } from '../../../../../../wa-common/style.enum';

@Component({
  selector: 'wa-select-iparapheur-circuit',
  templateUrl: './select-iparapheur-circuit.component.html',
  styleUrls: ['./select-iparapheur-circuit.component.scss']
})
export class SelectIParapheurCircuitComponent implements OnInit {

  messages = ProjectsMessages;
  waCommonMessages = WACommonMessages;
  commonMessages = CommonMessages;
  style = Style;
  iParapheurCircuitList: IParapheurCircuit[] = null;
  selectedIParapheurCircuit: string;
  errorMessage = null;
  @Input() project: Project;
  @Output() changeValue = new EventEmitter<string>();

  constructor(protected iParapheurService: IIParapheurService) {
  }

  ngOnInit() {
    if (this.project) {
      this.iParapheurService.getIParapheurCircuits(this.project)
        .subscribe(value => {
          if (!value.error) {
            return this.iParapheurCircuitList = value;
          } else {
            this.errorMessage = value.error;
          }
        });
    }
  }

  onChange() {
    this.changeValue.emit(this.selectedIParapheurCircuit);
  }

  isValid(): boolean {
    return !!this.selectedIParapheurCircuit;
  }

  getValue(): string {
    return this.selectedIParapheurCircuit ? this.selectedIParapheurCircuit : null;
  }
}
