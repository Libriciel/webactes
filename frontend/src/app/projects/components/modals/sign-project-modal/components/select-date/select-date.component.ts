import { AfterViewInit, Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { NgbDate, NgbDateStruct, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { NgModel, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { ProjectsMessages } from '../../../../../i18n/projects-messages';
import { CommonIcons } from '../../../../../../ls-common';

@Component({
  selector: 'wa-select-date',
  templateUrl: './select-date.component.html',
  styleUrls: ['./select-date.component.scss']
})
export class SelectDateComponent implements AfterViewInit {

  @Output() changeValue = new EventEmitter<NgbDateStruct>();
  messages = ProjectsMessages;
  commonIcons = CommonIcons;
  @ViewChild('datePicker', {static: true}) datePicker: NgbInputDatepicker;
  @ViewChild('dateModel', {static: true}) dateModel: NgModel;

  form = new UntypedFormGroup({'dateModel': new UntypedFormControl()});

  selectedDate: NgbDateStruct = null;

  constructor() {
  }

  OnChange($event) {
    this.changeValue.emit($event);
  }

  getNowInDatepickerFormat(): NgbDateStruct {
    const now = new Date();
    return {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
  }

  isCalendarShowing(): boolean {
    return this.datePicker !== undefined && this.datePicker.isOpen();
  }

  isInFuture = (date: NgbDate) => date.after(this.getNowInDatepickerFormat());

  isValid(): boolean {
    return this.form.valid;
  }

  ngAfterViewInit(): void {
    this.selectedDate = this.getNowInDatepickerFormat();
    this.OnChange(this.selectedDate);
  }

}
