import {AfterViewChecked, ChangeDetectorRef, Component, Input, QueryList, ViewChildren} from '@angular/core';
import {NgbActiveModal, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {Project} from '../../../../model/project/project';
import {Config} from '../../../../Config';
import {WADocument} from '../../../model/wa-document';
import {CommonMessages, NotificationService} from '../../../../ls-common';
import {IManageProjectsService} from '../../../services/i-manage-projects.service';
import {Observable} from 'rxjs';
import {SelectDateComponent} from './components/select-date/select-date.component';
import {
  SelectIParapheurCircuitComponent
} from './components/select-iparapheur-circuit/select-iparapheur-circuit.component';
import {ManageIParapheurService} from '../../../../ls-common/services/i-parapheur/manage-i-parapheur.service';
import {FileUtils} from '../../../../utility/Files/file-utils';
import {ProjectsMessages} from '../../../i18n/projects-messages';
import {StructureSettingsService} from '../../../../core';

@Component({
    selector: 'wa-sign-project-modal',
    templateUrl: './sign-project-modal.component.html',
    styleUrls: ['./sign-project-modal.component.scss']
})
export class SignProjectModalComponent implements AfterViewChecked {
    commonMessages = CommonMessages;
    messages = ProjectsMessages;
    mainDocumentAcceptedTypesBeforeSigningString = this.projectService.getMainDocumentFileExtensionsBeforeSigning();
    @Input()
    project: Project;
    @Input()
    signingType: 'i-parapheur' | 'manual';
    @ViewChildren('signingInput')
    signingInput: QueryList<SelectDateComponent | SelectIParapheurCircuitComponent>;
    signingMethod: () => Observable<any>;
    valid = false;
    signingButtonMessage: string;
    signingButtonIcon: string;
    isProcessing = false;
    signingInputValue: NgbDateStruct | string;
    documentTypeError = false;
    config = Config;
    isActGenerate: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        protected notificationService: NotificationService,
        protected projectService: IManageProjectsService,
        protected iParapheurService: ManageIParapheurService,
        protected structureSettingsService: StructureSettingsService,
        protected changeDetectorRef: ChangeDetectorRef,
    ) {
    }

    ngAfterViewChecked(): void {
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            // Récupération de la configuration
            this.isActGenerate = this.structureSettingsService.getConfig('act', 'act_generate');
            this.documentTypeError = this.project.mainDocument
                && !FileUtils.fileHasAcceptableType(this.project.mainDocument, Config.mainDocumentAcceptedTypesForSigning);
            switch (this.signingType) {
                case 'manual':
                    this.signingMethod = this.manuallySign;
                    this.signingButtonMessage = this.messages.SIGN_PROJECT_MODAL_ACTION_BUTTON_TEXT;
                    this.signingButtonIcon = this.messages.SIGN_PROJECT_MODAL_ACTION_BUTTON_ICON;
                    break;
                case 'i-parapheur':
                    this.signingMethod = this.sendToIParapheur;
                    this.signingButtonMessage = this.messages.SEND_PROJECT_TO_I_PARAPHEUR_MODAL_ACTION_BUTTON_TEXT;
                    this.signingButtonIcon = this.messages.SEND_PROJECT_TO_I_PARAPHEUR_MODAL_ACTION_BUTTON_ICON;
                    break;
                default:
            }
            this.OnChange();
        });
    }

    addMainDoc(event) {
        const fileList = event.target.files;
        if (fileList && fileList.length <= 0) {
            return;
        }
        const file: File = fileList[0];
        if (!this.projectService.hasAcceptableMainDocumentTypeForSigning(file)) {
            this.notificationService.showError(
                this.messages.add_document_format(Config.mainDocumentAcceptedTypesForSigning, this.messages.MAIN_DOCUMENT),
                this.messages.ADD_DOCUMENT_TYPE_ERROR_NOTIFICATION_TITLE);
        } else {
            this.project.mainDocument = new WADocument({file: file, mimeType: file.type, size: file.size, name: file.name, documentType: this.project.mainDocument.documentType});
            if (!this.projectService.checkDocumentsSize(this.project)) {
                this.notificationService.showError(this.messages.too_big(Config.MAX_PROJECT_SIZE));
            }
        }
        this.OnChange();
    }

    OnChange() {

        this.documentTypeError = this.project.mainDocument
            && !FileUtils.fileHasAcceptableType(this.project.mainDocument, Config.mainDocumentAcceptedTypesForSigning);
        this.valid = (!!this.project.mainDocument) &&
            !this.documentTypeError &&
            this.projectService.checkDocumentsSize(this.project) &&
            this.signingInput && this.signingInput.first.isValid();
    }

    signMethodCall() {
        this.isProcessing = true;
        this.signingMethod().subscribe(
            () => {
                return this.activeModal.close();
            },
            () => {
                this.isProcessing = false;
                this.notificationService.showError(this.messages.SIGN_PROJECT_MODAL_ERROR);
            });
    }

    private manuallySign(): Observable<any> {
        const date = this.signingInputValue as NgbDateStruct;
        return this.projectService.manuallySignProject(this.project, new Date(date.year, date.month - 1, date.day));
    }

    private sendToIParapheur(): Observable<any> {
        const circuitNumberName = this.signingInputValue as string;
        return this.iParapheurService.sendToIParapheur(this.project, circuitNumberName);
    }

    public changeSigningInputValue(value: string | NgbDateStruct) {
        this.signingInputValue = value;
        this.OnChange();
    }
}
