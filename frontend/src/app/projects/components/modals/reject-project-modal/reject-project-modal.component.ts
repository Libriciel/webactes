import { Component, OnInit } from '@angular/core';
import { ValidateStepModalComponent } from '../validate-step-modal/validate-step-modal.component';
import { Observable } from 'rxjs';
import { RejectProjectModalMessages } from './reject-project-modal-messages';
import { Style } from '../../../../wa-common/style.enum';
import { CommonIcons } from '../../../../ls-common/icons/common-icons';

@Component({
  selector: 'wa-reject-project-modal',
  templateUrl: '../validate-step-modal/validate-step-modal.component.html',
  styleUrls: ['../validate-step-modal/validate-step-modal.component.scss']
})
export class RejectProjectModalComponent extends ValidateStepModalComponent implements OnInit {

  ngOnInit() {
    this.messages = RejectProjectModalMessages;
    this.commentIsRequired = true;
    this.confirmBtnIcon = CommonIcons.REJECTED_ICON;
    this.style = Style.DANGER;
  }

  getAction$(): Observable<any> {
    return this.projectService.rejectProject(this.project, this.comment);
  }
}
