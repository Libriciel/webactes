export class RejectProjectModalMessages {
  static TITLE = 'Refus du projet';
  static ACTION_DESCRIPTION = 'Le projet sera renvoyé au rédacteur';
  static CONFIRM_BTN_TITLE = 'Refuser';
  static COMMENT_LABEL = 'Commentaire';
}
