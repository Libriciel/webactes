import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidateStepModalMessages } from './validate-step-modal-messages';
import { IManageProjectsService } from '../../../services/i-manage-projects.service';
import { Project } from 'src/app/model/project/project';
import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Weight } from '../../../../wa-common/weight.enum';
import { Style } from '../../../../wa-common/style.enum';
import { CommonMessages } from '../../../../ls-common/i18n/common-messages';
import { CommonIcons } from '../../../../ls-common/icons/common-icons';

@Component({
  selector: 'wa-validate-step-modal',
  templateUrl: './validate-step-modal.component.html',
  styleUrls: ['./validate-step-modal.component.scss']
})
export class ValidateStepModalComponent implements OnInit {

  @Input()
  project: Project;
  messages = ValidateStepModalMessages;
  commonMessages = CommonMessages;
  weight = Weight;
  comment: string;
  isProcessing = false;
  commentIsRequired = false;
  confirmBtnIcon = CommonIcons.VALIDATED_ICON;
  style: Style = Style.NORMAL;

  constructor(
    public activeModal: NgbActiveModal,
    protected projectService: IManageProjectsService,
  ) {
  }

  ngOnInit() {
    this.messages = ValidateStepModalMessages;
  }

  close(): void {
    this.activeModal.dismiss({isError: false});
  }

  confirmBtnIsDisabled() {
    return this.commentIsRequired ? !(this.comment) : false;
  }

  confirmAction() {
    this.isProcessing = true;

    const action$ = this.getAction$();
    action$.pipe(first())
      .subscribe(
        res => this.activeModal.close(res),
        () => this.activeModal.close({success: false})
      )
      .add(
        () => this.isProcessing = false
      );
  }

  getAction$(): Observable<any> {
    return this.projectService.validateProject(this.project, this.comment);
  }
}
