export class ValidateStepModalMessages {
  static TITLE = 'Validation du projet';
  static ACTION_DESCRIPTION = `Le projet sera envoyé à l'étape suivante`;
  static CONFIRM_BTN_TITLE = 'Valider';
  static COMMENT_LABEL = 'Commentaire';
}
