import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EditProjectAction } from '../../projects-list/actions/edit-project-action';
import { Router } from '@angular/router';
import { Project } from '../../../../model/project/project';
import { Style } from '../../../../wa-common/style.enum';
import { Weight } from '../../../../wa-common/weight.enum';
import { ProjectsMessages } from '../../../i18n/projects-messages';

@Component({
  selector: 'wa-iparapheur-refusal-modal',
  templateUrl: './iparapheur-refusal-modal.component.html',
  styleUrls: ['./iparapheur-refusal-modal.component.scss']
})
export class IParapheurRefusalModalComponent implements OnInit {

  messages = ProjectsMessages;
  styles = Style;
  weight = Weight;
  project: Project;
  refusalText = '';

  constructor(
    public activeModal: NgbActiveModal,
    protected router: Router,
    protected editProjectAction: EditProjectAction,
  ) {
  }

  ngOnInit() {
  }

  onModifyProject() {
    this.activeModal.close();
    this.editProjectAction.action([this.project]);
  }

  onCloseProject() {
    this.activeModal.close();
  }
}
