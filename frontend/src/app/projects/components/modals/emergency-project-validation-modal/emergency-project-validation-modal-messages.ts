export class EmergencyProjectValidationModalMessages {
  static TITLE = 'Validation en urgence du projet';
  static ACTION_DESCRIPTION = 'Le projet sera validé sans passer par les étapes suivantes de validation';
  static CONFIRM_BTN_TITLE = 'Valider en urgence';
  static COMMENT_LABEL = 'Commentaire';
}
