import { Component, OnInit } from '@angular/core';
import { ValidateStepModalComponent } from '../validate-step-modal/validate-step-modal.component';
import { EmergencyProjectValidationModalMessages } from './emergency-project-validation-modal-messages';
import { Observable } from 'rxjs';
import { Style } from '../../../../wa-common/style.enum';
import { ProjectsMessages } from '../../../i18n/projects-messages';

@Component({
  selector: 'wa-emergency-project-validation-modal',
  templateUrl: '../validate-step-modal/validate-step-modal.component.html',
  styleUrls: ['../validate-step-modal/validate-step-modal.component.scss']
})
export class EmergencyProjectValidationModalComponent extends ValidateStepModalComponent implements OnInit {

  ngOnInit() {
    this.messages = EmergencyProjectValidationModalMessages;
    this.style = Style.WARNING;
    this.confirmBtnIcon = ProjectsMessages.EMERGENCY_VALIDATION_BUTTON_ICON;
  }

  getAction$(): Observable<any> {
    return this.projectService.validateProjectInEmergency(this.project, this.comment);
  }

}
