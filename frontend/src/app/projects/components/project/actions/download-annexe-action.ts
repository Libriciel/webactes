import { Observable } from 'rxjs';
import { Annexe } from '../../../model/annexe';
import { IManageProjectsService } from '../../../services/i-manage-projects.service';
import { ActionResult } from '../../../../ls-common/components/tables/actuator/action-result';
import { map } from 'rxjs/operators';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';

export class DownloadAnnexeAction implements IActuator<Annexe> {

  constructor(protected  iManageProjectService: IManageProjectsService) {
  }

  action(annexes: Annexe[]): Observable<ActionResult> {
    const annexe = annexes[0];
    return this.iManageProjectService.getFile(annexe.fileId, annexe.mimeType, annexe.name)
      .pipe(map(() => ({needReload: false} as ActionResult)));
  }
}
