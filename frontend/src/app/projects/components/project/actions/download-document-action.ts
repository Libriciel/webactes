import { Observable, of } from 'rxjs';
import { IManageProjectsService } from '../../../services/i-manage-projects.service';
import { ActionResult } from '../../../../ls-common';
import { catchError, map } from 'rxjs/operators';
import { DownloadableDocument } from 'src/app/model/downloadable-document/downloadable-document';
import { TDTDocumentType } from 'src/app/model/downloadable-document/tdt-document-type.enum';
import { IParapheurDocumentType } from '../../../../model/iparapheur/iparapheur-document-type.enum';
import { IActuator } from '../../../../ls-common';
import { ProjectDocumentTypeEnum } from '../../../../model/downloadable-document/project-document-type.enum';

export class DownloadDocumentAction implements IActuator<DownloadableDocument> {

  constructor(protected projectService: IManageProjectsService) {
  }

  action(tdtDocList: DownloadableDocument[]): Observable<ActionResult> {
    if (!tdtDocList || !tdtDocList.length) {
      return of({});
    }
    let action$;
    const downloadableDocument = tdtDocList[0];
    const TDT_TYPES = TDTDocumentType;

    switch (downloadableDocument.type) {
      case TDT_TYPES.ACT:
        action$ = this.projectService.getStampedAct(downloadableDocument.projectId, downloadableDocument.mimeType,
          downloadableDocument.fileName);
        break;
      case TDT_TYPES.ACT_AR:
        action$ = this.projectService.getActAR(downloadableDocument.projectId, downloadableDocument.mimeType,
          downloadableDocument.fileName);
        break;
      case TDT_TYPES.BORDEREAU:
        action$ = this.projectService.getBordereau(downloadableDocument.projectId, downloadableDocument.mimeType,
          downloadableDocument.fileName);
        break;
      case TDT_TYPES.ANNEX:
        action$ = this.projectService.getStampedAnnex(downloadableDocument.annexId, downloadableDocument.projectId,
          downloadableDocument.mimeType, downloadableDocument.fileName);
        break;
      case IParapheurDocumentType.FORM:
        action$ = this.projectService.getIParapheurSignedFormDocument(downloadableDocument.projectId,
          downloadableDocument.mimeType, downloadableDocument.fileName);
        break;
      case IParapheurDocumentType.HISTORY:
        action$ = this.projectService.getIParapheurHistory(downloadableDocument.projectId,
          downloadableDocument.mimeType, downloadableDocument.fileName);
        break;
      case IParapheurDocumentType.MAIN_DOCUMENT:
        action$ = this.projectService.getIParapheurSignedMainDocument(downloadableDocument.projectId, downloadableDocument.mimeType,
          downloadableDocument.fileName);
        break;
      case ProjectDocumentTypeEnum.PROJECT:
        action$ = this.projectService.getDownloadProject(downloadableDocument.projectId, downloadableDocument.mimeType,
          downloadableDocument.fileName);
        break;
      case ProjectDocumentTypeEnum.ACT:
        action$ = this.projectService.getDownloadAct(downloadableDocument.projectId, downloadableDocument.mimeType,
          downloadableDocument.fileName);
        break;
      default: {
        action$ = of({});
      }
    }

    return action$.pipe(
      map(() => ({needReload: false} as ActionResult)),
      catchError(err => of({error: true, needReload: false, message: err}))
    );

  }
}
