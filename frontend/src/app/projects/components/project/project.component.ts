import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IManageProjectsService } from '../../services/i-manage-projects.service';
import { Project } from '../../../model/project/project';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { Annexe } from '../../model/annexe';
import { ActionItem, CommonIcons, CommonMessages, IActuator } from '../../../ls-common';
import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { DownloadAnnexeAction } from './actions/download-annexe-action';
import { WADocument } from '../../model/wa-document';
import { NgbAccordion, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { StateActCode } from 'src/app/model/state-act-code.enum';
import { DownloadDocumentAction } from './actions/download-document-action';
import { DownloadableDocument } from 'src/app/model/downloadable-document/downloadable-document';
import { TDTDocumentType } from 'src/app/model/downloadable-document/tdt-document-type.enum';
import { ProjectAvailableActions } from '../../../model/project/project-available-actions.enum';
import { BreadcrumbItem } from '../../../wa-common/model/breadcrumb-item';
import { IParapheurDocumentType } from '../../../model/iparapheur/iparapheur-document-type.enum';
import { ProjectActionItemFactory } from '../projects-list/actions/project-action-item-factory';
import { ProjectAction } from '../projects-list/actions/project-action.enum';
import { of } from 'rxjs';
import { Style } from '../../../wa-common';
import { ProjectDisplayState } from '../../../model/project/project-display-state.enum';
import { ShowIparapheurRefusalAction } from '../projects-list/actions/show-iparapheur-refusal-action';
import { ActActionItemFactory } from '../../../acts/components/acts-list/actions/act-action-item-factory';
import { ActAction } from '../../../acts/components/acts-list/actions/act-action.enum';
import { Weight } from '../../../wa-common/weight.enum';
import { ProjectsMessages } from '../../i18n/projects-messages';
import { StructureSettingsService } from '../../../core';
import { delay } from 'rxjs/operators';
import { ProjectHistoryElementType } from '../../../model/project/project-history-element-type.enum';
import { ActionSet } from '../../../ls-common/model/action-set';

@Component({
  selector: 'wa-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  tdtDocumentsList;
  iParapheurDocumentList;
  messages = ProjectsMessages;
  weight = Weight;
  styles = Style;
  @ViewChild('documentAccordion', {static: true}) documentAccordion: NgbAccordion;
  @Input() goto = RoutingPaths.PROJECTS_PATH;
  originalContext: { goto: string, breadcrumb: BreadcrumbItem[] };
  StateActCode = StateActCode;
  singleActions: ActionItemsSet<Annexe> =
    {
      actionItems: [new ActionItem({
        name: this.messages.DOWNLOAD_ANNEXE,
        icon: this.messages.DOCUMENT_DOWNLOAD_ICON,
        actuator: new DownloadAnnexeAction(this.iManageProjectService)
      })]
    };
  downloadDocumentActions: ActionItemsSet<DownloadableDocument> =
    {
      actionItems: [new ActionItem({
        name: this.messages.DOWNLOAD_DOC,
        icon: this.messages.DOCUMENT_DOWNLOAD_ICON,
        actuator: new DownloadDocumentAction(this.iManageProjectService)
      })]
    };

  showCheckIParapheurRefusalBtn = false;
  secondaryActionsItems: ActionSet<Project> = {actions: []};
  primaryActionsItems: ActionSet<Project> = {actions: []};
  otherActionSItems: ActionSet<Project> = {actions: []};
  project: Project = null;
  documentsParapheurPanelId = 'documents-parapheur';
  documentsTDTPanelId = 'documents-TDT';
  documentsOriginalPanelId = 'documents-original';
  public commonMessages = CommonMessages;
  public mainDownloading = false;
  sittingEnable: boolean | undefined;
  show: boolean;
  hasProblem: { value: boolean, message: string } = {value: false, message: ''};

  constructor(protected route: ActivatedRoute,
              protected iManageProjectService: IManageProjectsService,
              protected notificationService: NotificationService,
              protected router: Router,
              protected modalService: NgbModal,
              protected projectActionItemFactory: ProjectActionItemFactory,
              private actActionItemFactory: ActActionItemFactory,
              private showIparapheurRefusalAction: ShowIparapheurRefusalAction,
              protected structureSettingsService: StructureSettingsService) {
    this.goto = this.router.getCurrentNavigation().extras.state
      ? this.router.getCurrentNavigation().extras.state.goto
      : RoutingPaths.PROJECTS_PATH;
    if (this.router.getCurrentNavigation().extras.state
      && this.router.getCurrentNavigation().extras.state.originalContext) {
      this.originalContext = this.router.getCurrentNavigation().extras.state.originalContext;
      if (!this.goto) {
        this.goto = this.originalContext.goto;
      }
    }
  }

  ngOnInit() {
    this.show = false;
    this.route.data.subscribe(data => {
      this.project = data.project;
      !this.project.mainDocument.fileId
        ? this.hasProblem = {value: true, message: 'En cours de génération'}
        : this.hasProblem = {value: false, message: ''};
      this.initState(this.project.canonicalProjectDisplayState);
    });
    this.sittingEnable = this.structureSettingsService.getConfig('sitting', 'sitting_enable');
  }

  cancel() {
    if (this.originalContext && this.originalContext.goto !== this.goto) {
      this.router.navigateByUrl(this.goto, {state: {originalContext: this.originalContext}}).then();
    } else {
      this.router.navigateByUrl(this.goto).then();
    }
  }

  getOpenedAccordion(): string {
    if (this.tdtDocumentsList) {
      return this.documentsTDTPanelId;
    } else {
      if (this.iParapheurDocumentList) {
        return this.documentsParapheurPanelId;
      } else {
        return this.documentsOriginalPanelId;
      }
    }
  }

  showIParapheurRefusalPopup() {
    this.showIparapheurRefusalAction.action([this.project]).subscribe();
  }

  downloadMainDocument(waDocument: WADocument) {
    if (!this.mainDownloading) {
      this.mainDownloading = true;
      this.iManageProjectService.getFile(waDocument.fileId, waDocument.mimeType, waDocument.name)
        .subscribe(() => this.mainDownloading = false);
    }
  }

  private initState(state: ProjectDisplayState) {
    this.eraseActions();
    if (this.project.isSignedIParapheur) {
      this.initParapheurDocumentList();
    }
    this.secondaryActionsItems.actions.push(new ActionItem<Project>({
      name: CommonMessages.GO_BACK,
      icon: CommonIcons.GO_BACK_ICON,
      actuator: new class implements IActuator<Project> {
        constructor(private router: Router,
                    private goto: string,
                    private originalContext: { goto: string, breadcrumb: BreadcrumbItem[] }) {
        }

        action() {
          if (this.originalContext && this.originalContext.goto !== this.goto) {
            this.router.navigateByUrl(this.goto, {state: {originalContext: this.originalContext}}).then();
          } else {
            this.router.navigateByUrl(this.goto).then();
          }
          return of({});
        }
      }(this.router, this.goto, this.originalContext)
    }));

    this.secondaryActionsItems.actions.push(
      this.projectActionItemFactory
        .getActionItem(ProjectAction.EDIT_PROJECT, this.originalContext ? this.originalContext.breadcrumb : undefined));
    this.otherActionSItems.actions.push(this.projectActionItemFactory.getActionItem(ProjectAction.DELETE_PROJECT));

    switch (state) {
      case ProjectDisplayState.DRAFT: {
        this.primaryActionsItems.actions.push(this.projectActionItemFactory.getActionItem(ProjectAction.SEND_PROJECT_TO_CIRCUIT));
        break;
      }

      case ProjectDisplayState.VALIDATING:
      case ProjectDisplayState.TO_BE_VALIDATED: {
        if (this.project.hasAvailableAction(ProjectAvailableActions.canBeValidatedInCircuit)) {
          this.primaryActionsItems.actions.push(this.projectActionItemFactory.getActionItem(ProjectAction.VALIDATE_STEP));
          this.secondaryActionsItems.actions.push(this.projectActionItemFactory.getActionItem(ProjectAction.REJECT_PROJECT));
        }
        if (this.project.hasAvailableAction(ProjectAvailableActions.canValidateInEmergency)) {
          this.primaryActionsItems.actions.push(this.projectActionItemFactory.getActionItem(ProjectAction.VALIDATE_IN_EMERGENCY));
        }
        break;
      }

      case ProjectDisplayState.VALIDATED:
      case ProjectDisplayState.VOTED: {
        this.showCheckIParapheurRefusalBtn = this.project.hasAvailableAction(ProjectAvailableActions.isIParapheurRejected);
        if (this.project.hasAvailableAction(ProjectAvailableActions.canBeSendToIParapheur)) {
          this.primaryActionsItems.actions.push(this.projectActionItemFactory.getActionItem(ProjectAction.SEND_TO_SIGN_I_PARAPHEUR));
        }
        if (this.project.hasAvailableAction(ProjectAvailableActions.canBeSendToManualSignature)) {
          this.primaryActionsItems.actions.push(this.projectActionItemFactory.getActionItem(ProjectAction.SIGN_PROJECT));
        }
        if (this.project.hasAvailableAction(ProjectAvailableActions.canCheckIParapheurStatus)) {
          this.primaryActionsItems.actions.push(this.projectActionItemFactory.getActionItem(ProjectAction.CHECK_IPARAPHEUR_STATUS));
        }
      }
        break;

      case ProjectDisplayState.TO_BE_TRANSMITTED:
        this.primaryActionsItems.actions.push(this.actActionItemFactory.getActionItem(ActAction.CHECK_AND_SEND_TO_TDT));
        break;

      case ProjectDisplayState.TRANSMISSION_READY:
        this.primaryActionsItems.actions.push(this.actActionItemFactory.getActionItem(ActAction.ORDER_TELETRANSMISSION));
        break;

      case ProjectDisplayState.ACT: {
        this.initTdtDocumentList();
        break;
      }
    }

    this.show = true;
  }

  private initParapheurDocumentList() {
    if (!this.project || !this.project.mainDocument || !this.project.isSignedIParapheur) {
      return;
    }
    const projectId = this.project.id;
    const mainFileName = this.project.mainDocument.name ? this.project.mainDocument.name : ProjectsMessages.MAIN_DOCUMENT;
    this.iParapheurDocumentList = [
      new DownloadableDocument(projectId, IParapheurDocumentType.MAIN_DOCUMENT, mainFileName),
      new DownloadableDocument(projectId, IParapheurDocumentType.FORM, ProjectsMessages.FORM_DEFAULT_FILE_NAME),
      new DownloadableDocument(projectId, IParapheurDocumentType.HISTORY, ProjectsMessages.HISTORY_DEFAULT_FILE_NAME,
        ProjectsMessages.XML_FILE_TYPE),
    ];
  }

  private initTdtDocumentList() {
    if ((!this.project || !this.project.mainDocument)
      || !this.project.history.find(history => history.type === ProjectHistoryElementType.ACKNOWLEDGMENT_RECEIVED)) {
      return;
    }
    const projectId = this.project.id;
    const mainFileName = this.project.mainDocument.name ? this.project.mainDocument.name : ProjectsMessages.MAIN_DOCUMENT;
    this.tdtDocumentsList = [
      new DownloadableDocument(projectId, TDTDocumentType.ACT, mainFileName),
      new DownloadableDocument(projectId, TDTDocumentType.BORDEREAU, ProjectsMessages.FORM_DEFAULT_FILE_NAME),
      new DownloadableDocument(projectId, TDTDocumentType.ACT_AR, ProjectsMessages.ACKNOWLEDGEMENT_DEFAULT_FILE_NAME,
        ProjectsMessages.XML_FILE_TYPE),
    ];
  }

  private updateProject(event, oldState: ProjectDisplayState) {
    this.iManageProjectService.getProject(this.project.id).pipe(delay(2)).subscribe({
      error: () => {
        if (event.message.length) {
          if (event.error) {
            this.notificationService.showError(event.message);
          } else {
            this.notificationService.showSuccess(event.message);
            this.router.navigateByUrl(this.goto, {state: {originalContext: this.originalContext}}).then();
          }
        }
      },
      complete: () => {
        if (event.message.length && oldState === this.project.canonicalProjectDisplayState) {
          this.eraseActions();
          this.notificationService.showSuccess(event.message);
        } else if (event.message.length) {
          this.initState(this.project.canonicalProjectDisplayState);
          this.notificationService.showSuccess(event.message);
        }
      },
      next: result => {
        this.project = result;
      }
    });
  }

  fillComponent(event) {
    if (event && event.message) {
      this.show = false;
      const oldState = this.project.canonicalProjectDisplayState;
      this.updateProject(event, oldState);
    }
  }

  private eraseActions() {
    this.primaryActionsItems.actions = [];
    this.secondaryActionsItems.actions = [];
    this.otherActionSItems.actions = [];
  }
}
