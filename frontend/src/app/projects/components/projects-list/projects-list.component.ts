import { Component, OnInit } from '@angular/core';
import { ProjectsListMessages } from './projects-list-messages';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { ActivatedRoute, Router } from '@angular/router';
import { ManageProjectService } from '../../../ls-common/services/manage-project.service';
import { ActionProjectStateMapping } from './actions/action-project-state-mapping';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractProjectListComponent } from './abstract-project-list-component';
import { ProjectActionItemFactory } from './actions/project-action-item-factory';
import { ProjectAction } from './actions/project-action.enum';
import { ProjectsBreadcrumb } from '../../routing/projects-breadcrumb';

@Component({
  selector: 'wa-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss']
})
export class ProjectsListComponent extends AbstractProjectListComponent implements OnInit {

  messages = ProjectsListMessages;

  routingPaths = RoutingPaths;
  ProjectAction = ProjectAction;
  ProjectsBreadcrumb = ProjectsBreadcrumb;

  constructor(
    public modalService: NgbModal,
    public router: Router,
    protected projectService: ManageProjectService,
    protected route: ActivatedRoute,
    protected actionProjectStateMap: ActionProjectStateMapping,
    public projectActionItemFactory: ProjectActionItemFactory,
  ) {
    super(projectService, route);
  }

  ngOnInit() {
    super.ngOnInit();
    this.route.data.subscribe(data => {
      if (data.projectState) {
        this.singleActions = this.actionProjectStateMap.getSingleActions(data.projectState);
        this.otherSingleActions = this.actionProjectStateMap.getOtherSingleActions(data.projectState);
        this.commonActions = this.actionProjectStateMap.getCommonActions(data.projectState);
      }
    });
  }
}
