import { Component, OnInit } from '@angular/core';
import { Project } from '../../../model/project/project';
import { Pagination } from '../../../model/pagination';
import { ActivatedRoute } from '@angular/router';
import { ManageProjectService } from '../../../ls-common/services/manage-project.service';
import { ProjectDisplayState } from 'src/app/model/project/project-display-state.enum';
import { SortUtils } from '../../../utility/sort-utils';
import { StatesAct } from 'src/app/model/states-act';
import { StateIconsUtils } from '../../services/state-icons-utils';
import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { CommonIcons, CommonMessages } from '../../../ls-common';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';

@Component({
  template: ''
})
export abstract class AbstractProjectListComponent implements OnInit {

  projects: Project[];
  displayState: ProjectDisplayState;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};
  projectDisplayState = ProjectDisplayState;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  waCommonMessages = WACommonMessages;
  filterString: string;
  loading = true;
  singleActions: ActionItemsSet<Project>;
  otherSingleActions: ActionItemsSet<Project>[];
  commonActions: ActionItemsSet<Project>[];

  protected constructor(protected projectService: ManageProjectService,
                        protected route: ActivatedRoute) {
  }

  ngOnInit() {

    this.route.data.subscribe(data => {
      this.displayState = data.projectState ? data.projectState : '';
      this.reloadProjects();
    });
  }

  reloadProjects() {
    this.gotoPage(1);
  }

  gotoPage(numPage) {
    const filters = this.filterString ? {searchedText: this.filterString} : null;
    this.loading = true;
    this.projects = null;this.projectService.getProjects(this.displayState, numPage, filters)
      .subscribe((projects: { projects: Project[], pagination: Pagination }) => {
        this.projects = projects.projects.sort(SortUtils.sortRecentFirst);
        this.pagination = projects.pagination;
        this.loading = false;
      });
  }

  applyFilter(event) {
    this.filterString = event.trim();
    this.reloadProjects();
  }

  getIconForState(state: StatesAct): string {
    return StateIconsUtils.getIconClassForState(state);
  }

  hasFilter(): boolean {
    return this.filterString && this.filterString !== '';
  }
}
