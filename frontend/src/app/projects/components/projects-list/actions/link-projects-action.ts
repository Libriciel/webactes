import { Observable, of } from 'rxjs';
import { Project } from '../../../../model/project/project';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';

export class LinkProjectsAction implements IActuator<Project> {

  action(projects: Project[]): Observable<any> {
    // eslint-disable-next-line no-console
    console.debug('Associer les projets : ' + projects.map(project => project.id));
    return of({});
  }
}
