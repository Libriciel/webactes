import { Observable, of } from 'rxjs';
import { Project } from '../../../../model/project/project';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmPopupComponent } from '../../../../ls-common/components/confirm-popup/components/confirm-popup.component';
import { IIParapheurService } from '../../../services/i-iparapheur-service';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';

@Injectable({
  providedIn: 'root'
})
export class CheckIparapheurStatusAction implements IActuator<Project> {

  messages = ProjectsMessages;

  constructor(protected modalService: NgbModal, protected iparapheurService: IIParapheurService) {
  }

  action(projects: Project[]): Observable<any> {

    if (projects.length > 1) {
      console.error('CheckIparapheurStatusAction - Multiple projects not handled!');
    }

    if (projects.length < 1) {
      console.error('CheckIparapheurStatusAction - no target project');
      return of({});
    }

    return this.iparapheurService.getIParapheurStatus(projects[0]).pipe(
      map(value => {
        const modalRef = this.modalService.open(ConfirmPopupComponent, {
          backdrop: 'static',
          keyboard: false,
          size: 'lg',
          centered: true
        });
        modalRef.componentInstance.title = this.messages.CHECK_IPARAPHEUR_STATUS_MODAL_TITLE;
        modalRef.componentInstance.type = 'info';
        modalRef.componentInstance.content = value ? `[${value.status}] ${value.annotation}` : '';
        modalRef.componentInstance.confirmMsg = this.messages.CHECK_IPARAPHEUR_STATUS_MODAL_BUTTON_TITLE;
        modalRef.componentInstance.cancelButton = false;
        return of(modalRef.result.then(
          () => {
          }
          ).catch(error => {
            if (error.isError === undefined || error.isError) {
              console.error('Caught error in popup for check IParapheur status : ', error);
            }
          })
        );
      })
    );
  }
}
