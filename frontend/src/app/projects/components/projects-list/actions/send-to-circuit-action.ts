import { from, Observable, of } from 'rxjs';
import { Project } from '../../../../model/project/project';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectCircuitModalComponent } from '../../modals/select-circuit-modal/select-circuit-modal.component';
import { ActionResult } from 'src/app/ls-common/components/tables/actuator/action-result';
import { Injectable } from '@angular/core';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { IActuator } from '../../../../ls-common';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SendToCircuitAction implements IActuator<Project> {

  messages = ProjectsMessages;

  constructor(protected modalService: NgbModal) {
  }

  action(projects: Project[]): Observable<any> {
    if (projects.length > 1) {
      console.error('SendToCircuitAction - Multiple projects not handled!');
    }

    if (projects.length < 1) {
      console.error('SendToCircuitAction - no target project');
      return of({});
    }

    const modal = this.modalService.open(SelectCircuitModalComponent);
    modal.componentInstance.project = projects[0];

    return from(modal.result).pipe(
      map(() => {
        return {
          error: false,
          needReload: true,
          message: this.messages.PROJECT_SENT_TO_CIRCUIT_SUCCESS
        } as ActionResult;
      }),
      catchError((error) => {
        return error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.PROJECT_SENT_TO_CIRCUIT_FAILURE
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}
