import { Observable, of } from 'rxjs';
import { IActuator } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { Project } from '../../../../model/project/project';
import { BreadcrumbItem } from '../../../../wa-common/model/breadcrumb-item';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { Router } from '@angular/router';
import { VotersListService } from '../../../../ls-common/services/http-services/voters-list/voters-list.service';

@Injectable({
  providedIn: 'root'
})

export class VoteProjectAction implements IActuator<Project> {

  constructor(protected router: Router,
  protected service: VotersListService) {
  }

  setCurrentBreadcrumb(value: BreadcrumbItem[]) {
    this.currentBreadcrumb = value;
    return this;
  }

  setSittingId(value: number) {
    this.sittingId = value;
    return this;
  }

  private currentBreadcrumb: BreadcrumbItem[];
  private sittingId: number;

  action(projects: Project[]): Observable<any> {
    return of(this.router.navigateByUrl(RoutingPaths.projectVotingPath(String(projects[0].id),String(this.sittingId)), {
      state: {
        goto: this.router.url,
        originalContext: {goto: this.router.url, breadcrumb: this.currentBreadcrumb}
      }
    }));
  }
}
