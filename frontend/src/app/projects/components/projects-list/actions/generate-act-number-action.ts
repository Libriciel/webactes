import { Observable, Subject } from 'rxjs';
import { Project } from '../../../../model/project/project';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionResult, ConfirmPopupComponent, IActuator } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { IManageProjectsService } from '../../../services/i-manage-projects.service';

@Injectable({
  providedIn: 'root'
})
export class GenerateActNumberAction implements IActuator<Project> {

  messages = ProjectsMessages;

  constructor(protected modalService: NgbModal, protected projectService: IManageProjectsService) {
  }

  action(projects: Project[]): Observable<any> {

    const returnSubject = new Subject<ActionResult>();
    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      size: 'lg',
      centered: true
    });
    modalRef.componentInstance.title = this.messages.GENERATE_ACT_NUMBER_MODAL_TITLE;
    modalRef.componentInstance.confirmMsg = this.messages.INFO_GET_IT;
    modalRef.componentInstance.cancelButton = false;
    modalRef.componentInstance.type = 'info';

    this.projectService.generateActNumber(projects[0].id).subscribe({
      next: result => {
        modalRef.componentInstance.content = result
          ? `${ProjectsMessages.ACT_CODE}
            :  ${result.generatedActNumber}` : '';
        modalRef.result.then(
          () => {
            returnSubject.next({
              error: false,
              needReload: true,
              message: this.messages.GENERATE_ACT_NUMBER_SUCCESS
            });
          }
        ).catch(error => {
          if (error.isError === undefined || error.isError) {
            console.error('Caught error in popup for generate act number : ', error);
          }
        });
      },
      error: () => {
        modalRef.componentInstance.content = this.messages.GENERATE_ACT_NUMBER_ERROR;
        modalRef.result.then(
          () => {
            returnSubject.next({
              error: true,
              needReload: false,
              message: this.messages.GENERATE_ACT_NUMBER_SUCCESS
            });
          });
      }
    });
    return returnSubject.asObservable();
  }
}
