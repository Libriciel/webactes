import { ProjectAction } from './project-action.enum';
import { ActionItem, CommonIcons } from '../../../../ls-common';
import { DeleteProjectsAction } from './delete-projects-action';
import { ProjectsListMessages } from '../projects-list-messages';
import { Project } from '../../../../model/project/project';
import { EditProjectAction } from './edit-project-action';
import { SendToCircuitAction } from './send-to-circuit-action';
import { ShowProjectAction } from './show-project-action';
import { LinkProjectsAction } from './link-projects-action';
import { UnlinkProjectsAction } from './unlink-projects-action';
import { SignProjectAction } from './sign-project-action';
import { RejectProjectAction } from './reject-project-action';
import { ValidateStepAction } from './validate-step-action';
import { ValidateInEmergencyAction } from './validate-in-emergency-action';
import { DeleteProjectActionValidator } from './delete-project-action-validator';
import { EditProjectActionValidator } from './edit-project-action-validator';
import { RejectProjectActionValidator } from './reject-project-action-validator';
import { ValidateStepActionValidator } from './validate-step-action-validator';
import { BreadcrumbItem } from '../../../../wa-common/model/breadcrumb-item';
import { SendToParapheurProjectAction } from './send-to-parapheur-project-action';
import { CheckIparapheurStatusAction } from './check-iparapheur-status-action';
import { CheckIparapheurStatusActionValidator } from './check-iparapheur-status-action-validator';
import { SendToParapheurProjectActionValidator } from './send-to-parapheur-project-action-validator';
import { ShowParapheurRejectionActionValidator } from './show-parapheur-rejection-action-validator';
import { ShowIparapheurRefusalAction } from './show-iparapheur-refusal-action';
import { SignProjectActionValidator } from './sign-project-action-validator';
import { Injectable } from '@angular/core';
import { ValidateInEmergencyActionValidator } from './validate-in-emergency-action-validator';
import { SendToCircuitActionValidator } from './send-to-circuit-action-validator';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { VoteProjectAction } from './vote-project-action';
import { VoteProjectActionValidator } from './vote-project-action-validator';
import { DownloadProjectAction } from './download-project-action';
import { Style } from '../../../../wa-common';
import { GenerateActNumberAction } from './generate-act-number-action';
import { GenerateActNumberActionValidator } from './generate-act-number-action-validator';

@Injectable({
  providedIn: 'root'
})
export class ProjectActionItemFactory {

  constructor(private showIparapheurRefusalAction: ShowIparapheurRefusalAction,
              private checkIparapheurStatusAction: CheckIparapheurStatusAction,
              private deleteProjectsAction: DeleteProjectsAction,
              private sendProjectsAction: SendToCircuitAction,
              private sendToParapheurProjectAction: SendToParapheurProjectAction,
              private signProjectAction: SignProjectAction,
              private rejectProjectAction: RejectProjectAction,
              private validateStepAction: ValidateStepAction,
              private validateInEmergencyAction: ValidateInEmergencyAction,
              private editProjectAction: EditProjectAction,
              private showProjectAction: ShowProjectAction,
              private voteProjectAction: VoteProjectAction,
              private downloadProjectAction: DownloadProjectAction,
              private generateActNumberAction: GenerateActNumberAction) {
  }

  getActionItem(action: ProjectAction, currentBreadcrumb?: BreadcrumbItem[], sittingId?: number): ActionItem<Project> {
    switch (action) {
      case ProjectAction.DELETE_PROJECT:
        return new ActionItem({
          name: ProjectsListMessages.DELETE_PROJECTS_ACTION_NAME,
          icon: CommonIcons.DELETE_ICON,
          style: ProjectsListMessages.DELETE_PROJECTS_ACTION_ICON_STYLE,
          actuator: this.deleteProjectsAction,
          actionValidator: new DeleteProjectActionValidator(),
        });

      case ProjectAction.EDIT_PROJECT:
        return new ActionItem({
          name: ProjectsListMessages.EDIT_ACTION_NAME,
          longName: ProjectsListMessages.EDIT_ACTION_LONG_NAME,
          icon: CommonIcons.EDIT_ICON,
          actuator: this.editProjectAction.setCurrentBreadcrumb(currentBreadcrumb),
          actionValidator: new EditProjectActionValidator(),
        });

      case ProjectAction.SEND_PROJECT_TO_CIRCUIT:
        return new ActionItem({
          name: ProjectsListMessages.SEND_PROJECT_TO_CIRCUIT_ACTION_NAME,
          icon: ProjectsMessages.SEND_TO_CIRCUIT_BUTTON_ICON,
          actuator: this.sendProjectsAction,
          actionValidator: new SendToCircuitActionValidator(),
          fullButton: true
        });

      case ProjectAction.SHOW_PROJECT:
        return new ActionItem({
          name: ProjectsListMessages.SHOW_ACTION_NAME,
          icon: CommonIcons.SHOW_ICON,
          actuator: this.showProjectAction.setCurrentBreadcrumb(currentBreadcrumb),
        });

      case ProjectAction.SEND_TO_SIGN_I_PARAPHEUR:
        return new ActionItem({
          name: ProjectsListMessages.SEND_TO_SIGN_I_PARAPHEUR_ACTION_NAME,
          icon: ProjectsListMessages.SEND_TO_SIGN_I_PARAPHEUR_ACTION_ICON,
          actuator: this.sendToParapheurProjectAction,
          actionValidator: new SendToParapheurProjectActionValidator()
        });

      case ProjectAction.SIGN_PROJECT:
        return new ActionItem({
          name: ProjectsListMessages.SEND_TO_SIGN_MANUAL_ACTION_NAME,
          icon: ProjectsListMessages.SEND_TO_SIGN_MANUAL_ACTION_ICON,
          actuator: this.signProjectAction,
          actionValidator: new SignProjectActionValidator()
        });

      case ProjectAction.LINK_PROJECT:
        return new ActionItem({
          name: ProjectsListMessages.LINK_PROJECT_ACTION_NAME,
          icon: ProjectsListMessages.LINK_PROJECT_ACTION_ICON,
          style: ProjectsListMessages.LiNK_PROJECT_ACTION_ICON_COLOR,
          actuator: new LinkProjectsAction(),
        });

      case ProjectAction.UNLINK_PROJECT:
        return new ActionItem({
          name: ProjectsListMessages.UNLINK_PROJECT_ACTION_NAME,
          icon: ProjectsListMessages.UNLINK_PROJECT_ACTION_ICON,
          style: ProjectsListMessages.UNLINK_PROJECT_ACTION_ICON_STYLE,
          actuator: new UnlinkProjectsAction(),
        });

      case ProjectAction.REJECT_PROJECT:
        return new ActionItem({
          name: ProjectsMessages.REJECT_PROJECT_BUTTON_TITLE,
          icon: CommonIcons.REJECTED_ICON,
          style: ProjectsListMessages.REJECT_PROJECT_ACTION_ICON_STYLE,
          actuator: this.rejectProjectAction,
          actionValidator: new RejectProjectActionValidator(),
        });

      case ProjectAction.VALIDATE_STEP:
        return new ActionItem({
          name: ProjectsListMessages.VALIDATE_STEP_ACTION_NAME,
          icon: CommonIcons.VALIDATED_ICON,
          actuator: this.validateStepAction,
          actionValidator: new ValidateStepActionValidator(),
        });

      case ProjectAction.VALIDATE_IN_EMERGENCY:
        return new ActionItem({
          name: ProjectsListMessages.VALIDATE_IN_EMERGENCY_ACTION_NAME,
          icon: ProjectsListMessages.VALIDATE_IN_EMERGENCY_ACTION_ICON,
          style: ProjectsListMessages.VALIDATE_IN_EMERGENCY_ACTION_ICON_STYLE,
          actuator: this.validateInEmergencyAction,
          actionValidator: new ValidateInEmergencyActionValidator(),
          fullButton: true
        });

      case ProjectAction.CHECK_IPARAPHEUR_STATUS:
        return new ActionItem<Project>({
          name: ProjectsMessages.CHECK_IPARAPHEUR_STATUS_ACTION_NAME,
          icon: ProjectsMessages.CHECK_IPARAPHEUR_STATUS_ACTION_ICON,
          actuator: this.checkIparapheurStatusAction,
          actionValidator: new CheckIparapheurStatusActionValidator()
        });

      case ProjectAction.SHOW_IPARAPHEUR_REFUSAL:
        return new ActionItem({
          name: ProjectsMessages.SHOW_I_PARAPHEUR_REFUSAL_ACTION_NAME,
          icon: ProjectsMessages.SHOW_I_PARAPHEUR_REFUSAL_ACTION_ICON,
          style: ProjectsMessages.SHOW_I_PARAPHEUR_REFUSAL_ACTION_STYLE,
          actuator: this.showIparapheurRefusalAction,
          actionValidator: new ShowParapheurRejectionActionValidator()
        });

      case ProjectAction.VOTE:
        return new ActionItem({
          name: ProjectsMessages.VOTE_PROJECT_ACTION_NAME,
          icon: ProjectsMessages.VOTE_PROJECT_ACTION_ICON,
          style: ProjectsMessages.VOTE_PROJECT_ACTION_STYLE,
          actuator: this.voteProjectAction.setSittingId(sittingId),
          actionValidator: new VoteProjectActionValidator()
        });

      case ProjectAction.DOWNLOAD_GENERATE_PROJECT:
        return new ActionItem({
          name: ProjectsMessages.DOWNLOAD_GENERATE_PROJECT,
          icon: CommonIcons.DOWNLOAD_DOCUMENT_ICON,
          style: Style.NORMAL,
          actuator: this.downloadProjectAction
        });

      case ProjectAction.GENERATE_ACT_NUMBER:
        return new ActionItem({
          name: ProjectsMessages.GENERATE_ACT_NUMBER,
          icon: ProjectsMessages.GENERATE_ACT_NUMBER_ICON,
          style: Style.NORMAL,
          actuator: this.generateActNumberAction,
          actionValidator: new GenerateActNumberActionValidator()
        });
      default:
        throw new Error(`unimplemented action ${action}`);
    }

  }
}
