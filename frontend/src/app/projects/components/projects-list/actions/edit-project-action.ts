import { Observable, of } from 'rxjs';
import { Project } from '../../../../model/project/project';
import { Router } from '@angular/router';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { BreadcrumbItem } from '../../../../wa-common/model/breadcrumb-item';
import { Injectable } from '@angular/core';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';

@Injectable({
  providedIn: 'root'
})
export class EditProjectAction implements IActuator<Project> {

  private currentBreadcrumb: BreadcrumbItem[];

  constructor(protected router: Router) {
  }

  setCurrentBreadcrumb(value: BreadcrumbItem[]) {
    this.currentBreadcrumb = value;
    return this;
  }

  action(projects: Project[]): Observable<any> {
    return of(this.router.navigateByUrl(RoutingPaths.projectEditionPath(projects[0].id), {
      state: {
        goto: this.router.url,
        originalContext: {goto: this.router.url, breadcrumb: this.currentBreadcrumb}
      }
    }));
  }
}
