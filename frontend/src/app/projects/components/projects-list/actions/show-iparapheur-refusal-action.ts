import { Observable, of } from 'rxjs';
import { Project } from '../../../../model/project/project';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IParapheurRefusalModalComponent } from '../../modals/iparapheur-refusal-modal/iparapheur-refusal-modal.component';
import { IIParapheurService } from '../../../services/i-iparapheur-service';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';

@Injectable({
  providedIn: 'root'
})
export class ShowIparapheurRefusalAction implements IActuator<Project> {

  messages = ProjectsMessages;

  constructor(protected  modalService: NgbModal, protected iparapheurService: IIParapheurService) {
  }

  action(projects: Project[]): Observable<any> {

    if (projects.length > 1) {
      console.error('ShowIparapheurRefusalAction - Multiple projects not handled!');
    }

    if (projects.length < 1) {
      console.error('ShowIparapheurRefusalAction - no target project');
      return of({});
    }

    return this.iparapheurService.getIParapheurStatus(projects[0]).pipe(
      map(value => {
        const modalRef = this.modalService.open(IParapheurRefusalModalComponent, {
          backdrop: 'static',
          keyboard: false,
          size: 'lg',
          centered: true
        });
        modalRef.componentInstance.project = projects[0];
        modalRef.componentInstance.cancelButton = false;
        modalRef.componentInstance.refusalText = `[${value.status}] ${value.annotation}`;
        return of(modalRef.result
          .then(() => {
          })
          .catch(error => {
            if (error.isError) {
              console.error(error);
            }
          })
        );
      }));
  }
}
