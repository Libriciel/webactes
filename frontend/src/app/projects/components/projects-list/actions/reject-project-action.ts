import { from, Observable, of } from 'rxjs';
import { Project } from '../../../../model/project/project';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionResult } from 'src/app/ls-common/components/tables/actuator/action-result';
import { RejectProjectModalComponent } from '../../modals/reject-project-modal/reject-project-modal.component';
import { Injectable } from '@angular/core';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RejectProjectAction implements IActuator<Project> {

  messages = ProjectsMessages;

  constructor(protected modalService: NgbModal) {
  }

  action(projects: Project[]): Observable<any> {
    if (projects.length > 1) {
      console.error('ValidateStepAction - Multiple projects not handled!');
    }

    if (projects.length < 1) {
      console.error('ValidateStepAction - no target project');
      return of({});
    }

    const modal = this.modalService.open(RejectProjectModalComponent);
    modal.componentInstance.project = projects[0];

    return from(modal.result).pipe(
      map(() => {
        return {
          error: false,
          needReload: true,
          message: this.messages.reject_project_success_msg(projects[0].name)
        } as ActionResult;
      }),
      catchError((error) => {
        return error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.reject_project_error_msg(projects[0].name)
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}
