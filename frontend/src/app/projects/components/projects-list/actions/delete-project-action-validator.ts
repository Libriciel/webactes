import { Project } from 'src/app/model/project/project';
import { ProjectAvailableActions } from '../../../../model/project/project-available-actions.enum';
import { AvailableActionsBasedValidator } from '../../../../ls-common/components/tables/validator/available-actions-based-validator';

export class DeleteProjectActionValidator extends AvailableActionsBasedValidator<Project> {

  constructor() {
    super(ProjectAvailableActions.isDeletable);
  }
}
