import { ProjectAvailableActions } from '../../../../model/project/project-available-actions.enum';
import { AvailableActionsBasedValidator } from '../../../../ls-common/components/tables/validator/available-actions-based-validator';
import { Project } from '../../../../model/project/project';

export class VoteProjectActionValidator extends AvailableActionsBasedValidator<Project> {

  constructor() {
    super(ProjectAvailableActions.canBeVoted);
  }
}
