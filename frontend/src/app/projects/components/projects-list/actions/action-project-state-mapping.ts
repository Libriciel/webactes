import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { Project } from '../../../../model/project/project';
import { ProjectActionItemFactory } from './project-action-item-factory';
import { ProjectAction } from './project-action.enum';
import { ProjectDisplayState } from 'src/app/model/project/project-display-state.enum';
import { ProjectsBreadcrumb } from '../../../routing/projects-breadcrumb';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { Injectable } from '@angular/core';
import { StructureSettingsService } from '../../../../core';

@Injectable({
  providedIn: 'root'
})
export class ActionProjectStateMapping {


  constructor(
    protected projectActionItemFactory: ProjectActionItemFactory,
    protected structureSettingsService: StructureSettingsService,
  ) {
  }

  getCommonActions(projectState: ProjectDisplayState): ActionItemsSet<Project>[] {
    switch (projectState) {
      case ProjectDisplayState.DRAFT:
      case ProjectDisplayState.VALIDATING:
      case ProjectDisplayState.TO_BE_VALIDATED:
      case ProjectDisplayState.VALIDATED:
      case ProjectDisplayState.VOTED:
      case ProjectDisplayState.UNASSOCIATED:
      case ProjectDisplayState.VALIDATED_INTO_SITTING:
        return [];
    }
  }

  getSingleActions(projectState: ProjectDisplayState, sittingId?: number): ActionItemsSet<Project> {
    switch (projectState) {
      case ProjectDisplayState.SEARCH:
        return {
          actionItems: [
            this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT,
              ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_DRAFTS_PATH)
            ),
          ]
        };

      case ProjectDisplayState.DRAFT:
        let actions = [];
        if (this.structureSettingsService.getConfig('project', 'project_workflow')) {
          actions = actions.concat(this.projectActionItemFactory.getActionItem(ProjectAction.SEND_PROJECT_TO_CIRCUIT));
        }
        actions = actions.concat(
          this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT,
            ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_DRAFTS_PATH)),
          this.projectActionItemFactory.getActionItem(ProjectAction.EDIT_PROJECT,
            ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_DRAFTS_PATH)));

        return {
          actionItems: actions
        };

      case ProjectDisplayState.VALIDATING:
        return {
          actionItems: [
            this.projectActionItemFactory.getActionItem(ProjectAction.VALIDATE_IN_EMERGENCY),
            this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT,
              ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_VALIDATING_PATH)),
            this.projectActionItemFactory.getActionItem(ProjectAction.EDIT_PROJECT,
              ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_VALIDATING_PATH))
          ]
        };

      case ProjectDisplayState.TO_BE_VALIDATED:
        return {
          actionItems: [
            this.projectActionItemFactory.getActionItem(ProjectAction.VALIDATE_STEP),
            this.projectActionItemFactory.getActionItem(ProjectAction.REJECT_PROJECT),
            this.projectActionItemFactory.getActionItem(ProjectAction.EDIT_PROJECT,
              ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_TO_BE_VALIDATED_PATH)),
          ]
        };

      case ProjectDisplayState.VALIDATED:
      case ProjectDisplayState.VOTED:
        return {
          actionItems: [
            this.projectActionItemFactory.getActionItem(ProjectAction.GENERATE_ACT_NUMBER),
            this.projectActionItemFactory.getActionItem(ProjectAction.SEND_TO_SIGN_I_PARAPHEUR),
            this.projectActionItemFactory.getActionItem(ProjectAction.SIGN_PROJECT),
            this.projectActionItemFactory.getActionItem(ProjectAction.CHECK_IPARAPHEUR_STATUS),
            this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_IPARAPHEUR_REFUSAL),
          ]
        };

      case ProjectDisplayState.UNASSOCIATED:
        return {
          actionItems: [
            this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT,
              ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_WITHOUT_SITTING_PATH)),
            this.projectActionItemFactory.getActionItem(ProjectAction.EDIT_PROJECT,
              ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_WITHOUT_SITTING_PATH))
          ]
        };
      case ProjectDisplayState.VALIDATED_INTO_SITTING:
        return {
          actionItems: [
            this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT,
              ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_WITHOUT_SITTING_PATH)),
            this.projectActionItemFactory.getActionItem(ProjectAction.VOTE, undefined, sittingId)
          ]
        };
    }
  }

  getOtherSingleActions(projectState: ProjectDisplayState): ActionItemsSet<Project>[] {
    let actions = [];
    switch (projectState) {
      case ProjectDisplayState.DRAFT:
      case ProjectDisplayState.VALIDATING:
        actions = actions.concat(this.projectActionItemFactory.getActionItem(ProjectAction.DELETE_PROJECT));
        if (this.structureSettingsService.getConfig('project', 'project_generate')) {
          actions = actions.concat(this.projectActionItemFactory.getActionItem(ProjectAction.DOWNLOAD_GENERATE_PROJECT));
        }
        break;
      case ProjectDisplayState.TO_BE_VALIDATED:
        actions = actions.concat(
          this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT,
            ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_TO_BE_VALIDATED_PATH)),
          this.projectActionItemFactory.getActionItem(ProjectAction.DELETE_PROJECT),
          this.projectActionItemFactory.getActionItem(ProjectAction.VALIDATE_IN_EMERGENCY)
        );
        if (this.structureSettingsService.getConfig('project', 'project_generate')) {
          actions = actions.concat(this.projectActionItemFactory.getActionItem(ProjectAction.DOWNLOAD_GENERATE_PROJECT));
        }
        break;
      case ProjectDisplayState.VALIDATED:
      case ProjectDisplayState.VOTED:
        actions = actions.concat(
          this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT,
            ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_VALIDATED_PATH)),
          this.projectActionItemFactory.getActionItem(ProjectAction.EDIT_PROJECT,
            ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_VALIDATED_PATH)),
          this.projectActionItemFactory.getActionItem(ProjectAction.DELETE_PROJECT),
        );
        if (this.structureSettingsService.getConfig('project', 'project_generate')) {
          actions = actions.concat(this.projectActionItemFactory.getActionItem(ProjectAction.DOWNLOAD_GENERATE_PROJECT));
        }
        break;
      case ProjectDisplayState.UNASSOCIATED:
        actions = actions.concat(
          this.projectActionItemFactory.getActionItem(ProjectAction.DELETE_PROJECT),
        );
        if (this.structureSettingsService.getConfig('project', 'project_generate')) {
          actions = actions.concat(this.projectActionItemFactory.getActionItem(ProjectAction.DOWNLOAD_GENERATE_PROJECT));
        }
        break;
    }

    return [
      {
        actionItems: actions
      }];
  }
}
