import { Project } from '../../../../model/project/project';
import { from, Observable, of } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionResult } from '../../../../ls-common/components/tables/actuator/action-result';
import { SignProjectModalComponent } from '../../modals/sign-project-modal/sign-project-modal.component';
import { IManageProjectsService } from '../../../services/i-manage-projects.service';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';

@Injectable({
  providedIn: 'root'
})
export class SignProjectAction implements IActuator<Project> {

  messages = ProjectsMessages;

  constructor(protected  modalService: NgbModal, protected projectService: IManageProjectsService) {
  }

  action(projects: Project[]): Observable<ActionResult> {
    return projects.length === 1
      ? this.projectService.getProject(projects[0].id)
        .pipe(
          switchMap(project => {
            const modalRef = this.modalService.open(SignProjectModalComponent, {
              backdrop: 'static',
              keyboard: false,
              centered: true
            });
            modalRef.componentInstance.project = project;
            modalRef.componentInstance.signingType = 'manual';
            return from(modalRef.result).pipe(map(() => project));
          }),
          map((project) =>
            ({
              needReload: true,
              message: this.messages.sign_project_modal_success(project)
            } as ActionResult)),
          catchError((error) =>
            error && error.isError
              ? of({
                error: true,
                needReload: false,
                message: this.messages.SIGN_PROJECT_MODAL_ERROR
              } as ActionResult)
              : of({
                error: false,
                needReload: false
              } as ActionResult)
          ))
      : of(null);
  }
}
