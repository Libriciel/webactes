import {Observable, of, Subject} from 'rxjs';
import {Project} from '../../../../model/project/project';
import {ConfirmPopupComponent} from 'src/app/ls-common/components/confirm-popup/components/confirm-popup.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {IManageProjectsService} from 'src/app/projects/services/i-manage-projects.service';
import {ActionResult} from 'src/app/ls-common/components/tables/actuator/action-result';
import {Style} from '../../../../wa-common';
import {Injectable} from '@angular/core';
import {ProjectsMessages} from '../../../i18n/projects-messages';
import {CommonIcons, CommonMessages, IActuator} from '../../../../ls-common';

@Injectable({
    providedIn: 'root'
})
export class DeleteProjectsAction implements IActuator<Project> {

    constructor(private projectService: IManageProjectsService, private modalService: NgbModal) {
    }

    action(projects: Project[]): Observable<any> {
        if (!projects || projects.length < 1) {
            console.error('No project selected, action unavailable');
            return of({});
        }

        const returnSubject = new Subject<ActionResult>();

        const modalRef = this.modalService.open(ConfirmPopupComponent, {
            backdrop: 'static',
            keyboard: false,
            centered: true
        });

        if (projects.length > 1) {
            modalRef.componentInstance.title = ProjectsMessages.CONFIRM_DELETE_MULTIPLE_PROJECT_TITLE;
            modalRef.componentInstance.content = ProjectsMessages.build_confirm_delete_multiple_projects_message(projects.length);
        } else {
            const project = projects[0];
            modalRef.componentInstance.title = ProjectsMessages.CONFIRM_DELETE_SINGLE_PROJECT_TITLE;
            modalRef.componentInstance.content = ProjectsMessages.build_confirm_delete_single_project_message(project.name);
        }

        modalRef.componentInstance.confirmMsg = CommonMessages.DELETE;
        modalRef.componentInstance.style = Style.DANGER;
        modalRef.componentInstance.icon = CommonIcons.DELETE_ICON;

        modalRef.result.then(
            () => this.projectService.deleteProjects(projects).subscribe({
                complete: () => {
                    returnSubject.next({
                        error: false,
                        needReload: true,
                        message: ProjectsMessages.PROJECT_DELETE_SUCCESS
                    });
                },
                error: (error) => {
                    console.error('Error calling project deletion : ', error);
                    returnSubject.next({
                        error: true,
                        needReload: true,
                        message: ProjectsMessages.PROJECT_DELETE_FAILURE
                    });
                }
            }),
            () => returnSubject.complete()
        ).catch(error => {
            console.error('Caught error in popup for circuit deletion, this should not happen. Error : ', error);
        });

        return returnSubject.asObservable();
    }
}
