import { Observable, Subject } from 'rxjs';
import { ActionResult, ConfirmPopupComponent, IActuator, NotificationService } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { Project } from '../../../../model/project/project';
import { Router } from '@angular/router';
import { IManageProjectsService } from '../../../services/i-manage-projects.service';
import { FileType } from '../../../../utility/Files/file-types.enum';
import { SpinnerService } from '../../../../core/services/spinner.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { Style } from '../../../../wa-common';

@Injectable({
  providedIn: 'root'
})

export class DownloadProjectAction implements IActuator<Project> {

  messages = ProjectsMessages;

  constructor(
    protected router: Router,
    protected spinnerService: SpinnerService,
    protected service: IManageProjectsService,
    protected notificationService: NotificationService,
    protected modalService: NgbModal,
  ) {
  }

  action(projects: Project[]): Observable<ActionResult> {


    const message = projects[0].generationerror.find(generationerror => generationerror.type === 'project')
      ? projects[0].generationerror.find(generationerror => generationerror.type === 'project').message : null;


    const returnSubject = new Subject<ActionResult>();
    if (message) {
      const modalRef = this.modalService.open(ConfirmPopupComponent, {
        backdrop: 'static',
        keyboard: false,
        centered: true
      });
      modalRef.componentInstance.title = this.messages.CONFIRM_DOWNLOAD_TITLE;
      modalRef.componentInstance.content = this.messages.downloadProjectConfirm(projects[0].name, message);
      modalRef.componentInstance.confirmMsg = this.messages.DOWNLOAD_GENERATE_PROJECT;
      modalRef.componentInstance.style = Style.WARNING;
      modalRef.result
        .then(() => this.doDownload(projects[0], returnSubject),
          () => {
            returnSubject.next({needReload: false});
            returnSubject.complete();
          }
        );
    } else {
      this.doDownload(projects[0], returnSubject);
    }
    return returnSubject.asObservable();
  }

  private doDownload(project: Project, returnSubject: Subject<ActionResult>) {
    this.spinnerService.load();
    this.service.getDownloadProject(project.id, FileType.PDF, 'Projet').subscribe(
      () => {
        returnSubject.next({
          error: false,
          needReload: false,
        });
      },
      error => {
        console.log(error);
        this.spinnerService.error(error.statusText, null, error);
        returnSubject.next({
          error: true,
          needReload: true,
        });
      }
    ).add(() => {
      returnSubject.complete();
      this.spinnerService.close();
    });
  }
}
