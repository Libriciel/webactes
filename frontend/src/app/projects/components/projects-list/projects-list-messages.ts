import { Style } from '../../../wa-common/style.enum';

export class ProjectsListMessages {
  static CREATE_PROJECT = 'Créer un projet';
  static STEP = 'Étape courante';

  static EDIT_ACTION_NAME = 'Modifier';
  static EDIT_ACTION_LONG_NAME = 'Modifier le projet';

  static SEND_PROJECT_TO_CIRCUIT_ACTION_NAME = 'Envoyer dans un circuit';

  static SHOW_ACTION_NAME = 'Visualiser';

  static SEND_TO_SIGN_I_PARAPHEUR_ACTION_NAME = 'Envoyer au parapheur';
  static SEND_TO_SIGN_I_PARAPHEUR_ACTION_ICON = 'fa ls-icon-envoyer-au-i-parapheur';
  static SEND_TO_SIGN_MANUAL_ACTION_NAME = 'Déclarer signé';
  static SEND_TO_SIGN_MANUAL_ACTION_ICON = 'fa ls-icon-declarer-signature-manuscrite';

  static DELETE_PROJECTS_ACTION_NAME = 'Supprimer';
  static DELETE_PROJECTS_ACTION_ICON_STYLE = Style.DANGER;

  static LINK_PROJECT_ACTION_NAME = 'Associer';
  static LINK_PROJECT_ACTION_ICON = 'fas fa-link';
  static LiNK_PROJECT_ACTION_ICON_COLOR = Style.NEUTRAL;

  static UNLINK_PROJECT_ACTION_NAME = 'Dissocier';
  static UNLINK_PROJECT_ACTION_ICON = 'fas fa-unlink';
  static UNLINK_PROJECT_ACTION_ICON_STYLE = Style.NEUTRAL;

  static REJECT_PROJECT_ACTION_ICON_STYLE = Style.DANGER;

  static VALIDATE_STEP_ACTION_NAME = 'Valider';

  static VALIDATE_IN_EMERGENCY_ACTION_NAME = 'Valider en urgence';
  static VALIDATE_IN_EMERGENCY_ACTION_ICON = 'fa ls-icon-valider-en-urgence';
  static VALIDATE_IN_EMERGENCY_ACTION_ICON_STYLE = Style.WARNING;
}
