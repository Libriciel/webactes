import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { SittingOverview } from '../../../../model/Sitting/sitting-overview';
import { IManageProjectsService } from '../../../services/i-manage-projects.service';
import { Project } from '../../../../model/project/project';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonIcons, NotificationService } from '../../../../ls-common';
import { IManageClassificationService } from '../../../../classification/services/i-manage-classification-service';
import { ManageProjectComponent } from '../manage-project.component';
import { ProjectGeneralInformation } from '../../forms/project-general-information/project-general-information';
import { ProjectComplementaryInformation } from '../../forms/project-complementary-information/project-complementary-information';
import { ProjectDocumentsInformation } from '../../forms/project-documents/project-documents-information';
import { ProjectAvailableActions } from '../../../../model/project/project-available-actions.enum';
import { IManageThemeService } from '../../../../admin/theme/services/IManageThemeService';
import { DraftTemplatesService, StructureSettingsService } from '../../../../core';
import { IManageSittingService } from '../../../../sittings/services/iManageSittingService';

@Component({
  selector: 'wa-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.scss']
})
export class EditProjectComponent extends ManageProjectComponent implements OnInit, AfterViewChecked {

  generalInformationTabValid: boolean;
  documentTabValid: boolean;
  public commonIcons = CommonIcons;
  modifiedProjectGeneralInformation: ProjectGeneralInformation;
  modifiedProjectComplementaryInformation: ProjectComplementaryInformation;
  modifiedProjectDocumentsInformation: ProjectDocumentsInformation;

  public project: Project;
  public ProjectRight = ProjectAvailableActions;

  constructor(
    protected route: ActivatedRoute,
    protected manageProjectsService: IManageProjectsService,
    protected manageSittingService: IManageSittingService,
    protected manageClassificationService: IManageClassificationService,
    protected router: Router,
    protected notificationService: NotificationService,
    protected draftTemplatesService: DraftTemplatesService,
    protected manageThemeService: IManageThemeService,
    protected structureSettingsService: StructureSettingsService,
    protected changeDetectorRef: ChangeDetectorRef
  ) {
    super(
      router,
      manageProjectsService,
      manageSittingService,
      manageClassificationService,
      notificationService,
      manageThemeService,
      draftTemplatesService,
      structureSettingsService,
      changeDetectorRef
    );
  }

  ngOnInit() {
    super.ngOnInit();

    this.generalInformationTabValid = true;
    this.documentTabValid = true;
    this.project = null;

    this.route.data.subscribe(data => {
      this.project = data.project;
      this.generalInformation = {
        valid: true,
        typeAct: this.project.typeAct,
        stateAct: this.project.stateact,
        actName: this.project.name
      };
      this.complementaryInformation = {
        valid: true,
        theme: this.project.theme,
        sittings: this.project.sittings.map(sitting => new SittingOverview(sitting)),
        classification: this.project.classification,
        codeAct: this.project.codeAct !== '' ? this.project.codeAct : null,
        isMultichannel: this.project.isMultichannel,
      };
      this.documentsInformation = {
        mainDocument: this.project.mainDocument,
        annexes: this.project.annexes,
        projectTexts: this.project.projectTexts,
        valid: true,
      };
      this.upDateTypeAct(this.generalInformation.typeAct);
      this.manageProjectsService.getAllStatesActs('edit', this.project.id).subscribe(stateActs => this.stateActs = stateActs);
    });

    this.currentTypeAct.subscribe(typeAct => {
      this.manageProjectsService.getJoinDocumentTypes(typeAct).subscribe(
        documentTypes => {
          this.mainDocumentTypes = documentTypes.mainDocumentTypes;
          this.annexeDocumentTypes = documentTypes.annexeDocumentTypes;
        });
      this.manageSittingService.getAllSittings(typeAct).subscribe(sittings => this.sittings = sittings);
    });

    this.manageProjectsService.getAllTypesActsActive().subscribe(typeActs => this.typesActs = typeActs.typeacts);
  }

  fillGeneralInformation(generalInformation: ProjectGeneralInformation) {
    setTimeout(() => {
      this.modifiedProjectGeneralInformation = generalInformation;
      this.generalInformationTabValid = generalInformation.valid && this.modifiedProjectComplementaryInformation && this.modifiedProjectComplementaryInformation.valid;
    }, 0);
  }

  fillComplementaryInformation(complementaryInformation: ProjectComplementaryInformation) {
    if (complementaryInformation.codeAct && this.manageProjectsService.checkCodeActUniq(complementaryInformation.codeAct)) {
      complementaryInformation.valid = true;
    }
    setTimeout(() => {
      this.modifiedProjectComplementaryInformation = complementaryInformation;
      this.generalInformationTabValid = complementaryInformation.valid && this.modifiedProjectGeneralInformation && this.modifiedProjectGeneralInformation.valid;
    }, 0);
  }

  fillDocumentsInformation(documentInformation: ProjectDocumentsInformation) {
    setTimeout(() => {
      this.documentTabValid = documentInformation.valid;
      this.modifiedProjectDocumentsInformation = documentInformation;
    }, 0);
  }

  save() {
    this.isProcessing = true;
    this.project.update(this.modifiedProjectGeneralInformation, this.modifiedProjectComplementaryInformation, this.modifiedProjectDocumentsInformation);
    this.manageProjectsService.updateProject(this.project)
      .subscribe(
        () => {
          if (this.originalContext && this.originalContext.goto !== this.goto) {
            this.router.navigateByUrl(this.goto, {state: {originalContext: this.originalContext}})
              .then(() => this.notificationService.showSuccess(this.messages.UPDATE_SUCCESS));
          } else {
            this.router.navigateByUrl(this.goto)
              .then(() => this.notificationService.showSuccess(this.messages.UPDATE_SUCCESS));
          }
        },
        error => {
          if (error.error.errors.id) {
            error.error.errors.id.map(message => {
              this.notificationService.showError(message);
            });
          } else {
            this.notificationService.showError(this.messages.UPDATE_ERROR);
          }
        }
      ).add(
      () => this.isProcessing = false
    );
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }
}
