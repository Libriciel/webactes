import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { FlattenTreeItem } from '../../../utility/flatten-tree-item';
import { SittingOverview } from '../../../model/Sitting/sitting-overview';
import { TypesAct } from '../../../model/type-act/types-act';
import { StatesAct } from '../../../model/states-act';
import { JoinDocumentType } from '../../../model/join-document-type';
import { IManageProjectsService } from '../../services/i-manage-projects.service';
import { IManageClassificationService } from '../../../classification/services/i-manage-classification-service';
import { NotificationService } from '../../../ls-common';
import { Router } from '@angular/router';
import { ProjectGeneralInformation } from '../forms/project-general-information/project-general-information';
import { ProjectComplementaryInformation } from '../forms/project-complementary-information/project-complementary-information';
import { ProjectDocumentsInformation } from '../forms/project-documents/project-documents-information';
import { BreadcrumbItem } from '../../../wa-common/model/breadcrumb-item';
import { ProjectsMessages } from '../../i18n/projects-messages';
import { IManageThemeService } from '../../../admin/theme/services/IManageThemeService';
import { DraftTemplatesService, StructureSettingsService } from '../../../core';
import { ReplaySubject } from 'rxjs';
import { IManageSittingService } from '../../../sittings/services/iManageSittingService';

@Component({
  template: ''
})
export abstract class ManageProjectComponent implements OnInit, AfterViewInit {

  readonly messages = ProjectsMessages;

  @Input() goto = RoutingPaths.PROJECTS_PATH;
  isProcessing = false;
  originalContext: { goto: string, breadcrumb: BreadcrumbItem[] };
  currentTypeAct: ReplaySubject<TypesAct> = new ReplaySubject<TypesAct>();
  currentTypeActId: number;
  generalInformation: ProjectGeneralInformation = {
    valid: false,
    typeAct: null,
    stateAct: null,
    actName: null
  };
  complementaryInformation: ProjectComplementaryInformation = {
    valid: true,
    codeAct: null,
    classification: null,
    isMultichannel: false,
    sittings: [],
    theme: null
  };
  documentsInformation: ProjectDocumentsInformation = {
    valid: false,
    mainDocument: null,
    annexes: [],
    projectTexts: [],
  };

  themes: FlattenTreeItem[] = [];
  sittings: SittingOverview[] = [];
  typesActs: TypesAct[] = [];
  stateActs: StatesAct[] = [];
  classifications: FlattenTreeItem[] = [];
  mainDocumentTypes: JoinDocumentType[] = [];
  annexeDocumentTypes: JoinDocumentType[] = [];
  isWriting: boolean;

  filterString: string;

  protected constructor(
    protected router: Router,
    protected manageProjectsService: IManageProjectsService,
    protected manageSittingService: IManageSittingService,
    protected manageClassificationService: IManageClassificationService,
    protected notificationService: NotificationService,
    protected manageThemeService: IManageThemeService,
    protected draftTemplatesService: DraftTemplatesService,
    protected structureSettingsService: StructureSettingsService,
    protected changeDetectionRef: ChangeDetectorRef
  ) {
    this.goto = this.router.getCurrentNavigation().extras.state
      ? this.router.getCurrentNavigation().extras.state.goto
      : RoutingPaths.PROJECTS_PATH;
    this.originalContext = this.router.getCurrentNavigation().extras.state
    && this.router.getCurrentNavigation().extras.state.originalContext
      ? this.router.getCurrentNavigation().extras.state.originalContext : null;
  }

  ngOnInit() {
    const filters = this.filterString ? {searchedText: this.filterString, active: true} : {
      searchedText: '',
      active: true
    };
    this.isWriting = this.structureSettingsService.getConfig('project', 'project_writing');

    this.manageThemeService.getActiveThemesTree(filters).subscribe(themes => this.themes = themes);
    this.manageClassificationService.getClassificationSubjects()
      .subscribe(classifications => this.classifications = classifications);
  }

  upDateTypeAct(typeAct: TypesAct) {
    // console.log(typeAct.value);
    if (typeAct && this.currentTypeActId !== typeAct.id) {
      this.currentTypeActId = typeAct.id;
      this.currentTypeAct.next(typeAct);
    }
  }

  cancel() {
    if (this.originalContext && this.originalContext.goto !== this.goto) {
      this.router.navigateByUrl(this.goto, {state: {originalContext: this.originalContext}}).then();
    } else {
      this.router.navigateByUrl(this.goto).then();
    }
  }

  ngAfterViewInit(): void {
    // Récupération de la configuration
    this.changeDetectionRef.detectChanges();
  }

}
