import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { StepperComponent } from '../../../../ls-common/components/stepper/stepper.component';
import { Project } from '../../../../model/project/project';
import { ManageProjectComponent } from '../manage-project.component';
import { ProjectGeneralInformation } from '../../forms/project-general-information/project-general-information';
import { ProjectComplementaryInformation } from '../../forms/project-complementary-information/project-complementary-information';
import { ProjectDocumentsInformation } from '../../forms/project-documents/project-documents-information';
import { IManageProjectsService } from '../../../services/i-manage-projects.service';
import { IManageClassificationService } from '../../../../classification/services/i-manage-classification-service';
import { Router } from '@angular/router';
import { NotificationService } from '../../../../ls-common';
import { DraftTemplatesService, StructureSettingsService } from '../../../../core';
import { IManageThemeService } from '../../../../admin/theme/services/IManageThemeService';
import { JoinDocumentType } from 'src/app/model/join-document-type';
import { IManageSittingService } from '../../../../sittings/services/iManageSittingService';
import { WADocument } from '../../../model/wa-document';

@Component({
  selector: 'wa-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss']
})
export class CreateProjectComponent extends ManageProjectComponent implements OnInit, AfterViewInit, AfterViewChecked {

  @ViewChild(StepperComponent, {static: true}) stepper: StepperComponent;
  stepState: number;
  isStepValid: boolean;

  constructor(
    protected router: Router,
    protected manageProjectsService: IManageProjectsService,
    protected manageSittingService: IManageSittingService,
    protected manageClassificationService: IManageClassificationService,
    protected notificationService: NotificationService,
    protected manageThemeService: IManageThemeService,
    protected draftTemplatesService: DraftTemplatesService,
    protected structureSettingsService: StructureSettingsService,
    protected changeDetectorRef: ChangeDetectorRef
  ) {
    super(
      router,
      manageProjectsService,
      manageSittingService,
      manageClassificationService,
      notificationService,
      manageThemeService,
      draftTemplatesService,
      structureSettingsService,
      changeDetectorRef
    );
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  ngOnInit() {
    super.ngOnInit();
    this.isProcessing = false;
    this.isStepValid = false;
    this.stepState = 0;
    this.currentTypeAct.subscribe(typeAct => {
      this.manageProjectsService.getJoinDocumentTypes(typeAct).subscribe(
        documentTypes => {
          const isModifiedDocumentType = this.modifiedDocumentType(documentTypes.mainDocumentTypes);
          this.mainDocumentTypes = documentTypes.mainDocumentTypes;
          this.annexeDocumentTypes = documentTypes.annexeDocumentTypes;
          if (!this.validMainDocumentTypes()) {
            this.documentsInformation.mainDocument.documentType = null;
          } else if (!this.hasMainDocumentType() || this.hasMainDocumentType() && !this.isKnownMainDocumentType()) {
            this.documentsInformation.mainDocument = new WADocument({
              mimeType: '',
              name: '',
              size: 0,
              documentType: this.mainDocumentTypes[0]
            });
          }
          if (isModifiedDocumentType) {
            this.documentsInformation.annexes.forEach(annexe => annexe.documentType = null);
          }
        });

      this.manageSittingService.getAllSittings(typeAct).subscribe(sittings => this.sittings = sittings);
    });

    this.manageProjectsService.getAllTypesActsActive().subscribe(typeActs => {
      this.typesActs = typeActs.typeacts;
      this.generalInformation.typeAct = typeActs.typeacts.find(typeAct => typeAct.isDefault);
    });

    this.manageProjectsService.getAllStatesActs().subscribe(stateActs => {
      this.stateActs = stateActs;
      this.generalInformation.stateAct = stateActs.find(stateAct => stateAct.isDefault);
    });

  }

  save() {
    this.isProcessing = true;
    const project: Project = new Project();
    project.update(this.generalInformation, this.complementaryInformation, this.documentsInformation);

    this.manageProjectsService.addProject(project)
      .subscribe(
        () =>
          this.router.navigateByUrl(this.goto)
            .then(() => this.notificationService.showSuccess(this.messages.save_success(this.generalInformation.stateAct))),
        err => {
          console.error(err);
          this.notificationService.showError(this.messages.UPDATE_ERROR);
        }
      ).add(
      () => this.isProcessing = false
    );
  }

  stepChange($event) {
    this.stepState = $event.selectedIndex;
    switch (this.stepState) {
      case 0:
        this.validationStep_1(this.generalInformation);
        break;
      case 1:
        this.validationStep_2(this.complementaryInformation);
        break;
      case 2:
        this.validationStep_3(this.documentsInformation);
        break;
    }

  }

  validationStep_1(generalInformation: ProjectGeneralInformation) {
    // console.log('generalInformation', generalInformation);
    this.generalInformation = generalInformation;
    this.upDateTypeAct(generalInformation.typeAct);
    this.isStepValid = generalInformation.valid;
  }

  validationStep_2(complementaryInformation: ProjectComplementaryInformation) {
    this.complementaryInformation = complementaryInformation;
    setTimeout(() => this.isStepValid = complementaryInformation.valid, 0);
  }

  validationStep_3(documentsInformation: ProjectDocumentsInformation) {
    this.documentsInformation = documentsInformation;
    setTimeout(() => this.isStepValid = documentsInformation.valid, 0);
  }

  private validMainDocumentTypes(): boolean {
    return !!(this.mainDocumentTypes && this.mainDocumentTypes[0]);
  }

  private isKnownMainDocumentType(): boolean {
    return this.mainDocumentTypes.some(value => value.codeType === this.documentsInformation.mainDocument.documentType.codeType);
  }

  private hasMainDocumentType(): boolean {
    return !!(this.documentsInformation.mainDocument && this.documentsInformation.mainDocument.documentType);
  }

  private modifiedDocumentType(mainDocumentTypes: JoinDocumentType[]): boolean {
    return !mainDocumentTypes || !mainDocumentTypes[0] ||
      !this.mainDocumentTypes || !this.mainDocumentTypes[0] ||
      this.mainDocumentTypes[0].codeType !== mainDocumentTypes[0].codeType;
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }
}
