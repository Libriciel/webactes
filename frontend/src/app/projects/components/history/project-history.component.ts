import { Component, Input, OnInit } from '@angular/core';
import { ProjectHistoryElement } from '../../../model/project/project-history-element';
import { ProjectHistoryElementType } from '../../../model/project/project-history-element-type.enum';
import { ProjectsListMessages } from '../projects-list/projects-list-messages';
import { CommonStylesConstants } from '../../../wa-common/common-styles-constants';
import { ActsMessages } from '../../../acts/i18n/acts-messages';
import { HistoryMessages } from './history-messages';
import { Style } from '../../../wa-common/style.enum';
import { CommonMessages } from '../../../ls-common/i18n/common-messages';
import { ProjectsMessages } from '../../i18n/projects-messages';
import { CommonIcons } from '../../../ls-common/icons/common-icons';

@Component({
  selector: 'wa-project-history',
  templateUrl: './project-history.component.html',
  styleUrls: ['./project-history.component.scss']
})
export class ProjectHistoryComponent implements OnInit {

  messages = HistoryMessages;
  commonMessages = CommonMessages;

  @Input() projectHistory: ProjectHistoryElement[];

  constructor() {
  }

  ngOnInit() {
  }

  getIcon(type: ProjectHistoryElementType) {
    switch (type) {
      case ProjectHistoryElementType.PROJECT_CREATION:
        return CommonIcons.ADD_ICON;
      case ProjectHistoryElementType.PROJECT_MODIFICATION:
        return CommonIcons.EDIT_ICON;
      case ProjectHistoryElementType.SENT_TO_CIRCUIT:
        return ProjectsMessages.SEND_TO_CIRCUIT_BUTTON_ICON;
      case ProjectHistoryElementType.VALIDATED:
        return CommonIcons.VALIDATED_ICON;
      case ProjectHistoryElementType.REJECTED:
        return CommonIcons.REJECTED_ICON;
      case ProjectHistoryElementType.VALIDATED_EMERGENCY:
        return ProjectsListMessages.VALIDATE_IN_EMERGENCY_ACTION_ICON;
      case ProjectHistoryElementType.MAIN_DOC_MODIFIED:
        return CommonIcons.REPLACE_ICON;
      case ProjectHistoryElementType.MANUALLY_SIGNED:
        return ProjectsMessages.DECLARE_SIGNED_ICON;
      case ProjectHistoryElementType.SENT_TO_I_PARAPHEUR:
        return ProjectsListMessages.SEND_TO_SIGN_I_PARAPHEUR_ACTION_ICON;
      case ProjectHistoryElementType.I_PARAPHEUR_SIGNED:
        return ProjectsMessages.I_PARAPHEUR_SIGNED_ICON;
      case ProjectHistoryElementType.I_PARAPHEUR_REJECTED:
        return ProjectsMessages.I_PARAPHEUR_REFUSED_ICON;
      case ProjectHistoryElementType.SENT_TO_TDT:
        return ActsMessages.BUTTON_SEND_TO_CONTROL_ICON;
      case ProjectHistoryElementType.SENT_TO_PREFECTURE:
        return ActsMessages.BUTTON_ORDER_TELETRANSMISSION_ICON;
      case ProjectHistoryElementType.ACKNOWLEDGMENT_RECEIVED:
        return CommonIcons.SIGNED_ACTS_ICON;
      case ProjectHistoryElementType.CANCELLATION:
        return ProjectsMessages.CANCELLED_STATE_ICON;
      case ProjectHistoryElementType.VOTED_APPROVED:
        return CommonIcons.VOTE_ICON;
      case ProjectHistoryElementType.VOTED_REJECTED:
        return CommonIcons.VOTE_ICON;
      case ProjectHistoryElementType.TAKE_NOTE:
        return CommonIcons.VOTE_ICON;
      default:
        return '';
    }
  }

  getIconColor(type: ProjectHistoryElementType) {
    switch (type) {
      case ProjectHistoryElementType.REJECTED:
      case ProjectHistoryElementType.I_PARAPHEUR_REJECTED:
      case ProjectHistoryElementType.CANCELLATION:
        return CommonStylesConstants.getStyleClass(ProjectsListMessages.REJECT_PROJECT_ACTION_ICON_STYLE);
      case ProjectHistoryElementType.VALIDATED_EMERGENCY:
        return CommonStylesConstants.getStyleClass(ProjectsListMessages.VALIDATE_IN_EMERGENCY_ACTION_ICON_STYLE);
      default:
        return CommonStylesConstants.getStyleClass(Style.NORMAL);
    }
  }
}
