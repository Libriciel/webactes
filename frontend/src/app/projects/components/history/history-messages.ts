import { User } from '../../../model/user/user';
import { ProjectHistoryElement } from '../../../model/project/project-history-element';
import { DatePipe } from '@angular/common';
import { ProjectHistoryElementType } from '../../../model/project/project-history-element-type.enum';

export class HistoryMessages {

  static getUserName(user: User): string {
    return `${user.firstname} ${user.lastname}`;
  }

  static getDetails(item: ProjectHistoryElement) {
    switch (item.type) {
      case ProjectHistoryElementType.SENT_TO_CIRCUIT:
        return `envoyé dans circuit ${item.data && item.data.workflow_name ? item.data.workflow_name : ''} par ${HistoryMessages.getUserName(item.user)} le ${new DatePipe('fr').transform(item.date, 'dd/MM/yyyy à HH:mm')}`;
      case ProjectHistoryElementType.SENT_TO_I_PARAPHEUR:
        return `envoyé au i-Parapheur ${item.data && item.data.workflow_name ? item.data.workflow_name : ''}${item.user ? ' par ' + HistoryMessages.getUserName(item.user) : 'automatiquement'} le ${new DatePipe('fr').transform(item.date, 'dd/MM/yyyy à HH:mm')}`;
      case ProjectHistoryElementType.I_PARAPHEUR_SIGNED:
        return `signature récupérée du i-Parapheur automatiquement le ${new DatePipe('fr').transform(item.date, 'dd/MM/yyyy à HH:mm')}`;
      case ProjectHistoryElementType.I_PARAPHEUR_REJECTED:
        return `refus récupéré du i-Parapheur automatiquement le ${new DatePipe('fr').transform(item.date, 'dd/MM/yyyy à HH:mm')}`;
      case ProjectHistoryElementType.SENT_TO_TDT:
        return `déposé sur le TdT par ${HistoryMessages.getUserName(item.user)} le ${new DatePipe('fr').transform(item.date, 'dd/MM/yyyy à HH:mm')}`;
      case ProjectHistoryElementType.SENT_TO_PREFECTURE:
        return `envoyé en prefecture par ${HistoryMessages.getUserName(item.user)} le ${new DatePipe('fr').transform(item.date, 'dd/MM/yyyy à HH:mm')}`;
      case ProjectHistoryElementType.ACKNOWLEDGMENT_RECEIVED:
        return `acquittement récupéré automatiquement le ${new DatePipe('fr').transform(item.date, 'dd/MM/yyyy à HH:mm')}`;
      case ProjectHistoryElementType.CANCELLATION:
        return `annulation récupérée automatiquement le ${new DatePipe('fr').transform(item.date, 'dd/MM/yyyy à HH:mm')}`;
      case ProjectHistoryElementType.PROJECT_CREATION:
      case ProjectHistoryElementType.PROJECT_MODIFICATION:
      case ProjectHistoryElementType.VALIDATED:
      case ProjectHistoryElementType.REJECTED:
      case ProjectHistoryElementType.VALIDATED_EMERGENCY:
      case ProjectHistoryElementType.MAIN_DOC_MODIFIED:
      case ProjectHistoryElementType.MANUALLY_SIGNED:
      default:
        return `${item.user ? 'par ' + HistoryMessages.getUserName(item.user) : ''} le ${new DatePipe('fr').transform(item.date, 'dd/MM/yyyy à HH:mm')}`;
    }
  }

  static getDescription(type: ProjectHistoryElementType) {
    switch (type) {
      case ProjectHistoryElementType.PROJECT_CREATION:
        return 'Création du projet';
      case ProjectHistoryElementType.PROJECT_MODIFICATION:
        return 'Modification';
      case ProjectHistoryElementType.SENT_TO_CIRCUIT:
        return 'Circuit de validation';
      case ProjectHistoryElementType.VALIDATED:
        return 'Validation';
      case ProjectHistoryElementType.REJECTED:
        return 'Refus';
      case ProjectHistoryElementType.MAIN_DOC_MODIFIED:
        return 'Remplacement du document principal';
      case ProjectHistoryElementType.SENT_TO_I_PARAPHEUR:
        return 'I-Parapheur';
      case ProjectHistoryElementType.I_PARAPHEUR_SIGNED:
        return 'Signature i-Parapheur';
      case ProjectHistoryElementType.I_PARAPHEUR_REJECTED:
        return 'Refus i-Parapheur';
      case ProjectHistoryElementType.MANUALLY_SIGNED:
        return 'Déclaration de la signature';
      case ProjectHistoryElementType.SENT_TO_TDT:
        return 'Dépôt TdT';
      case ProjectHistoryElementType.SENT_TO_PREFECTURE:
        return 'Envoi en préfecture';
      case ProjectHistoryElementType.ACKNOWLEDGMENT_RECEIVED:
        return 'Acquittement reçu';
      case ProjectHistoryElementType.CANCELLATION:
        return 'Annulation';
      case ProjectHistoryElementType.VALIDATED_EMERGENCY:
        return 'Validation en urgence';
      default:
        return type;
    }
  }
}
