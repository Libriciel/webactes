import { Component, Input, OnInit } from '@angular/core';
import { WADocument } from '../../model/wa-document';
import { ProjectsMessages } from '../../i18n/projects-messages';
import { StructureSetting, StructureSettingsService } from '../../../core';

@Component({
  selector: 'wa-main-document-component',
  templateUrl: './main-document.component.html',
  styleUrls: ['./main-document.component.scss']
})
export class MainDocumentComponent implements OnInit {

  messages = ProjectsMessages;
  @Input()
  mainDocument: WADocument;
  public structureSetting: StructureSetting;
  isActGenerate: boolean;

  constructor(protected structureSettingsService: StructureSettingsService) {
  }

  ngOnInit() {
    this.testSettings();
  }

  testSettings() {
    this.isActGenerate = this.structureSettingsService.getConfig('act', 'act_generate');
  }

}
