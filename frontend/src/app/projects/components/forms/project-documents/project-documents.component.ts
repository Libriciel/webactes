import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Config } from '../../../../Config';
import { FileUtils } from '../../../../utility/Files/file-utils';
import { ProjectDocumentsInformation } from './project-documents-information';
import { Annexe } from '../../../model/annexe';
import { FileBtnComponent } from '../../../../ls-common/components/file-btn/file-btn.component';
import { WADocument } from '../../../model/wa-document';
import { IManageProjectsService } from '../../../services/i-manage-projects.service';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { CommonStylesConstants } from '../../../../wa-common/common-styles-constants';
import { Weight } from '../../../../wa-common/weight.enum';
import { Style } from '../../../../wa-common';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { FileType } from '../../../../utility/Files/file-types.enum';
import { OfficeEditorFile, ProjectTextFile } from '../../../../core';

@Component({
  selector: 'wa-project-documents',
  templateUrl: './project-documents.component.html',
  styleUrls: ['./project-documents.component.scss']
})
export class ProjectDocumentsComponent implements OnInit, AfterViewInit {

  @Input() isTransferable = false;
  @Input() isWriting = false;
  @Input() isCreating = false;
  @ViewChild('annexInput', {static: true}) annexInput: FileBtnComponent;
  @Input() documentsInformation: ProjectDocumentsInformation = {
    mainDocument: null,
    annexes: [],
    projectTexts: [],
    valid: false
  };
  @Output() values = new EventEmitter<ProjectDocumentsInformation>();
  messages = ProjectsMessages;
  commonMessages = CommonMessages;
  waCommonMessages = WACommonMessages;
  commonIcon = CommonIcons;
  mainDocumentAcceptedTypes = Config.mainDocumentAcceptedTypesBeforeSigning;
  mainDocumentAcceptedTypesBeforeSigningString = this.projectService.getMainDocumentFileExtensionsBeforeSigning();
  annexesTransferableAcceptedTypes = Config.annexesTransferableAcceptedTypes;
  annexesMergeableAcceptedTypes = Config.annexesFuseableAcceptedTypes;
  annexesTransferableAcceptedTypesString = this.projectService.getAnnexesFileExtensions();
  weight = Weight;
  style = Style;
  fileType = FileType;
  commonStyles = CommonStylesConstants;
  projectTextForm: UntypedFormGroup;

  @Input()
  public mainDocumentTypes = [];
  @Input()
  public annexeDocumentTypes = [];

  public showMandatoryMainDocMessage = false;
  public projectTextProject: OfficeEditorFile;
  public projectTextAct: OfficeEditorFile;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    protected notificationService: NotificationService,
    protected projectService: IManageProjectsService,
    protected changeDetectorRef: ChangeDetectorRef,) {
  }

  ngOnInit() {
    if (!this.isCreating && this.isWriting) {
      this.projectTextForm = this._formBuilder.group({
        projectTextProject: [null, [Validators.required, this.requiredFileType('odt')]],
        projectTextAct: [null, [Validators.required, this.requiredFileType('odt')]],
      });
    }

    if (this.isWriting) {
      let documentType = null;
      if (this.documentsInformation.mainDocument) {
        documentType = this.documentsInformation.mainDocument.documentType;
      }
      this.documentsInformation.mainDocument = new WADocument({
        file: null,
        mimeType: null,
        size: null,
        name: null,
        documentType: documentType,
      });
      this.documentsInformation.valid = true;
      this.values.emit(this.documentsInformation);
    }
    if (!this.isCreating && this.isWriting) {
      this.documentsInformation.projectTexts.forEach(projectText => {
          switch (projectText.draft_template.draft_template_type_id) {
            case Config.DRAFT_TYPE_PROJECT:
              this.projectTextProject = this.updateOfficeEditorFile(projectText.project_text_file);
              this.projectTextForm.patchValue({projectTextProject: this.projectTextProject});
              break;
            case Config.DRAFT_TYPE_ACT:
              this.projectTextAct = this.updateOfficeEditorFile(projectText.project_text_file);
              this.projectTextForm.patchValue({projectTextAct: this.projectTextAct});
              break;
          }
        }
      );
    }
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  addMainDoc(event) {
    const fileList = event.target.files;
    if (fileList.length <= 0) {
      this.documentsInformation.mainDocument = null;
      return;
    }
    if (!this.projectService.hasAcceptableMainDocumentTypeBeforeSigning(fileList[0])) {
      this.notificationService.showError(
        this.messages.add_document_format(Config.mainDocumentAcceptedTypesBeforeSigning, this.messages.MAIN_DOCUMENT),
        this.messages.ADD_DOCUMENT_TYPE_ERROR_NOTIFICATION_TITLE
      );
    } else {
      const file: File = fileList[0];
      this.documentsInformation.mainDocument = new WADocument({
        file: file,
        mimeType: file.type,
        size: file.size,
        name: file.name,
      });
      this.setDefaultMainTypeToMainDocIfAvailable();
      if (!this.projectService.checkDocumentsSize(this.documentsInformation)) {
        this.notificationService.showError(this.messages.too_big(Config.MAX_PROJECT_SIZE));
        this.documentsInformation.mainDocument = null;
      }
      this.OnChange();
    }
  }

  removeMainDoc() {
    this.documentsInformation.mainDocument = null;
    this.showMandatoryMainDocMessage = true;
    this.OnChange();
  }

  hasTransferableType(annexe: File | WADocument): boolean {
    return FileUtils.fileHasAcceptableType(annexe, this.annexesTransferableAcceptedTypes);
  }


  hasMergeableType(annexe: File | WADocument) {
    return FileUtils.fileHasAcceptableType(annexe, this.annexesMergeableAcceptedTypes);
  }

  addAnnexes(event): void {
    const fileList = event.target.files;
    if (fileList && fileList.length <= 0) {
      return;
    }
    if (!this.projectService.hasAcceptableAnnexeType(fileList)) {
      this.notificationService.showError(this.messages.add_document_format(Config.annexesAcceptedTypes, this.messages.ANNEXES),
        this.messages.ADD_DOCUMENT_TYPE_ERROR_NOTIFICATION_TITLE);
    } else {
      for (const file of fileList) {
        this.documentsInformation.annexes.push(new Annexe({
          file: file,
          mimeType: file.type,
          size: file.size,
          name: file.name
        }));
        if (!this.projectService.checkDocumentsSize(this.documentsInformation)) {
          this.notificationService.showError(this.messages.annex_too_big(Config.MAX_PROJECT_SIZE));
          this.documentsInformation.annexes.pop();
        }
      }

    }
    this.annexInput.resetInput();
    this.OnChange();
  }

  removeAnnexe(i): void {
    this.documentsInformation.annexes.splice(i, 1);
    this.OnChange();
  }

  protected OnChange() {
    setTimeout(() => {
      this.documentsInformation.valid = this.documentsInformation.mainDocument !== null
        && this.projectService.checkDocumentsSize(this.documentsInformation);
      this.values.emit(this.documentsInformation);
    });
  }

  private setDefaultMainTypeToMainDocIfAvailable() {
    if (this.mainDocumentTypes && this.mainDocumentTypes[0]) {
      this.documentsInformation.mainDocument.documentType = this.mainDocumentTypes[0];
    }
  }

  requiredFileType(type: string) {
    return function (control: UntypedFormControl) {

      const file = control.value;
      if (file) {
        const extension = file.name.split('.')[1];
        return type.toLowerCase() !== extension.toLowerCase() ? {
          requiredFileType: true
        } : null;
      }
      return null;
    };
  }

  updateOfficeEditorFile(projectTextFile: ProjectTextFile) {
    return {
      id: projectTextFile.id,
      name: projectTextFile.name,
      size: projectTextFile.size,
      mimetype: projectTextFile.mimetype,
      path: projectTextFile.path,
      baseUrl: projectTextFile.base_url,
      pathWopi: projectTextFile.path_wopi + projectTextFile.id,
    };
  }
}
