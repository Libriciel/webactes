import { FormValues } from '../../../../ls-common/model/form-values';
import { Annexe } from '../../../model/annexe';
import { WADocument } from '../../../model/wa-document';
import {ProjectText} from '../../../../core';

export interface ProjectDocumentsInformation extends FormValues {
  mainDocument: WADocument;
  annexes: Annexe[];
  projectTexts: ProjectText[];
}
