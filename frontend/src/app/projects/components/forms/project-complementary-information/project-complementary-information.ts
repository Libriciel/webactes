import { Theme } from '../../../../model/theme/theme';
import { FormValues } from '../../../../ls-common/model/form-values';
import { SittingOverview } from '../../../../model/Sitting/sitting-overview';
import { ClassificationSubject } from '../../../../model/classification/classification-subject';

export interface ProjectComplementaryInformation extends FormValues {
  theme?: Theme;
  sittings?: SittingOverview[];
  classification?: ClassificationSubject;
  codeAct?: string;
  isMultichannel?: boolean;
}
