import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SittingOverview } from '../../../../model/Sitting/sitting-overview';
import { ProjectComplementaryInformation } from './project-complementary-information';
import { FlattenTreeItem } from '../../../../utility/flatten-tree-item';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { CommonMessages } from '../../../../ls-common';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { StructureSettingsService } from '../../../../core';
import { TypesAct } from '../../../../model/type-act/types-act';
import { FormControl, UntypedFormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'wa-project-complementary-information',
  templateUrl: './project-complementary-information.component.html',
  styleUrls: ['./project-complementary-information.component.scss']
})
export class ProjectComplementaryInformationComponent implements OnInit, AfterViewInit, AfterViewChecked {

  public messages = ProjectsMessages;
  public commonMessages = CommonMessages;
  public waCommonMessages = WACommonMessages;

  @Output() values = new EventEmitter<ProjectComplementaryInformation>();
  @Input()
  classifications: FlattenTreeItem[] = [];
  @Input()
  themes: FlattenTreeItem[] = [];
  @Input()
  typeAct: TypesAct = null;

  @Input()
  public sittings: SittingOverview[] = [];

  private originalCodeAct: string;
  private codeActIsValid: boolean;
  private statusChangesSubscription: Subscription;

  sittingsIsValid: boolean;
  generateNumber = true;
  sittingEnable = true;

  constructor(private structureSettingsService: StructureSettingsService,
              protected changeDetectorRef: ChangeDetectorRef) {
  }

  _information: ProjectComplementaryInformation = {
    codeAct: null,
    isMultichannel: false,
    sittings: [],
    classification: null,
    theme: null,
    valid: true
  };

  formGroup = new UntypedFormGroup({
    isMultichannel: new FormControl(this._information.isMultichannel),
  });

  get information(): ProjectComplementaryInformation {
    return this._information;
  }

  @Input()
  set information(projectComplementaryInformation: ProjectComplementaryInformation) {
    this._information = projectComplementaryInformation;

    if (!this.originalCodeAct) {
      this.originalCodeAct = this._information.codeAct;
    }
  }

  ngOnInit() {
    // Récupération de la configuration
    this.generateNumber = this.structureSettingsService.getConfig('act', 'generate_number');
    this.sittingEnable = this.structureSettingsService.getConfig('sitting', 'sitting_enable');

    this.formGroup.patchValue({
      isMultichannel: this._information.isMultichannel
    });
    this.statusChangesSubscription = this.formGroup.valueChanges.subscribe(() => this.OnChange());
    this.OnChange();
  }

  OnChange() {
    this.values.emit({
      codeAct: this._information.codeAct !== '' ? this._information.codeAct : null,
      theme: this._information.theme,
      sittings: this._information.sittings,
      classification: this._information.classification,
      isMultichannel: this.formGroup.controls['isMultichannel'].value,
      valid: this.isValid(),
    } as ProjectComplementaryInformation);
  }

  isValid() {
    if (this.sittingEnable && this.generateNumber) {
      return true;
    }
    if (this.sittingEnable && this.codeActIsValid) {
      return true;
    }
    if (!this.generateNumber && this.sittingsIsValid) {
      return true;
    }
    if (this.sittingEnable && this.sittingsIsValid) {
      return true;
    }
    return !this.sittingEnable;
  }

  changeSittings($event) {
    this.sittingsIsValid = !(this._information.sittings.filter(typeSitting => typeSitting.isdeliberating).length > 1);
    this.OnChange();
  }

  actCodeValidityChanged(isValid: boolean) {
    this.codeActIsValid = this.generateNumber ? true : isValid;
    this.OnChange();
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  ngOnDestroy() {
    this.statusChangesSubscription.unsubscribe();
  }
}
