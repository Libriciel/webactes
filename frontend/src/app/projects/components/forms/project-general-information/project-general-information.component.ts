import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TypesAct } from '../../../../model/type-act/types-act';
import { StatesAct } from '../../../../model/states-act';
import { ProjectGeneralInformation } from './project-general-information';
import { ProjectsMessages } from '../../../i18n/projects-messages';
import { Choice } from '@libriciel/ls-composants';

@Component({
  selector: 'wa-project-general-information',
  templateUrl: './project-general-information.component.html',
  styleUrls: ['./project-general-information.component.scss']
})
export class ProjectGeneralInformationComponent implements OnInit, AfterViewInit {

  readonly messages = ProjectsMessages;

  get stateActs(): StatesAct[] {
    return this._stateActs;
  }

  @Input()
  set stateActs(stateActs: StatesAct[]) {
    this._stateActs = stateActs;
    this.stateActsChoices = this.stateActs.map(stateAct => ({name: stateAct.name, value: stateAct} as Choice));
  }

  get typesActs(): TypesAct[] {
    return this._typesActs;
  }

  @Input()
  set typesActs(typesActs: TypesAct[]) {
    this._typesActs = typesActs;
    this.typeActsChoices = this.typesActs.map(typeact => ({name: typeact.name, value: typeact} as Choice));
  }

  get information(): ProjectGeneralInformation {
    return this._information;
  }

  @Input()
  set information(projectGeneralInformation: ProjectGeneralInformation) {
    this._information = projectGeneralInformation;
    this.typeAct = projectGeneralInformation.typeAct;
    this.stateAct = projectGeneralInformation.stateAct;
    this.actName = projectGeneralInformation.actName;
  }

  get actName(): string {
    return this._actName;
  }

  @Input()
  set actName(value: string) {
    this._actName = value;
    this.onChange();
  }

  @Input() maxButtons: number;
  @Input() isStateEditable = true;
  private _typesActs: TypesAct[] = [];
  typeActsChoices: Choice[];
  private _stateActs: StatesAct[] = [];
  stateActsChoices: Choice[];
  private _actName: string;
  typeAct: TypesAct;
  stateAct: StatesAct;
  @Input() isTypeActEditable = true;

  @Output() values = new EventEmitter<ProjectGeneralInformation>();

  constructor(protected changeDetectorRef: ChangeDetectorRef) {
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  private _information: ProjectGeneralInformation = {
    valid: false,
    actName: null,
    stateAct: null,
    typeAct: null
  };

  ngOnInit() {
  }

  onChange(): void {
    this.values.emit({
      actName: this.actName,
      stateAct: this.stateAct,
      typeAct: this.typeAct,
      valid: (!(!this.typeAct || !this.actName || !this.stateAct))
    });

  }

  onChangeStateAct() {
    this.onChange();
  }

  onChangeTypeAct() {
    this.onChange();
  }
}
