import { TypesAct } from '../../../../model/type-act/types-act';
import { StatesAct } from '../../../../model/states-act';
import { FormValues } from '../../../../ls-common/model/form-values';

export interface ProjectGeneralInformation extends FormValues {
  typeAct: TypesAct;
  stateAct: StatesAct;
  actName: string;
}
