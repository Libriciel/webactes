import { FileType } from '../../utility/Files/file-types.enum';
import { FileUtils } from '../../utility/Files/file-utils';
import { DatePipe } from '@angular/common';
import { WADocument } from '../model/wa-document';
import { Project } from '../../model/project/project';
import { StatesAct } from '../../model/states-act';
import { Style } from '../../wa-common';
import { Config } from '../../Config';
import { CommonIcons, CommonMessages } from '../../ls-common';

export class ProjectsMessages {
  static STEP_1_LABEL = 'Informations principales';
  static STEP_2_LABEL = 'Informations complémentaires';
  static DOCUMENTS_TAB_TITLE = 'Documents';
  static INFORMATION_TAB_TITLE = 'Informations';
  static CIRCUIT_TAB_TITLE = 'Circuit';

  static MANDATORY_FIELD_MISSING_TOOLTIP = `L'onglet contient des champs obligatoires manquants ou erronés`;

  static ACT_TYPE = `Type d'acte`;
  static ACT_TYPE_PLACEHOLDER = `Sélectionner le type de l'acte`;
  static SITTINGS = 'Séance(s)';
  static SITTINGS_PLACEHOLDER = 'Sélectionner une ou plusieurs séances';
  static NO_ASSOCIATED_SITTING_MSG = 'Pas de séance associée';
  static ASSOCIATED__SITTING_IS_DELIBERATING_FALSE = 'Vous ne pouvez pas sélectionner deux séances délibérantes';
  static ACT_NAME = `Libellé de l'acte`;
  static ACT_STATE = `État de l'acte`;
  static COMMENT = 'Commentaire';
  static ACT_STATE_PLACEHOLDER = `Sélectionner l'état de l'acte`;
  static IS_MULTICHANNEL = 'Envoi de document papiers complémentaire';
  static CLASSIFICATION = 'Classification';
  static ACT_CODE = `Numéro de l'acte`;
  static THEME = 'Thème';
  static MAIN_DOCUMENT = 'Document principal';
  static MAIN_DOCUMENT_TYPE = 'Type de document principal';
  static MAIN_DOC_IS_MANDATORY_MESSAGE = `Le document principal de l'acte est obligatoire.`;
  static FILE_TEXTS_MESSAGE = `Les textes de projet sont enregistrés directement via l'éditeur.`;
  static ANNEXES = 'Annexes';
  static ANNEXES_TRANSFERABLE = 'Annexes télétransmissibles';
  static ADD_DOCUMENT = 'Ajouter un document';
  static ADD_DOCUMENTS = 'Ajouter un ou plusieurs documents';
  static TYPE = 'Type';
  static TYPE_PLACE_HOLDER = 'Sélectionner un type';
  static ANNEX_TYPE_NOT_SELECTABLE_TOOLTIP = `Si l'annexe est télétransmissible, cochez la case et sélectionnez un type`;
  static REMOVE_MAIN_DOCUMENT = 'Retirer le document principal';
  static REMOVE_ANNEXE = 'Retirer cette annexe';
  static SAVE_CHANGES = 'Enregistrer les modifications';
  static UPDATE_ERROR = `Votre projet n'a pas été enregistré.`;
  static UPDATE_SUCCESS = 'Modifications enregistrées avec succès\u00a0!';
  static DOCUMENT = 'Document';
  static DOCUMENT_NAME = 'Nom du document';
  static DOCUMENT_FUSION = 'Joindre à la fusion';

  static DOCUMENTS_IPARAPHEUR_TITLE = 'Documents de signature parapheur';
  static DOCUMENTS_TDT_TAB_TITLE = 'Documents TDT';
  static DOCUMENTS_ORIGINAL_TITLE = 'Documents originaux';

  static FORM_DEFAULT_FILE_NAME = 'Bordereau.pdf';
  static ACKNOWLEDGEMENT_DEFAULT_FILE_NAME = 'aracte.xml';
  static XML_FILE_TYPE = 'application/xml';

  static REMOTE_TRANSMISSION = 'À télétransmettre';
  static NON_TRANSFERABLE_ANNEXE = 'Type de fichier non télétransmissible';
  static NO_ANNEXES_MSG = `Pas d'annexe`;
  static NO_DOCUMENT_MSG = `Pas de document principal`;
  static DOCUMENT_ICON = 'far fa-file';
  static DOCUMENT_GENERATION = 'fa ls-icon-generation-document';
  static DOCUMENT_DOWNLOAD_ICON = 'fas fa-download';
  static DOCUMENT_DOWNLOAD_TITLE = 'Télécharger le document principal';
  static SEND_COMPLEMENTARY_PAPER_DOCUMENTS_INFO_MESSAGE =
    `Permet d'indiquer à la préfecture que l'envoi n'est pas complet, et que des pièces papier seront envoyées par voie postale.`;

  static PROJECT_SENT_TO_CIRCUIT_SUCCESS = 'Le projet a bien été envoyé dans le circuit';
  static PROJECT_SENT_TO_CIRCUIT_FAILURE = `Une erreur est survenue lors de l'envoi du projet dans le circuit`;

  static DOWNLOAD_ANNEXE = `Télécharger l'annexe`;
  static DOWNLOAD_DOC = `Télécharger le document`;

  static CONFIRM_DELETE_SINGLE_PROJECT_TITLE = 'Supprimer le projet';
  static CONFIRM_DELETE_MULTIPLE_PROJECT_TITLE = 'Supprimer plusieurs projets';

  static PROJECT_DELETE_SUCCESS = 'Projet supprimé avec succès!';
  static PROJECT_DELETE_FAILURE = 'Une erreur est survenue pendant la suppression de projet.';

  static CHECK_IPARAPHEUR_STATUS_MODAL_TITLE = 'Vérification du statut de la signature';
  static CHECK_IPARAPHEUR_STATUS_MODAL_BUTTON_TITLE = `J'ai compris`;
  static INFO_GET_IT = `J'ai compris`;

  static CHECK_IPARAPHEUR_REFUSAL_MODAL_TITLE = 'Motif du refus par le i-Parapheur';
  static CHECK_IPARAPHEUR_REFUSAL_MODAL_TEXT = 'Pensez à modifier le projet avant de le renvoyer en signature.';
  static CHECK_IPARAPHEUR_REFUSAL_MODAL_BUTTON_TITLE = ProjectsMessages.CHECK_IPARAPHEUR_STATUS_MODAL_BUTTON_TITLE;

  static MODIFY_PROJECT_BUTTON_TITLE = 'Modifier le projet';
  static MODIFY_PROJECT_BUTTON_ICON = CommonIcons.EDIT_ICON;
  static SEND_TO_CIRCUIT_BUTTON_ICON = 'fas fa-road';

  static CIRCUIT_NAME_FIELD_LABEL = 'Nom du circuit';
  static CIRCUIT_DESCRIPTION_FIELD_LABEL = 'Description';
  static NO_CIRCUIT_FOR_PROJECT_TXT = `Ce projet n'a pas été envoyé dans un circuit de validation`;
  static REJECT_PROJECT_BUTTON_TITLE = 'Refuser';
  static EMERGENCY_VALIDATION_BUTTON_ICON = 'fa ls-icon-valider-en-urgence';
  static VALIDATING_ICON = 'far fa-clock';
  static EDITING_ICON = 'far fa-edit';
  static UNKNOWN_STATE_ICON = 'fas fa-question';
  static CANCELLED_STATE_ICON = 'fas fa-times-circle';

  // Sign project manually modal
  static SIGN_PROJECT_MODAL_TITLE = 'Déclaration de signature';
  static SIGN_PROJECT_MODAL_INFO = 'Pensez à vérifier la version du document pdf.';
  static SIGN_PROJECT_MODAL_REPLACE_BUTTON = 'Remplacer le document';
  static SIGN_PROJECT_MODAL_REPLACE_BUTTON_ICON = 'fas fa-sync-alt';
  static SIGN_PROJECT_MODAL_DATE_INPUT_LABEL = 'Date de signature';
  static SIGN_PROJECT_MODAL_DATE_INPUT_PLACE_HOLDER = 'Sélectionnez une date';
  static SIGN_PROJECT_MODAL_ACTION_BUTTON_TEXT = 'Déclarer signé';
  static SIGN_PROJECT_MODAL_ACTION_BUTTON_ICON = 'fas fa-paper-plane';

  // Send project to IParapheur
  static SEND_PROJECT_TO_I_PARAPHEUR_MODAL_TITLE = 'Envoi au i-Parapheur';
  static SEND_PROJECT_TO_I_PARAPHEUR_MODAL_ACTION_BUTTON_TEXT = 'Envoyer';
  static SEND_PROJECT_TO_I_PARAPHEUR_MODAL_ACTION_BUTTON_ICON = ProjectsMessages.SIGN_PROJECT_MODAL_ACTION_BUTTON_ICON;
  static SEND_PROJECT_TO_I_PARAPHEUR_MODAL_CIRCUIT_INPUT_LABEL = 'Circuit';
  static SEND_PROJECT_TO_I_PARAPHEUR_MODAL_CIRCUIT_INPUT_TOOLTIP = 'Correspond aux sous-types déclarés dans le i-Parapheur';
  static SEND_PROJECT_TO_I_PARAPHEUR_MODAL_CIRCUIT_INPUT_PLACE_HOLDER = 'Sélectionner un circuit';

  static CHECK_IPARAPHEUR_STATUS_ACTION_NAME = 'Vérifier le statut de la signature';

  static CHECK_IPARAPHEUR_STATUS_ACTION_ICON = 'fas fa-history';

  static SHOW_I_PARAPHEUR_REFUSAL_ACTION_NAME = 'Visualiser le motif du refus';
  static SHOW_I_PARAPHEUR_REFUSAL_ACTION_ICON = 'fas fa-exclamation-triangle';
  static SHOW_I_PARAPHEUR_REFUSAL_ACTION_STYLE = Style.WARNING;
  static SHOW_I_PARAPHEUR_REFUSAL_BUTTON_ICON = CommonIcons.SHOW_ICON;
  static I_PARAPHEUR_REFUSAL_INFO_TEXT = 'Projet refusé par le i-parapheur.';

  static SIGN_PROJECT_MODAL_ERROR = `Échec de la déclaration de signature.`;
  static HISTORY = 'Historique';
  static HISTORY_DEFAULT_FILE_NAME = 'historique.xml';

  static DECLARE_SIGNED_ICON = 'fa ls-icon-declare-signe';
  static I_PARAPHEUR_SIGNED_ICON = 'fa ls-icon-signe-i-parapheur';
  static I_PARAPHEUR_REFUSED_ICON = 'fa ls-icon-refus-i-parapheur';
  static I_PARAPHEUR_SIGNING_ICON = 'fa ls-icon-en-cours-i-parapheur';
  static I_PARAPHEUR_ERROR_ICON = 'fa ls-icon-erreur-i-parapheur';
  // noinspection SpellCheckingInspection
  static TO_BE_TRANSMITTED_ICON = 'fa ls-icon-a-teletransmettre';
  static BEING_SENT_TO_TDT_ICON = 'fa ls-icon-en-cours-depot-tdt';
  static TRANSMISSION_READY_ICON = 'fa ls-icon-a-ordonner';
  static TRANSMITTING_ICON = 'fa ls-icon-en-cours-depot-tdt';
  static TRANSMITTING_ERROR_ICON = 'fa ls-icon-erreur-tdt';
  static CANCELLING_ICON = 'fa ls-icon-en-cours-annulation';

  static ADD_DOCUMENT_TYPE_ERROR_NOTIFICATION_TITLE = 'Format de fichier incorrect';
  static VOTE_PROJECT_ACTION_NAME = 'Voter';
  static VOTE_PROJECT_ACTION_ICON = 'fas fa-gavel';
  static VOTE_PROJECT_ACTION_STYLE = Style.NORMAL;

  // File text upload
  static FORM_FIELD_PROJECT_TEXT_PROJECT_FILE_LABEL = 'Texte de projet';
  static FORM_FIELD_PROJECT_TEXT_PROJECT_FILE_BUTTON_TEXT = 'Ajouter un fichier de texte de projet';

  static FORM_FIELD_PROJECT_TEXT_ACT_FILE_LABEL = 'Texte d\'acte';
  static FORM_FIELD_PROJECT_TEXT_ACT_FILE_BUTTON_TEXT = 'Ajouter un fichier de texte d\'Acte';

  static DOWNLOAD_GENERATE_PROJECT = 'Télécharger le document de projet';
  static GENERATE_ACT_NUMBER = 'Générer le numéro d\'acte';
  static GENERATE_ACT_NUMBER_ICON = 'fas fa-cog';
  static GENERATE_ACT_NUMBER_MODAL_TITLE = 'Numéro d\acte';
  static GENERATE_ACT_NUMBER_SUCCESS = 'Numéro d\'acte généré avec succès';
  static GENERATE_ACT_NUMBER_ERROR = 'Erreur lors de la génération du numéro d\'acte';
  static CONFIRM_DOWNLOAD_TITLE = 'Confirmation de téléchargement';

  static save_success(state: StatesAct) {
    return `Projet d'acte créé avec succès\u00a0! Il se trouve dans la liste Projets ${state.name}s.`;
  }

  static sign_project_modal_success(project: Project) {
    return `Signature de "${project.name}" déclarée avec succès\u00a0!`;
  }

  static send_To_IParapheur_modal_success(project: Project) {
    return `L'Acte "${project.name}" envoyé au i-Parapheur avec succès\u00a0!`;
  }

  static add_document_format(fileTypes: FileType[], docType: string): string {
    const docTypeString = docType === ProjectsMessages.MAIN_DOCUMENT ? 'le document principal' : 'les annexes';
    if (docTypeString === 'le document principal') {
      const message = fileTypes.length === 1
        ? `Le format autorisé pour ${docTypeString} est\u00a0: `
        : `Les formats autorisés pour ${docTypeString} sont\u00a0: `;
      return message + fileTypes.map(fileType => FileUtils.getFileExtension(fileType)).join(', ');
    } else {
      return `Les formats autorisés pour ${docTypeString} sont\u00a0: ${Config.annexesAcceptedTypes.map(fileType => FileUtils.getFileExtension(fileType)).join(', ')}.<br><br>Les formats autorisés pour ${docTypeString} <i>télétransmissibles</i> sont\u00a0: ${Config.annexesTransferableAcceptedTypes.map(fileType => FileUtils.getFileExtension(fileType)).join(', ')}.<br><br>Les formats autorisés pour ${docTypeString} <i>joignables à la fusion</i> sont\u00a0: ${Config.annexesMergeableAcceptedTypes.map(fileType => FileUtils.getFileExtension(fileType)).join(', ')}.`;
    }
  }

  static too_big(size: number): string {
    return `Le document sélectionné ne peut pas être chargé \
      car le poids total de l'acte ne doit pas dépasser les ${CommonMessages.getHumanlyReadableSizeInMo(size)}`;
  }

  static annex_too_big(size: number): string {
    return `L'un des documents sélectionnés ne peut pas être chargé \
      car le poids total de l'acte ne doit pas dépasser les ${CommonMessages.getHumanlyReadableSizeInMo(size)}`;
  }

  static sittingDate(date: Date): string {
    return `du ${new DatePipe('fr').transform(date, 'dd/MM/yyyy à HH:mm')}`;
  }

  static getFileInformation(document: WADocument) {
    const hasProblem = document.hasGenerationProblem('generationProblem');
    return !hasProblem || hasProblem.message !== ''
      ? hasProblem.message + ` \n ${new DatePipe('fr').transform(hasProblem.modified, 'dd/MM/yyyy HH:mm')}`
      : !document || !document.name
        ? 'En cours de génération… '
        : document.name + (document.size ? ` (${CommonMessages.getHumanlyReadableSizeInMo(document.size)})` : '');

  }

  static getFileModifiedDate(document: WADocument) {
    return document && document.modified
      ? `modifié le ${new DatePipe('fr').transform(document.modified, 'dd/MM/yyyy')}`
      : '';
  }

  static validate_project_success_msg(projectName: string): string {
    return `Projet '${projectName}' validé avec succès`;
  }

  static reject_project_success_msg(projectName: string): string {
    return `Projet '${projectName}' refusé avec succès`;
  }

  static validate_project_error_msg(projectName: string): string {
    return `Une erreur est survenue lors de la validation du projet '${projectName}'`;
  }

  static reject_project_error_msg(projectName: string): string {
    return `Une erreur est survenue lors du refus du projet '${projectName}'`;
  }

  static build_confirm_delete_single_project_message(projectName: string): string {
    return `Êtes-vous sûr de vouloir supprimer le projet "${projectName}"?`;
  }

  static build_confirm_delete_multiple_projects_message(numberOfProject: string | number): string {
    return `Êtes-vous sûr de vouloir supprimer ${numberOfProject} projets?`;
  }

  static formFieldDraftTemplateFileHelp(size: number): string {
    return `La taille recommandée pour afficher correctement une génération est de ${size} Mo.`;
  }

  static downloadProjectConfirm(name: string, reason: string): string {
    // tslint:disable-next-line:max-line-length
    return `La génération a échoué précédemment pour la raison : </br> ${reason} </br> <br/> Voulez-vous quand même télécharger le projet ${name} ? </br>`;
  }
}
