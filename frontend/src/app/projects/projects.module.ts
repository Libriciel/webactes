import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared';
import { CreateProjectComponent } from './components/manage-project/create-project/create-project.component';
import { ProjectsRoutingModule } from './routing/projects-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LibricielCommonModule } from '../ls-common/libriciel-common.module';
import { ProjectComponent } from './components/project/project.component';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { WaCommonModule } from '../wa-common/wa-common.module';
import { RouterModule } from '@angular/router';
import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import { EditProjectComponent } from './components/manage-project/edit-project/edit-project.component';
import { CircuitsModule } from '../admin/circuits/circuits.module';
import { SelectCircuitModalComponent } from './components/modals/select-circuit-modal/select-circuit-modal.component';
import { SignProjectModalComponent } from './components/modals/sign-project-modal/sign-project-modal.component';
import { ValidateStepModalComponent } from './components/modals/validate-step-modal/validate-step-modal.component';
import { EmergencyProjectValidationModalComponent } from './components/modals/emergency-project-validation-modal/emergency-project-validation-modal.component';
import { RejectProjectModalComponent } from './components/modals/reject-project-modal/reject-project-modal.component';
import { MainDocumentComponent } from './components/main-document/main-document.component';
import { LsComposantsModule } from '@libriciel/ls-composants';
import { ProjectGeneralInformationComponent } from './components/forms/project-general-information/project-general-information.component';
import { ProjectComplementaryInformationComponent } from './components/forms/project-complementary-information/project-complementary-information.component';
import { ProjectDocumentsComponent } from './components/forms/project-documents/project-documents.component';
import { SelectDateComponent } from './components/modals/sign-project-modal/components/select-date/select-date.component';
import { SelectIParapheurCircuitComponent } from './components/modals/sign-project-modal/components/select-iparapheur-circuit/select-iparapheur-circuit.component';
import { IParapheurRefusalModalComponent } from './components/modals/iparapheur-refusal-modal/iparapheur-refusal-modal.component';
import { ProjectHistoryComponent } from './components/history/project-history.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [
    CreateProjectComponent,
    ProjectComponent,
    ProjectsListComponent,
    ProjectGeneralInformationComponent,
    ProjectComplementaryInformationComponent,
    EditProjectComponent,
    ProjectGeneralInformationComponent,
    ProjectDocumentsComponent,
    EditProjectComponent,
    SignProjectModalComponent,
    SelectCircuitModalComponent,
    ValidateStepModalComponent,
    MainDocumentComponent,
    ValidateStepModalComponent,
    EmergencyProjectValidationModalComponent,
    RejectProjectModalComponent,
    SelectDateComponent,
    SelectIParapheurCircuitComponent,
    IParapheurRefusalModalComponent,
    ProjectHistoryComponent,
  ],
  exports: [
    ProjectsListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ProjectsRoutingModule,
    LibricielCommonModule,
    NgSelectModule,
    CdkStepperModule,
    NgbModule,
    WaCommonModule,
    RouterModule,
    NgxTrimDirectiveModule,
    ReactiveFormsModule,
    CircuitsModule,
    LsComposantsModule,
    MatRadioModule,
    MatTableModule,
    MatSortModule,
    SharedModule,
    NgxSpinnerModule
  ]
})
export class ProjectsModule {
}
