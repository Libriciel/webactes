import { ProjectsRoutingMessages } from './projects-routing-messages';
import { RoutingPaths } from '../../wa-common/routing-paths';
import { BreadcrumbItem } from '../../wa-common/model/breadcrumb-item';
import { SittingsRoutingMessages } from '../../sittings/routing/sittings-routing-messages';

export class ProjectsBreadcrumb {

  static PROJECT_BREADCRUMB = {name: ProjectsRoutingMessages.PAGE_PROJECTS_TITLE};
  static PathBreadcrumb = new Map<string, BreadcrumbItem[]>([
    [RoutingPaths.PROJECTS_PATH, [
      ProjectsBreadcrumb.PROJECT_BREADCRUMB]],
    [RoutingPaths.PROJECTS_DRAFTS_PATH, [
      ProjectsBreadcrumb.PROJECT_BREADCRUMB, {
        name: ProjectsRoutingMessages.PAGE_PROJECTS_DRAFT_TITLE,
        location: RoutingPaths.PROJECTS_DRAFTS_PATH
      }]],
    [RoutingPaths.PROJECTS_CREATE_PATH, [
      ProjectsBreadcrumb.PROJECT_BREADCRUMB, {
        name: ProjectsRoutingMessages.PAGE_PROJECTS_CREATE_TITLE,
        location: RoutingPaths.PROJECTS_CREATE_PATH
      }]],
    [RoutingPaths.PROJECTS_VALIDATING_PATH, [
      ProjectsBreadcrumb.PROJECT_BREADCRUMB, {
        name: ProjectsRoutingMessages.PAGE_PROJECTS_VALIDATING_TITLE,
        location: RoutingPaths.PROJECTS_VALIDATING_PATH
      }]],
    [RoutingPaths.PROJECTS_TO_BE_VALIDATED_PATH, [
      ProjectsBreadcrumb.PROJECT_BREADCRUMB, {
        name: ProjectsRoutingMessages.PAGE_PROJECTS_TO_BE_VALIDATED_TITLE,
        location: RoutingPaths.PROJECTS_TO_BE_VALIDATED_PATH
      }]],
    [RoutingPaths.PROJECTS_VALIDATED_PATH, [
      ProjectsBreadcrumb.PROJECT_BREADCRUMB, {
        name: ProjectsRoutingMessages.PAGE_PROJECTS_VALIDATED_TITLE,
        location: RoutingPaths.PROJECTS_VALIDATED_PATH
      }]],
    [RoutingPaths.PROJECTS_WITHOUT_SITTING_PATH, [
      ProjectsBreadcrumb.PROJECT_BREADCRUMB, {
        name: ProjectsRoutingMessages.PAGE_PROJECTS_WITHOUT_SITTING_TITLE,
        location: RoutingPaths.PROJECTS_WITHOUT_SITTING_PATH
      }]],
    [RoutingPaths.SITTING_SHOW_PATH, [
      ProjectsBreadcrumb.PROJECT_BREADCRUMB, {
        name: SittingsRoutingMessages.PAGE_SITTINGS_RUNNING_TITLE,
        location: RoutingPaths.SITTING_RUNNING_PATH
      }]]
  ]);
}
