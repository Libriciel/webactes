export class ProjectsRoutingMessages {
  static PAGE_PROJECTS_TITLE = `Mes projets d'acte`;
  static PAGE_PROJECTS_DRAFT_TITLE = 'Brouillons';
  static PAGE_PROJECTS_TO_BE_VALIDATED_TITLE = 'À traiter';
  static PAGE_PROJECTS_VALIDATING_TITLE = 'En cours dans un circuit';
  static PAGE_PROJECTS_VALIDATED_TITLE = 'Validés';
  static PAGE_PROJECTS_CREATE_TITLE = `Création d'un projet d'acte`;

  static PAGE_PROJECTS_WITHOUT_SITTING_TITLE = 'Projets sans séance';
  static PAGE_PROJECTS_VOTERS_LIST = 'Liste des élus votants';
  static PAGE_PROJECT_VOTING = 'Saisie des votes pour le projet :';

  static pageProjectVisualizationTitle(projectName: string) {
    return `${projectName}`;
  }

  static pageProjectEditionTitle(projectName: string) {
    return `Modification de ${projectName}`;
  }

  static pageProjectVotingTitle(projectName: string) {
    return ProjectsRoutingMessages.PAGE_PROJECT_VOTING + ` ${projectName}`;
  }
}
