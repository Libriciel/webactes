import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsListComponent } from '../components/projects-list/projects-list.component';
import { RoutingPaths } from '../../wa-common/routing-paths';
import { ProjectsRoutingMessages } from './projects-routing-messages';
import { CreateProjectComponent } from '../components/manage-project/create-project/create-project.component';
import { PageParameters } from '../../wa-common/components/page/model/page-parameters';
import { ProjectComponent } from '../components/project/project.component';
import { EditProjectComponent } from '../components/manage-project/edit-project/edit-project.component';
import { ProjectResolver } from './resolvers/project-resolver';
import { PageParametersResolver } from './resolvers/page-parameters-resolver';
import { ProjectDisplayState } from 'src/app/model/project/project-display-state.enum';
import { ProjectsBreadcrumb } from './projects-breadcrumb';
import { SittingProjectsVotesResolver } from '../../sittings/routing/resolvers/sitting-projects-votes-resolver';
import { VoteComponent } from '../../vote/components/vote/vote.component';

const routes: Routes = [
  {
    path: RoutingPaths.PROJECTS_PATH,
    redirectTo: RoutingPaths.PROJECTS_DRAFTS_PATH,
    pathMatch: 'full'
  },
  {
    path: RoutingPaths.PROJECTS_DRAFTS_PATH,
    component: ProjectsListComponent,
    data: {
      pageParameters: {
        title: ProjectsRoutingMessages.PAGE_PROJECTS_DRAFT_TITLE,
        breadcrumb: ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_DRAFTS_PATH),
        fluid: true
      } as PageParameters,
      projectState: ProjectDisplayState.DRAFT as ProjectDisplayState
    }
  }, {
    path: RoutingPaths.PROJECTS_CREATE_PATH,
    component: CreateProjectComponent,
    data: {
      pageParameters: {
        title: ProjectsRoutingMessages.PAGE_PROJECTS_CREATE_TITLE,
        breadcrumb: ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_CREATE_PATH)
      } as PageParameters
    }
  }, {
    path: RoutingPaths.PROJECTS_VALIDATING_PATH,
    component: ProjectsListComponent,
    data: {
      pageParameters: {
        title: ProjectsRoutingMessages.PAGE_PROJECTS_VALIDATING_TITLE,
        breadcrumb: ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_VALIDATING_PATH),
        fluid: true
      } as PageParameters,
      projectState: ProjectDisplayState.VALIDATING as ProjectDisplayState
    },
  }, {
    path: RoutingPaths.PROJECTS_TO_BE_VALIDATED_PATH,
    component: ProjectsListComponent,
    data: {
      pageParameters: {
        title: ProjectsRoutingMessages.PAGE_PROJECTS_TO_BE_VALIDATED_TITLE,
        breadcrumb: ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_TO_BE_VALIDATED_PATH),
        fluid: true
      } as PageParameters,
      projectState: ProjectDisplayState.TO_BE_VALIDATED as ProjectDisplayState
    },
  }, {
    path: RoutingPaths.PROJECTS_VALIDATED_PATH,
    component: ProjectsListComponent,
    data: {
      pageParameters: {
        title: ProjectsRoutingMessages.PAGE_PROJECTS_VALIDATED_TITLE,
        breadcrumb: ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_VALIDATED_PATH),
        fluid: true
      } as PageParameters,
      projectState: ProjectDisplayState.VALIDATED as ProjectDisplayState
    }
  }, {
    path: RoutingPaths.PROJECTS_WITHOUT_SITTING_PATH,
    component: ProjectsListComponent,
    data: {
      pageParameters: {
        title: ProjectsRoutingMessages.PAGE_PROJECTS_WITHOUT_SITTING_TITLE,
        breadcrumb: ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_WITHOUT_SITTING_PATH),
        fluid: true
      } as PageParameters,
      projectState: ProjectDisplayState.UNASSOCIATED as ProjectDisplayState
    }
  }, {
    path: RoutingPaths.PROJECTS_SHOW_PATH,
    component: ProjectComponent,
    resolve: {
      pageParameters: PageParametersResolver,
      project: ProjectResolver
    }
  }, {
    path: RoutingPaths.PROJECTS_EDITION_PATH,
    component: EditProjectComponent,
    resolve: {
      pageParameters: PageParametersResolver,
      project: ProjectResolver
    }
  }, {
    path: RoutingPaths.projectVotingPath(':projectId', ':sittingId'),
    component: VoteComponent,
    resolve: {
      data: SittingProjectsVotesResolver,
      pageParameters: PageParametersResolver,
    },
    data: {
      projectState: ProjectDisplayState.VALIDATED_INTO_SITTING as ProjectDisplayState
    }
  }];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ProjectsRoutingModule {
}
