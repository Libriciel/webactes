import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Project } from '../../../model/project/project';
import { Observable } from 'rxjs';
import { IManageProjectsService } from '../../services/i-manage-projects.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectResolver implements Resolve<Project> {
  constructor(protected iManageProjectService: IManageProjectsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project> {
    return this.iManageProjectService.getProject(route.paramMap.get('projectId'));
  }
}
