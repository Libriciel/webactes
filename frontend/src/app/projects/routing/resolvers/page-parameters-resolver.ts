import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { Observable, of } from 'rxjs';
import { ProjectsRoutingMessages } from '../projects-routing-messages';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { IManageProjectsService } from '../../services/i-manage-projects.service';
import { map } from 'rxjs/operators';
import { ProjectsBreadcrumb } from '../projects-breadcrumb';
import { BreadcrumbItem } from '../../../wa-common/model/breadcrumb-item';
import { ActsBreadcrumbs } from '../../../acts/routing/acts-breadcrumbs';
import { ProjectDisplayState } from '../../../model/project/project-display-state.enum';
import { SittingsRoutingMessages } from '../../../sittings/routing/sittings-routing-messages';

@Injectable({
  providedIn: 'root'
})
export class PageParametersResolver implements Resolve<PageParameters> {

  previousBreadcrumb: BreadcrumbItem[];

  constructor(protected  iManageProjectService: IManageProjectsService, protected  router: Router) {
  }

  // Compute breadcrumb in case of showing or editing a project or act
  // Depends of parameters passed by navigation or the state of the project or act
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
  // eslint-disable-next-line @typescript-eslint/type-annotation-spacing
    : Observable<PageParameters> | Promise<PageParameters> | PageParameters {

    return this.iManageProjectService.getProject(route.params['projectId']).pipe(map(project => {
        // Retrieve first part of breadcrumb if available
        this.previousBreadcrumb = this.router.getCurrentNavigation().extras.state
        && this.router.getCurrentNavigation().extras.state.originalContext
          ? this.router.getCurrentNavigation().extras.state.originalContext.breadcrumb : null;

        // Compute first part of breadcrumb in function of the state of the project
        if (!this.previousBreadcrumb) {
          this.previousBreadcrumb = ProjectsBreadcrumb.PathBreadcrumb.get(project.canonicalProjectDisplayState);
          let state = project.canonicalProjectDisplayState ? project.canonicalProjectDisplayState : route.data.projectState as ProjectDisplayState;
          switch (state) {
            case ProjectDisplayState.DRAFT:
              this.previousBreadcrumb = ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_DRAFTS_PATH);
              break;
            case ProjectDisplayState.TO_BE_VALIDATED:
              this.previousBreadcrumb = ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_TO_BE_VALIDATED_PATH);
              break;
            case ProjectDisplayState.VALIDATING:
              this.previousBreadcrumb = ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_VALIDATING_PATH);
              break;
            case ProjectDisplayState.VALIDATED:
              this.previousBreadcrumb = ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.PROJECTS_VALIDATED_PATH);
              break;
            case ProjectDisplayState.TO_BE_TRANSMITTED:
              this.previousBreadcrumb = ActsBreadcrumbs.PathBreadcrumb.get(RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_TDT_PATH);
              break;
            case ProjectDisplayState.TRANSMISSION_READY:
              this.previousBreadcrumb = ActsBreadcrumbs.PathBreadcrumb.get(RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_PREFECTURE_PATH);
              break;
            case ProjectDisplayState.ACT:
              this.previousBreadcrumb = ActsBreadcrumbs.PathBreadcrumb.get(RoutingPaths.ACTS_PATH_LIST_PATH);
              break;
            case ProjectDisplayState.UNASSOCIATED:
              break;
            case ProjectDisplayState.VALIDATED_INTO_SITTING:
              this.previousBreadcrumb =ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.SITTING_SHOW_PATH);
              break;
            case ProjectDisplayState.VOTED:
              this.previousBreadcrumb =ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.SITTING_SHOW_PATH);
              break;
          }
        }
        const breadcrumbEdition = this.previousBreadcrumb.concat([{
          name: ProjectsRoutingMessages.pageProjectEditionTitle(project.name),
          location: RoutingPaths.projectEditionPath(route.params['projectId'])
        }]);
        const breadcrumbVisualization = this.previousBreadcrumb.concat([{
          name: ProjectsRoutingMessages.pageProjectVisualizationTitle(project.name),
          location: RoutingPaths.projectPath(route.params['projectId'])
        }]);

        const breadcrumbVoting = this.previousBreadcrumb.concat([{
          name: SittingsRoutingMessages.PAGE_SITTING_SHOW_TITLE,
          location: RoutingPaths.sittingPath(route.params['sittingId'])
        }]);

        const breadcrumbSitting = this.previousBreadcrumb.concat([{
          name: SittingsRoutingMessages.PAGE_SITTING_SHOW_TITLE,
          location: RoutingPaths.sittingPath(route.params['sittingId'])
        }]);

        const routeParameterMap = new Map<string, PageParameters>([
          [RoutingPaths.PROJECTS_EDITION_PATH, {
            title: ProjectsRoutingMessages.pageProjectEditionTitle(project.name),
            breadcrumb: breadcrumbEdition
          }],
          [RoutingPaths.PROJECTS_SHOW_PATH, {
            title: ProjectsRoutingMessages.pageProjectVisualizationTitle(project.name),
            breadcrumb: breadcrumbVisualization
          }],
          [RoutingPaths.SITTING_SHOW_PATH, {
            title: ProjectsRoutingMessages.PAGE_PROJECT_VOTING,
            breadcrumb: breadcrumbSitting
          }],
          [RoutingPaths.projectVotingPath(':projectId',':sittingId'), {
            title: ProjectsRoutingMessages.pageProjectVotingTitle(project.name),
            breadcrumb: breadcrumbVoting
          }]
        ]);

        return routeParameterMap.get(route.routeConfig.path);
      }
    ));
  }
}
