import { Observable } from 'rxjs';
import { TypesAct } from '../../model/type-act/types-act';
import { StatesAct } from '../../model/states-act';
import { Project } from '../../model/project/project';
import { JoinDocumentType } from '../../model/join-document-type';
import { ActTeletransmissionInformation } from 'src/app/model/act-teletransmission-information';
import { ProjectDocumentsInformation } from '../components/forms/project-documents/project-documents-information';
import { Pagination } from '../../model/pagination';

export abstract class IManageProjectsService {

  abstract getAllTypesActs(): Observable<{ typeacts: TypesAct[] }>;

  abstract getTypesActs(): Observable<{ typeacts: TypesAct[] }>;

  abstract getAllTypesActsActive(): Observable<{ typeacts: TypesAct[] }>;

  abstract getAllStatesActs(action?: string, id?: number): Observable<StatesAct[]>;

  abstract getJoinDocumentTypes(typeAct: TypesAct): Observable<{
    mainDocumentTypes: JoinDocumentType[],
    annexeDocumentTypes: JoinDocumentType[]
  }>;

  abstract addProject(project: Project): Observable<any>;

  abstract updateProject(project: Project): Observable<any>;

  abstract getProject(project_id): Observable<Project>;

  abstract getFile(doc_id: number, mimeType: string, name: string): Observable<any>;

  abstract checkCodeActUniq(code): Observable<boolean>;

  abstract checkDocumentsSize(project: Project | ProjectDocumentsInformation): boolean;

  abstract hasAcceptableMainDocumentTypeBeforeSigning(file: File): boolean;

  abstract hasAcceptableMainDocumentTypeForSigning(file: File): boolean;

  abstract getAnnexesFileExtensions(): string ;

  abstract validateProject(project: Project, comment: string): Observable<any>;

  abstract getMainDocumentFileExtensionsBeforeSigning(): string;

  abstract hasAcceptableAnnexeType(fileList: FileList): boolean;

  abstract manuallySignProject(project: Project, date: Date);

  abstract rejectProject(project: Project, comment: string): Observable<any>;

  abstract validateProjectInEmergency(project: Project, comment: string): Observable<any>;

  abstract deleteProjects(projects: Project[]): Observable<any>;

  abstract updateTeletransmissionInfo(projectId: number, info: ActTeletransmissionInformation): Observable<Project>;

  abstract getStampedAct(projectId: number, mimeType: string, fileName: string): Observable<any>;

  abstract getActAR(projectId: number, mimeType: string, fileName: string): Observable<any>;

  abstract getBordereau(projectId: number, mimeType: string, fileName: string): Observable<any>;

  abstract getStampedAnnex(annexId: number, projectId: number, mimeType: string, fileName: string): Observable<any>;

  abstract getIParapheurSignedMainDocument(projectId: number, mimeType: string, fileName: string): Observable<any>;

  abstract getIParapheurSignedFormDocument(projectId: number, mimeType: string, fileName: string): Observable<any>;

  abstract getIParapheurHistory(projectId: number, mimeType: string, fileName: string): Observable<any>;

  abstract getDownloadProject(projectId: number, mimeType: string, fileName: string): Observable<any>;

  abstract getDownloadAct(projectId: number, mimeType: string, fileName: string): Observable<any>;

  abstract generateActNumber(projectId: number): Observable<{ success: boolean, generatedActNumber: string }>;

}
