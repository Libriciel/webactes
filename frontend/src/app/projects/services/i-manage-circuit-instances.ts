import { Project } from 'src/app/model/project/project';
import { Circuit } from 'src/app/model/circuit/circuit';
import { Observable } from 'rxjs';
import { CircuitInstance } from 'src/app/model/circuit/circuit-instance';

export abstract class IManageCircuitInstanceService {

  abstract sendProjectIntoCircuit(project: Project, circuit: Circuit): Observable<{ createdInstanceId: number }>;

  abstract validateCurrentStep(circuitInstance: CircuitInstance, comment: string): Observable<any>;

  abstract rejectCurrentStep(circuitInstance: CircuitInstance, comment: string): Observable<any>;

  abstract validateProjectInEmergency(project: Project, comment: string): Observable<any>;
}
