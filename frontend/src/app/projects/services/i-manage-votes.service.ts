import { Observable } from 'rxjs';
import { Vote } from '../../model/vote/vote';

export abstract class IManageVotesService {

 abstract add(vote: Vote, method: string, projectId: number): Observable<any>;

}
