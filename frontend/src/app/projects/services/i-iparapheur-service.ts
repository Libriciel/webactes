import { Project } from '../../model/project/project';
import { Observable } from 'rxjs';

export abstract class IIParapheurService {

  abstract getIParapheurCircuits(project: Project): Observable<any>;

  abstract sendToIParapheur(project: Project, iparapheurCircuitId: string): Observable<any>;

  abstract getIParapheurStatus(project: Project): Observable<{ status: string, annotation: string }>;
}
