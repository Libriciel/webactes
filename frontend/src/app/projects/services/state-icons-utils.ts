import { StatesAct } from 'src/app/model/states-act';
import { StateActCode } from 'src/app/model/state-act-code.enum';
import { ProjectsMessages } from '../i18n/projects-messages';
import { CommonIcons } from '../../ls-common/icons/common-icons';

export class StateIconsUtils {

  static getIconClassForState(state: StatesAct): string {
    let result = ProjectsMessages.UNKNOWN_STATE_ICON;
    if (!state) {
      return result;
    }

    switch (state.code) {
      case StateActCode.DRAFT:
        result = ProjectsMessages.EDITING_ICON;
        break;
      case StateActCode.VALIDATING:
        result = ProjectsMessages.VALIDATING_ICON;
        break;
      case StateActCode.VALIDATED:
        result = CommonIcons.VALIDATED_ICON;
        break;
      case StateActCode.REFUSED:
        result = CommonIcons.REJECTED_ICON;
        break;
      case StateActCode.DECLARED_SIGNED:
        result = ProjectsMessages.DECLARE_SIGNED_ICON;
        break;
      case StateActCode.I_PARAPHEUR_SIGNED:
        result = ProjectsMessages.I_PARAPHEUR_SIGNED_ICON;
        break;
      case StateActCode.I_PARAPHEUR_REFUSED:
        result = ProjectsMessages.I_PARAPHEUR_REFUSED_ICON;
        break;
      case StateActCode.I_PARAPHEUR_SIGNING:
      case StateActCode.I_PARAPHEUR_SENDING:
        result = ProjectsMessages.I_PARAPHEUR_SIGNING_ICON;
        break;
      case StateActCode.I_PARAPHEUR_ERROR:
        result = ProjectsMessages.I_PARAPHEUR_ERROR_ICON;
        break;
      case StateActCode.TO_BE_TRANSMITTED:
        result = ProjectsMessages.TO_BE_TRANSMITTED_ICON;
        break;
      case StateActCode.BEING_SENT_TO_TDT:
        result = ProjectsMessages.BEING_SENT_TO_TDT_ICON;
        break;
      case StateActCode.TRANSMISSION_READY:
        result = ProjectsMessages.TRANSMISSION_READY_ICON;
        break;
      case StateActCode.TRANSMITTING:
        result = ProjectsMessages.TRANSMITTING_ICON;
        break;
      case StateActCode.SETTLED:
        result = CommonIcons.SIGNED_ACTS_ICON;
        break;
      case StateActCode.CANCELLED:
        result = ProjectsMessages.CANCELLED_STATE_ICON;
        break;
      case StateActCode.TRANSMITTING_ERROR:
        result = ProjectsMessages.TRANSMITTING_ERROR_ICON;
        break;
      case StateActCode.CANCELLING:
        result = ProjectsMessages.CANCELLING_ICON;
        break;
      case StateActCode.VOTED_REFUSED:
      case StateActCode.VOTED_APPROVED:
      case StateActCode.TAKE_NOTE:
        result = ProjectsMessages.VOTE_PROJECT_ACTION_ICON;
        break;
      default:
        break;
    }
    return 'fa-lg ' + result;
  }
}
