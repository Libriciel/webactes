import { WADocument } from './wa-document';
import { JoinDocumentType } from '../../model/join-document-type';

export class Annexe extends WADocument {
  toGotTransfer: boolean;
  joinGenerate?: boolean;
  rank?: number;

  constructor(annexe: {
    file?: File,
    waId?: number,
    fileId?: number,
    rank?: number,
    joinGenerate?: boolean,
    toGotTransfer?: boolean,
    documentType?: JoinDocumentType,
    codeType?: string,
    name: string,
    mimeType: string,
    size: number,
    hasGenerationProblem?: { value: boolean, message: string, date: Date },
  }) {
    super({
      file: annexe.file,
      waId: annexe.waId,
      fileId: annexe.fileId,
      documentType: annexe.documentType,
      codeType: annexe.codeType,
      name: annexe.name,
      size: annexe.size,
      mimeType: annexe.mimeType
    });
    this.rank = annexe.rank;
    this.joinGenerate = annexe.joinGenerate;
    this.toGotTransfer = annexe.toGotTransfer;
    this.setHasGenerationProblem('generationProblem', annexe.hasGenerationProblem);
  }
}
