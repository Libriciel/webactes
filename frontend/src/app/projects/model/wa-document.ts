import { HasId, HasName } from '../../ls-common';
import { JoinDocumentType } from '../../model/join-document-type';
import { HasGenerationProblem } from '../../ls-common/model/has-generation-problem';

export class WADocument extends HasGenerationProblem implements HasId, HasName {
  id: number;
  fileId?: number;
  name: string;
  size: number;
  mimeType: string;
  file: File;
  documentType: JoinDocumentType;
  codeType: string;
  modified?: Date;

  constructor(waDocument: {
    file?: File,
    waId?: number,
    fileId?: number,
    documentType?: JoinDocumentType,
    name: string,
    size: number,
    codeType?: string,
    mimeType: string,
    modified?: Date,
    generationerror?: { message: string, date: Date },
  }) {
    super();
    this.id = waDocument.waId;
    this.fileId = waDocument.fileId;
    this.file = waDocument.file;
    this.name = waDocument.name;
    this.size = waDocument.size;
    this.mimeType = waDocument.mimeType;
    this.documentType = waDocument.documentType;
    this.modified = waDocument.modified;
    this.codeType = waDocument.codeType ? waDocument.codeType : (waDocument.documentType ? waDocument.documentType.codeType : null);
    this.setHasGenerationProblem(
      'generationProblem',
      waDocument.generationerror
    );
  }
}
