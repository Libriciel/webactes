import { FileType } from './utility/Files/file-types.enum';
import { StateActCode } from './model/state-act-code.enum';

export class Config {
  static baseUrl = '';
  static MAX_PROJECT_SIZE = 100 * 1024 * 1024;
  static mainDocumentAcceptedTypesForSigning = [
    FileType.PDF
  ];

  static mainDocumentAcceptedTypesBeforeSigning = [
    FileType.PDF, FileType.ODT, FileType.DOC, FileType.DOCX
  ];

  static headerImageAcceptedTypes = [
    FileType.PNG, FileType.JPG, FileType.GIF,
  ];

  static headerImageSize = 38;

  static annexesAcceptedTypes = [
    FileType.PDF,
    FileType.XML,
    FileType.PNG, FileType.JPG,
    FileType.ODT, FileType.ODS, FileType.ODP,
    FileType.DOC, FileType.XLS, FileType.PPT,
    FileType.DOCX, FileType.XLSX, FileType.PPTX];

  static annexesTransferableAcceptedTypes = [
    FileType.PDF,
    FileType.PNG, FileType.JPG
  ];

  static annexesMergeableAcceptedTypes = [
    FileType.PDF
  ];

  static TOASTER_TIMEOUT = 10000;

  static DEFAULT_PROJECT_CREATION_STATE_CODE = StateActCode.DRAFT;

  static MAX_RETRIEVABLE_ACTIVE_CIRCUITS = 150;

  static DRAFT_TYPE_PROJECT = 1;
  static DRAFT_TYPE_ACT = 2;
  static annexesFuseableAcceptedTypes = [
    FileType.PDF,
  ];

  static EMAIL_VALIDATION_REGEXP = /^[\p{L}0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[\p{L}0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[_\p{L}0-9][-_\p{L}0-9]*\.)*[\p{L}0-9][-\p{L}0-9]{0,62}\.(?:[a-z]{2}\.)?[a-z]{2,}$/ui;

  static LIMIT_PER_PAGE = 50;
}
