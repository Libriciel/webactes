export class SittingsMessages {
  static readonly SEND_SITTING_CONVOCATION_SUCCESS = 'Convocation envoyée avec succès';
  static readonly SEND_SITTING_CONVOCATION_ERROR = 'Erreur lors de l\'envoi de la convocation';
}
