import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { SittingsListMessages } from '../../sittings-list/SittingsListMessages';
import { FileType } from '../../../../utility/Files/file-types.enum';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AttachmentSummonInformation } from '../../../../model/summon/attachment-summon-Information';
import { AttachmentSummonFile, AttachmentSummonFilesService } from '../../../../core';
import { saveAs } from 'file-saver';
import { Action } from '../../../../ls-common/model/action';
import { Summon } from '../../../../model/summon/summon';
import { SummonAction } from '../../sittings-list/actions/summon-action.enum';
import { SummonActionItemFactory } from '../../sittings-list/summon-action-item-factory';
import { ActionResult, NotificationService } from '../../../../ls-common';
import { AttachmentSummonType } from '../../../../model/summon/attachment-sumon-type.enum';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'wa-manage-summon',
  templateUrl: './manage-summon.component.html',
  styleUrls: ['./manage-summon.component.scss']
})
export class ManageSummonComponent implements OnInit, OnDestroy {
  get summon(): Summon {
    return this._summon;
  }

  @Input()
  set summon(value: Summon) {
    this._summon = value;
    this.documentForm.patchValue({'convocation': this.getAttachmentConvocationFile()});
    this.documentForm.patchValue({'executiveSummary': this.getAttachmentNoteDeSyntheseFile()});
    this.documentsChanged();
  }

  private _summon: Summon;
  @Input()
  resetEvent: Observable<void>;
  private resetEventsSubscription: Subscription;

  readonly sittingListMessages = SittingsListMessages;
  readonly fileType = FileType;

  @Output()
  documentChanged = new EventEmitter<AttachmentSummonInformation>();
  @Output()
  documentChangedServerSide = new EventEmitter<void>();
  host: ElementRef;
  documentForm: FormGroup;
  generateSummonAction: Action<Summon>;

  constructor(private _formBuilder: FormBuilder,
              protected attachmentSummonFilesService: AttachmentSummonFilesService,
              protected notificationService: NotificationService,
              protected summonActionItemFactory: SummonActionItemFactory) {
    this.documentForm = this._formBuilder.group({
      convocation: ['', [Validators.required, this.requiredFileType('pdf')]],
      executiveSummary: ['', [this.requiredFileType('pdf')]],
    });
  }

  ngOnInit() {
    this.generateSummonAction = this.summonActionItemFactory.getActionItem(SummonAction.GENERATE_CONVOCATION);
    this.resetEventsSubscription = this.resetEvent.subscribe(() => {
      this.documentForm.patchValue({'convocation': this.getAttachmentConvocationFile()});
      this.documentForm.patchValue({'executiveSummary': this.getAttachmentNoteDeSyntheseFile()});
      this.documentsChanged(true);
    });
  }

  ngOnDestroy() {
    this.resetEventsSubscription.unsubscribe();
  }

  requiredFileType(type: string) {
    return function (control: FormControl) {
      const file = control.value;
      if (file) {
        const extension = file.name.split('.')[1].toLowerCase();
        return type.toLowerCase() !== extension.toLowerCase()
          ? {requiredFileType: true}
          : null;
      } else {
        return null;
      }
    };
  }


  downloadFile(attachmentSummonFile: AttachmentSummonFile) {
    if (!attachmentSummonFile.id) {
      saveAs(attachmentSummonFile, attachmentSummonFile.name);
    } else {
      this.attachmentSummonFilesService
        .getFile(attachmentSummonFile.id)
        .subscribe(blob => {
          const blobPart = new Blob([blob], {type: attachmentSummonFile.mimetype});
          saveAs(blobPart, attachmentSummonFile.name);
        });
    }
  }


  deleteFile() {
    if (this.host) {
      this.host.nativeElement.value = '';
    }
    this.documentForm.get(AttachmentSummonType.EXECUTIVE_SUMMARY).setValue(null);
    this.documentsChanged(true);
  }

  generationDone(actionResult: ActionResult) {
    if (actionResult.needReload) {
      this.documentChangedServerSide.emit();
    }
  }

  documentsChanged(forceDirty = false) {
    if (forceDirty || this.documentForm.dirty) {
      const documentInformations: AttachmentSummonInformation = {
        convocation: this.documentForm.get(AttachmentSummonType.CONVOCATION).value,
        noteDeSynthese: this.documentForm.get(AttachmentSummonType.EXECUTIVE_SUMMARY).value,
        valid: this.documentForm.valid
      };
      this.documentChanged.emit(documentInformations);
    }
  }

  private getAttachementSummon(rank: 1 | 2): AttachmentSummonFile {
    if (!this.summon.attachments_summons) {
      return undefined;
    }
    const attachmentSummon = this.summon.attachments_summons.find(attachmentSummon1 => attachmentSummon1.rank === rank);
    return attachmentSummon
      ? attachmentSummon.file
      : undefined;
  }

  private getAttachmentConvocationFile(): AttachmentSummonFile {
    return this.getAttachementSummon(1);
  }

  private getAttachmentNoteDeSyntheseFile(): AttachmentSummonFile {
    return this.getAttachementSummon(2);
  }

}
