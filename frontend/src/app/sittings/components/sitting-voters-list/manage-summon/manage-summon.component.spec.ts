import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSummonComponent } from './manage-summon.component';

describe('ManageSummonComponent', () => {
  let component: ManageSummonComponent;
  let fixture: ComponentFixture<ManageSummonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageSummonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSummonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
