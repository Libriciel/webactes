import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SittingVotersListComponent } from './sitting-voters-list.component';

describe('SittingVotersListComponent', () => {
  let component: SittingVotersListComponent;
  let fixture: ComponentFixture<SittingVotersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SittingVotersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SittingVotersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
