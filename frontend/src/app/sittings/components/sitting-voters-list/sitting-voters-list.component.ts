import { Component, OnInit } from '@angular/core';
import { SittingsListMessages } from '../sittings-list/SittingsListMessages';
import { ActionResult, CommonIcons, CommonMessages, NotificationService } from '../../../ls-common';
import { ActivatedRoute, Router } from '@angular/router';
import { Summon } from '../../../model/summon/summon';
import { IManageSummonService } from '../../../model/summon/imanage-summon-service';
import { Convocation } from '../../../model/summon/convocation';
import { FileType } from '../../../utility/Files/file-types.enum';
import { Weight } from '../../../wa-common/weight.enum';
import { SummonActionItemFactory } from '../sittings-list/summon-action-item-factory';
import { SummonAction } from '../sittings-list/actions/summon-action.enum';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { GeneralAction } from '../../../wa-common/actions/general-action.enum';
import { GeneralActionFactory } from '../../../wa-common/actions/general-action-factory';
import { Action } from '../../../ls-common/model/action';
import { ActionSet } from '../../../ls-common/model/action-set';
import { AttachmentSummonInformation } from '../../../model/summon/attachment-summon-Information';
import { AttachmentSummonFile, AttachmentSummonFilesService } from '../../../core';
import { forkJoin, Subject } from 'rxjs';
import { AttachmentSummonType } from '../../../model/summon/attachment-sumon-type.enum';

@Component({
  selector: 'wa-sitting-voters-list',
  templateUrl: './sitting-voters-list.component.html',
  styleUrls: ['./sitting-voters-list.component.scss']
})
export class SittingVotersListComponent implements OnInit {

  readonly commonIcons = CommonIcons;
  readonly commonMessages = CommonMessages;
  readonly sittingListMessages = SittingsListMessages;
  readonly weight = Weight;
  readonly fileType = FileType;

  convocationSpinner = 'convocationSpinner';
  sendConvocationAction: Action<Summon>;
  goBackAction: Action<void>;
  secondaryActionsItems: ActionSet<Summon> = {actions: []};
  convocationsListItems: Convocation[] = [];
  resetEventsSubject: Subject<void> = new Subject<void>();
  processingSavingAction = false;

  get summon(): Summon {
    return this._summon;
  }

  set summon(value: Summon) {
    this._summon = value;

    this.attachmentSummonInformation = {
      convocation: this.summon.getAttachement(AttachmentSummonType.CONVOCATION).file
        ? Object.assign({}, this.summon.getAttachement(AttachmentSummonType.CONVOCATION).file) : null,
      noteDeSynthese: this.summon.getAttachement(AttachmentSummonType.EXECUTIVE_SUMMARY).file
        ? Object.assign({}, this.summon.getAttachement(AttachmentSummonType.EXECUTIVE_SUMMARY).file) : null,
      valid: true
    };

    const convocations: Convocation[] = [];
    if (this.summon.convocations) {
      this.summon.convocations.map(convocation => convocations.push(convocation));
    }
    this.convocationsListItems = convocations;
  }

  private _summon: Summon;
  attachmentSummonInformation: AttachmentSummonInformation;

  constructor(protected activatedRoute: ActivatedRoute,
              protected router: Router,
              protected notificationService: NotificationService,
              protected summonActionItemFactory: SummonActionItemFactory,
              protected generalActionFactory: GeneralActionFactory,
              protected manageSummonService: IManageSummonService,
              protected attachmentSummonFilesService: AttachmentSummonFilesService,
  ) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.activatedRoute.data.subscribe(data => {
        this.summon = data.summon;

        this.sendConvocationAction = this.summonActionItemFactory.getActionItem(SummonAction.SEND_CONVOCATION);
        this.goBackAction = this.generalActionFactory.getAction(GeneralAction.GO_BACK, {goBackUrl: RoutingPaths.SITTING_RUNNING_PATH});
        this.secondaryActionsItems.actions.push(this.goBackAction);
      });
    }, 0);
  }

  reloadSummon() {
    this.manageSummonService.getBySittingId(this.summon.sitting_id)
      .subscribe(summon => this.summon = summon);
  }

  cancel() {
    this.resetEventsSubject.next();
    this.router.navigateByUrl(RoutingPaths.SITTINGS_PATH).then();
  }

  updateDocuments(values: AttachmentSummonInformation) {
    this.attachmentSummonInformation = values;
  }

  saveDocuments() {
    const saveActions = [];
    this.processingSavingAction = true;

    switch (this.getConvocationDocumentAction(AttachmentSummonType.CONVOCATION)) {
      case 'save':
        saveActions.push(this.attachmentSummonFilesService.sendFile(
          this.summon.getAttachement(AttachmentSummonType.CONVOCATION).attachment_id, this.attachmentSummonInformation.convocation));
        break;
      case 'delete':
        saveActions.push(this.attachmentSummonFilesService.destroy(this.summon.getAttachement(AttachmentSummonType.CONVOCATION).file.id));
        break;
      case 'keep':
        break;
    }

    switch (this.getConvocationDocumentAction(AttachmentSummonType.EXECUTIVE_SUMMARY)) {
      case 'save':
        saveActions.push(this.attachmentSummonFilesService.sendFile(
          this.summon.getAttachement(AttachmentSummonType.EXECUTIVE_SUMMARY).attachment_id, this.attachmentSummonInformation.noteDeSynthese));
        break;
      case 'delete':
        saveActions.push(this.attachmentSummonFilesService.destroy(this.summon.getAttachement(AttachmentSummonType.EXECUTIVE_SUMMARY).file.id));
        break;
      case 'keep':
        break;
    }

    forkJoin(saveActions).subscribe(
      () => {
        this.notificationService.showSuccess(this.sittingListMessages.CREATED_FILE_SUCCESS_TOASTR_TXT);
        this.reloadSummon();
      },
      error => {
        console.error(error);
        this.notificationService.showError(this.sittingListMessages.CREATED_FILE_ERROR_TOASTR_TXT);
      }
    ).add(() => this.processingSavingAction = false);
  }

  sendActionDone($event: ActionResult) {
    if ($event) {
      if ($event.error) {
        if ($event.message) {
          this.notificationService.showError($event.message);
        }
      } else {
        if ($event.message) {
          this.notificationService.showSuccess($event.message);
        }
      }
      setTimeout(() => this.reloadSummon(), 3000);
    }
  }

  getConvocationDocumentAction(attachmentSummonType: AttachmentSummonType): 'delete' | 'save' | 'keep' {
    let attachmentSummonFile: AttachmentSummonFile;
    switch (attachmentSummonType) {
      case AttachmentSummonType.CONVOCATION:
        attachmentSummonFile = this.attachmentSummonInformation.convocation;
        break;
      case AttachmentSummonType.EXECUTIVE_SUMMARY:
        attachmentSummonFile = this.attachmentSummonInformation.noteDeSynthese;
    }

    if (!!this.summon.getAttachement(attachmentSummonType).file && !attachmentSummonFile) {
      return 'delete';
    } else if (!this.summon.getAttachement(attachmentSummonType).file && attachmentSummonFile ||
      this.summon.getAttachement(attachmentSummonType).file && attachmentSummonFile &&
      this.summon.getAttachement(attachmentSummonType).file.lastModified !== attachmentSummonFile.lastModified
    ) {
      return 'save';
    }
    return 'keep';
  }

  documentsModified(): boolean {
    return !(this.getConvocationDocumentAction(AttachmentSummonType.CONVOCATION) === 'keep'
      && this.getConvocationDocumentAction(AttachmentSummonType.EXECUTIVE_SUMMARY) === 'keep');
  }

}
