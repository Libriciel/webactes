/* eslint-disable @typescript-eslint/member-ordering */
import { DatePipe } from '@angular/common';
import { SittingsRoutingMessages } from '../../routing/sittings-routing-messages';
import { Sitting } from '../../../model/Sitting/sitting';
import { Style } from '../../../wa-common';

export class SittingsListMessages {
  static CREATE_SEANCE = 'Créer une séance';
  static TITLE = 'Séances en cours';

  static type = 'Type de séance';
  static dateTime = 'Date et heure';
  static actions = 'Actions';
  static UPDATE_SITTING_ACTION_FAILED = 'Erreur lors de la sauvegarde des modifications de la séance.';
  static CONFIRM_DELETE_ONE_SITTING_TITLE = `Supprimer une séance`;
  static CONFIRM_DELETE_ONE_SITTING_WITH_ASSOCIATED_PROJECT_TITLE = `Dissocier et supprimer une séance`;
  static CONFIRM_DELETE_MULTIPLE_SITTINGS_WITH_ASSOCIATED_PROJECT_CONTENT =
    '<p>Des séances contiennent des projets associés.</p>' +
    '<p>Voulez-vous dissocier tous les projets et supprimer l\'ensemble des séances sélectionnées\u00A0?</p>' +
    '<p>Vous retrouverez les projets dissociés dans le menu <i>'
    + SittingsRoutingMessages.PAGE_PROJETS_TITLE + '\u00A0>\u00A0'
    + SittingsRoutingMessages.PAGE_SITTINGS_PROJECTS_TITLE + '</i>.</p>';
  static CONFIRM_DELETE_MULTIPLE_SITTINGS_TITLE = `Supprimer des séances`;
  static CONFIRM_DELETE_MULTIPLE_SITTINGS_WITH_ASSOCIATED_PROJECT_TITLE = `Dissocier et supprimer des séances`;
  static CONFIRM_DELETE_SITTINGS_WITH_ASSOCIATED_PROJECT_BUTTON = 'Dissocier et supprimer';
  static CONFIRM_DELETE_SITTINGS_BUTTON = 'Supprimer';
  static CONFIRM_CLOSE_ONE_SITTING_TITLE = `Clore une séance`;
  static CONFIRM_CLOSE_ONE_SITTING_CONTENT = `<p>Êtes-vous sûr de vouloir clore cette séance\u00A0?</p>`;
  static CONFIRM_CLOSE_MULTIPLE_SITTING_TITLE = `Clore des séances`;
  static CONFIRM_CLOSE_SITTINGS_BUTTON = 'Clore';
  static ERROR_DELETE_MULTIPLE_SITTINGS_WITH_VOTED_PROJECT_CONTENT = '<p>Des séances contiennent des projets votés.</p>';

  // Send Sitting action
  static SEND_SITTING_ACTION_NAME = 'Envoyer la séance';
  static SEND_SITTING_ACTION_ICON = 'fas fa-paper-plane';
  // Close Sitting action
  static CLOSE_SITTING_ACTION_NAME = 'Clore';
  static CLOSE_SITTING_ACTION_ICON = 'fas fa-lock';
  // Close Sittings action
  static CLOSE_SITTINGS_ACTION_NAME = 'Clore les séances';
  static CLOSE_SITTINGS_ACTION_ICON = SittingsListMessages.CLOSE_SITTING_ACTION_ICON;
  static CLOSE_SITTINGS_ACTION_ICON_STYLE = Style.WARNING;
  static CLOSE_MULTIPLE_SITTING_ERROR = 'Au moins d\'une des séances sélectionnées contient un projet';
  // Delete Sitting action
  static DELETE_SITTING_ACTION_NAME = 'Supprimer';
  static DELETE_SITTING_ACTION_ICON_STYLE = Style.DANGER;
  // Delete Sittings action
  static DELETE_SITTINGS_ACTION_NAME = 'Supprimer les séances';
  static DELETE_SITTINGS_ACTION_ICON_STYLE = Style.DANGER;
  // Update Sitting action
  static UPDATE_SITTING_ACTION_NAME = 'Modifier';
  static UPDATE_SITTINGS_ACTION_ICON_STYLE = Style.NEUTRAL;

  static DOWNLOAD_GENERATE_DELIBERATIONS_LIST = 'Télécharger le document de liste des délibérations';
  static DOWNLOAD_GENERATE_VERBAL_TRIAL = 'Télécharger le document de procès-verbal';

  // View Sitting Action
  static SHOW_SITTING_ACTION_NAME = 'Visualiser';

  static SEND_SITTING_CONVOCATION_DOCUMENTS_TITLE = 'Documents';
  static SEND_SITTING_CONVOCATION_ACTORS_TITLE = 'Acteurs';

  // Actions Convocation
  static SEND_SITTING_CONVOCATION_ACTION_STYLE = Style.NORMAL;
  static SEND_SITTING_CONVOCATION_ACTION_NAME = 'Envoyer les documents';

  static GENERATE_SITTING_CONVOCATION_ACTION_NAME = 'Générer les documents';
  static GENERATE_SITTING_CONVOCATION_CONFIRM_POPUP_CONTENT = `La génération de la convocation et de la note de synthèse remplacera les documents existants <b>sans possibilité d'annulation</b>.`;
  static GENERATE_SITTING_CONVOCATION_CONFIRM_POPUP_CONTENT_AFTER_GENERATION_ERROR = `La génération de la convocation et de la note de synthèse a précédemment échoué. <br/> <br/> Voulez-vous générer de nouveau les documents ?`;
  static GENERATE_SITTING_ACTION_ICON = 'fa fa-cog';

  static GENERATE_SITTING_CONVOCATION_ACTION_STYLE = Style.NORMAL;
  // Deletion confirmation
  static SAVE_DOCUMENTS = 'Enregistrer les documents';
  static FORM_FIELD_CONVOCATION_FILE_LABEL = 'Convocation';
  static FORM_FIELD_CONVOCATION_FILE_BUTTON_TEXT = 'Ajouter la convocation';
  static FORM_FIELD_EXECUTIVE_SUMMARY_FILE_LABEL = 'Note de synthèse';
  static FORM_FIELD_EXECUTIVE_SUMMARY_FILE_BUTTON_TEXT = 'Ajouter la note de synthèse';
  static CREATED_FILE_SUCCESS_TOASTR_TXT = 'Documents enregistrés';
  static CREATED_FILE_ERROR_TOASTR_TXT = `Erreur lors de l'enregistrement des documents`;
  static SCHEDULE_SITTING_ACTION_ICON_STYLE = Style.NORMAL;
  static SCHEDULE_SITTING_ACTION_NAME = 'Ordre du jour de la séance';
  static SEND_SITTING_TO_IDELIBRE_ACTION_NAME = 'Envoyer à idelibre';
  static SEND_SITTING_TO_IDELIBRE_ACTION_NAME_DONE = 'Séance envoyée à i-delibRE';
  static SEND_SITTING_TO_IDELIBRE_ACTION_NAME_ERROR = 'Séance non envoyée à i-delibRE';
  static SEND_SITTING_TO_IDELIBRE_ACTION_STYLE = Style.NORMAL;
  static SEND_TO_IDELIBRE_SITTING_ACTION_ICON = 'fa fa-cloud-upload-alt';
  static CONFIRM_DOWNLOAD_DELIBERATIONS_LIST_TITLE = 'Confirmation du téléchargement du document de liste des délibérations';
  static CONFIRM_DOWNLOAD_VERBAL_TRIAL_TITLE = 'Confirmation du téléchargement du document de procès-verbal';
  static CONFIRM_SEND_IDELIBRE_TITLE = 'Confirmation de l\'envoi de la séance à i-delibRE';
  static CONFIRM_SEND_IDELIBRE_CONTENT = 'La précédente génération de la convocation a échoué. <br/> <br/> Voulez-vous générer de nouveau et envoyer la convocation ?';

  static confirmDeleteOneSittingContent(type: string) {
    return 'Êtes-vous sûr de vouloir supprimer la séance "' + type + '"\u00A0?';
  }

  static sittingCreated(name: string, date: Date) {
    return `Votre séance ${name} prévue le ${new DatePipe('fr').transform(date, 'dd/MM/yyyy')} a bien été créée.`;
  }

  static update_sitting_action_done(sitting_type: string) {
    return 'Les modifications de la séance "' + sitting_type + '" ont été enregistrées avec succès.';
  }

  static confirm_delete_multiple_sittings_content(length: number) {
    return `Êtes-vous sûr de vouloir supprimer ces ${length} séances\u00A0?`;
  }

  static confirm_close_multiple_sittings_content(length: number) {
    return `<p>Êtes-vous sûr de vouloir clore ces ${length} séances\u00A0?</p>`;
  }

  static delete_sittings_action_done(sittings: Sitting[]) {
    return sittings.length === 1 ? 'Séance supprimée avec succès !' : sittings.length + ' séances supprimées avec succès !';
  }

  static delete_sittings_action_failed(sittings: Sitting[]) {
    return sittings.length === 1 ? 'Erreur lors de la suppression de la séance.' : 'Erreur lors de la suppression des séances.';
  }

  static close_sittings_action_done(sittings: Sitting[]) {
    const sitting = sittings[0];
    return sittings.length === 1
      ? 'La séance ' + sitting.typesitting.name
      + ' du ' + new DatePipe('fr').transform(sitting.date, 'dd/MM/yyyy à HH:mm')
      + ' a été close avec succès !'
      : 'Les séances ont été closes avec succès !';
  }

  static close_sittings_action_error(sittings: Sitting[]) {
    const sitting = sittings[0];
    return sittings.length === 1
      ? 'La séance ' + sitting.typesitting.name
      + ' du ' + new DatePipe('fr').transform(sitting.date, 'dd/MM/yyyy à HH:mm')
      + ' n\'a pas pu être cloturée'
      : 'Erreur lors de la clôture des séances.';
  }

  static formFieldFileHelp(size: number) {
    return `La taille recommandée pour afficher correctement une génération est de ${size} Mo.`;
  }

  static downloadDeliberationsListConfirm(name: string, reason: string): string {
    // tslint:disable-next-line:max-line-length
    return `Souhaitez vous télécharger le document de liste des délibérations <b>${name}</b> ? <br/> Sachant que cela a échoué précédemment pour la raison : </br> ${reason}`;
  }

  static downloadVerbalTrialConfirm(name: string, reason: string): string {
    // tslint:disable-next-line:max-line-length
    return `La génération précédente a échoué pour la raison : <br/> <br/>${reason}  <br/><br/> Souhaitez vous télécharger le document de procès-verbal <b>${name}</b> ?`;
  }
}
