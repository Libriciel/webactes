import { ActionItem, CommonIcons } from '../../../ls-common';
import { Sitting } from '../../../model/Sitting/sitting';
import { SittingAction } from './actions/sitting-action.enum';
import { SittingsListMessages } from './SittingsListMessages';
import { Injectable } from '@angular/core';
import { AddDocumentsSittingAction } from './actions/add-documents-sitting-action';
import { SendSittingToIdelibreAction } from './actions/send-sitting-to-idelibre-action';
import { GoToScheduleAction } from './actions/go-to-schedule-action';

@Injectable({
  providedIn: 'root'
})
export class SittingActionItemFactory {

  private messages = SittingsListMessages;

  constructor(protected sendToIdelibre: SendSittingToIdelibreAction,
              protected addDocumentsAction: AddDocumentsSittingAction,
              protected scheduleAction: GoToScheduleAction) {
  }

  getActionItem(action: SittingAction): ActionItem<Sitting> {
    switch (action) {
      case SittingAction.SEND_TO_IDELIBRE:
        return new ActionItem({
          name: this.messages.SEND_SITTING_TO_IDELIBRE_ACTION_NAME,
          icon: this.messages.SEND_TO_IDELIBRE_SITTING_ACTION_ICON,
          style: this.messages.SEND_SITTING_TO_IDELIBRE_ACTION_STYLE,
          actuator: this.sendToIdelibre,
        });
      case SittingAction.ADD_DOCUMENTS:
        return new ActionItem({
          name: this.messages.SAVE_DOCUMENTS,
          icon: CommonIcons.SAVE_ICON,
          actuator: this.addDocumentsAction,
        });
      case SittingAction.SCHEDULE_PROJECT:
        return new ActionItem({
          name: this.messages.SCHEDULE_SITTING_ACTION_NAME,
          icon: CommonIcons.SCHEDULE_ICON,
          actuator: this.scheduleAction,
        });

      default:
        throw new Error(`unimplemented action ${action}`);
    }

  }
}
