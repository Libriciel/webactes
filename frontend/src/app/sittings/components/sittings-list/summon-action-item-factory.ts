import { Injectable } from '@angular/core';
import { Summon } from '../../../model/summon/summon';
import { ActionItem } from '../../../ls-common';
import { SummonAction } from './actions/summon-action.enum';
import { SendSittingConvocationAction } from './actions/send-sitting-convocation-action';
import { SittingsListMessages } from './SittingsListMessages';
import { GenerateSittingConvocationAction } from './actions/generate-sitting-convocation-action';
import { SendSittingConvocationActionValidator } from './actions/send-sitting-convocation-action-validator';

@Injectable({
  providedIn: 'root'
})
export class SummonActionItemFactory {

  constructor(protected sendConvocationAction: SendSittingConvocationAction,
              protected generateConvocationAction: GenerateSittingConvocationAction) {
  }

  private messages = SittingsListMessages;

  getActionItem(action: SummonAction): ActionItem<Summon> {
    switch (action) {
      case SummonAction.SEND_CONVOCATION:
        return new ActionItem({
          name: this.messages.SEND_SITTING_CONVOCATION_ACTION_NAME,
          icon: this.messages.SEND_SITTING_ACTION_ICON,
          style: this.messages.SEND_SITTING_CONVOCATION_ACTION_STYLE,
          actuator: this.sendConvocationAction,
          actionValidator: new SendSittingConvocationActionValidator()
        });
      case SummonAction.GENERATE_CONVOCATION:
        return new ActionItem({
          name: this.messages.GENERATE_SITTING_CONVOCATION_ACTION_NAME,
          icon: this.messages.GENERATE_SITTING_ACTION_ICON,
          style: this.messages.GENERATE_SITTING_CONVOCATION_ACTION_STYLE,
          actuator: this.generateConvocationAction,
        });
      default:
        throw new Error(`unimplemented action ${action}`);
    }
  }
}
