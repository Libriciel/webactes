import { Observable, Subject } from 'rxjs';
import { ActionResult, ConfirmPopupComponent, IActuator, NotificationService } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { FileType } from '../../../../utility/Files/file-types.enum';
import { IManageSittingService } from '../../../services/iManageSittingService';
import { Sitting } from '../../../../model/Sitting/sitting';
import { SpinnerService } from '../../../../core/services/spinner.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Style } from '../../../../wa-common';
import { SittingsListMessages } from '../SittingsListMessages';

@Injectable({
  providedIn: 'root'
})

export class DownloadSittingVerbalTrial implements IActuator<Sitting> {

  constructor(protected router: Router,
              protected spinnerService: SpinnerService,
              protected notificationService: NotificationService,
              protected service: IManageSittingService,
              protected modalService: NgbModal) {
  }

  messages = SittingsListMessages;


  action(sittings: Sitting[]): Observable<ActionResult> {
    const returnSubject = new Subject<ActionResult>();

    const message = sittings[0].generationerrors.find(generationerror => generationerror.type === 'verbal-trial')
      ? sittings[0].generationerrors.find(generationerror => generationerror.type === 'verbal-trial').message : null;
    console.log('message', message);

    if (message) {
      const modalRef = this.modalService.open(ConfirmPopupComponent, {
        backdrop: 'static',
        keyboard: false,
        centered: true
      });
      modalRef.componentInstance.title = this.messages.CONFIRM_DOWNLOAD_VERBAL_TRIAL_TITLE;
      modalRef.componentInstance.content = this.messages.downloadVerbalTrialConfirm(
        sittings[0].typesitting.name
        + ' du ' + `${new DatePipe('fr').transform(sittings[0].date, 'dd/MM/yyyy à HH:mm')}`,
        message);
      modalRef.componentInstance.confirmMsg = this.messages.DOWNLOAD_GENERATE_VERBAL_TRIAL;
      modalRef.componentInstance.style = Style.WARNING;
      modalRef.result
        .then(() => this.doDownloadVerbalTrial(sittings[0], returnSubject),
          () => {
            returnSubject.next({needReload: false});
            returnSubject.complete();
          }
        );
    } else {
      this.doDownloadVerbalTrial(sittings[0], returnSubject);
    }
    return returnSubject.asObservable();
  }

  doDownloadVerbalTrial(sitting: Sitting, returnSubject: Subject<ActionResult>) {
    this.spinnerService.load();
    this.service.getDownloadVerbalTrial(sitting.id, FileType.PDF, 'procès_verbal').subscribe(
      () => {
        returnSubject.next({
          error: false,
          needReload: false,
        });
      },
      error => {
        this.spinnerService.error(error.statusText, null, error);
      }
    ).add(() => {
      this.spinnerService.close();
      returnSubject.next({
        error: true,
        needReload: false,
      });
    });
  }
}
