import { Sitting } from '../../../../model/Sitting/sitting';
import { Observable, of } from 'rxjs';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';

export class ManageAgendaAction implements IActuator<Sitting> {

  action(sittings: Sitting[]): Observable<any> {
    // eslint-disable-next-line no-console
    console.debug('Gestion de l\'ordre du jour des séances : ' + sittings.map(sitting => sitting.id));
    return of({});
  }
}
