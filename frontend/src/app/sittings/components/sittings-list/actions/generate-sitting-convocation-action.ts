import { Observable, of, Subject } from 'rxjs';
import { ActionResult, ConfirmPopupComponent, IActuator } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Summon } from '../../../../model/summon/summon';
import { IManageSittingService } from '../../../services/iManageSittingService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SittingsListMessages } from '../SittingsListMessages';
import { Style } from '../../../../wa-common';
import { AttachmentSummonType } from '../../../../model/summon/attachment-sumon-type.enum';
import { SpinnerService } from '../../../../core/services/spinner.service';

@Injectable({
  providedIn: 'root'
})
export class GenerateSittingConvocationAction implements IActuator<Summon> {

  readonly messages = SittingsListMessages;

  constructor(protected router: Router,
              protected spinnerService: SpinnerService,
              protected manageSittingService: IManageSittingService,
              protected modalService: NgbModal
  ) {
  }

  private doGeneration(summon: Summon, returnSubject: Subject<ActionResult>) {
    this.spinnerService.load();
    this.manageSittingService.generateSittingDocuments(summon.id).subscribe(
      () => {
        returnSubject.next({
          error: false,
          needReload: true,
        });
      },
      error => {
        this.spinnerService.error(error.statusText, null, error);
        returnSubject.next({
          error: true,
          needReload: true,
        });
      }
    ).add(() => {
      this.spinnerService.close();
      returnSubject.complete();
    });
  }

  action(summons: Summon[]): Observable<ActionResult> {
    if (!summons || summons.length === 0) {
      return of({});
    }

    const summon = summons[0];
    const returnSubject = new Subject<ActionResult>();

    if (!summon.getAttachement(AttachmentSummonType.CONVOCATION).file
      && !summon.getAttachement(AttachmentSummonType.EXECUTIVE_SUMMARY).file
      && summon.generationerrors.length === 0) {
      this.doGeneration(summon, returnSubject);
    } else {
      const modalRef = this.modalService.open(ConfirmPopupComponent, {
        backdrop: 'static',
        keyboard: false,
        centered: true
      });
      modalRef.componentInstance.title = this.messages.GENERATE_SITTING_CONVOCATION_ACTION_NAME;

      modalRef.componentInstance.style = Style.WARNING;
      modalRef.componentInstance.icon = this.messages.GENERATE_SITTING_ACTION_ICON;
      if (summon.generationerrors && summon.generationerrors.length !== 0) {
        modalRef.componentInstance.content = this.messages.GENERATE_SITTING_CONVOCATION_CONFIRM_POPUP_CONTENT_AFTER_GENERATION_ERROR;
        modalRef.componentInstance.confirmMsg = this.messages.GENERATE_SITTING_CONVOCATION_ACTION_NAME;
      } else {
        modalRef.componentInstance.content = this.messages.GENERATE_SITTING_CONVOCATION_CONFIRM_POPUP_CONTENT;
        modalRef.componentInstance.confirmMsg = this.messages.GENERATE_SITTING_CONVOCATION_ACTION_NAME;
      }
      modalRef.result
        .then(() => this.doGeneration(summon, returnSubject),
          () => {
            returnSubject.next({needReload: false});
            returnSubject.complete();
          }
        );
    }
    return returnSubject.asObservable();
  }

}
