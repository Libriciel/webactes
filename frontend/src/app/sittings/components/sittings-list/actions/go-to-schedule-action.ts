import { Observable, of } from 'rxjs';
import { IActuator } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BreadcrumbItem } from '../../../../wa-common/model/breadcrumb-item';
import { Sitting } from '../../../../model/Sitting/sitting';
import { RoutingPaths } from '../../../../wa-common/routing-paths';

@Injectable({
  providedIn: 'root'
})
export class GoToScheduleAction implements IActuator<Sitting> {

  private currentBreadcrumb: BreadcrumbItem[];

  constructor(protected  router: Router) {
  }

  setCurrentBreadcrumb(value: BreadcrumbItem[]) {
    this.currentBreadcrumb = value;
    return this;
  }

  action(sittings: Sitting[]): Observable<any> {
    return of(this.router.navigateByUrl(RoutingPaths.scheduleSittingPath(String(sittings[0].id)), {
      state: {
        goto: this.router.url,
        originalContext: {goto: this.router.url, breadcrumb: this.currentBreadcrumb}
      }
    }));
  }
}
