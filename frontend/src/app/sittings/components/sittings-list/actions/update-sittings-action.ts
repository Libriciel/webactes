import { Sitting } from '../../../../model/Sitting/sitting';
import { from, Observable, of } from 'rxjs';
import { SittingModalComponent } from '../../sitting-modal/sitting-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { catchError, map } from 'rxjs/operators';
import { ActionResult } from '../../../../ls-common/components/tables/actuator/action-result';
import { SittingsListMessages } from '../SittingsListMessages';
import { Injectable } from '@angular/core';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';

@Injectable({
  providedIn: 'root'
})
export class UpdateSittingsAction implements IActuator<Sitting> {

  messages = SittingsListMessages;

  constructor(protected  modalService: NgbModal) {
  }

  action(sittings: Sitting[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(SittingModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.sitting = sittings[0];
    return from(modalRef.result).pipe(
      map((updatedSitting: Sitting) => {
        return {
          needReload: true,
          message: this.messages.update_sitting_action_done(updatedSitting.typesitting.name)
        } as ActionResult;
      }),
      catchError((error) => {
        return error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.UPDATE_SITTING_ACTION_FAILED
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}

