import {ActionResult, ConfirmPopupComponent, IActuator} from '../../../../ls-common';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Sitting} from '../../../../model/Sitting/sitting';
import {Observable, Subject} from 'rxjs';
import {IManageSittingService} from '../../../services/iManageSittingService';
import {SittingsListMessages} from '../SittingsListMessages';
import {Injectable} from '@angular/core';
import {Style} from '../../../../wa-common';

@Injectable({
    providedIn: 'root'
})
export class CloseSittingsAction implements IActuator<Sitting> {

    messages = SittingsListMessages;
    needReload = true;

    constructor(
        protected modalService: NgbModal,
        protected sittingService: IManageSittingService,
    ) {
    }

    action(sittings: Sitting[]): Observable<ActionResult> {
        const modalRef = this.modalService.open(ConfirmPopupComponent, {
            backdrop: 'static',
            keyboard: false,
            centered: true
        });

        if (sittings.length === 1) {
            modalRef.componentInstance.title = this.messages.CONFIRM_CLOSE_ONE_SITTING_TITLE;
            modalRef.componentInstance.content = this.messages.CONFIRM_CLOSE_ONE_SITTING_CONTENT;
        } else {
            modalRef.componentInstance.title = this.messages.CONFIRM_CLOSE_MULTIPLE_SITTING_TITLE;
            modalRef.componentInstance.content = this.messages.confirm_close_multiple_sittings_content(sittings.length);
        }

        modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_CLOSE_SITTINGS_BUTTON;
        modalRef.componentInstance.style = Style.WARNING;
        modalRef.componentInstance.icon = this.messages.CLOSE_SITTING_ACTION_ICON;

        const returnSubject = new Subject<ActionResult>();

        modalRef.result.then(() => this.sittingService.close(sittings).subscribe({
                complete: () => {
                    returnSubject.next({
                        needReload: true,
                        message: this.messages.close_sittings_action_done(sittings),
                    });
                    returnSubject.complete();
                },
                error: (error) => {
                    console.error(error);
                    returnSubject.next({
                        error: true,
                        needReload: true,
                        message: sittings.length === 1 ? this.messages.close_sittings_action_error(sittings) : this.messages.CLOSE_MULTIPLE_SITTING_ERROR,
                    });
                    returnSubject.complete();
                },
            }),
            () => {
                returnSubject.complete();
            });
        return returnSubject.asObservable();
    }
}
