import { Sitting } from '../../../../model/Sitting/sitting';
import { Observable, Subject } from 'rxjs';
import { ActionResult, ConfirmPopupComponent, IActuator } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { IManageSummonService } from '../../../../model/summon/imanage-summon-service';
import { SittingsListMessages } from '../SittingsListMessages';
import { SpinnerService } from '../../../../core/services/spinner.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Style } from '../../../../wa-common';

@Injectable({
  providedIn: 'root'
})
export class SendSittingToIdelibreAction implements IActuator<Sitting> {

  messages = SittingsListMessages;

  constructor(protected router: Router,
              protected summonService: IManageSummonService,
              protected spinnerService: SpinnerService,
              protected modalService: NgbModal
  ) {
  }

  action(sittings: Sitting[]): Observable<ActionResult> {

    const returnSubject = new Subject<ActionResult>();

    const message = sittings[0].generationerrors.find(generationerror => generationerror.type === 'convocation')
      ? sittings[0].generationerrors.find(generationerror => generationerror.type === 'convocation').message : null;

    if (message) {
      const modalRef = this.modalService.open(ConfirmPopupComponent, {
        backdrop: 'static',
        keyboard: false,
        centered: true
      });
      modalRef.componentInstance.title = this.messages.CONFIRM_SEND_IDELIBRE_TITLE;
      modalRef.componentInstance.content = this.messages.CONFIRM_SEND_IDELIBRE_CONTENT;
      modalRef.componentInstance.confirmMsg = this.messages.SEND_SITTING_TO_IDELIBRE_ACTION_NAME;
      modalRef.componentInstance.style = Style.WARNING;
      modalRef.result
        .then(() => this.doSendIdelibre(sittings[0], returnSubject),
          () => {
            returnSubject.next({needReload: false});
            returnSubject.complete();
          }
        );
    } else {
      this.doSendIdelibre(sittings[0], returnSubject);
    }

    return returnSubject.asObservable();
  }

  doSendIdelibre(sitting: Sitting, returnSubject: Subject<ActionResult>) {
    this.spinnerService.load('Génération/Envoi en cours');
    this.summonService.sendToIdelibre(sitting.id).subscribe(
      () => {
        returnSubject.next({
          error: false,
          message: this.messages.SEND_SITTING_TO_IDELIBRE_ACTION_NAME_DONE,
          needReload: true,
        });
      },
      error => {
        this.spinnerService.error(error.statusText, null, error);
        returnSubject.next({
          error: true,
          message: this.messages.SEND_SITTING_TO_IDELIBRE_ACTION_NAME_ERROR,
          needReload: true,
        });
      }
    ).add(() => this.spinnerService.close());
  }
}
