import { Sitting } from '../../../../model/Sitting/sitting';
import { Observable, of } from 'rxjs';
import { IActuator } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AddDocumentsSittingAction implements IActuator<Sitting> {

  constructor(protected  router: Router) {
  }

  action(sittings: Sitting[]): Observable<any> {
    return of();
  }
}
