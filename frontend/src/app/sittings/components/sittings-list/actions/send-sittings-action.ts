import { Sitting } from '../../../../model/Sitting/sitting';
import { Observable, of } from 'rxjs';
import { IActuator } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { Router } from '@angular/router';
import { BreadcrumbItem } from '../../../../wa-common/model/breadcrumb-item';

@Injectable({
  providedIn: 'root'
})
export class SendSittingsAction implements IActuator<Sitting> {
  private currentBreadcrumb: BreadcrumbItem[];

  constructor(protected router: Router) {
  }

  action(sittings: Sitting[]): Observable<any> {


    return of(this.router.navigateByUrl(RoutingPaths.sendSittingPath(String(sittings[0].id)), {
      state: {
        goto: this.router.url,
        originalContext: {goto: this.router.url, breadcrumb: this.currentBreadcrumb}
      }
    }));
  }
}
