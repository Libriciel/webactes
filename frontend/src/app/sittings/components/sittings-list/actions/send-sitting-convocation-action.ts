import { Observable, of } from 'rxjs';
import { ActionResult, IActuator } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { IManageSummonService } from '../../../../model/summon/imanage-summon-service';
import { Summon } from '../../../../model/summon/summon';
import { catchError, map } from 'rxjs/operators';
import { SittingsMessages } from '../../../SittingsMessages';

@Injectable({
  providedIn: 'root'
})
export class SendSittingConvocationAction implements IActuator<Summon> {

  constructor(protected router: Router,
              protected summonService: IManageSummonService,
  ) {
  }

  action(summons: Summon[]): Observable<ActionResult> {
    return this.summonService.send(summons[0].id)
      .pipe(
        map(() => ({
          error: false,
          message: SittingsMessages.SEND_SITTING_CONVOCATION_SUCCESS
        } as ActionResult)),
        catchError((error) => {
            console.error(error.message);
            return (of({
              error: true,
              message: SittingsMessages.SEND_SITTING_CONVOCATION_ERROR
            } as ActionResult));
          }
        )
      );
  }
}
