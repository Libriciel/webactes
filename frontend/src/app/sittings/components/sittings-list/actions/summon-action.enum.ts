export enum SummonAction {
  SEND_CONVOCATION,
  GENERATE_CONVOCATION
}
