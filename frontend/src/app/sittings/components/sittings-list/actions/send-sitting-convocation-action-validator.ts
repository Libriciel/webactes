import { Summon } from '../../../../model/summon/summon';
import { IValidator } from '../../../../ls-common/components/tables/validator/i-validator';

export class SendSittingConvocationActionValidator implements IValidator<Summon> {

  constructor() {
  }

  isActionValid(summons: Summon[]): boolean {
    return summons.length > 0
      && !!summons[0].attachments_summons
      && !!summons[0].attachments_summons[0].file;
  }
}
