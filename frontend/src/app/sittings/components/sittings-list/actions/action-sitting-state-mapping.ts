import {SittingState} from '../../../../model/Sitting/sitting-state.enum';
import {ActionItemsSet} from '../../../../ls-common/model/action-items-set';
import {Sitting} from '../../../../model/Sitting/sitting';
import {CloseSittingsAction} from './close-sittings-action';
import {DeleteSittingsAction} from './delete-sittings-action';
import {SendSittingsAction} from './send-sittings-action';
import {UpdateSittingsAction} from './update-sittings-action';
import {SittingsListMessages} from '../SittingsListMessages';
import {ActionItem, CommonIcons} from '../../../../ls-common';
import {Injectable} from '@angular/core';
import {ShowSittingAction} from './show-sitting-action';
import {Style} from '../../../../wa-common';
import {DownloadSittingDeliberationsList} from './download-sitting-deliberations-list';
import {DownloadSittingVerbalTrial} from './download-sitting-verbal-trial';
import {GoToScheduleAction} from './go-to-schedule-action';
import {SendSittingToIdelibreAction} from './send-sitting-to-idelibre-action';
import {StructureSettingsService} from '../../../../core';
import {DeleteSittingActionValidator} from './delete-sitting-action-validator';
import {CloseSittingActionValidator} from './close-sitting-action-validator';

@Injectable({
    providedIn: 'root'
})
export class ActionSittingStateMapping {

    private messages = SittingsListMessages;

    constructor(
            protected closeSittingsAction: CloseSittingsAction,
            protected deleteSittingsAction: DeleteSittingsAction,
            protected updateSittingsAction: UpdateSittingsAction,
            protected showSittingsAction: ShowSittingAction,
            protected sendSittingAction: SendSittingsAction,
            protected downloadSittingDeliberationsList: DownloadSittingDeliberationsList,
            protected downloadSittingVerbalTrial: DownloadSittingVerbalTrial,
            protected sendToIdelibre: SendSittingToIdelibreAction,
            protected scheduleAction: GoToScheduleAction,
            protected structureSettingsService: StructureSettingsService,
    ) {
    }

    getCommonActions(sittingState: SittingState): ActionItemsSet<Sitting>[] {
        switch (sittingState) {
            case SittingState.ALL:
                return [{
                    actionItems: [
                        new ActionItem({
                            name: this.messages.CLOSE_SITTINGS_ACTION_NAME,
                            icon: this.messages.CLOSE_SITTINGS_ACTION_ICON,
                            style: this.messages.CLOSE_SITTINGS_ACTION_ICON_STYLE,
                            actuator: this.closeSittingsAction,
                        })]
                }, {
                    actionItems: [
                        new ActionItem({
                            name: this.messages.DELETE_SITTINGS_ACTION_NAME,
                            icon: CommonIcons.DELETE_ICON,
                            style: this.messages.DELETE_SITTINGS_ACTION_ICON_STYLE,
                            actuator: this.deleteSittingsAction
                        })]
                }];

            case SittingState.OPENED:
                return [{
                    actionItems: [
                        new ActionItem({
                            name: this.messages.CLOSE_SITTINGS_ACTION_NAME,
                            icon: this.messages.CLOSE_SITTINGS_ACTION_ICON,
                            style: this.messages.CLOSE_SITTINGS_ACTION_ICON_STYLE,
                            actuator: this.closeSittingsAction,
                        })]
                }, {
                    actionItems: [
                        new ActionItem({
                            name: this.messages.DELETE_SITTINGS_ACTION_NAME,
                            icon: CommonIcons.DELETE_ICON,
                            style: this.messages.DELETE_SITTINGS_ACTION_ICON_STYLE,
                            actuator: this.deleteSittingsAction,
                        })]
                }];

            case SittingState.CLOSED:
                return [];
        }
    }

    getSingleActions(sittingState: SittingState): ActionItemsSet<Sitting> {
        switch (sittingState) {
            case SittingState.ALL:
                return {
                    actionItems: []
                };

            case SittingState.OPENED:
                const actions = [
                    new ActionItem({
                        name: this.messages.SCHEDULE_SITTING_ACTION_NAME,
                        icon: CommonIcons.SCHEDULE_ICON,
                        style: this.messages.SCHEDULE_SITTING_ACTION_ICON_STYLE,
                        actuator: this.scheduleAction,
                    }),
                    new ActionItem({
                        name: this.messages.SHOW_SITTING_ACTION_NAME,
                        icon: CommonIcons.VOTE_ICON,
                        actuator: this.showSittingsAction,
                    }),
                    new ActionItem({
                        name: this.messages.UPDATE_SITTING_ACTION_NAME,
                        icon: CommonIcons.EDIT_ICON,
                        actuator: this.updateSittingsAction,
                    }),
                ];

                return {
                    actionItems: actions,
                };

            case SittingState.CLOSED:
                return null;
        }
    }

    getOtherSingleActions(sittingState: SittingState): ActionItemsSet<Sitting>[] {
        switch (sittingState) {
            case SittingState.ALL:
                return [
                    {
                        actionItems: [
                            new ActionItem({
                                name: this.messages.UPDATE_SITTING_ACTION_NAME,
                                icon: CommonIcons.EDIT_ICON,
                                style: this.messages.UPDATE_SITTINGS_ACTION_ICON_STYLE,
                                actuator: this.updateSittingsAction,
                            })]
                    }, {
                        actionItems: [
                            new ActionItem({
                                name: this.messages.CLOSE_SITTING_ACTION_NAME,
                                icon: this.messages.CLOSE_SITTING_ACTION_ICON,
                                style: this.messages.CLOSE_SITTINGS_ACTION_ICON_STYLE,
                                actuator: this.closeSittingsAction,
                                actionValidator: new CloseSittingActionValidator(),
                            }),
                            new ActionItem({
                                name: this.messages.DELETE_SITTING_ACTION_NAME,
                                icon: CommonIcons.DELETE_ICON,
                                style: this.messages.DELETE_SITTING_ACTION_ICON_STYLE,
                                actuator: this.deleteSittingsAction,
                                actionValidator: new DeleteSittingActionValidator()
                            })
                        ]
                    }];

            case SittingState.OPENED:
                let actions = [];

                if (this.structureSettingsService.getConfig('convocation', 'convocation_mailsec')) {
                    actions = actions.concat(new ActionItem({
                        name: this.messages.SEND_SITTING_ACTION_NAME,
                        icon: this.messages.SEND_SITTING_ACTION_ICON,
                        actuator: this.sendSittingAction,
                    }));
                }

                if (this.structureSettingsService.getConfig('convocation', 'convocation_idelibre')) {
                    actions = actions.concat(new ActionItem({
                        name: this.messages.SEND_SITTING_TO_IDELIBRE_ACTION_NAME,
                        icon: this.messages.SEND_TO_IDELIBRE_SITTING_ACTION_ICON,
                        actuator: this.sendToIdelibre,
                    }));
                }

                if (this.structureSettingsService.getConfig('sitting', 'sitting_generate_deliberations_list')) {
                    actions = actions.concat(new ActionItem({
                        name: this.messages.DOWNLOAD_GENERATE_DELIBERATIONS_LIST,
                        icon: CommonIcons.DOWNLOAD_DOCUMENT_ICON,
                        style: Style.NORMAL,
                        actuator: this.downloadSittingDeliberationsList
                    }));
                }

                if (this.structureSettingsService.getConfig('sitting', 'sitting_generate_verbal_trial')) {
                    actions = actions.concat(new ActionItem({
                        name: this.messages.DOWNLOAD_GENERATE_VERBAL_TRIAL,
                        icon: CommonIcons.DOWNLOAD_DOCUMENT_ICON,
                        style: Style.NORMAL,
                        actuator: this.downloadSittingVerbalTrial
                    }));
                }

                actions = actions.concat(
                        new ActionItem({
                            name: this.messages.CLOSE_SITTING_ACTION_NAME,
                            icon: this.messages.CLOSE_SITTING_ACTION_ICON,
                            style: this.messages.CLOSE_SITTINGS_ACTION_ICON_STYLE,
                            actuator: this.closeSittingsAction,
                            actionValidator: new CloseSittingActionValidator(),
                        }),
                        new ActionItem({
                            name: this.messages.DELETE_SITTING_ACTION_NAME,
                            icon: CommonIcons.DELETE_ICON,
                            style: this.messages.DELETE_SITTING_ACTION_ICON_STYLE,
                            actuator: this.deleteSittingsAction,
                            actionValidator: new DeleteSittingActionValidator()
                        }));

                return [{actionItems: actions}];

            case SittingState.CLOSED:
                return [];
        }
    }
}
