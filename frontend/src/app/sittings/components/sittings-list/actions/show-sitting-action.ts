import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { BreadcrumbItem } from '../../../../wa-common/model/breadcrumb-item';
import { Injectable } from '@angular/core';
import { IActuator } from '../../../../ls-common';
import { Sitting } from '../../../../model/Sitting/sitting';

@Injectable({
  providedIn: 'root'
})
export class ShowSittingAction implements IActuator<Sitting> {
  private currentBreadcrumb: BreadcrumbItem[];

  constructor(protected  router: Router) {
  }

  setCurrentBreadcrumb(value: BreadcrumbItem[]) {
    this.currentBreadcrumb = value;
    return this;
  }

  action(sittings: Sitting[]): Observable<any> {
    return of(this.router.navigateByUrl(RoutingPaths.sittingPath(sittings[0].id), {
      state: {
        goto: this.router.url,
        originalContext: {goto: this.router.url, breadcrumb: this.currentBreadcrumb}
      }
    }));
  }
}
