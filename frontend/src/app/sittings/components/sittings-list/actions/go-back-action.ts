import { Observable, of } from 'rxjs';
import { IActuator } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BreadcrumbItem } from '../../../../wa-common/model/breadcrumb-item';

@Injectable({
  providedIn: 'root'
})
export class GoBackAction implements IActuator<void> {
  private currentBreadcrumb: BreadcrumbItem[];
  private goBackUrl: string;

  constructor(protected router: Router) {
  }

  setCurrentBreadcrumb(value: BreadcrumbItem[]) {
    this.currentBreadcrumb = value;
    return this;
  }

  setGoBackUrl(value: string) {
    this.goBackUrl = value;
    return this;
  }

  action(): Observable<any> {
    return of(this.router.navigateByUrl(this.goBackUrl), {
      state: {
        goto: this.router.url,
        originalContext: {goto: this.router.url, breadcrumb: this.currentBreadcrumb}
      }
    });
  }
}
