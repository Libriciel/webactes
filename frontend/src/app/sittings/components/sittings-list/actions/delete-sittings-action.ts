import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Sitting } from '../../../../model/Sitting/sitting';
import { ActionResult, CommonIcons, ConfirmPopupComponent, IActuator } from '../../../../ls-common';
import { Observable, Subject } from 'rxjs';
import { IManageSittingService } from '../../../services/iManageSittingService';
import { SittingsListMessages } from '../SittingsListMessages';
import { Style } from '../../../../wa-common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeleteSittingsAction implements IActuator<Sitting> {

  messages = SittingsListMessages;
  needReload = true;

  constructor(
    protected modalService: NgbModal,
    protected sittingService: IManageSittingService,
  ) {
  }

  action(sittings: Sitting[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true,
      size: 'lg',
    });
    if (sittings.length === 1) {
      if (sittings[0].hasProjects) {
        modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_ONE_SITTING_WITH_ASSOCIATED_PROJECT_TITLE;
        modalRef.componentInstance.content = SittingsListMessages.CONFIRM_DELETE_MULTIPLE_SITTINGS_WITH_ASSOCIATED_PROJECT_CONTENT;
        modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_DELETE_SITTINGS_WITH_ASSOCIATED_PROJECT_BUTTON;
      } else {
        modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_ONE_SITTING_TITLE;
        modalRef.componentInstance.content = this.messages.confirmDeleteOneSittingContent(sittings[0].typesitting.name);
        modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_DELETE_SITTINGS_BUTTON;
      }
    } else {
      const someSittingsHaveAssociatedProjects = sittings.some(sitting => sitting.hasProjects);
      if (someSittingsHaveAssociatedProjects) {
        modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_MULTIPLE_SITTINGS_WITH_ASSOCIATED_PROJECT_TITLE;
        modalRef.componentInstance.content = this.messages.CONFIRM_DELETE_MULTIPLE_SITTINGS_WITH_ASSOCIATED_PROJECT_CONTENT;
        modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_DELETE_SITTINGS_WITH_ASSOCIATED_PROJECT_BUTTON;
      } else {
        modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_MULTIPLE_SITTINGS_TITLE;
        modalRef.componentInstance.content = this.messages.confirm_delete_multiple_sittings_content(sittings.length);
        modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_DELETE_SITTINGS_BUTTON;
      }
    }

    modalRef.componentInstance.style = Style.DANGER;
    modalRef.componentInstance.icon = CommonIcons.DELETE_ICON;

    const returnSubject = new Subject<ActionResult>();

    modalRef.result.then(() => this.sittingService.deleteSittings(sittings).subscribe({
        complete: () => {
          returnSubject.next({
            error: false,
            needReload: true,
            message: this.messages.delete_sittings_action_done(sittings),
          });
          returnSubject.complete();
        },
        error: (error) => {
          returnSubject.next({
            message: error.error ? error.error.error : this.messages.delete_sittings_action_failed(sittings),
            error: true,
            needReload: true,
          });
          returnSubject.complete();
        }
      }),
      () => returnSubject.complete()
    ).catch(error => {
      console.error('Caught error in popup for circuit deletion, this should not happen. Error : ', error);
    });
    return returnSubject.asObservable();
  }
}
