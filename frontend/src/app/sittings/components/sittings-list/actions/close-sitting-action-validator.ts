import { AvailableActionsBasedValidator } from '../../../../ls-common/components/tables/validator/available-actions-based-validator';
import { SittingAvailableActions } from '../../../../model/Sitting/sitting-available-actions.enum';
import { Sitting } from '../../../../model/Sitting/sitting';

export class CloseSittingActionValidator extends AvailableActionsBasedValidator<Sitting> {
  constructor() {
    super(SittingAvailableActions.canBeClosed);
  }
}
