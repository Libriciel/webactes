import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SittingModalComponent } from '../sitting-modal/sitting-modal.component';
import { SittingsListMessages } from './SittingsListMessages';
import { Sitting } from '../../../model/Sitting/sitting';
import { IManageSittingService } from '../../services/iManageSittingService';
import { SittingState } from '../../../model/Sitting/sitting-state.enum';
import { ActivatedRoute } from '@angular/router';
import { CommonIcons, CommonMessages, NotificationService } from '../../../ls-common';
import { Pagination } from '../../../model/pagination';
import { ActionSittingStateMapping } from './actions/action-sitting-state-mapping';
import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';

@Component({
  selector: 'wa-sittings-list',
  templateUrl: './sittings-list.component.html',
  styleUrls: ['./sittings-list.component.scss']
})
export class SittingsListComponent implements OnInit {

  readonly messages = SittingsListMessages;
  readonly waCommonMessages = WACommonMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly Number = Number;

  commonActions: ActionItemsSet<Sitting>[];
  singleActionsOthers: ActionItemsSet<Sitting>[];
  singleActions: ActionItemsSet<Sitting>;

  title = null;
  sittings: Sitting[];
  sittingState: SittingState = null;
  pagination: Pagination;
  filterString: string;
  loading = true;

  constructor(
    protected modalService: NgbModal,
    protected sittingService: IManageSittingService,
    protected notificationService: NotificationService,
    protected activatedRoute: ActivatedRoute,
    protected actionSittingStateMap: ActionSittingStateMapping,
  ) {
  }

  ngOnInit() {
    let activatedRoute = this.activatedRoute;
    while (activatedRoute.firstChild) {
      activatedRoute = activatedRoute.firstChild;
    }

    activatedRoute.data.subscribe(data => {
      if (data.sittingState) {
        this.sittingState = data.sittingState ? data.sittingState : SittingState.ALL;
        this.commonActions = this.actionSittingStateMap.getCommonActions(this.sittingState);
        this.singleActionsOthers = this.actionSittingStateMap.getOtherSingleActions(this.sittingState);
        this.singleActions = this.actionSittingStateMap.getSingleActions(this.sittingState);
      }
      this.gotoPage();
    });
  }

  createSitting() {
    const modalRef = this.modalService.open(SittingModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });
    modalRef.result
      .then(
        (newSitting: Sitting) => {
          if (newSitting) {
            this.gotoPage();
            this.notificationService.showSuccess(
              this.messages.sittingCreated(newSitting.typesitting ? newSitting.typesitting.name : '', newSitting.date));
          }
        })
      .catch(
        (error) => {
          if (error.message) {
            this.notificationService.showError(error.message);
          }
        });
  }

  reloadSittings() {
    this.gotoPage();
  }

  gotoPage(pageNumber = 1) {
    this.loading = true;
    this.sittings = null;
    const filters = this.filterString ? {searchedText: this.filterString} : null;
    this.sittingService.getSittings(this.sittingState, pageNumber, undefined, filters)
      .subscribe((sittings: { sittings: Sitting[], pagination: Pagination }) => {
        this.sittings = sittings.sittings;
        this.pagination = sittings.pagination;
        this.loading = false;
      });
  }

  applyFilter(event) {
    this.filterString = event.trim();
    this.reloadSittings();
  }

  hasFilter(): boolean {
    return this.filterString && this.filterString !== '';
  }
}
