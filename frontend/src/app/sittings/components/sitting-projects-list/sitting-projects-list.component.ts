import { Component, OnInit } from '@angular/core';
import { Sitting } from '../../../model/Sitting/sitting';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CommonIcons, CommonMessages, NotificationService } from '../../../ls-common';
import { Pagination } from '../../../model/pagination';
import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';
import { Project } from '../../../model/project/project';
import { ActionProjectStateMapping } from '../../../projects/components/projects-list/actions/action-project-state-mapping';
import { ProjectDisplayState } from '../../../model/project/project-display-state.enum';
import { AbstractProjectListComponent } from '../../../projects/components/projects-list/abstract-project-list-component';
import { ManageProjectService } from '../../../ls-common/services/manage-project.service';
import { ProjectActionItemFactory } from '../../../projects/components/projects-list/actions/project-action-item-factory';
import { ProjectAction } from 'src/app/projects/components/projects-list/actions/project-action.enum';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { ProjectsListMessages } from '../../../projects/components/projects-list/projects-list-messages';
import { ManageContainerSittingService } from '../../../ls-common/services/manage-container-sitting.service';

@Component({
  selector: 'wa-sitting-projects-list',
  templateUrl: './sitting-projects-list.component.html',
  styleUrls: ['./sitting-projects-list.component.scss']
})
export class SittingProjectsListComponent extends AbstractProjectListComponent implements OnInit {

  messages = ProjectsListMessages;
  waCommonMessages = WACommonMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  commonActions: ActionItemsSet<Project>[];
  singleActions: ActionItemsSet<Project>;
  otherSingleActions: ActionItemsSet<Project>[];

  title = null;
  sitting: Sitting;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};
  filterString: string;
  loading = true;
  params: Params;
  projectAction = ProjectAction;

  constructor(
    protected notificationService: NotificationService,
    protected activatedRoute: ActivatedRoute,
    public router: Router,
    protected projectService: ManageProjectService,
    protected actionProjectStateMapping: ActionProjectStateMapping,
    public projectActionItemFactory: ProjectActionItemFactory,
    protected containerSittingService: ManageContainerSittingService
  ) {
    super(projectService, activatedRoute);
  }

  ngOnInit() {

    this.projects = [];
    this.singleActions = {
      actionItems: []
    };
    this.otherSingleActions = [];
    this.gotoPage(1);
  }

  gotoPage(numPage) {
    this.loading = true;

    let activatedRoute = this.activatedRoute;
    while (activatedRoute.firstChild) {
      activatedRoute = activatedRoute.firstChild;
    }

    this.containerSittingService.getAllProjectsBySittingId(activatedRoute.snapshot.params['sittingId']).subscribe(projects => {

      this.projects = projects;
      this.commonActions = this.actionProjectStateMapping.getCommonActions(ProjectDisplayState.VALIDATED_INTO_SITTING);
      this.otherSingleActions = [];
      this.singleActions = this.actionProjectStateMapping.getSingleActions(
        ProjectDisplayState.VALIDATED_INTO_SITTING,
        activatedRoute.snapshot.params['sittingId']
      );
    });
  }


  goBack() {
    this.router.navigateByUrl(RoutingPaths.SITTING_RUNNING_PATH).then();
  }
}
