import { Component, Input, OnInit } from '@angular/core';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'wa-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  @Input() sittings = [];
  dateModel: {
    day: number;
    month: number;
    year: number;
  };
  selectedSittings = [];

  constructor() {
  }

  ngOnInit() {
  }

  pickerClicked() {
    if (!this.dateModel) {
      return;
    }
    this.selectedSittings = this.sittings
      .filter(el => {
        const date1 = new Date(el.date.getFullYear(), el.date.getMonth(), el.date.getDate());
        const date2 = new Date(this.dateModel.year, this.dateModel.month - 1, this.dateModel.day);
        return date1.getTime() === date2.getTime();
      });
    this.selectedSittings.forEach(el => el.formatedTime = new DatePipe('fr').transform(el.date, 'dd/MM/yyyy à HH:mm'));
  }

  isSitting(date: NgbDate): boolean {
    return this.sittings
      ? this.sittings
      .filter(el => {
        const date1 = new Date(el.date.getFullYear(), el.date.getMonth(), el.date.getDate());
        const date2 = new Date(date.year, date.month - 1, date.day);
        return date1.getTime() === date2.getTime();
      }).length > 0
      : false;
  }
}
