import { Component, OnInit } from '@angular/core';
import { CommonIcons, CommonMessages, NotificationService } from '../../../ls-common';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ManageProjectService } from '../../../ls-common/services/manage-project.service';
import { ProjectsListMessages } from '../../../projects/components/projects-list/projects-list-messages';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';
import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { Project } from '../../../model/project/project';
import { Sitting } from '../../../model/Sitting/sitting';
import { Pagination } from '../../../model/pagination';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { AbstractProjectListComponent } from '../../../projects/components/projects-list/abstract-project-list-component';
import { ProjectActionItemFactory } from '../../../projects/components/projects-list/actions/project-action-item-factory';
import { ContainersSittingsService } from '../../../ls-common/services/http-services/sittings/containers-sittings.service';
import { ContainerSitting } from '../../../model/Sitting/container-sitting';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'wa-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent extends AbstractProjectListComponent implements OnInit {

  messages = ProjectsListMessages;
  waCommonMessages = WACommonMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  commonActions: ActionItemsSet<Project>[];
  singleActions: ActionItemsSet<Project>;
  otherSingleActions: ActionItemsSet<Project>[];

  title = null;
  sitting: Sitting;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};
  filterString: string;
  loading = true;
  params: Params;
  containers_sitting: ContainerSitting[];

  constructor(
    protected notificationService: NotificationService,
    protected activatedRoute: ActivatedRoute,
    public router: Router,
    protected projectService: ManageProjectService,
    protected containerSittingService: ContainersSittingsService,
    public projectActionItemFactory: ProjectActionItemFactory,
  ) {
    super(projectService, activatedRoute);
  }

  ngOnInit() {
    this.gotoPage(1);
  }

  gotoPage(numPage) {
    this.loading = true;

    let activatedRoute = this.activatedRoute;
    while (activatedRoute.firstChild) {
      activatedRoute = activatedRoute.firstChild;
    }
    this.containerSittingService.getAll(activatedRoute.snapshot.params['sittingId']).subscribe(data => {
      this.containers_sitting = data.containersSittings;

      // Mise a jour des positions si un projet n'a pas d'ordre dans la séance
      if (this.containers_sitting.find(container_sitting => !container_sitting.rank)) {
        this.updateRank();
      }
    });
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.SITTING_RUNNING_PATH).then();
  }

  onDrop($event: CdkDragDrop<ContainerSitting[]>) {
    moveItemInArray(this.containers_sitting, $event.previousIndex, $event.currentIndex);
    this.updateRank();
  }

  updateRank() {
    this.containers_sitting.forEach((container_sitting, idx) => {
      container_sitting.rank = idx + 1;
    });
    this.containerSittingService.edit(this.containers_sitting).subscribe({
        complete: () => {
          this.notificationService.showSuccess('L\'ordre du jour à été enregistré avec succès.');
        },
        error: () => {
          this.notificationService.showError('Erreur lors de l\'ordre du jour.');
        }
      }
    );
  }

  announceSortChange(sortEvent: Sort) {
    if (!sortEvent.active || sortEvent.direction === '') {
      this.activatedRoute.data.subscribe(data => {
        this.containers_sitting = data.schedule.containersSittings;
      });
      return;
    }

    this.containers_sitting = this.containers_sitting.sort((projectOne, projectTwo) => {
      const isAsc = sortEvent.direction === 'asc';
      switch (sortEvent.active) {
        case 'theme':
          return this.compare(
            projectOne.container.project.theme.name,
            projectTwo.container.project.theme.name,
            isAsc
          );
        case 'stateact':
          return this.compare(
            projectOne.container.stateacts[projectOne.container.stateacts.length - 1].name,
            projectTwo.container.stateacts[projectTwo.container.stateacts.length - 1].name,
            isAsc
          );
        case 'code_act' :
          return this.compare(
            projectOne.container.project.code_act,
            projectTwo.container.project.code_act,
            isAsc
          );
        case 'name':
          return this.compare(
            projectOne.container.project.name,
            projectTwo.container.project.name,
            isAsc
          );
        default:
          return 0;
      }
    });

    this.updateRank();
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}
