import { DatePipe } from '@angular/common';

export class SittingModalMessages {
  static CREATE_NEW_SITTING = `Création d'une séance`;
  static EDIT_SITTING = 'Modification d\'une séance';
  static SITTING_TYPE = 'Type de séance';
  static SITTING_TYPE_PLACE_HOLDER = 'Choisissez un type de séance';
  static SITTING_DATE = 'Date de la séance';
  static SITTING_TIME = 'Heure début';
  static SITTINGS_CALENDAR = 'Calendrier des séances :';
  static SITTING_ADD = 'Créer la séance';
  static SITTING_UPDATE = 'Enregistrer';
  static SITTING_PRESIDENT = 'Président de la séance';
  static SITTING_PRESIDENT_PLACE_HOLDER = 'Président de la séance.';
  static SITTING_SECRETARY = 'Secrétaire de la séance';
  static SITTING_SECRETARY_PLACE_HOLDER = 'Secrétaire de la séance.';
  static SITTING_CONVOCATION_DATE = 'Date de la convocation';
  static SITTING_PLACE = 'Lieu de la séance';

  static sameDate(date: Date) {
    return `Une séance est déja prévue à ${new DatePipe('fr').transform(date, 'HH:mm le dd/MM/yyyy')}.<br>
        Veuillez selectionner un autre horaire ou une autre date.`;
  }
}
