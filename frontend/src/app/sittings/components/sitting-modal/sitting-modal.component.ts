import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { SittingModalMessages } from './sitting-modal-messages';
import { TypeSitting } from '../../../model/type-sitting/type-sitting';
import { Sitting } from '../../../model/Sitting/sitting';
import { ICreateSittingService } from '../../services/iCreateSittingService';
import { forkJoin } from 'rxjs';
import { NgbDateFrenchParserFormatter } from '../../../utility/NgbDateCustomParserFormatter';
import { padNumber } from '../../../utility/NgBoostrapUtils';
import { Weight } from '../../../wa-common/weight.enum';
import { CommonIcons, CommonMessages } from '../../../ls-common';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';
import { Actor, ActorListConfig, ActorsService } from '../../../core';
import { ActorList } from '../../../admin/actors/shared/models/actor-list.model';
import { ProjectsMessages } from '../../../projects/i18n/projects-messages';

@Component({
  selector: 'wa-sitting-modal',
  templateUrl: './sitting-modal.component.html',
  styleUrls: ['./sitting-modal.component.scss']
})
export class SittingModalComponent implements OnInit, AfterViewInit {
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  waCommonMessages = WACommonMessages;
  weight = Weight;
  @ViewChild('datePicker', {static: true}) datePicker: NgbInputDatepicker;
  @ViewChild('dateConvocationPicker', {static: true}) dateConvocationPicker: NgbInputDatepicker;
  @ViewChild('timePicker', {static: true}) timePicker: ElementRef;
  typeSittings: TypeSitting[];
  sittings: Sitting[];
  selectedType: TypeSitting;
  selectedDate: {
    year: number,
    month: number,
    day: number
  };
  selectedConvocationDate: {
    year: number,
    month: number,
    day: number
  };
  selectedTime: {
    hour: number,
    minute: number
  } = null;
  isSubmitDisabled = true;
  timePickerErrorMessage = '';
  isSubmitting = false;
  messages = SittingModalMessages;
  @Input()
  sitting: Sitting = new Sitting(null);
  timeDate: string;

  // Mandatory for actors
  actors: ActorList[];
  query: ActorListConfig;
  limit = 150;
  listConfig: ActorListConfig = {
    type: 'all',
    filters: {
      active: true,
      limit: 150
    }
  };
  public sittingPresident: Actor;
  public sittingSecretary: Actor;

  constructor(
    public activeModal: NgbActiveModal,
    protected sittingService: ICreateSittingService,
    protected actorsService: ActorsService,
    protected ngbDateFrenchParserFormatter: NgbDateFrenchParserFormatter,
    private renderer: Renderer2, private el: ElementRef) {
  }

  ngAfterViewInit(): void {
    const textarea = this.el.nativeElement.querySelector('textarea');
    if (textarea) {
      this.renderer.setAttribute(textarea, 'rows', '4');
      this.renderer.setStyle(textarea, 'resize', 'none');
      this.renderer.setStyle(textarea, 'max-height', '6em');
    }
  }

  isCalendarShowing(): boolean {
    return this.datePicker !== undefined && this.datePicker.isOpen();
  }

  ngOnInit() {
    if (this.sitting.id) {
      this.fillData();
    }

    const filters = {active: true};
    forkJoin([this.sittingService.getAllSittings(), this.sittingService.getAllTypes(filters, this.limit),
      this.actorsService.query(this.listConfig)])
      .subscribe(([sittings, typeSittings, actors]) => {
        this.sittings = sittings;
        this.typeSittings = typeSittings;
        this.actors = actors.actors as ActorList[];

        if (this.sitting.id) {
          this.selectedType = this.typeSittings.find(typeSitting => typeSitting.id === this.sitting.typesitting_id);
          this.sittingSecretary = this.actors.find(actor => actor.id === this.sitting.secretary_id);
          this.sittingPresident = this.actors.find(actor => actor.id === this.sitting.president_id);
        }
        this.checkDataValidity();
      });
  }

  onTimePickerChange(data: string) {
    if (data) {
      const timeArray = data.split(':');
      this.selectedTime = {
        minute: +timeArray[1],
        hour: +timeArray[0]
      };
    }
    this.checkDataValidity();
  }

  /**
   * add sittings if everything's ok
   * promise return {id: sittingId}
   */
  addOrUpdateSitting() {
    this.sitting.date = new Date(
      this.selectedDate.year, this.selectedDate.month - 1, this.selectedDate.day,
      this.selectedTime.hour, this.selectedTime.minute);

    if (this.selectedConvocationDate) {
      this.sitting.date_convocation = new Date(this.selectedConvocationDate.year,
        this.selectedConvocationDate.month - 1,
        this.selectedConvocationDate.day);
    } else {
      this.sitting.date_convocation = null;
    }
    this.sitting.typesitting_id = this.selectedType.id;
    this.sitting.typesitting = null;

    this.isSubmitting = true;

    this.sitting.president_id = this.sittingPresident ? this.sittingPresident.id : undefined;
    this.sitting.secretary_id = this.sittingSecretary ? this.sittingSecretary.id : undefined;


    const call = !this.sitting.id
      ? this.sittingService.addSitting(this.sitting)
      : this.sittingService.updateSitting(this.sitting);
    call.subscribe(
      data => {
        this.isSubmitting = false;
        this.activeModal.close(data);
      },
      err => {
        console.error(err);
        this.isSubmitting = false;
        this.activeModal.dismiss(err);
      });
  }

  /**
   * check if everything is ok and show error message
   */
  checkDataValidity(): void {
    this.timePickerErrorMessage = '';
    this.isSubmitDisabled = true;
    // if everything is filled
    if (this.selectedType && this.selectedDate && this.ngbDateFrenchParserFormatter.format(this.selectedDate) && this.selectedTime) {
      const selectedDatetime = new Date(
        this.selectedDate.year, this.selectedDate.month - 1, this.selectedDate.day,
        this.selectedTime.hour, this.selectedTime.minute);
      const match = this.sittings
        .filter(el => {
          return el.date.getTime() === selectedDatetime.getTime() && el.id !== this.sitting.id;
        });

      if (match.length > 0) {
        this.timePickerErrorMessage = this.messages.sameDate(selectedDatetime);
      } else {
        // if no error and everything is filled
        this.isSubmitDisabled = false;
      }
    }
  }

  close(): void {
    this.activeModal.dismiss({isError: false});
  }

  private fillData() {
    this.selectedDate = {
      year: this.sitting.date.getFullYear(),
      month: this.sitting.date.getMonth() + 1,
      day: this.sitting.date.getDate()
    };

    if (this.sitting.date_convocation instanceof Date) {
      this.selectedConvocationDate = {
        year: this.sitting.date_convocation.getFullYear(),
        month: this.sitting.date_convocation.getMonth() + 1,
        day: this.sitting.date_convocation.getDate()
      };
    }

    this.selectedTime = {
      hour: this.sitting.date.getHours(),
      minute: this.sitting.date.getMinutes()
    };
    this.timeDate = `${padNumber(this.sitting.date.getHours())}:${padNumber(this.sitting.date.getMinutes())}`;
  }
}
