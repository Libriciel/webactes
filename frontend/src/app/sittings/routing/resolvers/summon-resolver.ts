import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Summon } from '../../../model/summon/summon';
import { Observable, throwError } from 'rxjs';
import { IManageSummonService } from '../../../model/summon/imanage-summon-service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SummonResolver implements Resolve<Summon> {
  constructor(protected manageSummonService: IManageSummonService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Summon> {
    return this.manageSummonService.getBySittingId(Number(route.paramMap.get('sittingId'))).pipe(
      catchError(err => {
        console.error(`Error retrieving summon for sitting ${route.paramMap.get('sittingId')} : `, err);
        return throwError(`Error retrieving summon for sitting ${route.paramMap.get('sittingId')}`);
      })
    );
  }
}
