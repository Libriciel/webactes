import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { IManageSittingService } from '../../services/iManageSittingService';
import { Sitting } from '../../../model/Sitting/sitting';
import { catchError } from 'rxjs/operators';
import { Pagination } from '../../../core';

@Injectable({
  providedIn: 'root'
})
export class SittingResolver implements Resolve<{sitting: Sitting, pagination: Pagination}> {
  constructor(protected  iManageSittingService: IManageSittingService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{sitting: Sitting, pagination: Pagination}> {
    return this.iManageSittingService.get(route.paramMap.get('sittingId')).pipe(
      catchError(err => {
        console.warn('Error retrieving sitting ' + route.paramMap.get('sittingId') + ' : ', err);
        return throwError(`Error retrieving sitting ${route.paramMap.get('sittingId')}`);
      })
    );
  }
}
