import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { IManageSittingService } from '../../services/iManageSittingService';
import { Sitting } from '../../../model/Sitting/sitting';
import { catchError } from 'rxjs/operators';
import { Vote } from '../../../model/vote/vote';

@Injectable({
  providedIn: 'root'
})
export class SittingProjectsVotesResolver implements Resolve<{ sitting: Sitting, vote: Vote }> {
  constructor(protected manageSittingService: IManageSittingService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ sitting: Sitting, vote: Vote }> {
    return this.manageSittingService
      .getWithProjectVotes(Number(route.paramMap.get('sittingId')), Number(route.paramMap.get('projectId')))
      .pipe(
        catchError(err => {
          console.warn('Error retrieving sitting' + route.paramMap.get('sittingId') + ' : ', err);
          return throwError(`Error retrieving sitting ${route.paramMap.get('sittingId')}`);
        })
      );
  }
}
