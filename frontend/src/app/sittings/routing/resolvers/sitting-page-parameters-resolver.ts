import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { Observable } from 'rxjs';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { map } from 'rxjs/operators';
import { BreadcrumbItem } from '../../../wa-common/model/breadcrumb-item';
import { IManageSittingService } from '../../services/iManageSittingService';
import { SittingsRoutingMessages } from '../sittings-routing-messages';

@Injectable({
  providedIn: 'root'
})
export class SittingPageParametersResolver implements Resolve<PageParameters> {

  previousBreadcrumb: BreadcrumbItem[];

  constructor(protected iManageSittingService: IManageSittingService, protected router: Router) {
  }

  // Compute breadcrumb in case of showing or editing a project or act
  // Depends of parameters passed by navigation or the state of the project or act
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
  // eslint-disable-next-line @typescript-eslint/type-annotation-spacing
    : Observable<PageParameters> | Promise<PageParameters> | PageParameters {

    return this.iManageSittingService.get(route.params['sittingId']).pipe(map(sitting => {
        // Retrieve first part of breadcrumb if available
        this.previousBreadcrumb = this.router.getCurrentNavigation().extras.state
        && this.router.getCurrentNavigation().extras.state.originalContext
          ? this.router.getCurrentNavigation().extras.state.originalContext.breadcrumb : null;

        const breadcrumbVisualization = [
          {
            name: SittingsRoutingMessages.PAGE_SITTINGS_SHOW_TITLE,
            location: '/' + RoutingPaths.sittingPath(sitting.sitting.id)
          }];

        const breadcrumbVisualizationSingle = [
          {
            name: SittingsRoutingMessages.PAGE_SITTING_SHOW_TITLE,
            location: '/' + RoutingPaths.sittingPath(sitting.sitting.id)
          }];

        const breadcrumbVisualizationSend = [
          {
            name: SittingsRoutingMessages.PAGE_SITTING_SEND_TITLE,
            location: '/' + RoutingPaths.sendSittingPath(String(sitting.sitting.id))
          }];

        const breadcrumbVisualizationSchedule = [
          {
            name: SittingsRoutingMessages.PAGE_SITTING_SCHEDULE_TITLE,
            location: '/' + RoutingPaths.scheduleSittingPath(String(sitting.sitting.id))
          }];

        const routeParameterMap = new Map<string, PageParameters>([
          [RoutingPaths.SITTING_SHOW_PATH, {
            title: route.data.sittingTitleFragment + SittingsRoutingMessages.getSittingName(sitting.sitting),
            breadcrumb: breadcrumbVisualization,
            fluid: true
          }],
          [RoutingPaths.sittingPath(route.params['sittingId']), {
            title: route.data.sittingTitleFragment + SittingsRoutingMessages.getSittingName(sitting.sitting),
            breadcrumb: breadcrumbVisualizationSingle,
            fluid: true
          }],
          [RoutingPaths.SITTING_SHOW_PATH + '/envoie', {
            title: route.data.sittingTitleFragment + SittingsRoutingMessages.getSittingName(sitting.sitting),
            subTitle: '',
            breadcrumb: breadcrumbVisualizationSend
          }],
          [RoutingPaths.SITTING_SCHEDULE_SHOW_PATH, {
            title: route.data.sittingTitleFragment + SittingsRoutingMessages.getSittingName(sitting.sitting),
            subTitle: '',
            fluid: true,
            breadcrumb: breadcrumbVisualizationSchedule
          }]
        ]);
        return routeParameterMap.get(route.routeConfig.path);
      }
    ));
  }
}
