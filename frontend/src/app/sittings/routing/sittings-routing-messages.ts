import { Sitting } from '../../model/Sitting/sitting';
import { DatePipe } from '@angular/common';

export class SittingsRoutingMessages {
  static PAGE_SITTINGS_TITLE = 'Séances';
  static PAGE_PROJETS_TITLE = 'Projets';
  static PAGE_SITTINGS_PROJECTS_TITLE = 'Projets sans séances';
  static PAGE_SITTINGS_RUNNING_TITLE = 'Séances en cours';
  static PAGE_SITTINGS_CLOSED_TITLE = 'Séances closes';
  static PAGE_SITTING_SHOW_TITLE =  'Séance';
  static PAGE_SITTINGS_SHOW_TITLE =  'Visualisation de la séance';
  static PAGE_SITTING_PROJECTS_LIST_TITLE = 'Détail de la séance';
  static PAGE_SITTING_SEND_TITLE =  'Destinataires de la séance';
  static PAGE_SITTING_SCHEDULE_TITLE = 'Ordre du jour de la séance';

  static getSittingName(sitting: Sitting){
    return ' : ' + sitting.typesitting.name
      + ' du ' + `${new DatePipe('fr').transform(sitting.date, 'dd/MM/yyyy à HH:mm')}`;
  }
}
