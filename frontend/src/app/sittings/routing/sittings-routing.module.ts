import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SittingsListComponent } from '../components/sittings-list/sittings-list.component';
import { SittingsRoutingMessages } from './sittings-routing-messages';
import { RoutingPaths } from '../../wa-common/routing-paths';
import { PageParameters } from '../../wa-common/components/page/model/page-parameters';
import { SittingState } from '../../model/Sitting/sitting-state.enum';
import { SittingProjectsListComponent } from '../components/sitting-projects-list/sitting-projects-list.component';
import { SittingResolver } from './resolvers/sitting-resolver';
import { ProjectDisplayState } from '../../model/project/project-display-state.enum';
import { SittingVotersListComponent } from '../components/sitting-voters-list/sitting-voters-list.component';
import { ScheduleComponent } from '../components/schedule/schedule.component';
import { SittingPageParametersResolver } from './resolvers/sitting-page-parameters-resolver';
import { SummonResolver } from './resolvers/summon-resolver';

const routes: Routes = [
  {
    path: RoutingPaths.SITTINGS_PATH,
    redirectTo: RoutingPaths.SITTING_RUNNING_PATH,
    pathMatch: 'full'
  }, {
    path: RoutingPaths.SITTING_CLOSED_PATH,
    component: SittingsListComponent,
    data: {
      pageParameters: {
        title: SittingsRoutingMessages.PAGE_SITTINGS_CLOSED_TITLE,
        breadcrumb: [
          {name: SittingsRoutingMessages.PAGE_SITTINGS_TITLE},
          {
            name: SittingsRoutingMessages.PAGE_SITTINGS_CLOSED_TITLE,
            location: '/' + RoutingPaths.SITTING_CLOSED_PATH
          }
        ],
        fluid: true
      } as PageParameters,
      sittingState: SittingState.CLOSED as SittingState
    },
  }, {
    path: RoutingPaths.SITTING_RUNNING_PATH,
    component: SittingsListComponent,
    data: {
      projectState: ProjectDisplayState.VALIDATED_INTO_SITTING as ProjectDisplayState,
      pageParameters: {
        title: SittingsRoutingMessages.PAGE_SITTINGS_RUNNING_TITLE,
        breadcrumb: [
          {name: SittingsRoutingMessages.PAGE_SITTINGS_TITLE},
          {
            name: SittingsRoutingMessages.PAGE_SITTINGS_RUNNING_TITLE,
            location: '/' + RoutingPaths.SITTING_RUNNING_PATH
          }
        ],
        fluid: true
      } as PageParameters,
      sittingState: SittingState.OPENED as SittingState
    }
  }, {
    path: RoutingPaths.SITTING_SHOW_PATH, // seance/:sittingId
    component: SittingProjectsListComponent,
    resolve: {
      pageParameters: SittingPageParametersResolver,
    },
    data: {
      projectState: ProjectDisplayState.VALIDATED_INTO_SITTING,
      sittingState: SittingState.OPENED as SittingState,
      sittingTitleFragment: SittingsRoutingMessages.PAGE_SITTING_SHOW_TITLE
    }
  }, {
    path: RoutingPaths.sendSittingPath(':sittingId'),
    component: SittingVotersListComponent,
    resolve: {
      summon: SummonResolver,
      pageParameters: SittingPageParametersResolver,
    },
    data: {
      projectState: ProjectDisplayState.VALIDATED_INTO_SITTING,
      sittingState: SittingState.OPENED as SittingState,
      sittingTitleFragment: SittingsRoutingMessages.PAGE_SITTING_SEND_TITLE
    }
  }
  , {
    path: RoutingPaths.scheduleSittingPath(':sittingId'),
    component: ScheduleComponent,
    resolve: {
      pageParameters: SittingPageParametersResolver,
      sitting: SittingResolver,
    },
    data: {
      sittingTitleFragment: SittingsRoutingMessages.PAGE_SITTING_SCHEDULE_TITLE,
      sittingState: SittingState.OPENED as SittingState
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class SittingsRoutingModule {
}
