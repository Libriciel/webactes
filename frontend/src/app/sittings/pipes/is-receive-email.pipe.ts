import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'isReceiveEmail'
})
export class IsReceiveEmailPipe implements PipeTransform {

  transform(date: any, args?: any): any {
    return date ? `${new DatePipe('fr').transform(date, 'dd/MM/yyyy à HH:mm')}` : 'Non Reçu';
  }

}
