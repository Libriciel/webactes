import { Observable } from 'rxjs';
import { Sitting } from '../../model/Sitting/sitting';
import { SittingState } from '../../model/Sitting/sitting-state.enum';
import { Pagination } from '../../model/pagination';
import { FilterType } from '../../model/util/filter-type';
import { Vote } from '../../model/vote/vote';
import { TypesAct } from '../../model/type-act/types-act';
import { SittingOverview } from '../../model/Sitting/sitting-overview';

export abstract class IManageSittingService {

  abstract getAllSittings(typeAct: TypesAct): Observable<SittingOverview[]>;

  abstract getSittings(sittingState: SittingState, pageNumber: number, limit: number, filters?: FilterType)
    : Observable<{ sittings: Sitting[], pagination: Pagination }>;

  abstract close(sittings: Sitting[]): Observable<any>;

  abstract deleteSittings(sittings: Sitting[]): Observable<any>;

  abstract get(id: string): Observable<{ sitting: Sitting, pagination: Pagination }>;

  abstract getWithProjectVotes(id: number, projectId: number): Observable<{ sitting: Sitting, vote: Vote }>;

  abstract getDownloadDeliberationsList(sittingId: number, mimeType: string, fileName?: string): Observable<any>;

  abstract getDownloadVerbalTrial(sittingId: number, mimeType: string, fileName: string): Observable<any>;

  abstract generateSittingDocuments(summonId: number): Observable<any>;

}
