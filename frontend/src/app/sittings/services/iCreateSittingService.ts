import { Observable } from 'rxjs';
import { TypeSitting } from '../../model/type-sitting/type-sitting';
import { Sitting } from '../../model/Sitting/sitting';
import { FilterType } from '../../model/util/filter-type';

export abstract class ICreateSittingService {

  abstract getAllSittings(): Observable<Sitting[]>;

  abstract addSitting(sitting: Sitting): Observable<Sitting>;

  abstract getAllTypes(filters?: FilterType, limit?: number): Observable<TypeSitting[]>;

  abstract updateSitting(sitting: Sitting): Observable<Sitting>;
}
