import { Sitting } from '../../model/Sitting/sitting';
import { ContainerSitting } from '../../model/Sitting/container-sitting';
import { Observable } from 'rxjs';
import { Project } from '../../model/project/project';

export abstract class IManageContainersSittingService {

  abstract delete(sittings: Sitting[]);


  abstract getAllProjectsBySittingId(sittingId?: string): Observable<Project[]>;

  abstract getById(id: string);

  abstract edit(containerSittings: ContainerSitting[]);

}
