import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SittingModalComponent } from './components/sitting-modal/sitting-modal.component';
import { SittingsListComponent } from './components/sittings-list/sittings-list.component';
import { SittingsRoutingModule } from './routing/sittings-routing.module';
import { CalendarComponent } from './components/calendar/calendar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { LibricielCommonModule } from '../ls-common/libriciel-common.module';
import { WaCommonModule } from '../wa-common/wa-common.module';
import { LsComposantsModule } from '@libriciel/ls-composants';
import { SittingProjectsListComponent } from './components/sitting-projects-list/sitting-projects-list.component';
import { ProjectsModule } from '../projects/projects.module';
import { RouterModule } from '@angular/router';
import { SittingVotersListComponent } from './components/sitting-voters-list/sitting-voters-list.component';
import { IsSendEmailPipe } from './pipes/is-send-email.pipe';
import { IsReceiveEmailPipe } from './pipes/is-receive-email.pipe';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SharedModule } from '../shared';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSortModule } from '@angular/material/sort';
import { ManageSummonComponent } from './components/sitting-voters-list/manage-summon/manage-summon.component';

@NgModule({
  declarations: [
    SittingModalComponent,
    SittingsListComponent,
    CalendarComponent,
    SittingProjectsListComponent,
    SittingVotersListComponent,
    IsSendEmailPipe,
    IsReceiveEmailPipe,
    ScheduleComponent,
    ManageSummonComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    NgSelectModule,
    BrowserAnimationsModule,
    LibricielCommonModule,
    SittingsRoutingModule,
    WaCommonModule,
    LsComposantsModule,
    ProjectsModule,
    RouterModule,
    NgxSpinnerModule,
    SharedModule,
    DragDropModule,
    MatSortModule,
  ],
  exports: [
    SittingsListComponent,
    IsSendEmailPipe,
    IsReceiveEmailPipe,
  ]
})
export class SittingsModule {
}
