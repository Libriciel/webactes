import { CommonIcons } from './ls-common/icons/common-icons';

export class AppMessages {
  static MENU_PROJECTS_NAME = 'Projets';
  static MENU_PROJECTS_ICON = 'fas fa-folder';
  static MENU_PROJECTS_DRAFT_NAME = 'Brouillons';
  static MENU_PROJECTS_VALIDATING_NAME = 'En cours dans un circuit';
  static MENU_PROJECTS_TO_BE_VALIDATED_NAME = 'À traiter';
  static MENU_PROJECTS_VALIDATED_NAME = 'Validés';

  static MENU_SITTINGS_NAME = 'Séances';
  static MENU_SITTINGS_ICON = 'fa ls-font ls-seance';
  static MENU_SITTINGS_PROJECTS_NAME = 'Projets sans séances';
  static MENU_SITTINGS_RUNNING_NAME = 'Séances en cours';
  static MENU_SITTINGS_CLOSED_NAME = 'Séances closes';

  static MENU_TELETRANSMISSION_NAME = 'Télétransmission';
  static MENU_TELETRANSMISSION_ICON = CommonIcons.SIGNED_ACTS_ICON;
  static MENU_TELETRANSMISSION_TO_BE_SENT_NAME = 'À déposer sur le Tdt';
  static MENU_TELETRANSMISSION_READY_FOR_TELETRANSMISSION_NAME = 'À envoyer en préfecture';

  static MENU_ACTS_NAME = 'Actes';
  static MENU_ACTS_ICON = 'fa ls-icon-acte';
  static MENU_ACTS_NAME_LIST_NAME = 'Liste des actes';

  static MENU_ADMIN_ORGANIZATION = 'Personnalisation';
  static MENU_ADMIN_ORGANIZATIONS = 'Structures';
  static MENU_ADMIN_CONNEXION_PASTELL = 'Connexion Pastell';
  static MENU_ADMIN_CONNEXION_IDELIBRE = 'Connexion i-délibRE';
  static MENU_ADMIN_ACTOR = 'Acteurs';
  static MENU_ADMIN_ACTOR_GROUP = 'Groupes d\'acteurs';
  static MENU_ADMIN_DRAFT_TEMPLATES = 'Gabarits / textes par défaut';
  static MENU_ADMIN_GENERATE_TEMPLATES = 'Modèles de document';
  static MENU_ADMIN_STRUCTURE_SETTINGS = 'Configuration';

  static MENU_PREFERENCES_NOTIFICATIONS_OPTIONS = 'Notifications';
  static MENU_ADMIN_THEME = 'Thèmes';
  static MENU_ADMIN_TYPE_SEANCE = 'Types de séance';
  static MENU_ADMIN_SEQUENCE = 'Séquences';
  static MENU_ADMIN_TYPES_ACTS = 'Types d\'acte';
  static MENU_ADMIN_COUNTER = 'Compteurs';
  static MENU_ADMIN_USER = 'Utilisateurs';
  static MENU_ADMIN_ALL_USERS = 'Tous les utilisateurs';
}
