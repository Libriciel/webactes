import { CommonIcons } from '../../ls-common/icons/common-icons';

export class ActsMessages {
  static BUTTON_SEND_TO_CONTROL_TITLE = 'Envoyer au tdt';
  static BUTTON_SEND_TO_CONTROL_ICON = 'fa ls-icon-transmettre-au-tdt';
  static BUTTON_CHECK_AND_SEND_TO_CONTROL_TITLE = 'Envoyer au tdt';
  static BUTTON_SHOW_TDT_INFO_TITLE = 'Vérifier les infos de télétransmission';
  static BUTTON_CHECK_AND_SEND_TO_CONTROL_ICON = CommonIcons.SIGNED_ACTS_ICON;
  static MULTICHANNEL_INFO_TOOLTIP = `Permet d'indiquer à la préfecture que l'envoi n'est pas complet,\
                                      et que des pièces papier seront envoyées par voie postale`;

  static BUTTON_ORDER_TELETRANSMISSION_TITLE = 'Envoyer en préfecture';
  static BUTTON_ORDER_TELETRANSMISSION_ICON = 'fa ls-icon-ordonner-teletransmission';

  static ACT_UPDATE_SUCCESS_MESSAGE = 'Modifications enregistrées avec succès\u00a0!';
  static ACT_UPDATE_ERROR_MESSAGE = `Erreur lors de la mise à jour de l'Acte`;
  static ACT_SENT_TO_TDT_SUCCESS_MESSAGE = 'Acte envoyé au TDT avec succès\u00a0!';
  static ACT_SENT_TO_TDT_FAILURE_MESSAGE = `Échec de l'envoi au TDT`;

  static CANT_ORDER_TELETRANSMISSION_MESSAGE = `Impossible d'ordonner la télétransmission`;
  static ACT_TELETRANSMISSION_ORDERED_MESSAGE = 'Ordre de télétransmission envoyé';
  static ACT_TELETRANSMISSION_ORDERED_SUCCESS_MESSAGE = `Télétransmission de l'acte ordonnée avec succès`;
  static ACT_TELETRANSMISSION_ORDERED_FAILURE_MESSAGE = `Échec de la télétransmission de l'acte`;

  static IS_MULTICHANNEL = 'Envoi de document papiers complémentaire';
  static MAIN_DOC_TYPE = 'Type de pièce principale';
  static ANNEXES = 'Annexes';
  static DOCUMENT = 'Documents';
  static TYPOLOGY = 'Typologie';
  static REMOTE_TRANSMISSION = 'À télétransmettre';
  static TYPE_PLACE_HOLDER = 'Sélectionner un type';
  static ANNEX_TYPE_NOT_SELECTABLE_TOOLTIP = '';
  static NON_TRANSFERABLE_ANNEXE = '';

  static get_tdt_info_modal_title_for_act(actCode: string) {
    return `Information de télétransmission de l'acte ${(actCode ? actCode : '')}`;
  }
}
