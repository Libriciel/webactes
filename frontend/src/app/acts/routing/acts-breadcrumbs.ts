import { RoutingPaths } from '../../wa-common/routing-paths';
import { BreadcrumbItem } from '../../wa-common/model/breadcrumb-item';
import { ActsRoutingMessages } from './acts-routing-messages';

export class ActsBreadcrumbs {

  static TELETRANSMISSION_BREADCRUMB = {name: ActsRoutingMessages.PAGE_TELETRANSMISSION_ACTS_TITLE};
  static ACTS_BREADCRUMB = {name: ActsRoutingMessages.PAGE_ACTS_TITLE};
  static PathBreadcrumb = new Map<string, BreadcrumbItem[]>([
    [RoutingPaths.TELETRANSMISSION_PATH, [
      ActsBreadcrumbs.TELETRANSMISSION_BREADCRUMB]],
    [RoutingPaths.ACTS_PATH_LIST_PATH, [
      ActsBreadcrumbs.ACTS_BREADCRUMB, {
        name: ActsRoutingMessages.PAGE_ACTS_LIST_TITLE,
        location: RoutingPaths.ACTS_PATH_LIST_PATH
      }]],
    [RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_TDT_PATH, [
      ActsBreadcrumbs.TELETRANSMISSION_BREADCRUMB, {
        name: ActsRoutingMessages.PAGE_SIGNED_PROJECTS_TO_BE_SENT_TITLE,
        location: RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_TDT_PATH
      }]],
    [RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_PREFECTURE_PATH, [
      ActsBreadcrumbs.TELETRANSMISSION_BREADCRUMB, {
        name: ActsRoutingMessages.PAGE_ACTS_READY_FOR_TELETRANSMISSION_TITLE,
        location: RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_PREFECTURE_PATH
      }]],
    [RoutingPaths.TDT_AUTHENTICATION_RESPONSE_PATH, [
      ActsBreadcrumbs.TELETRANSMISSION_BREADCRUMB, {
        name: ActsRoutingMessages.PAGE_TDT_AUTHENTICATION_RESPONSE_TITLE,
        location: RoutingPaths.TDT_AUTHENTICATION_RESPONSE_PATH
      }]]
  ]);
}
