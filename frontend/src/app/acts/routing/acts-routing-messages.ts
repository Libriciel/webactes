export class ActsRoutingMessages {
  static PAGE_TELETRANSMISSION_ACTS_TITLE = 'Télétransmission';
  static PAGE_SIGNED_PROJECTS_TO_BE_SENT_TITLE = 'Actes à déposer sur le TDT';
  static PAGE_ACTS_READY_FOR_TELETRANSMISSION_TITLE = 'Actes à envoyer en préfecture';
  static PAGE_ACTS_TITLE = 'Actes';
  static PAGE_ACTS_LIST_TITLE = 'Liste des actes';
  static PAGE_TDT_AUTHENTICATION_RESPONSE_TITLE = 'Réponse du TDT';
}
