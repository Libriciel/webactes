import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { ActsListComponent } from '../../components/acts-list/acts-list.component';
import { ActsRoutingMessages } from '../acts-routing-messages';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { TdtAuthenticationCallbackComponent } from '../../components/tdt-authentication-callback/tdt-authentication-callback.component';
import { ProjectDisplayState } from 'src/app/model/project/project-display-state.enum';
import { ActsBreadcrumbs } from '../acts-breadcrumbs';

const routes: Routes = [
  {
    path: RoutingPaths.TELETRANSMISSION_PATH,
    redirectTo: RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_TDT_PATH,
    pathMatch: 'full'
  }, {
    path: RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_TDT_PATH,
    component: ActsListComponent,
    data: {
      pageParameters: {
        title: ActsRoutingMessages.PAGE_SIGNED_PROJECTS_TO_BE_SENT_TITLE,
        breadcrumb: ActsBreadcrumbs.PathBreadcrumb.get(RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_TDT_PATH),
        fluid: true
      } as PageParameters,
      projectState: ProjectDisplayState.TO_BE_TRANSMITTED as ProjectDisplayState
    }
  }, {
    path: RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_PREFECTURE_PATH,
    component: ActsListComponent,
    data: {
      pageParameters: {
        title: ActsRoutingMessages.PAGE_ACTS_READY_FOR_TELETRANSMISSION_TITLE,
        breadcrumb: ActsBreadcrumbs.PathBreadcrumb.get(RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_PREFECTURE_PATH),
        fluid: true
      } as PageParameters,
      projectState: ProjectDisplayState.TRANSMISSION_READY as ProjectDisplayState
    }
  }, {
    path: RoutingPaths.TDT_AUTHENTICATION_RESPONSE_PATH,
    component: TdtAuthenticationCallbackComponent,
    data: {
      pageParameters: {
        title: ActsRoutingMessages.PAGE_TDT_AUTHENTICATION_RESPONSE_TITLE,
        breadcrumb: ActsBreadcrumbs.PathBreadcrumb.get(RoutingPaths.TDT_AUTHENTICATION_RESPONSE_PATH),
        fluid: true
      } as PageParameters
    }
  }, {
    path: RoutingPaths.ACTS_PATH_LIST_PATH,
    component: ActsListComponent,
    data: {
      pageParameters: {
        title: ActsRoutingMessages.PAGE_ACTS_LIST_TITLE,
        breadcrumb: ActsBreadcrumbs.PathBreadcrumb.get(RoutingPaths.ACTS_PATH_LIST_PATH),
        fluid: true
      } as PageParameters,
      projectState: ProjectDisplayState.ACT as ProjectDisplayState
    }
  }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class ActsRoutingModule {
}
