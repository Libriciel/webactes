import { Component, OnInit } from '@angular/core';
import { IManageActsService } from '../../services/imanage-acts-service';
import { ActsMessages } from '../../i18n/acts-messages';
import { CommonStylesConstants } from '../../../wa-common/common-styles-constants';
import { AbstractProjectListComponent } from '../../../projects/components/projects-list/abstract-project-list-component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManageProjectService } from '../../../ls-common/services/manage-project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionActStateMapping } from './actions/action-act-state-mapping';
import { ProjectAction } from 'src/app/projects/components/projects-list/actions/project-action.enum';
import { ProjectActionItemFactory } from 'src/app/projects/components/projects-list/actions/project-action-item-factory';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { ActsBreadcrumbs } from '../../routing/acts-breadcrumbs';
import { ProjectDisplayState } from '../../../model/project/project-display-state.enum';

@Component({
  selector: 'wa-acts-list',
  templateUrl: './acts-list.component.html',
  styleUrls: ['./acts-list.component.scss']
})
export class ActsListComponent extends AbstractProjectListComponent implements OnInit {

  ProjectAction = ProjectAction;
  routingPaths = RoutingPaths;
  ActsBreadcrumbs = ActsBreadcrumbs;
  isShowingUpdateClassification;
  public stylesConstants = CommonStylesConstants;
  protected messages = ActsMessages;

  constructor(
    protected projectService: ManageProjectService,
    protected route: ActivatedRoute,
    public actsService: IManageActsService,
    protected modalService: NgbModal,
    protected actionActStateMap: ActionActStateMapping,
    protected router: Router,
    public projectActionItemFactory: ProjectActionItemFactory,
  ) {
    super(projectService, route);
  }

  ngOnInit() {
    super.ngOnInit();
    this.route.data.subscribe(data => {
      if (data.projectState) {
        this.singleActions = this.actionActStateMap.getSingleActions(data.projectState);
        this.commonActions = this.actionActStateMap.getCommonActions(data.projectState);
      }
      this.isShowingUpdateClassification = data.projectState &&
        [ProjectDisplayState.TRANSMISSION_READY, ProjectDisplayState.TO_BE_TRANSMITTED].indexOf(data.projectState) >= 0;
    });
  }

}
