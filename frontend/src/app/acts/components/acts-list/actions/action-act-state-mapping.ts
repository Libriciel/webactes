import { ActActionItemFactory } from './act-action-item-factory';
import { ProjectAction } from 'src/app/projects/components/projects-list/actions/project-action.enum';
import { ActAction } from './act-action.enum';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionItemsSet } from 'src/app/ls-common/model/action-items-set';
import { Project } from 'src/app/model/project/project';
import { ProjectDisplayState } from 'src/app/model/project/project-display-state.enum';
import { ProjectsBreadcrumb } from '../../../../projects/routing/projects-breadcrumb';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { ProjectActionItemFactory } from '../../../../projects/components/projects-list/actions/project-action-item-factory';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActionActStateMapping {

  constructor(
    protected modalService: NgbModal,
    protected actActionItemFactory: ActActionItemFactory,
    protected projectActionItemFactory: ProjectActionItemFactory,
  ) {
  }

  getCommonActions(projectState: ProjectDisplayState): ActionItemsSet<Project>[] {
    switch (projectState) {
      case ProjectDisplayState.TO_BE_TRANSMITTED:
      case ProjectDisplayState.TRANSMISSION_READY:
      case ProjectDisplayState.ACT:
        return [];
    }
  }

  getSingleActions(projectState: ProjectDisplayState): ActionItemsSet<Project> {
    switch (projectState) {
      case ProjectDisplayState.TO_BE_TRANSMITTED:
        return {
          actionItems: [
            this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT,
              ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_TDT_PATH)),
            this.actActionItemFactory.getActionItem(ActAction.CHECK_AND_SEND_TO_TDT)]
        };

      case ProjectDisplayState.TRANSMISSION_READY:
        return {
          actionItems: [
            this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT,
              ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_PREFECTURE_PATH)),
            this.actActionItemFactory.getActionItem(ActAction.ORDER_TELETRANSMISSION)]
        };

      case ProjectDisplayState.ACT:
        return {
          actionItems: [
            this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT,
              ProjectsBreadcrumb.PathBreadcrumb.get(RoutingPaths.ACTS_PATH_LIST_PATH))]
        };
    }
  }
}
