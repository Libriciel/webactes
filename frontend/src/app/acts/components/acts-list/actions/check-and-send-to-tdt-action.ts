import { from, Observable, of } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { catchError, map } from 'rxjs/operators';
import { ActionResult, IActuator } from '../../../../ls-common';
import { ActsMessages } from '../../../i18n/acts-messages';
import { ActSendToTdtModalComponent } from '../../act-send-to-tdt-modal/act-send-to-tdt-modal.component';
import { Project } from '../../../../model/project/project';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CheckAndSendToTdtAction implements IActuator<Project> {

  messages = ActsMessages;

  constructor(protected modalService: NgbModal) {
  }

  action(projects: Project[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(ActSendToTdtModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });
    if (!projects || projects.length === 0) {
      return of({});
    }

    let needReload = false;
    const project = projects[0];
    modalRef.componentInstance.project = project.unsafeClone();

    modalRef.componentInstance.projectChanged.subscribe(() => needReload = true);

    return from(modalRef.result).pipe(
      map(() => ({
        needReload: needReload,
        message: this.messages.ACT_SENT_TO_TDT_SUCCESS_MESSAGE
      } as ActionResult)),
      catchError((error) =>
        error && error.isError
          ? of({
            error: true,
            needReload: needReload,
            message: this.messages.ACT_SENT_TO_TDT_FAILURE_MESSAGE
          } as ActionResult)
          : of({
            error: false,
            needReload: needReload
          } as ActionResult))
    );
  }
}
