import { ActAction } from './act-action.enum';
import { ActionItem } from '../../../../ls-common';
import { Project } from '../../../../model/project/project';
import { ActsMessages } from '../../../i18n/acts-messages';
import { CheckAndSendToTdtAction } from './check-and-send-to-tdt-action';
import { OrderTeletransmissionAction } from './order-teletransmission';
import { SendToTdtAction } from './send-to-tdt-action';
import { CheckAndSendToTdtActionValidator } from './check-and-send-to-tdt-action-validator';
import { SendToTdtActionValidator } from './send-to-tdt-action-validator';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActActionItemFactory {

  constructor(
    protected checkAndSendToTdtAction: CheckAndSendToTdtAction,
    protected sendToTdtAction: SendToTdtAction,
    protected sendToTdtActionValidator: SendToTdtActionValidator,
    protected orderTeletransmissionAction: OrderTeletransmissionAction,
  ) {
  }

  getActionItem(action: ActAction): ActionItem<Project> {

    switch (action) {
      case ActAction.CHECK_AND_SEND_TO_TDT:
        return new ActionItem({
          name: ActsMessages.BUTTON_SHOW_TDT_INFO_TITLE,
          icon: ActsMessages.BUTTON_SEND_TO_CONTROL_ICON,
          actuator: this.checkAndSendToTdtAction,
          actionValidator: new CheckAndSendToTdtActionValidator(),
        });

      case ActAction.SEND_TO_TDT:
        return new ActionItem({
          name: ActsMessages.BUTTON_SEND_TO_CONTROL_TITLE,
          icon: ActsMessages.BUTTON_SEND_TO_CONTROL_ICON,
          actionValidator: this.sendToTdtActionValidator,
          actuator: this.sendToTdtAction,
          fullButton: true
        });

      case ActAction.ORDER_TELETRANSMISSION:
        return new ActionItem({
          name: ActsMessages.BUTTON_ORDER_TELETRANSMISSION_TITLE,
          icon: ActsMessages.BUTTON_ORDER_TELETRANSMISSION_ICON,
          actuator: this.orderTeletransmissionAction,
        });
    }
  }
}
