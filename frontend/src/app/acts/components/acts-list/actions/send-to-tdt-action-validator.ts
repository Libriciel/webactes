import { Project } from 'src/app/model/project/project';
import { StateActCode } from 'src/app/model/state-act-code.enum';
import { IManageActsService } from 'src/app/acts/services/imanage-acts-service';
import { Injectable } from '@angular/core';
import { IValidator } from '../../../../ls-common/components/tables/validator/i-validator';

@Injectable({
  providedIn: 'root'
})
export class SendToTdtActionValidator implements IValidator<Project> {
  constructor(protected theActsService: IManageActsService) {
  }

  isActionValid(projects: Project[]): boolean {
    return projects.every(act =>
      act.stateact.code !== StateActCode.BEING_SENT_TO_TDT
      && this.theActsService.isTdtOk(act));
  }
}
