import { Project } from 'src/app/model/project/project';
import { StateActCode } from 'src/app/model/state-act-code.enum';
import { IValidator } from '../../../../ls-common/components/tables/validator/i-validator';

export class CheckAndSendToTdtActionValidator implements IValidator<Project> {

  constructor() {
  }

  isActionValid(projects: Project[]): boolean {
    if (!projects || projects.length !== 1) {
      return false;
    }

    return projects[0].stateact.code !== StateActCode.BEING_SENT_TO_TDT;
  }

}
