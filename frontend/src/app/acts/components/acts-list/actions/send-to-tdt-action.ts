import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ActionResult } from '../../../../ls-common/components/tables/actuator/action-result';
import { ActsMessages } from '../../../i18n/acts-messages';
import { Project } from '../../../../model/project/project';
import { IManageActsService } from 'src/app/acts/services/imanage-acts-service';
import { Injectable } from '@angular/core';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';

@Injectable({
  providedIn: 'root'
})
export class SendToTdtAction implements IActuator<Project> {

  messages = ActsMessages;

  constructor(protected actsService: IManageActsService) {
  }

  action(projects: Project[]): Observable<ActionResult> {

    const project = projects[0];

    return this.actsService.sendActToTdt(project).pipe(
      map(() => ({
        needReload: true,
        message: this.messages.ACT_SENT_TO_TDT_SUCCESS_MESSAGE
      } as ActionResult)),
      catchError(() => of({
        error: true,
        needReload: false,
        message: this.messages.ACT_SENT_TO_TDT_FAILURE_MESSAGE
      } as ActionResult))
    );
  }
}
