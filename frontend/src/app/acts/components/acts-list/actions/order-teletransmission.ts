import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ActionResult } from '../../../../ls-common/components/tables/actuator/action-result';
import { ActsMessages } from '../../../i18n/acts-messages';
import { Project } from '../../../../model/project/project';
import { IManageActsService } from 'src/app/acts/services/imanage-acts-service';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { Injectable } from '@angular/core';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';

@Injectable({
  providedIn: 'root'
})
export class OrderTeletransmissionAction implements IActuator<Project> {

  messages = ActsMessages;

  constructor(protected  actsService: IManageActsService) {
  }

  action(projects: Project[]): Observable<ActionResult> {

    const project = projects[0];
    const callbackUrl = this.actsService.buildCallbackUrlForPath(project.id, RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_PREFECTURE_PATH);
    return this.actsService.orderTeletransmission(project, callbackUrl).pipe(
      map(() => ({
        error: false,
        needReload: true,
        message: this.messages.ACT_TELETRANSMISSION_ORDERED_MESSAGE
      })),
      catchError(() => of({
        error: true,
        needReload: false,
        message: this.messages.CANT_ORDER_TELETRANSMISSION_MESSAGE
      }))
    );
  }
}
