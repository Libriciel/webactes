import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActsMessages } from '../../i18n/acts-messages';
import { Project } from '../../../model/project/project';
import { IManageProjectsService } from 'src/app/projects/services/i-manage-projects.service';
import { Observable, Subscription } from 'rxjs';
import { FormControl, UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { ActTeletransmissionInformation } from '../../../model/act-teletransmission-information';
import { IManageActsService } from '../../services/imanage-acts-service';
import { WADocument } from 'src/app/projects/model/wa-document';
import { FileUtils } from 'src/app/utility/Files/file-utils';
import { Config } from 'src/app/Config';
import { JoinDocumentType } from 'src/app/model/join-document-type';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { tap } from 'rxjs/operators';
import { Style } from '../../../wa-common';
import { Weight } from '../../../wa-common/weight.enum';
import { CommonIcons, CommonMessages } from '../../../ls-common';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { Router } from '@angular/router';

@Component({
  selector: 'wa-act-send-tdt-modal',
  templateUrl: './act-send-to-tdt-modal.component.html',
  styleUrls: ['./act-send-to-tdt-modal.component.scss']
})
export class ActSendToTdtModalComponent implements OnInit, OnDestroy {

  commonMessages = CommonMessages;
  messages = ActsMessages;
  waCommonMessages = WACommonMessages;
  commonIcons = CommonIcons;
  style = Style;
  weight = Weight;
  @Output()
  projectChanged = new EventEmitter();
  isSubmitting = false;
  isSaving = false;
  isSaveDisabled = false;
  isSubmitDisabled = false;

  annexeDocumentTypes: JoinDocumentType[] = [];
  mainDocumentTypes: JoinDocumentType[] = [];
  _information: ActTeletransmissionInformation = {
    classification: null,
    codeAct: null,
    mainDocumentType: null,
    isMultichannel: false,
    annexes: []
  };
  formGroup = new UntypedFormGroup({
    isMultichannel: new FormControl(this._information.isMultichannel, Validators.required),
    annexTypes: new UntypedFormArray([
      new UntypedFormControl({
        type: ['', Validators.required]
      })
    ])
  });
  @Input()
  private project: Project = null;
  private annexesTransferableAcceptedTypes: any[] = Config.annexesTransferableAcceptedTypes;
  private statusChangesSubscription: Subscription;

  constructor(
    public activeModal: NgbActiveModal,
    public router: Router,
    private projectService: IManageProjectsService,
    private actsService: IManageActsService,
    private notificationService: NotificationService,
  ) {

  }

  ngOnInit() {
    if (this.project) {
      this._information.classification = this.project.classification;
      if (JSON.stringify(this._information.classification) === JSON.stringify({})) {
        this._information.classification = null;
      }
      this._information.codeAct = this.project.codeAct;
      this._information.isMultichannel = this.project.isMultichannel;
      this._information.annexes = this.project.annexes || [];

      if (this.project.mainDocument && this.project.mainDocument.documentType) {
        this._information.mainDocumentType = this.project.mainDocument.documentType;
      }
    }

    this.formGroup.patchValue({isMultichannel: this._information.isMultichannel});

    this.projectService.getJoinDocumentTypes(this.project.typeAct).subscribe(
      documentTypes => {
        this.mainDocumentTypes = documentTypes.mainDocumentTypes;
        this.annexeDocumentTypes = documentTypes.annexeDocumentTypes;
      }
    );

    this.statusChangesSubscription = this.formGroup.valueChanges.subscribe(() => this.OnChange());
    this.OnChange();
  }

  ngOnDestroy() {
    this.statusChangesSubscription.unsubscribe();
  }

  close() {
    this.activeModal.dismiss({isError: false});
  }

  OnChange() {
    this._information.isMultichannel = this.formGroup.get('isMultichannel').value;
    this.project.updateTransmissionInfo(this._information);
    this.isSubmitDisabled = this.isSaveDisabled || !this.actsService.isTdtOk(this.project);
  }

  hasTransferableType(annexe: File | WADocument): boolean {
    return FileUtils.fileHasAcceptableType(annexe, this.annexesTransferableAcceptedTypes);
  }

  saveProject() {
    this.isSaving = true;
    this.saveProjectChanges$().subscribe(
      updatedProject => {
        this.project = updatedProject;
        this.notificationService.showSuccess(this.messages.ACT_UPDATE_SUCCESS_MESSAGE);
        this.router.navigateByUrl(RoutingPaths.TELETRANSMISSION_PATH).then();
      },
      err => {
        console.error('error from updateProject : ', err);
        this.notificationService.showError(this.messages.ACT_UPDATE_ERROR_MESSAGE);
      })
      .add(() => {
        return this.isSaving = false;
      });
  }

  sendToTdt() {
    this.isSubmitting = true;
    this.saveProjectChanges$().subscribe(
      updatedProject => {
        if (this.actsService.isTdtOk(updatedProject)) {
          this.actsService.sendActToTdt(updatedProject)
            .subscribe(
              () => {
                this.activeModal.close();
                this.router.navigateByUrl(RoutingPaths.TELETRANSMISSION_PATH).then();
              },
              tdtError => {
                console.error('error from sendToTdt : ', tdtError);
                this.activeModal.dismiss({isError: true, message: tdtError});
              })
            .add(
              () => {
                return this.isSubmitting = false;
              });

        } else {
          console.error('Project is invalid for TDT transmission');
          this.isSubmitting = false;
        }
      },
      err => {
        console.error('error from updateProject : ', err);
        this.activeModal.dismiss({isError: true, message: err});
      }
    );

  }

  private saveProjectChanges$(): Observable<any> {
    return this.projectService.updateTeletransmissionInfo(this.project.id, this._information).pipe(
      tap(updatedProject => this.projectChanged.emit(updatedProject))
    );
  }

  get isMultichannel() {
    return this.formGroup.controls['isMultichannel'].value;
  }

  resetCodeType(annexe: WADocument) {
    annexe.codeType = null;
    this.OnChange();
  }
}
