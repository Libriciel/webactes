import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActsMessages } from '../../i18n/acts-messages';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { IManageActsService } from '../../services/imanage-acts-service';

@Component({
  selector: 'wa-tdt-authentication-callback',
  templateUrl: './tdt-authentication-callback.component.html',
  styleUrls: ['./tdt-authentication-callback.component.scss']
})
export class TdtAuthenticationCallbackComponent implements OnInit {

  messages = ActsMessages;
  endedInError = false;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected notificationService: NotificationService,
    protected actService: IManageActsService,
  ) {
  }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        const projectId = Number(params.projectId);
        this.endedInError = params.error === '1';

        if (this.endedInError) {
          setTimeout(() =>
            this.notificationService.showError(this.messages.ACT_TELETRANSMISSION_ORDERED_FAILURE_MESSAGE), 0);
        } else {
          if (projectId) {
            this.actService.teletransmissionWasOrderedCorrectly(projectId).subscribe(() => {
              if (params.targetRoute) {
                this.router.navigateByUrl(params.targetRoute).then();
              }
            });
          }
          setTimeout(() =>
            this.notificationService.showSuccess(this.messages.ACT_TELETRANSMISSION_ORDERED_SUCCESS_MESSAGE), 0);
        }
      });
  }
}
