import { Project } from '../../model/project/project';
import { Observable } from 'rxjs';
import { TdtReturnType } from 'src/app/ls-common/services/http-services/tdt-return-type';

export abstract class IManageActsService {

  abstract isCodeActOK(project: Project): boolean;

  abstract isClassificationOK(project: Project): boolean;

  abstract isMainDocOk(project: Project): boolean;

  abstract isAnnexesOK(project: Project): boolean;

  abstract isTdtOk(project: Project): boolean;

  abstract sendActToTdt(project: Project): Observable<TdtReturnType>;

  abstract redirectToRGSAuthentication(authenticationUrl: string, callbackUrl: string);

  abstract buildCallbackUrlForPath(projectId: number, targetPath: string): string;

  abstract orderTeletransmission(project: Project, callbackUrl: string);

  abstract teletransmissionWasOrderedCorrectly(projectId: number): Observable<any> ;
}
