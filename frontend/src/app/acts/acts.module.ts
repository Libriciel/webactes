import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActsListComponent } from './components/acts-list/acts-list.component';
import { WaCommonModule } from '../wa-common/wa-common.module';
import { ActsRoutingModule } from './routing/acts-routing/acts-routing.module';
import { RouterModule } from '@angular/router';
import { LibricielCommonModule } from '../ls-common/libriciel-common.module';
import { NgbDropdownModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ActSendToTdtModalComponent } from './components/act-send-to-tdt-modal/act-send-to-tdt-modal.component';
import { ClassificationModule } from '../classification/classification.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { TdtAuthenticationCallbackComponent } from './components/tdt-authentication-callback/tdt-authentication-callback.component';

@NgModule({
  declarations: [ActsListComponent, ActSendToTdtModalComponent, TdtAuthenticationCallbackComponent],
  imports: [
    CommonModule,
    WaCommonModule,
    ActsRoutingModule,
    RouterModule,
    LibricielCommonModule,
    NgbDropdownModule,
    ClassificationModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
  ],
  exports: [
    ActsListComponent
  ]
})
export class ActsModule {
}
