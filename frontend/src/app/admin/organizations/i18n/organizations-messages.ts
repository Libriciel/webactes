export class OrganizationsMessages {

  static IMPORT_ORGANIZATION_MODAL_TITLE = 'Importer des structures';
  static IMPORT_ORGANIZATION_MODAL_IMPORT_ERROR = `Impossible d'importer les structures sélectionnées`;
  static IMPORT_ORGANIZATION_MODAL_IMPORT_SUCCESS = `Importation réussie`;
  static IMPORT_ORGANIZATION_BUTTON_TITLE = 'Importer';
  static PAGE_ORGANIZATIONS_TITLE = 'Structures';

  static modalSelectedStructures(nbStructures) {
    return nbStructures + ' structures sélectionnées';
  }
}
