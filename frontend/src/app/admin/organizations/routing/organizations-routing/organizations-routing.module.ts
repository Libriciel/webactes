import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { OrganizationsListComponent } from '../../components/organizations-list/organizations-list.component';
import { AdminMessages } from '../../../i18n/admin-messages';
import { OrganizationsMessages } from '../../i18n/organizations-messages';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_ORGANIZATIONS_PATH,
    component: OrganizationsListComponent,
    data: {
      pageParameters: {
        title: OrganizationsMessages.PAGE_ORGANIZATIONS_TITLE,
        breadcrumb: [
          {
            name: AdminMessages.PAGE_ADMIN_TITLE,
            location: RoutingPaths.ADMIN_PATH
          },
          {
            name: OrganizationsMessages.PAGE_ORGANIZATIONS_TITLE,
            location: `/${RoutingPaths.ADMIN_ORGANIZATIONS_PATH}`
          }
        ]
      }
    },
    children: [
      {path: '', redirectTo: RoutingPaths.ADMIN_ORGANIZATIONS_PATH, pathMatch: 'full'},
      {path: '**', redirectTo: RoutingPaths.ADMIN_ORGANIZATIONS_PATH, pathMatch: 'full'}
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class OrganizationsRoutingModule {

}
