import { Component, OnInit } from '@angular/core';
import { Organization } from '../../../../model/organization';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PastellStructure } from '../../../../model/pastell-structure';
import { IOrganizationsAndStructuresService } from '../../serviceInterfaces/iorganizations-and-structures-service';
import { map } from 'rxjs/operators';
import { ImportOrganizationsComponent } from '../import-organizations/import-organizations.component';
import { NodeItem } from '../../model/nodeItem';
import { OrganizationsMessages } from '../../i18n/organizations-messages';
import { Router } from '@angular/router';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { CommonMessages } from '../../../../ls-common/i18n/common-messages';
import { StructureInfo } from '../../../../model/structure-info';
import { Pagination } from '../../../../model/pagination';
import { isArray } from 'rxjs/internal-compatibility';
import { FilterType } from '../../../../model/util/filter-type';

@Component({
  selector: 'wa-organizations-list',
  templateUrl: './organizations-list.component.html',
  styleUrls: ['./organizations-list.component.scss']
})
export class OrganizationsListComponent implements OnInit {

  pagination: Pagination = {page: 1, count: 1, perPage: 1};
  structures: { structures: StructureInfo[] };
  organizationsAndStructuresList: StructureInfo[] = [];
  organizationsList: Organization[];
  organization: Organization[];
  loading = false;
  messages = OrganizationsMessages;
  commonMessages = CommonMessages;
  filterString: string;

  constructor(protected modalService: NgbModal,
              protected organizationsAndStructuresService: IOrganizationsAndStructuresService,
              protected router: Router) {
  }

  private static alphabeticalSort(a: PastellStructure, b: PastellStructure): 0 | 1 | -1 {
    return a.denomination < b.denomination
      ? -1
      : a.denomination > b.denomination
        ? 1
        : 0;
  }

  ngOnInit() {
    this.reloadStructures();
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

  structuresImport() {
    this.loading = true;
    this.organizationsAndStructuresService.getOrganizationsAndStructuresToBeImported().pipe(
      map(structures => this.deserializeStructures(structures)),
      map(structures => {
        this.loading = false;
        return structures;
      })
    ).subscribe(data => {
      const modalRef = this.modalService.open(ImportOrganizationsComponent, {
        size: 'lg',
        centered: true,
        backdrop: 'static',
        keyboard: false
      });
      modalRef.componentInstance.treeOrganizations = data;
      modalRef.result.then(() => this.retrieveOrganizationsAndStructures());
    });
  }

  reloadStructures() {
    this.gotoPage(1);
  }

  gotoPage(numPage: number) {
    const filters = this.filterString ? {searchedText: this.filterString} : null;

    this.retrieveOrganizationsAndStructures(numPage, filters);
  }

  private retrieveOrganizationsAndStructures(numPage?: number, filters?: FilterType) {
    this.organizationsAndStructuresList = [];
    this.loading = true;
    this.organizationsAndStructuresService.getStructures(numPage, filters)
      .subscribe(result => {
        this.loading = false;
        this.pagination = result.pagination;
        this.organizationsAndStructuresList = result.structures;
        this.createOrganizationWithStructuresList(result.structures);
      });
  }

  createOrganizationWithStructuresList(structureOrOrga?: any) {
    if (isArray(structureOrOrga)) {
      structureOrOrga.map(organization => {
        if (organization.structures) {
          organization.structures.map(structureOrOrganization => {
            this.createOrganizationWithStructuresList(structureOrOrganization);
          });
        }
      });
    } else {
      this.organizationsAndStructuresList.push(structureOrOrga);
    }
  }

  private deserializeStructures(structures: PastellStructure[]): NodeItem<PastellStructure>[] {
    let queue: PastellStructure[] = Object.assign([], structures.sort(OrganizationsListComponent.alphabeticalSort));
    let queueSize = queue.length;
    let nextQueue: PastellStructure[] = [];
    const preparedStructured: NodeItem<PastellStructure>[] = [];
    const structureList: NodeItem<PastellStructure>[] = [];

    function insertOrRequeueStructure(pastellStructure: PastellStructure): void {
      // if (pastellStructure.isRoot()) {
      const newItem = new NodeItem({
        id: pastellStructure.id_e,
        name: pastellStructure.denomination + ' (' + pastellStructure.type + ' - id: ' + pastellStructure.id_e + ')',
        value: pastellStructure
      });
      preparedStructured.push(newItem);
      structureList.push(newItem);
      // } else {
      //   const item = structureList.find(structure => structure.value.id_e === pastellStructure.entite_mere);
      //   if (item) {
      //     const newItem = new NodeItem({
      //       id: pastellStructure.id_e,
      //       name: pastellStructure.denomination + ' (' + pastellStructure.type + ' - id: ' + pastellStructure.id_e + ')',
      //       value: pastellStructure
      //     });
      //     item.children.push(newItem);
      //     structureList.push(newItem);
      //   } else {
      //     nextQueue.push(pastellStructure);
      //   }
      // }
    }

    function passToNextQueue(): boolean {
      // We tried to process elements of the queue and none could be inserted in the tree.
      // Probably a structure with an invalid entite_mere.
      if (queueSize === nextQueue.length) {
        return false;
      }
      queue = nextQueue;
      queueSize = queue.length;
      nextQueue = [];
      return true;
    }

    while (queue.length > 0) {
      const pastellStructure: PastellStructure = queue.shift();
      insertOrRequeueStructure(pastellStructure);
      if (queue.length === 0 && !passToNextQueue()) {
        throw new Error('unprocessable elements ' + queueSize);
      }
    }
    return preparedStructured;
  }

  applyFilter(event) {
    this.filterString = event.trim();
    this.reloadStructures();
  }
}
