import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PastellStructure } from '../../../../model/pastell-structure';
import { IOrganizationsAndStructuresService } from '../../serviceInterfaces/iorganizations-and-structures-service';
import { NodeItem } from '../../model/nodeItem';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { OrganizationsMessages } from '../../i18n/organizations-messages';
import { Weight } from '../../../../wa-common/weight.enum';
import { ITreeOptions, TreeComponent } from '@circlon/angular-tree-component';
import { ITreeNode } from '@circlon/angular-tree-component/lib/defs/api';

@Component({
  selector: 'wa-import-organizations',
  templateUrl: './import-organizations.component.html',
  styleUrls: ['./import-organizations.component.scss']
})
export class ImportOrganizationsComponent implements OnInit, AfterViewInit {

  @ViewChild(TreeComponent, {static: true}) tree: TreeComponent;
  options: ITreeOptions = {
    useCheckbox: true
  };
  isSubmitting = false;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  messages = OrganizationsMessages;
  weight = Weight;
  protected pastellStructures = new Map<number, PastellStructure>();

  constructor(
    public activeModal: NgbActiveModal,
    protected organizationsAndStructuresService: IOrganizationsAndStructuresService,
    protected notificationService: NotificationService,
  ) {
  }

  @Input()
  private _treeOrganizations: NodeItem<PastellStructure>[];

  get treeOrganizations(): NodeItem<PastellStructure>[] {
    return this._treeOrganizations;
  }

  set treeOrganizations(treeOrganizations: NodeItem<PastellStructure>[]) {
    const self = this;

    function buildMap(treeviewItem: NodeItem<PastellStructure>): void {
      self.pastellStructures.set(treeviewItem.value.id_e, treeviewItem.value);
      if (treeviewItem.children) {
        for (const child of treeviewItem.children) {
          buildMap(child);
        }
      }
    }

    treeOrganizations
      .forEach(treeviewItem => buildMap(treeviewItem));
    this._treeOrganizations = treeOrganizations;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.tree.treeModel.expandAll();
  }

  close(): void {
    this.activeModal.dismiss({isError: false});
  }

  selectedOrganizations(): any[] {
    const self = this;
    const resultMap: Map<string, ITreeNode> = new Map();
    Object.keys(this.tree.treeModel.selectedLeafNodeIds).forEach(
      (key) => {
        if (self.tree.treeModel.selectedLeafNodeIds[key]) {
          let node: ITreeNode = self.tree.treeModel.getNodeById(key);
          while (node) {
            if (node.level === 1) {
              resultMap.set(node.data.id, node.data.value);
            }
            node = node.parent;
          }
        }
      });
    return Array.from(resultMap, ([, value]) => value);
  }

  selectedStructures(): any[] {
    const self = this;
    const resultMap: Map<string, ITreeNode> = new Map();
    Object.keys(this.tree.treeModel.selectedLeafNodeIds).forEach(
      (key) => {
        if (self.tree.treeModel.selectedLeafNodeIds[key]) {
          let node: ITreeNode = self.tree.treeModel.getNodeById(key);
          while (node && !node.isRoot) {
            resultMap.set(node.data.id, node.data.value);
            node = node.parent;
          }
        }
      });
    return Array.from(resultMap, ([, value]) => value);
  }

  createOrganizationsAndStructures() {
    this.organizationsAndStructuresService.createOrganizationsAndStructures(
      this.createdStructuredStructuresMapIds())
      .subscribe(
        () => {
          this.activeModal.close();
          this.notificationService.showSuccess(this.messages.IMPORT_ORGANIZATION_MODAL_IMPORT_SUCCESS);
        },
        err => {
          console.error(err);
          this.notificationService.showError(this.messages.IMPORT_ORGANIZATION_MODAL_IMPORT_ERROR);
        });
  }

  selectedEntities(): any[] {
    return this.selectedOrganizations().concat(this.selectedStructures());
  }

  private createdStructuredStructuresMapIds(): number[][] {
    const checkedStructuresMap: number[][] = [];
    const self = this;
    Object.keys(this.tree.treeModel.selectedLeafNodeIds).forEach(
      (key) => {
        // get All checked leaves
        if (self.tree.treeModel.selectedLeafNodeIds[key]) {
          let node: ITreeNode = self.tree.treeModel.getNodeById(key);
          // If root => only with first level => empty array
          if (node.level === 1) {
            checkedStructuresMap[node.data.id] = [];
          } else {
            let rootParent = node;
            // Get root parent
            while (rootParent.level !== 1) {
              rootParent = rootParent.parent;
            }
            // Create if needed empty array
            if (!checkedStructuresMap[rootParent.data.id]) {
              checkedStructuresMap[rootParent.data.id] = [];
            }
            while (node) {
              // Add add non checked non root node to array
              if (node.level > 1 && !checkedStructuresMap[rootParent.data.id].includes(node.data.id)) {
                checkedStructuresMap[rootParent.data.id].push(node.data.id);
              }
              node = node.parent;
            }
          }
        }
      });
    return checkedStructuresMap;
  }
}
