import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationsListComponent } from './components/organizations-list/organizations-list.component';
import { ImportOrganizationsComponent } from './components/import-organizations/import-organizations.component';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { OrganizationsRoutingModule } from './routing/organizations-routing/organizations-routing.module';
import { TreeModule } from '@circlon/angular-tree-component';

@NgModule({
  declarations: [OrganizationsListComponent, ImportOrganizationsComponent],
  imports: [
    CommonModule,
    LibricielCommonModule,
    OrganizationsRoutingModule,
    TreeModule
  ],
  exports: [OrganizationsListComponent]
})
export class OrganizationsModule {
}
