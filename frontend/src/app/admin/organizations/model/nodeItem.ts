export class NodeItem<T> {
  id: number;
  name: string;
  value: T;
  children: NodeItem<T>[];

  constructor(obj: { id: number, name: string, value: T }) {
    this.id = obj.id;
    this.name = obj.name;
    this.value = obj.value;
    this.children = [];
  }
}
