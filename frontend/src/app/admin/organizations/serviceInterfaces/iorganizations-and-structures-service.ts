import { PastellStructure } from '../../../model/pastell-structure';
import { Observable } from 'rxjs';
import { Organization } from '../../../model/organization';
import { Pagination } from '../../../model/pagination';
import { FilterType } from '../../../model/util/filter-type';
import { StructureInfo } from '../../../model/structure-info';


export abstract class IOrganizationsAndStructuresService {

  abstract getOrganizationsAndStructuresToBeImported(): Observable<PastellStructure[]>;

  abstract createOrganizationsAndStructures(array: number[][]): Observable<any>;

  abstract getOrganizationsAndStructures(numPage?: number, filters?: FilterType): Observable<{ organizations: Organization[], pagination: Pagination }>;

  abstract getStructures(numPage?: number, filters?: FilterType): Observable<{ structures: StructureInfo[], pagination: Pagination }>;

}

