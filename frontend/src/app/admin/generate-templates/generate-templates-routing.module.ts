import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageParameters } from 'src/app/wa-common/components/page/model/page-parameters';
import { GenerateTemplatesComponent } from './generate-templates.component';

const routes: Routes = [
  {
    path: 'administration/generate-templates',
    component: GenerateTemplatesComponent,
    data: {
      pageParameters: {
        title: `Modèles de document`,
        breadcrumb: [
          {
            name: `Administration`,
            location: `administration`
          },
          {
            name: `Modèles de document`, location: 'administration/generate-templates'
          }
        ],
        fluid: true
      } as PageParameters
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenerateTemplatesRoutingModule {
}
