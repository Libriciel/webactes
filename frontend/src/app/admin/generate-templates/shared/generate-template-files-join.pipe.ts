import { Pipe, PipeTransform } from '@angular/core';
import { GenerateTemplateFile } from '../../../core';

/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'joinGenerateTemplateFiles'})
export class joinGenerateTemplateFilesPipe implements PipeTransform {
  // transform(value: number, exponent?: number): number {
  //   return Math.pow(value, isNaN(exponent) ? 1 : exponent);
  // }
  transform(generate_template_files: Array<GenerateTemplateFile>, separator = ', '): string {
    return generate_template_files.map(generate_template_file => generate_template_file.name).join(separator);
  }
}
