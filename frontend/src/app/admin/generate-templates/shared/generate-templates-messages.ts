import { Validators } from '@angular/forms';

export class GenerateTemplatesMessages {
  static HEADER_NAME = `Nom du modèle`;
  static HEADER_TYPE = `Type`;
  static HEADER_FILE = `Fichier`;

  static MODAL_TITLE_CREATE = `Création d'un modèle`;
  static MODAL_TITLE_UPDATE = `Modification d'un modèle`;
  static FORM_FIELD_GENERATE_TEMPLATE_TYPES_LABEL = `Type de génération`;
  static FORM_FIELD_GENERATE_TEMPLATE_TYPES_LABEL_PLACEHOLDER = `Type de génération du document`;
  static FORM_FIELD_NAME_LABEL = `Nom`;
  static FORM_FIELD_NAME_LABEL_PLACEHOLDER = `Nom du modèle`;
  static FORM_FIELD_GENERATE_TEMPLATE_FILE_LABEL = `Modèle`;
  static FORM_FIELD_GENERATE_TEMPLATE_FILE_BUTTON_TEXT = `Ajouter un fichier de modèle`;

  static BUTTON_ADD = `Créer un modèle de document`;
  static BUTTON_SAVE = `Enregistrer`;

  static DELETED_CONFIRMATION_TOASTR_TXT = `Modèle supprimé avec succès`;
  static DELETE_FAILED_TOASTR_TXT = `La suppression du modèle a échoué`;
  static CONFIRM_DELETE_TITLE = `Suppression du modèle`;
  static CONFIRM_DELETE_BUTTON = `Supprimer`;

  static CREATED_SUCCESS_TOASTR_TXT = `Modèle créé avec succès`;
  static CREATED_ERROR_TOASTR_TXT = `Erreur lors de la création du modèle.`;
  static CREATED_FILE_ERROR_TOASTR_TXT = `Erreur lors du téléchargement du fichier de modèle.`;

  static sameName(name: string) {
    return `Un modèle avec le nom ${name} est déjà présent.<br>
        Veuillez renseigner un autre nom pour le modèle.`;
  }

  static updateGenerateTemplateActionDone(nameGenerateTemplate: string): string {
    return `Les modifications du modèle "${nameGenerateTemplate}" ont été enregistrées avec succès.`;
  }

  static addGenerateTemplateActionDone(nameGenerateTemplate: string): string {
    return `Le modèle "${nameGenerateTemplate}" a été créé avec succès.`;
  }

  static deleteGenerateTemplateConfirm(nameGenerateTemplate: string): string {
    return `Voulez-vous vraiment supprimer le modèle "${nameGenerateTemplate}"?`;
  }

  static formFieldGenerateTemplateFileHelp(size: number): string {
    return `La taille recommandée pour afficher correctement une génération est de ${size} Mo.`;
  }

}
