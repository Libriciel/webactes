import { Component, EventEmitter, Input, Output } from '@angular/core';

import { CommonIcons, CommonMessages, } from '../../../../ls-common';
import { Style } from '../../../../wa-common';

import { GenerateTemplatesMessages } from '../../shared';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { GenerateTemplate, GenerateTemplateFilesService, GenerateTemplatesService, GenerateTemplateType, GenerateTemplateTypesService } from '../../../../core';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FileType } from '../../../../utility/Files/file-types.enum';

@Component({
  selector: 'generate-template-modal-create',
  templateUrl: 'generate-template-modal-create.component.html'
})
export class GenerateTemplateModalCreateComponent {

  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  messages = GenerateTemplatesMessages;

  generateTemplateForm: UntypedFormGroup;
  generateTemplate: GenerateTemplate = {} as GenerateTemplate;
  optionGenerateTemplateTypes: GenerateTemplateType[];
  style = Style;
  fileType = FileType;

  isSubmitting = false;
  isSubmitDisabled = true;

  @Input() imageFormats: FileType[];
  @Output() isCreated = new EventEmitter<boolean>();

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private activeModal: NgbActiveModal,
    private generateTemplatesService: GenerateTemplatesService,
    private generateTemplateTypesService: GenerateTemplateTypesService,
    private generateTemplateFilesService: GenerateTemplateFilesService,
    protected notificationService: NotificationService
  ) {
    this.generateTemplateForm = this._formBuilder.group({
      generate_template_type: ['', Validators.required],
      name: ['', [Validators.required, Validators.maxLength(255)]],
      generate_template_file: ['', [Validators.required, this.requiredFileType('odt')]],
    });

    this.generateTemplateTypesService.getAll()
      .subscribe(data => {
        this.optionGenerateTemplateTypes = data.generateTemplateTypes;
      });
  }

  onSubmit() {
    this.isSubmitting = true;
    if (this.generateTemplateForm.dirty && this.generateTemplateForm.valid) {

      // update the model
      this.updateGenerateTemplate(this.generateTemplateForm.value);

      this.generateTemplatesService.save(this.generateTemplate)
        .subscribe(
          generateTemplate => {
            this.generateTemplateFilesService.sendFile(generateTemplate.id.toString(), this.generateTemplateForm.get('generate_template_file').value).subscribe(
              () => {
                this.notificationService.showSuccess(this.messages.addGenerateTemplateActionDone(generateTemplate.name));
                this.isSubmitting = true;
                this.activeModal.close();
                this.isCreated.emit(true);
              },
              () => {
                this.generateTemplatesService.destroy(generateTemplate.id);
                this.notificationService.showError(this.messages.CREATED_FILE_ERROR_TOASTR_TXT);
                this.isSubmitting = false;
              }
            );
          },
          () => {
            this.notificationService.showError(this.messages.CREATED_ERROR_TOASTR_TXT);
            this.isSubmitting = false;
          }
        );
    }

  }

  submitGenerateTemplate() {
    this.onSubmit();
  }

  requiredFileType(type: string) {
    return function (control: UntypedFormControl) {
      const file = control.value;
      if (file) {
        const extension = file.name.split('.')[1].toLowerCase();
        if (type.toLowerCase() !== extension.toLowerCase()) {
          return {
            requiredFileType: true
          };
        }

        return null;
      }

      return null;
    };
  }

  updateGenerateTemplate(values: Object) {
    Object.assign(this.generateTemplate, values);
  }
}
