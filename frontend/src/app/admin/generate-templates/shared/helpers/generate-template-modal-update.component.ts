import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { Style } from '../../../../wa-common';

import { GenerateTemplatesMessages } from '../../shared';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import {
  GenerateTemplate,
  GenerateTemplateFile,
  GenerateTemplateFilesService,
  GenerateTemplatesService,
  GenerateTemplateType,
  GenerateTemplateTypesService,
  OfficeEditorFile
} from '../../../../core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FileType } from '../../../../utility/Files/file-types.enum';
import { saveAs } from 'file-saver';


@Component({
  selector: 'generate-template-modal-update',
  templateUrl: 'generate-template-modal-update.component.html'
})
export class GenerateTemplateModalUpdateComponent implements OnInit {

  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  messages = GenerateTemplatesMessages;

  generateTemplateForm: UntypedFormGroup;
  optionGenerateTemplateTypes: GenerateTemplateType[];
  style = Style;
  fileType = FileType;

  isSubmitting = false;
  isSubmitDisabled = true;

  officeEditorFile: OfficeEditorFile;

  @Input() public generateTemplate: GenerateTemplate;
  @Output() isUpdated = new EventEmitter<boolean>();

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private activeModal: NgbActiveModal,
    private generateTemplatesService: GenerateTemplatesService,
    private generateTemplateTypesService: GenerateTemplateTypesService,
    private generateTemplateFilesService: GenerateTemplateFilesService,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit() {
    this.generateTemplateForm = this._formBuilder.group({
      id: this.generateTemplate.id,
      name: [this.generateTemplate.name, [Validators.required, Validators.maxLength(255)]],
      generate_template_type: [this.generateTemplate.generate_template_type, [Validators.required]],
      generate_template_file: [this.generateTemplate.generate_template_file, [Validators.required, this.requiredFileType('odt')]],
    });

    this.generateTemplateTypesService.getAll()
      .subscribe(data => {
        this.optionGenerateTemplateTypes = data.generateTemplateTypes;
      });

    this.updateOfficeEditorFile(this.generateTemplate.generate_template_file);

  }

  onSubmit() {
    this.isSubmitting = true;
    if (this.generateTemplateForm.valid) {

      let oldIdFile = this.generateTemplate.generate_template_file.id;
      // update the model
      this.updateGenerateTemplate(this.generateTemplateForm.value);

      this.generateTemplatesService.save(this.generateTemplate).subscribe({
        next: () => {
          if (this.generateTemplate.generate_template_file.id == undefined) {
            // Delete file
            this.generateTemplateFilesService.destroy(oldIdFile).subscribe(() => {
            });
            // Add file
            this.generateTemplateFilesService.sendFile(this.generateTemplate.id.toString(), this.generateTemplateForm.get('generate_template_file').value).subscribe(
              () => {
                this.notificationService.showSuccess(this.messages.updateGenerateTemplateActionDone(this.generateTemplate.name));
                this.activeModal.close({isError: false});
                this.isUpdated.emit(true);
              },
              () => {
                this.notificationService.showError(this.messages.CREATED_FILE_ERROR_TOASTR_TXT);
                this.isSubmitting = false;
              }
            );
          } else {
            this.notificationService.showSuccess(this.messages.updateGenerateTemplateActionDone(this.generateTemplate.name));
            this.activeModal.close({isError: false});
            this.isUpdated.emit(true);
          }
        },
        error: () => {
          this.notificationService.showError(this.messages.CREATED_ERROR_TOASTR_TXT);
          this.isSubmitting = false;
        },
      });
    }
  }

  submitGenerateTemplate() {
    this.onSubmit();
  }

  requiredFileType(type: string) {
    return function (control: UntypedFormControl) {

      const file = control.value;
      if (file) {
        const extension = file.name.split('.')[1].toLowerCase();
        if (type.toLowerCase() !== extension.toLowerCase()) {
          return {
            requiredFileType: true
          };
        }

        return null;
      }

      return null;
    };
  }

  updateGenerateTemplate(values: Object) {
    Object.assign(this.generateTemplate, values);
  }

  updateOfficeEditorFile(generateTemplateFile: GenerateTemplateFile) {
    this.officeEditorFile = {
      id: generateTemplateFile.id,
      name: generateTemplateFile.name,
      size: generateTemplateFile.size,
      mimetype: generateTemplateFile.mimetype,
      path: generateTemplateFile.path,
      baseUrl: generateTemplateFile.base_url,
      pathWopi: generateTemplateFile.path_wopi + generateTemplateFile.id,
    };
  }

  download(generateTemplateFile: GenerateTemplateFile): void {
    this.generateTemplateFilesService
      .geFile(generateTemplateFile.id)
      .subscribe(blob => {
        const blobPart = new Blob([blob], {type: generateTemplateFile.mimetype});
        saveAs(blobPart, generateTemplateFile.name);
      });
  }

}
