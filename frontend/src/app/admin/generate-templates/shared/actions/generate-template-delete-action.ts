import { Observable, Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Style } from '../../../../wa-common';
import { ActionResult, ConfirmPopupComponent, IActuator, NotificationService } from '../../../../ls-common';
import { GenerateTemplate, GenerateTemplatesService } from '../../../../core';
import { GenerateTemplatesMessages } from '../generate-templates-messages';

export class GenerateTemplateDeleteAction implements IActuator<GenerateTemplate> {

  messages = GenerateTemplatesMessages;

  constructor(
    protected GenerateTemplatesService: GenerateTemplatesService,
    protected modalService: NgbModal,
    protected notificationService: NotificationService
  ) {
  }

  action(GenerateTemplates: GenerateTemplate[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    const returnSubject = new Subject<ActionResult>();

    modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_TITLE;
    const GenerateTemplatesNames = GenerateTemplates.map(GenerateTemplate => GenerateTemplate.name).join(', ');
    modalRef.componentInstance.content = this.messages.deleteGenerateTemplateConfirm(GenerateTemplatesNames);

    modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_DELETE_BUTTON;
    modalRef.componentInstance.style = Style.DANGER;

    modalRef.result
      .then(
        () => {
          this.GenerateTemplatesService.destroy(GenerateTemplates[0].id).subscribe({
            complete: () => {
              returnSubject.next({
                error: false,
                needReload: true,
                message: this.messages.DELETED_CONFIRMATION_TOASTR_TXT
              });
            },
            error: () => {
              console.error('Error calling actor deletion');
              returnSubject.next({
                error: true,
                needReload: true,
                message: this.messages.DELETE_FAILED_TOASTR_TXT
              });
            }
          });
        },
        () => returnSubject.complete()
      )
      .catch(error => {
        console.error('Caught error in popup for circuit deletion : ', error);
      });

    return returnSubject.asObservable();

  }
}
