import { Observable, Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionResult, IActuator, NotificationService } from '../../../../ls-common';
import { GenerateTemplate, GenerateTemplatesService } from '../../../../core';
import { GenerateTemplatesMessages } from '../generate-templates-messages';
import { GenerateTemplateModalUpdateComponent } from '../helpers';

export class GenerateTemplateUpdateAction implements IActuator<GenerateTemplate> {

  messages = GenerateTemplatesMessages;

  constructor(
    protected GenerateTemplatesService: GenerateTemplatesService,
    protected modalService: NgbModal,
    protected notificationService: NotificationService
  ) {
  }

  action(GenerateTemplates: GenerateTemplate[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(GenerateTemplateModalUpdateComponent, {
      backdrop: 'static',
      centered: true,
      size: 'lg'
    });

    modalRef.componentInstance.generateTemplate = GenerateTemplates[0];

    const returnSubject = new Subject<ActionResult>();

    modalRef.componentInstance.isUpdated.subscribe(() => {
      returnSubject.next({
        error: false,
        needReload: true,
        message: null
      });
    });

    modalRef.result.then(
      () => returnSubject.complete(),
      () => {
        returnSubject.next();
        returnSubject.complete();
      }
    );

    return returnSubject.asObservable();

  }
}
