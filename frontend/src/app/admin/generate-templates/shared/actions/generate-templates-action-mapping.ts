import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { Injectable } from '@angular/core';

import { CommonIcons, CommonMessages, } from '../../../../ls-common';
import { ActionItem } from '../../../../ls-common/model/action-item';
import { GenerateTemplate, GenerateTemplatesService } from '../../../../core';

import { Style } from '../../../../wa-common/style.enum';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationService } from '../../../../ls-common/services/notification.service';

import { GenerateTemplatesMessages } from '../generate-templates-messages';
import { GenerateTemplateDeleteAction } from './generate-template-delete-action';
import { GenerateTemplateUpdateAction } from './generate-template-update-action';

@Injectable({
  providedIn: 'root'
})
export class GenerateTemplatesActionMapping {

  messages = GenerateTemplatesMessages;

  constructor(
    protected modalService: NgbModal,
    protected GenerateTemplatesService: GenerateTemplatesService,
    protected notificationService: NotificationService,
  ) {
  }

  getCommonActions(): ActionItemsSet<GenerateTemplate> {
    return null;
  }

  getOtherSingleActions(): ActionItemsSet<GenerateTemplate> {
    return null;
  }

  getSingleActions(): ActionItemsSet<GenerateTemplate> {
    return {
      actionItems: [
        new ActionItem({
          name: CommonMessages.MODIFY,
          icon: CommonIcons.EDIT_ICON,
          actuator: new GenerateTemplateUpdateAction(
            this.GenerateTemplatesService,
            this.modalService,
            this.notificationService
          ),
        }),
        new ActionItem({
          name: CommonMessages.DELETE,
          icon: CommonIcons.DELETE_ICON,
          style: Style.DANGER,
          actuator: new GenerateTemplateDeleteAction(
            this.GenerateTemplatesService,
            this.modalService,
            this.notificationService
          ),
        })
      ]
    };
  };
}
