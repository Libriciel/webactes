import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


import { GenerateTemplate, GenerateTemplateFilesService, GenerateTemplateListConfig, GenerateTemplatesService, Pagination, } from '../../core';

import { ActionItemsSet } from '../../ls-common/model/action-items-set';

import { CommonIcons, CommonMessages } from '../../ls-common';

import { GenerateTemplatesActionMapping, GenerateTemplatesMessages } from './shared';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GenerateTemplateModalCreateComponent } from './shared/helpers';

@Component({
  selector: 'wa-actors',
  templateUrl: './generate-templates.component.html',
  styleUrls: ['./generate-templates.component.scss']
})
export class GenerateTemplatesComponent implements OnInit {
  canModify: boolean;
  canDelete: boolean;
  isSubmitting = false;
  isDeleting = false;

  messages = GenerateTemplatesMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  commonActions: ActionItemsSet<GenerateTemplate>;
  singleActions: ActionItemsSet<GenerateTemplate>;
  otherSingleActions: ActionItemsSet<GenerateTemplate>;

  pagination: Pagination = {page: 1, count: 1, perPage: 1};

  query: GenerateTemplateListConfig;
  generateTemplates: GenerateTemplate[];
  loading = false;
  currentPage = 1;
  totalPages: Array<number> = [1];

  limit: number;
  listConfig: GenerateTemplateListConfig = {
    type: 'all',
    filters: {}
  };

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private generateTemplatesService: GenerateTemplatesService,
    private generateTemplateFilesService: GenerateTemplateFilesService,
    private generateTemplatesActionStateMap: GenerateTemplatesActionMapping,
  ) {
  }

  config(config: GenerateTemplateListConfig) {
    if (config) {
      this.limit = 10;
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  ngOnInit() {
    this.singleActions = this.generateTemplatesActionStateMap.getSingleActions();
    this.otherSingleActions = this.generateTemplatesActionStateMap.getOtherSingleActions();
    this.commonActions = this.generateTemplatesActionStateMap.getCommonActions();
    this.config(this.listConfig);
  }

  goBack() {
    this.router.navigateByUrl('administration').then();
  }

  setToPage(pageNumber: number) {
    this.currentPage = pageNumber;
    this.runQuery();
  }

  runQuery() {
    this.loading = true;
    this.generateTemplates = null;

    // Create limit and offset filter (if necessary)
    if (this.limit) {
      this.query.filters.limit = this.limit;
      this.query.filters.offset = (this.limit * (this.currentPage - 1));
      this.query.filters.page = this.currentPage;
    }

    this.generateTemplatesService.query(this.query)
      .subscribe(data => {
        this.loading = false;
        this.generateTemplates = data.generateTemplates;
        this.pagination = data.pagination;
      });
  }

  applyFilter(event: any) {
    this.query.filters.searchedText = event.trim();
    this.reload();
  }


  hasFilter(): boolean {
    return this.query.filters.searchedText && this.query.filters.searchedText !== '';
  }

  createGenerateTemplate() {
    const modalRef = this.modalService.open(GenerateTemplateModalCreateComponent, {
      backdrop: 'static',
      centered: true,
      size: 'lg'
    });
    modalRef.componentInstance.isCreated.subscribe(() => {
      this.reload();
    });
  }

  gotoPage(numPage) {
    this.setToPage(numPage);
  }

  reload() {
    this.setToPage(1);
  }
}
