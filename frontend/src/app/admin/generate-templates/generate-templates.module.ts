import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared';

import { GenerateTemplatesRoutingModule } from './generate-templates-routing.module';
import { GenerateTemplatesComponent } from './generate-templates.component';
import { GenerateTemplateModalCreateComponent, GenerateTemplateModalUpdateComponent } from './shared/helpers';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { joinGenerateTemplateFilesPipe } from './shared';


@NgModule({
  declarations: [
    GenerateTemplatesComponent,
    GenerateTemplateModalCreateComponent,
    GenerateTemplateModalUpdateComponent,
    joinGenerateTemplateFilesPipe
  ],
  imports: [
    SharedModule,
    GenerateTemplatesRoutingModule,
  ],
  providers: [NgbActiveModal]
})
export class GenerateTemplatesModule {
}
