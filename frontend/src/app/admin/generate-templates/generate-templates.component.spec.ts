import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateTemplatesComponent } from './generate-templates.component';

describe('ActorsComponent', () => {
  let component: GenerateTemplatesComponent;
  let fixture: ComponentFixture<GenerateTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenerateTemplatesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
