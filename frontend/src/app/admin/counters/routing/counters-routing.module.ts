import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { AdminMessages } from '../../i18n/admin-messages';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountersListComponent } from '../components/counters-list/counters-list.component';
import { CountersListMessages } from '../components/counters-list/counters-list-messages';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_COUNTER_PATH,
    component: CountersListComponent,
    data: {
      pageParameters: {
        title: CountersListMessages.TITLE,
        breadcrumb: [{
          name: AdminMessages.PAGE_ADMIN_TITLE,
          location: RoutingPaths.ADMIN_PATH
        }
          ,
          {name: CountersListMessages.TITLE, location: RoutingPaths.ADMIN_COUNTER_PATH}
        ],
        fluid: true
      } as PageParameters
    }
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class CountersRoutingModule {
}
