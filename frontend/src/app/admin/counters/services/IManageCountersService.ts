import { Observable } from 'rxjs';
import { FilterType } from '../../../model/util/filter-type';
import { Pagination } from '../../../model/pagination';
import { Counter } from '../../../model/counter/counter';
import { Sequence } from '../../../model/sequence/sequence';

export abstract class IManageCountersService {

  abstract addCounter(counter: Counter): Observable<Counter>;

  abstract getAllWithPagination(numPage?: number, filters?: FilterType, limit?: number): Observable<{ counters: Counter[], pagination: Pagination }>;

  abstract getAllWithoutPagination(filters?: FilterType): Observable<{ counters: Counter[] }>;

  abstract updateCounter(counter: Counter): Observable<Counter>;

  abstract deleteCounter(counter: Counter[]);

}
