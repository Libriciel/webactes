import { Counter } from '../../../model/counter/counter';
import { AvailableActionsBasedValidator } from '../../../ls-common/components/tables/validator/available-actions-based-validator';
import { CounterAvailableActions } from '../../../model/counter/counter-available-actions.enum';

export class EditCounterActionValidator extends AvailableActionsBasedValidator<Counter> {

  constructor() {
    super(CounterAvailableActions.isEditable);
  }
}
