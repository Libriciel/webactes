import { Injectable } from '@angular/core';
import { ActionItem, CommonIcons } from '../../../ls-common';
import { CounterAction } from './counters-action.enum';
import { CountersListMessages } from '../components/counters-list/counters-list-messages';
import { Counter } from '../../../model/counter/counter';
import { AddCounterAction } from './add-counter-action';
import { EditCounterAction } from './edit-counter-action';
import { DeleteCounterAction } from './delete-counter-action';
import { EditCounterActionValidator } from './edit-counter-action-validator';
import { DeleteCounterActionValidator } from './delete-counter-action-validator';

@Injectable({
  providedIn: 'root'
})

export class CounterActionItemFactory {
  constructor(private addCounterAction: AddCounterAction,
              private editCounterAction: EditCounterAction,
              private deleteCounterAction: DeleteCounterAction) {
  }

  getActionItem(action: CounterAction): ActionItem<Counter> {
    switch (action) {
      case CounterAction.ADD_COUNTER:
        return new ActionItem({
          name: CountersListMessages.CREATE_COUNTER,
          icon: CommonIcons.ADD_ICON,
          style: CountersListMessages.ADD_COUNTER_ACTION_ICON_STYLE,
          actuator: this.addCounterAction
        });
      case CounterAction.EDIT_COUNTER:
        return new ActionItem({
          name: CountersListMessages.EDIT_COUNTER,
          icon: CommonIcons.EDIT_ICON,
          style: CountersListMessages.EDIT_COUNTER_ACTION_ICON_STYLE,
          actuator: this.editCounterAction,
          actionValidator: new EditCounterActionValidator(),
        });
      case CounterAction.DELETE_COUNTER:
        return new ActionItem({
          name: CountersListMessages.DELETE_COUNTER,
          icon: CommonIcons.DELETE_ICON,
          style: CountersListMessages.DELETE_COUNTER_ACTION_ICON_STYLE,
          actuator: this.deleteCounterAction,
          actionValidator: new DeleteCounterActionValidator(),
        });
      default:
        throw new Error(`unimplemented action ${action}`);
    }
  }
}
