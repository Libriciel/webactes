import { Injectable } from '@angular/core';
import { ActionResult, IActuator } from '../../../ls-common';
import { Counter } from '../../../model/counter/counter';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { map } from 'rxjs/operators';
import { CountersListMessages } from '../components/counters-list/counters-list-messages';
import { CountersModalComponent } from '../components/counters-modal/counters-modal.component';

@Injectable({
  providedIn: 'root'
})

export class EditCounterAction implements IActuator<Counter> {
  messages = CountersListMessages;

  constructor(private modalService: NgbModal) {
  }

  action(counter: Counter[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(CountersModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.counter = counter[0];

    return from(modalRef.result).pipe(
      map((updatedCounter: Counter) => {
        return {
          needReload: true,
          message: this.messages.update_counter_action_done(updatedCounter.name)
        } as ActionResult;
      }),
      catchError((error) => {
        return error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.UPDATE_COUNTER_ACTION_FAILED,
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}
