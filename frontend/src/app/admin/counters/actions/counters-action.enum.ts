export enum CounterAction {
  ADD_COUNTER,
  EDIT_COUNTER,
  DELETE_COUNTER,
}
