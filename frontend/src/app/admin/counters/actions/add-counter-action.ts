import { Injectable } from '@angular/core';
import { ActionResult, IActuator } from '../../../ls-common';
import { Counter } from '../../../model/counter/counter';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from, Observable, of } from 'rxjs';
import { CountersModalComponent } from '../components/counters-modal/counters-modal.component';
import { catchError } from 'rxjs/internal/operators';
import { map } from 'rxjs/operators';
import { CountersListMessages } from '../components/counters-list/counters-list-messages';
import { CountersModalMessages } from '../components/counters-modal/counters-modal-messages';

@Injectable({
  providedIn: 'root'
})

export class AddCounterAction implements IActuator<void> {

  listMessages = CountersListMessages;
  modalMessages = CountersModalMessages;

  constructor(private modalService: NgbModal) {
  }

  action(): Observable<ActionResult> {

    const modalRef = this.modalService.open(CountersModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });

    return from(modalRef.result).pipe(
      map((createdCounter: Counter) => {
        return {
          needReload: true,
          message: this.listMessages.add_counter_action_done(createdCounter.name),
        } as ActionResult;
      }),
      catchError((error) => {
        return error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.modalMessages.COUNTER_ADD_FAILURE,
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}
