import { Injectable } from '@angular/core';
import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { Counter } from '../../../model/counter/counter';
import { CounterAction } from './counters-action.enum';
import { CounterActionItemFactory } from './counter-action-item-factory';

@Injectable({
  providedIn: 'root'
})

export class ActionCounterMapping {

  constructor(protected counterActionItemFactory: CounterActionItemFactory) {
  }

  getCommonActions(): ActionItemsSet<Counter>[] {
    return [
    ];
  }

  getSingleActions(): ActionItemsSet<Counter> {
    return {
      actionItems: [
        this.counterActionItemFactory.getActionItem(CounterAction.EDIT_COUNTER),
        this.counterActionItemFactory.getActionItem(CounterAction.DELETE_COUNTER),
      ]
    };
  }

  getOtherSingleActions(): ActionItemsSet<Counter>[] {
    return [];
  }
}
