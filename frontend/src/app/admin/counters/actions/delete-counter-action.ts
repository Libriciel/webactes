import { Injectable } from '@angular/core';
import { ActionResult, CommonIcons, CommonMessages, ConfirmPopupComponent, IActuator } from '../../../ls-common';
import { Counter } from '../../../model/counter/counter';
import { CountersModalMessages } from '../components/counters-modal/counters-modal-messages';
import { IManageCountersService } from '../services/IManageCountersService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject } from 'rxjs';
import { Style } from '../../../wa-common';

@Injectable({
  providedIn: 'root'
})

export class DeleteCounterAction implements IActuator<Counter> {

  messages = CountersModalMessages;

  constructor(private iManageCounterService: IManageCountersService, private modalService: NgbModal) {
  }

  action(counter: Counter[]): Observable<any> {

    const returnSubject = new Subject<ActionResult>();

    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_COUNTER_TITLE;
    const counterToDelete = counter[0];
    modalRef.componentInstance.content = CountersModalMessages.build_confirm_delete_counter_message(counterToDelete.name);
    modalRef.componentInstance.confirmMsg = CommonMessages.DELETE;
    modalRef.componentInstance.style = Style.DANGER;
    modalRef.componentInstance.icon = CommonIcons.DELETE_ICON;

    modalRef.result.then(
      () => this.iManageCounterService.deleteCounter(counter)
        .subscribe(() => {
            returnSubject.next({
              error: false,
              needReload: true,
              message: CountersModalMessages.COUNTER_DELETE_SUCCESS,
            });
          },
          error => {
            console.error('Error calling sequence deletion : ', error);
            returnSubject.next({
              error: true,
              needReload: true,
              message: CountersModalMessages.COUNTER_DELETE_FAILURE,
            });
          }),
      () => returnSubject.complete()
    ).catch(error => console.error('Caught error in popup for counter deletion, this should not happen. Error : ', error));

    return returnSubject.asObservable();
  }
}
