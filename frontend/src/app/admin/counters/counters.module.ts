import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CountersListComponent } from './components/counters-list/counters-list.component';
import { CountersRoutingModule } from './routing/counters-routing.module';
import { CountersModalComponent } from './components/counters-modal/counters-modal.component';
import { SharedModule } from '../../shared';


@NgModule({
  declarations: [CountersListComponent, CountersModalComponent],
  imports: [
    CommonModule,
    CountersRoutingModule,
    HttpClientModule,
    LibricielCommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgbModule,
    SharedModule
  ]
})
export class CountersModule {
}
