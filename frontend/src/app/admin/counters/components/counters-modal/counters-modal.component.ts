import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { IManageCountersService } from '../../services/IManageCountersService';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { Counter } from '../../../../model/counter/counter';
import { CountersModalMessages } from './counters-modal-messages';
import { Sequence } from '../../../../model/sequence/sequence';
import { IManageSequenceService } from '../../../sequences/services/IManageSequenceService';
import { AbstractControl, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

export function nameValidator(createdCounters: Counter[]): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null =>
    createdCounters.find(counter => counter.name === control.value) ?
      {duplicatedName: true}
      : null;
}

export function counterDefLengthValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null =>
    control.value.split('#').join('').length > 15 ?
      {counterDefLengthError: true}
      : null;
}

@Component({
  selector: 'wa-counters-modal',
  templateUrl: './counters-modal.component.html',
  styleUrls: ['./counters-modal.component.scss']
})

export class CountersModalComponent implements OnInit {

  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  waCommonMessages = WACommonMessages;
  messages = CountersModalMessages;
  @Input()
  counter: Counter = new Counter();
  createdCounters: Counter[] = [];
  isSubmitDisabled = true;
  isSubmitting = false;
  availableSequences: Sequence[];
  formatDef: { value: string, helper: string };
  formatDate: any;
  formatDefHelp = [];
  formatDateHelp = [];
  sequence: Sequence;
  filterString: string;
  limit = 50;
  numPage = 1;
  counterForm: UntypedFormGroup;

  constructor(
    public activeModal: NgbActiveModal,
    protected iManageCountersService: IManageCountersService,
    protected iManageSequenceService: IManageSequenceService,
    protected notificationService: NotificationService) {
  }

  ngOnInit() {
    this.counterForm = new UntypedFormGroup({
      name: new UntypedFormControl(this.counter.name, Validators.required),
      comment: new UntypedFormControl(this.counter.comment),
      counter_def: new UntypedFormControl(this.counter.counter_def, [Validators.required, counterDefLengthValidator()]),
      formatDate: new UntypedFormControl(this.formatDateHelp),
      formatDef: new UntypedFormControl(this.formatDefHelp),
      reinit_def: new UntypedFormControl(this.counter.reinit_def, [Validators.required]),
      sequence: new UntypedFormControl(this.counter.sequence, [Validators.required])
    });

    this.fillData();

    this.iManageCountersService.getAllWithoutPagination().subscribe((countersList) => {
      this.createdCounters = countersList.counters.filter(counter => counter.id !== this.counter.id);
      this.counterForm.controls['name'].setValidators([Validators.required, nameValidator(this.createdCounters)]);
    });

    this.iManageSequenceService.getAll().subscribe((sequencesList) => {
      this.availableSequences = sequencesList.sequences;
      if (this.counter.sequence_id) {
        this.sequence = this.availableSequences.find(sequence => sequence.id === this.counter.sequence_id);
      }
    });
  }

  close(): void {
    this.activeModal.dismiss({isError: false});
  }

  /**
   * add counter if everything's ok
   */
  addOrUpdateCounter() {
    this.isSubmitting = true;

    let counter = new Counter(
      this.counter.id,
      this.counterForm.controls['name'].value.trim(),
      this.counterForm.controls['comment'].value,
      this.counterForm.controls['counter_def'].value,
      this.counterForm.controls['sequence'].value.id,
      null,
      this.counterForm.controls['reinit_def'].value
    );

    const call = !this.counter.id
      ? this.iManageCountersService.addCounter(counter)
      : this.iManageCountersService.updateCounter(counter);

    call.subscribe(
      () => {
        this.isSubmitting = false;
        this.activeModal.close(counter);
      },
      err => {
        console.error(err);

        if (err.error.errors.structure_id) {
          this.counterForm.controls['name'].setErrors({duplicatedName: true});
        } else if (err.error.errors.counter_def.acte_counter_def) {
          this.counterForm.controls['counter_def'].setErrors({counterDefSyntaxError: true});
        } else {
          this.counter.id ?
            this.notificationService.showError(this.messages.COUNTER_EDIT_FAILURE) :
            this.notificationService.showError(this.messages.COUNTER_ADD_FAILURE);
          this.activeModal.dismiss(err);
        }
        this.isSubmitting = false;
      })
      .add(() => this.isSubmitting = false);
  }

  private fillData() {

    this.formatDefHelp = [
      {value: '#s#', valueNoHashtag: 's', helper: 'numéro de la séquence'},
      {value: '#S#', valueNoHashtag: 'S', helper: 'numéro de la séquence sur 1 chiffre'},
      {
        value: '#SS#',
        valueNoHashtag: 'SS',
        helper: 'numéro de la séquence sur 2 chiffres (complété par un souligné)'
      },
      {
        value: '#SSS#',
        valueNoHashtag: 'SSS',
        helper: 'numéro de la séquence sur 3 chiffres (complété par des soulignés)'
      },
      {
        value: '#SSSS#',
        valueNoHashtag: 'SSSS',
        helper: 'numéro de la séquence sur 4 chiffres (complété par des soulignés)'
      },
      {
        value: '#00#',
        valueNoHashtag: '00',
        helper: 'numéro de la séquence sur 2 chiffres (complété par un zéro)'
      },
      {
        value: '#000#',
        valueNoHashtag: '000',
        helper: 'numéro de la séquence sur 3 chiffres (complété par des zéros)'
      },
      {
        value: '#0000#',
        valueNoHashtag: '0000',
        helper: 'numéro de la séquence sur 4 chiffres (complété par des zéros)'
      },
      {value: '#AAAA#', valueNoHashtag: 'AAAA', helper: 'année sur 4 chiffres'},
      {value: '#AA#', valueNoHashtag: 'AA', helper: 'année sur 2 chiffres'},
      {value: '#M#', valueNoHashtag: 'M', helper: 'numéro du mois sans zéro significatif'},
      {value: '#MM#', valueNoHashtag: 'MM', helper: 'numéro du mois avec zéro significatif'},
      {value: '#J#', valueNoHashtag: 'J', helper: 'numéro du jour sans zéro significatif'},
      {value: '#JJ#', valueNoHashtag: 'JJ', helper: 'numéro du jour avec zéro significatif'},
      {value: '#p#', valueNoHashtag: 'p', helper: 'numéro de la position'}
    ];

    this.formatDateHelp = [
      {value: '#AAAA#', helper: 'Année'},
      {value: '#MM#', helper: 'Mois'},
      {value: '#JJ#', helper: 'Jour'}
    ];

    if (this.counter.id) {
      this.formatDate = this.formatDateHelp.find(f => f.value === this.counter.reinit_def);
    }
  }

  updateReinitDef() {
    const formatDateFormValue = this.counterForm.get('formatDate').value; // value du form
    this.counterForm.get('reinit_def').setValue(formatDateFormValue.value); // value du tableau formatDefHelp
  }

  appendToCounterDef() {
    const newValue = this.counterForm.get('counter_def').value + this.counterForm.get('formatDef').value.value;
    this.counterForm.get('counter_def').setValue(newValue);
  }
}
