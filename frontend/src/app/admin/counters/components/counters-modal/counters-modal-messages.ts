export class CountersModalMessages {
  static CREATE_COUNTER = `Création d'un compteur`;
  static UPDATE_COUNTER = `Modification d'un compteur`;
  static COUNTER_ADD = 'Créer';
  static COUNTER_UPDATE = 'Enregistrer';
  static COMMENT = 'Commentaire';
  static COUNTER_DEF = 'Définition';
  static COUNTER_REINIT_DEF = 'Critère de réinitialisation';
  static COUNTER_SEQUENCE = 'Séquence';
  static NEW_COUNTER_NAME = 'Libellé';
  static NEW_COUNTER_NAME_PLACE_HOLDER = 'Veuillez saisir le libellé du compteur';
  static NEW_COUNTER_COMMENT_PLACE_HOLDER = 'Veuillez saisir un commentaire';
  static NEW_COUNTER_REINIT_DEF = 'Veuillez choisir un critère de réinitialisation';
  static COUNTER_DEF_HELP = 'Veuillez sélectionner des options pour votre définition';

  static CONFIRM_DELETE_COUNTER_TITLE = 'Supprimer le compteur';
  static COUNTER_DELETE_SUCCESS = 'Compteur supprimé avec succès!';
  static COUNTER_DELETE_FAILURE = 'Une erreur est survenue pendant la suppression du compteur';
  static COUNTER_ADD_FAILURE = 'Erreur lors de la création du compteur';
  static COUNTER_EDIT_FAILURE = 'Erreur lors de l\'édition du compteur';
  static ALREADY_USED_SEQUENCE_ERROR_MESSAGE = 'Cette séquence est déjà utilisée par un compteur';
  static DUPLICATED_COUNTER_NAME_ERROR_MESSAGE = 'Ce nom de compteur existe déjà';
  static COUNTER_DEF_LENGTH_ERROR_MESSAGE = 'La définition de compteur ne doit pas dépasser 15 caractères';
  static COUNTER_SYNTAX_ERROR_MESSAGE = 'La définition du compteur ne respecte pas le protocole @cte';

  static build_confirm_delete_counter_message(counterName: string): string {
    return `Êtes-vous sûr de vouloir supprimer le compteur "${counterName}"?`;
  }
}
