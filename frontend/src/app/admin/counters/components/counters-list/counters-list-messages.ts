import { Style } from '../../../../wa-common';

export class CountersListMessages {

  static CREATE_COUNTER = 'Créer un compteur';
  static NAME = 'Libellé';
  static TITLE = 'Compteurs';
  static EDIT_COUNTER  = 'Modifier un compteur';
  static DELETE_COUNTER  = 'Supprimer';
  static COMMENT = 'Commentaire';
  static COUNTER_DEF = 'Définition';
  static COUNTER_REINIT_DEF = 'Réinitialisation';
  static COUNTER_SEQUENCE = 'Séquence';
  static UPDATE_COUNTER_ACTION_FAILED = 'Erreur lors de la sauvegarde des modifications du compteur.';
// ******* CSS MESSAGES ********
  static ADD_COUNTER_ACTION_ICON_STYLE = Style.NORMAL;
  static EDIT_COUNTER_ACTION_ICON_STYLE = Style.NORMAL;
  static DELETE_COUNTER_ACTION_ICON_STYLE = Style.DANGER;

  static update_counter_action_done(counter: string) {
    return 'Les modifications du compteur "' + counter + '" ont été enregistrées avec succès.';
  }

  static add_counter_action_done(counter: string) {
    return 'Le compteur "' + counter + '" a été créé avec succès.';
  }
}
