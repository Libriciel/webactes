import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Pagination } from '../../../../model/pagination';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { Counter } from '../../../../model/counter/counter';
import { CountersListMessages } from './counters-list-messages';
import { IManageCountersService } from '../../services/IManageCountersService';
import { first } from 'rxjs/operators';
import { CounterAction } from '../../actions/counters-action.enum';
import { CounterActionItemFactory } from '../../actions/counter-action-item-factory';
import { ActionCounterMapping } from '../../actions/action-counter-mapping';
import { Sequence } from '../../../../model/sequence/sequence';

@Component({
  selector: 'wa-counters-list',
  templateUrl: './counters-list.component.html',
  styleUrls: ['./counters-list.component.scss']
})
export class CountersListComponent implements OnInit {
  @Output() actionResult = new EventEmitter();

  messages = CountersListMessages;
  counters: Counter[];

  loading = true;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};
  sequences: Sequence[];

  waCommonMessages = WACommonMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  commonActions: ActionItemsSet<Counter>[];
  singleActions: ActionItemsSet<Counter>;
  otherSingleActions: ActionItemsSet<Counter>[];

  filterString: string;

  counterAction = CounterAction;

  constructor(
    protected modalService: NgbModal,
    protected iManageCountersService: IManageCountersService,
    protected notificationService: NotificationService,
    protected activatedRoute: ActivatedRoute,
    protected actionCounterStateMap: ActionCounterMapping,
    public counterActionItemFactory: CounterActionItemFactory,
    protected router: Router,
  ) {
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(() => {
      this.singleActions = this.actionCounterStateMap.getSingleActions();
      this.otherSingleActions = this.actionCounterStateMap.getOtherSingleActions();
      this.commonActions = this.actionCounterStateMap.getCommonActions();
    });
    this.gotoPage(1);
  }

  gotoPage(numPage: number) {
    this.loading = true;
    this.counters = null;
    const filters = this.filterString ? {searchedText: this.filterString} : {searchedText: ''};
    this.iManageCountersService.getAllWithPagination(numPage, filters)
      .pipe(first())
      .subscribe(countersResult => {
        this.counters = countersResult.counters;
        this.pagination = countersResult.pagination;
        this.loading = false;
      });
  }

  reloadCounters() {
    this.gotoPage(1);
  }

  applyFilter(event: string) {
    this.filterString = event.trim();
    this.reloadCounters();
  }

  hasFilter(): boolean {
    return this.filterString && this.filterString !== '';
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

  public actionDone($event: { error: any; message: string; needReload: any; }) {
    if ($event) {
      if ($event.error) {
        if ($event.message) {
          this.notificationService.showError($event.message);
        }
        this.reloadCounters();
      } else {
        if ($event.message) {
          this.notificationService.showSuccess($event.message);
        }
        if ($event.needReload) {
          this.reloadCounters();
        }
      }
    }
  }
}
