export class AdminMessages {
  static PAGE_ADMIN_TITLE = 'Administration';

  static PAGE_CONNEXION_PASTELL_TITLE = 'Connexion Pastell';

  static CONNEXION_PASTELL_HELP = `Pour utiliser la télétransmission, vous devez vous connecter à l'application <em>Pastell</em> en enregistrant les paramètres de connexion.`;

  static URL_PASTELL_INPUT_LABEL = 'Url de Pastell';
  // noinspection SpellCheckingInspection
  static URL_PASTELL_INPUT_PLACE_HOLDER = 'monpastell.fr/';
  static ID_PASTELL_INPUT_LABEL = 'Identifiant de connexion Pastell';
  // noinspection SpellCheckingInspection
  static ID_PASTELL_INPUT_PLACE_HOLDER = 'webactes_collectivite';
  static PASSWORD_PASTELL_INPUT_LABEL = 'Mot de passe';

  static IDE_PASTELL_INPUT_LABEL = 'Identifiant de collectivité Pastell (id_e)';
  static IDE_PASTELL_INPUT_PLACE_HOLDER = '';

  static BTN_TEST_CONNEXION_PASTELL_TITLE = 'Tester la connexion Pastell';

  static URL_SLOW_INPUT_LABEL = 'Url de Slow';
  // noinspection SpellCheckingInspection
  static URL_SLOW_INPUT_PLACE_HOLDER = 'monslow.fr/';

  static PASTELL_CONNEXION_OK = 'La connexion à <em>Pastell</em> a réussi avec ces paramètres.';
  static PASTELL_CONNEXION_KO = 'La connexion à <em>Pastell</em> a échoué avec ces paramètres.';

  static CONNEXION_PASTELL_SUCCESS_MESSAGE = 'Connexion Pastell enregistrée.';
  static CONNEXION_PASTELL_ERROR_MESSAGE = 'Erreur lors de la sauvegarde de la connexion.';


  static CONNEXION_IDELIBRE_HELP = `Pour utiliser l'envoi de la convocation vers <em>i-délibRE</em>, vous devez vous connecter à l'application en enregistrant les paramètres de connexion.`;

  static PAGE_CONNEXION_IDELIBRE_TITLE = 'Connexion i-délibRE';

  static URL_IDELIBRE_INPUT_LABEL = 'Url d\'i-délibRE';
  // noinspection SpellCheckingInspection
  static URL_IDELIBRE_INPUT_PLACE_HOLDER = 'monidelibre.fr/';
  static ID_IDELIBRE_INPUT_LABEL = 'Identifiant de connexion i-délibRE';
  // noinspection SpellCheckingInspection
  static ID_IDELIBRE_INPUT_PLACE_HOLDER = 'webactes_collectivite';
  static PASSWORD_IDELIBRE_INPUT_LABEL = 'Mot de passe';

  static IDE_IDELIBRE_INPUT_LABEL = 'Identifiant de collectivité i-délibRE';
  static IDE_IDELIBRE_INPUT_PLACE_HOLDER = '';

  static BTN_TEST_CONNEXION_IDELIBRE_TITLE = 'Tester la connexion i-délibRE';

  static IDELIBRE_CONNEXION_OK = 'La connexion à <em>Idelibre</em> a réussi avec ces paramètres.';
  static IDELIBRE_CONNEXION_KO = 'La connexion à <em>Idelibre</em> a échoué avec ces paramètres.';

  static CONNEXION_IDELIBRE_SUCCESS_MESSAGE = 'Connexion Idelibre enregistrée.';
  static CONNEXION_IDELIBRE_ERROR_MESSAGE = 'Erreur lors de la sauvegarde de la connexion.';
}
