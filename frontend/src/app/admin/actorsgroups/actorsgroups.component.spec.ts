import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActorsgroupsComponent } from './actorsgroups.component';

describe('ActorsgroupsComponent', () => {
  let component: ActorsgroupsComponent;
  let fixture: ComponentFixture<ActorsgroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActorsgroupsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActorsgroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
