import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Actorgroup, ActorgroupListConfig, ActorsgroupsService, Pagination, } from '../../core';
import { ActionItemsSet } from '../../ls-common/model/action-items-set';
import { CommonIcons, CommonMessages } from '../../ls-common';
import { ActorsgroupsActionMapping, ActorsgroupsMessages } from './shared';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActorgroupModalCreateComponent } from './shared/helpers/actorgroup-modal-create.component';

@Component({
  selector: 'wa-actorsgroups',
  templateUrl: './actorsgroups.component.html',
  styleUrls: ['./actorsgroups.component.scss']
})
export class ActorsgroupsComponent implements OnInit {
  canModify: boolean;
  canDelete: boolean;
  isSubmitting = false;
  isDeleting = false;

  messages = ActorsgroupsMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  commonActions: ActionItemsSet<Actorgroup>;
  singleActions: ActionItemsSet<Actorgroup>;
  otherSingleActions: ActionItemsSet<Actorgroup>;

  pagination: Pagination = {page: 1, count: 1, perPage: 1};

  query: ActorgroupListConfig;
  actorGroups: Actorgroup[];
  loading = false;
  currentPage = 1;
  totalPages: Array<number> = [1];

  limit: number;
  listConfig: ActorgroupListConfig = {
    type: 'all',
    filters: {}
  };

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private actorsgroupsService: ActorsgroupsService,
    protected actorsgroupsActionStateMap: ActorsgroupsActionMapping,
  ) {
  }

  config(config: ActorgroupListConfig) {
    if (config) {
      this.limit = 10;
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  ngOnInit() {
    this.singleActions = this.actorsgroupsActionStateMap.getSingleActions();
    this.otherSingleActions = this.actorsgroupsActionStateMap.getOtherSingleActions();
    this.commonActions = this.actorsgroupsActionStateMap.getCommonActions();
    this.config(this.listConfig);
  }

  goBack() {
    this.router.navigateByUrl('administration').then();
  }

  setToPage(pageNumber: number) {
    this.currentPage = pageNumber;
    this.runQuery();
  }

  runQuery() {
    this.loading = true;
    this.actorGroups = null;

    // Create limit and offset filter (if necessary)
    if (this.limit) {
      this.query.filters.limit = this.limit;
      this.query.filters.offset = (this.limit * (this.currentPage - 1));
      this.query.filters.page = this.currentPage;
    }

    this.actorsgroupsService.query(this.query)
      .subscribe(data => {
        this.loading = false;
        this.actorGroups = data.actorGroups;
        this.pagination = data.pagination;
      });
  }

  applyFilter(event: any) {
    this.query.filters.searchedText = event.trim();
    this.reload();
  }


  hasFilter(): boolean {
    return this.query.filters.searchedText && this.query.filters.searchedText !== '';
  }

  createActorsGroup() {
    const modalRef = this.modalService.open(ActorgroupModalCreateComponent);
    modalRef.componentInstance.isCreated.subscribe(() => {
      this.reload();
    });
  }

  gotoPage(numPage) {
    this.setToPage(numPage);
  }

  reload() {
    this.setToPage(1);
  }
}
