import { Component, EventEmitter, Output } from '@angular/core';

import { CommonIcons, CommonMessages } from '../../../../ls-common';

import { ActorsgroupsMessages } from '../../shared';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Actorgroup, ActorsgroupsService } from '../../../../core';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'actorgroup-modal-create',
  templateUrl: 'actorgroup-modal-create.component.html'
})
export class ActorgroupModalCreateComponent {

  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  messages = ActorsgroupsMessages;

  actorgroupForm: UntypedFormGroup;
  actorgroup: Actorgroup;

  isSubmitting = false;
  isSubmitDisabled = true;

  @Output() isCreated = new EventEmitter<boolean>();

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private activeModal: NgbActiveModal,
    private actorsgroupsService: ActorsgroupsService,
    private notificationService: NotificationService
  ) {
    this.actorgroupForm = this._formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]]
    });
  }

  onSubmit() {
    this.isSubmitting = true;
    if (this.actorgroupForm.dirty && this.actorgroupForm.valid) {
      this.actorsgroupsService.save(this.actorgroupForm.value).subscribe({
        complete: () => {
          this.notificationService.showSuccess(this.messages.addActorGroupActionDone(this.actorgroupForm.value.name));
          this.activeModal.close();
          this.isCreated.emit(true);
        },
        error: () => {
          this.notificationService.showError('Erreur lors de la création du groupe d\'acteurs.');
          this.isSubmitting = false;
        }
      });
    }

  }

  submitActorgroup() {
    this.onSubmit();
  }
}
