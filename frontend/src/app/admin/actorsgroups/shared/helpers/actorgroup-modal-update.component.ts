import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';

import { ActorsgroupsMessages } from '../../shared';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Actorgroup, ActorsgroupsService } from '../../../../core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'actorgroup-modal-update',
  templateUrl: 'actorgroup-modal-update.component.html'
})
export class ActorgroupModalUpdateComponent implements OnInit {

  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  messages = ActorsgroupsMessages;

  actorgroupForm: UntypedFormGroup;

  isSubmitting = false;
  isSubmitDisabled = true;

  @Input() public actorgroup: Actorgroup;
  @Output() isUpdated = new EventEmitter<boolean>();

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private activeModal: NgbActiveModal,
    private actorsgroupsService: ActorsgroupsService,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit() {
    this.actorgroupForm = this._formBuilder.group({
      id: this.actorgroup.id,
      name: [this.actorgroup.name, [Validators.required, Validators.maxLength(255)]]
    });
  }

  onSubmit() {
    this.isSubmitting = true;
    if (this.actorgroupForm.dirty && this.actorgroupForm.valid) {
      this.actorsgroupsService.save(this.actorgroupForm.value).subscribe({
        complete: () => {
          this.notificationService.showSuccess(this.messages.updateActorGroupActionDone(this.actorgroupForm.value.name));
          this.activeModal.close({isError: false});
          this.isUpdated.emit(true);
        },
        error: () => {
          this.notificationService.showError('Erreur lors de la création du groupe d\'acteurs.');
          this.isSubmitting = false;
        }
      });
    }
  }

  submitActorgroup() {
    this.onSubmit();
  }

}
