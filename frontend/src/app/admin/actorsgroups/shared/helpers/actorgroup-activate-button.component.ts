import { Component, Input } from '@angular/core';
import { Actorgroup, ActorsgroupsService } from '../../../../core';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';

@Component({
  selector: 'wa-actorgroup-activate-button',
  templateUrl: './actorgroup-activate-button.component.html'
})
export class ActorgroupActiveButtonComponent {
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  constructor(
    private notificationService: NotificationService,
    private actorsgroupsService: ActorsgroupsService
  ) {
  }

  @Input() actorgroup: Actorgroup;

  onToggleActivation(actorgroup: Actorgroup, activate: boolean) {
    if (activate) {
      this.actorsgroupsService.activate(actorgroup.id).subscribe({
        complete: () => {
          this.notificationService.showSuccess('Activation du groupe d\'acteur.');
        },
        error: () => {
          this.notificationService.showError('Erreur lors de l\'activation du groupe d\'acteur.');
        }
      });
      return;
    }

    this.actorsgroupsService.deactivate(actorgroup.id).subscribe({
      complete: () => {
        this.notificationService.showSuccess('Désactivation du groupe d\'acteur.');
      },
      error: () => {
        this.notificationService.showError('Erreur lors de la désactivation du groupe d\'acteur.');
      }
    });
  }

}
