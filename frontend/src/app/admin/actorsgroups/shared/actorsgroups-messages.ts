export class ActorsgroupsMessages {
  static HEADER_NAME = `Nom du groupe d'acteurs`;
  static MODAL_TITLE_CREATE = `Création d'un groupe d\'acteurs`;
  static MODAL_TITLE_UPDATE = `Modification d'un groupe d\'acteurs`;
  static FORM_FIELD_NAME_LABEL = 'Nom';
  static FORM_FIELD_NAME_LABEL_PLACEHOLDER = 'Nom du groupe d\'acteurs';
  static BUTTON_ADD = 'Créer un groupe d\'acteurs';
  static BUTTON_SAVE = 'Enregistrer';

  static DELETE_FAILED_TOASTR_TXT = `La suppression du groupe d\'acteurs a échoué`;
  static CONFIRM_DELETE_TITLE = `Suppression du groupe d'acteurs`;
  static CONFIRM_DELETE_BUTTON = `Supprimer`;

  static sameName(name: string) {
    return `Un groupe avec le nom ${name} est déjà présent.<br>
        Veuillez renseigner un autre nom pour le groupe.`;
  }

  static updateActorGroupActionDone(nameActorGroup: string) {
    return `Les modifications du groupe d'acteurs "${nameActorGroup}" ont été enregistrées avec succès.`;
  }

  static addActorGroupActionDone(nameActorGroup: string) {
    return `Le groupe d'acteurs "${nameActorGroup}" a été créé avec succès.`;
  }

  static deleteActorGroupConfirm(actorgroupName: string): string {
    return `Voulez-vous vraiment supprimer le groupe d'acteurs "${actorgroupName}"?`;
  }

  static deleteActorGroupActionDone(actorgroupName: string): string {
    return `Le groupe d\'acteurs "${actorgroupName}" a été supprimé avec succès.`;
  }

}
