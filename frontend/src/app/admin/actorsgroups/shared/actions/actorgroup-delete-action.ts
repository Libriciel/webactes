import { Observable, Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Style } from '../../../../wa-common';
import { ActionResult, ConfirmPopupComponent, IActuator, NotificationService } from '../../../../ls-common';
import { Actorgroup, ActorsgroupsService } from '../../../../core';
import { ActorsgroupsMessages } from '../actorsgroups-messages';

export class ActorgroupDeleteAction implements IActuator<Actorgroup> {

  messages = ActorsgroupsMessages;

  constructor(
    protected actorsgroupsService: ActorsgroupsService,
    protected modalService: NgbModal,
    protected notificationService: NotificationService
  ) {
  }

  action(actorsgroups: Actorgroup[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    const returnSubject = new Subject<ActionResult>();

    modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_TITLE;
    const actorsgroupsNames = actorsgroups.map(actorgroup => actorgroup.name).join(', ');
    modalRef.componentInstance.content = this.messages.deleteActorGroupConfirm(actorsgroupsNames);

    modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_DELETE_BUTTON;
    modalRef.componentInstance.style = Style.DANGER;

    modalRef.result
      .then(
        () => {
          this.actorsgroupsService.destroy(actorsgroups[0].id).subscribe({
            complete: () => {
              returnSubject.next({
                error: false,
                needReload: true,
                message: this.messages.deleteActorGroupActionDone(actorsgroups[0].name)
              });
            },
            error: () => {
              console.error('Error calling acotorgroup deletion');
              returnSubject.next({
                error: true,
                needReload: true,
                message: this.messages.DELETE_FAILED_TOASTR_TXT
              });
            }
          });
        },
        () => returnSubject.complete()
      )
      .catch(error => {
        console.error('Caught error in popup for circuit deletion : ', error);
      });

    return returnSubject.asObservable();

  }
}
