import { Observable, Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionResult, IActuator, NotificationService } from '../../../../ls-common';
import { Actorgroup } from '../../../../core';
import { ActorsgroupsMessages } from '../actorsgroups-messages';
import { ActorgroupModalUpdateComponent } from '../helpers/actorgroup-modal-update.component';

export class ActorgroupUpdateAction implements IActuator<Actorgroup> {

  readonly messages = ActorsgroupsMessages;

  constructor(
    protected modalService: NgbModal,
    protected notificationService: NotificationService
  ) {
  }

  action(actorsgroups: Actorgroup[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(ActorgroupModalUpdateComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true,
    });

    modalRef.componentInstance.actorgroup = actorsgroups[0];
    const returnSubject = new Subject<ActionResult>();

    modalRef.componentInstance.isUpdated.subscribe(() => {
      returnSubject.next({
        error: false,
        needReload: true,
        message: null
      });
    });

    modalRef.result.then(
      () => returnSubject.complete(),
      () => {
        returnSubject.next();
        returnSubject.complete();
      }
    );

    return returnSubject.asObservable();
  }
}
