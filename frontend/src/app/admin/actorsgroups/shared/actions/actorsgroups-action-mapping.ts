import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { Injectable } from '@angular/core';

import { CommonIcons, CommonMessages } from '../../../../ls-common';
import { ActionItem } from '../../../../ls-common';
import { Actorgroup, ActorsgroupsService } from '../../../../core';

import { Style } from '../../../../wa-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationService } from '../../../../ls-common';

import { ActorsgroupsMessages } from '../actorsgroups-messages';
import { ActorgroupDeleteAction } from './actorgroup-delete-action';
import { ActorgroupUpdateAction } from './actorgroup-update-action';

@Injectable({
  providedIn: 'root'
})
export class ActorsgroupsActionMapping {

  messages = ActorsgroupsMessages;

  constructor(
    protected modalService: NgbModal,
    protected actorsgroupsService: ActorsgroupsService,
    protected notificationService: NotificationService,
  ) {
  }

  getCommonActions(): ActionItemsSet<Actorgroup> {
    return null;
  }

  getOtherSingleActions(): ActionItemsSet<Actorgroup> {
    return null;
  }

  getSingleActions(): ActionItemsSet<Actorgroup> {
    return {
      actionItems: [
        new ActionItem({
          name: CommonMessages.MODIFY,
          icon: CommonIcons.EDIT_ICON,
          actuator: new ActorgroupUpdateAction(
            this.modalService,
            this.notificationService
          ),
        }),
        new ActionItem({
          name: CommonMessages.DELETE,
          icon: CommonIcons.DELETE_ICON,
          style: Style.DANGER,
          actuator: new ActorgroupDeleteAction(
            this.actorsgroupsService,
            this.modalService,
            this.notificationService
          ),
        })
      ]
    };
  }
}
