import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageParameters } from 'src/app/wa-common/components/page/model/page-parameters';
import { ActorsgroupsComponent } from './actorsgroups.component';

const routes: Routes = [
  {
    path: 'administration/groupes-acteurs',
    component: ActorsgroupsComponent,
    data: {
      pageParameters: {
        title: `Groupes d'acteurs`,
        breadcrumb: [
          {
            name: `Administration`,
            location: `administration`
          },
          {
            name: `Groupes d'acteurs`, location: 'administration/groupes-acteurs'
          }
        ],
        fluid: true
      } as PageParameters
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActorgroupRoutingModule { }
