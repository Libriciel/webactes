import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared';
import { ActorgroupRoutingModule } from './actorgroup-routing.module';
import { ActorsgroupsComponent } from './actorsgroups.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActorgroupActiveButtonComponent } from './shared/helpers/actorgroup-activate-button.component';
import { ActorgroupModalCreateComponent } from './shared/helpers/actorgroup-modal-create.component';
import { ActorgroupModalUpdateComponent } from './shared/helpers/actorgroup-modal-update.component';


@NgModule({
  declarations: [
    ActorsgroupsComponent,
    ActorgroupActiveButtonComponent,
    ActorgroupModalCreateComponent,
    ActorgroupModalUpdateComponent
  ],
  imports: [
    SharedModule,
    ActorgroupRoutingModule,
  ],
  providers: [NgbActiveModal]
})
export class ActorsgroupsModule {
}
