import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageParameters } from 'src/app/wa-common/components/page/model/page-parameters';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: 'administration',
    component: AdminComponent,
    data: {
      pageParameters: {
        title: `Administration`,
        breadcrumb: [{
          name: `Administration`, location: `administration`
        }
        ],
        fluid: true
      } as PageParameters
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
