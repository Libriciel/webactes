import { Observable } from 'rxjs';
import { Sequence } from '../../../model/sequence/sequence';
import { Pagination } from '../../../model/pagination';
import { FilterType } from '../../../model/util/filter-type';

export abstract class IManageSequenceService {

  abstract addSequence(sequence: Sequence): Observable<Sequence>;

  abstract getAll(numPage?: number, filters?: FilterType, limit?: number): Observable<{ sequences: Sequence[], pagination: Pagination }>;

  abstract updateSequence(sequence: Sequence): Observable<Sequence>;

  abstract deleteSequence(sequences: Sequence[]);
}
