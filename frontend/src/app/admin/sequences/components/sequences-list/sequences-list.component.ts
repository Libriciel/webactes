import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IManageSequenceService } from '../../services/IManageSequenceService';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { ActivatedRoute, Router } from '@angular/router';
import { Pagination } from '../../../../model/pagination';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { SequencesListMessages } from './sequences-list-messages';
import { Sequence } from '../../../../model/sequence/sequence';
import { SequenceAction } from '../action/sequence-action.enum';
import { ActionSequenceMapping } from '../action/action-sequence-mapping';
import { SequenceActionItemFactory } from '../action/sequence-action-item-factory';
import { first } from 'rxjs/operators';
import { RoutingPaths } from '../../../../wa-common/routing-paths';

@Component({
  selector: 'wa-sequences-list',
  templateUrl: './sequences-list.component.html',
  styleUrls: ['./sequences-list.component.scss']
})
export class SequencesListComponent implements OnInit {
  @Output() actionResult = new EventEmitter();

  messages = SequencesListMessages;
  sequences: Sequence[];

  loading = true;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};

  waCommonMessages = WACommonMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  commonActions: ActionItemsSet<Sequence>[];
  singleActions: ActionItemsSet<Sequence>;
  otherSingleActions: ActionItemsSet<Sequence>[];

  filterString: string;

  sequenceAction = SequenceAction;

  constructor(
    protected modalService: NgbModal,
    protected iManageSequenceService: IManageSequenceService,
    protected notificationService: NotificationService,
    protected activatedRoute: ActivatedRoute,
    protected actionSequenceStateMap: ActionSequenceMapping,
    public sequenceActionItemFactory: SequenceActionItemFactory,
    protected router: Router,
  ) {
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(() => {
      this.singleActions = this.actionSequenceStateMap.getSingleActions();
      this.otherSingleActions = this.actionSequenceStateMap.getOtherSingleActions();
      this.commonActions = this.actionSequenceStateMap.getCommonActions();
    });
    this.gotoPage(1);
  }

  gotoPage(numPage: number) {
    this.loading = true;
    this.sequences = null;
    const filters = this.filterString ? {searchedText: this.filterString} : {searchedText: ''};
    this.iManageSequenceService.getAll(numPage, filters, 10)
      .pipe(first())
      .subscribe(sequencesResult => {
        this.sequences = sequencesResult.sequences;
        this.pagination = sequencesResult.pagination;
        this.loading = false;
      });
  }

  reloadSequences() {
    this.gotoPage(1);
  }

  applyFilter(event: string) {
    this.filterString = event.trim();
    this.reloadSequences();
  }

  hasFilter(): boolean {
    return this.filterString && this.filterString !== '';
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

  public actionDone($event: { error: any; message: string; needReload: any; }) {
    if ($event) {
      if ($event.error) {
        if ($event.message) {
          this.notificationService.showError($event.message);
        }
        this.reloadSequences();
      } else {
        if ($event.message) {
          this.notificationService.showSuccess($event.message);
        }
        if ($event.needReload) {
          this.reloadSequences();
        }
      }
    }
  }

}
