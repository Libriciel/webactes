import { Style } from '../../../../wa-common';

export class SequencesListMessages {

  static CREATE_SEQUENCE = 'Créer une séquence';
  static NAME = 'Libellé';
  static TITLE = 'Séquences';
  static EDIT_SEQUENCE = 'Modifier une séquence';
  static DELETE_SEQUENCE = 'Supprimer';
  static COMMENT = 'Commentaire';
  static SEQUENCE_NUM = 'Numéro de séquence';
  static UPDATE_SEQUENCE_ACTION_FAILED = 'La sauvegarde des modifications de la séquence a échoué.';
// ******* CSS MESSAGES ********
  static ADD_SEQUENCE_ACTION_ICON_STYLE = Style.NORMAL;
  static EDIT_SEQUENCE_ACTION_ICON_STYLE =  Style.NORMAL;
  static DELETE_SEQUENCE_ACTION_ICON_STYLE = Style.DANGER;

  static update_sequence_action_done(sequence: string) {
    return 'Les modifications de la séquence "' + sequence + '" ont été enregistrées avec succès.';
  }

}
