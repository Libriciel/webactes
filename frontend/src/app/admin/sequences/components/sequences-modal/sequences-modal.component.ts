import { Component, Input, OnInit } from '@angular/core';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SequencesModalMessages } from './sequences-modal-messages';
import { Sequence } from '../../../../model/sequence/sequence';
import { IManageSequenceService } from '../../services/IManageSequenceService';
import { take } from 'rxjs/operators';
import { AbstractControl, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

export function nameValidator(sequencesList): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null =>
    sequencesList.find(sequence => sequence.name === control.value) ?
      {duplicatedName: true}
      : null;
}

@Component({
  selector: 'wa-sequences-modal',
  templateUrl: './sequences-modal.component.html',
  styleUrls: ['./sequences-modal.component.scss']
})
export class SequencesModalComponent implements OnInit {

  @Input()
  sequence: Sequence = new Sequence();
  sequencesList: Sequence[] = [];
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  messages = SequencesModalMessages;
  isSubmitting = false;
  sequenceForm: UntypedFormGroup;

  constructor(
    public activeModal: NgbActiveModal,
    protected manageSequenceService: IManageSequenceService,
    protected notificationService: NotificationService) {
  }

  ngOnInit() {
    this.sequenceForm = new UntypedFormGroup({
      name: new UntypedFormControl(this.sequence.name, Validators.required),
      comment: new UntypedFormControl(this.sequence.comment),
      sequence_num: new UntypedFormControl(this.sequence.sequence_num, [Validators.required, Validators.pattern('\\d*')])
    });
    this.manageSequenceService.getAll()
      .pipe(take(1))
      .subscribe(sequencesResult => {
        this.sequencesList = sequencesResult.sequences.filter(sequence => sequence.id !== this.sequence.id);
        this.sequenceForm.controls['name'].setValidators([Validators.required, nameValidator(this.sequencesList)]);
      });
  }

  close(): void {
    this.activeModal.dismiss({isError: false});
  }

  /**
   * add sequence if everything's ok
   */
  addOrUpdateSequence() {
    this.isSubmitting = true;
    const sequence: Sequence = this.sequence.clone();
    sequence.name = this.sequenceForm.controls['name'].value;
    sequence.comment = this.sequenceForm.controls['comment'].value;
    sequence.sequence_num = this.sequenceForm.controls['sequence_num'].value;

    const call = !this.sequence.id
      ? this.manageSequenceService.addSequence(sequence)
      : this.manageSequenceService.updateSequence(sequence);

    call.subscribe(
      () => this.activeModal.close(sequence),
      err => {
        console.error(err);
        this.sequence.id ?
          this.notificationService.showError(this.messages.SEQUENCE_EDIT_FAILURE) :
          this.notificationService.showError(this.messages.SEQUENCE_ADD_FAILURE);
        this.activeModal.dismiss(err);
      })
      .add(() => this.isSubmitting = false);
  }

}
