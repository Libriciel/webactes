
export class SequencesModalMessages {
  static CREATE_SEQUENCE = `Création d'une séquence`;
  static UPDATE_SEQUENCE = `Modification d'une séquence`;
  static SEQUENCE_ADD = 'Créer';
  static SEQUENCE_UPDATE = 'Enregistrer';
  static COMMENT = 'Commentaire';
  static SEQUENCE_NUM = 'Numéro de séquence';
  static NEW_SEQUENCE_NAME = 'Libellé';
  static NEW_SEQUENCE_NAME_PLACE_HOLDER = 'Veuillez saisir le libellé de la séquence';
  static NEW_SEQUENCE_COMMENT_PLACE_HOLDER = 'Veuillez saisir un commentaire';
  static NEW_SEQUENCE_NUM_PLACE_HOLDER = 'Veuillez saisir un numéro de séquence';

  static CONFIRM_DELETE_SEQUENCE_TITLE = 'Supprimer la séquence';
  static SEQUENCE_DELETE_SUCCESS = 'Séquence supprimée avec succès!';
  static SEQUENCE_DELETE_FAILURE = 'Une erreur est survenue pendant la suppression de la séquence';
  static SEQUENCE_ADD_FAILURE = 'La création de la séquence a échoué';
  static SEQUENCE_EDIT_FAILURE = 'La modification de la séquence a échoué';
  static SEQUENCE_FORMAT_ERROR = 'Le numéro de séquence ne doit comporter que des chiffres';
  static SEQUENCE_DUPLICATE_NAME = 'Cette séquence a déjà été créée';

  static build_confirm_delete_sequence_message(sequenceName: string): string {
    return `Êtes-vous sûr de vouloir supprimer la séquence "${sequenceName}"?`;
  }

  static add_sequence_action_done(sequence: string) {
    return 'La séquence "' + sequence + '" a été créée avec succès.';
  }
}
