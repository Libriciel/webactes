import { Injectable } from '@angular/core';
import { ActionItem, CommonIcons } from '../../../../ls-common';
import { SequenceAction } from './sequence-action.enum';
import { Sequence } from '../../../../model/sequence/sequence';
import { SequencesListMessages } from '../sequences-list/sequences-list-messages';
import { AddSequenceAction } from './add-sequence-action';
import { EditSequenceAction } from './edit-sequence-action';
import { DeleteSequenceAction } from './delete-sequence-action';
import { EditSequenceActionValidator } from './edit-sequence-action-validator';
import { DeleteSequenceActionValidator } from './delete-sequence-action-validator';


@Injectable({
  providedIn: 'root'
})

export class SequenceActionItemFactory {
  constructor(private addSequenceAction: AddSequenceAction,
              private editSequenceAction: EditSequenceAction,
              private deleteSequenceAction: DeleteSequenceAction) {
  }

  getActionItem(action: SequenceAction): ActionItem<Sequence> {
    switch (action) {
      case SequenceAction.ADD_SEQUENCE:
        return new ActionItem({
          name: SequencesListMessages.CREATE_SEQUENCE,
          icon: CommonIcons.ADD_ICON,
          style: SequencesListMessages.ADD_SEQUENCE_ACTION_ICON_STYLE,
          actuator: this.addSequenceAction
        });
      case SequenceAction.EDIT_SEQUENCE:
        return new ActionItem({
          name: SequencesListMessages.EDIT_SEQUENCE,
          icon: CommonIcons.EDIT_ICON,
          style: SequencesListMessages.EDIT_SEQUENCE_ACTION_ICON_STYLE,
          actuator: this.editSequenceAction,
          actionValidator: new EditSequenceActionValidator(),
        });
      case SequenceAction.DELETE_SEQUENCE:
        return new ActionItem({
          name: SequencesListMessages.DELETE_SEQUENCE,
          icon: CommonIcons.DELETE_ICON,
          style: SequencesListMessages.DELETE_SEQUENCE_ACTION_ICON_STYLE,
          actuator: this.deleteSequenceAction,
          actionValidator: new DeleteSequenceActionValidator(),
        });
      default:
        throw new Error(`unimplemented action ${action}`);
    }
  }
}
