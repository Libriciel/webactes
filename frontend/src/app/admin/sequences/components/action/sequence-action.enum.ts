export enum SequenceAction {
  ADD_SEQUENCE,
  EDIT_SEQUENCE,
  DELETE_SEQUENCE,
}
