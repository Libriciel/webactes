import { AvailableActionsBasedValidator } from '../../../../ls-common/components/tables/validator/available-actions-based-validator';
import { Sequence } from '../../../../model/sequence/sequence';
import { SequenceAvailableActions } from '../../../../model/sequence/sequence-available-actions.enum';


export class DeleteSequenceActionValidator extends AvailableActionsBasedValidator<Sequence> {
  constructor() {
    super(SequenceAvailableActions.isDeletable);
  }
}
