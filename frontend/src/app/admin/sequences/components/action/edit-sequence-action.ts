import { Injectable } from '@angular/core';
import { ActionResult, IActuator } from '../../../../ls-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { SequencesListMessages } from '../sequences-list/sequences-list-messages';
import { Sequence } from '../../../../model/sequence/sequence';
import { SequencesModalComponent } from '../sequences-modal/sequences-modal.component';

@Injectable({
  providedIn: 'root'
})

export class EditSequenceAction implements IActuator<Sequence> {
  messages = SequencesListMessages;

  constructor(private modalService: NgbModal) {
  }

  action(sequence: Sequence[]): Observable<ActionResult> {

    const modalRef = this.modalService.open(SequencesModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.sequence = sequence[0];

    return from(modalRef.result).pipe(
      map((updatedSequence: Sequence) =>
        ({
          needReload: true,
          message: this.messages.update_sequence_action_done(updatedSequence.name)
        } as ActionResult)),
      catchError((error) =>
        error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.UPDATE_SEQUENCE_ACTION_FAILED,
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult))
    );
  }
}
