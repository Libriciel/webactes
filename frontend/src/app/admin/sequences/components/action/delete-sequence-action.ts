import { Injectable } from '@angular/core';
import { ActionResult, CommonIcons, CommonMessages, ConfirmPopupComponent, IActuator } from '../../../../ls-common';
import { Sequence } from '../../../../model/sequence/sequence';
import { IManageSequenceService } from '../../services/IManageSequenceService';
import { Observable, Subject } from 'rxjs';
import { Style } from '../../../../wa-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SequencesModalMessages } from '../sequences-modal/sequences-modal-messages';


@Injectable({
  providedIn: 'root'
})

export class DeleteSequenceAction implements IActuator<Sequence> {

  messages = SequencesModalMessages;

  constructor(private iManageSequenceService: IManageSequenceService, private modalService: NgbModal) {
  }

  action(sequence: Sequence[]): Observable<any> {

    const returnSubject = new Subject<ActionResult>();

    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_SEQUENCE_TITLE;
    const sequenceToDelete = sequence[0];
    modalRef.componentInstance.content = SequencesModalMessages.build_confirm_delete_sequence_message(sequenceToDelete.name);
    modalRef.componentInstance.confirmMsg = CommonMessages.DELETE;
    modalRef.componentInstance.style = Style.DANGER;
    modalRef.componentInstance.icon = CommonIcons.DELETE_ICON;

    modalRef.result.then(
      () => this.iManageSequenceService.deleteSequence(sequence)
        .subscribe(() => {
            returnSubject.next({
              error: false,
              needReload: true,
              message: SequencesModalMessages.SEQUENCE_DELETE_SUCCESS,
            });
          },
          error => {
            console.error('Error calling sequence deletion : ', error);
            returnSubject.next({
              error: true,
              needReload: true,
              message: SequencesModalMessages.SEQUENCE_DELETE_FAILURE,
            });
          }),
      () => returnSubject.complete()
    ).catch(error => console.error('Caught error in popup for sequence deletion, this should not happen. Error : ', error));

    return returnSubject.asObservable();
  }
}
