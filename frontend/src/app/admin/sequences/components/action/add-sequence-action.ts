import { Injectable } from '@angular/core';
import { ActionResult, IActuator } from '../../../../ls-common';
import { Sequence } from '../../../../model/sequence/sequence';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { SequencesModalMessages } from '../sequences-modal/sequences-modal-messages';
import { SequencesModalComponent } from '../sequences-modal/sequences-modal.component';

@Injectable({
  providedIn: 'root'
})

export class AddSequenceAction implements IActuator<void> {

  messages = SequencesModalMessages;

  constructor(private modalService: NgbModal) {
  }

  action(): Observable<ActionResult> {

    const modalRef = this.modalService.open(SequencesModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });

    return from(modalRef.result).pipe(
      map((createdSequence: Sequence) => {
        return {
          needReload: true,
          message: this.messages.add_sequence_action_done(createdSequence.name),
        } as ActionResult;
      }),
      catchError((error) => {
        return error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.SEQUENCE_ADD_FAILURE,
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}
