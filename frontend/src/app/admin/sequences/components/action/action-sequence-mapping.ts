import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { Injectable } from '@angular/core';
import { SequenceActionItemFactory } from './sequence-action-item-factory';
import { Sequence } from '../../../../model/sequence/sequence';
import { SequenceAction } from './sequence-action.enum';

@Injectable({
  providedIn: 'root'
})
export class ActionSequenceMapping {

  constructor(protected sequenceActionItemFactory: SequenceActionItemFactory) {
  }

  getCommonActions(): ActionItemsSet<Sequence>[] {
    return [
    ];
  }

  getSingleActions(): ActionItemsSet<Sequence> {
    return {
      actionItems: [
        this.sequenceActionItemFactory.getActionItem(SequenceAction.EDIT_SEQUENCE),
        this.sequenceActionItemFactory.getActionItem(SequenceAction.DELETE_SEQUENCE),
      ]
    };
  }

  getOtherSingleActions(): ActionItemsSet<Sequence>[] {
    return [];
  }
}
