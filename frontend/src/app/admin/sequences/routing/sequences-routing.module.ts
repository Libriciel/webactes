import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { AdminMessages } from '../../i18n/admin-messages';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SequencesListComponent } from '../components/sequences-list/sequences-list.component';
import { SequencesListMessages } from '../components/sequences-list/sequences-list-messages';


const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_SEQUENCE_PATH,
    component: SequencesListComponent,
    data: {
      pageParameters: {
        title: SequencesListMessages.TITLE,
        breadcrumb: [{
          name: AdminMessages.PAGE_ADMIN_TITLE,
          location: RoutingPaths.ADMIN_PATH
        }
          ,
          {name: SequencesListMessages.TITLE, location: RoutingPaths.ADMIN_SEQUENCE_PATH}
        ],
        fluid: true
      } as PageParameters
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})

export class SequencesRoutingModule { }
