import { NgModule } from '@angular/core';
import { SequencesListComponent } from './components/sequences-list/sequences-list.component';
import { SequencesModalComponent } from './components/sequences-modal/sequences-modal.component';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SequencesRoutingModule } from './routing/sequences-routing.module';


@NgModule({
  declarations: [SequencesListComponent, SequencesModalComponent],
  imports: [
    CommonModule,
    SequencesRoutingModule,
    HttpClientModule,
    LibricielCommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgbModule,
  ]
})

export class SequenceModule {
}
