import { Observable } from 'rxjs';
import { ConnexionIdelibre } from '../model/connexion-idelibre';

export abstract class IManageConnexionIdelibreService {

  abstract checkIdelibreConnexion(connexionIdelibre: ConnexionIdelibre): Observable<boolean> ;

  abstract getIdelibreConnexion(): Observable<ConnexionIdelibre> ;

  abstract saveIdelibreConnexion(connexionIdelibre: ConnexionIdelibre): Observable<any> ;

}
