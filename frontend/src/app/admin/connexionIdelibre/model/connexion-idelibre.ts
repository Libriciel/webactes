import { CommonMessages } from '../../../ls-common/i18n/common-messages';

export class ConnexionIdelibre {
  username: string;
  password: string;
  url: string;
  connexion_option: string;

  constructor(data: { password: string; connexion_option: string; url: string; username: string;}) {
    this.url = data.url;
    this.password = data.password;
    this.connexion_option = data.connexion_option;
    this.username = data.username;
  }

  withProtocol(): ConnexionIdelibre {
    const clone = new ConnexionIdelibre(this);
    clone.url = CommonMessages.HTTPS_PROTOCOL + clone.url;
    return clone;
  }
}
