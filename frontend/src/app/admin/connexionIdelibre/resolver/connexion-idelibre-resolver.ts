import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { ConnexionIdelibre } from '../model/connexion-idelibre';
import { Observable } from 'rxjs';
import { IManageConnexionIdelibreService } from '../services/imanage-connexion-idelibre-service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConnexionIdelibreResolver implements Resolve<ConnexionIdelibre> {

  constructor(protected connexionIdelibreService: IManageConnexionIdelibreService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ConnexionIdelibre> {
    return this.connexionIdelibreService.getIdelibreConnexion();
  }
}
