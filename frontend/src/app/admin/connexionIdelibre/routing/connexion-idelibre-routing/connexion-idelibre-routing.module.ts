import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { AdminMessages } from '../../../i18n/admin-messages';
import { PageParameters } from '../../../../wa-common/components/page/model/page-parameters';
import { ConfigurationIdelibrePageComponent } from '../../components/configuration-idelibre-page/configuration-idelibre-page/configuration-idelibre-page.component';
import { ConnexionIdelibreResolver } from '../../resolver/connexion-idelibre-resolver';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_CONNEXION_IDELIBRE_PATH,
    component: ConfigurationIdelibrePageComponent,
    resolve: {
      connexionIdelibre: ConnexionIdelibreResolver
    },
    data: {
      pageParameters: {
        title: AdminMessages.PAGE_CONNEXION_IDELIBRE_TITLE,
        breadcrumb: [
          {
            name: AdminMessages.PAGE_ADMIN_TITLE,
            location: RoutingPaths.ADMIN_PATH
          },
          {
            name: AdminMessages.PAGE_CONNEXION_IDELIBRE_TITLE,
            location: `/${RoutingPaths.ADMIN_CONNEXION_IDELIBRE_PATH}`
          }
        ]
      } as PageParameters
    },
    children: [
      {path: '**', redirectTo: RoutingPaths.ADMIN_PATH, pathMatch: 'full'}
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ConnexionIdelibreRoutingModule {
}
