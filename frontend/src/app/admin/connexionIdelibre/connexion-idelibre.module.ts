import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationIdelibrePageComponent } from './components/configuration-idelibre-page/configuration-idelibre-page/configuration-idelibre-page.component';
import { ConnexionIdelibreRoutingModule } from './routing/connexion-idelibre-routing/connexion-idelibre-routing.module';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LsComposantsModule } from '@libriciel/ls-composants';

@NgModule({
  declarations: [ConfigurationIdelibrePageComponent],
  imports: [
    CommonModule,
    ConnexionIdelibreRoutingModule,
    LibricielCommonModule,
    FormsModule,
    ReactiveFormsModule,
    LsComposantsModule
  ]
})
export class ConnexionIdelibreModule {
}
