import { Component, OnInit } from '@angular/core';
import { AdminMessages } from '../../../../i18n/admin-messages';
import { CommonMessages, NotificationService } from '../../../../../ls-common';
import { IManageConnexionIdelibreService } from '../../../services/imanage-connexion-idelibre-service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnexionIdelibre } from '../../../model/connexion-idelibre';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RoutingPaths } from '../../../../../wa-common/routing-paths';
import { CommonStylesConstants } from '../../../../../wa-common/common-styles-constants';
import { Weight } from '../../../../../wa-common/weight.enum';

@Component({
  selector: 'wa-configuration-idelibre-page',
  templateUrl: './configuration-idelibre-page.component.html',
  styleUrls: ['./configuration-idelibre-page.component.scss']
})
export class ConfigurationIdelibrePageComponent implements OnInit {

  messages = AdminMessages;
  commonMessages = CommonMessages;
  commonStyles = CommonStylesConstants;
  weight = Weight;
  isSubmitting = false;
  isSubmitDisabled = true;
  isTesting = false;
  testSuccess = false;
  connexionForm = new FormGroup({
    url: new FormControl(null, [
      Validators.required
    ]),
    username: new FormControl(null, [
      Validators.required
    ]),
    password: new FormControl(null, []),
    connexion_option: new FormControl(null, [
      Validators.required
    ]),
  });
  testMessage = null;

  constructor(
    protected notificationService: NotificationService,
    protected connexionIdelibreService: IManageConnexionIdelibreService,
    protected router: Router,
    protected route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.connexionForm.get('url').setValue(data.connexionIdelibre.url
        .substring(this.commonMessages.HTTPS_PROTOCOL.length, data.connexionIdelibre.url.length));
      this.connexionForm.get('username').setValue(data.connexionIdelibre.username);
      this.connexionForm.get('password').setValue(data.connexionIdelibre.password);
      this.connexionForm.get('connexion_option').setValue(data.connexionIdelibre.connexion_option);
      this.isSubmitDisabled = false;
    });
  }

  save() {
    this.isSubmitting = true;
    this.connexionIdelibreService.saveIdelibreConnexion(this.getConnexionFromValues())
      .subscribe(
        () => {
          this.notificationService.showSuccess(this.messages.CONNEXION_IDELIBRE_SUCCESS_MESSAGE);
        },
        error => {
          console.error(error);
          this.notificationService.showError(this.messages.CONNEXION_IDELIBRE_ERROR_MESSAGE);
        })
      .add(
        () => {
          return this.isSubmitting = false;
        });
  }

  testConnexion() {
    this.isTesting = true;
    this.isSubmitDisabled = true;
    this.connexionIdelibreService.checkIdelibreConnexion(this.getConnexionFromValues())
      .subscribe(
        value => {
          this.testSuccess = value;
          this.testMessage = value ? this.messages.IDELIBRE_CONNEXION_OK : this.messages.IDELIBRE_CONNEXION_KO;
        }).add(
      () => {
        this.isTesting = false;
        this.isSubmitDisabled = false;
      });
  }

  getConnexionFromValues(): ConnexionIdelibre {
    console.debug(this.connexionForm.get('password').dirty);
    const connexionIdelibre: ConnexionIdelibre = new ConnexionIdelibre(
      {
        url: this.connexionForm.get('url').value,
        username: this.connexionForm.get('username').value,
        password: this.connexionForm.get('password').dirty && this.connexionForm.get('password').value === '' ? null : this.connexionForm.get('password').value,
        connexion_option: this.connexionForm.get('connexion_option').value,
      });
    return connexionIdelibre.withProtocol();
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }
}
