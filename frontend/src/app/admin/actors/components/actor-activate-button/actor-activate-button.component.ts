import { Component, Input } from '@angular/core';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { Actor, ActorsService } from '../../../../core';


@Component({
  selector: 'wa-actor-activate-button',
  templateUrl: './actor-activate-button.component.html'
})
export class ActorActiveButtonComponent {
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  constructor(
    private notificationService: NotificationService,
    private actorsService: ActorsService
  ) {
  }

  @Input() actor: Actor;

  onToggleActivation(actor: Actor, activate: boolean) {
    if (activate) {
      this.actorsService.activate(actor.id).subscribe({
        complete: () => {
          this.notificationService.showSuccess('Activation de l\'acteur.');
        },
        error: () => {
          this.notificationService.showError('Erreur lors de l\'activation de l\'acteur.');
        }
      });
      return;
    }

    this.actorsService.deactivate(actor.id).subscribe({
      complete: () => {
        this.notificationService.showSuccess('Désactivation de l\'acteur.');
      },
      error: () => {
        this.notificationService.showError('Erreur lors de la désactivation de l\'acteur.');
      }
    });
  }

}
