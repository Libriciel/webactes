import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonIcons, CommonMessages } from '../../../../ls-common';
import { Actor, Actorgroup, ActorsgroupsService, ActorsService, ValidationService } from '../../../../core';
import { ActorsMessages } from '../../shared/actors-messages';

@Component({
  selector: 'wa-actor-modal',
  templateUrl: 'actor-modal.component.html'
})
export class ActorModalComponent implements OnInit {

  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  messages = ActorsMessages;
  actorForm: UntypedFormGroup;
  @Input()
  actor: Actor = {
    active: false,
    rank: '',
    cellphone: '',
    email: '',
    firstname: '',
    lastname: '',
    civility: '',
    actor_groups: [],
    address: '',
    address_supplement: '',
    city: '',
    full_name: '',
    id: null,
    name: '',
    order: 0,
    phone: '',
    post_code: null,
    title: '',
    votes: []
  };
  optionActorGroups: Actorgroup[];
  isSubmitting = false;
  isSubmitDisabled = true;

  numPage = 1;
  limit = 200;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private activeModal: NgbActiveModal,
    private actorsService: ActorsService,
    private actorsGroupsService: ActorsgroupsService,
    protected notificationService: NotificationService
  ) {
  }

  ngOnInit(): void {
    this.actorForm = this._formBuilder.group({
      civility: [this.actor.civility, [Validators.required, Validators.maxLength(50)]],
      lastname: [this.actor.lastname, [Validators.required, Validators.maxLength(50)]],
      firstname: [this.actor.firstname, [Validators.required, Validators.maxLength(50)]],
      email: [this.actor.email, [Validators.required, ValidationService.emailValidator, Validators.maxLength(100)]],
      cellphone: [this.actor.cellphone, [Validators.maxLength(10), ValidationService.cellPhoneValidator]],
      actor_groups: [this.actor.actor_groups],
      rank: [this.actor.rank, [Validators.min(1)]],
      title: [this.actor.title, [Validators.maxLength(250)]],
    });
    this.actorsGroupsService.getAllWithPagination(this.numPage, this.limit)
      .subscribe(data => this.optionActorGroups = data.actorGroups.filter(group => group.active));
  }

  onSubmit() {
    this.isSubmitting = true;
    // update the model
    this.updateActor(this.actorForm.value);
    this.actorsService.save(this.actor)
      .subscribe(
        actor => {
          this.notificationService.showSuccess(
            this.actor.id
              ? this.messages.updateActorActionDone(actor.full_name)
              : this.messages.addActorActionDone(actor.full_name));
          this.activeModal.close();
        },
        error => {
          console.error(error);
          if (error.errors.structure_id._isUnique) {
            this.actorForm.controls['email'].setErrors({duplicate: true});
          } else {
            this.notificationService.showError(this.actor.id
              ? this.messages.ACTOR_UPDATE_ERROR_MESSAGE
              : this.messages.ACTOR_CREATION_ERROR_MESSAGE);
          }
        },
      )
      .add(() => this.isSubmitting = false);
  }

  updateActor(values: Object) {
    Object.assign(this.actor, values);
  }
}
