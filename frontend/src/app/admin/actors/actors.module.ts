import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared';
import { ActorRoutingModule } from './actor-routing.module';
import { ActorsComponent } from './actors.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActorModalComponent } from './components/actor-modal/actor-modal.component';
import { JoinActorGroupsPipe } from './shared/actors-join.pipe';
import { ActorActiveButtonComponent } from './components/actor-activate-button/actor-activate-button.component';


@NgModule({
  declarations: [
    ActorsComponent,
    ActorActiveButtonComponent,
    ActorModalComponent,
    JoinActorGroupsPipe
  ],
  imports: [
    SharedModule,
    ActorRoutingModule,
  ],
  providers: [NgbActiveModal],
  exports: [
    JoinActorGroupsPipe
  ]
})
export class ActorsModule {
}
