import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Actor, ActorListConfig, ActorsService, Pagination, } from '../../core';
import { ActionItemsSet } from '../../ls-common/model/action-items-set';
import { CommonIcons, CommonMessages } from '../../ls-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActorList } from './shared/models/actor-list.model';
import { ActorModalComponent } from './components/actor-modal/actor-modal.component';
import { ActorsMessages } from './shared/actors-messages';
import { ActorsActionMapping } from './shared/actions/actors-action-mapping';

@Component({
  selector: 'wa-actors',
  templateUrl: './actors.component.html',
  styleUrls: ['./actors.component.scss']
})
export class ActorsComponent implements OnInit {
  isSubmitting = false;
  messages = ActorsMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  commonActions: ActionItemsSet<Actor>;
  singleActions: ActionItemsSet<Actor>;
  otherSingleActions: ActionItemsSet<Actor>;

  pagination: Pagination = {page: 1, count: 1, perPage: 1};

  query: ActorListConfig;
  actors: ActorList[];
  loading = false;
  currentPage = 1;
  limit: number;
  listConfig: ActorListConfig = {
    type: 'all',
    filters: {}
  };

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private actorsService: ActorsService,
    protected actorsActionStateMap: ActorsActionMapping,
  ) {
  }

  config(config: ActorListConfig) {
    if (config) {
      this.limit = 10;
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  ngOnInit() {
    this.singleActions = this.actorsActionStateMap.getSingleActions();
    this.otherSingleActions = this.actorsActionStateMap.getOtherSingleActions();
    this.commonActions = this.actorsActionStateMap.getCommonActions();
    this.config(this.listConfig);
  }

  goBack() {
    this.router.navigateByUrl('administration').then();
  }

  setToPage(pageNumber: number) {
    this.currentPage = pageNumber;
    this.runQuery();
  }

  runQuery() {
    this.actors = null;
    this.loading = true;

    // Create limit and offset filter (if necessary)
    if (this.limit) {
      this.query.filters.limit = this.limit;
      this.query.filters.offset = (this.limit * (this.currentPage - 1));
      this.query.filters.page = this.currentPage;
    }

    this.actorsService.query(this.query)
      .subscribe(data => {
        this.loading = false;
        this.actors = data.actors as ActorList[];
        this.pagination = data.pagination;
      });
  }

  applyFilter(event: any) {
    this.query.filters.searchedText = event.trim();
    this.reload();
  }


  hasFilter(): boolean {
    return this.query.filters.searchedText && this.query.filters.searchedText !== '';
  }

  createActor() {
    const modalRef = this.modalService.open(ActorModalComponent, {
      backdrop: 'static',
      centered: true,
      size: 'lg'
    });
    modalRef.result.then(() => this.reload());
  }

  gotoPage(numPage) {
    this.setToPage(numPage);
  }

  reload() {
    this.setToPage(1);
  }
}
