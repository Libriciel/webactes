import { Pipe, PipeTransform } from '@angular/core';
import { Actorgroup } from '../../../core';

/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'joinActorGroups'})
export class JoinActorGroupsPipe implements PipeTransform {
  // transform(value: number, exponent?: number): number {
  //   return Math.pow(value, isNaN(exponent) ? 1 : exponent);
  // }
  transform(actor_groups: Array<Actorgroup>, separator = ', '): string {
    return actor_groups.map(actor_group => actor_group.name).join(separator);
  }
}
