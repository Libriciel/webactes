import { Actor } from '../../../../core';
import { HasActions } from '../../../../ls-common/model/has-actions';

export interface ActorList extends Actor, HasActions {
}
