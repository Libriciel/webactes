export enum ActorListActionEnum {
  canRemove = 'can_remove',
  canModify = 'can_modify',
}
