export class ActorsMessages {
  static HEADER_RANK = `Position`;
  static HEADER_NAME = `Nom de l'acteur`;
  static HEADER_ACTORGROUP = `Groupes`;

  static MODAL_TITLE_CREATE = `Création d'un acteur`;
  static MODAL_TITLE_UPDATE = `Modification d'un acteur`;
  static FORM_FIELD_ACTOR_GROUPS_LABEL = `Groupe d'acteurs`;
  static FORM_FIELD_ACTOR_GROUPS_LABEL_PLACEHOLDER = `Groupe d'acteurs de l'acteur`;
  static FORM_FIELD_RANK_LABEL = `Ordre dans le conseil`;
  static FORM_FIELD_RANK_LABEL_PLACEHOLDER = `Ordre dans le conseil de l'acteur`;
  static FORM_FIELD_CIVILITY_LABEL = `Civilité`;
  static FORM_FIELD_CIVILITY_LABEL_PLACEHOLDER = `Civilité de l'acteur`;
  static FORM_FIELD_LASTNAME_LABEL = `Nom`;
  static FORM_FIELD_LASTNAME_LABEL_PLACEHOLDER = `Nom de l'acteur`;
  static FORM_FIELD_FIRSTNAME_LABEL = `Prénom`;
  static FORM_FIELD_FIRSTNAME_LABEL_PLACEHOLDER = `Prénom de l'acteur`;
  static FORM_FIELD_EMAIL_LABEL = `Adresse e-mail`;
  static FORM_FIELD_EMAIL_LABEL_PLACEHOLDER = `Adresse e-mail de l'acteur`;
  static FORM_FIELD_CELLPHONE_LABEL = `Téléphone mobile`;
  static FORM_FIELD_CELLPHONE_LABEL_PLACEHOLDER = `Téléphone mobile de l'acteur`;
  static FORM_FIELD_TITLE_LABEL = `Titre`;
  static FORM_FIELD_TITLE_LABEL_PLACEHOLDER = `Titre de l'acteur`;
  static BUTTON_ADD = `Créer un acteur`;

  static DELETE_FAILED_TOASTR_TXT = `La suppression de l\'acteur a échoué`;
  static CONFIRM_DELETE_TITLE = `Suppression de l'acteur`;
  static CONFIRM_DELETE_BUTTON = `Supprimer`;
  static ACTOR_CREATION_ERROR_MESSAGE = `Erreur lors de la création de l'acteur.`;
  static ACTOR_UPDATE_ERROR_MESSAGE = `Erreur lors de la modification de l'acteur.`;
  static ACTOR_EMAIL_ALREADY_USED_ERROR_MESSAGE = 'Cette adresse e-mail est déjà utilisée dans webactes';

  static sameName(name: string) {
    return `Un acteur avec le nom ${name} est déjà présent.<br>
        Veuillez renseigner un autre nom pour l'acteur.`;
  }

  static updateActorActionDone(nameActor: string) {
    return `Les modifications de l'acteur "${nameActor}" ont été enregistrées avec succès.`;
  }

  static addActorActionDone(nameActor: string) {
    return `L'acteur "${nameActor}" a été créé avec succès.`;
  }

  static deleteActorConfirm(ActorName: string): string {
    return `Voulez-vous vraiment supprimer l'acteur "${ActorName}"?`;
  }

  static deleteActorActionDone(ActorName: string): string {
    return `L\'acteur "${ActorName}" a été supprimé avec succès.`;
  }

}
