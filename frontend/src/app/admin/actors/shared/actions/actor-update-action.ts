import { from, Observable, of } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionResult, IActuator, NotificationService } from '../../../../ls-common';
import { Actor } from '../../../../core';
import { ActorsMessages } from '../actors-messages';
import { ActorModalComponent } from '../../components/actor-modal/actor-modal.component';
import { catchError, map } from 'rxjs/operators';

export class ActorUpdateAction implements IActuator<Actor> {

  messages = ActorsMessages;

  constructor(
    protected modalService: NgbModal,
    protected notificationService: NotificationService
  ) {
  }

  action(Actors: Actor[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(ActorModalComponent, {
      backdrop: 'static',
      centered: true,
      size: 'lg'
    });

    modalRef.componentInstance.actor = Actors[0];

    return from(modalRef.result).pipe(
      map((updateOrSavedActor: Actor) => {
        return {
          needReload: true,
          message: updateOrSavedActor.name,
        } as ActionResult;
      }),
      catchError((error) => {
        return error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: error.message,
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}
