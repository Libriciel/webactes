import { Observable, Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Style } from '../../../../wa-common';
import { ActionResult, ConfirmPopupComponent, IActuator, NotificationService } from '../../../../ls-common';
import { Actor, ActorsService } from '../../../../core';
import { ActorsMessages } from '../actors-messages';

export class ActorDeleteAction implements IActuator<Actor> {

  messages = ActorsMessages;

  constructor(
    protected actorsService: ActorsService,
    protected modalService: NgbModal,
    protected notificationService: NotificationService
  ) {
  }

  action(Actors: Actor[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    const returnSubject = new Subject<ActionResult>();

    modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_TITLE;
    const ActorsNames = Actors.map(actor => actor.name).join(', ');
    modalRef.componentInstance.content = this.messages.deleteActorConfirm(ActorsNames);

    modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_DELETE_BUTTON;
    modalRef.componentInstance.style = Style.DANGER;

    modalRef.result
      .then(
        () => {
          this.actorsService.destroy(Actors[0].id).subscribe({
            complete: () => {
              returnSubject.next({
                error: false,
                needReload: true,
                message: this.messages.deleteActorActionDone(Actors[0].name)
              });
            },
            error: () => {
              console.error('Error calling actor deletion');
              returnSubject.next({
                error: true,
                needReload: true,
                message: this.messages.DELETE_FAILED_TOASTR_TXT
              });
            }
          });
        },
        () => returnSubject.complete()
      )
      .catch(error => {
        console.error('Caught error in popup for circuit deletion : ', error);
      });

    return returnSubject.asObservable();

  }
}
