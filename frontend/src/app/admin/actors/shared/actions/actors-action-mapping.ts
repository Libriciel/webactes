import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { Injectable } from '@angular/core';
import { ActionItem, CommonIcons, CommonMessages, NotificationService, } from '../../../../ls-common';
import { ActorsService } from '../../../../core';
import { Style } from '../../../../wa-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActorsMessages } from '../actors-messages';
import { ActorDeleteAction } from './actor-delete-action';
import { ActorUpdateAction } from './actor-update-action';
import { ActorDeleteActionValidator } from './actor-delete-action-validator';
import { ActorList } from '../models/actor-list.model';
import { ActorModifyActionValidator } from './actor-modify-action-validator';

@Injectable({
  providedIn: 'root'
})
export class ActorsActionMapping {

  messages = ActorsMessages;

  constructor(
    protected modalService: NgbModal,
    protected actorsService: ActorsService,
    protected notificationService: NotificationService,
  ) {
  }

  getCommonActions(): ActionItemsSet<ActorList> {
    return null;
  }

  getOtherSingleActions(): ActionItemsSet<ActorList> {
    return null;
  }

  getSingleActions(): ActionItemsSet<ActorList> {
    return {
      actionItems: [
        new ActionItem({
          name: CommonMessages.MODIFY,
          icon: CommonIcons.EDIT_ICON,
          actuator: new ActorUpdateAction(
            this.modalService,
            this.notificationService
          ),
          actionValidator: new ActorModifyActionValidator(),
        }),
        new ActionItem({
          name: CommonMessages.DELETE,
          icon: CommonIcons.DELETE_ICON,
          style: Style.DANGER,
          actuator: new ActorDeleteAction(
            this.actorsService,
            this.modalService,
            this.notificationService
          ),
          actionValidator: new ActorDeleteActionValidator(),
        })
      ]
    };
  }
}
