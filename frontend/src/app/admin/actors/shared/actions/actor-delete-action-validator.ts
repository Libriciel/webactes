import { ActorList } from '../models/actor-list.model';
import { ActorListActionEnum } from '../models/actor-list-action-enum';
import { AvailableActionsValidator } from '../../../../ls-common/components/tables/validator/available-actions-validator';

export class ActorDeleteActionValidator extends AvailableActionsValidator<ActorList> {

  constructor() {
    super(ActorListActionEnum.canRemove);
  }
}
