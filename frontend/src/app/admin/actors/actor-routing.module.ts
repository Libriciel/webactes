import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageParameters } from 'src/app/wa-common/components/page/model/page-parameters';
import { ActorsComponent } from './actors.component';

const routes: Routes = [
  {
    path: 'administration/acteurs',
    component: ActorsComponent,
    data: {
      pageParameters: {
        title: `Acteurs`,
        breadcrumb: [
          {
            name: `Administration`,
            location: `administration`
          },
          {
            name: `Acteurs`, location: 'administration/acteurs'
          }
        ],
        fluid: true
      } as PageParameters
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActorRoutingModule {
}
