import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListRoleComponent } from './list-role/list-role.component';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { RoleRoutingModule } from './role-routing/role-routing.module';
import { ShowRoleComponent } from './show-role/show-role.component';
import { TreeModule } from '@circlon/angular-tree-component';

@NgModule({
  declarations: [ListRoleComponent, ShowRoleComponent],
  imports: [
    CommonModule,
    LibricielCommonModule,
    TreeModule,
    RoleRoutingModule,

  ]
})
export class RoleModule {
}
