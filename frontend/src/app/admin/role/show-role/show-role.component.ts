import { Component, OnInit, ViewChild } from '@angular/core';
import { IManageRoleService } from '../services/IManageRoleService';
import { ActivatedRoute } from '@angular/router';
import { KEYS, TREE_ACTIONS, TreeComponent, TreeModel } from '@circlon/angular-tree-component';

@Component({
  selector: 'wa-show-role',
  templateUrl: './show-role.component.html',
  styleUrls: ['./show-role.component.scss']
})
export class ShowRoleComponent implements OnInit {

  @ViewChild('roleTree', {static: true}) treeComponent: TreeComponent;
  nodes = [];
  options = {
    displayField: 'text',
    useCheckbox: true,
    idField: 'value',
    actionMapping: {
      mouse: {
        dblClick: (tree, node, $event) => {
          if (node.hasChildren) {
            TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);
          }
        }
      },
      keys: {
        [KEYS.ENTER]: (tree, node) => node.expandAll()
      },
    },
  };

  constructor(
    protected iManageRoleService: IManageRoleService,
    protected route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.iManageRoleService.getPermissions(id).subscribe(data => {
      this.nodes = data.role.permissions;
      if (!this.treeComponent) {
        return;
      }
      const treeModel: TreeModel = this.treeComponent.treeModel;
      // hack : node is empty without setTimeout
      setTimeout(() =>
        treeModel.doForAll(node => {
          if (node.data.checked) {
            treeModel.getNodeById(node.data.value).setIsSelected(true);
          }
        }), 0);
    });
  }

}
