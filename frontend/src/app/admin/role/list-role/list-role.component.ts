import { Component, OnInit } from '@angular/core';
import { Role } from '../../../model/role';
import { IManageRoleService } from '../services/IManageRoleService';
import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { Router } from '@angular/router';
import { ActionItem } from '../../../ls-common/model/action-item';
import { of } from 'rxjs';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { CommonMessages } from '../../../ls-common/i18n/common-messages';
import { IActuator } from '../../../ls-common/components/tables/actuator/i-actuator';

@Component({
  selector: 'wa-list-role',
  templateUrl: './list-role.component.html',
  styleUrls: ['./list-role.component.scss']
})
export class ListRoleComponent implements OnInit {

  roles: Role[];
  commonMessages = CommonMessages;
  singleActions: ActionItemsSet<Role> = {
    actionItems: [new ActionItem({
      name: 'foo',
      actuator: new class implements IActuator<Role> {
        constructor(protected router: Router) {
        }

        action(roles: Role[]) {
          this.router.navigateByUrl(`/show-role/${roles[0].id}`).then();
          return of({});
        }
      }(this.router)
    })]
  };

  constructor(
    protected iManageRoleService: IManageRoleService,
    protected router: Router,
  ) {
  }

  ngOnInit() {
    this.iManageRoleService.getRoles().subscribe(roles => {
      this.roles = roles.roles;
    });
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

}
