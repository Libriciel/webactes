import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { ListRoleComponent } from '../list-role/list-role.component';
import { ShowRoleComponent } from '../show-role/show-role.component';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { RoleMessages } from '../i18n/role-messages';
import { AdminMessages } from '../../i18n/admin-messages';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_ROLE_LIST_PATH,
    component: ListRoleComponent,
    data: {
      pageParameters: {
        title: RoleMessages.ADMIN_ROLE_LIST_TITLE,
        breadcrumb: [
          {
            name: AdminMessages.PAGE_ADMIN_TITLE,
            location: RoutingPaths.ADMIN_PATH
          },
          {name: RoleMessages.ADMIN_ROLE_LIST_TITLE, location: RoutingPaths.ADMIN_ROLE_LIST_PATH}
        ],
        fluid: true
      } as PageParameters
    }
  },
  {
    path: RoutingPaths.ADMIN_ROLE_DETAIL_PATH,
    component: ShowRoleComponent,
    data: {
      pageParameters: {
        title: RoleMessages.DETAIL,
        breadcrumb: [
          {name: RoleMessages.DETAIL, location: RoutingPaths.ADMIN_ROLE_DETAIL_PATH}
        ],
        fluid: true
      } as PageParameters
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class RoleRoutingModule {
}
