import { Observable } from 'rxjs';
import { Role } from '../../../model/role';

export abstract class IManageRoleService {

  abstract getRoles(): Observable<{ roles: Role[] }>;

  abstract getPermissions(id): Observable<any>;

}

