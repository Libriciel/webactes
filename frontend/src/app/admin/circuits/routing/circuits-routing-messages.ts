export class CircuitsRoutingMessages {
  static PAGE_CIRCUITS_TITLE = 'Circuits';
  static PAGE_CIRCUITS_CREATION_TITLE = 'Création de circuit';
  static PAGE_CIRCUITS_MODIFICATION_TITLE = 'Modification de circuit';
  static PAGE_CIRCUITS_VISUALISATION_TITLE = 'Visualisation de circuit';
}
