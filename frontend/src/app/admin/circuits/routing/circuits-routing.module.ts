import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { CircuitsRoutingMessages } from './circuits-routing-messages';
import { CircuitCreationComponent } from '../components/circuit-creation/circuit-creation.component';
import { CircuitsListComponent } from '../components/circuits-list/circuits-list.component';
import { AdminMessages } from '../../i18n/admin-messages';
import { CircuitModificationComponent } from '../components/circuit-modification/circuit-modification.component';
import { CircuitVisualisationComponent } from '../components/circuit-visualisation/circuit-visualisation.component';
import { PageParametersResolver } from './resolvers/page-parameter-resolver';
import { CircuitResolver } from './resolvers/circuit-resolver';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_CIRCUIT_PATH,
    component: CircuitsListComponent,
    data: {
      pageParameters: {
        title: CircuitsRoutingMessages.PAGE_CIRCUITS_TITLE,
        breadcrumb: [
          {
            name: AdminMessages.PAGE_ADMIN_TITLE,
            location: RoutingPaths.ADMIN_PATH
          }, {
            name: CircuitsRoutingMessages.PAGE_CIRCUITS_TITLE,
            location: '/' + RoutingPaths.ADMIN_CIRCUIT_PATH
          }
        ]
      }
    },
    children: [
      {path: '', redirectTo: RoutingPaths.ADMIN_CIRCUIT_PATH, pathMatch: 'full'},
      {path: '**', redirectTo: RoutingPaths.ADMIN_CIRCUIT_PATH, pathMatch: 'full'}
    ]
  }, {
    path: RoutingPaths.ADMIN_CIRCUIT_CREATION_PATH,
    component: CircuitCreationComponent,
    data: {
      pageParameters: {
        title: CircuitsRoutingMessages.PAGE_CIRCUITS_CREATION_TITLE,
        breadcrumb: [
          {
            name: AdminMessages.PAGE_ADMIN_TITLE,
            location: RoutingPaths.ADMIN_PATH
          }, {
            name: CircuitsRoutingMessages.PAGE_CIRCUITS_TITLE,
            location: `/${RoutingPaths.ADMIN_CIRCUIT_PATH}`
          }, {
            name: CircuitsRoutingMessages.PAGE_CIRCUITS_CREATION_TITLE,
            location: `/${RoutingPaths.ADMIN_CIRCUIT_CREATION_PATH}`
          }
        ]
      }
    }
  }, {
    path: RoutingPaths.buildAdminCircuitModificationPath(':circuitId'),
    component: CircuitModificationComponent,
    resolve: {
      pageParameters: PageParametersResolver,
      circuit: CircuitResolver
    }
  }, {
    path: RoutingPaths.buildAdminCircuitVisualisationPath(':circuitId'),
    component: CircuitVisualisationComponent,
    resolve: {
      pageParameters: PageParametersResolver,
      circuit: CircuitResolver
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class CircuitsRoutingModule {
}
