import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CircuitsRoutingMessages } from '../circuits-routing-messages';
import { AdminMessages } from 'src/app/admin/i18n/admin-messages';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { PageParameters } from 'src/app/wa-common/components/page/model/page-parameters';

@Injectable({
  providedIn: 'root'
})
export class PageParametersResolver implements Resolve<PageParameters> {

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<PageParameters> | Promise<PageParameters> | PageParameters {
    const routeParameterMap = new Map<string, PageParameters>();
    routeParameterMap.set(RoutingPaths.buildAdminCircuitVisualisationPath(':circuitId'), {
      title: CircuitsRoutingMessages.PAGE_CIRCUITS_VISUALISATION_TITLE,
      breadcrumb: [
        {
          name: AdminMessages.PAGE_ADMIN_TITLE,
          location: RoutingPaths.ADMIN_PATH
        }, {
          name: CircuitsRoutingMessages.PAGE_CIRCUITS_TITLE,
          location: `/${RoutingPaths.ADMIN_CIRCUIT_PATH}`
        }, {
          name: CircuitsRoutingMessages.PAGE_CIRCUITS_VISUALISATION_TITLE,
          location: `/${RoutingPaths.buildAdminCircuitVisualisationPath(route.params['circuitId'])}`
        }
      ]
    });

    routeParameterMap.set(RoutingPaths.buildAdminCircuitModificationPath(':circuitId'), {
      title: CircuitsRoutingMessages.PAGE_CIRCUITS_MODIFICATION_TITLE,
      breadcrumb: [
        {
          name: AdminMessages.PAGE_ADMIN_TITLE,
          location: RoutingPaths.ADMIN_PATH
        }, {
          name: CircuitsRoutingMessages.PAGE_CIRCUITS_TITLE,
          location: `/${RoutingPaths.ADMIN_CIRCUIT_PATH}`
        }, {
          name: CircuitsRoutingMessages.PAGE_CIRCUITS_MODIFICATION_TITLE,
          location: `/${RoutingPaths.buildAdminCircuitModificationPath(':circuitId')}`
        }
      ]
    });

    return routeParameterMap.get(route.routeConfig.path);
  }

}
