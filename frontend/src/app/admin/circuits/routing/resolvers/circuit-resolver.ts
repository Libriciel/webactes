import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { IManageCircuitService } from '../../services/IManageCircuitService';
import { Circuit } from 'src/app/model/circuit/circuit';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CircuitResolver implements Resolve<Circuit> {
  constructor(protected circuitService: IManageCircuitService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Circuit> | Promise<Circuit> | Circuit {
    return this.circuitService.getCircuit(+route.paramMap.get('circuitId')).pipe(
      catchError(err => {
        console.warn('Error retrieving circuit ' + route.paramMap.get('circuitId') + ' : ', err);
        return throwError(`Error retrieving circuit ${route.paramMap.get('circuitId')}`);
      })
    );
  }
}
