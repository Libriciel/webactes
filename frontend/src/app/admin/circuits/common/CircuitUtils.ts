import { CircuitConstants } from './CircuitConstants';
import { Step } from 'src/app/model/circuit/step';
import { StepType_t } from 'src/app/model/circuit/step-type';

export class CircuitUtils {

  static getStepIconClass(step: Step): string {
    let result: string;
    switch (step.type.id) {
      case StepType_t.Simple: {
        result = CircuitConstants.simple_step_icn_class;
        break;
      }
      case StepType_t.Concurrent: {
        result = CircuitConstants.concurrent_step_icn_class;
        break;
      }
      case StepType_t.Collaborative: {
        result = CircuitConstants.collaborative_step_icn_class;
        break;
      }

      default: {
        result = CircuitConstants.simple_step_icn_class;
      }
    }
    return result;
  }
}
