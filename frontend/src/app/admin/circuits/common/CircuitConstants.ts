export class CircuitConstants {
  static simple_step_icn_class = 'wa-step-icn-simple';
  static concurrent_step_icn_class = 'wa-step-icn-concurrente';
  static collaborative_step_icn_class = 'wa-step-icn-collaborative';

  static CIRCUIT_START_ICON = 'far fa-flag';
  static CIRCUIT_END_ICON = 'fas fa-flag-checkered';
  static CIRCUIT_COMMENT_ICON = 'far fa-comment-dots';
}
