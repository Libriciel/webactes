import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CircuitsListComponent } from './components/circuits-list/circuits-list.component';
import { LibricielCommonModule } from 'src/app/ls-common/libriciel-common.module';
import { WaCommonModule } from 'src/app/wa-common/wa-common.module';
import { CircuitsRoutingModule } from './routing/circuits-routing.module';
import { CircuitCreationComponent } from './components/circuit-creation/circuit-creation.component';
import { RouterModule } from '@angular/router';
import { StepEditorComponent } from './components/step-editor/step-editor.component';
import { CircuitModificationComponent } from './components/circuit-modification/circuit-modification.component';
import { CircuitDisplayComponent } from './components/circuit-display/circuit-display.component';
import { CircuitVisualisationComponent } from './components/circuit-visualisation/circuit-visualisation.component';
import { CircuitCommentComponent } from './components/circuit-comment/circuit-comment.component';

@NgModule({
  declarations: [
    CircuitsListComponent,
    CircuitCreationComponent,
    StepEditorComponent,
    CircuitModificationComponent,
    CircuitDisplayComponent,
    CircuitVisualisationComponent,
    CircuitCommentComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    BrowserAnimationsModule,
    LibricielCommonModule,
    CircuitsRoutingModule,
    WaCommonModule,
    RouterModule
  ],
    exports: [
        CircuitsListComponent,
        CircuitDisplayComponent,
        CircuitCommentComponent
    ]
})
export class CircuitsModule {
}
