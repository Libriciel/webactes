import { Observable } from 'rxjs';
import { Circuit } from '../../../model/circuit/circuit';
import { Pagination } from 'src/app/model/pagination';
import { FilterType } from 'src/app/model/util/filter-type';

export abstract class IManageCircuitService {
  abstract getAllCircuits(numPage?: number, filters?: FilterType): Observable<{ circuits: Circuit[], pagination: Pagination }>;

  abstract getAllActiveCircuits(): Observable<{ circuits: Circuit[] }>;

  abstract getCircuit(id: number): Observable<Circuit>;

  abstract deleteCircuits(circuits: Circuit[]): Observable<boolean>;

  abstract switchCircuitActiveState(circuits: Circuit, active: boolean): Observable<{success: boolean, circuit: Circuit}>;
  }
