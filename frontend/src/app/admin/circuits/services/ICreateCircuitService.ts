import { Observable } from 'rxjs';
import { Circuit } from 'src/app/model/circuit/circuit';

export abstract class ICreateCircuitService {

  abstract addCircuit(circuit: Circuit): Observable<Circuit>;

  abstract updateCircuit(id: number, circuit: Circuit): Observable<Circuit>;
}
