import { Observable } from 'rxjs';
import { User } from 'src/app/model/user/user';

export abstract class ICreateStepService {

  abstract getValidators(): Observable<{ users: User[] }>;
}
