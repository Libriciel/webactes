import { ActionItem } from '../../../../../ls-common/model/action-item';
import { CommonIcons } from '../../../../../ls-common/icons/common-icons';
import { CircuitAction } from './circuit-action.enum';
import { ShowCircuitAction } from './show-circuit-action';
import { Circuit } from '../../../../../model/circuit/circuit';
import { Style } from '../../../../../wa-common/style.enum';
import { DeleteCircuitsAction } from './delete-circuits-action';
import { DeleteCircuitActionValidator } from './delete-circuit-action-validator';
import { EditCircuitAction } from './edit-circuit-action';
import { EditCircuitActionValidator } from './edit-circuit-action-validator';
import { Injectable } from '@angular/core';
import { IManageCircuitService } from '../../../services/IManageCircuitService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationService } from '../../../../../ls-common/services/notification.service';
import { Router } from '@angular/router';
import { CircuitsListMessages } from '../i18n/circuits-list-messages';

@Injectable({
  providedIn: 'root'
})
export class CircuitActionItemFactory {
  constructor(
    protected circuitService: IManageCircuitService,
    protected modalService: NgbModal,
    protected notificationService: NotificationService,
    protected router: Router,
  ) {
  }

  getActionItem(action: CircuitAction): ActionItem<Circuit> {
    switch (action) {
      case CircuitAction.DELETE_CIRCUIT:
        return new ActionItem({
          name: CircuitsListMessages.DELETE_CIRCUITS_ACTION_NAME,
          icon: CommonIcons.DELETE_ICON,
          style: Style.DANGER,
          actuator: new DeleteCircuitsAction(this.circuitService, this.modalService, this.notificationService),
          actionValidator: new DeleteCircuitActionValidator(),
        });

      case CircuitAction.EDIT_CIRCUIT:
        return new ActionItem({
          name: CircuitsListMessages.EDIT_ACTION_NAME,
          icon: CommonIcons.EDIT_ICON,
          actuator: new EditCircuitAction(this.router),
          actionValidator: new EditCircuitActionValidator(),
        });

      case CircuitAction.SHOW_CIRCUIT:
        return new ActionItem({
          name: CircuitsListMessages.SHOW_ACTION_NAME,
          icon: CommonIcons.SHOW_ICON,
          actuator: new ShowCircuitAction(this.router),
        });

      default:
        throw new Error(`unimplemented action ${action}`);
    }
  }
}
