import { Observable, Subject } from 'rxjs';
import { Circuit } from '../../../../../model/circuit/circuit';
import { IManageCircuitService } from '../../../services/IManageCircuitService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmPopupComponent } from 'src/app/ls-common/components/confirm-popup/components/confirm-popup.component';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { ActionResult } from 'src/app/ls-common/components/tables/actuator/action-result';
import { Style } from '../../../../../wa-common/style.enum';
import { CircuitsListMessages } from '../i18n/circuits-list-messages';
import { IActuator } from '../../../../../ls-common/components/tables/actuator/i-actuator';

export class DeleteCircuitsAction implements IActuator<Circuit> {

  messages = CircuitsListMessages;

  constructor(
    protected circuitService: IManageCircuitService,
    protected modalService: NgbModal,
    protected notificationService: NotificationService
  ) {
  }

  action(circuits: Circuit[]): Observable<any> {
    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    const returnSubject = new Subject<ActionResult>();

    modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_CIRCUIT_TITLE;
    const circuitsNames = circuits.map(circuit => circuit.name).join(', ');
    modalRef.componentInstance.content = this.messages.buildConfirmDeleteCircuitMsg(circuitsNames);
    modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_DELETE_CIRCUIT_BUTTON;
    modalRef.componentInstance.style = Style.DANGER;

    modalRef.result
      .then(
        () => {
          this.circuitService.deleteCircuits(circuits)
            .subscribe(() =>
                returnSubject.next({
                  error: false,
                  needReload: true,
                  message: this.messages.CIRCUIT_DELETED_CONFIRMATION_TOASTR_TXT
                }),
              error => {
                console.error(error);
                console.error('Error calling circuit deletion');
                returnSubject.next({
                  error: true,
                  needReload: true,
                  message: this.messages.CIRCUIT_DELETE_FAILED_TOASTR_TXT
                });
              });
        },
        () => returnSubject.complete()
      )
      .catch(error => {
        console.error('Caught error in popup for circuit deletion : ', error);
      });

    return returnSubject.asObservable();
  }
}
