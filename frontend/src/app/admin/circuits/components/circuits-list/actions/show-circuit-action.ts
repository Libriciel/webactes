import { Observable, of } from 'rxjs';
import { Circuit } from '../../../../../model/circuit/circuit';
import { Router } from '@angular/router';
import { RoutingPaths } from '../../../../../wa-common/routing-paths';
import { IActuator } from '../../../../../ls-common/components/tables/actuator/i-actuator';

export class ShowCircuitAction implements IActuator<Circuit> {
  constructor(protected  router: Router) {
  }

  action(circuits: Circuit[]): Observable<any> {
    return !circuits || circuits.length === 0
      ? of({})
      : of(this.router.navigateByUrl(RoutingPaths.buildAdminCircuitVisualisationPath('' + circuits[0].id)));
  }
}
