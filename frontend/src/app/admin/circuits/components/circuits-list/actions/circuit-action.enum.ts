export enum CircuitAction {
  DELETE_CIRCUIT,
  EDIT_CIRCUIT,
  SHOW_CIRCUIT,
}
