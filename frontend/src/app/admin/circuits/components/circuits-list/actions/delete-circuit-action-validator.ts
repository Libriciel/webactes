import { CircuitRight } from '../../../../../model/circuit/circuit-right.enum';
import { AvailableActionsBasedValidator } from '../../../../../ls-common/components/tables/validator/available-actions-based-validator';
import { Circuit } from '../../../../../model/circuit/circuit';

export class DeleteCircuitActionValidator extends AvailableActionsBasedValidator<Circuit> {

  constructor() {
    super(CircuitRight.isDeletable);
  }
}
