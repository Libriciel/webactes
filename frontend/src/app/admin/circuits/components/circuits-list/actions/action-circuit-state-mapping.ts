import { ActionItemsSet } from '../../../../../ls-common/model/action-items-set';
import { Circuit } from '../../../../../model/circuit/circuit';
import { Injectable } from '@angular/core';
import { CircuitActionItemFactory } from './circuit-action-item-factory';
import { CircuitAction } from './circuit-action.enum';

@Injectable({
  providedIn: 'root'
})
export class ActionCircuitStateMapping {

  constructor(protected  circuitActionItemFactory: CircuitActionItemFactory) {
  }

  getSingleActions(): ActionItemsSet<Circuit> {
    return {
      actionItems: [
        this.circuitActionItemFactory.getActionItem(CircuitAction.SHOW_CIRCUIT),
        this.circuitActionItemFactory.getActionItem(CircuitAction.DELETE_CIRCUIT),
        this.circuitActionItemFactory.getActionItem(CircuitAction.EDIT_CIRCUIT)
      ]
    };
  }
}
