import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { IManageCircuitService } from '../../services/IManageCircuitService';
import { Circuit } from 'src/app/model/circuit/circuit';
import { first } from 'rxjs/operators';
import { ActionCircuitStateMapping } from './actions/action-circuit-state-mapping';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { Pagination } from 'src/app/model/pagination';
import { CommonMessages } from '../../../../ls-common/i18n/common-messages';
import { CommonIcons } from '../../../../ls-common/icons/common-icons';
import { CircuitActionItemFactory } from './actions/circuit-action-item-factory';
import { CircuitAction } from './actions/circuit-action.enum';
import { CircuitsListMessages } from './i18n/circuits-list-messages';

@Component({
  selector: 'wa-circuits-list',
  templateUrl: './circuits-list.component.html',
  styleUrls: ['./circuits-list.component.scss']
})
export class CircuitsListComponent implements OnInit, OnDestroy {

  messages = CircuitsListMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  circuitAction = CircuitAction;
  singleActions = this.actionCircuitStateMapping.getSingleActions();
  circuitList: Circuit[] = null;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};
  filterString: string;
  loading = true;

  constructor(
    protected circuitService: IManageCircuitService,
    protected actionCircuitStateMapping: ActionCircuitStateMapping,
    public circuitActionItemFactory: CircuitActionItemFactory,
    protected router: Router,
    protected notificationService: NotificationService,
  ) {
  }

  ngOnInit() {
    this.reloadCircuits();
  }

  ngOnDestroy(): void {
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

  createCircuit() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_CIRCUIT_CREATION_PATH).then();
  }

  reloadCircuits() {
    this.gotoPage(1);
  }

  gotoPage(numPage: number) {
    this.loading = true;
    this.circuitList = null;
    const filters = this.filterString ? {searchedText: this.filterString} : null;
    console.log('filters', filters);
    this.circuitService.getAllCircuits(numPage, filters)
      .pipe(first())
      .subscribe(circuitsResult => {
        this.circuitList = circuitsResult.circuits;
        this.pagination = circuitsResult.pagination;
        this.loading = false;
      });
  }

  applyFilter(event) {
    this.filterString = event.trim();
    this.reloadCircuits();
  }

  toggleActivation(circuit: Circuit, value: boolean, button: any) {
    this.circuitService.switchCircuitActiveState(circuit, value).subscribe(result => {
      if (!result.success) {
        button.writeValue(!value);
        this.notificationService.showError(value ?
          this.messages.CIRCUIT_ACTIVATION_FAILED_TOASTR_TXT
          : this.messages.CIRCUIT_DEACTIVATION_FAILED_TOASTR_TXT
        );
      } else {
        this.notificationService.showSuccess(this.messages.switch_circuit_active_done(circuit.name, circuit.active));
      }
    });
  }

  hasFilter(): boolean {
    return this.filterString && this.filterString !== '';
  }
}
