export class CircuitsListMessages {
  static CREATE_CIRCUIT = 'Créer un circuit';
  static TITLE = 'Circuits existants';

  static COLUMN_TITLE_NAME = 'Nom du circuit';
  static COLUMN_DESCRIPTION_NAME = 'Description';

  static SHOW_ACTION_NAME = 'Visualiser';
  static EDIT_ACTION_NAME = 'Modifier';
  static DELETE_CIRCUITS_ACTION_NAME = 'Supprimer';

  static CONFIRM_DELETE_CIRCUIT_TITLE = 'Suppression du circuit';
  static CONFIRM_DELETE_CIRCUIT_BUTTON = 'Supprimer';
  static CIRCUIT_DELETED_CONFIRMATION_TOASTR_TXT = 'Circuit supprimé avec succès';
  static CIRCUIT_DELETE_FAILED_TOASTR_TXT = 'La suppression du circuit a échoué';

  static CIRCUIT_ACTIVATION_FAILED_TOASTR_TXT = `L'activation du circuit a échoué`;
  static CIRCUIT_DEACTIVATION_FAILED_TOASTR_TXT = 'La désactivation du circuit a échoué';

  static buildConfirmDeleteCircuitMsg(circuitName: string): string {
    return `Voulez-vous vraiment supprimer le circuit "${circuitName}"?`;
  }

  static switch_circuit_active_done(circuitName: string, active: boolean) {
    return active ? `Le circuit "${circuitName}" a été activé avec succès.` : `Le circuit "${circuitName}" a été désactivé avec succès.`;
  }
}
