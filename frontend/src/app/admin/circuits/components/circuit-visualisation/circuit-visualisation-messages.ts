export class CircuitVisualisationMessages {
  static MODIFY_CIRCUIT_BTN_TXT = 'Modifier';

  static CIRCUIT_NAME_FIELD_LABEL = 'Nom du circuit';
  static CIRCUIT_DESCRIPTION_FIELD_LABEL = 'Description';

  static CIRCUIT_NON_EDITABLE_TXT = 'Ce circuit est utilisé, il ne peut pas être modifié.';
}
