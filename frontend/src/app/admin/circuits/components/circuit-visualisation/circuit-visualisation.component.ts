import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Circuit } from 'src/app/model/circuit/circuit';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { CircuitRight } from '../../../../model/circuit/circuit-right.enum';
import { Weight } from '../../../../wa-common/weight.enum';
import { CommonIcons } from '../../../../ls-common/icons/common-icons';
import { CircuitVisualisationMessages } from './circuit-visualisation-messages';

@Component({
  selector: 'wa-circuit-visualisation',
  templateUrl: './circuit-visualisation.component.html',
  styleUrls: ['./circuit-visualisation.component.scss']
})
export class CircuitVisualisationComponent implements OnInit {

  messages = CircuitVisualisationMessages;
  commonIcons = CommonIcons;
  currentCircuit: Circuit;
  CircuitRight = CircuitRight;
  weight = Weight;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router
  ) {
  }

  ngOnInit() {
    this.route.data.subscribe(data => this.currentCircuit = data.circuit);
  }

  modifyCircuit() {
    this.router.navigateByUrl(RoutingPaths.buildAdminCircuitModificationPath('' + this.currentCircuit.id)).then();
  }

  back() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_CIRCUIT_PATH).then();
  }
}
