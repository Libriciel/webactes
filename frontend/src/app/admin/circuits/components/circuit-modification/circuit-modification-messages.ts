export class CircuitModificationMessages {
  static TITLE = 'Modification du circuit';
  static CIRCUIT_MODIFICATION_SUCCESS_MESSAGES = 'Circuit modifié avec succès';
  static CIRCUIT_MODIFICATION_ERROR_MESSAGES = 'La modification du circuit a échoué';
}
