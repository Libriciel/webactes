import { Component, OnInit } from '@angular/core';
import { CircuitCreationComponent } from '../circuit-creation/circuit-creation.component';
import { ICreateCircuitService } from '../../services/ICreateCircuitService';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { CircuitModificationMessages } from './circuit-modification-messages';

@Component({
  selector: 'wa-circuit-modification',
  templateUrl: '../circuit-creation/circuit-creation.component.html',
  styleUrls: ['../circuit-creation/circuit-creation.component.scss']
})
export class CircuitModificationComponent extends CircuitCreationComponent implements OnInit {
  modificationMessages = CircuitModificationMessages;

  constructor(
    protected circuitCreationService: ICreateCircuitService,
    protected modalService: NgbModal,
    protected notificationService: NotificationService,
    protected router: Router,
    protected route: ActivatedRoute
  ) {
    super(circuitCreationService, modalService, notificationService, router);
  }

  ngOnInit() {
    this.route.data.subscribe(data => this.currentCircuit = data.circuit);
  }

  saveCircuit() {
    this.waitingForBackendResponse = true;
    this.circuitCreationService.updateCircuit(this.currentCircuit.id, this.currentCircuit)
      .subscribe(
        () => {
          this.notificationService.showSuccess(this.modificationMessages.CIRCUIT_MODIFICATION_SUCCESS_MESSAGES);
        },
        error => {
          console.error('saveCircuit - got error : ', error);
          this.notificationService.showError(this.modificationMessages.CIRCUIT_MODIFICATION_ERROR_MESSAGES);
        })
      .add(
        () => {
          return this.waitingForBackendResponse = false;
        });
  }

}
