import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ICreateCircuitService } from '../../services/ICreateCircuitService';
import { Circuit } from 'src/app/model/circuit/circuit';
import { Step } from 'src/app/model/circuit/step';
import { CircuitCreationMessages } from './CircuitCreationMessages';
import { NgbModal, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmPopupComponent } from 'src/app/ls-common/components/confirm-popup/components/confirm-popup.component';
import { CircuitUtils } from '../../common/CircuitUtils';
import { Router } from '@angular/router';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { Style } from '../../../../wa-common/style.enum';
import { Weight } from '../../../../wa-common/weight.enum';
import { CommonMessages } from '../../../../ls-common/i18n/common-messages';
import { CommonIcons } from '../../../../ls-common/icons/common-icons';

@Component({
  selector: 'wa-circuit-creation',
  templateUrl: './circuit-creation.component.html',
  styleUrls: ['./circuit-creation.component.scss']
})
export class CircuitCreationComponent implements OnInit {

  messages = CircuitCreationMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  currentCircuit: Circuit = new Circuit();
  currentEditedStepIdx: number;
  weight = Weight;
  waitingForBackendResponse = false;
  currentPopover: NgbPopover;
  @ViewChild(NgbPopover, {static: true}) circuitEditionPopover: NgbPopover;
  @ViewChildren(NgbPopover) allCircuitEditionPopover: QueryList<NgbPopover>;

  constructor(
    protected circuitCreationService: ICreateCircuitService,
    protected modalService: NgbModal,
    protected notificationService: NotificationService,
    protected router: Router,
  ) {
  }

  ngOnInit() {
  }

  saveCircuit() {
    this.waitingForBackendResponse = true;
    this.circuitCreationService.addCircuit(this.currentCircuit)
      .subscribe(
        () => {
          this.notificationService.showSuccess(this.messages.CIRCUIT_CREATION_SUCCESS_MESSAGES);
          this.router.navigateByUrl(RoutingPaths.ADMIN_CIRCUIT_PATH).then();
        },
        error => {
          this.notificationService.showError(this.messages.CIRCUIT_CREATION_ERROR_MESSAGES, error);
        })
      .add(
        () => {
          return this.waitingForBackendResponse = false;
        });
  }

  saveBtnDisabled() {
    return !this.currentCircuit ||
      !this.currentCircuit.name ||
      !this.currentCircuit.steps ||
      this.currentCircuit.steps.length === 0;
  }

  back() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_CIRCUIT_PATH).then();
  }

  deleteStep(stepIdx: number) {
    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_STEP_TITLE;
    modalRef.componentInstance.content = this.messages.CONFIRM_DELETE_STEP_CONTENT;
    modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_DELETE_STEP_BUTTON;
    modalRef.componentInstance.style = Style.WARNING;
    modalRef.componentInstance.icon = '';
    modalRef.result
      .then(
        () => {
          this.currentCircuit.steps.splice(stepIdx, 1);
          this.notificationService.showSuccess(this.messages.STEP_DELETED_CONFIRMATION_TOASTR_TXT);
        })
      .catch(
        error => {
          console.error('Caught error in popup for step deletion : ', error);
        });
  }

  confirmStepCreation(stepData: Step, idx: number) {
    this.addStep(stepData, idx);
    this.closeCurrentPopover();
  }

  confirmStepEdition(stepData: Step, idx: number) {
    this.replaceStep(stepData, idx);
    this.closeCurrentPopover();
  }

  cancelStepEdition() {
    this.closeCurrentPopover();
  }

  cancelStepCreation() {
    this.closeCurrentPopover();
  }

  isFirstStep(): boolean {
    return (!this.currentCircuit || this.currentCircuit.steps.length === 0);
  }

  getAdditionalStepClass(idx: number): string {
    return this.currentEditedStepIdx !== null && this.currentEditedStepIdx === idx ? 'selected' : '';
  }

  getStepIconClass(step: Step): string {
    return CircuitUtils.getStepIconClass(step);
  }

  getOtherStepsNames(currentStepIdx: number): string[] {
    return this.currentCircuit.steps
      .filter((step, idx) => idx !== currentStepIdx)
      .map(step => step.name);
  }

  cloneStep(src: Step): Step {
    const clone = new Step();
    clone.name = src.name;
    clone.type = src.type;
    clone.validators = [...src.validators];
    return clone;
  }

  createStep(stepIdx: number) {
    const newStep = new Step();

    this.currentPopover = this.getCreationPopoverForIdx(stepIdx);
    this.currentPopover.hidden.subscribe(() => this.cancelStepCreation());
    this.currentPopover.open({stepData: newStep, idx: stepIdx});
  }

  public editStep(stepIdx: number) {
    this.currentEditedStepIdx = stepIdx;
    const editingStep = this.cloneStep(this.currentCircuit.steps[stepIdx]);
    this.currentPopover = this.getEditionPopoverForIdx(stepIdx);
    this.currentPopover.hidden.subscribe(() => this.cancelStepEdition());
    this.currentPopover.open({stepData: editingStep, idx: stepIdx});
  }

  private closeCurrentPopover() {
    if (this.currentPopover && this.currentPopover.isOpen()) {
      this.currentPopover.close();
    }
    this.currentPopover = null;
    this.currentEditedStepIdx = null;
  }

  private addStep(step: Step, pos: number): void {
    this.currentCircuit.steps.splice(pos, 0, step);
  }

  private replaceStep(newStep: Step, stepIdx: number): void {
    this.currentCircuit.steps.splice(stepIdx, 1, newStep);
  }

  private getCreationPopoverForIdx(idx: number): NgbPopover {
    const filteredList = this.allCircuitEditionPopover.filter(
      item => {
        const elem = (item as any)._elementRef;
        return elem && elem.nativeElement ? elem.nativeElement.hasAttribute('create-btn') : true;
      });

    return filteredList[idx];
  }

  private getEditionPopoverForIdx(idx: number): NgbPopover {
    const filteredList = this.allCircuitEditionPopover.filter(
      item => {
        const elem = (item as any)._elementRef;
        return elem && elem.nativeElement ? !(elem.nativeElement.hasAttribute('create-btn')) : true;
      });
    return filteredList[idx];
  }

}
