import { CircuitConstants } from '../../common/CircuitConstants';

export class CircuitCreationMessages {
  static SAVE_CIRCUIT = 'Enregistrer le circuit';
  static TITLE = 'Création de circuit';
  static CONFIRM_DELETE_STEP_TITLE = 'Suppression de l\'étape';
  static CONFIRM_DELETE_STEP_CONTENT = 'Êtes-vous sûr de vouloir supprimer cette étape du circuit?';
  static CONFIRM_DELETE_STEP_BUTTON = 'Ok';
  static STEP_DELETED_CONFIRMATION_TOASTR_TXT = 'Étape supprimée avec succès!';

  static CREATE_STEP_TITLE = 'Ajouter une étape';
  static EDIT_STEP_TITLE = 'Modification de l\'étape';
  static ADD_STEP_BTN_TXT = 'Ajouter une étape';
  static FIRST_STEP_TXT = 'Début';
  static LAST_STEP_TXT = 'Fin';

  static CIRCUIT_NAME_FIELD_LABEL = 'Nom du circuit';
  static CIRCUIT_DESCRIPTION_FIELD_LABEL = 'Description';
  static CIRCUIT_NAME_PLACEHOLDER = 'Entrer un nom pour le circuit';
  static CIRCUIT_DESCRIPTION_PLACEHOLDER = 'Entrer une définition pour le circuit';
  static CIRCUIT_CREATION_SUCCESS_MESSAGES = 'Circuit créé avec succès';
  static CIRCUIT_CREATION_ERROR_MESSAGES = 'La création du circuit a échoué';

  static CIRCUIT_START_ICON = CircuitConstants.CIRCUIT_START_ICON;
  static CIRCUIT_END_ICON = CircuitConstants.CIRCUIT_END_ICON;
}
