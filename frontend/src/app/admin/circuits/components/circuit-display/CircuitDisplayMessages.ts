/* eslint-disable @typescript-eslint/member-ordering */
import { CircuitConstants } from '../../common/CircuitConstants';

export class CircuitDisplayMessages {
  static TITLE = 'Création de circuit';

  static FIRST_STEP_TXT = 'Début';
  static LAST_STEP_TXT = 'Fin';

  static COMMENT_POPUP_TILE_TXT = 'Commentaire';
  static COMMENT_TOOLTIP_TXT = 'Voir le commentaire';
  static CIRCUIT_COMMENT_ICON = CircuitConstants.CIRCUIT_COMMENT_ICON;
  static CIRCUIT_START_ICON = CircuitConstants.CIRCUIT_START_ICON;
  static CIRCUIT_END_ICON = CircuitConstants.CIRCUIT_END_ICON;
  static CIRCUIT_ACTIVE_STEP_ICON = 'fa-lg fas fa-arrow-right';
  static VALIDATED_IN_EMERGENCY_INFOS_CLASS = 'wa-emergency-validation';
  static SIMPLE_STEP_TOOLTIP = 'Étape simple';
  static CONCURRENT_STEP_TOOLTIP = 'Étape concurrente';
  static COLLABORATIVE_STEP_TOOLTIP = 'Étape collaborative';

  static validatedInEmergencyByToolTipTxt(name: string): string {
    return `Projet validé en urgence par ${name}`;
  }

}
