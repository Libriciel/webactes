import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Circuit } from 'src/app/model/circuit/circuit';
import { CircuitUtils } from '../../common/CircuitUtils';
import { Step } from 'src/app/model/circuit/step';
import { CircuitDisplayMessages } from './CircuitDisplayMessages';
import { CircuitInstance } from 'src/app/model/circuit/circuit-instance';
import { StepInstance, StepState } from 'src/app/model/circuit/step-instance';
import { User } from 'src/app/model/user/user';
import { StepType_t } from 'src/app/model/circuit/step-type';
import { CommonStylesConstants } from '../../../../wa-common/common-styles-constants';
import { Style } from '../../../../wa-common/style.enum';
import { CommonIcons } from '../../../../ls-common/icons/common-icons';

@Component({
  selector: 'wa-circuit-display',
  templateUrl: './circuit-display.component.html',
  styleUrls: ['./circuit-display.component.scss']
})
export class CircuitDisplayComponent implements OnInit, OnChanges {

  @Input() circuit: Circuit | CircuitInstance;
  @Input() activeStep: number;

  isInstance = false;
  isOver = false;
  isRejected = false;

  messages = CircuitDisplayMessages;
  commonStyles = CommonStylesConstants;

  constructor() {
  }

  ngOnInit() {
    if (!this.circuit) {
      console.error('Missing input circuit data');
      this.circuit = new Circuit();
    }
  }

  ngOnChanges() {
    if (this.circuit instanceof CircuitInstance) {
      this.activeStep = this.circuit.currentStepIdx;
      this.isInstance = true;
      this.isOver = this.circuit.isOver;
      this.isRejected = this.circuit.rejected;
    }
  }

  getStepIconClass(step: Step): string {
    return CircuitUtils.getStepIconClass(step);
  }

  getStepTooltip(step: Step): string {
    let result = '';
    switch (step.type.id) {
      case StepType_t.Simple: {
        result = this.messages.SIMPLE_STEP_TOOLTIP;
        break;
      }
      case StepType_t.Concurrent: {
        result = this.messages.CONCURRENT_STEP_TOOLTIP;
        break;
      }
      case StepType_t.Collaborative: {
        result = this.messages.COLLABORATIVE_STEP_TOOLTIP;
        break;
      }
    }
    return result;
  }

  isActiveStep(stepIdx: number): boolean {
    return !this.isRejected && !this.isOver && stepIdx === this.activeStep;
  }

  isPassed(step: StepInstance) {
    return this.isInstance && ((step.state === StepState.PASSED) || (step.state === StepState.REJECTED));
  }

  isActualValidator(step: StepInstance, validator: User) {
    return this.isInstance && (step.validatedBy.some((elem) => elem.id === validator.id));
  }

  isCircuitOver() {
    return this.isOver && !this.isRejected;
  }

  showSkippedStepsIcons(stepIdx: number): boolean {
    return this.isInstance && (this.circuit.steps[stepIdx] as StepInstance).state === StepState.ADMIN_APPROVED;
  }

  getCircuitEndAdditionalClasses() {
    const additionalClasses = [];
    if (this.isInstance) {
      if (!this.isCircuitOver()) {
        additionalClasses.push('not-active');
      } else {
        additionalClasses.push('wa-active-step');
      }
    }
    return additionalClasses;
  }

  getStepClasses(stepIdx: number, step: StepInstance): string[] {
    const classes = [];
    if (this.isActiveStep(stepIdx)) {
      classes.push('wa-active-step wa-white-icn');
    }

    if (this.isInstance) {
      if (step.state === StepState.SKIPPED || step.state === StepState.ADMIN_APPROVED) {
        classes.push('wa-circuit-skipped-step');
      }
    }
    return classes;
  }

  getValidatorClasses(step: StepInstance): string {
    return step.state === StepState.REJECTED
      ? `${CommonIcons.REJECTED_ICON} ${CommonStylesConstants.getStyleClass(Style.DANGER)}`
      : `${CommonIcons.VALIDATED_ICON} ${CommonStylesConstants.getStyleClass(Style.NORMAL)}`;
  }

  getSkippedStepInfoClass(): string {
    if ((this.circuit as CircuitInstance).wasValidatedInEmergency) {
      return this.messages.VALIDATED_IN_EMERGENCY_INFOS_CLASS;
    }
  }
}
