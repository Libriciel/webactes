import { Component, Input, OnInit } from '@angular/core';
import { CircuitDisplayMessages } from '../circuit-display/CircuitDisplayMessages';

@Component({
  selector: 'wa-circuit-comment',
  templateUrl: './circuit-comment.component.html',
  styleUrls: ['./circuit-comment.component.scss']
})
export class CircuitCommentComponent implements OnInit {

  @Input() comment: string;
  messages = CircuitDisplayMessages;

  constructor() {
  }

  ngOnInit() {
  }
}
