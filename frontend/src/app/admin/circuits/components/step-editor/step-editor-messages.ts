export class StepEditorMessages {
  static STEP_NAME = `Nom de l'étape : `;
  static STEP_NAME_PLACEHOLDER = 'Mon étape';
  static STEP_TYPE_PLACE_HOLDER = 'Mon type';
  static STEP_VALIDATOR_PLACE_HOLDER = 'Valideurs...';
  static STEP_TYPE = `Type d'étape : `;
  static STEP_VALIDATORS = 'Valideurs : ';
  static STEP_SINGLE_VALIDATOR = 'Valideur : ';

  static STEP_NAME_ALREADY_IN_USE = `Ce nom d'étape est déjà utilisé`;
  static STEP_NAME_REQUIRED = `Le nom d'étape ne peut être vide`;
}
