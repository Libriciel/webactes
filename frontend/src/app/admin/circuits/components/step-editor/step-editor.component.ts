import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ICreateStepService } from '../../services/ICreateStepService';
import { STEP_TYPE_CHOICES, STEP_TYPE_LIST, StepType, StepType_t } from 'src/app/model/circuit/step-type';
import { Step } from 'src/app/model/circuit/step';
import { first } from 'rxjs/operators';
import { CircuitConstants } from '../../common/CircuitConstants';
import { User } from 'src/app/model/user/user';
import { AbstractControl, UntypedFormControl, UntypedFormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Weight } from '../../../../wa-common/weight.enum';
import { CommonIcons, CommonMessages } from '../../../../ls-common';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { StepEditorMessages } from './step-editor-messages';

@Component({
  selector: 'wa-step-editor',
  templateUrl: './step-editor.component.html',
  styleUrls: ['./step-editor.component.scss']
})
export class StepEditorComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input()
  step: Step = new Step();
  @Input()
  forUpdate = false;
  @Input()
  previousStepNames = [];
  @Output()
  validateChange = new EventEmitter();
  @Output()
  cancelChange = new EventEmitter();
  @ViewChild('nameInput', {static: true}) nameInputElemRef: ElementRef;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  messages = StepEditorMessages;
  waCommonMessages = WACommonMessages;
  circuitConstants = CircuitConstants;
  weight = Weight;
  availableStepTypes: StepType[] = STEP_TYPE_LIST;
  stepTypeChoices = STEP_TYPE_CHOICES;
  stepTypeIconClasses: string[] = [];
  availableValidators: User[] = [];
  validateBtnDisabled = true;
  statusChangesSubscription: Subscription;
  formGroup: UntypedFormGroup;

  constructor(protected stepService: ICreateStepService, protected changeDetectorRef: ChangeDetectorRef) {
  }

  noDuplicateNameValidator(otherStepNames: string[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = otherStepNames.includes(control.value);
      return forbidden ? {'alreadyUsedName': control.value} : null;
    };
  }

  ngOnInit() {
    this.stepService.getValidators().pipe(first()).subscribe(validators => this.availableValidators = validators.users);
    this.loadIconsForTypes();
    this.validateBtnDisabled = true;
    if (this.nameInputElemRef && this.nameInputElemRef.nativeElement) {
      this.nameInputElemRef.nativeElement.focus();
    }

    this.formGroup = new UntypedFormGroup({
      stepName: new UntypedFormControl(this.step.name, [
          Validators.maxLength(45),
          this.noDuplicateNameValidator(this.previousStepNames)
        ]
      )
    });

    this.statusChangesSubscription = this.formGroup.statusChanges
      .subscribe(() => {
          this.step.name = this.formGroup.get('stepName').value;
          this.checkDataValidity();
        }
      );
  }

  ngOnDestroy() {
    this.statusChangesSubscription.unsubscribe();
  }

  selectedTypeChanged($event) {
    if ($event) {
      this.step.type = $event;
      if (this.step.type && this.step.type.id === StepType_t.Simple) {
        this.step.validators = [this.step.validators[0]];
      }
      this.checkDataValidity();
    }
  }

  checkDataValidity(): void {
    this.validateBtnDisabled = true;
    if (!this.step) {
      return;
    }

    if (!this.formGroup.get('stepName').valid) {
      return;
    }

    if (!this.step.type) {
      return;
    }

    if (!this.step.validators || this.step.validators.length === 0) {
      return;
    }

    if (!this.step.validators[0]) {
      return;
    }

    if (this.step.type.id === StepType_t.Concurrent || this.step.type.id === StepType_t.Collaborative) {
      if (this.step.validators.length < 2) {
        return;
      }
    }

    this.validateBtnDisabled = false;
  }

  typeAllowMultipleValidators(): boolean {
    if (!this.step || !this.step.type) {
      return false;
    }
    return this.step.type.id !== StepType_t.Simple;
  }

  addOrUpdateStep() {
    this.validateChange.emit(this.step);
  }

  cancel() {
    this.cancelChange.emit();
  }

  private loadIconsForTypes() {
    this.availableStepTypes.forEach((type) => {
      switch (type.id) {
        case StepType_t.Simple: {
          this.stepTypeIconClasses.push(this.circuitConstants.simple_step_icn_class);
          break;
        }
        case StepType_t.Concurrent: {
          this.stepTypeIconClasses.push(this.circuitConstants.concurrent_step_icn_class);
          break;
        }
        case StepType_t.Collaborative: {
          this.stepTypeIconClasses.push(this.circuitConstants.collaborative_step_icn_class);
          break;
        }
        default: {
          break;
        }
      }
    });
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }
}
