import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { AdminMessages } from '../../../i18n/admin-messages';
import { ManageStructureInformationComponent } from '../../component/manage-structure-information/manage-structure-information.component';
import { OrganizationMessages } from '../../i18n/organization-messages';
import { PageParameters } from '../../../../wa-common/components/page/model/page-parameters';
import { Config } from '../../../../Config';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_ORGANIZATION_PATH,
    component: ManageStructureInformationComponent,
    data: {
      pageParameters: {
        title: OrganizationMessages.PAGE_LOGO_MANAGEMENT_TITLE,
        breadcrumb: [
          {
            name: AdminMessages.PAGE_ADMIN_TITLE,
            location: RoutingPaths.ADMIN_PATH
          },
          {
            name: OrganizationMessages.PAGE_LOGO_MANAGEMENT_TITLE,
            location: `/${RoutingPaths.ADMIN_ORGANIZATION_PATH}`
          }
        ]
      } as PageParameters,
      imageSize: Config.headerImageSize,
      imageFormats: Config.headerImageAcceptedTypes
    },
    children: [
      {path: '**', redirectTo: RoutingPaths.ADMIN_PATH, pathMatch: 'full'}
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class OrganizationRoutingModule {
}
