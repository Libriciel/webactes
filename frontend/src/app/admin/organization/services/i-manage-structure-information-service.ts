import { Observable } from 'rxjs';
import { Pagination } from '../../../model/pagination';
import { FilterType } from '../../../model/util/filter-type';
import { StructureInfo } from '../../../model/structure-info';

export abstract class IManageStructureInformationService {

  abstract saveOrUpdate(file: File, name: string, spinner: string): Observable<Object>;

  abstract getStructureLogo(): Observable<Blob | string> ;

  abstract getCustomName(): Observable<string>;

  abstract getSpinner(): Observable<string>;

  abstract getAllWithPagination(numPage: number, filters?: FilterType, limit?: number): Observable<{ structures: StructureInfo[], pagination: Pagination }>;

  abstract getAllWithoutPagination(): Observable<{ structures: StructureInfo[] }>;

  abstract deleteLogo(file: File): Observable<any>;
}
