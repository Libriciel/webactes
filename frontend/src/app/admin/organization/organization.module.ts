import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageStructureInformationComponent } from './component/manage-structure-information/manage-structure-information.component';
import { OrganizationRoutingModule } from './routing/organization-routing/organization-routing.module';
import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import { NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SpinnerComponent } from '../../shared/widgets/spinner';

@NgModule({
  declarations: [ManageStructureInformationComponent],
  providers: [SpinnerComponent],
  imports: [
    CommonModule,
    OrganizationRoutingModule,
    NgxTrimDirectiveModule,
    NgbPopoverModule,
    LibricielCommonModule,
    FormsModule,
    NgSelectModule,
  ]
})
export class OrganizationModule {
}
