export class OrganizationMessages {

  static PAGE_LOGO_MANAGEMENT_TITLE = 'Personnalisation';

  static ADD_IMAGE_TITLE = `Ajouter une image`;
  static ADD_IMAGE_BUTTON_TITLE = 'Logo de la structure';

  static DELETE_IMAGE_TITLE = `Supprimer l'image d'entête`;
  static REPLACE_IMAGE_TITLE = `Remplacer l'image d'entête`;

  static HEADER_TEXT_INPUT_TEXT_TITLE = 'Texte à afficher en en-tête';
  static HEADER_TEXT_INPUT_PLACE_HOLDER = `Ma collectivité, mon service,...`;

  static SUCCESS_MESSAGE = 'Informations de personnalisation de la structure enregistrées.';
  static ERROR_MESSAGE = `Problème lors de l'enregistrement des informations de personnalisation de la structure.`;
  static DELETE_FILE_SUCCESS_TOASTR_TXT = 'Fichier correctement supprimé.';
  static DELETE_FILE_ERROR_TOASTR_TXT = 'Erreur lors de la suppression du fichier';

  static SPINNER_LABEL = 'Animation d\'attente';
  static SPINNER_PLACE_HOLDER = 'Sélectionner une animation d\'attente';

  static AddImageHelp(size: number): string {
    return `La taille recommandée pour afficher correctement votre image est de ${size} pixels de largeur maximum.`;
  }
}
