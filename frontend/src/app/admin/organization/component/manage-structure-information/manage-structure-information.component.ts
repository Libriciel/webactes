import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { OrganizationMessages } from '../../i18n/organization-messages';
import { ActivatedRoute, Router } from '@angular/router';
import { FileUtils } from '../../../../utility/Files/file-utils';
import { FileType } from '../../../../utility/Files/file-types.enum';
import { IManageStructureInformationService } from '../../services/i-manage-structure-information-service';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { STRUCTURE_USER_INFORMATION } from '../../../../wa-common/variables/structure-user-info';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { Style } from '../../../../wa-common';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { SpinnerService } from '../../../../core/services/spinner.service';

@Component({
  selector: 'wa-manage-structure-information',
  templateUrl: './manage-structure-information.component.html',
  styleUrls: ['./manage-structure-information.component.scss']
})
export class ManageStructureInformationComponent implements OnInit {

  @Output() deleteFile = new EventEmitter<Object>();
  @ViewChild('inputFile', {static: true}) inputFile;
  @Input() imageSize: number;
  @Input() imageFormats: FileType[];
  selectedFile: File;
  name: string = STRUCTURE_USER_INFORMATION.structureInformation.customName;
  url: string | ArrayBuffer;
  isSubmitting = false;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  messages = OrganizationMessages;
  style = Style;
  file: File | null = null;

  @ViewChild('spinnerStructure') selectSpinnerStructure;
  spinnersItems: Array<{ value: string, name: string }>;
  spinnerDefault = 'ball-clip-rotate';
  spinnerStructure: string = STRUCTURE_USER_INFORMATION.structureInformation.spinner;

  protected readonly WACommonMessages = WACommonMessages;

  constructor(protected activatedRoute: ActivatedRoute,
              protected structureInformationService: IManageStructureInformationService,
              protected notificationService: NotificationService,
              protected router: Router,
              protected spinnerService: SpinnerService
  ) {
    this.spinnersItems = [
      {value: 'ball-scale', name: 'Balle ondulante'},
      {value: 'ball-climbing-dot', name: 'Balle qui monte des escaliers'},
      {value: 'ball-scale-multiple', name: 'Balles de plusieurs tailles se multipliant'},
      {value: 'ball-scale-pulse', name: 'Balles de plusieurs tailles qui impulsent'},
      {value: 'ball-elastic-dots', name: 'Balles élastiques'},
      {value: 'ball-square-clockwise-spin', name: 'Balles en carré en rotation'},
      {value: 'ball-circus', name: 'Balles en cercle'},
      {value: 'ball-pulse-rise', name: 'Balles en impulsion et rotation'},
      {value: 'ball-pulse-sync', name: 'Balles en impulsion synchronisées'},
      {value: 'ball-pulse', name: 'Balles en pulsation'},
      {value: 'ball-triangle-path', name: 'Balles en triangle'},
      {value: 'ball-spin-clockwise', name: 'Balles en rotation'},
      {value: 'ball-rotate', name: 'Balles en rotation sur une balle'},
      {value: 'ball-spin-clockwise-fade', name: 'Balles en rotation qui disparaissent'},
      {value: 'ball-beat', name: 'Balles en rythme'},
      {value: 'ball-running-dots', name: 'Balles qui courent'},
      {value: 'ball-fussion', name: 'Balles qui fusionnent'},
      {value: 'ball-fall', name: 'Balles qui tombent'},
      {value: 'ball-zig-zag', name: 'Balles zig-zag'},
      {value: 'ball-zig-zag-deflect', name: 'Balles zig-zag déviées'},
      {value: 'square-loader', name: 'Carré de chargement'},
      {value: 'square-jelly-box', name: 'Carré en gelée'},
      {value: 'square-spin', name: 'Carré en rotation'},
      {value: 'ball-8bits', name: 'Cercle 8-bits'},
      {value: 'ball-clip-rotate-multiple', name: 'Cercles multiples en rotation'},
      {value: 'ball-clip-rotate-pulse', name: 'Cercle en rotation avec balle en pulsation'},
      {value: 'ball-clip-rotate', name: 'Cercle en rotation'},
      {value: 'ball-scale-ripple', name: 'Cercle ondulant'},
      {value: 'ball-scale-ripple-multiple', name: 'Cercles ondulants'},
      {value: 'cube-transition', name: 'Cube en transition'},
      {value: 'fire', name: 'Feu'},
      {value: 'ball-grid-beat', name: 'Grille de balles en pulsation'},
      {value: 'ball-grid-pulse', name: 'Grille de balles en impulsion'},
      {value: 'timer', name: 'Horloge'},
      {value: 'line-scale', name: 'Ligne d\'échelle'},
      {value: 'line-spin-clockwise-fade', name: 'Ligne en rotation dans le sens des aiguilles d\'une montre'},
      {value: 'line-spin-fade', name: 'Ligne en rotation disparaissant'},
      {value: 'line-scale-party', name: 'Ligne en rythme'},
      {value: 'line-scale-pulse-out-rapid', name: 'Ligne en rythme rapide'},
      {value: 'line-scale-pulse-out', name: 'Ligne en rythme sortie'},
      {value: 'pacman', name: 'Pac-Man'},
      {value: 'cog', name: 'Roue dentée'},
      {value: 'ball-newton-cradle', name: 'Sphère de Newton pendulaire'},
      {value: 'ball-atom', name: 'Sphère d\'atome'},
      {value: 'triangle-skew-spin', name: 'Triangle en rotation oblique'},
    ];
  }

  ngOnInit() {
    this.selectSpinnerStructure = this.spinnersItems.find(spinner => spinner.value === this.spinnerStructure).value;

    this.structureInformationService.getStructureLogo().subscribe(value => {
      if (value instanceof Blob) {
        this.createImageFromBlob(value);
      }
    });
    this.activatedRoute.data.subscribe(data => {
      if (data.imageSize) {
        this.imageSize = data.imageSize;
      }
      if (data.imageFormats) {
        this.imageFormats = data.imageFormats;
      }
    });
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then().catch(error => console.error(error));
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => this.url = reader.result, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }

  getFileExtensions() {
    return FileUtils.getFileExtensions(this.imageFormats);
  }

  onFileChanged(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (event1: any) => this.url = event1.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedFile = event.target.files[0];
    }
  }

  save() {
    this.isSubmitting = true;
    this.structureInformationService.saveOrUpdate(this.selectedFile, this.name, this.selectSpinnerStructure)
      .subscribe(
        () => {
          this.notificationService.showSuccess(this.messages.SUCCESS_MESSAGE);
          this.reloadCurrentPage();
        },
        error => {
          console.error(error);
          this.notificationService.showError(this.messages.ERROR_MESSAGE);
        })
      .add(() => this.isSubmitting = false);
  }

  reloadCurrentPage() {
    this.router.navigateByUrl('/', {skipLocationChange: true})
      .then(() => this.router.navigate([this.router.url])
        .then().catch(error => console.error(error)))
      .catch(error => console.error(error));
  }

  deleteImage() {
    this.structureInformationService.deleteLogo(this.selectedFile).subscribe(
      () => {
        this.notificationService.showSuccess(this.messages.DELETE_FILE_SUCCESS_TOASTR_TXT);
        this.isSubmitting = true;
      },
      () => {
        this.notificationService.showError(this.messages.DELETE_FILE_ERROR_TOASTR_TXT);
        this.isSubmitting = false;
      })
      .add(() => this.isSubmitting = false);
  }

  onSpinnerChanged(selected: { value: string, name: string }) {
    this.spinnerService.globalSpinnerType = selected.value;
    this.spinnerService.load();
    setTimeout(() => this.spinnerService.close(), 2000);
  }

  resetSpinner() {
    this.selectSpinnerStructure = [this.spinnerDefault];
  }
}
