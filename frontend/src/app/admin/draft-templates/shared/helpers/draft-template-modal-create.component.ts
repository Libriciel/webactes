import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonIcons, CommonMessages } from '../../../../ls-common';
import { DraftTemplatesMessages } from '../draft-templates-messages';
import { DraftTemplate, DraftTemplateFilesService, DraftTemplatesService, DraftTemplateType, DraftTemplateTypesService } from '../../../../core';
import { Style } from '../../../../wa-common';
import { FileType } from '../../../../utility/Files/file-types.enum';

@Component({
  selector: 'wa-draft-template-modal-create',
  templateUrl: 'draft-template-modal-create.component.html'
})
export class DraftTemplateModalCreateComponent {

  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  messages = DraftTemplatesMessages;

  draftTemplateForm: UntypedFormGroup;
  draftTemplate: DraftTemplate = {} as DraftTemplate;
  optionDraftTemplateTypes: DraftTemplateType[];
  style = Style;
  fileType = FileType;

  isSubmitting = false;
  isSubmitDisabled = true;

  @Input() imageFormats: FileType[];
  @Output() isCreated = new EventEmitter<boolean>();

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private activeModal: NgbActiveModal,
    private draftTemplatesService: DraftTemplatesService,
    private draftTemplateTypesService: DraftTemplateTypesService,
    private draftTemplateFilesService: DraftTemplateFilesService,
    protected notificationService: NotificationService
  ) {
    this.draftTemplateForm = this._formBuilder.group({
      draft_template_type: ['', Validators.required],
      name: ['', [Validators.required, Validators.maxLength(255)]],
      draft_template_file: ['', [Validators.required, this.requiredFileType('odt')]],
    });

    this.draftTemplateTypesService.getAll()
      .subscribe(data => {
        this.optionDraftTemplateTypes = data.draftTemplateTypes;
      });
  }

  onSubmit() {
    this.isSubmitting = true;
    if (this.draftTemplateForm.dirty && this.draftTemplateForm.valid) {

      // update the model
      this.updateDraftTemplate(this.draftTemplateForm.value);

      this.draftTemplatesService.save(this.draftTemplate)
        .subscribe(
          draftTemplate => {
            this.draftTemplateFilesService.sendFile(draftTemplate.id.toString(), this.draftTemplateForm.get('draft_template_file').value).subscribe(
              () => {
                this.notificationService.showSuccess(this.messages.addDraftTemplateActionDone(draftTemplate.name));
                this.isSubmitting = true;
                this.activeModal.close();
                this.isCreated.emit(true);
              },
              () => {
                  this.draftTemplatesService.destroy(draftTemplate.id);
                  this.notificationService.showError(this.messages.CREATED_FILE_ERROR_TOASTR_TXT);
                  this.isSubmitting = false;
                }
              );
          },
          () => {
            this.notificationService.showError(this.messages.CREATED_ERROR_TOASTR_TXT);
            this.isSubmitting = false;
          }
        );
    }

  }

  submitDraftTemplate() {
    this.onSubmit();
  }

  requiredFileType(type: string) {
    return function (control: UntypedFormControl) {
      const file = control.value;
      if (file) {
        const extension = file.name.split('.')[1].toLowerCase();
        if (type.toLowerCase() !== extension.toLowerCase()) {
          return {
            requiredFileType: true
          };
        }

        return null;
      }

      return null;
    };
  }

  updateDraftTemplate(values: Object) {
    Object.assign(this.draftTemplate, values);
  }
}
