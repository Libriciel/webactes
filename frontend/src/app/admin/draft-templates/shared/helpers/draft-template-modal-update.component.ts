import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from 'file-saver';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { DraftTemplatesMessages } from '../draft-templates-messages';
import {
  DraftTemplate,
  DraftTemplateFile,
  DraftTemplateFilesService,
  DraftTemplatesService,
  DraftTemplateType,
  DraftTemplateTypesService,
  OfficeEditorFile
} from '../../../../core';
import { Style } from '../../../../wa-common';
import { FileType } from '../../../../utility/Files/file-types.enum';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'wa-draft-template-modal-update',
  templateUrl: 'draft-template-modal-update.component.html'
})
export class DraftTemplateModalUpdateComponent implements OnInit {

  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  messages = DraftTemplatesMessages;

  draftTemplateForm: UntypedFormGroup;
  optionDraftTemplateTypes: DraftTemplateType[];
  style = Style;
  fileType = FileType;

  isSubmitting = false;
  isSubmitDisabled = true;

  officeEditorFile: OfficeEditorFile;

  @Input() public draftTemplate: DraftTemplate;
  @Output() isUpdated = new EventEmitter<boolean>();

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private activeModal: NgbActiveModal,
    private draftTemplatesService: DraftTemplatesService,
    private draftTemplateTypesService: DraftTemplateTypesService,
    private draftTemplateFilesService: DraftTemplateFilesService,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit() {
    this.draftTemplateForm = this._formBuilder.group({
      id: this.draftTemplate.id,
      name: [this.draftTemplate.name, [Validators.required, Validators.maxLength(255)]],
      draft_template_type: [this.draftTemplate.draft_template_type, [Validators.required]],
      draft_template_file: [this.draftTemplate.draft_template_file, [Validators.required, this.requiredFileType('odt')]],
    });

    this.draftTemplateTypesService.getAll()
      .subscribe(data => {
        this.optionDraftTemplateTypes = data.draftTemplateTypes;
      });

    this.updateOfficeEditorFile(this.draftTemplate.draft_template_file);
  }

  onSubmit() {
    this.isSubmitting = true;
    if (this.draftTemplateForm.valid) {

      const oldIdFile = this.draftTemplate.draft_template_file.id;

      // update the model
      this.updateDraftTemplate(this.draftTemplateForm.value);

      this.draftTemplatesService.save(this.draftTemplate).subscribe({
        next: () => {
          if (this.draftTemplate.draft_template_file.id == undefined) {
            // Delete file
            this.draftTemplateFilesService.destroy(oldIdFile).subscribe(() => {
            });
            // Add file
            this.draftTemplateFilesService
              .sendFile(this.draftTemplate.id.toString(), this.draftTemplateForm.get('draft_template_file').value)
              .subscribe(
                () => {
                  this.notificationService.showSuccess(this.messages.updateDraftTemplateActionDone(this.draftTemplate.name));
                  this.activeModal.close({isError: false});
                  this.isUpdated.emit(true);
                },
                () => {
                  this.notificationService.showError(this.messages.CREATED_FILE_ERROR_TOASTR_TXT);
                  this.isSubmitting = false;
                }
              );
          } else {
            this.notificationService.showSuccess(this.messages.updateDraftTemplateActionDone(this.draftTemplate.name));
            this.activeModal.close({isError: false});
            this.isUpdated.emit(true);
          }
        },
        error: () => {
          this.notificationService.showError(this.messages.CREATED_ERROR_TOASTR_TXT);
          this.isSubmitting = false;
        }
      });
    }
  }

  submitDraftTemplate() {
    this.onSubmit();
  }

  requiredFileType(type: string) {
    return function (control: UntypedFormControl) {

      const file = control.value;
      if (file) {
        const extension = file.name.split('.')[1].toLowerCase();
        if (type.toLowerCase() !== extension.toLowerCase()) {
          return {
            requiredFileType: true
          };
        }

        return null;
      }

      return null;
    };
  }

  updateDraftTemplate(values: Object) {
    Object.assign(this.draftTemplate, values);
  }

  updateOfficeEditorFile(draftTemplateFile: DraftTemplateFile) {
    this.officeEditorFile = {
      id: draftTemplateFile.id,
      name: draftTemplateFile.name,
      size: draftTemplateFile.size,
      mimetype: draftTemplateFile.mimetype,
      path: draftTemplateFile.path,
      baseUrl: draftTemplateFile.base_url,
      pathWopi: draftTemplateFile.path_wopi + draftTemplateFile.id,
    };
  }

  download(draftTemplateFile: DraftTemplateFile): void {
    this.draftTemplateFilesService
      .geFile(draftTemplateFile.id)
      .subscribe(blob => {
        const blobPart = new Blob([blob], {type: draftTemplateFile.mimetype});
        saveAs(blobPart, draftTemplateFile.name);
      });
  }
}
