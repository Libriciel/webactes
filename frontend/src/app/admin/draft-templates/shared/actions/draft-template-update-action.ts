import { Observable, Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionResult, IActuator, NotificationService } from '../../../../ls-common';
import { DraftTemplate } from '../../../../core';
import { DraftTemplatesMessages } from '../draft-templates-messages';
import { DraftTemplateModalUpdateComponent } from '../helpers';

export class DraftTemplateUpdateAction implements IActuator<DraftTemplate> {

  messages = DraftTemplatesMessages;

  constructor(
    protected modalService: NgbModal,
    protected notificationService: NotificationService
  ) {
  }

  action(DraftTemplates: DraftTemplate[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(DraftTemplateModalUpdateComponent, {
      backdrop: 'static',
      centered: true,
      size: 'lg'
    });

    modalRef.componentInstance.draftTemplate = DraftTemplates[0];

    const returnSubject = new Subject<ActionResult>();

    modalRef.componentInstance.isUpdated.subscribe(() => {
      returnSubject.next({
        error: false,
        needReload: true,
        message: null
      });
    });

    modalRef.result.then(
      () => returnSubject.complete(),
      () => {
        returnSubject.next();
        returnSubject.complete();
      }
    );

    return returnSubject.asObservable();

  }
}
