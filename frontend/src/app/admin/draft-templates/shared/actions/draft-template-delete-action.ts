import { Observable, Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Style } from '../../../../wa-common';
import { ActionResult, ConfirmPopupComponent, IActuator, NotificationService } from '../../../../ls-common';
import { DraftTemplate, DraftTemplatesService } from '../../../../core';
import { DraftTemplatesMessages } from '../draft-templates-messages';

export class DraftTemplateDeleteAction implements IActuator<DraftTemplate> {

  messages = DraftTemplatesMessages;

  constructor(
    protected draftTemplatesService: DraftTemplatesService,
    protected modalService: NgbModal,
    protected notificationService: NotificationService
  ) {
  }

  action(draftTemplates: DraftTemplate[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    const returnSubject = new Subject<ActionResult>();

    modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_TITLE;
    const draftTemplatesNames = draftTemplates.map(draftTemplate => draftTemplate.name).join(', ');
    modalRef.componentInstance.content = this.messages.deleteDraftTemplateConfirm(draftTemplatesNames);

    modalRef.componentInstance.confirmMsg = this.messages.CONFIRM_DELETE_BUTTON;
    modalRef.componentInstance.style = Style.DANGER;

    modalRef.result
      .then(
        () => {
          this.draftTemplatesService.destroy(draftTemplates[0].id).subscribe({
            complete: () => {
              returnSubject.next({
                error: false,
                needReload: true,
                message: this.messages.DELETED_CONFIRMATION_TOASTR_TXT
              });
            },
            error: () => {
              console.error('Error calling actor deletion');
              returnSubject.next({
                error: true,
                needReload: true,
                message: this.messages.DELETE_FAILED_TOASTR_TXT
              });
            }
          });
        },
        () => returnSubject.complete()
      )
      .catch(error => {
        console.error('Caught error in popup for circuit deletion : ', error);
      });

    return returnSubject.asObservable();
  }
}
