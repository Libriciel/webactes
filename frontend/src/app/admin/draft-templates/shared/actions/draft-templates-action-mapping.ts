import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { Injectable } from '@angular/core';

import { ActionItem, CommonIcons, CommonMessages, NotificationService, } from '../../../../ls-common';
import { DraftTemplate, DraftTemplatesService } from '../../../../core';

import { Style } from '../../../../wa-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { DraftTemplatesMessages } from '../draft-templates-messages';
import { DraftTemplateDeleteAction } from './draft-template-delete-action';
import { DraftTemplateUpdateAction } from './draft-template-update-action';

@Injectable({
  providedIn: 'root'
})
export class DraftTemplatesActionMapping {

  messages = DraftTemplatesMessages;

  constructor(
    protected modalService: NgbModal,
    protected draftTemplatesService: DraftTemplatesService,
    protected notificationService: NotificationService) {
  }

  getCommonActions(): ActionItemsSet<DraftTemplate> {
    return null;
  }

  getOtherSingleActions(): ActionItemsSet<DraftTemplate> {
    return null;
  }

  getSingleActions(): ActionItemsSet<DraftTemplate> {
    return {
      actionItems: [
        new ActionItem({
          name: CommonMessages.MODIFY,
          icon: CommonIcons.EDIT_ICON,
          actuator: new DraftTemplateUpdateAction(
            this.modalService,
            this.notificationService
          ),
        }),
        new ActionItem({
          name: CommonMessages.DELETE,
          icon: CommonIcons.DELETE_ICON,
          style: Style.DANGER,
          actuator: new DraftTemplateDeleteAction(
            this.draftTemplatesService,
            this.modalService,
            this.notificationService
          ),
        })
      ]
    };
  }
}
