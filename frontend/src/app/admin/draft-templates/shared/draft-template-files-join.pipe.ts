import { Pipe, PipeTransform } from '@angular/core';
import { DraftTemplateFile } from '../../../core';

/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'joinDraftTemplateFiles'})
export class joinDraftTemplateFilesPipe implements PipeTransform {
  // transform(value: number, exponent?: number): number {
  //   return Math.pow(value, isNaN(exponent) ? 1 : exponent);
  // }
  transform(draft_template_files: Array<DraftTemplateFile>, separator = ', '): string {
    return draft_template_files.map(draft_template_file => draft_template_file.name).join(separator);
  }
}
