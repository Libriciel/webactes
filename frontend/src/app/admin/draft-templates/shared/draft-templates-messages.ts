import { Validators } from '@angular/forms';

export class DraftTemplatesMessages {
  static HEADER_NAME = `Nom du gabarit`;
  static HEADER_TYPE = `Type`;
  static HEADER_FILE = `Fichier`;

  static MODAL_TITLE_CREATE = `Création d'un gabarit`;
  static MODAL_TITLE_UPDATE = `Modification d'un gabarit`;
  static FORM_FIELD_DRAFT_TEMPLATE_TYPES_LABEL = `Type de gabarit`;
  static FORM_FIELD_DRAFT_TEMPLATE_TYPES_LABEL_PLACEHOLDER = `Type de gabarit`;
  static FORM_FIELD_NAME_LABEL = `Nom`;
  static FORM_FIELD_NAME_LABEL_PLACEHOLDER = `Nom du gabarit`;
  static FORM_FIELD_DRAFT_TEMPLATE_FILE_LABEL = `Gabarit`;
  static FORM_FIELD_DRAFT_TEMPLATE_FILE_BUTTON_TEXT = `Ajouter un fichier de gabarit`;

  static BUTTON_ADD = `Créer un gabarit`;
  static BUTTON_SAVE = `Enregistrer`;

  static DELETED_CONFIRMATION_TOASTR_TXT = `Gabarit supprimé avec succès`;
  static DELETE_FAILED_TOASTR_TXT = `La suppression du gabarit a échoué`;
  static CONFIRM_DELETE_TITLE = `Suppression du gabarit`;
  static CONFIRM_DELETE_BUTTON = `Supprimer`;

  static CREATED_SUCCESS_TOASTR_TXT = `Gabarit créé avec succès`;
  static CREATED_ERROR_TOASTR_TXT = `Erreur lors de la création du gabarit.`;
  static CREATED_FILE_ERROR_TOASTR_TXT = `Erreur lors du téléchargement du fichier de gabarit.`;

  static sameName(name: string) {
    return `Un gabarit avec le nom ${name} est déjà présent.<br>
        Veuillez renseigner un autre nom pour le gabarit.`;
  }

  static updateDraftTemplateActionDone(nameDraftTemplate: string): string {
    return `Les modifications du gabarit "${nameDraftTemplate}" ont été enregistrées avec succès.`;
  }

  static addDraftTemplateActionDone(nameDraftTemplate: string): string {
    return `Le gabarit "${nameDraftTemplate}" a été créé avec succès.`;
  }

  static deleteDraftTemplateConfirm(nameDraftTemplate: string): string {
    return `Voulez-vous vraiment supprimer le gabarit "${nameDraftTemplate}"?`;
  }

  static formFieldDraftTemplateFileHelp(size: number): string {
    return `La taille recommandée pour afficher correctement une génération est de ${size} Mo.`;
  }

}
