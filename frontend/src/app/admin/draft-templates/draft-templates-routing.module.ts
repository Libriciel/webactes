import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageParameters } from 'src/app/wa-common/components/page/model/page-parameters';
import { DraftTemplatesComponent } from './draft-templates.component';

const routes: Routes = [
  {
    path: 'administration/draft-templates',
    component: DraftTemplatesComponent,
    data: {
      pageParameters: {
        title: `Gabarits / textes par défaut`,
        breadcrumb: [
          {
            name: `Administration`,
            location: `administration`
          },
          {
            name: `Gabarits`, location: 'administration/draft-templates'
          }
        ],
        fluid: true
      } as PageParameters
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DraftTemplatesRoutingModule {
}
