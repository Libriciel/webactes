import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DraftTemplatesComponent } from './draft-templates.component';

describe('DraftTemplateComponent', () => {
  let component: DraftTemplatesComponent;
  let fixture: ComponentFixture<DraftTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DraftTemplatesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DraftTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
