import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared';

import { DraftTemplatesRoutingModule } from './draft-templates-routing.module';
import { DraftTemplatesComponent } from './draft-templates.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { joinDraftTemplateFilesPipe } from './shared';
import { DraftTemplateModalCreateComponent, DraftTemplateModalUpdateComponent } from './shared/helpers';


@NgModule({
  declarations: [
    DraftTemplatesComponent,
    DraftTemplateModalCreateComponent,
    DraftTemplateModalUpdateComponent,
    joinDraftTemplateFilesPipe
  ],
  imports: [
    SharedModule,
    DraftTemplatesRoutingModule,
  ],
  providers: [NgbActiveModal]
})
export class DraftTemplatesModule {
}
