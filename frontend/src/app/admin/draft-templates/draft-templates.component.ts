import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DraftTemplate, DraftTemplateFilesService, DraftTemplateListConfig, DraftTemplatesService, Pagination, } from '../../core';
import { ActionItemsSet } from '../../ls-common/model/action-items-set';
import { CommonIcons, CommonMessages } from '../../ls-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DraftTemplatesActionMapping, DraftTemplatesMessages } from './shared';
import { DraftTemplateModalCreateComponent } from './shared/helpers';

@Component({
  selector: 'wa-actors',
  templateUrl: './draft-templates.component.html',
  styleUrls: ['./draft-templates.component.scss']
})
export class DraftTemplatesComponent implements OnInit {
  isSubmitting = false;

  messages = DraftTemplatesMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  commonActions: ActionItemsSet<DraftTemplate>;
  singleActions: ActionItemsSet<DraftTemplate>;
  otherSingleActions: ActionItemsSet<DraftTemplate>;

  pagination: Pagination = {page: 1, count: 1, perPage: 1};

  query: DraftTemplateListConfig;
  draftTemplates: DraftTemplate[];
  loading = false;
  currentPage = 1;

  limit: number;
  listConfig: DraftTemplateListConfig = {
    type: 'all',
    filters: {}
  };

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private draftTemplatesService: DraftTemplatesService,
    private draftTemplateFilesService: DraftTemplateFilesService,
    private draftTemplatesActionStateMap: DraftTemplatesActionMapping,
  ) {
  }

  config(config: DraftTemplateListConfig) {
    if (config) {
      this.limit = 10;
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  ngOnInit() {
    this.singleActions = this.draftTemplatesActionStateMap.getSingleActions();
    this.otherSingleActions = this.draftTemplatesActionStateMap.getOtherSingleActions();
    this.commonActions = this.draftTemplatesActionStateMap.getCommonActions();
    this.config(this.listConfig);
  }

  goBack() {
    this.router.navigateByUrl('administration').then();
  }

  setToPage(pageNumber: number) {
    this.currentPage = pageNumber;
    this.runQuery();
  }

  gotoPage(numPage) {
    this.setToPage(numPage);
  }

  runQuery() {
    this.loading = true;
    this.draftTemplates = null;

    // Create limit and offset filter (if necessary)
    if (this.limit) {
      this.query.filters.limit = this.limit;
      this.query.filters.offset = (this.limit * (this.currentPage - 1));
      this.query.filters.page = this.currentPage;
    }

    this.draftTemplatesService.query(this.query)
      .subscribe(data => {
        this.loading = false;
        this.draftTemplates = data.draftTemplates;
        this.pagination = data.pagination;
      });
  }

  applyFilter(event: any) {
    this.query.filters.searchedText = event.trim();
    this.reload();
  }


  hasFilter(): boolean {
    return this.query.filters.searchedText && this.query.filters.searchedText !== '';
  }

  createDraftTemplate() {
    const modalRef = this.modalService.open(DraftTemplateModalCreateComponent, {
      backdrop: 'static',
      centered: true,
      size: 'lg'
    });
    modalRef.componentInstance.isCreated.subscribe(() => {
      this.reload();
    });
  }

  reload() {
    this.setToPage(1);
  }
}
