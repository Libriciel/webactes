import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PageParameters } from '../../../../wa-common/components/page/model/page-parameters';
import { TypeActsListComponent } from '../type-acts-list/type-acts-list.component';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { TypeActsMessages } from '../i18n/type-acts-messages';
import { AdminMessages } from '../../../i18n/admin-messages';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_TYPES_ACT_LIST_PATH,
    component: TypeActsListComponent,
    data: {
      pageParameters: {
        title: TypeActsMessages.TYPE_ACT,
        breadcrumb: [{
          name: AdminMessages.PAGE_ADMIN_TITLE,
          location: RoutingPaths.ADMIN_PATH
        }, {
          name: TypeActsMessages.TYPE_ACT, location: RoutingPaths.ADMIN_TYPES_ACT_LIST_PATH
        }
        ],
        fluid: true
      } as PageParameters
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class TypeActsRoutingModule {
}
