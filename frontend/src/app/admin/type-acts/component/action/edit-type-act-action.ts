import { from, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { IActuator } from '../../../../ls-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionResult } from '../../../../ls-common';
import { catchError, map } from 'rxjs/operators';
import { TypesAct } from '../../../../model/type-act/types-act';
import { TypeActsMessages } from '../i18n/type-acts-messages';
import { TypeActModalComponent } from '../type-acts-modal/type-act-modal.component';

@Injectable({
  providedIn: 'root'
})
export class EditTypeActAction implements IActuator<TypesAct> {

  messages = TypeActsMessages;

  constructor(private modalService: NgbModal) {
  }

  action(typesActs?: TypesAct[]): Observable<ActionResult> {

    const modalRef = this.modalService.open(TypeActModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });

    modalRef.componentInstance.typeAct = typesActs[0];

    return from(modalRef.result).pipe(
      map((editedTypeAct: TypesAct) =>
        ({
          needReload: true,
          message: this.messages.edit_type_act_action_done(editedTypeAct.name)
        } as ActionResult)),
      catchError((error) => {
        console.debug(error);
        return error && error.isError !== false
          ? of({
            error: true,
            needReload: false,
            message: this.messages.UPDATE_TYPE_ACT_ACTION_FAILED
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}
