import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { Injectable } from '@angular/core';
import { TypeActsActionItemFactory } from './type-act-action-item-factory';
import { TypesAct } from '../../../../model/type-act/types-act';
import { TypeActActionEnum } from './type-act-action.enum';

@Injectable({
  providedIn: 'root'
})
export class ActionTypeActMapping {

  constructor(protected typeActActionItemFactory: TypeActsActionItemFactory) {
  }

  getCommonActions(): ActionItemsSet<TypesAct>[] {
    return [
    ];
  }

  getSingleActions(): ActionItemsSet<TypesAct> {
    return {
      actionItems: [
        this.typeActActionItemFactory.getActionItem(TypeActActionEnum.EDIT_TYPE_ACT),
        this.typeActActionItemFactory.getActionItem(TypeActActionEnum.DELETE_TYPE_ACT),
      ]
    };
  }

  getOtherSingleActions(): ActionItemsSet<TypesAct>[] {
    return [];
  }
}
