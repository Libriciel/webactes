import { AvailableActionsBasedValidator } from '../../../../ls-common/components/tables/validator/available-actions-based-validator';
import { TypeActAvailableActions } from '../../../../model/type-act/type-act-available-actions.enum';
import { TypesAct } from '../../../../model/type-act/types-act';

export class DeleteTypeActActionValidator extends AvailableActionsBasedValidator<TypesAct> {

  constructor() {
    super(TypeActAvailableActions.isDeletable);
  }
}
