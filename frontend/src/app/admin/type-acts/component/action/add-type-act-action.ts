import { from, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActionResult, IActuator } from '../../../../ls-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { catchError, map } from 'rxjs/operators';
import { TypesAct } from '../../../../model/type-act/types-act';
import { TypeActsMessages } from '../i18n/type-acts-messages';
import { TypeActModalComponent } from '../type-acts-modal/type-act-modal.component';

@Injectable({
  providedIn: 'root'
})
export class AddTypeActAction implements IActuator<void> {

  messages = TypeActsMessages;

  constructor(private modalService: NgbModal) {
  }

  action(): Observable<ActionResult> {

    const modalRef = this.modalService.open(TypeActModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });

    return from(modalRef.result).pipe(
      map((createdTypeAct: TypesAct) => {
        return {
          needReload: true,
          message: this.messages.add_type_act_action_done(createdTypeAct.name)
        } as ActionResult;
      }),
      catchError((error) => {
        return error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.CREATE_TYPE_ACT_ACTION_FAILED
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}
