import { ActionItem, CommonIcons } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { DeleteTypeActAction } from './delete-type-act-action';
import { TypeActsListMessages } from '../type-acts-list/type-act-list-messages';
import { TypesAct } from '../../../../model/type-act/types-act';
import { TypeActActionEnum } from './type-act-action.enum';
import { DeleteTypeActActionValidator } from './delete-type-act-action-validator';
import { AddTypeActAction } from './add-type-act-action';
import { EditTypeActAction } from './edit-type-act-action';
import { EditTypeActActionValidator } from './edit-type-act-action-validator';

@Injectable({
  providedIn: 'root'
})

export class TypeActsActionItemFactory {

  constructor(private deleteTypeActAction: DeleteTypeActAction,
              private addTypeActAction: AddTypeActAction,
              private editTypeActAction: EditTypeActAction) {
  }

  getActionItem(action: TypeActActionEnum): ActionItem<TypesAct> {
    switch (action) {
      case TypeActActionEnum.DELETE_TYPE_ACT:
        return new ActionItem({
          name: TypeActsListMessages.DELETE_TYPE_ACT,
          icon: CommonIcons.DELETE_ICON,
          style: TypeActsListMessages.DELETE_TYPE_ACT_ACTION_ICON_STYLE,
          actuator: this.deleteTypeActAction,
          actionValidator: new DeleteTypeActActionValidator(),
        });
      case TypeActActionEnum.ADD_TYPE_ACT:
        return new ActionItem({
          name: TypeActsListMessages.CREATE_TYPE_ACT,
          icon: CommonIcons.ADD_ICON,
          style: TypeActsListMessages.ADD_TYPE_ACTS_ACTION_ICON_STYLE,
          actuator: this.addTypeActAction
        });
      case TypeActActionEnum.EDIT_TYPE_ACT:
        return new ActionItem({
          name: TypeActsListMessages.EDIT_TYPE_ACT,
          icon: CommonIcons.EDIT_ICON,
          style: TypeActsListMessages.EDIT_TYPE_ACTS_ACTION_ICON_STYLE,
          actuator: this.editTypeActAction,
          actionValidator: new EditTypeActActionValidator(),
        });
      default:
        throw new Error(`unimplemented action ${action}`);
    }
  }
}
