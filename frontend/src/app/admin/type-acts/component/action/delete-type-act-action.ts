import { Observable, of, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActionResult, IActuator } from '../../../../ls-common/components/tables';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IManageTypeActService } from '../../services/IManageTypeActService';
import { ConfirmPopupComponent } from '../../../../ls-common/components/confirm-popup';
import { CommonMessages } from '../../../../ls-common/i18n';
import { Style } from '../../../../wa-common';
import { CommonIcons } from '../../../../ls-common/icons';
import { TypeActModalMessages } from '../type-acts-modal/type-act-modal-messages';
import { TypesAct } from '../../../../model/type-act/types-act';

@Injectable({
  providedIn: 'root'
})
export class DeleteTypeActAction implements IActuator<TypesAct> {


  constructor(private typeActService: IManageTypeActService, private modalService: NgbModal) {
  }

  action(typeActs: TypesAct[]): Observable<any> {
    if (!typeActs || typeActs.length < 1) {
      console.error('No project selected, action unavailable');
      return of({});
    }

    const returnSubject = new Subject<ActionResult>();

    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    if (typeActs.length > 1) {
      modalRef.componentInstance.title = TypeActModalMessages.CONFIRM_DELETE_MULTIPLE_ACT_TITLE;
      modalRef.componentInstance.content = TypeActModalMessages.build_confirm_delete_multiple_type_acts_message(typeActs.length);
    } else {
      const typeAct = typeActs[0];
      modalRef.componentInstance.title = TypeActModalMessages.CONFIRM_DELETE_SINGLE_ACT_TITLE;
      modalRef.componentInstance.content = TypeActModalMessages.build_confirm_delete_single_act_message(typeAct.name);
    }

    modalRef.componentInstance.confirmMsg = CommonMessages.DELETE;
    modalRef.componentInstance.style = Style.DANGER;
    modalRef.componentInstance.icon = CommonIcons.DELETE_ICON;

    modalRef.result.then(
      () => this.typeActService.deleteTypeActs(typeActs).subscribe(
        () => {
          returnSubject.next({
            error: false,
            needReload: true,
            message: TypeActModalMessages.TYPE_ACT_DELETE_SUCCESS
          });
        },
        error => {
          console.error('Error calling type acts deletion : ', error);
          returnSubject.next({
            error: true,
            needReload: true,
            message: TypeActModalMessages.TYPE_ACT_DELETE_FAILURE
          });
        }),
      () => returnSubject.complete()
    ).catch(error => console.error('Caught error in popup for type acts deletion, this should not happen. Error : ', error));

    return returnSubject.asObservable();
  }
}
