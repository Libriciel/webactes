import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Weight } from '../../../../wa-common/weight.enum';
import { CommonIcons, CommonMessages } from '../../../../ls-common';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { TypesAct } from '../../../../model/type-act/types-act';
import { IManageTypeActService } from '../../services/IManageTypeActService';
import { AbstractControl, FormControl, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { TypeActModalMessages } from './type-act-modal-messages';
import { Nature } from '../../../../model/Nature/nature';
import { IManageNatureService } from '../../services/IManageNatureService';
import { IManageDraftTemplateService } from '../../services/IManageDraftTemplateService';
import { DraftTemplate } from '../../../../model/draft-template/draft-template';
import { IManageGenerateTemplateService } from '../../services/IManageGenerateTemplateService';
import { GenerateTemplate } from '../../../../model/generate-template/generate-template';
import { Counter } from '../../../../model/counter/counter';
import { IManageCountersService } from '../../../counters/services/IManageCountersService';
import { StructureSettingsService } from '../../../../core';
import { ManageTypeActService } from '../../../../ls-common/services/manage-type-act.service';
import { take } from 'rxjs/operators';

export function nameValidator(typeActs): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null =>
    typeActs.find(typeAct => typeAct.name === control.value) ?
      {duplicatedName: true}
      : null;
}

@Component({
  selector: 'wa-act-type-modal',
  templateUrl: './type-act-modal.component.html',
  styleUrls: ['./type-act-modal.component.scss']
})
export class TypeActModalComponent implements OnInit {
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  waCommonMessages = WACommonMessages;
  weight = Weight;
  isSubmitting = false;
  messages = TypeActModalMessages;
  typeAct: TypesAct = new TypesAct();
  availableNatures: Nature[];
  availableDraftTemplates: DraftTemplate[];
  availableProjectGenerateTemplates: GenerateTemplate[];
  availableActGenerateTemplates: GenerateTemplate[];
  typeActForm: UntypedFormGroup;
  isSubmitDisabled = true;
  availableCounters: Counter[] = [];
  availableTypesActs: TypesAct[] = [];
  generateNumber: boolean;
  isActGenerate: boolean;
  isProjectGenerate: boolean;
  isWriting: boolean;
  isSittingEnable: boolean;
  filterString: string;
  numPage = 1;
  limit = 50;

  constructor(
    public activeModal: NgbActiveModal,
    protected typesActService: IManageTypeActService,
    protected natureService: IManageNatureService,
    protected draftTemplateService: IManageDraftTemplateService,
    protected generateTemplateService: IManageGenerateTemplateService,
    protected counterService: IManageCountersService,
    protected structureSettingsService: StructureSettingsService,
    protected manageTypeActService: ManageTypeActService,
  ) {
  }

  ngOnInit() {
    this.generateNumber = this.structureSettingsService.getConfig('act', 'generate_number');
    this.isActGenerate = this.structureSettingsService.getConfig('act', 'act_generate');
    this.isProjectGenerate = this.structureSettingsService.getConfig('project', 'project_generate');
    this.isWriting = this.structureSettingsService.getConfig('project', 'project_writing');
    this.isSittingEnable = this.structureSettingsService.getConfig('sitting', 'sitting_enable');

    this.typeActForm = new UntypedFormGroup({
      name: new UntypedFormControl(this.typeAct.name, Validators.required),
      natures: new UntypedFormControl(this.typeAct.nature, Validators.required),
      isTransferable: new FormControl(this.typeAct.isTransferable, [Validators.required]),
      isDeliberative: new FormControl(this.typeAct.isDeliberative, [Validators.required]),
      counters: new UntypedFormControl(this.typeAct.counter, this.generateNumber ? [Validators.required] : null),
      draftTemplates: new UntypedFormControl(this.typeAct.draftTemplates, this.isWriting ? [Validators.required] : null),
      actGenerateTemplate: new UntypedFormControl(this.typeAct.generate_template_act_id, this.isActGenerate ? [Validators.required] : null),
      projectGenerateTemplate: new UntypedFormControl(this.typeAct.generate_template_project_id, this.isProjectGenerate ? [Validators.required] : null),
    });

    this.manageTypeActService.getAllWithPagination().pipe(take(1)).subscribe(typeActs => {
      this.availableTypesActs = typeActs.typeacts.filter(typeAct => typeAct.id !== this.typeAct.id);
      this.typeActForm.controls['name'].setValidators([Validators.required, nameValidator(this.availableTypesActs)]);
    });

    this.fillData();
  }

  close(): void {
    this.activeModal.dismiss({isError: false});
  }

  /**
   * add sittings if everything's ok
   * promise return {id: sittingId}
   */
  addOrUpdateAct() {
    this.isSubmitting = true;

    const typeAct: TypesAct = this.typeAct.clone();
    typeAct.name = this.typeActForm.controls['name'].value.trim();
    typeAct.nature = this.typeActForm.controls['natures'].value;
    typeAct.isTransferable = this.typeActForm.controls['isTransferable'].value;
    typeAct.isDeliberative = this.isSittingEnable ? this.typeActForm.controls['isDeliberative'].value : false;
    typeAct.counter = this.typeActForm.controls['counters'].value;
    typeAct.draftTemplates = this.typeActForm.controls['draftTemplates'].value;
    typeAct.generate_template_act_id = this.typeActForm.controls['actGenerateTemplate'].value;
    typeAct.generate_template_project_id = this.typeActForm.controls['projectGenerateTemplate'].value;

    const call = !this.typeAct.id
      ? this.typesActService.addTypeAct(typeAct)
      : this.typesActService.updateTypeAct(typeAct);

    call.subscribe(
      () => {
        this.isSubmitting = false;
        this.activeModal.close(typeAct);
      },
      err => {
        console.error(err);
        this.isSubmitting = false;
        this.activeModal.dismiss(err);
      });
  }

  private fillData() {
    const filters = this.filterString ? {searchedText: this.filterString} : {searchedText: ''};
    if (this.typeAct.id === 0) {
      this.typeAct.nature = null;
    } else {
      this.typeActForm.patchValue({isTransferable: this.typeAct.isTransferable});
      this.typeActForm.patchValue({isDeliberative: this.typeAct.isDeliberative});
    }

    this.natureService.getAllWithoutPagination().subscribe(natures => {
      this.availableNatures = natures;
      if (this.typeAct.id && this.typeAct.nature.id) {
        this.typeAct.nature = this.availableNatures.find(nature => nature.id === this.typeAct.nature.id);
        this.typeActForm.controls['natures'].setValue(this.typeAct.nature);
      }
    });

    this.draftTemplateService.getAllWithoutPagination().subscribe(draftTemplates => this.availableDraftTemplates = draftTemplates);

    this.generateTemplateService.getAllByTypeCode('project').subscribe((generateTemplates) => {
      this.availableProjectGenerateTemplates = generateTemplates;
    });

    this.generateTemplateService.getAllByTypeCode('act').subscribe((generateTemplates) => {
      this.availableActGenerateTemplates = generateTemplates;
    });

    this.counterService.getAllWithoutPagination(filters).subscribe((counters) => {
      this.availableCounters = counters.counters;
      if (this.typeAct.id) {
        this.typeAct.counter = this.availableCounters.find(counter => counter.id === this.typeAct.counterId);
        this.typeActForm.controls['counters'].setValue(this.typeAct.counter);
      }
    });

    this.typesActService.getAllWithPagination().subscribe((typesacts) => {
      this.availableTypesActs = typesacts.typeacts;
    });
  }


  get isTransferable() {

    return this.typeActForm.controls['isTransferable'].value;
  }

  get isDeliberative() {
    return this.typeActForm.controls['isDeliberative'].value;
  }
}
