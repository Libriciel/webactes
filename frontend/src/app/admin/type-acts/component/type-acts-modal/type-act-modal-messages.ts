
export class TypeActModalMessages {

  static SITTING_TYPE_ADD = 'Créer';
  static TYPE_SITTING_UPDATE = 'Enregistrer';
  static NEW_TYPE_SITTING_NAME = 'Libellé';
  static DUPLICATED_TYPEACT_ERROR_MESSAGE = 'Ce type d\'acte a déjà été créé';

  static CONFIRM_DELETE_MULTIPLE_ACT_TITLE = 'Supprimer plusieurs types d\'actes';
  static CONFIRM_DELETE_SINGLE_ACT_TITLE = 'Supprimer le type de d\'acte';
  static TYPE_ACT_DELETE_SUCCESS = 'Type d\'acte supprimé avec succès!';
  static TYPE_ACT_DELETE_FAILURE = 'Une erreur est survenue pendant la suppression du type d\'acte.';
  static UPDATE_NEW_TYPE_ACT = 'Modification d\'un type d\'acte';
  static CREATE_NEW_TYPE_ACT = 'Création d\'un type d\'acte';
  static NEW_TYPE_ACT_NAME_PLACE_HOLDER = 'Veuillez saisir le libellé du type d\'acte';
  static TYPE_ACT_IS_DELIBERATING = 'L\'acte est-il délibérant ?';
  static FORM_IS_TRANSMISSIBLE = 'L\'acte est-il télétransmissible ?';
  static NATURES_LIST = 'Nature';
  static NATURE_PLACE_HOLDER = 'Veuillez sélectionner une nature dans la liste déroulante.';
  static DRAFT_TEMPLATE_LIST = 'Gabarits';
  static DRAFT_TEMPLATE_PLACE_HOLDER = 'Veuillez sélectionner des gabarits dans la liste déroulante.';
  static GENERATE_TEMPLATE_PROJECT_LIST = 'Modèle de projet';
  static GENERATE_TEMPLATE_PROJECT_HOLDER = 'Veuillez sélectionner un modèle de projet dans la liste déroulante.';
  static GENERATE_TEMPLATE_ACT_LIST = 'Modèle d\'acte';
  static GENERATE_TEMPLATE_ACT_HOLDER = 'Veuillez sélectionner un modèle d\'acte dans la liste déroulante.';
  static ACT_TYPE_UPDATE = "Modifier";
  static ACT_TYPE_ADD = "Créer";
  static COUNTER_LIST = 'Compteurs' ;
  static COUNTER_PLACE_HOLDER = 'Veuillez choisir un compteur';

  static build_confirm_delete_single_act_message(typeActName: string): string {
    return `Êtes-vous sûr de vouloir supprimer le type d'acte "${typeActName}"?`;
  }

  static build_confirm_delete_multiple_type_acts_message(numberOfTypeActs: string | number): string {
    return `Êtes-vous sûr de vouloir supprimer ${numberOfTypeActs} types d'actes?`;
  }
}
