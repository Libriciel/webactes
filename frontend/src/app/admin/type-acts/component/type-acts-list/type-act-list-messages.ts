import { Style } from '../../../../wa-common';

export class TypeActsListMessages {
  static CREATE_TYPE_ACT = "Créer un type d\'acte";
  static ADD_TYPE_ACTS_ACTION_ICON_STYLE =  Style.NORMAL;
  static EDIT_TYPE_ACT = "Modifier un type d\'acte";
  static EDIT_TYPE_ACTS_ACTION_ICON_STYLE =  Style.NORMAL;
  static DELETE_TYPE_ACT_ACTION_ICON_STYLE = Style.DANGER;
  static DELETE_TYPE_ACT = "";

}
