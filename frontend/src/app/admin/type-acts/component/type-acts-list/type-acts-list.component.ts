import { Component, OnInit } from '@angular/core';
import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { IManageTypeActService } from '../../services/IManageTypeActService';
import { TypeActsMessages } from '../i18n/type-acts-messages';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { CommonMessages } from '../../../../ls-common';
import { TypesAct } from '../../../../model/type-act/types-act';
import { ActionTypeActMapping } from '../action/action-type-act-mapping';
import { CommonIcons, NotificationService } from '../../../../ls-common';
import { TypeActsActionItemFactory } from '../action/type-act-action-item-factory';
import { TypeActActionEnum } from '../action/type-act-action.enum';
import { Pagination } from '../../../../model/pagination';

@Component({
  selector: 'wa-type-acts-list',
  templateUrl: './type-acts-list.component.html',
  styleUrls: ['./type-acts-list.component.scss']
})
export class TypeActsListComponent implements OnInit {

  typesActs: TypesAct[];
  commonMessages = CommonMessages;
  public messages = TypeActsMessages;

  commonActions: ActionItemsSet<TypesAct>[];
  singleActions: ActionItemsSet<TypesAct>;
  otherSingleActions: ActionItemsSet<TypesAct>[];
  loading =  true;

  commonIcons = CommonIcons;

  typeActAction = TypeActActionEnum;

  filterString: string;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};

  constructor(
    protected iManageTypeAct: IManageTypeActService,
    protected router: Router,
    protected  activatedRoute: ActivatedRoute,
    protected  actionTypeActStateMap: ActionTypeActMapping,
    protected  notificationService: NotificationService,
    public typeActActionItemFactory: TypeActsActionItemFactory,
    protected  typesActService: IManageTypeActService,
  ) {
  }

  ngOnInit() {

    this.gotoPage(1);

    this.activatedRoute.data.subscribe(() => {
      this.singleActions = this.actionTypeActStateMap.getSingleActions();
      this.otherSingleActions = this.actionTypeActStateMap.getOtherSingleActions();
      this.commonActions = this.actionTypeActStateMap.getCommonActions();
    });
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

  applyFilter(event) {
    this.filterString = event.trim();
    this.reloadTypesActs();
  }

  hasFilter(): boolean {
    return this.filterString && this.filterString !== '';
  }

  toggleActivation(typeAct: TypesAct, value: boolean, button: any) {
    this.typesActService.switchTypeActActiveState(typeAct, value).subscribe(result => {
      if (!result.success) {
        button.writeValue(!value);
        this.notificationService.showError(value ?
          this.messages.TYPE_ACT_ACTIVATION_FAILED_TOASTR_TXT
          : this.messages.TYPE_ACT_DEACTIVATION_FAILED_TOASTR_TXT);
      } else {
        this.notificationService.showSuccess(this.messages.switch_type_act_active_done(typeAct.name, typeAct.active));
      }
    });
  }

  public actionDone($event: { error: any; message: string; needReload: any; }) {
    if ($event) {
      if ($event.error) {
        if ($event.message) {
          this.notificationService.showError($event.message);
        }
        this.reloadTypesActs();
      } else {
        if ($event.message) {
          this.notificationService.showSuccess($event.message);
        }
        if ($event.needReload) {
          this.reloadTypesActs();
        }
      }
    }
  }

  gotoPage(numPage) {
    this.typesActs = null;
    this.loading = true;
    const filters = this.filterString ? {searchedText: this.filterString} : {searchedText: ''};

    this.iManageTypeAct.getAllWithPagination(numPage, filters).subscribe((typeActs) => {
      this.typesActs = typeActs.typeacts;
      this.pagination = typeActs.pagination;
      this.loading = false;
    });
  }

   reloadTypesActs() {
    this.gotoPage(1);
  }
}
