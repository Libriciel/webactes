export class TypeActsMessages {
  static TYPE_ACT = `Type d'actes`;
  static NAME = 'Libellé';
  static NATURE = 'Nature';
  static TDT = 'Télétransmission';
  static CREATE_TYPE_ACT = `Créer un type d'acte`;
  static UPDATE_TYPE_ACT_ACTION_FAILED = `Une erreur est survenue lors de la modification du type d'acte.`;
  static CREATE_TYPE_ACT_ACTION_FAILED = `Une erreur est survenue lors de la création du type d'acte.`;
  static TYPE_ACT_ACTIVATION_FAILED_TOASTR_TXT = `L'activation du tye d'acte a échoué`;
  static TYPE_ACT_DEACTIVATION_FAILED_TOASTR_TXT = `La désactivation du tye d'acte a échoué`;

  static add_type_act_action_done(typeActName: string) {
    return `Le type d'acte "${typeActName}" a été créé avec succès.`;
  }

  static edit_type_act_action_done(typeActName: string) {
    return `Le type d'acte "${typeActName}" a été modifié avec succès.`;
  }

  static switch_type_act_active_done(typeActName: string, active: boolean) {
    return active ? `Le type d'acte "${typeActName}" a été activé avec succès.` : `Le type d'acte "${typeActName}" a été désactivé avec succès.`;
  }
}
