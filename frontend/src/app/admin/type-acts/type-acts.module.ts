import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TypeActsListComponent } from './component/type-acts-list/type-acts-list.component';
import { TypeActsRoutingModule } from './component/type-acts-routing/type-acts-routing.module';
import { TypeActModalComponent } from './component/type-acts-modal/type-act-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [TypeActsListComponent, TypeActModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    LibricielCommonModule,
    TypeActsRoutingModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule,
  ]
})


export class TypeActsModule {
}
