import { Observable } from 'rxjs';
import { GenerateTemplate } from '../../../model/generate-template/generate-template';

export abstract class IManageGenerateTemplateService {
  abstract getAllByTypeCode($code): Observable<GenerateTemplate[]>;
}
