import { Observable } from 'rxjs';
import { TypesAct } from '../../../model/type-act/types-act';
import { FilterType } from '../../../model/util/filter-type';
import { Pagination } from '../../../model/pagination';
import { TypeactFilterType } from '../../../model/util/typeact-filter-type';

export abstract class IManageTypeActService {

  abstract getAllWithPagination(numPage?: number, filters?: FilterType): Observable<{ pagination: Pagination; typeacts: TypesAct[] }>;

  abstract getAllWithoutPagination(filters?: TypeactFilterType): Observable<{ typeacts: TypesAct[] }>;

  abstract getAll(): Observable<TypesAct[]>;

  abstract updateTypeAct(typesAct: TypesAct): Observable<TypesAct>;

  abstract deleteTypeActs(typesActs: TypesAct[]);

  abstract switchTypeActActiveState(typeAct: TypesAct, value: boolean): Observable<{typeAct: TypesAct, success: boolean } | { success: boolean }>;

  abstract addTypeAct(typeAct: TypesAct): Observable<TypesAct>;

}
