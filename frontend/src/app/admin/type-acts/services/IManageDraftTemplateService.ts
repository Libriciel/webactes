import { Observable } from 'rxjs';
import { DraftTemplate } from '../../../model/draft-template/draft-template';

export abstract class IManageDraftTemplateService {
  abstract getAllWithPagination(): Observable<DraftTemplate[]>;
  abstract getAllWithoutPagination(): Observable<DraftTemplate[]>;
}
