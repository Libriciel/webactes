import { Observable } from 'rxjs';
import { Nature } from '../../../model/Nature/nature';

export abstract class IManageNatureService {
  abstract getAllWithoutPagination(): Observable<Nature[]>;
}
