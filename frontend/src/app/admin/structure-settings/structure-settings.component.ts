import { Component, HostBinding, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { StructureSetting, StructureSettingsService } from '../../core';
import { RoutingPaths } from '../../wa-common/routing-paths';
import { Router } from '@angular/router';
import { StructureSettingsHelpMessage } from './i18n/structure-settings-help-message';

@Component({
  selector: 'wa-structure-settings',
  templateUrl: './structure-settings.component.html',
  styleUrls: ['./structure-settings.component.scss']
})
export class StructureSettingsComponent implements OnInit {
  @HostBinding() class = 'wa-tabs';

  public structureSetting: StructureSetting;
  messages = StructureSettingsHelpMessage;

  constructor(
    protected router: Router,
    protected structureSettingsService: StructureSettingsService,
    //protected notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.structureSettingsService.get().subscribe(
      structureSetting => {
        this.structureSetting = structureSetting;
      },
      () => {
        //this.notificationService.showError(NotificationsOptionsMessages.NOTIFICATIONS_OPTIONS_GET_ERROR);
        return of(null);
      });
  }

  valueChanged(groupCode: string, keyCode: string, value: boolean) {
    this.structureSettingsService.saveTypeBoolean(groupCode, keyCode, value)
      .subscribe(
        () => {
        },
        () => {
          //this.notificationService.showError(NotificationsOptionsMessages.NOTIFICATIONS_OPTIONS_SAVE_ERROR);
          //option.active = !value;
        });
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

}
