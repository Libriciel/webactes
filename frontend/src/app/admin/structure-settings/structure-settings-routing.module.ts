import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageParameters } from 'src/app/wa-common/components/page/model/page-parameters';
import { StructureSettingsComponent } from './structure-settings.component';

const routes: Routes = [
  {
    path: 'administration/structure-settings',
    component: StructureSettingsComponent,
    data: {
      pageParameters: {
        title: `Configuration`,
        breadcrumb: [
          {
            name: `Administration`,
            location: `administration`
          },
          {
            name: `Configuration`, location: 'administration/structure-settings'
          }
        ],
        fluid: true
      } as PageParameters
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StructureSettingsRoutingModule {
}
