export class StructureSettingsHelpMessage {
  static USE_NOTICE = 'Toute modification de configuration nécessite une déconnexion/reconnexion des utilisateurs pour être prise en compte.';
}
