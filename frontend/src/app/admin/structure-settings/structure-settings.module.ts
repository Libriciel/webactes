import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StructureSettingsComponent } from './structure-settings.component';
import { StructureSettingsRoutingModule } from './structure-settings-routing.module';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { SharedModule } from '../../shared';

@NgModule({
  declarations: [
    StructureSettingsComponent,
  ],
  imports: [
    CommonModule,
    StructureSettingsRoutingModule,
    LibricielCommonModule,
    SharedModule
  ],
  exports: [StructureSettingsComponent]
})
export class StructureSettingsModule {
}
