import { Component, OnInit } from '@angular/core';
import { AdminMessages } from '../../../../i18n/admin-messages';
import { CommonMessages, NotificationService } from '../../../../../ls-common';
import { IManageConnexionAdministrationService } from '../../../services/imanage-connexion-administration-service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnexionPastell } from '../../../model/connexion-pastell';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RoutingPaths } from '../../../../../wa-common/routing-paths';
import { CommonStylesConstants } from '../../../../../wa-common/common-styles-constants';
import { Weight } from '../../../../../wa-common/weight.enum';

@Component({
  selector: 'wa-configuration-pastell-page',
  templateUrl: './configuration-pastell-page.component.html',
  styleUrls: ['./configuration-pastell-page.component.scss']
})
export class ConfigurationPastellPageComponent implements OnInit {

  messages = AdminMessages;
  commonMessages = CommonMessages;
  commonStyles = CommonStylesConstants;
  weight = Weight;
  isSubmitting = false;
  isSubmitDisabled = true;
  isTesting = false;
  testSuccess = false;
  connexionForm = new FormGroup({
    pastellUrl: new FormControl(null, [
      Validators.required
    ]),
    username: new FormControl(null, [
      Validators.required
    ]),
    password: new FormControl(null, []),
    pastellEntityId: new FormControl(null, [
      Validators.required
    ]),
    slowUrl: new FormControl(null, [
      Validators.required
    ])
  });
  testMessage = null;

  constructor(
    protected notificationService: NotificationService,
    protected connexionAdministrationService: IManageConnexionAdministrationService,
    protected router: Router,
    protected route: ActivatedRoute,
  ) {
  }

  ngOnInit() {

    this.connexionForm.valueChanges.subscribe(() => {
      this.isSubmitDisabled = !this.connexionForm.valid;
    });

    this.route.data.subscribe(data => {
      this.connexionForm.get('pastellUrl').setValue(data.connexionPastell.pastellUrl
        .substring(this.commonMessages.HTTPS_PROTOCOL.length, data.connexionPastell.pastellUrl.length));
      this.connexionForm.get('username').setValue(data.connexionPastell.username);
      this.connexionForm.get('password').setValue(data.connexionPastell.password);
      this.connexionForm.get('pastellEntityId').setValue(data.connexionPastell.pastellEntityId);
      this.connexionForm.get('slowUrl').setValue(data.connexionPastell.slowUrl
        .substring(this.commonMessages.HTTPS_PROTOCOL.length, data.connexionPastell.slowUrl.length));
      // this.isSubmitDisabled = false;
    });

  }

  save() {
    // this.isSubmitting = true;
    this.connexionAdministrationService.savePastellConnexion(this.getConnexionFromValues())
      .subscribe(
        () => {
          this.notificationService.showSuccess(this.messages.CONNEXION_PASTELL_SUCCESS_MESSAGE);
        },
        error => {
          console.error(error);
          this.notificationService.showError(this.messages.CONNEXION_PASTELL_ERROR_MESSAGE);
        })
      .add(
        () => {
          return this.isSubmitting = false;
        });
  }

  testConnexion() {
    this.isTesting = true;
    // this.isSubmitDisabled = true;
    this.connexionAdministrationService.checkPastellConnexion(this.getConnexionFromValues())
      .subscribe(
        value => {
          this.testSuccess = value;
          this.testMessage = value ? this.messages.PASTELL_CONNEXION_OK : this.messages.PASTELL_CONNEXION_KO;
        }).add(
      () => {
        this.isTesting = false;
        // this.isSubmitDisabled = false;
      });
  }

  getConnexionFromValues(): ConnexionPastell {
    const connexionPastell: ConnexionPastell = new ConnexionPastell(
      {
        pastellUrl: this.connexionForm.get('pastellUrl').value,
        username: this.connexionForm.get('username').value,
        password: this.connexionForm.get('password').value,
        pastellEntityId: this.connexionForm.get('pastellEntityId').value,
        slowUrl: this.connexionForm.get('slowUrl').value
      });
    return connexionPastell.withProtocol();
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }
}
