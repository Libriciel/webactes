import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { AdminMessages } from '../../../i18n/admin-messages';
import { PageParameters } from '../../../../wa-common/components/page/model/page-parameters';
import { ConfigurationPastellPageComponent } from '../../components/configuration-pastell-page/configuration-pastell-page/configuration-pastell-page.component';
import { ConnexionPastellResolver } from '../../resolver/connexion-pastell-resolver';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_CONNEXION_PASTELL_PATH,
    component: ConfigurationPastellPageComponent,
    resolve: {
      connexionPastell: ConnexionPastellResolver
    },
    data: {
      pageParameters: {
        title: AdminMessages.PAGE_CONNEXION_PASTELL_TITLE,
        breadcrumb: [
          {
            name: AdminMessages.PAGE_ADMIN_TITLE,
            location: RoutingPaths.ADMIN_PATH
          },
          {
            name: AdminMessages.PAGE_CONNEXION_PASTELL_TITLE,
            location: `/${RoutingPaths.ADMIN_CONNEXION_PASTELL_PATH}`
          }
        ]
      } as PageParameters
    },
    children: [
      {path: '**', redirectTo: RoutingPaths.ADMIN_PATH, pathMatch: 'full'}
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ConnexionPastellRoutingModule {
}
