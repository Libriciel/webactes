import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { ConnexionPastell } from '../model/connexion-pastell';
import { Observable } from 'rxjs';
import { IManageConnexionAdministrationService } from '../services/imanage-connexion-administration-service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConnexionPastellResolver implements Resolve<ConnexionPastell> {

  constructor(protected connexionAdministrationService: IManageConnexionAdministrationService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ConnexionPastell> {
    return this.connexionAdministrationService.getPastellConnexion();
  }
}
