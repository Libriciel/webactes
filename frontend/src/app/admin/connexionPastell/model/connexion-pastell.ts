import { CommonMessages } from '../../../ls-common/i18n/common-messages';

export class ConnexionPastell {
  username: string;
  password: string;
  pastellUrl: string;
  pastellEntityId: number;
  slowUrl?: string;

  constructor(data: { password: string; pastellEntityId: number; pastellUrl: string; username: string; slowUrl?: string }) {
    this.pastellUrl = data.pastellUrl;
    this.slowUrl = data.slowUrl;
    this.password = data.password;
    this.pastellEntityId = data.pastellEntityId;
    this.username = data.username;
  }

  withProtocol(): ConnexionPastell {
    const clone = new ConnexionPastell(this);
    clone.pastellUrl = CommonMessages.HTTPS_PROTOCOL + clone.pastellUrl;
    clone.slowUrl = CommonMessages.HTTPS_PROTOCOL + clone.slowUrl;
    return clone;
  }
}
