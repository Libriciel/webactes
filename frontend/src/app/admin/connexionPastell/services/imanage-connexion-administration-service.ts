import { Observable } from 'rxjs';
import { ConnexionPastell } from '../model/connexion-pastell';

export abstract class IManageConnexionAdministrationService {

  abstract checkPastellConnexion(connexionPastell: ConnexionPastell): Observable<boolean> ;

  abstract getPastellConnexion(): Observable<ConnexionPastell> ;

  abstract savePastellConnexion(connexionPastell: ConnexionPastell): Observable<any> ;

}
