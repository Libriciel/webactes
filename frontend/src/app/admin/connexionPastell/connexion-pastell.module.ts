import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationPastellPageComponent } from './components/configuration-pastell-page/configuration-pastell-page/configuration-pastell-page.component';
import { ConnexionPastellRoutingModule } from './routing/connexion-pastell-routing/connexion-pastell-routing.module';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LsComposantsModule } from '@libriciel/ls-composants';

@NgModule({
  declarations: [ConfigurationPastellPageComponent],
  imports: [
    CommonModule,
    ConnexionPastellRoutingModule,
    LibricielCommonModule,
    FormsModule,
    ReactiveFormsModule,
    LsComposantsModule
  ]
})
export class ConnexionPastellModule {
}
