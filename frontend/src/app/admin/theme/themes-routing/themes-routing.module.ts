import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { ThemesListComponent } from '../components/themes-list/themes-list.component';
import { RouterModule, Routes } from '@angular/router';
import { AdminMessages } from '../../i18n/admin-messages';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { ThemesMessages } from '../ThemesMessages';



const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_THEME_PATH,
    component: ThemesListComponent,
    data: {
      pageParameters: {
        title: ThemesMessages.THEMES,
        breadcrumb: [{
          name: AdminMessages.PAGE_ADMIN_TITLE,
          location: RoutingPaths.ADMIN_PATH
        }
        ,
          {name: ThemesMessages.THEMES, location: RoutingPaths.ADMIN_THEME_PATH}
        ],
        fluid: true
      } as PageParameters
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})

export class ThemesRoutingModule { }

