export enum ThemeActions {
  SHOW_THEME,
  DELETE_THEME,
  EDIT_THEME,
  ADD_THEME
}
