import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IActuator, NotificationService } from '../../../ls-common';
import { from, Observable, of } from 'rxjs';
import { Theme } from '../../../model/theme/theme';
import { Injectable } from '@angular/core';
import { ActionResult } from 'src/app/ls-common/components/tables/actuator/action-result';
import { ThemesMessages } from '../ThemesMessages';
import { catchError, map } from 'rxjs/operators';
import { ThemeModalComponent } from '../components/modals/theme-modal/theme-modal.component';


@Injectable({
  providedIn: 'root'
})

export class EditThemeAction implements IActuator<Theme> {

  messages = ThemesMessages;

  constructor(protected notificationService: NotificationService,
              protected modalService: NgbModal) {
  }

  action(themes: Theme[]): Observable<ActionResult> {

    const modalRef = this.modalService.open(ThemeModalComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });
    if (!themes || themes.length === 0 || themes.length > 1) {
      throw new Error('Edit Action must be called on one and only one Theme.');
    }
    modalRef.componentInstance.originalTheme = themes[0];

    return from(modalRef.result).pipe(
      map((updatedTheme: Theme) =>
        ({
          needReload: true,
          message: this.messages.update_theme_action_done(updatedTheme.name)
        } as ActionResult)),
      catchError((error) =>
        error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.UPDATE_THEME_ACTION_FAILED
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult))
    );
  }
}
