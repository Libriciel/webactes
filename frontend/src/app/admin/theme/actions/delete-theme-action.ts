import { Observable, Subject } from 'rxjs';
import { Theme } from '../../../model/theme/theme';
import { ConfirmPopupComponent } from 'src/app/ls-common/components/confirm-popup/components/confirm-popup.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionResult } from 'src/app/ls-common/components/tables/actuator/action-result';
import { Style } from '../../../wa-common';
import { Injectable } from '@angular/core';
import { ThemesMessages } from '../ThemesMessages';
import { CommonIcons, CommonMessages, IActuator } from '../../../ls-common';
import { IManageThemeService } from '../services/IManageThemeService';
import { NotificationService } from 'src/app/ls-common/services/notification.service';


@Injectable({
  providedIn: 'root'
})

export class DeleteThemeAction implements IActuator<Theme> {

  messages = ThemesMessages;

  constructor(protected manageThemeService: IManageThemeService,
              private modalService: NgbModal,
              protected notificationService: NotificationService) {
  }

  action(theme: Theme[]): Observable<ActionResult> {

    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    const returnSubject = new Subject<ActionResult>();

    modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_THEME_TITLE;
    const themeToDelete = theme[0];
    modalRef.componentInstance.content = ThemesMessages.build_confirm_delete_theme_message(themeToDelete.name);
    modalRef.componentInstance.confirmMsg = CommonMessages.DELETE;
    modalRef.componentInstance.style = Style.DANGER;
    modalRef.componentInstance.icon = CommonIcons.DELETE_ICON;

    modalRef.result.then(
      () => this.manageThemeService.deleteThemes(theme)
        .subscribe(() => {
            returnSubject.next({
              error: false,
              needReload: true,
              message: ThemesMessages.THEME_DELETE_SUCCESS
            });
          },
          error => {
            console.error('Error calling project deletion : ', error);
            returnSubject.next({
              error: true,
              needReload: true,
              message: ThemesMessages.THEME_DELETE_FAILURE
            });
          }),
      () => returnSubject.complete()
    ).catch(error => console.error('Caught error in popup for theme deletion, this should not happen. Error : ', error));

    return returnSubject.asObservable();
  }
}
