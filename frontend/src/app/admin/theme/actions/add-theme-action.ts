import { from, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActionResult, IActuator } from '../../../ls-common';
import { Theme } from '../../../model/theme/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { catchError, map } from 'rxjs/operators';
import { ThemesMessages } from '../ThemesMessages';
import { NotificationService } from 'src/app/ls-common/services/notification.service';
import { ThemeModalComponent } from '../components/modals/theme-modal/theme-modal.component';


@Injectable({
  providedIn: 'root'
})
export class AddThemeAction implements IActuator<void> {

  messages = ThemesMessages;

  constructor(protected modalService: NgbModal,
              protected notificationService: NotificationService) {
  }

  action(): Observable<ActionResult> {

    const modalRef = this.modalService.open(ThemeModalComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    return from(modalRef.result).pipe(
      map((createdTheme: { Themes: Theme }) =>
        ({
          needReload: true,
          message: this.messages.createdTheme(createdTheme.Themes.name),
        } as ActionResult)),
      catchError((error) =>
        error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.ADD_THEME_ERROR_MESSAGE
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult))
    );
  }
}
