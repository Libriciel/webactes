import { AddThemeAction } from './add-theme-action';
import { IManageThemeService } from '../services/IManageThemeService';
import { ThemeActions } from './theme-actions.enum';
import { Injectable } from '@angular/core';
import { Style } from '../../../wa-common';
import { ActionItem, CommonIcons, NotificationService } from '../../../ls-common';
import { Theme } from '../../../model/theme/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ThemesMessages } from '../ThemesMessages';
import { DeleteThemeAction } from './delete-theme-action';
import { DeleteThemeActionValidator } from './delete-theme-action-validator';
import { EditThemeActionValidator } from './edit-theme-action-validator';
import { EditThemeAction } from './edit-theme-action';


@Injectable({
  providedIn: 'root'
})
export class ThemeActionItemFactory {

  constructor(protected modalService: NgbModal,
              protected notificationService: NotificationService,
              protected iManageThemeService: IManageThemeService) {
  }

  getActionItem(action: ThemeActions): ActionItem<Theme> {
    switch (action) {
      case ThemeActions.ADD_THEME:
        return new ActionItem({
          name: ThemesMessages.CREATE_THEME,
          icon: CommonIcons.ADD_ICON,
          style: ThemesMessages.ADD_THEME_ACTION_ICON_STYLE,
          actuator: new AddThemeAction(this.modalService, this.notificationService)
        });

      case ThemeActions.DELETE_THEME:
        return new ActionItem({
          name: ThemesMessages.DELETE_THEME_ACTION_NAME,
          icon: CommonIcons.DELETE_ICON,
          style: Style.DANGER,
          actuator: new DeleteThemeAction(this.iManageThemeService, this.modalService, this.notificationService),
          actionValidator: new DeleteThemeActionValidator(),
        });

      case ThemeActions.EDIT_THEME:
        return new ActionItem({
          name: ThemesMessages.EDIT_ACTION_NAME,
          longName: ThemesMessages.EDIT_ACTION_LONG_NAME,
          icon: CommonIcons.EDIT_ICON,
          actuator: new EditThemeAction(this.notificationService, this.modalService),
          actionValidator: new EditThemeActionValidator(),
        });

      default:
        throw new Error(`unimplemented action ${action}`);
    }
  }
}
