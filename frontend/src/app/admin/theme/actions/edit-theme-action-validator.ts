import { ThemeRight } from '../../../model/theme/theme-right.enum';
import { Theme } from 'src/app/model/theme/theme';
import { AvailableActionsBasedValidator } from '../../../ls-common/components/tables/validator/available-actions-based-validator';

export class EditThemeActionValidator extends AvailableActionsBasedValidator<Theme> {

  constructor() {
    super(ThemeRight.isEditable);
  }
}
