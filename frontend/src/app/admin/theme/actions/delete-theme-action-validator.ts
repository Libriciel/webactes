import { ThemeRight } from '../../../model/theme/theme-right.enum';
import { Theme } from '../../../model/theme/theme';
import { AvailableActionsBasedValidator } from '../../../ls-common/components/tables/validator/available-actions-based-validator';

export class DeleteThemeActionValidator extends AvailableActionsBasedValidator<Theme> {

  constructor() {
    super(ThemeRight.isDeletable);
  }
}
