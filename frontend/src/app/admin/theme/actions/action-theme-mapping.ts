import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { Theme } from '../../../model/theme/theme';
import { Injectable } from '@angular/core';
import { ThemeActionItemFactory } from './theme-action-item-factory';
import { ThemeActions } from './theme-actions.enum';

@Injectable({
  providedIn: 'root'
})
export class ActionThemeMapping {

  constructor(protected themeActionItemFactory: ThemeActionItemFactory) {
  }

  getSingleActions(): ActionItemsSet<Theme> {
    return {
      actionItems: [
        this.themeActionItemFactory.getActionItem(ThemeActions.EDIT_THEME),
        this.themeActionItemFactory.getActionItem(ThemeActions.DELETE_THEME)
      ]
    };
  }

  getOtherSingleActions(): ActionItemsSet<Theme>[] {
    return [];
  }

}
