export class ThemeModalMessages {
  static CREATE_NEW_THEME = `Création d'un thème`;
  static UPDATE_THEME = 'Modification d\'un thème';
  static PARENT_THEME = 'Thème parent';
  static MAIN_THEME_PLACE_HOLDER = 'Choisissez un thème principal';
  static SORT_CRITERIA_PLACE_HOLDER = 'Indiquez un critère de tri';
  static THEME_NAME_PLACE_HOLDER = 'Indiquez le nom du thème';
  static ADD_THEME = 'Créer le thème';
  static SAVE_THEME = 'Enregistrer';
  static THEME_NAME = 'Nom du thème';
  static SORT_CRITERIA_NAME = 'Critère de tri';
  public static ERROR_MESSAGE = 'La création du thème a échoué.';
  public static SPECIAL_CHARACTERS_ERROR_MESSAGE = 'Le champ ne doit pas contenir de caractères spéciaux.';
  public static DUPLICATED_NAME_ERROR_MESSAGE = 'Ce thème a déjà été créé.';
  public static DUPLICATED_POSITION_ERROR_MESSAGE = 'Ce critère de tri a déjà été créé.';

  public static parentPosition(parentPosition: string) {
    return `Le critère de tri du thème parent est : "${parentPosition}".`;
  }

}
