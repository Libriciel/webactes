import { IManageThemeService } from 'src/app/admin/theme/services/IManageThemeService';
import { Component, Input, OnInit } from '@angular/core';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../../ls-common';
import { WACommonMessages } from '../../../../../wa-common/i18n/wa-common-messages';
import { Theme } from '../../../../../model/theme/theme';
import { ThemeModalMessages } from '../theme-modal-messages';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { first } from 'rxjs/operators';
import { TreeUtils } from '../../../../../utility/TreeUtils';

@Component({
  selector: 'wa-theme-modal',
  templateUrl: './theme-modal.component.html',
  styleUrls: ['./theme-modal.component.scss']
})
export class ThemeModalComponent implements OnInit {

  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  waCommonMessages = WACommonMessages;
  originalTheme: Theme = new Theme();
  theme: Theme = new Theme();
  isSubmitDisabled = true;
  isSubmitting = false;
  @Input()
  messages = ThemeModalMessages;
  themeList: Theme[] = [];
  parent: Theme;
  parentPositionMessage = '';
  duplicatedPositionErrorMessage = '';
  duplicatedNameErrorMessage = '';
  positionSpecialCharactersMessage = '';


  constructor(
    public activeModal: NgbActiveModal,
    protected manageThemeService: IManageThemeService,
    protected notificationService: NotificationService) {
  }

  ngOnInit() {
    this.manageThemeService.getAllThemes().pipe(first()).subscribe(
      themesResult => {
        this.themeList = TreeUtils.themeTreeToList(themesResult.themes).filter(theme => theme.id !== this.theme.id);
        this.checkRequiredFields();
        if (this.theme.parentId) {
          this.parent = this.themeList.find(theme => theme.id === this.theme.parentId);
        }
        this.getParentPositionMessage();
      }
    );

    this.theme = this.originalTheme.clone();
    this.checkRequiredFields();
  }

  checkRequiredFields() {
    this.isSubmitDisabled = true;
    const format = /[!@#$%^&*()_+=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(this.theme.position)) {
      this.positionSpecialCharactersMessage = this.messages.SPECIAL_CHARACTERS_ERROR_MESSAGE;
      return;
    } else {
      this.positionSpecialCharactersMessage = '';
    }

    const match = this.themeList.filter(item => item.name === this.theme.name);
    const matchId = this.themeList.filter(item => item.id === this.theme.id && item.name === this.theme.name);

    if (match.length > 0 && matchId.length === 0) {
      this.duplicatedNameErrorMessage = this.messages.DUPLICATED_NAME_ERROR_MESSAGE;
      return;
    } else {
      this.duplicatedNameErrorMessage = '';
    }

    const matchPosition = this.themeList.filter(item => item.position === this.theme.position);
    const matchIdPosition = this.themeList.filter(item => item.id === this.theme.id && item.position === this.theme.position);

    if (matchPosition.length > 0 && matchIdPosition.length === 0) {
      this.duplicatedPositionErrorMessage = this.messages.DUPLICATED_POSITION_ERROR_MESSAGE;
      return;
    } else {
      this.duplicatedPositionErrorMessage = '';
    }
    this.isSubmitDisabled = (!this.theme.name || !this.theme.position);
  }

  getParentPositionMessage() {
    if (this.parent) {
      this.parentPositionMessage = this.messages.parentPosition(this.parent.position);
    }
  }

  /**
   * update theme if everything's ok
   * promise return {id: themeId}
   */
  createOrUpdateTheme() {
    this.isSubmitting = true;
    this.theme.parentId = this.parent ? this.parent.id : null;
    this.manageThemeService.addOrUpdateTheme(this.theme)
      .subscribe(
        data => {
          this.isSubmitting = false;
          this.activeModal.close(data);
        },
        err => {
          console.error(err);
          this.notificationService.showError(this.messages.ERROR_MESSAGE);
          this.isSubmitting = false;
          this.activeModal.dismiss(err);
        }
      );
  }

  close(): void {
    this.activeModal.dismiss({isError: false});
  }

}
