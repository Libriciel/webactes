import { ThemeActions } from '../../actions/theme-actions.enum';
import { ThemeActionItemFactory } from '../../actions/theme-action-item-factory';
import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { ActivatedRoute, Router } from '@angular/router';
import { Theme } from 'src/app/model/theme/theme';
import { IManageThemeService } from '../../services/IManageThemeService';
import { ThemesMessages } from '../../ThemesMessages';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { Pagination } from '../../../../model/pagination';
import { first } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionThemeMapping } from '../../actions/action-theme-mapping';
import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { TreeUtils } from '../../../../utility/TreeUtils';


@Component({
  selector: 'wa-themes-list',
  templateUrl: './themes-list.component.html',
  styleUrls: ['./themes-list.component.scss']
})

export class ThemesListComponent implements OnInit, OnDestroy {

  @Output() actionResult = new EventEmitter();

  messages = ThemesMessages;
  waCommonMessages = WACommonMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  themeActions = ThemeActions;

  themesList: Theme[];
  pagination: Pagination = {page: 1, count: 1, perPage: 1};
  singleActions: ActionItemsSet<Theme>;
  otherSingleActions: ActionItemsSet<Theme>[];
  filterString: string;
  loading = true;
  item = null;
  child = null;
  fullThemesList: Theme[] = [];
  position: string;
  name: string;


  constructor(protected iManageThemeService: IManageThemeService,
              protected actionThemeMapping: ActionThemeMapping,
              protected router: Router,
              public themeActionItemFactory: ThemeActionItemFactory,
              protected notificationService: NotificationService,
              protected activatedRoute: ActivatedRoute,
              protected modalService: NgbModal) {
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(() => {
      this.singleActions = this.actionThemeMapping.getSingleActions();
      this.otherSingleActions = this.actionThemeMapping.getOtherSingleActions();
    });
    this.gotoPage(1);
  }

  ngOnDestroy(): void {
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

  reloadThemes() {
    this.gotoPage(1);
  }

  gotoPage(numPage: number) {
    this.fullThemesList = null;
    this.loading = true;
    this.themesList = null;
    const filters = this.filterString ? {searchedText: this.filterString} : {searchedText: ''};
    this.iManageThemeService.getAll(filters, numPage)
      .pipe(first())
      .subscribe(themesResult => {
        this.themesList = themesResult.themes;
        this.pagination = themesResult.pagination;
        this.loading = false;
        this.fullThemesList = TreeUtils.themeTreeToList(this.themesList);
      });
  }

  toggleActivation(theme: Theme, value: boolean, button: any) {
    this.iManageThemeService.switchThemeActiveState(theme, value).subscribe(result => {
      if (!result.success) {
        button.writeValue(!value);
        this.notificationService.showError(value ?
          this.messages.THEME_ACTIVATION_FAILED_TOASTR_TXT
          : this.messages.THEME_DEACTIVATION_FAILED_TOASTR_TXT
        );
      } else {
        this.notificationService.showSuccess(this.messages.switch_theme_active_done(theme.name, theme.active));
      }
    });
  }

  applyFilter(event) {
    this.filterString = event.trim();
    this.reloadThemes();
  }

  hasFilter(): boolean {
    return this.filterString && this.filterString !== '';
  }

  public actionDone($event: { error: any; message: string; needReload: any; }) {
    if ($event) {
      if ($event.error) {
        if ($event.message) {
          this.notificationService.showError($event.message);
        }
        this.reloadThemes();
      } else {
        if ($event.message) {
          this.notificationService.showSuccess($event.message);
        }
        if ($event.needReload) {
          this.reloadThemes();
        }
      }
    }
  }
}
