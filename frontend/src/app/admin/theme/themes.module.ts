import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemesRoutingModule } from './themes-routing/themes-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ThemesListComponent } from './components/themes-list/themes-list.component';
import { ThemeModalComponent } from './components/modals/theme-modal/theme-modal.component';


@NgModule({
  declarations: [ThemesListComponent, ThemeModalComponent],
  imports: [
    CommonModule,
    ThemesRoutingModule,
    HttpClientModule,
    LibricielCommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgbModule,
  ]
})
export class ThemesModule {
}

