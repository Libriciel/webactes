import { Style } from '../../wa-common/style.enum';

export class ThemesMessages {

  public static THEMES = 'Thèmes';
  public static EDIT_THEME = 'Modifier un thème';
  public static CREATE_THEME = 'Créer un thème';
  public static THEME_PLACEHOLDER = 'Une thématique';
  public static THEME_NAME = 'Nom du thème';
  public static SHOW_NAME = 'Afficher';
  public static ADD_THEME_SUCCESS_MESSAGE = 'Le nouveau thème a bien été enregistré';
  public static ADD_THEME_ERROR_MESSAGE = 'La création du thème a échoué';
  public static UPDATE_THEME_ACTION_FAILED = 'Erreur lors de la sauvegarde des modifications du thème.';
  public static COLUMN_TITLE_NAME = 'Thèmes';
  public static COLUMN_TITLE_POSITION = 'Tri';
  public static CONFIRM_DELETE_THEME_TITLE = 'Supprimer le thème';
  public static DELETE_THEME_ACTION_NAME = 'Supprimer';
  public static DELETE_THEME_ACTION_ICON_STYLE = Style.DANGER;
  public static ADD_THEME_ACTION_ICON_STYLE = Style.NORMAL;
  public static EDIT_ACTION_NAME = 'Modifier';
  public static EDIT_ACTION_LONG_NAME = 'Modifier le thème';
  static update_theme_action_failed = 'Erreur lors de la sauvegarde des modifications du thème.';
  static THEME_DELETE_SUCCESS = 'Thème supprimé avec succès!';
  static THEME_DELETE_FAILURE = 'Une erreur est survenue pendant la suppression du thème.';
  static THEME_ACTIVATION_FAILED_TOASTR_TXT = `L'activation du thème a échoué`;
  static THEME_DEACTIVATION_FAILED_TOASTR_TXT = 'La désactivation du thème a échoué';

  public static createdTheme(name: string) {
    return `Le thème "${name}" a bien été créé.`;
  }

  public static build_confirm_delete_theme_message(themeName: string): string {
    return `Êtes-vous sûr de vouloir supprimer le thème "${themeName}" ?`;
  }

  public static update_theme_action_done(themeName: string) {
    return `Les modifications du thème "${themeName}" ont été enregistrées avec succès.`;
  }

  public static switch_theme_active_done(themeName: string, active: boolean) {
    return active ? `Le thème "${themeName}" a été activé avec succès.` : `Le thème "${themeName}" a été désactivé avec succès.`;
  }

}
