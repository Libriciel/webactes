import { Theme } from '../../../model/theme/theme';
import { Observable } from 'rxjs';
import { FilterType } from 'src/app/model/util/filter-type';
import { Pagination } from 'src/app/model/pagination';
import { FlattenTreeItem } from '../../../utility/flatten-tree-item';


export abstract class IManageThemeService {

  abstract addOrUpdateTheme(theme: Theme): Observable<Theme>;

  abstract getAll(filters?: FilterType, numPage?: number): Observable<{ themes: Theme[], pagination: Pagination }>;

  abstract getAllThemes(filters?: FilterType): Observable<{ themes: Theme[] }>;

  abstract getTheme(id: number): Observable<Theme>;

  abstract getActiveThemesTree(filters?: FilterType): Observable<FlattenTreeItem[]>;

  abstract deleteThemes(theme: Theme[]): Observable<any>;

  abstract switchThemeActiveState(theme: Theme, active: boolean): Observable<{ success: boolean, theme: Theme } | { success: boolean }>;

}
