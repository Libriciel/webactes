import { NgModule } from '@angular/core';
import { OrganizationsModule } from './organizations/organizations.module';
import { OrganizationModule } from './organization/organization.module';
import { AppRoutingModule } from '../app-routing.module';
import { ConnexionPastellModule } from './connexionPastell/connexion-pastell.module';
import { NotificationsOptionsModule } from './notifications-options/notifications-options.module';
import { ActorsModule } from './actors/actors.module';
import { ActorsgroupsModule } from './actorsgroups/actorsgroups.module';
import { AdminComponent } from './admin.component';
import { SharedModule } from '../shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import {
  ActorsgroupsService,
  ActorsService,
  AttachmentSummonFilesService,
  DraftTemplateFilesService,
  DraftTemplatesService,
  DraftTemplateTypesService,
  GenerateTemplateFilesService,
  GenerateTemplatesService,
  GenerateTemplateTypesService,
  StructureSettingsService,
} from '../core';
import { GenerateTemplatesModule } from './generate-templates/generate-templates.module';
import { DraftTemplatesModule } from './draft-templates/draft-templates.module';
import { ConnexionIdelibreModule } from './connexionIdelibre/connexion-idelibre.module';
import { StructureSettingsModule } from './structure-settings/structure-settings.module';


@NgModule({
  declarations: [AdminComponent],
  imports: [
    SharedModule,
    OrganizationsModule,
    OrganizationModule,
    AdminRoutingModule,
    AppRoutingModule,
    NotificationsOptionsModule,
    ConnexionPastellModule,
    ConnexionIdelibreModule,
    ActorsModule,
    ActorsgroupsModule,
    GenerateTemplatesModule,
    DraftTemplatesModule,
    StructureSettingsModule,
  ],
  providers: [
    ActorsService,
    ActorsgroupsService,
    GenerateTemplatesService,
    GenerateTemplateTypesService,
    GenerateTemplateFilesService,
    DraftTemplatesService,
    DraftTemplateTypesService,
    DraftTemplateFilesService,
    AttachmentSummonFilesService,
    StructureSettingsService,
  ]
})
export class AdminModule {
}
