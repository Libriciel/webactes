import {Component, OnInit} from '@angular/core';
import {MenuItem} from '../ls-common/model/menu-item';
import {AppMessages} from '../app-messages';
import {RoutingPaths} from '../wa-common/routing-paths';
import {CircuitsRoutingMessages} from './circuits/routing/circuits-routing-messages';
import {CommonRights} from '../wa-common/rights/common-rights';
import {Router} from '@angular/router';

@Component({
  selector: 'wa-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  adminMenuGroup1: MenuItem[] = [];
  adminMenuGroup2: MenuItem[] = [];
  adminMenuGroup3: MenuItem[] = [];
  superAdminMenu: MenuItem[] = [];

  constructor(private router: Router) {
  }

  ngOnInit() {

    if (!CommonRights.hasSuperAdminMenuRight() && !CommonRights.hasAdminMenuRight() ) {
      this.router.navigateByUrl(RoutingPaths.PROJECTS_DRAFTS_PATH);
    }

    this.adminMenuGroup1 = [
      {
        name: AppMessages.MENU_ADMIN_THEME,
        ref: RoutingPaths.ADMIN_THEME_PATH
      },
      {
        name: AppMessages.MENU_ADMIN_SEQUENCE,
        ref: RoutingPaths.ADMIN_SEQUENCE_PATH
      },
      {
        name: AppMessages.MENU_ADMIN_COUNTER,
        ref: RoutingPaths.ADMIN_COUNTER_PATH
      },
      {
        name: AppMessages.MENU_ADMIN_DRAFT_TEMPLATES,
        ref: `${RoutingPaths.ADMIN_PATH}/draft-templates`
      },
      {
        name: AppMessages.MENU_ADMIN_GENERATE_TEMPLATES,
        ref: `${RoutingPaths.ADMIN_PATH}/generate-templates`
      },
    ];
    this.adminMenuGroup2 = [
      {
        name: AppMessages.MENU_ADMIN_ACTOR_GROUP,
        ref: `${RoutingPaths.ADMIN_PATH}/groupes-acteurs`
      },
      {
        name: AppMessages.MENU_ADMIN_ACTOR,
        ref: `${RoutingPaths.ADMIN_PATH}/acteurs`
      },
      {
        name: AppMessages.MENU_ADMIN_TYPES_ACTS,
        ref: RoutingPaths.ADMIN_TYPES_ACT_LIST_PATH
      },
      {
        name: AppMessages.MENU_ADMIN_TYPE_SEANCE,
        ref: RoutingPaths.ADMIN_TYPE_SEANCE_PATH
      },
      {
        name: CircuitsRoutingMessages.PAGE_CIRCUITS_TITLE,
        ref: RoutingPaths.ADMIN_CIRCUIT_PATH
      },
    ];
    this.adminMenuGroup3 = [
      {
        name: AppMessages.MENU_ADMIN_USER,
        ref: RoutingPaths.ADMIN_USER_PATH
      },
      {
        name: AppMessages.MENU_ADMIN_ORGANIZATION,
        ref: RoutingPaths.ADMIN_ORGANIZATION_PATH
      },
      {
        name: AppMessages.MENU_ADMIN_STRUCTURE_SETTINGS,
        ref: `${RoutingPaths.ADMIN_PATH}/structure-settings`
      },
      {
        name: AppMessages.MENU_ADMIN_CONNEXION_PASTELL,
        ref: RoutingPaths.ADMIN_CONNEXION_PASTELL_PATH
      },
      {
        name: AppMessages.MENU_ADMIN_CONNEXION_IDELIBRE,
        ref: RoutingPaths.ADMIN_CONNEXION_IDELIBRE_PATH
      },
    ];

    if (CommonRights.hasSuperAdminMenuRight()) {
      this.superAdminMenu = [
        {
          name: AppMessages.MENU_ADMIN_ALL_USERS,
          ref: RoutingPaths.ADMIN_ALL_USERS_PATH
        },
        {
          name: AppMessages.MENU_ADMIN_ORGANIZATIONS,
          ref: RoutingPaths.ADMIN_ORGANIZATIONS_PATH
        },
      ];
      // this.adminMenuGroup3 = this.adminMenuGroup3.concat([ {
      //   name: AppMessages.MENU_ADMIN_ORGANIZATIONS,
      //   ref: RoutingPaths.ADMIN_ORGANIZATIONS_PATH,
      // }]);
    }
  }
}
