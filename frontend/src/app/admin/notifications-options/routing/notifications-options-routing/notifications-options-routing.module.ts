import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { PageParameters } from '../../../../wa-common/components/page/model/page-parameters';
import { NotificationsOptionsComponent } from '../../notifications-options/notifications-options.component';
import { NotificationsOptionsMessages } from '../../i18n/notifications-options-messages';
import { AdminMessages } from '../../../i18n/admin-messages';

const routes: Routes = [{
  path: RoutingPaths.PREFERENCES_NOTIFICATIONS_OPTIONS_PATH,
  component: NotificationsOptionsComponent,
  data: {
    pageParameters: {
      title: NotificationsOptionsMessages.PAGE_NOTIFICATIONS_OPTIONS_TITLE,
      breadcrumb: [
        {name: AdminMessages.PAGE_ADMIN_TITLE, location: RoutingPaths.ADMIN_PATH},
        {
          name: NotificationsOptionsMessages.PAGE_NOTIFICATIONS_OPTIONS_TITLE,
          location: RoutingPaths.PREFERENCES_NOTIFICATIONS_OPTIONS_PATH
        }
      ]
    } as PageParameters
  }
}];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class NotificationsOptionsRoutingModule {
}
