import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsOptionsComponent } from './notifications-options/notifications-options.component';
import { NotificationsOptionsRoutingModule } from './routing/notifications-options-routing/notifications-options-routing.module';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';

@NgModule({
  declarations: [NotificationsOptionsComponent],
  imports: [
    CommonModule,
    NotificationsOptionsRoutingModule,
    LibricielCommonModule
  ],
  exports: [NotificationsOptionsComponent]
})
export class NotificationsOptionsModule {
}
