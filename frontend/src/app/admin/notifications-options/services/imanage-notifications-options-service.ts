import { Observable } from 'rxjs';
import { WANotificationsOptions } from '../../../model/notification/notifications-options';
import { Option } from '../../../ls-common/model/option';

export abstract class IManageNotificationsOptionsService {

  abstract getUserNotificationsOptions(): Observable<WANotificationsOptions> ;

  abstract saveNotificationOption(option: Option): Observable<null>;
}
