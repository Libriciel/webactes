export class NotificationsOptionsMessages {

  static PAGE_NOTIFICATIONS_OPTIONS_TITLE = 'Notifications';

  static NOTIFICATIONS_OPTIONS_GET_ERROR = 'Erreur lors de la récupération des options de notification.';
  static NOTIFICATIONS_OPTIONS_SAVE_ERROR = 'Erreur lors de la sauvegarde des options de notification.';

}
