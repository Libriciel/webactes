import { Component, HostBinding, OnInit } from '@angular/core';
import { IManageNotificationsOptionsService } from '../services/imanage-notifications-options-service';
import { WANotificationsOptions } from '../../../model/notification/notifications-options';
import { of } from 'rxjs';
import { NotificationService } from '../../../ls-common/services/notification.service';
import { NotificationsOptionsMessages } from '../i18n/notifications-options-messages';

@Component({
  selector: 'wa-notifications-options',
  templateUrl: './notifications-options.component.html',
  styleUrls: ['./notifications-options.component.scss']
})
export class NotificationsOptionsComponent implements OnInit {
  @HostBinding() class = 'wa-tabs';

  public notificationsOptions: WANotificationsOptions;

  constructor(protected notificationsOptionsService: IManageNotificationsOptionsService,
              protected notificationService: NotificationService) {
  }

  ngOnInit() {
    this.notificationsOptionsService.getUserNotificationsOptions().subscribe(
      value => this.notificationsOptions = value,
      () => {
        this.notificationService.showError(NotificationsOptionsMessages.NOTIFICATIONS_OPTIONS_GET_ERROR);
        return of(null);
      });
  }
}
