import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../../model/user/user';
import { Role } from '../../../model/role';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserMessages } from '../i18n/user-messages';
import { Weight } from '../../../wa-common/weight.enum';
import { CommonIcons, CommonMessages, NotificationService } from '../../../ls-common';
import { IManageUserService } from '../i-manage-user';
import { UserListMessages } from '../user-list/user-list-messages';
import { UserModalMessages } from './user-modal-messages';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';
import { StructureInfo } from '../../../model/structure-info';
import { IManageStructureInformationService } from '../../organization/services/i-manage-structure-information-service';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { STRUCTURE_USER_INFORMATION } from '../../../wa-common/variables/structure-user-info';
import { UserRightsUtils } from '../../../utility/user-rights-utils';
import { Config } from '../../../Config';

@Component({
  selector: 'wa-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
export class UserModalComponent implements OnInit {

  readonly messages = UserMessages;
  readonly userListMessages = UserListMessages;
  readonly waCommonMessages = WACommonMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly userRightsUtils = UserRightsUtils;
  readonly userModalMessages = UserModalMessages;
  readonly weight = Weight;

  @Input()
  user: User = new User();
  userForm: UntypedFormGroup;
  availableStructures: StructureInfo[];
  selectedStructure: StructureInfo;
  isSubmitting = false;

  disabled = false;
  filterString: string;
  limit = 50;
  numPage = 1;

  constructor(
    public activeModal: NgbActiveModal,
    protected manageUsers: IManageUserService,
    protected notificationService: NotificationService,
    protected manageStructureInformationService: IManageStructureInformationService
  ) {
  }

  ngOnInit() {
    this.userForm = new UntypedFormGroup({
      role: new UntypedFormControl(this.user.id ? this.user.roles[0] : null, Validators.required),
      structure: new UntypedFormControl({value: this.user.structures, disabled: !!this.user.id}),
      civility: new UntypedFormControl(this.user.civility, Validators.required),
      lastname: new UntypedFormControl(this.user.lastname, [Validators.required, Validators.maxLength(50)]),
      firstname: new UntypedFormControl(this.user.firstname, [Validators.required, Validators.maxLength(50)]),
      name: new UntypedFormControl(this.user.firstname && this.user.lastname ? this.user.firstname.concat(' ' + this.user.lastname) : ''),
      username: new UntypedFormControl(this.user.username, [Validators.required, Validators.maxLength(50)]),
      email: new UntypedFormControl(this.user.email, [
        Validators.pattern(Config.EMAIL_VALIDATION_REGEXP),
        Validators.required,
        Validators.maxLength(100)]),
    });

    const filters = this.filterString ? {searchedText: this.filterString} : {searchedText: ''};

    if (UserRightsUtils.isSuperAdmin()) {
      this.userForm.get('structure').valueChanges.subscribe(() => this.onStructureChange());
      this.userForm.get('structure').setValidators(Validators.required);

      this.manageStructureInformationService.getAllWithoutPagination()
        .subscribe((structures) => {
          this.availableStructures = structures.structures;
          this.selectedStructure = this.availableStructures && this.user.id
            ? this.availableStructures.find(structure => structure.id === this.user.structures[0].id)
            : null;
          this.userForm.controls['structure'].setValue(this.selectedStructure);
          this.userForm.controls['role'].setValue(this.selectedStructure.roles.find(role => role.id === this.user.roles[0].id));
        });
    } else {
      this.selectedStructure = STRUCTURE_USER_INFORMATION.structureInformation;
    }

    if (this.selectedStructure) {
      this.selectedStructure.roles = this.getRoles();
    }
  }

  onStructureChange() {
    this.selectedStructure = this.userForm.controls['structure'].value;
    this.userForm.controls['role'].setValue(null);
    if (!this.selectedStructure) {
      this.userForm.controls['role'].disable();
    } else {
      this.userForm.controls['role'].enable();
    }
  }

  getRoles(): Role[] {
    if (!this.selectedStructure || !this.selectedStructure.roles) {
      return [];
    }
    return UserRightsUtils.isAdmin()
      ? this.selectedStructure.roles.filter(roles => roles.name !== 'Super Administrateur')
      : this.selectedStructure.roles;
  }

  close(): void {
    this.activeModal.dismiss({isError: false});
  }

  addOrUpdateUser() {
    this.isSubmitting = true;

    const user: User = this.user.clone();
    user.role = this.userForm.controls['role'].value;
    user.roles = [this.userForm.controls['role'].value];
    user.structures = [this.selectedStructure];
    user.structure_id = this.selectedStructure.id;
    user.civility = this.userForm.controls['civility'].value;
    user.firstname = this.userForm.controls['firstname'].value.trim();
    user.lastname = this.userForm.controls['lastname'].value.trim();
    user.username = this.userForm.controls['username'].value.trim();
    user.email = this.userForm.controls['email'].value.trim();

    const call = !this.user.id
      ? this.manageUsers.addUser(user)
      : this.manageUsers.updateUser(user);
    call.subscribe(
      () => this.activeModal.close(user),
      err => {
        console.error(err);
        this.user.id ?
          this.notificationService.showError(this.userModalMessages.USER_EDIT_FAILURE) :
          this.notificationService.showError(this.userModalMessages.USER_ADD_FAILURE);
        this.activeModal.dismiss(err);
      })
      .add(() => this.isSubmitting = false);
  }
}
