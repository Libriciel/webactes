
export class UserModalMessages {
  static CREATE_USER = `Création d'un utilisateur`;
  static UPDATE_USER = `Modification d'un utilisateur`;
  static USER_ADD = 'Créer';
  static USER_UPDATE = 'Enregistrer';
  static COMMENT = 'Commentaire';
  static LAST_NAME_PLACE_HOLDER = 'Nom de l\'utilisateur';
  static FIRST_NAME_PLACE_HOLDER = 'Prénom de l\'utilisateur';
  static USERNAME_PLACE_HOLDER = 'Identifiant de l\'utilisateur';
  static EMAIL_PLACE_HOLDER = 'Email de l\'utilisateur';
  static PHONE_PLACE_HOLDER = 'Téléphone de l\'utilisateur';
  static CIVILITY_PLACE_HOLDER = 'Civilité de l\'utilisateur';

  static CONFIRM_DELETE_USER_TITLE = 'Supprimer l\'utilisateur';
  static USER_DELETE_SUCCESS = 'Utilisateur supprimé avec succès!';
  static USER_DELETE_FAILURE = 'Une erreur est survenue pendant la suppression de l\'utilisateur';
  static USER_ADD_FAILURE = 'La création de l\'utilisateur a échoué';
  static USER_EDIT_FAILURE = 'La modification de l\'utilisateur a échoué';
  static USER_DUPLICATE_NAME = 'Cet utilisateur a déjà été créé';
  static USER_DUPLICATE_LOGIN = 'Un utilisateur possède déjà cet identifiant';
  static USER_WRONG_EMAIL = 'Cette adresse email n\'est pas valide';
  static USER_DUPLICATE_EMAIL = 'Cette adresse email est déjà utilisée';

  static build_confirm_delete_user_message(userName: string): string {
    return `Êtes-vous sûr de vouloir supprimer l\'utilisateur "${userName}"?`;
  }

  static add_user_action_done(user: string) {
    return 'L\'utilisateur "' + user + '" a été créé avec succès.';
  }
}
