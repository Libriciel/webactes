import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { UserListComponent } from './user-list/user-list.component';
import { UsersRoutingModule } from './users-routing/users-routing.module';
import { UserModalComponent } from './user-modal/user-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { StructuresJoinPipe } from './user-list/structures-join.pipe';
import { RolesJoinPipe } from './user-list/roles-join.pipe';
import { UsersOrganizationRoutingModule } from './users-routing/users-organization-routing.module';
import { UserOrganizationListComponent } from './user-organization-list/user-organization-list.component';

@NgModule({
  declarations: [UserListComponent, UserOrganizationListComponent, UserModalComponent,
    StructuresJoinPipe, RolesJoinPipe],
  imports: [
    CommonModule,
    LibricielCommonModule,
    FormsModule,
    UsersRoutingModule,
    UsersOrganizationRoutingModule,
    NgSelectModule,
    ReactiveFormsModule,
  ]
})
export class UsersModule {
}
