import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../model/user/user';
import { CommonIcons, CommonMessages, NotificationService } from '../../../ls-common';
import { UserMessages } from '../i18n/user-messages';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { IManageUserService } from '../i-manage-user';
import { ActionUserMapping } from '../action/action-user-mapping';
import { first } from 'rxjs/operators';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';
import { Pagination } from '../../../model/pagination';
import { UserActionItemFactory } from '../action/user-action-item-factory';
import { UserAction } from '../action/user-action.enum';
import { UserListMessages } from '../user-list/user-list-messages';
import { IManageStructureInformationService } from '../../organization/services/i-manage-structure-information-service';
import { StructureInfo } from '../../../model/structure-info';


@Component({
  selector: 'wa-organization-user-list',
  templateUrl: './user-organization-list.component.html',
  styleUrls: ['./user-organization-list.component.scss']
})
export class UserOrganizationListComponent implements OnInit {
  @Output() actionResult = new EventEmitter();

  users: User[];
  availableStructures: StructureInfo[];
  usersListMessages = UserListMessages;
  messages = UserMessages;
  commonMessages = CommonMessages;
  waCommonMessages = WACommonMessages;
  commonIcons = CommonIcons;

  loading = true;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};
  filterString: string;

  singleActions: ActionItemsSet<User>;

  userAction = UserAction;
  limit = 50;
  numPage = 1;

  constructor(
    protected modalService: NgbModal,
    protected manageUserService: IManageUserService,
    protected notificationService: NotificationService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    protected actionUserStateMap: ActionUserMapping,
    public userActionItemFactory: UserActionItemFactory,
    public manageStructureInformationService: IManageStructureInformationService,
  ) {
  }

  ngOnInit() {

    this.activatedRoute.data.subscribe(() => this.singleActions = this.actionUserStateMap.getSingleActions());
    this.gotoPage(1);
  }

  gotoPage(numPage) {
    this.loading = true;
    this.users = null;
    const filters = this.filterString ? {searchedText: this.filterString} : {searchedText: ''};
    this.manageUserService.getAllOrganizationUsers(numPage, filters)
      .pipe(first())
      .subscribe(usersResult => {
        this.users = usersResult.users;
        this.pagination = usersResult.pagination;
        this.loading = false;
      });

    this.manageStructureInformationService.getAllWithPagination(this.numPage, filters, this.limit)
      .subscribe((structures) => this.availableStructures = structures.structures);
  }

  reloadUsers() {
    this.gotoPage(1);
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

  applyFilter(event) {
    this.filterString = event.trim();
    this.reloadUsers();
  }

  hasFilter(): boolean {
    return this.filterString && this.filterString !== '';
  }

  toggleActivation(user: User, value: boolean, button: any) {
    this.manageUserService.switchUserActiveState(user, value).subscribe(result => {
      if (!result.success) {
        button.writeValue(!value);
        this.notificationService.showError(value ?
          this.usersListMessages.USER_ACTIVATION_FAILED_TOASTR_TXT
          : this.usersListMessages.USER_DEACTIVATION_FAILED_TOASTR_TXT
        );
      } else {
        this.notificationService.showSuccess(this.usersListMessages.switch_user_active_done(user.username, user.active));
      }
    });
  }

  public actionDone($event: { error: any; message: string; needReload: any; }) {
    if ($event) {
      if ($event.error) {
        if ($event.message) {
          this.notificationService.showError($event.message);
        }
        this.reloadUsers();
      } else {
        if ($event.message) {
          this.notificationService.showSuccess($event.message);
        }
        if ($event.needReload) {
          this.reloadUsers();
        }
      }
    }
  }
}
