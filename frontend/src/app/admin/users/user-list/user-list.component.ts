import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../model/user/user';
import { CommonIcons, CommonMessages, NotificationService } from '../../../ls-common';
import { UserMessages } from '../i18n/user-messages';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { IManageUserService } from '../i-manage-user';
import { ActionUserMapping } from '../action/action-user-mapping';
import { first } from 'rxjs/operators';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';
import { Pagination } from '../../../model/pagination';
import { UserListMessages } from './user-list-messages';
import { IManageStructureInformationService } from '../../organization/services/i-manage-structure-information-service';
import { StructureInfo } from '../../../model/structure-info';
import { CommonRights } from '../../../wa-common/rights/common-rights';
import { Action } from '../../../ls-common/model/action';
import { UserActionItemFactory } from '../action/user-action-item-factory';
import { UserAction } from '../action/user-action.enum';

@Component({
  selector: 'wa-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  readonly usersListMessages = UserListMessages;
  readonly messages = UserMessages;
  readonly commonMessages = CommonMessages;
  readonly waCommonMessages = WACommonMessages;
  readonly commonIcons = CommonIcons;
  readonly commonRights = CommonRights;

  loading = true;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};
  filterString: string;
  singleActions: ActionItemsSet<User>;
  actionAddUser: Action<void>;

  availableStructures: StructureInfo[];
  limit = 50;
  numPage = 1;

  @Output() actionResult = new EventEmitter();
  users: User[];

  constructor(
    protected modalService: NgbModal,
    protected manageUserService: IManageUserService,
    protected notificationService: NotificationService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    protected actionUserStateMap: ActionUserMapping,
    protected userActionItemFactory: UserActionItemFactory,
    public manageStructureInformationService: IManageStructureInformationService
  ) {
  }

  ngOnInit() {
    this.actionAddUser = this.userActionItemFactory.getActionItem(UserAction.ADD_USER);
    this.singleActions = this.actionUserStateMap.getSingleActions();
    this.gotoPage(1);
  }

  gotoPage(numPage) {
    this.loading = true;
    this.users = null;
    const filters = this.filterString ? {searchedText: this.filterString} : {searchedText: ''};
    this.manageUserService.getAllUsers(numPage, filters)
      .pipe(first())
      .subscribe(usersResult => {
        this.users = usersResult.users;
        this.pagination = usersResult.pagination;
        this.loading = false;
      });

    if (this.commonRights.hasSuperAdminMenuRight()) {
      this.manageStructureInformationService.getAllWithPagination(this.numPage, filters, this.limit)
        .subscribe((structures) => this.availableStructures = structures.structures);
    }
  }

  reloadUsers() {
    this.gotoPage(1);
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

  applyFilter(event) {
    this.filterString = event.trim();
    this.reloadUsers();
  }

  hasFilter(): boolean {
    return this.filterString && this.filterString !== '';
  }

  toggleActivation(user: User, value: boolean, button: any) {
    this.manageUserService.switchUserActiveState(user, value).subscribe(result => {
      if (!result.success) {
        button.writeValue(!value);
        this.notificationService.showError(value ?
          this.usersListMessages.USER_ACTIVATION_FAILED_TOASTR_TXT
          : this.usersListMessages.USER_DEACTIVATION_FAILED_TOASTR_TXT
        );
      } else {
        this.notificationService.showSuccess(this.usersListMessages.switch_user_active_done(user.username, user.active));
      }
    });
  }

  public actionDone($event: { error: any; message: string; needReload: any; }) {
    if ($event) {
      if ($event.error) {
        if ($event.message) {
          this.notificationService.showError($event.message);
        }
        this.reloadUsers();
      } else {
        if ($event.message) {
          this.notificationService.showSuccess($event.message);
        }
        if ($event.needReload) {
          this.reloadUsers();
        }
      }
    }
  }

}
