import { Pipe, PipeTransform } from '@angular/core';
import { StructureInfo } from '../../../model/structure-info';
import { Role } from '../../../model/role';

/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'joinRoles'})
export class RolesJoinPipe implements PipeTransform {
  // transform(value: number, exponent?: number): number {
  //   return Math.pow(value, isNaN(exponent) ? 1 : exponent);
  // }
  transform(roles: Array<Role>, separator = ', '): string {
    return roles.map(roles => roles.name).join(separator);
  }
}
