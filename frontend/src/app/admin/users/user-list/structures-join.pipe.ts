import { Pipe, PipeTransform } from '@angular/core';
import { StructureInfo } from '../../../model/structure-info';
import { StructureRaw } from '../../../ls-common/services/http-services/info/structure-raw';

/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'joinStructures'})
export class StructuresJoinPipe implements PipeTransform {
  // transform(value: number, exponent?: number): number {
  //   return Math.pow(value, isNaN(exponent) ? 1 : exponent);
  // }
  transform(structures: Array<StructureInfo>, separator = ', '): string {
    return structures.map(structure => structure.businessName).join(separator);
  }
}
