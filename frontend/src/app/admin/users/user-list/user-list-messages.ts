import { Style } from '../../../wa-common';

export class UserListMessages {
  static CREATE_USER = 'Créer un utilisateur';
  static TITLE = 'Utilisateurs';
  static EDIT_USER = 'Modifier un utilisateur';
  static DELETE_USER = 'Supprimer';
  static COMMENT = 'Commentaire';
  static UPDATE_USER_ACTION_FAILED = 'La sauvegarde des modifications de la séquence a échoué.';
  static USER_ACTIVATION_FAILED_TOASTR_TXT = `L'activation de l'utilisateur a échoué`;
  static USER_DEACTIVATION_FAILED_TOASTR_TXT = `La désactivation de l'utilisateur a échoué`;
  // ******* CSS MESSAGES ********
  static ADD_USER_ACTION_ICON_STYLE = Style.NORMAL;
  static EDIT_USER_ACTION_ICON_STYLE = Style.NORMAL;
  static DELETE_USER_ACTION_ICON_STYLE = Style.DANGER;

  static update_user_action_done(user: string) {
    return `Les modifications de l'utilisateur "${user}" ont été enregistrées avec succès.`;
  }

  public static switch_user_active_done(username: string, active: boolean) {
    return active ? `L'utilisateur "${username}" a été activé avec succès.` : `L'utilisateur "${username}" a été désactivé avec succès.`;
  }
}
