import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { UserMessages } from '../i18n/user-messages';
import { AdminMessages } from '../../i18n/admin-messages';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { UserOrganizationListComponent } from '../user-organization-list/user-organization-list.component';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_ALL_USERS_PATH,
    component: UserOrganizationListComponent,
    data: {
      pageParameters: {
        title: UserMessages.STRUCTURES_USERS_LIST,
        breadcrumb: [
          {
            name: AdminMessages.PAGE_ADMIN_TITLE,
            location: RoutingPaths.ADMIN_PATH
          },
          {
            name: UserMessages.STRUCTURES_USERS_LIST,
            location: RoutingPaths.ADMIN_ALL_USERS_PATH
          }
        ]
      } as PageParameters
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class UsersOrganizationRoutingModule {

}
