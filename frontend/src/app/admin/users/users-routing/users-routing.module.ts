import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { UserListComponent } from '../user-list/user-list.component';
import { UserMessages } from '../i18n/user-messages';
import { AdminMessages } from '../../i18n/admin-messages';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_USER_PATH,
    component: UserListComponent,
    data: {
      pageParameters: {
        title: UserMessages.USERS_LIST,
        breadcrumb: [
          {
            name: AdminMessages.PAGE_ADMIN_TITLE,
            location: RoutingPaths.ADMIN_PATH
          },
          {
            name: UserMessages.USERS_LIST,
            location: RoutingPaths.ADMIN_USER_PATH
          }
        ]
      } as PageParameters
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class UsersRoutingModule {
}
