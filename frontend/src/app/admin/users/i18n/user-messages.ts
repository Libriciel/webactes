export class UserMessages {
  static USERS_LIST ='Liste d\'utilisateurs';
  static STRUCTURES_USERS_LIST ='Liste d\'utilisateurs des structures';
  static SELECT_ROLE = 'Veuillez sélectionner le role pour l\'utilisateur';
  static SAVE_USER_ROLE_SUCCESS = 'Le role à bien été affecté';
  static SAVE_USER_ROLE_ERROR = 'Erreur lors de l\'affectation du role';
  static STRUCTURE = 'Structures';
  static STRUCTURE_NAME = 'Structure';
  static CIVILITY = 'Civilité';
  static LAST_NAME = 'Nom';
  static FIRST_NAME = 'Prénom';
  static USERNAME = 'Login';
  static PHONE = 'Téléphone';
  static EMAIL = 'Email';
  static ROLE = 'Profil';
}
