import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { Injectable } from '@angular/core';
import { UserAction } from './user-action.enum';
import { User } from '../../../model/user/user';
import { UserActionItemFactory } from './user-action-item-factory';

@Injectable({
  providedIn: 'root'
})
export class ActionUserMapping {

  constructor(protected userActionItemFactory: UserActionItemFactory) {
  }

  getSingleActions(): ActionItemsSet<User> {
    return {
      actionItems: [
        this.userActionItemFactory.getActionItem(UserAction.EDIT_USER),
        this.userActionItemFactory.getActionItem(UserAction.DELETE_USER),
      ]
    };
  }
}
