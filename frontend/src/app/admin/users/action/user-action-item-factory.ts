import { Injectable } from '@angular/core';
import { ActionItem, CommonIcons } from '../../../ls-common';
import { UserAction } from './user-action.enum';
import { UserListMessages } from '../user-list/user-list-messages';
import { AddUserAction } from './add-user-action';
import { User } from '../../../model/user/user';
import { EditUserAction } from './edit-user-action';
import { DeleteUserAction } from './delete-user-action';
import { EditUserActionValidator } from './edit-user-action-validator';
import { DeleteUserActionValidator } from './delete-user-action-validator';
import { Action } from '../../../ls-common/model/action';

@Injectable({
  providedIn: 'root'
})

export class UserActionItemFactory {
  constructor(private addUserAction: AddUserAction,
              private editUserAction: EditUserAction,
              private deleteUserAction: DeleteUserAction) {
  }

  getActionItem(action: UserAction): Action<User | void> {
    switch (action) {
      case UserAction.ADD_USER:
        return new Action({
          name: UserListMessages.CREATE_USER,
          icon: CommonIcons.ADD_ICON,
          style: UserListMessages.ADD_USER_ACTION_ICON_STYLE,
          actuator: this.addUserAction
        });
      case UserAction.EDIT_USER:
        return new ActionItem({
          name: UserListMessages.EDIT_USER,
          icon: CommonIcons.EDIT_ICON,
          style: UserListMessages.EDIT_USER_ACTION_ICON_STYLE,
          actuator: this.editUserAction,
          actionValidator: new EditUserActionValidator(),
        });
      case UserAction.DELETE_USER:
        return new ActionItem({
          name: UserListMessages.DELETE_USER,
          icon: CommonIcons.DELETE_ICON,
          style: UserListMessages.DELETE_USER_ACTION_ICON_STYLE,
          actuator: this.deleteUserAction,
          actionValidator: new DeleteUserActionValidator(),
        });
      default:
        throw new Error(`unimplemented action ${action}`);
    }
  }
}
