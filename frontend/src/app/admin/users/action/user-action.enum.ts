export enum UserAction {
  ADD_USER,
  EDIT_USER,
  DELETE_USER,
}
