import { AvailableActionsBasedValidator } from '../../../ls-common/components/tables/validator/available-actions-based-validator';
import { User } from '../../../model/user/user';
import { UserAvailableActions } from '../../../model/user/user-available-actions.enum';


export class DeleteUserActionValidator extends AvailableActionsBasedValidator<User> {
  constructor() {
    super(UserAvailableActions.isDeletable);
  }
}
