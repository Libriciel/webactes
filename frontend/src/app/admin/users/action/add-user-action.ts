import { Injectable } from '@angular/core';
import { ActionResult, IActuator } from '../../../ls-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from '../../../model/user/user';
import { UserModalMessages } from '../user-modal/user-modal-messages';
import { UserModalComponent } from '../user-modal/user-modal.component';
import { StructureInfo } from '../../../model/structure-info';

@Injectable({
  providedIn: 'root'
})

export class AddUserAction implements IActuator<void> {

  messages = UserModalMessages;

  constructor(private modalService: NgbModal) {
  }

  action(truc, availableStructures?: StructureInfo): Observable<ActionResult> {
    const modalRef = this.modalService.open(UserModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });

    if (availableStructures) {
      modalRef.componentInstance.availableStructures = availableStructures;
    }

    return from(modalRef.result).pipe(
      map((createdUser: User) => ({
        needReload: true,
        message: this.messages.add_user_action_done(createdUser.username),
      } as ActionResult)),
      catchError((error) =>
        error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.USER_ADD_FAILURE,
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult))
    );
  }
}
