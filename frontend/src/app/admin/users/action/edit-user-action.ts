import { Injectable } from '@angular/core';
import { ActionResult, IActuator } from '../../../ls-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from '../../../model/user/user';
import { UserModalComponent } from '../user-modal/user-modal.component';
import { UserListMessages } from '../user-list/user-list-messages';


@Injectable({
  providedIn: 'root'
})

export class EditUserAction implements IActuator<User> {
  messages = UserListMessages;

  constructor(private modalService: NgbModal) {
  }

  action(user: User[]): Observable<ActionResult> {
    const modalRef = this.modalService.open(UserModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.user = user[0];

    return from(modalRef.result).pipe(
      map((updatedUser: User) => {
        return {
          needReload: true,
          message: this.messages.update_user_action_done(updatedUser.username)
        } as ActionResult;
      }),
      catchError((error) => {
        return error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.UPDATE_USER_ACTION_FAILED,
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}
