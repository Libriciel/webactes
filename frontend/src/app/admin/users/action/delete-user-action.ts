import { Injectable } from '@angular/core';
import { ActionResult, CommonIcons, CommonMessages, ConfirmPopupComponent, IActuator } from '../../../ls-common';
import { Observable, Subject } from 'rxjs';
import { Style } from '../../../wa-common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserModalMessages } from '../user-modal/user-modal-messages';
import { User } from '../../../model/user/user';
import { IManageUserService } from '../i-manage-user';


@Injectable({
  providedIn: 'root'
})

export class DeleteUserAction implements IActuator<User> {

  messages = UserModalMessages;

  constructor(private iManageUserService: IManageUserService, private modalService: NgbModal) {
  }

  action(user: User[]): Observable<any> {

    const returnSubject = new Subject<ActionResult>();

    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    modalRef.componentInstance.title = this.messages.CONFIRM_DELETE_USER_TITLE;
    const userToDelete = user[0];
    modalRef.componentInstance.content = UserModalMessages.build_confirm_delete_user_message(userToDelete.username);
    modalRef.componentInstance.confirmMsg = CommonMessages.DELETE;
    modalRef.componentInstance.style = Style.DANGER;
    modalRef.componentInstance.icon = CommonIcons.DELETE_ICON;

    modalRef.result.then(
      () => this.iManageUserService.deleteUser(user)
        .subscribe(() => {
            returnSubject.next({
              error: false,
              needReload: true,
              message: UserModalMessages.USER_DELETE_SUCCESS,
            });
          },
          error => {
            console.error('Error calling user deletion : ', error);
            returnSubject.next({
              error: true,
              needReload: true,
              message: UserModalMessages.USER_DELETE_FAILURE,
            });
          }),
      () => returnSubject.complete()
    ).catch(error => console.error('Caught error in popup for user deletion, this should not happen. Error : ', error));

    return returnSubject.asObservable();
  }
}
