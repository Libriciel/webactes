import { Observable } from 'rxjs';
import { User } from '../../model/user/user';
import { FilterType } from '../../model/util/filter-type';
import { Pagination } from '../../model/pagination';
import { Counter } from '../../model/counter/counter';
import { Theme } from '../../model/theme/theme';

export abstract class IManageUserService {

  abstract getAll(filters?: FilterType): Observable<{ users: User[] }>;

  abstract getAllUsers(numPage?: number, filters?: FilterType, limit?: number): Observable<{ users: User[] , pagination: Pagination }>;

  abstract getAllActiveUsers(): Observable<{ users: User[] }>;

  abstract getAllOrganizationUsers(numPage?: number, filters?: FilterType, limit?: number): Observable<{ users: User[] , pagination: Pagination }>;

  abstract setRole(roleId: number): Observable<any>;

  abstract addUser(user: User): Observable<{ user: User }>;

  abstract updateUser(user: User): Observable<{ user: User }>;

  abstract deleteUser(user: User[]);

  abstract switchUserActiveState(user: User, active: boolean): Observable<{ success: boolean, user: User } | { success: boolean }>;

}

