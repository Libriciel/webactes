import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypeSittingsListComponent } from './component/types-sittings-list/type-sittings-list.component';
import { TypeSittingModalComponent } from './component/type-sittings-modal/type-sitting-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LibricielCommonModule } from '../../ls-common/libriciel-common.module';
import { WaCommonModule } from '../../wa-common/wa-common.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TypeSittingRoutingModule } from './routing/type-sitting-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from '../../shared';
import { ActorsModule } from '../actors/actors.module';

@NgModule({
  declarations: [TypeSittingsListComponent, TypeSittingModalComponent],

  imports: [
    CommonModule,
    NgbModule,
    LibricielCommonModule,
    WaCommonModule,
    RouterModule,
    FormsModule,
    TypeSittingRoutingModule,
    NgSelectModule,
    ReactiveFormsModule,
    SharedModule,
    ActorsModule,
  ]
})

export class TypeSittingModule {
}
