
export class TypeSittingModalMessages {
  static CREATE_NEW_SITTING_TYPE = `Création d'un type de séance`;
  static UPDATE_NEW_SITTING_TYPE = `Modification d'un type de séance`;
  static SITTING_TYPE = 'Type de séance';
  static SITTING_TYPE_ADD = 'Créer';
  static TYPE_SITTING_UPDATE = 'Enregistrer';
  static NEW_TYPE_SITTING_NAME = 'Libellé';
  static NEW_TYPE_SITTING_NAME_PLACE_HOLDER = 'Veuillez saisir le libellé du type de séance';
  static NEW_TYPE_ACTS_NAME_PLACE_HOLDER: 'Veuillez choisir les types d\'actes associés';
  static TYPE_SITTING_DUPLICATE_NAME = 'Ce type de séance a déjà été créé';
  static TYPE_ACTS = 'Type d\'actes';

  static TYPE_SITTING_EDIT_FAILURE = 'La modification du type de séance a échoué';
  static TYPE_SITTING_ADD_FAILURE = 'La création du type de séance a échoué';

  static CONFIRM_DELETE_MULTIPLE_PROJECT_TITLE = 'Supprimer plusieurs types de séances';
  static CONFIRM_DELETE_SINGLE_PROJECT_TITLE = 'Supprimer le type de séance';
  static TYPE_SITTING_DELETE_SUCCESS = 'Type de séance supprimé avec succès!';
  static TYPE_SITTING_DELETE_FAILURE = 'Une erreur est survenue pendant la suppression du type de séance.';
  static TYPE_SITTING_ADD_SUCCESS = 'Le type de séance à été ajouté';
  static FORM_IS_DELIBERATING =  'La séance est-elle délibérante ?';
  static FORM_IS_DELIBERATING_INFO_MESSAGE = 'Définit s\'il y a des votes en séance pour ce type de séance.';

  static GENERATE_TEMPLATE_CONVOCATION_LIST = `Modèle de convocation`;
  static GENERATE_TEMPLATE_CONVOCATION_PLACE_HOLDER = `Veuillez sélectionner un modèle de convocation dans la liste déroulante.`;
  static GENERATE_TEMPLATE_EXECUTIVE_SUMMARY_LIST = `Modèle de note de synthèse`;
  static GENERATE_TEMPLATE_EXECUTIVE_SUMMARY_PLACE_HOLDER = `Veuillez sélectionner un modèle de note de synthèse dans la liste déroulante.`;
  static GENERATE_TEMPLATE_DELIBERATIONS_LIST_LIST = `Modèle de liste des délibérations`;
  static GENERATE_TEMPLATE_DELIBERATIONS_LIST_PLACE_HOLDER = `Veuillez sélectionner un modèle de liste des délibérations dans la liste déroulante.`;
  static GENERATE_TEMPLATE_VERBAL_TRIAL_LIST = `Modèle de procès-verbal`;
  static GENERATE_TEMPLATE_VERBAL_TRIAL_PLACE_HOLDER = `Veuillez sélectionner un modèle de procès-verbal dans la liste déroulante.`;

  static build_confirm_delete_single_project_message(typeSittingName: string): string {
    return `Êtes-vous sûr de vouloir supprimer le type de séance "${typeSittingName}"?`;
  }

  static build_confirm_delete_multiple_type_sittings_message(numberOfTypeSittings: string | number): string {
    return `Êtes-vous sûr de vouloir supprimer ${numberOfTypeSittings} types de séances?`;
  }
}
