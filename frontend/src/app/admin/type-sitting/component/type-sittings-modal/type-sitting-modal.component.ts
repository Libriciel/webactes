import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TypeSittingModalMessages } from './type-sitting-modal-messages';
import { Weight } from '../../../../wa-common/weight.enum';
import { CommonIcons, CommonMessages, NotificationService } from '../../../../ls-common';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { TypeSitting } from '../../../../model/type-sitting/type-sitting';
import { IManageTypeSittingService } from '../../services/IManageTypeSittingService';
import { TypesAct } from '../../../../model/type-act/types-act';
import { IManageTypeActService } from '../../../type-acts/services/IManageTypeActService';
import { AbstractControl, FormControl, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Actorgroup, ActorsgroupsService } from '../../../../core';
import { ActorsMessages } from '../../../actors/shared/actors-messages';
import { IManageGenerateTemplateService } from '../../../type-acts/services/IManageGenerateTemplateService';
import { GenerateTemplate } from '../../../../model/generate-template/generate-template';
import { Pagination } from '../../../../model/pagination';
import { take } from 'rxjs/operators';

export function nameValidator(TypeSittings): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null =>
    TypeSittings.find(typeSitting => typeSitting.name === control.value) ?
      {duplicatedName: true}
      : null;
}

@Component({
  selector: 'wa-sitting-type-modal',
  templateUrl: './type-sitting-modal.component.html',
  styleUrls: ['./type-sitting-modal.component.scss']
})
export class TypeSittingModalComponent implements OnInit {
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  waCommonMessages = WACommonMessages;
  weight = Weight;
  isSubmitting = false;
  messages = TypeSittingModalMessages;
  typeSitting: TypeSitting = new TypeSitting();
  typeSittingsList: TypeSitting[] = [];
  availableTypeActs: TypesAct[];
  typeSittingForm: UntypedFormGroup;
  isSubmitDisabled = true;
  optionActorGroups: Actorgroup[];
  messagesActorGroup = ActorsMessages;
  filterString: string;
  limit = 200;
  numPage = 1;
  availableConvocationGenerateTemplates: GenerateTemplate[];
  availableExecutiveSummaryGenerateTemplates: GenerateTemplate[];
  availableDeliberationsListGenerateTemplates: GenerateTemplate[];
  availableVerbalTrialGenerateTemplates: GenerateTemplate[];

  pagination: Pagination = {page: 1, count: 1, perPage: 1};

  constructor(
    public activeModal: NgbActiveModal,
    protected typeSittingService: IManageTypeSittingService,
    protected actorsGroupsService: ActorsgroupsService,
    protected generateTemplateService: IManageGenerateTemplateService,
    protected typesActService: IManageTypeActService,
    protected notificationService: NotificationService
  ) {
  }

  ngOnInit() {
    this.typeSittingForm = new UntypedFormGroup({
      name: new UntypedFormControl(this.typeSitting.name, Validators.required),
      isDeliberating: new FormControl(this.typeSitting.isdeliberating, [Validators.required]),
      typesActs: new UntypedFormControl(this.typeSitting.typeActs, [Validators.required]),
      actorGroups: new UntypedFormControl(this.typeSitting.actorGroups, [Validators.required]),
      convocationGenerateTemplate: new UntypedFormControl(this.typeSitting.generate_template_convocation_id),
      executiveSummaryGenerateTemplate: new UntypedFormControl(this.typeSitting.generate_template_executive_summary_id),
      deliberationsListGenerateTemplate: new UntypedFormControl(this.typeSitting.generate_template_deliberations_list_id),
      verbalTrialGenerateTemplate: new UntypedFormControl(this.typeSitting.generate_template_verbal_trial_id),
    });

    const filters = this.filterString ? {searchedText: this.filterString} : {searchedText: ''};

    this.typeSittingService.getAll(this.numPage, filters).pipe(take(1)).subscribe((typeSittings) => {
      this.typeSittingsList = typeSittings.typeSittings.filter(typeSitting => typeSitting.id !== this.typeSitting.id);
      this.typeSittingForm.controls['name'].setValidators([Validators.required, nameValidator(this.typeSittingsList)]);
    });

    this.fillData();
  }

  close(): void {
    this.activeModal.dismiss({isError: false});
  }

  /**
   * add sittings if everything's ok
   * promise return {id: sittingId}
   */
  addOrUpdateSitting() {
    this.isSubmitting = true;

    const typeSitting: TypeSitting = this.typeSitting.clone();
    typeSitting.name = this.typeSittingForm.controls['name'].value.trim();
    typeSitting.isdeliberating = this.typeSittingForm.controls['isDeliberating'].value;
    typeSitting.typeActs = this.typeSittingForm.controls['typesActs'].value;
    typeSitting.actorGroups = this.typeSittingForm.controls['actorGroups'].value;
    typeSitting.generate_template_convocation_id = this.typeSittingForm.controls['convocationGenerateTemplate'].value;
    typeSitting.generate_template_executive_summary_id = this.typeSittingForm.controls['executiveSummaryGenerateTemplate'].value;
    typeSitting.generate_template_deliberations_list_id = this.typeSittingForm.controls['deliberationsListGenerateTemplate'].value;
    typeSitting.generate_template_verbal_trial_id = this.typeSittingForm.controls['verbalTrialGenerateTemplate'].value;

    const call = !this.typeSitting.id
      ? this.typeSittingService.addTypeSitting(typeSitting)
      : this.typeSittingService.updateTypeSitting(typeSitting);

    call.subscribe(
      () => {
        this.isSubmitting = false;
        this.activeModal.close(typeSitting);
      },
      err => {
        console.error(err);
        this.typeSitting.id ?
          this.notificationService.showError(this.messages.TYPE_SITTING_EDIT_FAILURE) :
          this.notificationService.showError(this.messages.TYPE_SITTING_ADD_FAILURE);
        this.isSubmitting = false;
        this.activeModal.dismiss(err);
      });
  }

  private fillData() {
    this.actorsGroupsService.getAllWithoutPagination().subscribe(data => {
      this.optionActorGroups = data.actorGroups.filter(item => item.active === true);
    });
    if (this.typeSitting.id) {
      this.typeSittingForm.patchValue({isDeliberating: this.typeSitting.isdeliberating});
    }
    this.typesActService.getAllWithoutPagination({active: true, isdeliberating: true}).subscribe((typeActs) => {
      this.availableTypeActs = typeActs.typeacts;
    });

    this.generateTemplateService.getAllByTypeCode('convocation').subscribe((generateTemplates) => {
      this.availableConvocationGenerateTemplates = generateTemplates;
    });
    this.generateTemplateService.getAllByTypeCode('executive_summary').subscribe((generateTemplates) => {
      this.availableExecutiveSummaryGenerateTemplates = generateTemplates;
    });
    this.generateTemplateService.getAllByTypeCode('deliberations_list').subscribe((generateTemplates) => {
      this.availableDeliberationsListGenerateTemplates = generateTemplates;
    });
    this.generateTemplateService.getAllByTypeCode('verbal_trial').subscribe((generateTemplates) => {
      this.availableVerbalTrialGenerateTemplates = generateTemplates;
    });
  }

  get isDeliberating() {

    return this.typeSittingForm.controls['isDeliberating'].value;
  }
}
