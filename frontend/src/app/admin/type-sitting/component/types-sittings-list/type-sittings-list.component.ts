import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CommonIcons } from '../../../../ls-common/icons/common-icons';
import { TypeSittingsListMessages } from './type-sittings-list-messages';
import { WACommonMessages } from '../../../../wa-common/i18n/wa-common-messages';
import { CommonMessages } from '../../../../ls-common/i18n/common-messages';
import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { TypeActRaw } from '../../../../ls-common/services/http-services/type-act/type-act-raw';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationService } from '../../../../ls-common/services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TypeSitting } from '../../../../model/type-sitting/type-sitting';
import { first } from 'rxjs/operators';
import { Pagination } from '../../../../model/pagination';
import { RoutingPaths } from '../../../../wa-common/routing-paths';
import { IManageTypeSittingService } from '../../services/IManageTypeSittingService';
import { ActionTypeSittingMapping } from '../action/action-type-sitting-mapping';
import { TypeSittingActionItemFactory } from '../action/type-sitting-action-item-factory';
import { TypeSittingAction } from '../action/type-sitting-action.enum';

@Component({
    selector: 'wa-typeSittings-list',
    templateUrl: './type-sittings-list.component.html',
  styleUrls: ['./type-sittings-list.component.scss']
})


export class TypeSittingsListComponent implements OnInit {
  @Output() actionResult = new EventEmitter();

  messages = TypeSittingsListMessages;
  typeSittings:  TypeSitting[];
  typeActs: TypeActRaw[]  = null;

  loading = true;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};

  waCommonMessages = WACommonMessages;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  commonActions: ActionItemsSet<TypeSitting>[];
  singleActions: ActionItemsSet<TypeSitting>;
  otherSingleActions: ActionItemsSet<TypeSitting>[];

  filterString: string;

  typeSittingAction = TypeSittingAction;

  constructor(
    protected  modalService: NgbModal,
    protected typeSittingService: IManageTypeSittingService,
    protected  notificationService: NotificationService,
    protected  activatedRoute: ActivatedRoute,
    protected  actionTypeSittingStateMap: ActionTypeSittingMapping,
    public typeSittingActionItemFactory: TypeSittingActionItemFactory,
    protected router: Router,
  ) {
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(() => {
      this.singleActions = this.actionTypeSittingStateMap.getSingleActions();
      this.otherSingleActions = this.actionTypeSittingStateMap.getOtherSingleActions();
      this.commonActions = this.actionTypeSittingStateMap.getCommonActions();
    });
    this.gotoPage(1);
  }

  gotoPage(numPage) {
    this.loading = true;
    this.typeSittings = null;
    const filters = this.filterString ? {searchedText: this.filterString} : {searchedText: ''};
    this.typeSittingService.getAll(numPage, filters)
      .pipe(first())
      .subscribe(typeSittingsResult => {
        this.typeSittings = typeSittingsResult.typeSittings;
        this.pagination = typeSittingsResult.pagination;
        this.loading = false;
      });
  }

  reloadTypesSittings() {
    this.gotoPage(1);
  }

  applyFilter(event) {
    this.filterString = event.trim();
    this.reloadTypesSittings();
  }

  hasFilter(): boolean {
    return this.filterString && this.filterString !== '';
  }

  toggleActivation(typeSitting: TypeSitting, value: boolean, button: any) {
      this.typeSittingService.switchTypeSittingActiveState(typeSitting, value).subscribe(result => {
        if (!result.success) {
          button.writeValue(!value);
          this.notificationService.showError(value ?
            this.messages.TYPE_SITTING_ACTIVATION_FAILED_TOASTR_TXT
            : this.messages.TYPE_SITTING_DEACTIVATION_FAILED_TOASTR_TXT);
        }
      });
  }

  goBack() {
    this.router.navigateByUrl(RoutingPaths.ADMIN_PATH).then();
  }

  public actionDone($event: { error: any; message: string; needReload: any; }) {
    if ($event) {
      if ($event.error) {
        if ($event.message) {
          this.notificationService.showError($event.message);
        }
        this.reloadTypesSittings();
      } else {
        if ($event.message) {
          this.notificationService.showSuccess($event.message);
        }
        if ($event.needReload) {
          this.reloadTypesSittings();
        }
      }
    }
  }
}
