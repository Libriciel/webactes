/* eslint-disable @typescript-eslint/member-ordering */
import { Style } from '../../../../wa-common/style.enum';

export class TypeSittingsListMessages {

  static CREATE_TYPE_SEANCE = 'Créer un type de séance';
  static NAME = 'Libellé';
  static ACT_TYPES = 'Type d\'actes associés';
  static TITLE = 'Types de séances';
  static EDIT_TYPE_SEANCE = 'Modifier un type de séance';
  static DELETE_TYPE_SEANCE = 'Supprimer';
  static SHOW_TYPE_SEANCE = 'Visualiser';
  static UPDATE_TYPE_SITTING_ACTION_FAILED = 'Erreur lors de la sauvegarde des modifications du type de séance.';
// ******* CSS MESSAGES ********
  static ADD_TYPE_SITTING_ACTION_ICON_STYLE = Style.NORMAL;
  static EDIT_TYPE_SITTING_ACTION_ICON_STYLE =  Style.NORMAL;
  static DELETE_TYPE_SITTING_ACTION_ICON_STYLE = Style.DANGER;
  static SHOW_TYPE_SITTING_ACTION_ICON_STYLE = Style.NORMAL;
  static TYPE_SITTING_ACTIVATION_FAILED_TOASTR_TXT =  'Erreur lors de l\'activation du type de séance.';
  static TYPE_SITTING_DEACTIVATION_FAILED_TOASTR_TXT = 'Erreur lors de la désactivation du type de séance.';
  static ACTOR_GROUPS = 'Groupes d\'acteurs';

   static update_type_sitting_action_done(sitting_type: string) {
    return 'Les modifications du type de séance "' + sitting_type + '" ont été enregistrées avec succès.';
  }

  static add_type_sitting_action_done(sitting_type: string) {
    return 'La séance "' + sitting_type + '" a été créée avec succès.';
  }
}
