import { ActionItemsSet } from '../../../../ls-common/model/action-items-set';
import { Injectable } from '@angular/core';
import { TypeSittingActionItemFactory } from './type-sitting-action-item-factory';
import { TypeSitting } from '../../../../model/type-sitting/type-sitting';
import { TypeSittingAction } from './type-sitting-action.enum';

@Injectable({
  providedIn: 'root'
})
export class ActionTypeSittingMapping {

  constructor(protected typeSittingActionItemFactory: TypeSittingActionItemFactory) {
  }

  getCommonActions(): ActionItemsSet<TypeSitting>[] {
    return [
    ];
  }

  getSingleActions(): ActionItemsSet<TypeSitting> {
    return {
      actionItems: [
        this.typeSittingActionItemFactory.getActionItem(TypeSittingAction.EDIT_TYPE_SITTING),
        this.typeSittingActionItemFactory.getActionItem(TypeSittingAction.DELETE_TYPE_SITTING),]
    };
  }

  getOtherSingleActions(): ActionItemsSet<TypeSitting>[] {
    return [];
  }
}
