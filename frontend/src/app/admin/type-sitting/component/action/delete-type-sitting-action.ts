import { Observable, of, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActionResult, CommonIcons, CommonMessages, ConfirmPopupComponent, IActuator } from '../../../../ls-common';
import { TypeSitting } from '../../../../model/type-sitting/type-sitting';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IManageTypeSittingService } from '../../services/IManageTypeSittingService';
import { Style } from '../../../../wa-common';
import { TypeSittingModalMessages } from '../type-sittings-modal/type-sitting-modal-messages';

@Injectable({
  providedIn: 'root'
})
export class DeleteTypeSittingAction implements IActuator<TypeSitting> {


  constructor(private typeSittingService: IManageTypeSittingService, private modalService: NgbModal) {
  }

  action(typeSittings: TypeSitting[]): Observable<ActionResult> {
    if (!typeSittings || typeSittings.length < 1) {
      console.error('No project selected, action unavailable');
      return of({});
    }

    const returnSubject = new Subject<ActionResult>();

    const modalRef = this.modalService.open(ConfirmPopupComponent, {
      backdrop: 'static',
      keyboard: false,
      centered: true
    });

    if (typeSittings.length > 1) {
      modalRef.componentInstance.title = TypeSittingModalMessages.CONFIRM_DELETE_MULTIPLE_PROJECT_TITLE;
      modalRef.componentInstance.content =
        TypeSittingModalMessages.build_confirm_delete_multiple_type_sittings_message(typeSittings.length);
    } else {
      const typeSitting = typeSittings[0];
      modalRef.componentInstance.title = TypeSittingModalMessages.CONFIRM_DELETE_SINGLE_PROJECT_TITLE;
      modalRef.componentInstance.content = TypeSittingModalMessages.build_confirm_delete_single_project_message(typeSitting.name);
    }

    modalRef.componentInstance.confirmMsg = CommonMessages.DELETE;
    modalRef.componentInstance.style = Style.DANGER;
    modalRef.componentInstance.icon = CommonIcons.DELETE_ICON;

    modalRef.result.then(
      () => this.typeSittingService.deleteTypeSitting(typeSittings).subscribe(
        () => {
          returnSubject.next({
            error: false,
            needReload: true,
            message: TypeSittingModalMessages.TYPE_SITTING_DELETE_SUCCESS
          });
        },
        error => {
          console.error('Error calling type sittings deletion : ', error);
          returnSubject.next({
            error: true,
            needReload: true,
            message: TypeSittingModalMessages.TYPE_SITTING_DELETE_FAILURE
          });
        }),
      () => returnSubject.complete()
    ).catch(error => console.error('Caught error in popup for type sittings deletion, this should not happen. Error : ', error));

    return returnSubject.asObservable();
  }
}
