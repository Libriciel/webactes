import { AvailableActionsBasedValidator } from '../../../../ls-common/components/tables/validator/available-actions-based-validator';
import { TypeSittingAvailableActions } from '../../../../model/type-sitting/type-sitting-available-actions.enum';
import { TypeSitting } from '../../../../model/type-sitting/type-sitting';

export class DeleteTypeSittingActionValidator extends AvailableActionsBasedValidator<TypeSitting> {

  constructor() {
    super(TypeSittingAvailableActions.isDeletable);
  }
}
