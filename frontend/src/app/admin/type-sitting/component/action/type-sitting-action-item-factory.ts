import { ActionItem, CommonIcons } from '../../../../ls-common';
import { Injectable } from '@angular/core';
import { AddTypeSittingAction } from './add-type-sitting-action';
import { TypeSittingAction } from './type-sitting-action.enum';
import { TypeSittingsListMessages } from '../types-sittings-list/type-sittings-list-messages';
import { EditTypeSittingActionValidator } from './edit-type-sitting-action-validator';
import { EditTypeSittingAction } from './edit-type-sitting-action';
import { DeleteTypeSittingAction } from './delete-type-sitting-action';
import { TypeSitting } from '../../../../model/type-sitting/type-sitting';
import { DeleteTypeSittingActionValidator } from './delete-type-sitting-action-validator';

@Injectable({
  providedIn: 'root'
})
export class TypeSittingActionItemFactory {

  constructor(private addTypeSittingAction: AddTypeSittingAction,
              private editTypeSittingAction: EditTypeSittingAction,
              private deleteTypeSittingAction: DeleteTypeSittingAction) {
  }

  getActionItem(action: TypeSittingAction): ActionItem<TypeSitting> {
    switch (action) {
      case TypeSittingAction.ADD_TYPE_SITTING:
        return new ActionItem({
          name: TypeSittingsListMessages.CREATE_TYPE_SEANCE,
          icon: CommonIcons.ADD_ICON,
          style: TypeSittingsListMessages.ADD_TYPE_SITTING_ACTION_ICON_STYLE,
          actuator: this.addTypeSittingAction
        });
      case TypeSittingAction.EDIT_TYPE_SITTING:
        return new ActionItem({
          name: TypeSittingsListMessages.EDIT_TYPE_SEANCE,
          icon: CommonIcons.EDIT_ICON,
          style: TypeSittingsListMessages.EDIT_TYPE_SITTING_ACTION_ICON_STYLE,
          actuator: this.editTypeSittingAction,
          actionValidator: new EditTypeSittingActionValidator(),
        });
      case TypeSittingAction.DELETE_TYPE_SITTING:
        return new ActionItem({
          name: TypeSittingsListMessages.DELETE_TYPE_SEANCE,
          icon: CommonIcons.DELETE_ICON,
          style: TypeSittingsListMessages.DELETE_TYPE_SITTING_ACTION_ICON_STYLE,
          actuator: this.deleteTypeSittingAction,
          actionValidator: new DeleteTypeSittingActionValidator(),
        });
      default:
        throw new Error(`unimplemented action ${action}`);
    }
  }
}
