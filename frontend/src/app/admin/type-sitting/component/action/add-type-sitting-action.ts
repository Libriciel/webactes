import { from, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { IActuator } from '../../../../ls-common/components/tables/actuator/i-actuator';
import { TypeSitting } from '../../../../model/type-sitting/type-sitting';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TypeSittingModalComponent } from '../type-sittings-modal/type-sitting-modal.component';
import { ActionResult } from '../../../../ls-common/components/tables/actuator/action-result';
import { catchError, map } from 'rxjs/operators';
import { TypeSittingsListMessages } from '../types-sittings-list/type-sittings-list-messages';

@Injectable({
  providedIn: 'root'
})
export class AddTypeSittingAction implements IActuator<TypeSitting> {

  messages = TypeSittingsListMessages;

  constructor(private modalService: NgbModal) {
  }

  action(typeSittings?: TypeSitting[]): Observable<ActionResult> {

    const modalRef = this.modalService.open(TypeSittingModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });

    return from(modalRef.result).pipe(
      map((createdTypeSitting: TypeSitting) => {
        return {
          needReload: true,
          message: this.messages.add_type_sitting_action_done(createdTypeSitting.name)
        } as ActionResult;
      }),
      catchError((error) => {
        return error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.UPDATE_TYPE_SITTING_ACTION_FAILED
          } as ActionResult)
          : of({
            error: false,
            needReload: false
          } as ActionResult);
      })
    );
  }
}
