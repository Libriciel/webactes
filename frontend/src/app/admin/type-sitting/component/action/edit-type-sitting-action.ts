import { from, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { IActuator } from '../../../../ls-common';
import { TypeSitting } from '../../../../model/type-sitting/type-sitting';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TypeSittingModalComponent } from '../type-sittings-modal/type-sitting-modal.component';
import { catchError, map } from 'rxjs/operators';
import { ActionResult } from '../../../../ls-common';
import { TypeSittingsListMessages } from '../types-sittings-list/type-sittings-list-messages';

@Injectable({
  providedIn: 'root'
})
export class EditTypeSittingAction implements IActuator<TypeSitting> {
  messages = TypeSittingsListMessages;


  constructor(private modalService: NgbModal) {
  }

  action(typeSittings: TypeSitting[]): Observable<ActionResult> {


    const modalRef = this.modalService.open(TypeSittingModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static',
      keyboard: false
    });
    modalRef.componentInstance.typeSitting = typeSittings[0];

    return from(modalRef.result).pipe(
      map((updatedTypeSitting: TypeSitting) =>
        ({
          needReload: true,
          message: this.messages.update_type_sitting_action_done(updatedTypeSitting.name)
        } as ActionResult)),
      catchError((error) =>
        error && error.isError
          ? of({
            error: true,
            needReload: false,
            message: this.messages.UPDATE_TYPE_SITTING_ACTION_FAILED
          } as ActionResult)
          : of({
            error: false,
            needReload: true
          } as ActionResult))
    );
  }
}
