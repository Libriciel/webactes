import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { TypeSittingsListComponent } from '../component/types-sittings-list/type-sittings-list.component';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { TypeSittingsListMessages } from '../component/types-sittings-list/type-sittings-list-messages';
import { AdminMessages } from '../../i18n/admin-messages';

const routes: Routes = [
  {
    path: RoutingPaths.ADMIN_TYPE_SEANCE_PATH,
    component: TypeSittingsListComponent,
    data: {
      pageParameters: {
        title: TypeSittingsListMessages.TITLE,
        breadcrumb: [{
          name: AdminMessages.PAGE_ADMIN_TITLE,
          location: RoutingPaths.ADMIN_PATH
        }
          ,
          {name: TypeSittingsListMessages.TITLE, location: RoutingPaths.ADMIN_TYPE_SEANCE_PATH}
        ],
        fluid: true
      } as PageParameters
    }
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class TypeSittingRoutingModule {
}
