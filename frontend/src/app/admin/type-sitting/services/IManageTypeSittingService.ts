import { Observable } from 'rxjs';
import { TypeSitting } from '../../../model/type-sitting/type-sitting';
import { Pagination } from '../../../model/pagination';
import { FilterType } from '../../../model/util/filter-type';

export abstract class IManageTypeSittingService {

  abstract addTypeSitting(typeSitting: TypeSitting): Observable<TypeSitting>;

  abstract getAll(numPage: number, filters?: FilterType ): Observable<{ typeSittings: TypeSitting[], pagination: Pagination }>;

  abstract updateTypeSitting(typeSitting: TypeSitting): Observable<TypeSitting>;

  abstract deleteTypeSitting(typeSittings: TypeSitting[]);

  abstract switchTypeSittingActiveState(typeSitting: TypeSitting, value: boolean): Observable<{typeSitting: TypeSitting, success: boolean } | { success: boolean }>;
}
