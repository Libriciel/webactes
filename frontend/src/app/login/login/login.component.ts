import { Component, OnInit } from '@angular/core';
import { UserService } from '../../ls-common/services/http-services/user/User.service';

@Component({
  selector: 'wa-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(protected userService: UserService) {
  }

  ngOnInit() {
    this.userService.loginStructure().subscribe(data => console.log(data));
  }

}
