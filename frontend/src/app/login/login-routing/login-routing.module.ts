import { CommonModule } from '@angular/common';

import { NgModule } from '@angular/core';
import { PageParameters } from '../../wa-common/components/page/model/page-parameters';
import { RoutingPaths } from '../../wa-common/routing-paths';
import { LoginComponent } from '../login/login.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: 'login',
  component: LoginComponent,
  data: {
    pageParameters: {
      title: '',
      breadcrumb: [
        {
          name: '',
          location: RoutingPaths.LOGIN
        }
      ],
      fluid: true
    } as PageParameters
  }
}];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})

export class LoginRoutingModule {
}
