import { Observable } from 'rxjs';
import { Search } from '../../model/search';
import { Project } from '../../model/project/project';
import { Pagination } from '../../model/pagination';

export abstract class IManageSearchService {

  abstract postSearch(search: Search, numPage?: number): Observable<{projects: Project[], pagination: Pagination}>;

}
