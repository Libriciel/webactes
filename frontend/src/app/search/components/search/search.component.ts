import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { CommonIcons, CommonMessages } from '../../../ls-common';
import { Pagination } from '../../../model/pagination';
import { SearchMessages } from '../../search-messages';
import { NgbAccordion, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { IManageSearchService } from '../../services/IManageSearchService';
import { Project } from '../../../model/project/project';
import { TypesAct } from '../../../model/type-act/types-act';
import { Search } from '../../../model/search';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { IManageProjectsService } from '../../../projects/services/i-manage-projects.service';
import { ActionItemsSet } from '../../../ls-common/model/action-items-set';
import { ProjectActionItemFactory } from '../../../projects/components/projects-list/actions/project-action-item-factory';
import { ProjectAction } from '../../../projects/components/projects-list/actions/project-action.enum';
import { ProjectDisplayState } from 'src/app/model/project/project-display-state.enum';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';

@Component({
  selector: 'wa-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Output() closeAction = new EventEmitter();
  @ViewChild('datePicker', {static: true}) datePicker: NgbInputDatepicker;
  @ViewChild('documentAccordion', {static: true}) documentAccordion: NgbAccordion;

  ProjectAction = ProjectAction;
  ProjectDisplayState = ProjectDisplayState;

  searchMessages = SearchMessages;
  commonMessages = CommonMessages;
  waCommonMessages = WACommonMessages;
  commonIcons = CommonIcons;
  filterString: string;
  pagination: Pagination = {page: 1, count: 1, perPage: 1};
  loading = true;
  isSubmitDisabled = true;
  singleActions: ActionItemsSet<Project>;

  search: Search = new Search();
  projects: Project[];
  projectsList: Project[];
  typesacts: TypesAct[];
  availableTypeActs: TypesAct[];
  searchForm = 'Formulaire';
  searchSubmit = false;
  resultFound: boolean;

  constructor(protected iManageSearchService: IManageSearchService,
              protected iManageProjectsService: IManageProjectsService,
              protected router: Router,
              protected activatedRoute: ActivatedRoute,
              public projectActionItemFactory: ProjectActionItemFactory) {
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(() => {
      this.singleActions = {
        actionItems: [
          this.projectActionItemFactory.getActionItem(ProjectAction.SHOW_PROJECT)
        ]
      };
      this.iManageProjectsService.getTypesActs()
        .pipe(first())
        .subscribe(typesActs => {
          this.availableTypeActs = typesActs.typeacts;
        });
    });
    this.gotoPage(1);
  }

  gotoPage(numPage) {
    this.loading = true;
    this.iManageSearchService.postSearch(this.search, numPage)
      .pipe(first())
      .subscribe(result => {
        this.pagination = result.pagination;
        this.projectsList = result.projects;
        this.loading = false;
      });
  }

  checkEmptyRequest() {
    this.isSubmitDisabled =
      (!this.search.project_name && !this.search.typeacts && !this.search.code_act)
        ? true
        : false;
  }

  searchRequest() {
    this.iManageSearchService.postSearch(this.search).subscribe(result => {
      this.searchSubmit = true;
      this.projectsList = result.projects;
      this.pagination = result.pagination;
      this.resultFound = this.projectsList.length > 0 ? true : false;
    });
  }

  getOpenedAccordion() {
    return this.searchForm;
  }

  reset() {
    this.searchSubmit = false;
  }

}
