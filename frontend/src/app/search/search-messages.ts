import { Style } from '../wa-common';

export class SearchMessages {
  public static PAGE_TITLE = 'Recherche multi-critères';
  public static FORM_TITLE = 'Formulaire de recherche';
  public static ACT_NAME = 'Libellé';
  public static TYPESACTS = 'Type d\'acte(s)';
  public static STATES = 'Etat(s)';
  public static CODE_ACT = 'Numéros d\'acte(s)';
  public static THEME = 'Thème(s)';
  public static SHOW = 'Visualiser';
  public static SIGNATURE_DATE = 'Date de signature';
  public static TYPESITTING = 'Type(s) de séance';
  public static SITTING_DATE = 'Date(s) de séance';
  public static ACT_NAME_PLACE_HOLDER = 'Mot ou libellé entier';
  public static TYPE_ACT_PLACE_HOLDER = 'Sélectionner un ou des types d\'acte(s)';
  public static STATE_ACT_PLACE_HOLDER = 'Sélectionner un ou des état(s)';
  public static CODE_ACT_PLACE_HOLDER = 'Numéro(s)';
  public static THEME_ACT_PLACE_HOLDER = 'Thème(s)';
  public static TYPESITTING_PLACE_HOLDER = 'Sélectionner un ou des type(s) de séance';
  public static NO_RESULT = 'Aucun(s) résultat(s) ne correspond à ces critères.';
  public static HELPER = 'Saisir les numéros d\'actes séparés par une virgule. Ex: DELIB129,ARR_2020_03';
  public static SHOW_SEARCH_ACTION_ICON_STYLE = Style.NORMAL;
}
