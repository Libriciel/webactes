import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../wa-common/routing-paths';
import { PageParameters } from '../../wa-common/components/page/model/page-parameters';
import { SearchComponent } from '../components/search/search.component';
import { SearchMessages } from '../search-messages';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  {
    path: RoutingPaths.SEARCH_PATH,
    component: SearchComponent,
    data: {
      pageParameters: {
        title: SearchMessages.PAGE_TITLE,
        breadcrumb: [
          {name: SearchMessages.PAGE_TITLE, location: RoutingPaths.ADMIN_THEME_PATH}
        ],
        fluid: true
      } as PageParameters
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})

export class SearchRoutingModule { }
