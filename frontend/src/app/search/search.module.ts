import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LibricielCommonModule } from '../ls-common/libriciel-common.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchRoutingModule } from './routing/search-routing.module';
import { SearchComponent } from './components/search/search.component';
import { ProjectsModule } from '../projects/projects.module';
import { ActsModule } from '../acts/acts.module';

@NgModule({
  declarations: [SearchComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    LibricielCommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgbModule,
    SearchRoutingModule,
    ProjectsModule,
    ActsModule
  ]
})
export class SearchModule {
}
