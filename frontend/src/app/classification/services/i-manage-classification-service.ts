import { Observable } from 'rxjs';
import { ClassificationDate } from '../../model/classification/classification-date';
import { FlattenTreeItem } from '../../utility/flatten-tree-item';

export abstract class IManageClassificationService {

  abstract updateClassification(): Observable<null>;

  abstract getClassificationDate(): Observable<ClassificationDate>;

  abstract getClassificationSubjects(): Observable<FlattenTreeItem[]>;
}
