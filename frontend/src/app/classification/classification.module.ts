import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateClassificationComponent } from './components/update-classification/update-classification.component';
import { LibricielCommonModule } from '../ls-common/libriciel-common.module';

@NgModule({
  declarations: [UpdateClassificationComponent],
  exports: [
    UpdateClassificationComponent
  ],
  imports: [
    CommonModule,
    LibricielCommonModule
  ]
})
export class ClassificationModule {
}
