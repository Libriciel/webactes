import { DatePipe } from '@angular/common';

export class ClassificationMessages {

  static UPDATE_BUTTON_TITLE = 'Mettre à jour';
  static UPDATE_CLASSIFICATION_SUCCESS = 'La classification a bien été mise à jour';
  static UPDATE_CLASSIFICATION_ERROR = 'Erreur lors de la mise à jour de la classification';

  static UPDATE_BUTTON_ICON = 'fas fa-sync-alt';

  static classificationPanelMessage(date: Date) {
    return date
      ? `La classification enregistrée date du ${new DatePipe('fr').transform(date, 'dd/MM/yyyy à HH:mm')}`
      : 'Aucune classification enregistrée';
  }
}
