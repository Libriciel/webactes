import { Component, OnInit } from '@angular/core';
import { ClassificationMessages } from '../../i18n/classification-messages';
import { IManageClassificationService } from '../../services/i-manage-classification-service';
import { NotificationService } from '../../../ls-common/services/notification.service';
import { ClassificationDate } from '../../../model/classification/classification-date';
import { Style } from '../../../wa-common/style.enum';
import { Weight } from '../../../wa-common/weight.enum';
import { CommonMessages } from '../../../ls-common/i18n/common-messages';

@Component({
  selector: 'wa-update-classification',
  templateUrl: './update-classification.component.html',
  styleUrls: ['./update-classification.component.scss']
})
export class UpdateClassificationComponent implements OnInit {

  classificationMessages = ClassificationMessages;
  commonMessages = CommonMessages;
  isLoading = false;
  classificationDate: ClassificationDate = null;
  style = Style;
  weight = Weight;

  constructor(
    protected classificationService: IManageClassificationService,
    protected notificationService: NotificationService,
  ) {
  }

  ngOnInit() {
    this.classificationService.getClassificationDate()
      .subscribe(classificationDate => this.classificationDate = classificationDate);
  }

  updateClassification() {
    this.isLoading = true;
    this.classificationService.updateClassification()
      .subscribe(
        () => {
          this.notificationService.showSuccess(this.classificationMessages.UPDATE_CLASSIFICATION_SUCCESS);
        },
        (error) => {
          console.error(error);
          return this.notificationService.showError(this.classificationMessages.UPDATE_CLASSIFICATION_ERROR);
        })
      .add(
        () => {
          return this.isLoading = false;
        });
  }
}
