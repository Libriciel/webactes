import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreferencesPageComponent } from './preferences-page/preferences-page.component';
import { PreferencesRoutingModule } from './routing/preferences-routing/preferences-routing.module';
import { LibricielCommonModule } from '../ls-common/libriciel-common.module';


@NgModule({
  declarations: [
    PreferencesPageComponent
  ],
  imports: [
    CommonModule,
    PreferencesRoutingModule,
    LibricielCommonModule
  ]
})
export class PreferencesModule {
}
