import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../ls-common/model/menu-item';
import { AppMessages } from '../../app-messages';
import { RoutingPaths } from '../../wa-common/routing-paths';

@Component({
  selector: 'wa-preferences-page',
  templateUrl: './preferences-page.component.html',
  styleUrls: ['./preferences-page.component.scss']
})
export class PreferencesPageComponent implements OnInit {

  preferenceMenu: MenuItem[] = [];

  constructor() {
  }

  ngOnInit() {
    this.preferenceMenu = [
      {
        name: AppMessages.MENU_PREFERENCES_NOTIFICATIONS_OPTIONS,
        ref: RoutingPaths.PREFERENCES_NOTIFICATIONS_OPTIONS_PATH
      }
    ];
  }

}
