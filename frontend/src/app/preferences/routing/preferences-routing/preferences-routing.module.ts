import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { PreferencesPageComponent } from '../../preferences-page/preferences-page.component';
import { PreferencesMessages } from '../../i18n/preferences-messages';

const routes: Routes = [{
  path: RoutingPaths.PREFERENCES_PATH,
  component: PreferencesPageComponent,
  data: {
    pageParameters: {
      title: PreferencesMessages.PAGE_PREFERENCES_TITLE,
      breadcrumb: [
        {name: PreferencesMessages.PAGE_PREFERENCES_TITLE, location: RoutingPaths.PREFERENCES_PATH}
      ]
    } as PageParameters
  }
}];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class PreferencesRoutingModule {
}
