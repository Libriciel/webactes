export class VoteMessages {
  static readonly HEADER_TITLE_VOTERS = 'Élus';
  static readonly HEADER_TITLE_ABSENT_VOTERS = 'Élus absents';
  static readonly HEADER_TITLE_SHORTCUT = 'Raccourcis';
  static readonly COMMENT_TITLE = 'Commentaire';
  static readonly VOTE_METHOD = 'Saisie du vote';
  static readonly METHOD_PLACEHOLDER = 'Choisir une méthode de vote';
  static readonly PRESIDENT_TITLE = 'Président du vote';
  static readonly ACTOR_PLACE_HOLDER = 'Sélectionner un président';
  static readonly VOTE_PRESIDENT_LABEL = 'Président(e) du vote';
  static readonly HEADER_TITLE_RESULT = 'Résultat du vote';
  static readonly HEADER_TITLE_ACTFOR = 'Prendre acte';
  static readonly HEADER_TITLE_NAME = 'Nom';
  static readonly EDIT_VOTERS_LIST = 'Modifier la liste des élus présents ';
  static readonly VOTE_SUCCESS = 'Vote enregistré avec succès';
  static readonly VOTE_ERROR = 'Erreur lors de la saisie du vote';
  static readonly SAVE_PRESENCE_SUCCESS = 'La liste de présence à été enregistrée avec succès';
  static readonly SAVE_PRESENCE_ERROR = `Erreur lors de l'enregistrement de la liste de présence`;
  static readonly GET_VOTERS_LIST = 'Récupérer la liste de présence précédente';
  static readonly DUPLICATE_PRESENCE_SUCCESS = 'La liste de présence précédente à été récupéré avec succès';
  static readonly DUPLICATE_PRESENCE_ERROR = 'Erreur lors de la récupération de la liste de présence précédente.';
  static readonly LABEL_NO_PARTICIPATING = 'Pas de participation';
  static readonly LABEL_NO = 'Non';
  static readonly LABEL_YES = 'Oui';
  static readonly LABEL_ABSTENTION = 'Abstention';
  static readonly LABEL_ADOPTED = 'Adopté';
  static readonly LABEL_REJECTED = 'Rejeté';

  static readonly VOTER_LIST_MODAL_TITLE = 'Modification de la liste des élus présents';
  static readonly HEADER_TITLE_PRESENT_VOTER = 'Présent';
  static readonly HEADER_TITLE_REPRESENTATIVE_VOTER = 'Mandataire';
  static readonly PLACE_HOLDER_REPRESENTATIVE_VOTERS = 'Sélectionnez un mandataire';
  static readonly LABEL_REPRESENTATIVE_VOTERS = 'Mandataires';
  static readonly TITLE_YES_NO_TOGGLE_BUTTON = 'Oui / Non';

  static getTotalVote(totalVotes: number, totalVoters: number) {
    return 'Total: ' + totalVotes + '/' + totalVoters;
  }


  updateListCurrentVote(projectName: string) {
    return `La liste de présence a été mise à jour pour le vote ${projectName}`;
  }

  updateListCurrentVoteFailed(projectName: string) {
    return `Erreur lors de la mise à jour de la liste de présence pour le vote ${projectName}`;
  }
}
