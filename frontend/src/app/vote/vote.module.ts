import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { LsComposantsModule } from '@libriciel/ls-composants';
import { ProjectsModule } from '../projects/projects.module';
import { SharedModule } from '../shared';
import { VoteComponent } from './components/vote/vote.component';
import { VotersListModalComponent } from './components/voters-list-modal/voters-list-modal.component';
import { VoterRepresentativeJoinPipe } from './pipes/voter-representative-join.pipe';
import { DetailedVoteResultComponent } from './components/detailed-vote-result/detailed-vote-result.component';
import { TotalVoteResultComponent } from './components/total-vote-result/total-vote-result.component';
import { ResultVoteResultComponent } from './components/result-vote-result/result-vote-result.component';
import { TakeActResultComponent } from './components/take-act-result/take-act-result.component';

@NgModule({
  declarations: [
    VoteComponent,
    VotersListModalComponent,
    VoterRepresentativeJoinPipe,
    DetailedVoteResultComponent,
    TotalVoteResultComponent,
    ResultVoteResultComponent,
    TakeActResultComponent
  ],
  exports: [
    VoteComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgbAccordionModule,
    ProjectsModule,
    LsComposantsModule
  ],
})
export class VoteModule {
}
