import { Component, OnInit } from '@angular/core';
import { VotesService } from '../../../ls-common/services/http-services/vote/votes.service';
import { NotificationService } from '../../../ls-common';
import { Router } from '@angular/router';
import { AbstractVoteResultComponent } from '../abstract-vote-result/abstract-vote-result-component';
import { Vote } from '../../../model/vote/vote';
import { VoteMethodEnum } from '../../../model/vote/vote-method.enum';

@Component({
  selector: 'wa-total-vote-result',
  templateUrl: './total-vote-result.component.html',
  styleUrls: ['./total-vote-result.component.scss']
})
export class TotalVoteResultComponent extends AbstractVoteResultComponent implements OnInit {

  constructor(protected voteService: VotesService,
              protected notificationService: NotificationService,
              protected router: Router) {
    super(voteService, notificationService, router);
  }

  yes_counter: number;
  no_counter: number;
  abstentions_counter: number;
  no_words_counter: number;

  // @Todo remove
  fakeVote: Vote = new Vote();

  ngOnInit() {
    super.ngOnInit();
    this.yes_counter = this.vote.yes_counter;
    this.no_counter = this.vote.no_counter;
    this.abstentions_counter = this.vote.abstentions_counter;
    this.no_words_counter = this.vote.no_words_counter;
    this.onValueChange();
  }

  protected prepareVote(): Vote {
    const vote = super.prepareVote();
    vote.method = VoteMethodEnum.TOTAL;
    vote.yes_counter = this.yes_counter;
    vote.no_counter = this.no_counter;
    vote.abstentions_counter = this.abstentions_counter;
    vote.no_words_counter = this.no_words_counter;
    return vote;
  }

  onValueChange() {
    this.voteAvailable.emit(this.yes_counter > 0 || this.no_counter > 0 || this.abstentions_counter > 0 || this.no_words_counter > 0);
  }
}
