import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalVoteResultComponent } from './total-vote-result.component';

describe('TotalVoteResultComponent', () => {
  let component: TotalVoteResultComponent;
  let fixture: ComponentFixture<TotalVoteResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalVoteResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalVoteResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
