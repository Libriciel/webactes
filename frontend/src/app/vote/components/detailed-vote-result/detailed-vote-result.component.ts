import { Component, Input, OnInit } from '@angular/core';
import { VoteMessages } from '../../vote-messages';
import { Vote } from '../../../model/vote/vote';
import { VoterListItem } from '../../../model/voters-list/voter-list-item';
import { isNumber } from '../../../utility/NgBoostrapUtils';
import { AbstractVoteResultComponent } from '../abstract-vote-result/abstract-vote-result-component';
import { VoteAction } from 'src/app/model/vote/vote-action.enum';
import { VotesService } from '../../../ls-common/services/http-services/vote/votes.service';
import { NotificationService } from '../../../ls-common';
import { Router } from '@angular/router';
import { VoteMethodEnum } from '../../../model/vote/vote-method.enum';

@Component({
  selector: 'wa-detailed-vote-result',
  templateUrl: './detailed-vote-result.component.html',
  styleUrls: ['./detailed-vote-result.component.scss']
})
export class DetailedVoteResultComponent extends AbstractVoteResultComponent implements OnInit {

  readonly voteMessages = VoteMessages;
  protected readonly VoteAction = VoteAction;

  @Input()
  voters: VoterListItem[] = [];
  @Input()
  voterGroups: VoterListItem[] = [];
  @Input()
  presentAndRepresentedVoters: VoterListItem[] = [];

  voteShortcutListId = 'vote-shortcut-list';
  votersListId = 'voters-list';
  absentVotersListId = 'absent-voters-list';

  // @Todo remove
  fakeVote = new Vote();

  constructor(protected voteService: VotesService,
              protected notificationService: NotificationService,
              protected router: Router) {
    super(voteService, notificationService, router);
  }


  ngOnInit() {
    super.ngOnInit();
    this.onValueChange();
  }

  getOpenedAccordion(): string {
    return this.voteShortcutListId;
  }

  selectAllVoteOnChange(element: VoterListItem) {
    this.voters.map(voter =>
      this.sitting.typesitting.actorGroups.find(group => group.name === element.name).actors.map(actor => {
        if (actor.id === voter.actor_id && (voter.is_present || (!voter.is_present && isNumber(voter.mandataire_id)))) {
          voter.result = element.result;
        }
        this.onValueChange();
      }));
  }

  protected prepareVote(): Vote {
    const vote = super.prepareVote();
    vote.actors_votes = this.voters.map(voterListItem => ({
        id: voterListItem.actor_id,
        actor_id: voterListItem.actor_id,
        vote_id: vote.id,
        result: voterListItem.result,
      })
    );
    vote.method = VoteMethodEnum.DETAILS;
    vote.yes_counter = this.getVoteCount(VoteAction.YES);
    vote.no_counter = this.getVoteCount(VoteAction.NO);
    vote.abstentions_counter = this.getVoteCount(VoteAction.ABSTENTION);
    vote.no_words_counter = this.getVoteCount(VoteAction.NO_WORDS);
    return vote;
  }

  getTotalVotes(): number {
    return this.getVoteCount(VoteAction.YES)
      + this.getVoteCount(VoteAction.NO)
      + this.getVoteCount(VoteAction.ABSTENTION)
      + this.getVoteCount(VoteAction.NO_WORDS);
  }

  getTotalVoters(): number {
    return this.presentAndRepresentedVoters.length;
  }

  getVoteCount(voteAction: VoteAction): number {
    return this.voters.filter(voter => (voter.is_present || (!voter.is_present && voter.mandataire_id))
      && voter.result === voteAction).length;
  }

  getAbsentVoters(): VoterListItem[] {
    return this.voters.filter(voter => !voter.is_present && !voter.mandataire_id);
  }

  onValueChange() {
    this.voteAvailable.emit(this.getTotalVoters() > 0 && this.getTotalVoters() === this.getTotalVotes());
  }
}
