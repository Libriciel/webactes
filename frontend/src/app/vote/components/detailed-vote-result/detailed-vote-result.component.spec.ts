import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedVoteResultComponent } from './detailed-vote-result.component';

describe('DetailedVoteResultComponent', () => {
  let component: DetailedVoteResultComponent;
  let fixture: ComponentFixture<DetailedVoteResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedVoteResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedVoteResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
