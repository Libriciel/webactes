import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakeActResultComponent } from './take-act-result.component';

describe('TakeActResultComponent', () => {
  let component: TakeActResultComponent;
  let fixture: ComponentFixture<TakeActResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakeActResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakeActResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
