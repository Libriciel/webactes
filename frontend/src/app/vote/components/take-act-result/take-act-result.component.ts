import { Component, OnInit } from '@angular/core';
import { VotesService } from '../../../ls-common/services/http-services/vote/votes.service';
import { NotificationService } from '../../../ls-common';
import { Router } from '@angular/router';
import { Vote } from '../../../model/vote/vote';
import { VoteMethodEnum } from '../../../model/vote/vote-method.enum';
import { AbstractVoteResultComponent } from '../abstract-vote-result/abstract-vote-result-component';

@Component({
  selector: 'wa-take-act-result',
  templateUrl: './take-act-result.component.html',
  styleUrls: ['./take-act-result.component.scss']
})
export class TakeActResultComponent extends AbstractVoteResultComponent implements OnInit {


  constructor(protected voteService: VotesService,
              protected notificationService: NotificationService,
              protected router: Router) {
    super(voteService, notificationService, router);
  }

  takeAct: boolean;

  // @Todo remove
  fakeVote: Vote = new Vote();

  ngOnInit() {
    super.ngOnInit();
    this.takeAct = !!this.vote.take_act;
    this.onValueChange();
  }

  protected prepareVote(): Vote {
    const vote = super.prepareVote();
    vote.method = VoteMethodEnum.TAKE_ACT;
    vote.take_act = this.takeAct;
    return vote;
  }

  onValueChange() {
    this.voteAvailable.emit(this.takeAct);
  }
}
