import { Component, OnInit } from '@angular/core';
import { AbstractVoteResultComponent } from '../abstract-vote-result/abstract-vote-result-component';
import { VotesService } from '../../../ls-common/services/http-services/vote/votes.service';
import { NotificationService } from '../../../ls-common';
import { Router } from '@angular/router';
import { Vote } from '../../../model/vote/vote';
import { VoteMethodEnum } from '../../../model/vote/vote-method.enum';

@Component({
  selector: 'wa-result-vote-result',
  templateUrl: './result-vote-result.component.html',
  styleUrls: ['./result-vote-result.component.scss']
})
export class ResultVoteResultComponent extends AbstractVoteResultComponent implements OnInit {

  constructor(protected voteService: VotesService,
              protected notificationService: NotificationService,
              protected router: Router) {
    super(voteService, notificationService, router);
  }

  result: boolean;

  // @Todo remove
  fakeVote: Vote = new Vote();

  ngOnInit() {
    super.ngOnInit();
    if (!!this.vote.result) {
      this.result = true;
    } else if (this.vote.result === false) {
      this.result = false;
    }
    this.onValueChange();
  }

  protected prepareVote(): Vote {
    const vote = super.prepareVote();
    vote.method = VoteMethodEnum.RESULT;
    vote.result = this.result;
    return vote;
  }

  onValueChange() {
    this.voteAvailable.emit(this.result !== undefined);
  }
}
