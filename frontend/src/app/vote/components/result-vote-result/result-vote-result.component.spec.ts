import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultVoteResultComponent } from './result-vote-result.component';

describe('ResultVoteResultComponent', () => {
  let component: ResultVoteResultComponent;
  let fixture: ComponentFixture<ResultVoteResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultVoteResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultVoteResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
