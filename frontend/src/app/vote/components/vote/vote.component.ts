import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VoterListItem } from 'src/app/model/voters-list/voter-list-item';
import { Vote } from 'src/app/model/vote/vote';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VoteMessages } from '../../vote-messages';
import { CommonIcons, CommonMessages, NotificationService } from '../../../ls-common';
import { VotersListService } from '../../../ls-common/services/http-services/voters-list/voters-list.service';
import { ManageSittingService } from '../../../ls-common/services/manage-sitting.service';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { VotersListModalComponent } from '../voters-list-modal/voters-list-modal.component';
import { Sitting } from '../../../model/Sitting/sitting';
import { Subject } from 'rxjs';
import { VoteMethodEnum } from '../../../model/vote/vote-method.enum';
import { VoteMethod } from 'src/app/model/vote/vote-method';
import { VoteMethods } from '../../../model/vote/vote-methods';

@Component({
  selector: 'wa-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss']
})
export class VoteComponent implements OnInit, AfterViewChecked {

  readonly voteMessages = VoteMessages;
  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly voteMethodEnum = VoteMethodEnum;
  readonly voteMethods = VoteMethods.methodsChoices;

  method: VoteMethod = this.voteMethods[0].value;
  votePresident: VoterListItem = null;
  vote: Vote;
  projectId: number;
  sitting: Sitting;
  voters: VoterListItem[] = [];
  voterGroups: VoterListItem[] = [];
  saveEventsSubject: Subject<void> = new Subject<void>();
  voteAvailable = false;
  savingVotersList = false;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected votersListService: VotersListService,
    protected router: Router,
    protected notificationService: NotificationService,
    private modalService: NgbModal,
    protected manageSittingService: ManageSittingService,
    protected changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.sitting = data.data.sitting;
      this.vote = data.data.vote;
      this.projectId = this.vote.project_id;

      this.getVoterItemsList();
      this.initializeVoteGroups();

      const method = this.voteMethods.find(method => method.value.code === this.vote.method);
      this.method = method ? method.value : this.method;
    });
  }

  getVoterItemsList() {
    this.voters = [];
    this.sitting.actors_projects_sittings = this.sitting.actors_projects_sittings
      .sort((a, b) =>
        a.actor.lastname.toLocaleLowerCase() < b.actor.lastname.toLocaleLowerCase()
          ? -1 : a.actor.lastname.toLocaleLowerCase() === b.actor.lastname.toLocaleLowerCase()
            ? 0 : 1
      );

    this.sitting.actors_projects_sittings.map(voter => {
      if (voter.actor) {
        voter.name = voter.actor.name;
        if (this.vote.id && this.vote.method === VoteMethodEnum.DETAILS) {
          const previous_voter = this.vote.actors_votes.find(vote => vote.actor_id === voter.actor_id);
          voter.result = (voter.is_present === false && voter.mandataire_id === null) ? null : previous_voter.result;
        }
        this.voters.push(
          new VoterListItem(
            voter.id,
            voter.actor_id,
            voter.name,
            voter.is_present,
            voter.result,
            voter.actor,
            voter.president,
            voter.mandataire_id)
        );
      }
    });
    this.voters.forEach(voter =>
      voter.representative = voter.mandataire_id
        ? this.voters.find(voter1 => voter1.actor_id === voter.mandataire_id)
        : null);
    this.initializePresident();
  }

  presentAndRepresentedVoters(): VoterListItem[] {
    return this.voters.filter(voter => voter.representative != null)
      .concat(this.voters.filter(voter => voter.is_present === true));
  }

  editVotersList() {
    const modalRef = this.modalService.open(VotersListModalComponent, {
      backdrop: 'static',
      centered: true,
      size: 'lg'
    });

    modalRef.componentInstance.votersListItems = this.voters;
    modalRef.componentInstance.sittingId = this.sitting.id;
    modalRef.componentInstance.projectId = this.projectId;

    modalRef.result.then(() => {
    }).catch(error => {
      if (error.isError === undefined || error.isError) {
        console.error('Caught error in popup VotersListModalComponent : ', error);
      }
    }).finally(() =>
      this.manageSittingService.getWithProjectVotes(this.sitting.id, this.projectId).subscribe(
        value => {
          this.sitting = value.sitting;
          this.getVoterItemsList();
        }));
  }

  getVotersListFromPrecedent() {
    this.sitting.actors_projects_sittings_previous.forEach(previousActor => {
      const actor = this.sitting.actors_projects_sittings.find(actor1 => actor1.actor_id === previousActor.actor_id);
      actor.mandataire_id = previousActor.mandataire_id;
      actor.is_present = previousActor.is_present;
      actor.representative = this.sitting.actors_projects_sittings.find(actor1 => actor1.actor_id === previousActor.mandataire_id);
    });
    this.savingVotersList = true;
    this.votersListService.save(this.sitting.id, this.projectId, this.sitting.actors_projects_sittings)
      .subscribe(
        () => {
          this.notificationService.showSuccess(this.voteMessages.DUPLICATE_PRESENCE_SUCCESS);
          this.getVoterItemsList();
        },
        () => this.notificationService.showError(this.voteMessages.DUPLICATE_PRESENCE_ERROR))
      .add(() => this.savingVotersList = false);
  }

  private initializeVoteGroups() {
    this.sitting.typesitting.actorGroups.map(
      actorGroup => this.voterGroups.push(new VoterListItem(
        0,
        undefined,
        actorGroup.name,
        undefined,
        undefined,
        undefined,
        actorGroup
      ))
    );
  }

  private initializePresident() {
    const president = this.vote.president_id ? this.voters.find(voter => voter.actor_id === this.vote.president_id) : undefined;
    if (president) {
      this.votePresident = new VoterListItem(
        president ? president.id : null,
        president ? president.actor_id : null,
        president ? president.name : null,
        president ? president.is_present : null,
        undefined,
        undefined,
        president ? president.actor : null,
        undefined,
      );
    }
  }

  saveVote() {
    this.saveEventsSubject.next();
  }

  cancel() {
    this.router.navigateByUrl(RoutingPaths.sittingPath(this.sitting.id)).then();
  }

  setVoteAvailable($event: boolean) {
    this.voteAvailable = $event;
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }
}
