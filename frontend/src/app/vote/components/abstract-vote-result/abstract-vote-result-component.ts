import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { VotesService } from '../../../ls-common/services/http-services/vote/votes.service';
import { NotificationService } from '../../../ls-common';
import { Router } from '@angular/router';
import { VoteMessages } from '../../vote-messages';
import { Sitting } from '../../../model/Sitting/sitting';
import { Vote } from '../../../model/vote/vote';
import { VoterListItem } from '../../../model/voters-list/voter-list-item';
import { Observable } from 'rxjs';

@Component({
  template: ''
})
export abstract class AbstractVoteResultComponent implements OnInit, OnDestroy {

  voteMessages = VoteMessages;

  @Input()
  sitting: Sitting;
  @Input()
  vote: Vote = new Vote();
  @Input()
  votePresident: VoterListItem;
  @Output()
  voteAvailable: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() saveEventSubject: Observable<void>;
  private saveEventSubscription;


  constructor(protected voteService: VotesService,
              protected notificationService: NotificationService,
              protected router: Router) {
  }

  ngOnInit(): void {
    this.saveEventSubscription = this.saveEventSubject.subscribe(() => this.saveVote());
  }

  ngOnDestroy() {
    this.saveEventSubscription.unsubscribe();
  }

  protected prepareVote(): Vote {
    return new Vote(
      this.vote.id,
      this.votePresident ? this.votePresident.actor_id : null,
      this.vote.project_id,
      this.vote.yes_counter,
      this.vote.no_counter,
      this.vote.abstentions_counter,
      this.vote.no_words_counter,
      this.vote.take_act,
      this.vote.result,
      this.vote.comment,
      this.vote.opinion,
      this.vote.method,
      this.vote.actors_votes);
  }

  protected saveVote() {
    const vote = this.prepareVote();
    const methodCall = vote.id
      ? this.voteService.edit(vote)
      : this.voteService.add(vote);
    methodCall.subscribe(
      () => {
        this.notificationService.showSuccess(this.voteMessages.VOTE_SUCCESS);
        this.router.navigateByUrl(RoutingPaths.sittingPath(this.sitting.id)).then();
      },
      error => {
        !!error
          ? this.notificationService.showError(error.error.message)
          : this.notificationService.showError(this.voteMessages.VOTE_ERROR);
        console.error(error);
      }
    );
  }
}

