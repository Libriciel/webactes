import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VotersListModalComponent } from './voters-list-modal.component';

describe('VotersListModalComponent', () => {
  let component: VotersListModalComponent;
  let fixture: ComponentFixture<VotersListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VotersListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotersListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
