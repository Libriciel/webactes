import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VotersListService } from '../../../ls-common/services/http-services/voters-list/voters-list.service';
import { NotificationService } from '../../../ls-common';
import { VoterListItem } from '../../../model/voters-list/voter-list-item';
import { FormBuilder, FormGroup } from '@angular/forms';
import { VoteMessages } from '../../vote-messages';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';

@Component({
  selector: 'wa-voters-list-modal',
  templateUrl: './voters-list-modal.component.html',
  styleUrls: ['./voters-list-modal.component.scss']
})
export class VotersListModalComponent implements OnInit {
  votersForm: FormGroup;
  presentVoters: VoterListItem[] = [];

  @Input()
  votersListItems: VoterListItem[] = [];

  @Input()
  sittingId: number;
  @Input()
  projectId: number;

  voteMessages = VoteMessages;
  saving = false;

  waCommonMessages = WACommonMessages;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private votersListService: VotersListService,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit() {
    this.presentVoters = this.getRepresentativesListItems(this.votersListItems);
    this.votersForm = this.formBuilder.group({
      presentVoters: [this.presentVoters],
      votersListItems: [this.votersListItems],
    });


    // S'assurer que votersListItems est correctement chargé et initialisé
    if (!this.votersListItems || this.votersListItems.length === 0) {
      return;
    }

    this.votersForm.valueChanges.subscribe(data => {
      this.presentVoters = this.getRepresentativesListItems(data.votersListItems);
    });
  }

  getRepresentativesListItems(votersListItems: VoterListItem[]): VoterListItem[] {
    return votersListItems.filter(voter => voter.is_present);
  }

  close() {
    this.activeModal.close();
  }

  onToggleIsPresent(id: number) {
    this.presentVoters = this.getRepresentativesListItems(this.votersForm.value.votersListItems);

    const voter = this.votersForm.value.votersListItems.find(v => v.id === id);

    if (voter && voter.is_present) {
      voter.representative = null;
      voter.mandataire_id = null;
    }

    this.clearRepresentative();
  }

  clearRepresentative() {
    this.votersForm.value.votersListItems.forEach(voter => {
      // Check if the representative is not present and clear it
      if (voter.representative && !this.presentVoters.some(presentVoter => presentVoter.id === voter.representative.id)) {
        voter.representative = null;
        voter.mandataire_id = null;
      }
    });
  }

  save() {
    if (this.votersForm.invalid) {
      return;
    }

    this.saving = true;
    this.votersListService.save(this.sittingId, this.projectId, this.votersForm.value.votersListItems).subscribe(
      () => {
        this.notificationService.showSuccess(this.voteMessages.SAVE_PRESENCE_SUCCESS);
        this.close();
      },
      err => {
        const errorMessage = err.error.message || this.voteMessages.SAVE_PRESENCE_ERROR;
        this.notificationService.showError(errorMessage);
      }
    ).add(() => this.saving = false);
  }
}
