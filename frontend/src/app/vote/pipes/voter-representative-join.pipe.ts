import { Pipe, PipeTransform } from '@angular/core';
import { VoterListItem } from '../../model/voters-list/voter-list-item';

@Pipe({name: 'joinVoterRepresentative'})
export class VoterRepresentativeJoinPipe implements PipeTransform {

  transform(voter: VoterListItem, prefix = '- (donne mandat à ', suffix = ' )', separator = ' '): string {
    return voter.name + separator + prefix + voter.representative.name + suffix;
  }
}
