import { UserInfo } from '../../model/user/user-info';
import { StructureInfo } from '../../model/structure-info';
import { StructureSetting } from '../../core';

export let STRUCTURE_USER_INFORMATION: {
  userInformation: UserInfo,
  structureInformation: StructureInfo,
  structureSetting: StructureSetting,
} = {
  userInformation: null,
  structureInformation: null,
  structureSetting: null,
};
