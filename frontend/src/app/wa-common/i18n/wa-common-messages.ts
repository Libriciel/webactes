export class WACommonMessages {

  static CODE_ACT = `Numéro d'Acte`;
  static ACT_NAME = 'Libellé';
  static CLASSIFICATION = 'Classification';
  static TYPE_ACT = `Type d'acte`;
  static MAIN_DOC_TYPE = 'Type de pièce principale';
  static MULTICHANNEL = 'Envoi compl.';
  static ANNEXES = 'Annexes';
  static MISSING_INFO = 'à renseigner';
  static STATE = 'État';
  static SEARCH_OR_FILTER_FIELD_PLACEHOLDER = 'Filtrer';
  static EMPTY_FILTERS = 'Supprimer le filtre';
  static CLEAR_ALL = 'Tout supprimer';
  static NO_RESULT = 'Aucun Résultat';

  static PAGE_RGPD_TITLE = 'Politique de confidentialité';
  static ORDRE = 'Ordre';
  static THEME = 'Thème';

  static SORT_BY_ACT_NAME = 'name';
  static SORT_BY_CODE_ACT = 'code_act';
  static SORT_BY_TYPE_ACT = 'typeact';
  static SORT_BY_ACT_STATE = 'stateact';
  static SORT_BY_ACT_THEME = 'theme';
}

