import { Component, Input, OnInit } from '@angular/core';
import { ReleaseNote } from '../../../model/release-note';
import { ApplicationAboutMessages } from '../../i18n/application-about-messages';

@Component({
  selector: 'wa-release-notes',
  templateUrl: './release-notes.component.html',
  styleUrls: ['./release-notes.component.scss']
})
export class ReleaseNotesComponent implements OnInit {

  public applicationAboutMessages = ApplicationAboutMessages;

  @Input()
  releaseNotes: ReleaseNote[] = [];

  @Input()
  applicationName = '';

  constructor() {
  }

  private static getApplicationVersion(releaseNote: ReleaseNote): string {
    return `${releaseNote.version.major}.${releaseNote.version.minor}.${releaseNote.version.patch}`;
  }

  getVersionTitle(releaseNote: ReleaseNote): string {
    return `[${ReleaseNotesComponent.getApplicationVersion(releaseNote)}] - ${releaseNote.date}`;
  }

  ngOnInit() {
  }
}
