import { Component, OnInit } from '@angular/core';
import { ReleaseNote } from '../../../model/release-note';
import { IManageApplicationVersionService } from '../../services/imanage-application-version-service';
import { Version } from '../../../model/version';

@Component({
  selector: 'wa-application-about',
  templateUrl: './application-about.component.html',
  styleUrls: ['./application-about.component.scss']
})
export class ApplicationAboutComponent implements OnInit {

  releaseNotes: ReleaseNote[];
  licence: string;
  applicationVersion: Version;

  constructor(protected applicationVersionService: IManageApplicationVersionService) {
  }

  ngOnInit() {
    this.applicationVersionService.getReleaseNotes().subscribe(value => this.releaseNotes = value);
    this.applicationVersionService.getLicence().subscribe(value => this.licence = value);
    this.applicationVersionService.getApplicationName().subscribe(value => this.applicationVersion = value);
  }

}
