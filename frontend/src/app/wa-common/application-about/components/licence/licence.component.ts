import { Component, Input, OnInit } from '@angular/core';
import { ApplicationAboutMessages } from '../../i18n/application-about-messages';

@Component({
  selector: 'wa-licence',
  templateUrl: './licence.component.html',
  styleUrls: ['./licence.component.scss']
})
export class LicenceComponent implements OnInit {
  public applicationAboutMessages = ApplicationAboutMessages;

  @Input()
  licence: string;

  constructor() {
  }

  ngOnInit() {
  }

}
