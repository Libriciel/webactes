import { Component, OnInit } from '@angular/core';
import { ApplicationAboutMessages } from '../../i18n/application-about-messages';

@Component({
  selector: 'wa-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {

  public applicationAboutMessages = ApplicationAboutMessages;

  constructor() {
  }

  ngOnInit() {
  }

}
