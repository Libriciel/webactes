export class ApplicationAboutMessages {

  public static ADDED = 'Ajouts';
  public static CHANGED = 'Modifications';
  public static FIXED = 'Corrections';
  public static DEPRECATED = 'Dépréciations';
  public static REMOVED = 'Retraits';
  public static SECURITY = 'Sécurité';
  public static OTHERS = 'Autres';
  public static VERSIONS = 'Versions';
  public static LICENCE = 'Informations légales';
  public static SUPPORT = 'Assistance / support';
  public static SUPPORT_LINK = `<p>Si vous disposez d'un contrat de support et maintenance chez Libriciel SCOP, <a href="https://www.libriciel.fr/support-et-maintenance/" target="_blank">suivez-ce lien</a>.`;
  public static CURRENT_VERSION = `Version courante&nbsp;:&nbsp;`;
  public static LIMITATIONS = `Limitations`;
}
