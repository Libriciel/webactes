import { Observable } from 'rxjs';
import { ReleaseNote } from '../../model/release-note';
import { Version } from '../../model/version';

export abstract class IManageApplicationVersionService {

  abstract getReleaseNotes(): Observable<ReleaseNote[]>;

  abstract getLicence(): Observable<string>;

  abstract getApplicationName(): Observable<Version>;
}
