import { CommonMessages } from '../ls-common';

export class Constants {
  static readonly YES_VALUE = {id: 0, name: CommonMessages.YES, value: true, code: true};
  static readonly NO_VALUE = {id: 1, name: CommonMessages.NO, value: false, code: false};
  static readonly YES_NO_CHOICES = [
    {name: CommonMessages.YES, value: Constants.YES_VALUE},
    {name: CommonMessages.NO, value: Constants.NO_VALUE}
  ];
}
