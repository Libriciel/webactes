export class ThemeSelectorMessages {
  static THEME_LABEL = 'Thème';
  static THEME_PLACEHOLDER = 'Sélectionner le thème';
}
