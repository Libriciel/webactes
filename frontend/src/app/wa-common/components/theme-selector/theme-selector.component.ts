import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ThemeSelectorMessages } from './theme-selector-messages';
import { FlattenTreeItem } from '../../../utility/flatten-tree-item';
import { Theme } from '../../../model/theme/theme';
import { IManageThemeService } from '../../../admin/theme/services/IManageThemeService';
import { WACommonMessages } from '../../i18n/wa-common-messages';

@Component({
  selector: 'wa-theme-selector',
  templateUrl: './theme-selector.component.html',
  styleUrls: ['./theme-selector.component.scss']
})
export class ThemeSelectorComponent implements OnInit {

  messages = ThemeSelectorMessages;
  waCommonMessages = WACommonMessages;

  @Input() requireNotEmpty = false;
  @Input() themeList: FlattenTreeItem[] = null;
  @Input() theme: Theme;
  @Output() themeChange = new EventEmitter<Theme>();
  @Output() change = new EventEmitter<string>();

  constructor(protected iManageThemeService: IManageThemeService) {
  }

  ngOnInit() {
  }

  getLeafClass(item: FlattenTreeItem): string {
    return item.depth === 0 ? 'root' : 'leaf leaf-' + item.depth;
  }

}
