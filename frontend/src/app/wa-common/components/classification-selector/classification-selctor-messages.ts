export class ClassificationSelectorMessages {
  static CLASSIFICATION_LABEL = 'Classification';
  static CLASSIFICATION_PLACEHOLDER = 'Sélectionner la classification';
}
