import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ClassificationSelectorMessages } from './classification-selctor-messages';
import { ClassificationSubject } from 'src/app/model/classification/classification-subject';
import { IManageClassificationService } from 'src/app/classification/services/i-manage-classification-service';
import { FlattenTreeItem } from 'src/app/utility/flatten-tree-item';
import { WACommonMessages } from '../../i18n/wa-common-messages';

@Component({
  selector: 'wa-classification-selector',
  templateUrl: './classification-selector.component.html',
  styleUrls: ['./classification-selector.component.scss']
})
export class ClassificationSelectorComponent implements OnInit {

  messages = ClassificationSelectorMessages;
  waCommonMessages = WACommonMessages;

  @Input() requireNotEmpty = false;
  @Input() classificationList: FlattenTreeItem[] = null;
  @Input() classification: ClassificationSubject;
  @Output() classificationChange = new EventEmitter<ClassificationSubject>();
  @Output() change = new EventEmitter<string>();

  constructor(protected iManageClassificationService: IManageClassificationService) {
  }

  ngOnInit() {
    if (!this.classificationList || this.classificationList.length === 0) {
      this.iManageClassificationService.getClassificationSubjects()
        .subscribe(classifications => this.classificationList = classifications);
    }
  }

  additionalClasses(): string {
    return this.requireNotEmpty && !this.classification ? 'ng-invalid' : '';
  }
}
