import { Component, Input, OnInit } from '@angular/core';
import { Config } from '../../../Config';
import { CommonIcons } from '../../../ls-common/icons/common-icons';

@Component({
  selector: 'wa-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  commonIcons = CommonIcons;
  config = Config;

  @Input() menu;
  @Input() name: string;
  @Input() logo: string;

  constructor() {
  }

  ngOnInit() {
  }

}
