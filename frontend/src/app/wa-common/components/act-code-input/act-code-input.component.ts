import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
import { IManageProjectsService } from 'src/app/projects/services/i-manage-projects.service';
import { map } from 'rxjs/operators';
import { ActCodeMessages } from './act-code-messages';

@Component({
  selector: 'wa-act-code-input',
  templateUrl: './act-code-input.component.html',
  styleUrls: ['./act-code-input.component.scss']
})
export class ActCodeInputComponent implements OnInit, OnDestroy {

  messages = ActCodeMessages;

  formGroup = new UntypedFormGroup({
    codeAct: new UntypedFormControl('', [
        Validators.pattern('[A-Z\\d\\s_]+'),
        Validators.maxLength(15)],
      this.checkCodeActUniq.bind(this)),
    isMultichannel: new UntypedFormControl('')
  });

  @Input() requireNotEmpty = false;
  @Input() codeValue: string;
  @Output() codeValueChange = new EventEmitter();
  @Output() validityChange = new EventEmitter();
  statusChangesSubscription: Subscription;
  private originalCodeAct: string;

  constructor(protected iManageProject: IManageProjectsService) {
  }

  ngOnInit() {
    this.formGroup.patchValue({codeAct: this.codeValue});
    this.originalCodeAct = this.codeValue;
    this.statusChangesSubscription = this.formGroup.statusChanges
      .subscribe(() => {
          this.codeValue = this.formGroup.get('codeAct').value;
          this.codeValueChange.emit(this.codeValue);
          this.validityChange.emit(this.formGroup.valid);
        }
      );
  }

  ngOnDestroy() {
    this.statusChangesSubscription.unsubscribe();
  }

  checkCodeActUniq(control: AbstractControl): Observable<{ [key: string]: any } | null> {
    if (control.dirty && this.originalCodeAct !== control.value) {
      return this.iManageProject.checkCodeActUniq(control.value)
        .pipe(map(value => !value ? {'codeActExists': true} : null));
    } else {
      return of(null);
    }
  }

}
