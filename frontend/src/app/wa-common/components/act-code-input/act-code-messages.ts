export class ActCodeMessages {
  static ACT_CODE = `Numéro de l'acte`;
  static ALREADY_USED_ACT_CODE = `Ce code d'acte est déjà utilisé`;
  static ACT_CODE_PLACEHOLDER = 'Maximum 15 caractères, majuscules, chiffres et _ pour le caractère spécial';
}
