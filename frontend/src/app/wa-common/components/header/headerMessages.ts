export class HeaderMessages {
  static MENU_FAQ = 'À propos - Aide';
  static MENU_FAQ_ICON = 'fas fa-question';
  static MENU_GPRD = 'Politique de confidentialité (RGPD)';
  static MENU_GPRD_ICON = 'fas fa-user-secret';
  static MENU_LOG_OUT = 'Se déconnecter';
  static MENU_LOGOUT_ICON = 'fas fa-sign-out-alt';
  static MENU_ADMIN = 'Administration';
  static MENU_ADMIN_ICON = 'fas fa-cog';
  static MENU_PREFERENCES = 'Préférences utilisateur';
  static MENU_PREFERENCES_ICON = 'fas fa-user';
  static MENU_SEARCH_ICON = 'fas fa-search-plus';
}
