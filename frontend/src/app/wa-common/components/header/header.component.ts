import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../../ls-common/model/menu-item';
import { HeaderMessages } from './headerMessages';
import { STRUCTURE_USER_INFORMATION } from '../../variables/structure-user-info';
import { UserService } from '../../../ls-common/services/http-services/user/User.service';
import { RoutingPaths } from '../../routing-paths';
import { CommonRights } from '../../rights/common-rights';

@Component({
  selector: 'wa-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  messages = HeaderMessages;
  routingPaths = RoutingPaths;
  structureUserInformation = STRUCTURE_USER_INFORMATION;
  menu: MenuItem[][] = CommonRights.hasAdminMenuRight() || CommonRights.hasSuperAdminMenuRight()
    ? [[
      {
        name: this.messages.MENU_FAQ,
        icon: this.messages.MENU_FAQ_ICON,
        ref: RoutingPaths.ABOUT_PATH
      }, {
        name: this.messages.MENU_GPRD,
        icon: this.messages.MENU_GPRD_ICON,
        ref: RoutingPaths.RGPD_PAGE_PATH
      }, {
        name: this.messages.MENU_PREFERENCES,
        icon: this.messages.MENU_PREFERENCES_ICON,
        ref: RoutingPaths.PREFERENCES_PATH,
      }, {
        name: this.messages.MENU_ADMIN,
        icon: this.messages.MENU_ADMIN_ICON,
        ref: RoutingPaths.ADMIN_PATH,
      }
    ], [{
      name: this.messages.MENU_LOG_OUT,
      icon: this.messages.MENU_LOGOUT_ICON,
      action: this.logout.bind(this)
    }]]
    : [[
      {
        name: this.messages.MENU_FAQ,
        icon: this.messages.MENU_FAQ_ICON,
        ref: RoutingPaths.ABOUT_PATH
      }, {
        name: this.messages.MENU_GPRD,
        icon: this.messages.MENU_GPRD_ICON,
        ref: RoutingPaths.RGPD_PAGE_PATH
      }, {
        name: this.messages.MENU_PREFERENCES,
        icon: this.messages.MENU_PREFERENCES_ICON,
        ref: RoutingPaths.PREFERENCES_PATH,
      }
    ], [{
      name: this.messages.MENU_LOG_OUT,
      icon: this.messages.MENU_LOGOUT_ICON,
      action: this.logout.bind(this)
    }]]
  ;

  constructor(protected  userService: UserService) {
  }

  logout() {
    this.userService.logout().subscribe(
      () => {
      },
      error => console.error(error));
  }

  ngOnInit() {
  }
}
