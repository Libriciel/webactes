import { BreadcrumbItem } from '../../../model/breadcrumb-item';

export interface PageParameters {
  fluid?: boolean;
  title?: string;
  subTitle?: string;
  breadcrumb: BreadcrumbItem[];
}
