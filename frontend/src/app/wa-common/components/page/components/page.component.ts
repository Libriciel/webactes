import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { BreadcrumbItem } from '../../../model/breadcrumb-item';

@Component({
  selector: 'wa-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  @HostBinding('class') class = 'wa-page';
  fluid = false;
  title = null;
  subTitle = null;
  breadcrumb: BreadcrumbItem[] = [];

  constructor(protected  router: Router) {
    router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit() {
    this.router.events.pipe(
      filter((event) => event instanceof ActivationEnd)
    ).subscribe((event) => {
      // @ts-ignore
      if (!event.snapshot.firstChild) {
        // @ts-ignore
        this.title = event.snapshot.data.pageParameters.title
          // @ts-ignore
          ? event.snapshot.data.pageParameters.title
          : null;
        // @ts-ignore
        this.subTitle = event.snapshot.data.pageParameters.subTitle
          // @ts-ignore
          ? event.snapshot.data.pageParameters.subTitle
          : null;
        // @ts-ignore
        this.breadcrumb = event.snapshot.data.pageParameters.breadcrumb
          // @ts-ignore
          ? event.snapshot.data.pageParameters.breadcrumb
          : [];
        // @ts-ignore
        this.fluid = !!event.snapshot.data.pageParameters.fluid;
        this.class = this.fluid
          ? 'wa-page container-fluid'
          : 'wa-page container';
      }
    });
  }
}
