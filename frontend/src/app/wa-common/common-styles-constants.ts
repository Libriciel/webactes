import { Style } from './style.enum';

export class CommonStylesConstants {

  static getStyleClass(style: Style): string {
    return `wa-${style.toString()}`;
  }

  static getButtonStyleClass(style: Style): string {
    return `btn-${style.toString()}`;
  }

  static getButtonOutlineStyleClass(style: Style): string {
    return `btn-outline-${style.toString()}`;
  }

  static getButtonLinkStyleClass(): string {
    return `btn-link`;
  }

  static getButtonMenuItemStyleClass(): string {
    return `menu-item ${CommonStylesConstants.getButtonLinkStyleClass()}`;
  }

  static getOnHoverStyleClass(style: Style): string {
    return `${this.getStyleClass(style)}-on-hover`;
  }

  static getSuccessStyleClass(): string {
    return this.getStyleClass(Style.SUCCESS);
  }

  static getWarningStyleClass(): string {
    return this.getStyleClass(Style.WARNING);
  }

  static getErrorStyleClass(): string {
    return this.getStyleClass(Style.DANGER);
  }

  static getPanelStyleClass(style: Style): string {
    return `wa-panel ${this.getStyleClass(style)}-panel`;
  }

  static getButtonSmallSizeClass() {
    return 'fa-sm';
  }

  static getButtonLargeSizeClass() {
    return 'fa-lg';
  }
}
