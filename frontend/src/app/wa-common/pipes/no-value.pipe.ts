import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noValue'
})
export class NoValuePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value ? value : 'A renseigner';
  }

}
