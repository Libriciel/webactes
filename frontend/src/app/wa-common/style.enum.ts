export enum Style {
  NEUTRAL = 'secondary',
  NORMAL = 'primary',
  SUCCESS = 'success',
  WARNING = 'warning',
  DANGER = 'danger',
  INFO = 'info'
}
