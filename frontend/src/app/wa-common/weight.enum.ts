export enum Weight {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  TERTIARY = 'tertiary',
  MENU_ITEM = 'menu-item'
}
