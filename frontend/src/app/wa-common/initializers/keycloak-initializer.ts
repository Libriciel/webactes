import { KeycloakService } from 'keycloak-angular';
import { UserService } from '../../ls-common/services/http-services/user/User.service';
import { STRUCTURE_USER_INFORMATION } from '../variables/structure-user-info';
import { StructureInfo } from '../../model/structure-info';
import { UserInfo } from '../../model/user/user-info';
import { StructureSetting } from '../../core';
import { environment } from '../../../environments/environment';

export function keycloakInitialize(keycloak: KeycloakService, userService: UserService): () => Promise<any> {
  return (): Promise<any> =>
    new Promise(async (resolve, reject) => {
      try {
        await keycloak.init({
          config: environment.keycloak,
          initOptions: {
            onLoad: 'login-required',
            checkLoginIframe: false,
            // redirectUri: window.location.origin + '/login'
          },
          bearerExcludedUrls: ['/assets']  // path not protected by keycloak
        } as any);

        userService.loginStructure().subscribe(
          () => userService.getStructureInfo()
            .subscribe(rawInfo => {
                const structureSetting: StructureSetting = {} as StructureSetting;
                Object.assign(structureSetting, rawInfo.structureSetting);
                STRUCTURE_USER_INFORMATION.structureInformation = new StructureInfo(rawInfo.structure);
                STRUCTURE_USER_INFORMATION.userInformation = new UserInfo(rawInfo.user);
                STRUCTURE_USER_INFORMATION.structureSetting = structureSetting;

                resolve(null);
              },
              error1 => reject(error1)),
          error1 => reject(error1));
      } catch (error) {
        reject(error);
      }
    });
}
