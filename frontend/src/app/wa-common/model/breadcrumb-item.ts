export interface BreadcrumbItem {
  name: string;
  location?: string;
}
