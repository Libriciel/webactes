export class Version {
  major: number;
  minor: number;
  patch: number;
  preReleaseIdentifier?: string;
  name?: string;

  getFullName(): string {
    return `${this.name}  ${this.major}.${this.minor}.${this.patch}`
      + (this.preReleaseIdentifier ? `-${this.preReleaseIdentifier}` : '');
  }

  constructor(version: {
    major: number,
    minor: number,
    patch: number,
    preReleaseIdentifier?: string,
    name?: string
  }) {
    this.major = version.major;
    this.minor = version.minor;
    this.patch = version.patch;
    this.preReleaseIdentifier = version.preReleaseIdentifier;
    this.name = version.name;
  }
}
