import { Version } from './version';

export interface ReleaseNote {
  version: Version;
  date: string;
  added?: string[];
  changed?: string[];
  deprecated?: string[];
  removed?: string[];
  fixed?: string[];
  security?: string[];
  limitations?: string[];
  others?: string[];
}
