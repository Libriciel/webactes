import { Injectable } from '@angular/core';
import { CommonIcons, CommonMessages } from '../../ls-common';
import { GoBackAction } from '../../sittings/components/sittings-list/actions/go-back-action';
import { GeneralAction } from './general-action.enum';
import { Action } from '../../ls-common/model/action';

@Injectable({
  providedIn: 'root'
})
export class GeneralActionFactory {
  constructor(protected goBackAction: GoBackAction) {
  }

  getAction(action: GeneralAction, options?: { goBackUrl?: string }): Action<void> {
    switch (action) {
      case GeneralAction.GO_BACK:
        return new Action({
          name: CommonMessages.GO_BACK,
          icon: CommonIcons.GO_BACK_ICON,
          actuator: this.goBackAction.setGoBackUrl(options['goBackUrl']),
        });
      default:
        throw new Error(`unimplemented action ${action}`);
    }
  }
}
