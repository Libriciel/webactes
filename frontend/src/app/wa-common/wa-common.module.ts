import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';
import { PageComponent } from './components/page/components/page.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LsComposantsModule } from '@libriciel/ls-composants';
import { ClassificationSelectorComponent } from './components/classification-selector/classification-selector.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActCodeInputComponent } from './components/act-code-input/act-code-input.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RgpdPageComponent } from './components/rgpd-page/rgpd-page.component';
import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import { ReleaseNotesComponent } from './application-about/components/release-notes/release-notes.component';
import { ApplicationAboutComponent } from './application-about/components/application-about-page/application-about.component';
import { LicenceComponent } from './application-about/components/licence/licence.component';
import { SupportComponent } from './application-about/components/support/support.component';
import { ThemeSelectorComponent } from './components/theme-selector/theme-selector.component';
import { NoValuePipe } from './pipes/no-value.pipe';
import { LibricielCommonModule } from '../ls-common/libriciel-common.module';

@NgModule({
  declarations: [
    HeaderComponent,
    PageComponent,
    SidebarComponent,
    ClassificationSelectorComponent,
    ActCodeInputComponent,
    RgpdPageComponent,
    ApplicationAboutComponent,
    ReleaseNotesComponent,
    LicenceComponent,
    SupportComponent,
    ThemeSelectorComponent,
    NoValuePipe,
  ],
  exports: [
    HeaderComponent,
    PageComponent,
    SidebarComponent,
    ClassificationSelectorComponent,
    ApplicationAboutComponent,
    ActCodeInputComponent,
    ThemeSelectorComponent,
    NoValuePipe,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgbModule,
    LsComposantsModule,
    NgxTrimDirectiveModule,
    LibricielCommonModule
  ]
})
export class WaCommonModule {
}
