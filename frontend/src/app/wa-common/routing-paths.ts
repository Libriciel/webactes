/* eslint-disable @typescript-eslint/member-ordering */

export class RoutingPaths {
  static PROJECTS_PATH = 'projets';
  // noinspection SpellCheckingInspection
  static PROJECTS_CREATE_PATH = `${RoutingPaths.PROJECTS_PATH}/creer-projet`;
  static PROJECTS_DRAFTS_PATH = `${RoutingPaths.PROJECTS_PATH}/brouillons`;
  static PROJECTS_WITHOUT_SITTING_PATH = `${RoutingPaths.PROJECTS_PATH}/sans-seance`;
  static PROJECTS_VALIDATING_PATH = `${RoutingPaths.PROJECTS_PATH}/en-cours`;
  static PROJECTS_TO_BE_VALIDATED_PATH = `${RoutingPaths.PROJECTS_PATH}/a-valider`;
  static PROJECTS_VALIDATED_PATH = `${RoutingPaths.PROJECTS_PATH}/valides`;
  static PROJECTS_EDITION_FRAGMENT = '/edition';
  static PROJECTS_SHOW_PATH = `${RoutingPaths.PROJECTS_PATH}/:projectId`;
  static PROJECTS_EDITION_PATH = RoutingPaths.PROJECTS_SHOW_PATH + RoutingPaths.PROJECTS_EDITION_FRAGMENT;
  static SITTINGS_PATH = 'seances';
  static SITTING_RUNNING_PATH = `${RoutingPaths.SITTINGS_PATH}/en-cours`;
  static SITTING_CLOSED_PATH = `${RoutingPaths.SITTINGS_PATH}/closes`;
  // noinspection SpellCheckingInspection
  static TELETRANSMISSION_PATH = 'teletransmission';
  // noinspection SpellCheckingInspection
  static TELETRANSMISSION_TO_BE_SEND_TO_TDT_PATH = `${RoutingPaths.TELETRANSMISSION_PATH}/a-deposer-sur-le-tdt`;
  static TELETRANSMISSION_TO_BE_SEND_TO_PREFECTURE_PATH = `${RoutingPaths.TELETRANSMISSION_PATH}/a-envoyer-en-prefecture`;
  // noinspection SpellCheckingInspection
  static TDT_AUTHENTICATION_RESPONSE_PATH = `${RoutingPaths.TELETRANSMISSION_PATH}/reponse-tdt`;
  static ACTS_PATH = 'actes';
  static ACTS_PATH_LIST_PATH = `${RoutingPaths.ACTS_PATH}/listes-des-actes`;
  static ADMIN_PATH = 'administration';
  // Manage the worflows
  static ADMIN_CIRCUIT_PATH = `${RoutingPaths.ADMIN_PATH}/circuits`;
  static ADMIN_CIRCUIT_CREATION_PATH = `${RoutingPaths.ADMIN_CIRCUIT_PATH}/creation-circuit`;
  // Manage the groups of actors
  static ADMIN_ACTOR_GROUP_PATH = `${RoutingPaths.ADMIN_PATH}/acteurs-groupes`;
  static ADMIN_ACTOR_GROUP_CREATION_PATH = `${RoutingPaths.ADMIN_PATH}/creation-acteurs-groupes`;
  // Manage the types acts
  static ADMIN_TYPES_ACT_LIST_PATH = `${RoutingPaths.ADMIN_PATH}/types-actes-list`;
  // Manage the roles
  static ADMIN_ROLE_LIST_PATH = `${RoutingPaths.ADMIN_PATH}/roles-list`;
  static ADMIN_ROLE_DETAIL_PATH = 'show-role/:id';
  // Manage the users
  static ADMIN_USER_PATH = `${RoutingPaths.ADMIN_PATH}/utilisateurs`;
  static ADMIN_ALL_USERS_PATH = `${RoutingPaths.ADMIN_PATH}/utilisateurs-organisation`;

  static ADMIN_CONNEXION_PASTELL_PATH = `${RoutingPaths.ADMIN_PATH}/connexion-pastell`;
  static ADMIN_CONNEXION_IDELIBRE_PATH = `${RoutingPaths.ADMIN_PATH}/connexion-idelibre`;
  static LOGIN = 'login';
  static ADMIN_ORGANIZATION_PATH = `${RoutingPaths.ADMIN_PATH}/organisation`;
  // Manage themes
  static ADMIN_THEME_PATH = `${RoutingPaths.ADMIN_PATH}/themes`;
  static ADMIN_THEME_DETAIL_PATH = `${RoutingPaths.ADMIN_PATH}/themes/:id`;
  static ADMIN_THEME_CREATION_PATH = `${RoutingPaths.ADMIN_THEME_PATH}/creation-theme`;
  static ADMIN_THEME_EDIT_PATH = `${RoutingPaths.ADMIN_THEME_PATH}/edit-theme`;
  // Manage sequences
  static ADMIN_SEQUENCE_PATH = `${RoutingPaths.ADMIN_PATH}/sequences`;
  static ADMIN_SEQUENCE_DETAIL_PATH = `${RoutingPaths.ADMIN_PATH}/sequences/:id`;
  // Manage organizations
  static ADMIN_ORGANIZATIONS_PATH = `${RoutingPaths.ADMIN_PATH}/organisations`;

  // noinspection SpellCheckingInspection
  static RGPD_PAGE_PATH = 'politique-de-confidentialite';
  static PREFERENCES_PATH = 'preferences';
  static PREFERENCES_NOTIFICATIONS_OPTIONS_PATH = `${RoutingPaths.PREFERENCES_PATH}/notifications`;
  static ABOUT_PATH = 'a-propos';
  static ADMIN_TYPE_SEANCE_PATH = `${RoutingPaths.ADMIN_PATH}/type-de-seance`;
  static ADMIN_COUNTER_PATH = `${RoutingPaths.ADMIN_PATH}/compteurs`;
  static SEARCH_PATH = `recherche`;
  static SITTING_SHOW_PATH = `${RoutingPaths.SITTINGS_PATH}/:sittingId`;
  static SITTING_SCHEDULE_SHOW_PATH = `${RoutingPaths.SITTINGS_PATH}/:sittingId/schedule`;

  // Vote
  static PROJECTS_VOTING_FRAGMENT = 'vote';
  static PROJECTS_VOTERS_LIST_FRAGMENT = 'voters';

  static projectPath(id: number) {
    return `${this.PROJECTS_PATH}/${id}`;
  }

  static sittingPath(id: number) {
    return `${RoutingPaths.SITTINGS_PATH}/${id}`;
  }

  static projectEditionPath(id: number) {
    return this.projectPath(id) + RoutingPaths.PROJECTS_EDITION_FRAGMENT;
  }

  static buildAdminCircuitModificationPath(id: string): string {
    return `${RoutingPaths.ADMIN_CIRCUIT_PATH}/${id}/edition`;
  }

  static buildAdminCircuitVisualisationPath(id: string): string {
    return `${RoutingPaths.ADMIN_CIRCUIT_PATH}/${id}/visualisation`;
  }

  static projectVotingPath(projectId: string, sittingId: string) {
    return `${RoutingPaths.PROJECTS_PATH}/${projectId}/${RoutingPaths.SITTINGS_PATH}/${sittingId}/${RoutingPaths.PROJECTS_VOTING_FRAGMENT}`;
  }

  static sendSittingPath(sittingId: string) {
    return `${RoutingPaths.SITTINGS_PATH}/${sittingId}/envoie`;
  }

  static scheduleSittingPath(sittingId: string) {
    return `${RoutingPaths.SITTINGS_PATH}/${sittingId}/schedule`;
  }
}
