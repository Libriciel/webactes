import { UserRightsUtils } from '../../utility/user-rights-utils';

export class CommonRights {
  static hasAdminMenuRight() {
    return UserRightsUtils.isAdmin();
  }

  static hasSuperAdminMenuRight() {
    return UserRightsUtils.isSuperAdmin();
  }
}
