export enum UserRole {

  // noinspection JSUnusedGlobalSymbols
  SUPER_ADMIN = 'Super Administrateur',
  ADMIN = 'Administrateur',
  ADMIN_FONCTIONNEL = 'Administrateur Fonctionnel',
  SECRETARIAT_GENERAL = 'Secrétariat général',
  REDACTEUR_VALIDEUR = 'Rédacteur/Valideur'

}
