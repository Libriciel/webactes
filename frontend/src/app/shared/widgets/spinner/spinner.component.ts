import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SpinnerService } from '../../../core/services/spinner.service';

@Component({
  selector: 'wa-spinner',
  templateUrl: 'spinner.component.html',
  styleUrls: ['spinner.component.scss'],
})
export class SpinnerComponent implements OnInit, OnDestroy {

  private changeTextEventsSubscription: Subscription;
  globalSpinnerText;

  constructor(public spinnerService: SpinnerService) {
  }

  ngOnInit() {
    this.changeTextEventsSubscription = this.spinnerService.globalSpinnerTextObservable
      .subscribe((spinnerText) => this.globalSpinnerText = spinnerText);
  }

  ngOnDestroy() {
    this.changeTextEventsSubscription.unsubscribe();
  }

}
