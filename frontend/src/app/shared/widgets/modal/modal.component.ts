import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import {
  CommonIcons, CommonMessages
} from '../../../ls-common';

import { Style } from '../../../wa-common';

@Component({
    selector: 'wa-modal',
    templateUrl: 'modal.component.html',
    styleUrls: ['modal.component.scss'],
})
export class ModalComponent implements OnInit {

    commonMessages = CommonMessages;
    commonIcons = CommonIcons;
    style = Style;

    @Input() title = `Information`;

    constructor(
      public activeModal: NgbActiveModal
    ) {}

    ngOnInit() {
    }

    close() {
      this.activeModal.dismiss({isError: false});
    }
}
