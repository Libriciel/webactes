import { HttpParams } from '@angular/common/http';
import { AfterViewChecked, Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { OfficeEditorFile } from '../../../core';
import { KeycloakService } from 'keycloak-angular';
import { CommonIcons, CommonMessages } from '../../../ls-common';
import { Style } from '../../../wa-common';

@Component({
  selector: 'app-office-editor',
  templateUrl: './office-editor.component.html',
  styleUrls: ['./office-editor.component.scss']
})
export class OfficeEditorComponent implements OnInit, AfterViewChecked {

  commonMessages = CommonMessages;
  commonIcons = CommonIcons;
  style = Style;

  loading: boolean;

  iframeHeight: number;
  iframeWidth: number;
  fileEditing: boolean;

  actionUrl: string;
  accessToken: string;
  //accessTokenTTL: string;

  @Input() officeEditorFile: OfficeEditorFile;
  @Output() CloseEditor = new EventEmitter<string>();
  @Output() OpenEditor = new EventEmitter<string>();

  @ViewChild('modalOffice') modalOffice: ElementRef;
  @ViewChild('officeFrame') officeFrame: ElementRef;
  @ViewChild('officeForm') officeForm: ElementRef;

  @HostListener('window:resize', [])
  public onResize() {
    this.detectScreenSize();
  }

  constructor(
    protected readonly keycloak: KeycloakService,
  ) {
    this.keycloak.getToken().then(
      data => this.accessToken = data,
      error => console.error(error)
    );
  }

  ngOnInit() {
    setTimeout(() => {

      this.actionUrl = null;
      this.fileEditing = false;
      this.loading = false;

      this.detectScreenSize();
    });
  }

  private detectScreenSize() {
    this.iframeHeight = window.innerHeight - 50;
    this.iframeWidth = window.innerWidth - 200;
  }

  loadDocument(officeEditorFile: OfficeEditorFile) {
    this.fileEditing = true;
    this.actionUrl = this.getActionUrlForDocument(officeEditorFile);
    setTimeout(() => {
      this.officeForm.nativeElement.submit();
    });

  }

  getActionUrlForDocument(officeEditorFile: OfficeEditorFile) {
    return this.getOdtEditorActionUrlForWOPISrc(officeEditorFile);
  }

  getOdtEditorActionUrlForWOPISrc(officeEditorFile: OfficeEditorFile) {
    const baseUrl = officeEditorFile.baseUrl;
    let WOPISrc = officeEditorFile.pathWopi;

    const httpParams = new HttpParams({
      fromObject: {
        WOPISrc,
        lang: 'fr-FR'
      }
    });
    console.log(`${baseUrl}?${httpParams.toString()}`);
    return `${baseUrl}?${httpParams.toString()}`;
  }

  closeEditor() {
    this.fileEditing = false;
    const message = {
      'MessageId': 'Action_Close',
      'Values': null
    };
    this.officeFrame.nativeElement.contentWindow.postMessage(JSON.stringify(message), '*');
    this.CloseEditor.emit();
  }

  openEditor() {
    this.fileEditing = true;
    this.OpenEditor.emit();
  }

  ngAfterViewChecked(): void {


  }
}
