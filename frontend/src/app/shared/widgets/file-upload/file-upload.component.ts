import { Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CommonIcons, CommonMessages } from '../../../ls-common';
import { Style } from '../../../wa-common';
import { FileUtils } from '../../../utility/Files/file-utils';
import { OfficeEditorComponent } from '../office-editor';
import { OfficeEditorFile } from '../../../core';

@Component({
  selector: 'wa-file-upload',
  templateUrl: 'file-upload.component.html',
  styleUrls: ['file-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: FileUploadComponent,
      multi: true
    }
  ]
})
export class FileUploadComponent implements ControlValueAccessor {

  readonly commonMessages = CommonMessages;
  readonly commonIcons = CommonIcons;
  readonly style = Style;

  onChange: Function;
  file: File | null = null;

  @Input() fileUploadLabel = `Mon label`;
  @Input() fileUploadRequired = false;
  @Input() fileUploadEditable = false;
  @Input() fileUploadDeletable = true;
  @Input() fileUploadDownload = true;
  @Input() fileUploadReplaceable = true;
  @Input() officeEditorFile: OfficeEditorFile | null = null;
  @Input() fileUploadEditableTitle = 'Modifier le fichier';
  @Input() fileUploadTooltipMessage = 'Ajouter un fichier';
  @Input() fileUploadBtnText = 'Ajouter un fichier';
  @Input() fileUploadBtnTitle = 'Ajouter un fichier';
  @Input() fileUploadAcceptType = [];
  @Input() fileUploadBtnDeleteText = 'Supprimer le fichier';
  @Input() fileUploadBtnReplaceText = 'Remplacer le fichier';

  @Output() fileDownload = new EventEmitter<Object | File>();
  @Output() deleteFile = new EventEmitter<void>();

  @ViewChild('officeEditor') officeEditor: OfficeEditorComponent;

  @HostListener('change', ['$event.target.files']) emitFiles(event: FileList) {
    const file = event && event.item(0);
    this.onChange(file);
    this.file = file;
  }

  constructor(private host: ElementRef<HTMLInputElement>) {
  }

  writeValue(value: null) {
    this.host.nativeElement.value = '';
    this.file = value;
  }

  getFileExtensions() {
    return FileUtils.getFileExtensions(this.fileUploadAcceptType);
  }

  registerOnChange(fn: Function) {
    this.onChange = fn;
  }

  registerOnTouched(fn: Function) {
  }

  deleteGenerateTemplateFile() {
    this.writeValue(null);
    this.deleteFile.emit();
  }

  editGenerateTemplateFile(officeEditorFile: OfficeEditorFile) {
    this.officeEditor.loadDocument(officeEditorFile);
  }

  downloadDocument(fileInfo: Object) {
    if (fileInfo) {
      this.fileDownload.emit(fileInfo);
    } else {
      this.fileDownload.emit(this.file);
    }
  }

}
