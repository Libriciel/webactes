import { Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ValidationService } from '../../core';

@Component({
  selector: 'wa-control-messages',
  template: `
    <div *ngIf="errorMessage !== null" class="ls-error-message text-danger">{{errorMessage}}</div>`,
})
export class ControlMessagesComponent {
  @Input() control: AbstractControl;

  constructor() {
  }

  get errorMessage() {
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return ValidationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
      }
    }
    return null;
  }
}
