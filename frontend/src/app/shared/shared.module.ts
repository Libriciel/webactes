import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { LibricielCommonModule } from '../ls-common/libriciel-common.module';
import { FileUploadComponent, ModalComponent, OfficeEditorComponent } from './widgets';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ControlMessagesComponent } from './control-messages/control-messages.component';
import { NgSelectModule } from '@ng-select/ng-select';

import { SpinnerComponent } from './widgets/spinner';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    LibricielCommonModule,
    NgSelectModule,
    NgxSpinnerModule
  ],
  declarations: [
    ModalComponent,
    FileUploadComponent,
    OfficeEditorComponent,
    ControlMessagesComponent,
    SpinnerComponent,
  ],
  exports: [
    ControlMessagesComponent,
    ModalComponent,
    FileUploadComponent,
    OfficeEditorComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    LibricielCommonModule,
    NgSelectModule,
    SpinnerComponent
  ],
  bootstrap: [ModalComponent, FileUploadComponent, OfficeEditorComponent, SpinnerComponent],
  entryComponents: [ModalComponent, FileUploadComponent, OfficeEditorComponent, SpinnerComponent]
})
export class SharedModule {
}
