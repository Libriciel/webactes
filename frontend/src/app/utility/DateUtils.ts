import { ExplodedDate } from './ExplodedDate';
import { Sitting } from '../model/Sitting/sitting';
import * as moment from 'moment';

export class DateUtils {
  /**
   * explode a datetime
   */
  static explode(date: Date): ExplodedDate {
    const dateTime = new Date(date);
    return {
      year: dateTime.getFullYear(),
      month: dateTime.getMonth() + 1,
      day: dateTime.getDate(),
      hour: dateTime.getHours(),
      minute: dateTime.getMinutes()
    };
  }

  static formattedDate(explodedDate): string {
    return `${this.addZero(explodedDate.day)}/${this.addZero(explodedDate.month)}/${explodedDate.year}`;
  }

  static formattedHour(explodedDate): string {
    return `${this.addZero(explodedDate.hour)}h${this.addZero(explodedDate.minute)}`;
  }

  static formattedDatetime(explodedDate): string {
    return `${this.formattedDate(explodedDate)} à ${this.formattedHour(explodedDate)}`;
  }

  // add 0 if one digit
  static addZero(num) {
    return `0${num}`.slice(-2);
  }

  static sortFarFirst = (s1: Sitting, s2: Sitting): number => {
    const d1 = new Date(s1.date);
    const d2 = new Date(s2.date);
    if (d1.getTime() === d2.getTime()) {
      return s1.id < s2.id ? -1 : 1;
    }
    if (d1 < d2) {
      return 1;
    }
    if (d1 > d2) {
      return -1;
    }
  };

  static toJSONPreservingUTC = function () {
    // @ts-ignore
    return moment(this).utc(true).format();
  };
}
