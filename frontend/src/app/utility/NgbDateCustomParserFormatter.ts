import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';
import { isNumber, padNumber, toInteger } from './NgBoostrapUtils';

@Injectable({
  providedIn: 'root'
})
export class NgbDateFrenchParserFormatter extends NgbDateParserFormatter {
  parse(value: string): NgbDateStruct {
    if (value) {
      const dateParts = value.trim().split('/');
      if (dateParts.length === 3) {
        if (toInteger(dateParts[2]) < 100) {
          dateParts[2] = String(toInteger(dateParts[2]) + 2000);
        }
        return {day: toInteger(dateParts[0]), month: toInteger(dateParts[1]), year: toInteger(dateParts[2])};
      }
      return null;
    }
  }

  format(date: NgbDateStruct): string {
    return date && isNumber(date.day) && isNumber(date.month) && isNumber(date.year) ?
      `${padNumber(date.day)}/${padNumber(date.month)}/${date.year}` :
      '';
  }
}
