import { FileType } from './file-types.enum';
import { WADocument } from '../../projects/model/wa-document';

export class FileUtils {

  static fileHasAcceptableType(file: File | WADocument, fileTypes: FileType[]): boolean {
    const mimeType = file instanceof WADocument ? file.mimeType : file.type;
    return fileTypes.find(fileType => mimeType.endsWith(fileType.toString())) !== undefined;
  }

  static filesHaveAcceptableTypes(files: FileList, fileTypes: FileType[]): boolean {
    for (let i = 0; i < files.length; i++) {
      if (!this.fileHasAcceptableType(files.item(i), fileTypes)) {
        return false;
      }
    }
    return true;
  }

  static getFileExtensions(fileTypes: FileType[]): string {
    return fileTypes.map(fileType => FileUtils.getFileExtension(fileType)).join(',');
  }

  static getHumanlyReadableSizeInMo(size: number, decimals = 2): string {
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const i = Math.floor(Math.log(size) / Math.log(k));
    const sizes = ['o', 'Ko', 'Mo', 'Go', 'To', 'Po'];
    return `${parseFloat((size / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
  }

  static getFileExtension(fileType: FileType): string {
    switch (fileType) {
      case FileType.PDF:
        return '.pdf';
      case FileType.XML:
        return '.xml';
      case FileType.PNG:
        return '.png';
      case FileType.JPG:
        return '.jpg';
      case FileType.ODT:
        return '.odt';
      case FileType.ODS:
        return '.ods';
      case FileType.ODP:
        return '.odp';
      case FileType.DOC:
        return '.doc';
      case FileType.XLS:
        return '.xls';
      case FileType.PPT:
        return '.ppt';
      case FileType.DOCX:
        return '.docx';
      case FileType.XLSX:
        return '.xlsx';
      case FileType.PPTX:
        return '.pptx';
    }
  }
}
