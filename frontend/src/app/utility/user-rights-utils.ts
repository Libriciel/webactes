import { STRUCTURE_USER_INFORMATION } from '../wa-common/variables/structure-user-info';
import { UserRole } from '../wa-common/rights/user-role.enum';

export class UserRightsUtils {

  static isAdmin() {
    return STRUCTURE_USER_INFORMATION.userInformation.role.name === UserRole.ADMIN;
  }

  static isSuperAdmin() {
    return STRUCTURE_USER_INFORMATION.userInformation.role.name === UserRole.SUPER_ADMIN;
  }
}
