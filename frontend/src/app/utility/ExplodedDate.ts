export interface ExplodedDate {
  year: number;
  month: number;
  day: number;
  hour: number;
  minute: number;
}
