import { HasDate } from '../ls-common/model/has-date';
import { HasId } from '../ls-common';

export class SortUtils {

  static sortRecentFirst = (s1: HasDate & HasId, s2: HasDate & HasId): number => {
    const d1 = new Date(s1.date);
    const d2 = new Date(s2.date);
    return d1.getTime() === d2.getTime()
      ? s1.id > s2.id ? -1 : 1
      : d1 > d2 ? 1 : -1;
  };
}
