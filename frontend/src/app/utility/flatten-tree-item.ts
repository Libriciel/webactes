import { HasId } from '../ls-common/model/has-id';
import { HasName } from '../ls-common/model/has-name';

export class FlattenTreeItem implements HasId, HasName {
  name: string;
  id: number;
  depth: number;
  parent_id: number;
  parent_name?: number;
}
