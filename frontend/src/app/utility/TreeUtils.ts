import { FlattenTreeItem } from './flatten-tree-item';

export class TreeUtils {
  static flatten(tree, depth: number, newArray: FlattenTreeItem[]): FlattenTreeItem[] {
    for (const t of tree) {
      const f: FlattenTreeItem = {
        name: t.name,
        id: t.id,
        depth: depth,
        parent_id: t.parent_id,
        parent_name: t.parent_name
      };
      newArray.push(f);
      if (t.children && t.children.length) {
        TreeUtils.flatten(t.children, depth + 1, newArray);
      }
    }
    return newArray;
  }

  static themeTreeToList(entities: any[]): any[] {
    return TreeUtils.themeTreeToList_I(entities, []);
  }

  static themeTreeToList_I(entities: any[], currentList): any[] {
    entities.map(entity => {
      currentList.push(entity);
      if (entity.children.length > 0) {
        TreeUtils.themeTreeToList_I(entity.children, currentList);
      }
    });
    return currentList.sort((a, b) => a.position.localeCompare(b.position));
  }
}
