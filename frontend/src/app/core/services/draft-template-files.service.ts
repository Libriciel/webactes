import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ApiService } from './api.service';
import { DownloadService } from './download.service';
import { UploadService } from './upload.service';
import { DraftTemplateFile } from '../models';
import { map } from 'rxjs/operators';

@Injectable()
export class DraftTemplateFilesService {
  constructor(
    protected apiService: ApiService,
    protected downloadService: DownloadService,
    protected uploadService: UploadService,
  ) {
  }

  get(id): Observable<DraftTemplateFile> {
    return this.apiService.get('/files/' + id)
      .pipe(map(data => data.draftTemplateFile));
  }

  download(id): Observable<DraftTemplateFile> {
    return this.apiService.get('/files/' + id)
      .pipe(map(data => data.draftTemplateFile));
  }

  destroy(id): Observable<any> {
    return this.apiService.delete('/files/' + id);
  }

  save(draftTemplateFile): Observable<DraftTemplateFile> {
    // If we're updating an existing file
    if (draftTemplateFile.id) {
      return this.apiService.put('/files/' + draftTemplateFile.id, {draftTemplateFile: draftTemplateFile}['DraftTemplateFile'])
        .pipe(map(data => data.draftTemplateFile));

      // Otherwise, create a new file
    } else {
      return this.apiService.post('/files', {draftTemplateFile: draftTemplateFile}['DraftTemplateFile'])
        .pipe(map(data => data.draftTemplateFile));
    }
  }

  sendFile(draftTemplateId, file): Observable<DraftTemplateFile> {
    const formData = new FormData();
    formData.append('draft_template_file', file);
    formData.append('draft_template_id', draftTemplateId);
    file.inProgress = true;
    return this.uploadService.sendFormData('/files', formData)
      .pipe(map(data => data.draftTemplateFile));
  }

  geFile(draftTemplateId): Observable<any> {
    return this.downloadService.download('/files/view/' + draftTemplateId);
  }
}
