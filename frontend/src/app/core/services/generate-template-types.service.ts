import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ApiService} from './api.service';
import { GenerateTemplateType, GenerateTemplateTypeListConfig, Pagination } from '../models';
import { map } from 'rxjs/operators';


@Injectable()
export class GenerateTemplateTypesService {
  constructor(
    private apiService: ApiService
  ) {
  }

  config: GenerateTemplateTypeListConfig;

  query(config: GenerateTemplateTypeListConfig): Observable<{ GenerateTemplateTypes: GenerateTemplateType[], pagination: Pagination }> {
    // Convert any filters over to Angular's URLSearchParams
    const params = {};
    this.config = config;

    Object.keys(config.filters)
      .forEach((key) => {
        if (key == 'searchedText') {
          params['name'] = config.filters[key];
        } else {
          params[key] = config.filters[key];
        }
      });

    return this.apiService
      .get(
        '/generate-template-types' + ((config.type === 'feed') ? '/feed' : ''),
        new HttpParams({fromObject: params})
      );
  }

  get(id): Observable<GenerateTemplateType> {
    return this.apiService.get('/generate-template-types/' + id)
      .pipe(map(data => data.GenerateTemplateType));
  }

  getAll(): Observable<{ generateTemplateTypes: GenerateTemplateType[] }> {
    return this.apiService.get('/generate-template-types/');
  }

  destroy(id) {
    return this.apiService.delete('/generate-template-types/' + id);
  }

  save(GenerateTemplateType): Observable<GenerateTemplateType> {
    // If we're updating an existing article
    if (GenerateTemplateType.id) {
      return this.apiService.put('/generate-template-types/' + GenerateTemplateType.id, {GenerateTemplateType: GenerateTemplateType}['GenerateTemplateType'])
        .pipe(map(data => data.Actor));

      // Otherwise, create a new article
    } else {
      return this.apiService.post('/generate-template-types', {GenerateTemplateType: GenerateTemplateType}['GenerateTemplateType'])
        .pipe(map(data => data.Actor));
    }
  }
}
