import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { UploadService } from './upload.service';
import { ApiService } from './api.service';
import { GenerateTemplateFile } from '../models';
import { map } from 'rxjs/operators';
import { DownloadService } from './download.service';


@Injectable()
export class GenerateTemplateFilesService {
  constructor(
    protected apiService: ApiService,
    protected downloadService: DownloadService,
    protected uploadService: UploadService
  ) {
  }

  get(id): Observable<GenerateTemplateFile> {
    return this.apiService.get('/files/' + id)
      .pipe(map(data => data.generateTemplateFile));
  }

  destroy(id): Observable<any> {
    return this.apiService.delete('/files/' + id);
  }

  save(generateTemplateFile): Observable<GenerateTemplateFile> {
    // If we're updating an existing file
    if (generateTemplateFile.id) {
      return this.apiService.put('/files/' + generateTemplateFile.id, {generateTemplateFile: generateTemplateFile}['GenerateTemplateFile'])
        .pipe(map(data => data.generateTemplateFile));

      // Otherwise, create a new file
    } else {
      return this.apiService.post('/files', {generateTemplateFile: generateTemplateFile}['GenerateTemplateFile'])
        .pipe(map(data => data.generateTemplateFile));
    }
  }

  sendFile(generateTemplateId, file): Observable<GenerateTemplateFile> {
    const formData = new FormData();
    formData.append('generate_template_file', file);
    formData.append('generate_template_id', generateTemplateId);
    file.inProgress = true;
    return this.uploadService.sendFormData('/files', formData)
      .pipe(map(data => data.generateTemplateFile));
  }

  geFile(draftTemplateId): Observable<any> {
    return this.downloadService.download('/files/view/' + draftTemplateId);
  }

}
