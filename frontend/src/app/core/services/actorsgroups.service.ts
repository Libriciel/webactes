import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ApiService } from './api.service';
import { Actorgroup, ActorgroupListConfig, Pagination } from '../models';
import { map } from 'rxjs/operators';


@Injectable()
export class ActorsgroupsService {
  constructor (
    private apiService: ApiService
  ) {}

  config: ActorgroupListConfig;

  query(config: ActorgroupListConfig): Observable<{ actorGroups: Actorgroup[], pagination: Pagination }> {
    // Convert any filters over to Angular's URLSearchParams
    const params = {};
    this.config = config;

    Object.keys(config.filters)
      .forEach((key) => {
        if (key == 'searchedText') {
          params['name'] = config.filters[key];
        } else {
          params[key] = config.filters[key];
      }
      });

    return this.apiService
      .get(
        '/actor-groups' + ((config.type === 'feed') ? '/feed' : ''),
        new HttpParams({fromObject: params})
      );
  }

  get(id): Observable<Actorgroup> {
    return this.apiService.get('/actor-groups/' + id)
      .pipe(map(data => data.actorgroup));
  }

  getAllWithPagination(numPage?: number, limit?: number): Observable<{ actorGroups: Actorgroup[], pagination: Pagination }> {
    return this.apiService.get('/actor-groups/' + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (limit ? `&limit=${limit}` : `&limit=10`));
  }

  getAllWithoutPagination(): Observable<{ actorGroups: Actorgroup[] }> {
    return this.apiService.get('/actor-groups/' + '?paginate=false');
  }

  // AllDestroy(actorsgroups: Actorgroup[]): Observable<any> {
  //   return this.apiService.delete('/actorsgroups/ids=' + actorsgroups.map(actorgroup => actorgroup.id).join(','));
  // }

  destroy(id) {
    return this.apiService.delete('/actor-groups/' + id);
  }

  save(actorgroup): Observable<Actorgroup> {
    // If we're updating an existing article
    if (actorgroup.id) {
      return this.apiService.put('/actor-groups/' + actorgroup.id, {actorgroup: actorgroup}['actorgroup'])
        .pipe(map(data => data.actorgroup));

    // Otherwise, create a new article
    } else {
      return this.apiService.post('/actor-groups', {actorgroup: actorgroup}['actorgroup'])
        .pipe(map(data => data.actorgroup));
    }
  }

  activate(id): Observable<Actorgroup> {
    return this.apiService.post('/actor-groups/' + id + '/activate');
  }

  deactivate(id): Observable<Actorgroup> {
    return this.apiService.post('/actor-groups/' + id + '/deactivate');
  }

}
