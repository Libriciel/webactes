import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CommonMessages, NotificationService } from '../../ls-common';
import { STRUCTURE_USER_INFORMATION } from '../../wa-common/variables/structure-user-info';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  get globalSpinner(): string {
    return this._globalSpinner;
  }

  get globalSpinnerTextObservable(): BehaviorSubject<string> {
    return this._globalSpinnerTextObservable;
  }

  constructor(
    protected ngxSpinnerService: NgxSpinnerService,
    protected notificationService: NotificationService
  ) {
  }

  private _globalSpinner = 'globalSpinner';
  globalSpinnerType = STRUCTURE_USER_INFORMATION.structureInformation.spinner;

  private _globalSpinnerTextObservable: BehaviorSubject<string> = new BehaviorSubject(CommonMessages.GLOBAL_SPINNER_TEXT);

  changeGlobalSpinnerText(newText: string) {
    this._globalSpinnerTextObservable.next(newText);
  }

  load(spinnerText?: string) {
    if (spinnerText) {
      this.changeGlobalSpinnerText(spinnerText);
    } else {
      this.changeGlobalSpinnerText(CommonMessages.GLOBAL_SPINNER_TEXT);
    }
    this.ngxSpinnerService.show(this._globalSpinner,
      {
        size: 'medium',
        bdColor: 'rgba(0, 0, 0, 0.8)',
        color: '#0C9142',
        fullScreen: true
      }
    ).then().catch(error => console.error(error));
  }

  error(message: string, title?: string, technicalMessage?: any) {
    this.ngxSpinnerService.hide(this._globalSpinner).catch(error => console.error(error));
    this.notificationService.showError(message, title, technicalMessage);
  }

  close(message?: string) {
    this.ngxSpinnerService.hide(this._globalSpinner).catch(error => console.error(error));
    if (message) {
      this.notificationService.showSuccess(message);
    }
  }
}
