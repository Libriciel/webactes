import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable ,  throwError } from 'rxjs';

import { JwtService } from './jwt.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class DownloadService {

  headers = new HttpHeaders({'Accept': 'application/json'});

  constructor(
    private http: HttpClient,
    private jwtService: JwtService
  ) {}

  private formatErrors(error: any) {
    return  throwError(error.error);
  }

  public download(path, headers:  HttpHeaders = this.headers): Observable<Blob> {
    return this.http.get(
      `${environment.apiUrl}${path}`,
      { responseType: 'blob' }).pipe(catchError(this.formatErrors));
  }
}
