import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ApiService} from './api.service';
import { DraftTemplateType, DraftTemplateTypeListConfig, Pagination } from '../models';
import { map } from 'rxjs/operators';


@Injectable()
export class DraftTemplateTypesService {
  constructor(
    private apiService: ApiService
  ) {
  }

  config: DraftTemplateTypeListConfig;

  query(config: DraftTemplateTypeListConfig): Observable<{ DraftTemplateTypes: DraftTemplateType[], pagination: Pagination }> {
    // Convert any filters over to Angular's URLSearchParams
    const params = {};
    this.config = config;

    Object.keys(config.filters)
      .forEach((key) => {
        if (key == 'searchedText') {
          params['name'] = config.filters[key];
        } else {
          params[key] = config.filters[key];
        }
      });

    return this.apiService
      .get(
        '/draft-template-types' + ((config.type === 'feed') ? '/feed' : ''),
        new HttpParams({fromObject: params})
      );
  }

  get(id): Observable<DraftTemplateType> {
    return this.apiService.get('/draft-template-types/' + id)
      .pipe(map(data => data.DraftTemplateType));
  }

  getAll(): Observable<{ draftTemplateTypes: DraftTemplateType[] }> {
    return this.apiService.get('/draft-template-types/');
  }

  destroy(id) {
    return this.apiService.delete('/draft-template-types/' + id);
  }

  save(DraftTemplateType): Observable<DraftTemplateType> {
    // If we're updating an existing article
    if (DraftTemplateType.id) {
      return this.apiService.put('/draft-template-types/' + DraftTemplateType.id, {DraftTemplateType: DraftTemplateType}['DraftTemplateType'])
        .pipe(map(data => data.Actor));

      // Otherwise, create a new article
    } else {
      return this.apiService.post('/draft-template-types', {DraftTemplateType: DraftTemplateType}['DraftTemplateType'])
        .pipe(map(data => data.Actor));
    }
  }
}
