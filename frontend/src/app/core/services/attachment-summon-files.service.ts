import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { UploadService } from './upload.service';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
import { AttachmentSummonFile } from '../models';
import { DownloadService } from './download.service';


@Injectable()
export class AttachmentSummonFilesService {
  constructor(
    protected apiService: ApiService,
    protected downloadService: DownloadService,
    protected uploadService: UploadService
  ) {
  }

  get(id): Observable<AttachmentSummonFile> {
    return this.apiService.get(`/files/${id}`)
      .pipe(map(data => data.attachmentSummonFile));
  }

  destroy(id: number): Observable<void> {
    return this.apiService.delete(`/files/${id}`);
  }

  save(attachmentSummonFile): Observable<AttachmentSummonFile> {
    // If we're updating an existing file
    if (attachmentSummonFile.id) {
      return this.apiService.put(`/files/${attachmentSummonFile.id}`, {attachmentSummonFile: attachmentSummonFile}['AttachmentSummonFile'])
        .pipe(map(data => data.attachmentSummonFile));

      // Otherwise, create a new file
    } else {
      return this.apiService.post('/files', {attachmentSummonFile: attachmentSummonFile}['AttachmentSummonFile'])
        .pipe(map(data => data.attachmentSummonFile));
    }
  }

  sendFile(attachmentSummonId: number, file: AttachmentSummonFile): Observable<AttachmentSummonFile> {
    const formData = new FormData();
    formData.append('attachment_summon_id', String(attachmentSummonId));
    formData.append('attachment_summon_file', file as File);
    return this.uploadService.sendFormData('/files', formData)
      .pipe(map(data => data.attachmentSummonFile));
  }

  getFile(draftTemplateId): Observable<any> {
    return this.downloadService.download('/files/view/' + draftTemplateId);
  }
}
