import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ApiService } from './api.service';
import { ProjectText, DraftTemplate, DraftTemplateListConfig, Pagination, Actorgroup } from '../models';
import { map, tap } from 'rxjs/operators';


@Injectable()
export class DraftTemplatesService {
  constructor(
    protected apiService: ApiService
  ) {
  }

  config: DraftTemplateListConfig;

  query(config: DraftTemplateListConfig): Observable<{ draftTemplates: DraftTemplate[], pagination: Pagination }> {
    // Convert any filters over to Angular's URLSearchParams
    const params = {};
    this.config = config;

    Object.keys(config.filters)
      .forEach((key) => {
        if (key == 'searchedText') {
          params['name'] = config.filters[key];
        } else {
          params[key] = config.filters[key];
        }
      });

    return this.apiService
      .get(
        '/draft-templates' + ((config.type === 'feed') ? '/feed' : ''),
        new HttpParams({fromObject: params})
      );
  }

  get(id): Observable<DraftTemplate> {
    return this.apiService.get('/draft-templates/' + id)
      .pipe(map(data => data.DraftTemplate));
  }

  getAll(): Observable<{ draftTemplates: DraftTemplate[] }> {
    return this.apiService.get('/draft-templates/');
  }

  getAllByTypesactId(id): Observable<{ draftTemplates: DraftTemplate[] }> {
    return this.apiService.get('/draft-templates/get-all-by-typesact-id/' + id);
  }

  getProjetTextByDraftTemplateIdAndProjectId(draftTemplateId, projectId): Observable<{ projectText: ProjectText }> {
    return this.apiService.get('/draft-templates/get-projet-text-id-by-draft-template-id-and-project-id/' + draftTemplateId + '/' + projectId);
  }

  destroy(id) {
    return this.apiService.delete('/draft-templates/' + id);
  }

  save(draftTemplate): Observable<DraftTemplate> {
    // If we're updating an existing draftTemplate
    if (draftTemplate.id) {
      return this.apiService.put(
        '/draft-templates/' + draftTemplate.id, {draftTemplate: draftTemplate}
      ).pipe(map(data => data.draftTemplates));

      // Otherwise, create a new draftTemplate
    } else {
      return this.apiService.post('/draft-templates', {draftTemplate: draftTemplate})
        .pipe(map(data => data.draftTemplate));
    }
  }
}
