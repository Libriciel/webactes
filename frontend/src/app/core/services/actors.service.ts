import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { Actor, ActorListConfig, Pagination } from '../models';
import { map } from 'rxjs/operators';


@Injectable()
export class ActorsService {
  constructor(
    private apiService: ApiService
  ) {
  }

  config: ActorListConfig;

  query(config: ActorListConfig): Observable<{ actors: Actor[], pagination: Pagination }> {
    // Convert any filters over to Angular's URLSearchParams
    const params = {};
    this.config = config;

    Object.keys(config.filters)
      .forEach((key) => {
        if (key === 'searchedText') {
          params['name'] = config.filters[key];
        } else {
          params[key] = config.filters[key];
        }
      });

    return this.apiService
      .get(
        '/actors' + ((config.type === 'feed') ? '/feed' : ''),
        new HttpParams({fromObject: params})
      );
  }

  get(id): Observable<Actor> {
    return this.apiService.get('/actors/' + id)
      .pipe(map(data => data.Actor));
  }

  destroy(id) {
    return this.apiService.delete('/actors/' + id);
  }

  save(actor: Actor): Observable<Actor> {
    return actor.id
      ? this.apiService.put('/actors/' + actor.id, {actor: actor})
        .pipe(map(data => data.actor))
      : this.apiService.post('/actors', {actor: actor})
        .pipe(map(data => data.actor));
  }

  activate(id): Observable<Actor> {
    return this.apiService.post('/actors/' + id + '/activate');
  }

  deactivate(id): Observable<Actor> {
    return this.apiService.post('/actors/' + id + '/deactivate');
  }

}
