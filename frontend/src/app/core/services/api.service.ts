import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable ,  throwError } from 'rxjs';

import { JwtService } from './jwt.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ApiService {

  headers = new HttpHeaders({'Accept': 'application/json'});

  constructor(
    private http: HttpClient,
    private jwtService: JwtService
  ) {}

  private formatErrors(error: any) {
    return  throwError(error.error);
  }

  public get(path: string, params: HttpParams = new HttpParams(), headers:  HttpHeaders = this.headers): Observable<any> {
    return this.http.get(`${environment.apiUrl}${path}`, { headers, params })
      .pipe(catchError(this.formatErrors));
  }

  public put(path: string, body: Object = {}, headers:  HttpHeaders = this.headers): Observable<any> {
    return this.http.put(
      `${environment.apiUrl}${path}`,
      JSON.stringify(body), { headers })
      .pipe(catchError(this.formatErrors));
  }

  public post(path: string, body: Object = {}, headers:  HttpHeaders = this.headers): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}${path}`,
      JSON.stringify(body), { headers })
      .pipe(catchError(this.formatErrors));
  }

  public delete(path, headers:  HttpHeaders = this.headers): Observable<any> {
    return this.http.delete(
      `${environment.apiUrl}${path}`, { headers })
      .pipe(catchError(this.formatErrors));
  }
}
