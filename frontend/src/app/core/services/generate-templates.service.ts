import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ApiService } from './api.service';
import { GenerateTemplate, GenerateTemplateListConfig, Pagination } from '../models';
import { map, tap } from 'rxjs/operators';


@Injectable()
export class GenerateTemplatesService {
  constructor(
    protected apiService: ApiService
  ) {
  }

  config: GenerateTemplateListConfig;

  query(config: GenerateTemplateListConfig): Observable<{ generateTemplates: GenerateTemplate[], pagination: Pagination }> {
    // Convert any filters over to Angular's URLSearchParams
    const params = {};
    this.config = config;

    Object.keys(config.filters)
      .forEach((key) => {
        if (key == 'searchedText') {
          params['name'] = config.filters[key];
        } else {
          params[key] = config.filters[key];
        }
      });

    return this.apiService
      .get(
        '/generate-templates' + ((config.type === 'feed') ? '/feed' : ''),
        new HttpParams({fromObject: params})
      );
  }

  get(id): Observable<GenerateTemplate> {
    return this.apiService.get('/generate-templates/' + id)
      .pipe(map(data => data.GenerateTemplate));
  }

  destroy(id) {
    return this.apiService.delete('/generate-templates/' + id);
  }

  save(generateTemplate): Observable<GenerateTemplate> {
    // If we're updating an existing generateTemplate
    if (generateTemplate.id) {
      return this.apiService.put(
        '/generate-templates/' + generateTemplate.id, {generateTemplate: generateTemplate}
      ).pipe(map(data => data.generateTemplates));

      // Otherwise, create a new generateTemplate
    } else {
      return this.apiService.post('/generate-templates', {generateTemplate: generateTemplate})
        .pipe(map(data => data.generateTemplate));
    }
  }
}
