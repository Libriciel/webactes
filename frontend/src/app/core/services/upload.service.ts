import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable ,  throwError } from 'rxjs';

import { JwtService } from './jwt.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class UploadService {

  headers = new HttpHeaders({'Accept': 'application/json'});
// {
//   reportProgress: true,
//   observe: 'events'
// }

  constructor(
    private http: HttpClient,
    private jwtService: JwtService
  ) {}

  private formatErrors(error: any) {
    return  throwError(error.error);
  }

  public sendFormData(path: string, formData: FormData, headers:  HttpHeaders = this.headers): Observable<any> {
    return this.http.post<any>(
      `${environment.apiUrl}${path}`,
      formData, { headers }).pipe(catchError(this.formatErrors));
  }

}
