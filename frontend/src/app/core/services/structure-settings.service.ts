import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { ApiService } from './api.service';
import { Actorgroup, StructureSetting } from '../models';
import { map, tap } from 'rxjs/operators';
import { STRUCTURE_USER_INFORMATION } from '../../wa-common/variables/structure-user-info';


@Injectable()
export class StructureSettingsService {
  constructor(
    protected apiService: ApiService
  ) {
  }

  get(): Observable<StructureSetting> {
    return this.apiService.get('/structure-settings')
      .pipe(map(data => data.structureSetting));
  }

  saveTypeBoolean(groupCode: string, keyCode: string, value: boolean): Observable<any> {
      return this.apiService.put(
        '/structure-settings/save-type-boolean', {
          groupCode: groupCode,
          keyCode: keyCode,
          value: value
        }
      );
  }

  getConfig($group: string, $name: string) {
    const setting = STRUCTURE_USER_INFORMATION.structureSetting.groupSettings
      .find(groupSettings => groupSettings.code === $group);
    return setting === undefined ? false : setting.settings.find(settings => settings.code === $name).active;
  }
}
