import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { HTTP_INTERCEPTORS } from '@angular/common/http';

//import { HttpTokenInterceptor } from './interceptors/http.token.interceptor';
import {
  ApiService,
  AuthGuard,
  ActorsgroupsService,
  JwtService,
  UserService,
  DownloadService,
  UploadService,
} from './services';
import { ValidationService } from './validators';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
   // {provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true},
    ApiService,
    DownloadService,
    UploadService,
    AuthGuard,
    JwtService,
    UserService,
    ValidationService,
    ActorsgroupsService,
  ],
  declarations: []
})
export class CoreModule { }
