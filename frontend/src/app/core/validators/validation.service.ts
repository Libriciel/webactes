import { CommonMessages } from '../../ls-common';

export class ValidationService {
  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    const config:
      { invalidEmailAddress: string; minlength: string; invalidPassword: string; invalidCellPhone: string; required: string } = {
      required: CommonMessages.REQUIRED_FIELD,
      invalidEmailAddress: 'Cette adresse email n\'est pas valide',
      invalidPassword:
        'Mot de passe incorrect. Le mot de passe doit comporter au moins 6 caractères et contenir un nombre.',
      minlength: `Longueur minimale ${validatorValue.requiredLength}`,
      invalidCellPhone: 'Le numéro de téléphone n\'est pas valide'
    };
    return config[validatorName];
  }

  static emailValidator(control) {
    // RFC 2822 compliant regex
    if (!control.value.match(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,})+$/)) {
      return {invalidEmailAddress: true};
    } else {
      return null;
    }
  }

  static passwordValidator(control) {
    // {6,100}           - Assert password is between 6 and 100 characters
    // (?=.*[0-9])       - Assert a string has at least one number
    if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
      return null;
    } else {
      return {invalidPassword: true};
    }
  }

  static cellPhoneValidator(control) {
    if (control.value && !control.value.match(/^0[6-7]\d{8}$/)) {
      return {invalidCellPhone: true};
    } else {
      return null;
    }
  }
}
