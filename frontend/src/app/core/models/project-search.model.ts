export interface ProjectSearch {
  project_name: string;
  code_act: string;
  theme_name: string;
  typeacts: string;
}
