import { HasId } from '../../ls-common';

export interface AttachmentSummonFile extends HasId, File {
  attachment_summon_id: number;
  path: string;
  mimetype: string;
  size: number;
}
