export interface GenerateTemplateListConfig {
  type: string;

  filters: {
    searchedText?: string,
    limit?: number,
    offset?: number,
    page?: number
  };
}
