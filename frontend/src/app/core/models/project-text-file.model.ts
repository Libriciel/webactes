import { HasId, HasName, HasWopi } from '../../ls-common';

export interface ProjectTextFile extends HasId, HasName, HasWopi {
  projet_text_id: number;
  path: string;
  mimetype: string;
  size: string;
}
