export interface GenerateTemplateTypeListConfig {
  type: string;

  filters: {
    searchedText?: string,
    limit?: number,
    offset?: number,
    page?: number
  };
}
