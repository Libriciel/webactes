import { HasId, HasName } from '../../ls-common';
import { DraftTemplateType, DraftTemplateFile } from './index';

export interface DraftTemplate extends HasId, HasName {
  draft_template_type_id: number;
  draft_template_type: DraftTemplateType
  draft_template_file: DraftTemplateFile
}
