import { HasId } from '../../ls-common';
import { ProjectTextFile } from './project-text-file.model';
import { DraftTemplate } from './draft-template.model';

export interface ProjectText extends HasId {
  project_id: number
  draft_template_id: number
  draft_template: DraftTemplate
  project_text_file: ProjectTextFile
}
