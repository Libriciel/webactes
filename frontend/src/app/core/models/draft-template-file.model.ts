import { HasId, HasName, HasWopi } from '../../ls-common';

export interface DraftTemplateFile extends HasId, HasName, HasWopi {
  draft_template_id: number;
  path: string;
  mimetype: string;
  size: string;
}
