import { HasId } from '../../ls-common/model/has-id';
import { HasName } from '../../ls-common/model/has-name';
import { Actor } from './actor.model';

export interface Actorgroup extends HasId, HasName{
  active: boolean;
  actors: Array<Actor>;
}
