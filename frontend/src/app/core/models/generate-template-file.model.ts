import {HasId, HasName, HasWopi} from '../../ls-common';

export interface GenerateTemplateFile extends HasId, HasName, HasWopi {
  generate_template_id: number;
  path: string;
  mimetype: string;
  size: string;
}
