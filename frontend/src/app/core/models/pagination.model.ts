export interface Pagination {
  page: number;
  count: number;
  perPage: number;
}
