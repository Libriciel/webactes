import { HasId, HasName } from '../../ls-common';

export interface OfficeEditorFile extends HasId, HasName {
  path: string;
  pathWopi: string;
  baseUrl: string;
  mimetype: string;
  size: string;
}
