import { HasId, HasName, HasOrder } from '../../ls-common';
import { Actorgroup } from './actorgroup.model';
import { Vote } from '../../model/vote/vote';
import { HasActions } from '../../ls-common/model/has-actions';

export interface Actor extends HasId, HasName, HasOrder {
  active: boolean;
  rank: string;
  name: string;
  full_name: string;
  civility: string;
  lastname: string;
  firstname: string;
  email: string;
  address: string;
  address_supplement: string;
  post_code: number;
  city: string;
  phone: string;
  cellphone: string;
  title: string;
  actor_groups: Array<Actorgroup>;
  votes?: Vote[];
}
