import { StructureSettingGroup } from './structure-setting-group.model';

export interface StructureSetting {
  groupSettings: [StructureSettingGroup];
}
