export interface ListActions {
  can_read: ListActionsValue;
  can_modify: ListActionsValue;
  can_remove: ListActionsValue;
}

export interface ListActionsValue {
  value: boolean;
  data: string;
}
