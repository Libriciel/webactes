export interface ActorListConfig {
  type: string;

  filters: {
    searchedText?: string,
    active?: boolean,
    limit?: number,
    offset?: number,
    page?: number
  };
}
