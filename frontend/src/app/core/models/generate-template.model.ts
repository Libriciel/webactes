import { HasId, HasName } from '../../ls-common';
import { GenerateTemplateType, GenerateTemplateFile } from './index';

export interface GenerateTemplate extends HasId, HasName {
  generate_template_type_id: number;
  generate_template_type: GenerateTemplateType
  generate_template_file: GenerateTemplateFile
}
