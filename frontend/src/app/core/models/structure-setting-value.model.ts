export interface StructureSettingValue {
  code: string;
  name: string;
  active: boolean;
}
