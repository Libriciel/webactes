import { HasId } from '../../ls-common/model/has-id';
import { HasName } from '../../ls-common/model/has-name';

export interface DraftTemplateType extends HasId, HasName{
  code: string;
}
