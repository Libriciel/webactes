import { StructureSettingValue } from './structure-setting-value.model';

export interface StructureSettingGroup {
  name: string;
  code: string;
  settings: [StructureSettingValue];
}
