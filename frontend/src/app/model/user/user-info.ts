import { HasId } from '../../ls-common/model/has-id';
import { HasName } from '../../ls-common/model/has-name';
import { Role } from '../role';
import { UserRaw } from '../../ls-common/services/http-services/info/user-raw';

export class UserInfo implements HasId, HasName {
  id: number;
  firstname: string;
  lastname: string;
  role: Role;
  name: string;

  constructor(userRaw: UserRaw) {
    this.id = userRaw.id;
    this.firstname = userRaw.firstname;
    this.lastname = userRaw.lastname;
    this.role = userRaw.role;
    this.name = `${userRaw.firstname} ${userRaw.lastname}`;
  }
}
