import { Role } from '../role';
import { HasAvailableActions } from '../../ls-common/model/has-available-actions';
import { HasId, HasName } from '../../ls-common';
import { UserAvailableActions } from './user-available-actions.enum';
import { UserAvailableActionsRaw } from '../../ls-common/services/http-services/user/user-available-actions-raw';
import { StructureInfo } from '../structure-info';

export class User extends HasAvailableActions implements HasName, HasId {
  id: number;
  active: boolean;
  civility: string;
  firstname: string;
  lastname: string;
  email: string;
  nom_complet_court: string;
  nom_complet: string;
  phone: string;
  services: any[];
  role: Role;
  roles: Role[];
  structure_id: number;
  structures: StructureInfo[];
  username: string;
  name: string;
  availableActions: UserAvailableActionsRaw;

  constructor(
    id?: number,
    active?: boolean,
    civility?: string,
    firstname?: string,
    lastname?: string,
    email?: string,
    nom_complet_court?: string,
    nom_complet?: string,
    phone?: string,
    services?: any[],
    role?: Role,
    roles?: Role[],
    structure_id?: number,
    structures?: StructureInfo[],
    username?: string,
    name?: string,
    availableActions?: UserAvailableActionsRaw
  )
  {
    super();
    this.id = id;
    this.active = active;
    this.civility = civility;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.nom_complet_court = nom_complet_court;
    this.nom_complet = nom_complet;
    this.phone = phone;
    this.services = services;
    this.role = role;
    this.roles = roles;
    this.structure_id = structure_id;
    this.structures = structures;
    this.username = username;
    this.name = `${firstname} ${lastname}`;
    this.setAvailableAction(UserAvailableActions.isDeletable, availableActions ? availableActions.can_be_deleted.value : false);
    this.setAvailableAction(UserAvailableActions.isEditable, availableActions ? availableActions.can_be_edited.value : false);
  }


  clone() : User {
    return new User(
        this.id,
        this.active,
        this.civility,
        this.firstname,
        this.lastname,
        this.email,
        this.nom_complet_court,
        this.nom_complet,
        this.phone,
        this.services,
        this.role,
        this.roles,
        this.structure_id,
        this.structures,
        this.username,
        this.name,
        this.availableActions
      );
  }
}
