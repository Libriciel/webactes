export enum UserAvailableActions {
  'isEditable',
  'isDeletable'
}
