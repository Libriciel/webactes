import { HasId } from '../../ls-common/model/has-id';
import { HasName } from '../../ls-common/model/has-name';

export class Nature implements HasId, HasName {
  id: number;
  name: string;
  typeAbrege: string;

  constructor(natureRaw?: { id: number, libelle: string, typeabrege: string }) {
    this.id = natureRaw.id;
    this.name = natureRaw.libelle;
    this.typeAbrege = natureRaw.typeabrege;
  }
}
