
export class NatureRaw {
  id: number;
  libelle: string;
  typeabrege: string;

  constructor(natureRaw?: { id: number, libelle: string, typeabrege: string}) {
    this.id = natureRaw.id;
    this.libelle = natureRaw.libelle;
    this.typeabrege = natureRaw.typeabrege;
  }
}
