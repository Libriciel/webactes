import { Actor, Actorgroup } from '../../core';
import { VoteAction } from '../vote/vote-action.enum';

export class VoterListItem {
  id: number;
  actor_id?: number;
  name: string;
  is_present?: boolean;
  // Must be initialized once all VoterListItem are created
  representative?: VoterListItem;
  president?: VoterListItem;
  mandataire_id?: number;
  president_id?: number;
  actor?: Actor;
  actorGroup?: Actorgroup;
  result?: VoteAction;

  constructor(id: number,
              actor_id?: number,
              name?: string,
              is_present?: boolean,
              result?: VoteAction,
              actor?: Actor,
              president?: VoterListItem,
              mandataire_id?: number,
              actorGroup?: Actorgroup) {
    this.id = id;
    this.actor_id = actor_id;
    this.name = name;
    this.is_present = is_present;
    this.president = president;
    this.mandataire_id = mandataire_id;
    this.actor = actor;
    this.actorGroup = actorGroup;
    this.result = result;
  }
}
