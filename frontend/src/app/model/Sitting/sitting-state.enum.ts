export enum SittingState {
  OPENED = 'ouverture_seance',
  CLOSED = 'seance_close',
  ALL = 'all',
}
