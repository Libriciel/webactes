export enum SittingAvailableActions {
  isDeletable,
  isEditable,
  can_propose_to_vote,
  canBeClosed,
}
