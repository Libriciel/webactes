import { TypeSitting } from '../type-sitting/type-sitting';
import { HasDate } from '../../ls-common/model/has-date';
import { HasId } from '../../ls-common';
import { VoterListItem } from '../voters-list/voter-list-item';
import { AttachmentSummonFile } from '../../core';
import { Summon } from '../summon/summon';
import { HasAvailableActions } from '../../ls-common/model/has-available-actions';
import { SittingAvailableActions } from './sitting-available-actions.enum';
import { SittingAvailableActionsRaw } from '../../ls-common/services/http-services/sittings/sitting-available-actions-raw';
import { SittingRaw } from '../../ls-common/services/http-services/sittings/sitting-raw';

export class Sitting extends HasAvailableActions implements HasDate, HasId {
  id: number;
  date: Date;
  date_convocation: Date;
  typesitting?: TypeSitting;
  typesitting_id?: number;
  president_id?: number;
  secretary_id?: number;
  actors_projects_sittings: VoterListItem[];
  actors_projects_sittings_previous: VoterListItem[];
  convocation?: AttachmentSummonFile;
  summons: Summon[];
  hasProjects = false;
  availableActions: SittingAvailableActionsRaw;
  generationerrors: { type: string; message: string; date: Date }[];
  place?: String;

  constructor(sittingRaw?: SittingRaw) {
    super();
    if (sittingRaw) {
      this.id = sittingRaw.id;
      this.date = new Date(sittingRaw.date);
      this.date_convocation = sittingRaw.date_convocation ? new Date(sittingRaw.date_convocation) : null;
      this.typesitting = sittingRaw.typesitting;
      this.typesitting_id = sittingRaw.typesitting_id;
      this.secretary_id = sittingRaw.secretary_id;
      this.president_id = sittingRaw.president_id;
      this.actors_projects_sittings = sittingRaw.actors_projects_sittings ? sittingRaw.actors_projects_sittings : [];
      this.actors_projects_sittings_previous =
        sittingRaw.actors_projects_sittings_previous ? sittingRaw.actors_projects_sittings_previous : [];
      this.summons = sittingRaw.summons;
      this.hasProjects = sittingRaw.has_projects;
      this.availableActions = sittingRaw.availableActions;
      this.generationerrors = sittingRaw.generationerrors;
      this.place = sittingRaw.place;
      this.setAvailableAction(
        SittingAvailableActions.isEditable,
        this.availableActions ? this.availableActions.can_be_edited.value : false
      );
      this.setAvailableAction(
        SittingAvailableActions.isDeletable,
        this.availableActions ? this.availableActions.can_be_deleted.value : false
      );
      this.setAvailableAction(
        SittingAvailableActions.can_propose_to_vote,
        this.availableActions ? this.availableActions.can_propose_to_vote.value : false
      );
      this.setAvailableAction(
        SittingAvailableActions.canBeClosed,
        this.availableActions ? this.availableActions.can_be_closed.value : false
      );
    } else {
      this.date = new Date();
    }
  }
}
