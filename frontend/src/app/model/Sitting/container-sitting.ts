import { HasId } from '../../ls-common';

export class ContainerSitting implements HasId{
  id: number;
  container: any;
  container_id: number;
  sitting_id: number;
  rank: number;
}
