import { Sitting } from './sitting';
import { DatePipe } from '@angular/common';
import { TypeActRaw } from '../../ls-common/services/http-services/type-act/type-act-raw';

export class SittingOverview {
  date: Date;
  id: number;
  type: string;
  fullName: string;
  isdeliberating: boolean;

  constructor(sitting: Sitting) {
    this.date = sitting.date;
    this.id = sitting.id;
    this.type = sitting.typesitting ? sitting.typesitting.name : '';
    this.isdeliberating = sitting.typesitting ? sitting.typesitting.isdeliberating : null;
    this.fullName = this.type + ' - ' + new DatePipe('fr').transform(sitting.date, 'dd/MM/yyyy à HH:mm');
  }
}
