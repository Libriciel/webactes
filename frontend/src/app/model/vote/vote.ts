import { HasId } from '../../ls-common';
import { ActorVote } from './actor-vote';

export class Vote implements HasId {
  id: number;
  method ?: string;
  project_id: number;
  president_id: number;
  yes_counter ?: number;
  no_counter ?: number;
  abstentions_counter ?: number;
  no_words_counter ?: number;
  take_act ?: boolean;
  result ?: boolean;
  comment ?: string;
  opinion ?: boolean;
  actors_votes?: ActorVote[];

  constructor(id?: number,
              presidentId?: number,
              projectId?: number,
              yesCounter?: number,
              noCounter?: number,
              abstentionsCounter?: number,
              noWordsCounter?: number,
              prendreActe?: boolean,
              resultat?: boolean,
              comment?: string,
              opinion?: boolean,
              method?: string,
              actorsVotes?: ActorVote[]) {
    this.id = id;
    this.president_id = presidentId;
    this.project_id = projectId;
    this.yes_counter = yesCounter ? yesCounter : 0;
    this.no_counter = noCounter ? noCounter : 0;
    this.abstentions_counter = abstentionsCounter ? abstentionsCounter : 0;
    this.no_words_counter = noWordsCounter ? noWordsCounter : 0;
    this.take_act = prendreActe;
    this.result = resultat;
    this.comment = comment ? comment : '';
    this.opinion = opinion;
    this.method = method;
    this.actors_votes = actorsVotes;
  }
}
