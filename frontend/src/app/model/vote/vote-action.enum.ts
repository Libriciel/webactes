export enum VoteAction {
  YES = 'yes', NO = 'no', ABSTENTION = 'abstention', NO_WORDS = 'no_words'
}
