import { HasId, HasName } from '../../ls-common';

export interface VoteMethod extends HasId, HasName {
  id: number;
  name: string;
  code: string;
}
