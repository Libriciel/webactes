import { HasId } from '../../ls-common';
import { VoteAction } from './vote-action.enum';

export class ActorVote implements HasId {
  id: number;
  actor_id: number;
  vote_id: number;
  result: VoteAction;
}
