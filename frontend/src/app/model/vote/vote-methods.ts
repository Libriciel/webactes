import { VoteMethodEnum } from './vote-method.enum';
import { VoteMethod } from './vote-method';
import { Choice } from '@libriciel/ls-composants';

export class VoteMethods {

  public static methodsChoices: Choice[] = [
    {name: 'Détails des voix', value: {id: 0, name: 'Détails des voix', code: VoteMethodEnum.DETAILS} as VoteMethod},
    {name: 'Total des voix', value: {id: 1, name: 'Total des voix', code: VoteMethodEnum.TOTAL} as VoteMethod},
    {name: 'Résultat', value: {id: 2, name: 'Résultat', code: VoteMethodEnum.RESULT} as VoteMethod},
    {name: 'Prendre acte', value: {id: 3, name: 'Prendre acte', code: VoteMethodEnum.TAKE_ACT} as VoteMethod}
  ];
}
