export enum VoteMethodEnum {
  DETAILS = 'details',
  TOTAL = 'total',
  RESULT = 'result',
  TAKE_ACT = 'take_act'
}
