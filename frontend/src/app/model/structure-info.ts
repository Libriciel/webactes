import { HasId, HasName } from '../ls-common';
import { StructureRaw } from '../ls-common/services/http-services/info/structure-raw';
import { Role } from './role';

export class StructureInfo implements HasId, HasName {
  id: number;
  name: string;
  businessName: string;
  customName: string;
  active: boolean;
  organizationId: number;
  roles?: Role[];
  spinner: string;

  constructor(structureRaw: StructureRaw) {
    this.id = structureRaw.id;
    this.name = structureRaw.name ? structureRaw.name : structureRaw.customname;
    this.businessName = structureRaw.business_name ? structureRaw.business_name : structureRaw.customname;
    this.customName = structureRaw.customname ? structureRaw.customname : structureRaw.name;
    this.active = structureRaw.active;
    this.organizationId = structureRaw.organization_id;
    this.roles = structureRaw.roles;
    this.spinner = structureRaw.spinner;
  }

}


