import { HasId } from '../ls-common/model/has-id';
import { HasName } from '../ls-common/model/has-name';
import { StructureInfo } from './structure-info';

export class Organization implements HasId, HasName {
  id: number;
  name: string;
  structures: StructureInfo;

  constructor(id?: number, name?: string, structures?: StructureInfo) {
    this.id = id;
    this.name = name;
    this.structures = structures;
  }
}
