export enum ThemeRight {
  isDeletable = 'isDeletable',
  isEditable = 'isEditable',
  isDeactivable = 'isDeactivable'
}
