import { HasId, HasName } from '../../ls-common';
import { HasAvailableActions } from '../../ls-common/model/has-available-actions';
import { ThemeRaw } from '../../ls-common/services/http-services/themes/theme-raw';
import { ThemeRight } from './theme-right.enum';

export class Theme extends HasAvailableActions implements HasId, HasName {

  id: number;
  name: string;
  structureId: number;
  active: boolean;
  parentId?: number;
  lft: number;
  rght: number;
  children?: Theme[] = [];
  position: string;
  isDefault?: boolean;

  constructor(themeRaw?: {
    id?: number,
    name: string,
    structure_id: number,
    active: boolean,
    parent_id?: number,
    lft: number,
    rght: number,
    childrenRaw?: ThemeRaw[],
    position: string,
    isDefault?: boolean,
    is_deletable: boolean,
    is_editable: boolean,
    is_deactivable: boolean,
  }) {
    super();
    if (themeRaw) {
      this.id = themeRaw.id;
      this.name = themeRaw.name;
      this.structureId = themeRaw.structure_id;
      this.active = themeRaw.active;
      this.parentId = themeRaw.parent_id;
      this.lft = themeRaw.lft;
      this.rght = themeRaw.rght;
      this.position = themeRaw.position;
      this.isDefault = themeRaw && themeRaw.isDefault ? themeRaw.isDefault : false;
      this.setAvailableAction(ThemeRight.isDeletable, themeRaw.is_deletable);
      this.setAvailableAction(ThemeRight.isEditable, themeRaw.is_editable);
      this.setAvailableAction(ThemeRight.isDeactivable, themeRaw.is_deactivable);

      if (themeRaw.childrenRaw) {
        themeRaw.childrenRaw.forEach(child => {
          const childTheme = new Theme(
            {
              id: child.id,
              name: child.name,
              structure_id: child.structure_id,
              active: child.active,
              parent_id: child.parent_id,
              childrenRaw: child.children,
              position: child.position,
              isDefault: child.isDefault,
              lft: child.lft,
              rght: child.rght,
              is_deletable: child.is_deletable,
              is_editable: child.is_editable,
              is_deactivable: child.is_deactivable,
            }
          );
          this.children.push(childTheme);
        });
      }
    } else {
      this.active = false;
    }
  }

  clone(): Theme {
    const theme = new Theme({
      id: this.id,
      name: this.name,
      structure_id: this.structureId,
      active: this.active,
      parent_id: this.parentId,
      lft: this.lft,
      rght: this.rght,
      position: this.position,
      isDefault: this.isDefault,
      is_deletable: this.hasAvailableAction(ThemeRight.isDeletable),
      is_editable: this.hasAvailableAction(ThemeRight.isEditable),
      is_deactivable: this.hasAvailableAction(ThemeRight.isDeactivable),
    });
    theme.children = this.children;
    return theme;
  }
}
