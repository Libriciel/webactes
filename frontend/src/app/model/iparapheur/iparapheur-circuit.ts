import { HasId } from '../../ls-common/model/has-id';
import { HasName } from '../../ls-common/model/has-name';

export interface IParapheurCircuit extends HasId, HasName {
  id: number;
  name: string;
}
