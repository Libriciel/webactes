import { IParapheurStatus } from './iparapheur-status.enum';

export interface IParapheurHistoryElement {
  status: IParapheurStatus;
  message: string;
}
