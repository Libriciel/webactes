export enum IParapheurStatus {
  STATUS_LU = 'Lu',
  STATUS_SIGNE = 'Signe',
  STATUS_REJET_SIGNATAIRE = 'RejetSignataire',
  STATUS_PRET_VISA = 'PretVisa',
  STATUS_VISE = 'Vise',
  STATUS_REJET_VISA = 'RejetVisa',

  STATUS_PRET_TDT = 'PretTdT',
  STATUS_EN_COURS_TRANSMISSION = 'EnCoursTransmission',
  STATUS_TRANSMISSION_OK = 'TransmissionOK',
  STATUS_TRANSMISSION_KO = 'TransmissionKO',
  STATUS_REJET_TRANSMISSION = 'RejetTransmission',

  STATUS_PRET_MAILSEC = 'PretMailSec',
  STATUS_EN_COURS_MAILSEC = 'EnCoursMailSec',
  STATUS_MAILSEC_OK = 'MailSecOK',
  STATUS_REJET_MAILSEC = 'RejetMailSec',

  STATUS_PRET_MAILSEC_PASTELL = 'PretMailSecPastell',
  STATUS_EN_COURS_MAILSEC_PASTELL = 'EnCoursMailSecPastell',
  STATUS_OK_MAILSEC_PASTELL = 'MailSecPastellOK',
  STATUS_REJET_MAILSEC_PASTELL = 'RejetMailSecPastell',

  STATUS_EN_COURS_ATTEST = 'EnCoursAttest',
  STATUS_ATTEST_KO = 'AttestKO',
  STATUS_ATTEST_OK = 'AttestOK',

  STATUS_PRET_CACHET = 'PretCachet',
  STATUS_REJET_CACHET = 'RejetCachet',
  STATUS_CACHET_OK = 'CachetOK',

  STATUS_ARCHIVE = 'Archive',
  STATUS_A_TRAITER = 'ATraiter',

  STATUS_EN_COURS = 'EnCours'
}
