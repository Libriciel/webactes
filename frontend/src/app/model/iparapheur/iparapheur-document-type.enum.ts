export enum IParapheurDocumentType {
  HISTORY = 'Historique I-Parapheur',
  FORM = 'Bordereau de signature',
  MAIN_DOCUMENT = 'Document Principal signé'
}
