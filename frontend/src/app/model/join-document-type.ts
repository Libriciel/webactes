import { HasId } from '../ls-common/model/has-id';
import { HasName } from '../ls-common/model/has-name';
import { JoinDocumentTypeRaw } from '../ls-common/services/http-services/joinDocumentType/join-document-type-raw';

export class JoinDocumentType implements HasId, HasName {
  id: number;
  name: string;
  codeType: string;

  constructor(joinDocumentTypeRaw: JoinDocumentTypeRaw) {
    this.id = joinDocumentTypeRaw.id;
    this.name = `${joinDocumentTypeRaw.libelle} (${joinDocumentTypeRaw.codetype})`;
    this.codeType = joinDocumentTypeRaw.codetype;
  }
}
