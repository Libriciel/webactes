import { FilterType } from './filter-type';

export interface TypeactFilterType extends FilterType{
  isdeliberating?: boolean;
}
