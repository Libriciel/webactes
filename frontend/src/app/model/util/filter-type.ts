export interface FilterType {
  searchedText?: string;
  active?: boolean;
}
