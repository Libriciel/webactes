import { HasId } from '../ls-common/model/has-id';
import { HasName } from '../ls-common/model/has-name';

export class Role implements HasId, HasName {
  id: number;
  name: string;

  constructor(id?: number, name?: string,){
    this.id = id;
    this.name = name;
  }
}
