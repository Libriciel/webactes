import { HasId, HasName } from '../ls-common';

export class StatesAct implements HasId, HasName {
  id: number;
  isDefault?: boolean;
  code: string;
  name: string;
}
