import { Actor, Actorgroup } from '../../core';
import { Opinion } from './opinion.enum';

export class Voter implements Actor {
  active: boolean;
  actor_groups: Array<Actorgroup>;
  address: string;
  address_supplement: string;
  cellphone: string;
  city: string;
  civility: string;
  email: string;
  firstname: string;
  full_name: string;
  id: number;
  lastname: string;
  name: string;
  order: number;
  phone: string;
  post_code: number;
  rank: string;
  title: string;

  // Specific to voter
  isPresent: boolean;
  opinion: Opinion;

  constructor(actor: Actor) {
    this.id = actor.id;
    this.name = actor.name;
    this.active = actor.active;
    this.rank = actor.rank;
    this.order = actor.order;
  }
}
