import { Summon } from './summon';
import { Observable } from 'rxjs';

export abstract class IManageSummonService {

  abstract add(summon: Summon): Observable<Summon>;

  abstract getBySittingId(sittingId: number): Observable<Summon>;

  abstract send(summonId: Number): Observable<any>;

  abstract sendToIdelibre(summonId: Number): Observable<any>;

}
