import { Attachment } from './attachment';
import { AttachmentSummonFile } from '../../core';

export class AttachmentSummon {
  summon_id: number;
  attachment_id?: number;
  rank: number;
  attachment?: Attachment;
  file?: AttachmentSummonFile;

  constructor(summon_id: number,
              rank: number,
              attachment?: Attachment,
              attachment_id?: number,
              attachment_summon_file?: AttachmentSummonFile) {
    this.summon_id = summon_id;
    this.attachment_id = attachment_id;
    this.rank = rank;
    this.attachment = attachment;
    this.file = attachment_summon_file;
  }
}
