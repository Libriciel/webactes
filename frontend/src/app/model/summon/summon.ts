import { Convocation } from './convocation';
import { AttachmentSummon } from './attachment-summon';
import { AttachmentSummonType } from './attachment-sumon-type.enum';

export class Summon {

  static ATTACHEMENT_SUMMON_CONVOCATION_RANK = 1;
  static ATTACHEMENT_SUMMON_EXECUTIVE_SUMMARY_RANK = 2;

  id: number;
  summon_type_id: number;
  structure_id: number;
  sitting_id: number;
  convocations: Convocation[];
  attachments_summons: AttachmentSummon[];
  generationerrors: { type: string; message: string; date: Date }[];


  getAttachement(attachmentSummonType: AttachmentSummonType): AttachmentSummon {
    switch (attachmentSummonType) {
      case AttachmentSummonType.CONVOCATION:
        return this.attachments_summons.find(attachement => attachement.rank === Summon.ATTACHEMENT_SUMMON_CONVOCATION_RANK);
      case AttachmentSummonType.EXECUTIVE_SUMMARY:
        return this.attachments_summons.find(attachement => attachement.rank === Summon.ATTACHEMENT_SUMMON_EXECUTIVE_SUMMARY_RANK);
    }
  }
}
