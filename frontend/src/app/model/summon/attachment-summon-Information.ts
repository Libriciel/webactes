import { AttachmentSummonFile } from '../../core';
import { FormValues } from '../../ls-common/model/form-values';

export interface AttachmentSummonInformation extends FormValues {
  convocation: AttachmentSummonFile;
  noteDeSynthese: AttachmentSummonFile;
}
