export enum AttachmentSummonType {
  CONVOCATION = 'convocation',
  EXECUTIVE_SUMMARY = 'executiveSummary'
}
