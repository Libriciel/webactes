import { HasId, HasName } from '../../ls-common';

export class Attachment implements HasId, HasName {
  id: number;
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}
