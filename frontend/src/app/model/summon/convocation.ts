import { Actor } from '../../core';
import { HasId } from '../../ls-common';

export interface Convocation extends HasId {
  actor: Actor[];
  date_open: string;
  date_send: string;
  id: number;
  jeton: string;
  pastell_id: number;
  structure_id: number;
  summon_id: number;
}

