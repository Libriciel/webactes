import {Options} from '../../ls-common/model/options';

export interface WANotificationsOptions {
  userNotifications: Options;
  otherNotifications: Options;
}
