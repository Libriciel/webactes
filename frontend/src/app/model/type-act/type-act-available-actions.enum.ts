export enum TypeActAvailableActions {
  isDeletable,
  isEditable,
  isDeactivable,
}
