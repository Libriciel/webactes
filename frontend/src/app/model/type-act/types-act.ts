import { HasId, HasName } from '../../ls-common';
import { Nature } from '../Nature/nature';
import { TypeActRaw } from '../../ls-common/services/http-services/type-act/type-act-raw';
import { HasAvailableActions } from '../../ls-common/model/has-available-actions';
import { TypeActAvailableActions } from './type-act-available-actions.enum';
import { DraftTemplate } from '../draft-template/draft-template';
import { Counter } from '../counter/counter';

export class TypesAct extends HasAvailableActions implements HasName, HasId {

  id: number;
  name: string;
  isTransferable: boolean;
  isDeliberative: boolean;
  active: boolean;
  nature?: Nature;
  natureId?: number;
  isDefault: boolean;
  draftTemplates?: DraftTemplate[];
  generate_template_project_id?: number;
  generate_template_act_id?: number;
  counter?: Counter;
  counterId?: number;

  constructor(typeAct?: TypeActRaw) {
    super();
    this.id = typeAct && typeAct.id ? typeAct.id : 0;
    this.isTransferable = typeAct && typeAct.istdt ? typeAct.istdt : false;
    this.isDeliberative = typeAct && typeAct.isdeliberating ? typeAct.isdeliberating : false;
    this.name = typeAct && typeAct.name ? typeAct.name : '';
    this.active = typeAct && typeAct.active ? typeAct.active : false;
    this.nature = typeAct && typeAct.nature ? typeAct.nature : null;
    this.natureId = typeAct && typeAct.nature_id ? typeAct.nature_id : null;
    this.draftTemplates = typeAct && typeAct.draftTemplates ? typeAct.draftTemplates : [];
    this.isDefault = typeAct && typeAct.isdefault ? typeAct.isdefault : false;
    this.generate_template_project_id = typeAct && typeAct.generate_template_project_id ? typeAct.generate_template_project_id : null;
    this.generate_template_act_id = typeAct && typeAct.generate_template_act_id ? typeAct.generate_template_act_id : null;
    this.counter = typeAct && typeAct.counter ? typeAct.counter : null;
    this.counterId = typeAct && typeAct.counter_id ? typeAct.counter_id : null;
    this.setAvailableAction(
      TypeActAvailableActions.isDeletable,
      typeAct && typeAct.availableActions ? typeAct.availableActions.can_be_deleted.value : true);
    this.setAvailableAction(
      TypeActAvailableActions.isEditable,
      typeAct && typeAct.availableActions ? typeAct.availableActions.can_be_edited.value : false);
    this.setAvailableAction(
      TypeActAvailableActions.isDeactivable,
      typeAct && typeAct.availableActions ? typeAct.availableActions.can_be_deactivate.value : false);
  }

  clone(): TypesAct {
    return new TypesAct({
      id: this.id,
      istdt: this.isTransferable,
      isdeliberating: this.isDeliberative,
      name: this.name,
      active: this.active,
      nature: this.nature,
      nature_id: this.natureId,
      draftTemplates: this.draftTemplates,
      isdefault: this.isDefault,
      generate_template_project_id: this.generate_template_project_id,
      generate_template_act_id: this.generate_template_act_id,
      counter: this.counter,
      counter_id: this.counterId,
    });
  }
}
