export class PastellStructure {
  id_e: number;
  denomination: string;
  siren: number;
  type: string;
  centre_de_gestion: number;
  entite_mere: number;

  constructor(obj: { id_e: string, denomination: string, siren: string, type: string, centre_de_gestion: string, entite_mere: string }) {
    this.id_e = Number(obj.id_e);
    this.denomination = obj.denomination;
    this.siren = Number(obj.siren);
    this.type = obj.type;
    this.centre_de_gestion = Number(obj.centre_de_gestion);
    this.entite_mere = Number(obj.entite_mere);
  }

  isRoot(): boolean {
    return this.entite_mere === 0;
  }
}
