import { BackendStep } from './backend-step';

export class BackendCircuit {
  name: string;
  steps: BackendStep[] = [];
  description?: string;
}
