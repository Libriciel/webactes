import { ClassificationSubject } from './classification/classification-subject';
import { Annexe } from '../projects/model/annexe';
import { JoinDocumentType } from './join-document-type';

export interface ActTeletransmissionInformation {
  classification?: ClassificationSubject;
  codeAct?: string;
  mainDocumentType?: JoinDocumentType;
  isMultichannel?: boolean;
  annexes: Annexe[];
}
