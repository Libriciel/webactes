import { HasId, HasName } from '../../ls-common';
import { TypeActRaw } from '../../ls-common/services/http-services/type-act/type-act-raw';
import { HasAvailableActions } from '../../ls-common/model/has-available-actions';
import { TypeSittingAvailableActions } from './type-sitting-available-actions.enum';
import { TypeSittingAvailableActionsRaw } from '../../ls-common/services/http-services/type-sittings/type-sitting-available-actions-raw.enum';
import { Actorgroup } from '../../core';

export class TypeSitting extends HasAvailableActions implements HasName, HasId {
  id: number;
  name: string;
  isdeliberating: boolean;
  active?: boolean;
  typeActs?: TypeActRaw[];
  availableActions: TypeSittingAvailableActionsRaw;
  actorGroups?: Actorgroup[];
  generate_template_convocation_id?: number;
  generate_template_executive_summary_id?: number;
  generate_template_deliberations_list_id?: number;
  generate_template_verbal_trial_id?: number;

  constructor(
    id?: number,
    name?: string,
    isDeliberating?: boolean,
    active?: boolean,
    typeActs?: TypeActRaw[],
    availableActions?: TypeSittingAvailableActionsRaw,
    actorGroups?: Actorgroup[],
    generate_template_convocation_id?: number,
    generate_template_executive_summary_id?: number,
    generate_template_deliberations_list_id?: number,
    generate_template_verbal_trial_id?: number,
  ) {
    super();
    this.id = id;
    this.name = name ? name : '';
    this.isdeliberating = isDeliberating;
    this.active = active ? active : false;
    this.typeActs = typeActs ? typeActs : [];
    this.actorGroups = actorGroups ? actorGroups : [];
    this.generate_template_convocation_id = generate_template_convocation_id ? generate_template_convocation_id : null;
    this.generate_template_executive_summary_id = generate_template_executive_summary_id ? generate_template_executive_summary_id : null;
    this.generate_template_deliberations_list_id =
      generate_template_deliberations_list_id ? generate_template_deliberations_list_id : null;
    this.generate_template_verbal_trial_id = generate_template_verbal_trial_id ? generate_template_verbal_trial_id : null;
    this.setAvailableAction(TypeSittingAvailableActions.isDeletable, availableActions ? availableActions.can_be_deleted.value : false);
    this.setAvailableAction(TypeSittingAvailableActions.isEditable, availableActions ? availableActions.can_be_edited.value : false);
    this.setAvailableAction(
      TypeSittingAvailableActions.isDeactivatable,
      availableActions ? availableActions.can_be_deactivate.value : false);
  }

  clone(): TypeSitting {
    return new TypeSitting(
      this.id,
      this.name,
      this.isdeliberating,
      this.active,
      this.typeActs,
      this.availableActions,
      this.actorGroups,
      this.generate_template_convocation_id,
      this.generate_template_executive_summary_id,
      this.generate_template_deliberations_list_id,
      this.generate_template_verbal_trial_id,
    );
  }
}
