export enum TypeSittingAvailableActions {
  isDeletable,
  isEditable,
  isDeactivatable,
}
