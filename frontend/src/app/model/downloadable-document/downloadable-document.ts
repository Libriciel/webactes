import { HasId } from '../../ls-common';
import { TDTDocumentType } from './tdt-document-type.enum';
import { IParapheurDocumentType } from '../iparapheur/iparapheur-document-type.enum';
import { ProjectDocumentTypeEnum } from './project-document-type.enum';

export class DownloadableDocument implements HasId {
  id: number;
  projectId: number;
  type: TDTDocumentType | IParapheurDocumentType | ProjectDocumentTypeEnum;
  typeName: string;
  fileName: string;
  annexId?: number;
  mimeType?: string;

  constructor(id: number, type: TDTDocumentType | IParapheurDocumentType | ProjectDocumentTypeEnum, fileName: string, mimeType?: string, annexId?: number) {
    this.projectId = id;
    this.type = type;
    this.typeName = type.toString();
    this.fileName = fileName;
    this.annexId = annexId;
    this.mimeType = mimeType || 'application/pdf';
  }
}
