export enum TDTDocumentType {
  ACT = 'Acte tamponné',
  BORDEREAU = 'Bordereau',
  ACT_AR = 'ARacte',
  ANNEX = 'Annexe tamponnée'
}
