export class Pagination {
  page: number;
  count: number;
  perPage: number;
}
