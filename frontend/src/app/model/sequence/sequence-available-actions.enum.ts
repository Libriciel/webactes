export enum SequenceAvailableActions {
  'isEditable',
  'isDeletable',
}
