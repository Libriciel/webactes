import { HasAvailableActions } from '../../ls-common/model/has-available-actions';
import { HasName } from '../../ls-common';
import { HasId } from '../../ls-common';
import { SequenceAvailableActions } from './sequence-available-actions.enum';
import { SequenceAvailableActionsRaw } from '../../ls-common/services/http-services/sequence/sequence-available-actions-raw.enum';

export class Sequence extends HasAvailableActions implements HasName, HasId {
  id: number;
  name: string;
  comment: string;
  sequence_num: number;
  availableActions: SequenceAvailableActionsRaw;

  constructor(id?: number, name?: string, comment?: string, sequence_num?: number, availableActions?: SequenceAvailableActionsRaw) {
    super();
    this.id = id;
    this.name = name;
    this.comment = comment;
    this.sequence_num = sequence_num;
    this.setAvailableAction(SequenceAvailableActions.isDeletable, availableActions ? availableActions.can_be_deleted.value : false);
    this.setAvailableAction(SequenceAvailableActions.isEditable, availableActions ? availableActions.can_be_edited.value : false);
  }

  clone(): Sequence {
    return new Sequence(
      this.id,
      this.name,
      this.comment,
      this.sequence_num,
      this.availableActions,
    );
  }
}
