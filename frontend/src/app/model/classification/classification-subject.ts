import { HasId } from '../../ls-common/model/has-id';
import { HasName } from '../../ls-common/model/has-name';

export interface ClassificationRaw {
  id: number;
  libelle: string;
  parent_id?: number;
  children: ClassificationRaw[];
}

export class ClassificationSubject implements HasId, HasName {

  id: number;
  name: string;
  parent_name?: string;
  parent_id: number;
  children: ClassificationSubject[];

  constructor(classificationRaw: ClassificationRaw) {
    this.id = classificationRaw.id;
    this.name = classificationRaw.libelle;
    this.parent_id = classificationRaw.parent_id;
    this.children = classificationRaw.children.map(value => {
      const classificationSubject = new ClassificationSubject(value);
      classificationSubject.parent_name = this.name;
      return classificationSubject;
    });
  }
}
