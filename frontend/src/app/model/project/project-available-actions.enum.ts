export enum ProjectAvailableActions {
  isDeletable,
  isEditable,
  isStateEditable,
  canBeSendToCircuit,
  canBeValidatedInCircuit,
  canValidateInEmergency,
  canBeSendToIParapheur,
  canBeSendToManualSignature,
  isIParapheurRejected,
  canCheckIParapheurStatus,
  canBeVoted,
  canGenerateActNumber
}
