import { ProjectHistoryElementType } from './project-history-element-type.enum';
import { User } from '../user/user';
import { ProjectHistoryElementRaw } from '../../ls-common/services/http-services/project/project-history-element-raw';

export class ProjectHistoryElement {
  type: ProjectHistoryElementType;
  date: Date;
  user: User;
  data: any;

  constructor(projectHistoryElementRaw: ProjectHistoryElementRaw) {
    this.type = projectHistoryElementRaw.comment;
    this.date = projectHistoryElementRaw.created;
    this.user = projectHistoryElementRaw.user;
    this.data = projectHistoryElementRaw.data;
  }
}

