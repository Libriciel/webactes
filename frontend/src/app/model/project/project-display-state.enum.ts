export enum ProjectDisplayState {
  DRAFT = 'brouillon',
  TO_BE_VALIDATED = 'a-valider',
  VALIDATING = 'en-cours-de-validation',
  VALIDATED = 'valide',

  SEARCH = 'search',

  TO_BE_TRANSMITTED = 'a-deposer-sur-le-tdt',
  TRANSMISSION_READY = 'a-envoyer-en-prefecture',

  ACT = 'acte',

  UNASSOCIATED = 'unassociated',
  VALIDATED_INTO_SITTING = 'valide-into-sitting',
  VOTING = 'voting',
  VOTED = 'voted',
}
