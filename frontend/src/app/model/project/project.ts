import {Annexe} from '../../projects/model/annexe';
import {TypesAct} from '../type-act/types-act';
import {StatesAct} from '../states-act';
import {Sitting} from '../Sitting/sitting';
import {Theme} from '../theme/theme';
import {WADocument} from '../../projects/model/wa-document';
import {CircuitInstance} from '../circuit/circuit-instance';
import {HasId} from '../../ls-common';
import {ClassificationSubject} from '../classification/classification-subject';
import {ActTeletransmissionInformation} from '../act-teletransmission-information';
import {HasDate} from '../../ls-common/model/has-date';
import {
  ProjectGeneralInformation
} from '../../projects/components/forms/project-general-information/project-general-information';
import {
  ProjectComplementaryInformation
} from '../../projects/components/forms/project-complementary-information/project-complementary-information';
import {
  ProjectDocumentsInformation
} from '../../projects/components/forms/project-documents/project-documents-information';
import {HasAvailableActions} from '../../ls-common/model/has-available-actions';
import {ProjectAvailableActions} from './project-available-actions.enum';
import {IParapheurHistoryElement} from '../iparapheur/iparapheur-history-element';
import {ProjectHistoryElement} from './project-history-element';
import {ProjectAvailableActionsRaw} from '../../ls-common/services/http-services/project/project-available-actions-raw';
import {ProjectDisplayState} from './project-display-state.enum';
import {Vote} from '../vote/vote';
import {ProjectText} from '../../core';

export class Project extends HasAvailableActions implements HasId, HasDate {
    id: number;

    // General Information
    name: string;
    stateact_id: number;
    stateact?: StatesAct;
    circuit?: CircuitInstance;
    signature?: IParapheurHistoryElement[];
    history?: ProjectHistoryElement[];
    typesact_id: number;
    typeAct?: TypesAct;
    date: Date;
    canonicalProjectDisplayState: ProjectDisplayState;
    vote?: Vote;

    // Complementary Information
    codeAct?: string;
    sittings_ids: number[];
    sittings?: Sitting[];
    theme?: Theme;
    theme_id?: number;
    classification?: ClassificationSubject;
    classification_id?: number;
    isMultichannel?: boolean;

    // Documents
    mainDocument: WADocument;
    annexes?: Annexe[];
    projectTexts?: ProjectText[];

    isSignedIParapheur: boolean;
    rank?: number;
    generationerror: { type: string; message: string; date: Date }[];

    constructor(data?: {
        date: Date;
        availableActions: ProjectAvailableActionsRaw;
        code: string;
        circuit: CircuitInstance;
        signature: IParapheurHistoryElement[];
        history: ProjectHistoryElement[];
        classification: ClassificationSubject;
        annexes: any;
        classificationId: number;
        isStateEditable: boolean;
        projectTexts: [];
        generationerror: { type: string; message: string; date: Date }[];
        stateact: StatesAct;
        sittingsId: number[];
        isSignedIParapheur: boolean;
        name: string;
        isMultichannel: boolean;
        theme: Theme;
        id: number;
        sittings: Sitting[];
        mainDocument: WADocument;
        typeact: TypesAct
    }) {
        super();
        if (data) {
            this.id = data.id;
            this.initializeAvailableActions(data);
            this.name = data.name;
            this.codeAct = data.code;
            this.sittings_ids = data.sittingsId;
            this.isMultichannel = data.isMultichannel;
            this.mainDocument = data.mainDocument;
            this.annexes = data.annexes;
            this.projectTexts = data.projectTexts;
            this.theme = data.theme;
            // this.theme_id = data.themeId ? data.themeId : (data.theme ? data.theme.id : null);
            this.typeAct = data.typeact;
            this.typesact_id = data.typeact ? data.typeact.id : null;
            this.stateact = data.stateact;
            this.stateact_id = data.typeact ? data.typeact.id : null;
            this.classification = data.classification;
            this.classification_id = data.classificationId ? data.classificationId : (data.classification ? data.classification.id : null);
            this.sittings = data.sittings;
            this.circuit = data.circuit;
            this.signature = data.signature;
            this.history = data.history;
            this.isSignedIParapheur = data.isSignedIParapheur;
            this.generationerror = data.generationerror;

        }
    }

    private initializeAvailableActions(data: {
        id?: number;
        name: string;
        code: string;
        date: Date;
        isMultichannel: boolean;
        sittings?: Sitting[];
        themeId?: number;
        theme?: Theme;
        actTypeId?: number;
        typeact?: TypesAct;
        actStateId?: number;
        stateact?: StatesAct;
        classificationId?: number;
        classification?: ClassificationSubject;
        sittingsId: number[];
        mainDocument: WADocument;
        annexes: Annexe[];
        projectTexts: ProjectText[];
        circuit?: CircuitInstance;
        isStateEditable: boolean;
        signature?: IParapheurHistoryElement[];
        isSignedIParapheur?: boolean;
        history?: ProjectHistoryElement[];
        availableActions?: ProjectAvailableActionsRaw
    }) {
        this.setAvailableAction(ProjectAvailableActions.isStateEditable, data.isStateEditable);
        this.setAvailableAction(
            ProjectAvailableActions.isEditable,
            data.availableActions.can_be_edited
                ? data.availableActions.can_be_edited.value
                : false);
        this.setAvailableAction(
            ProjectAvailableActions.isDeletable,
            data.availableActions.can_be_deleted
                ? data.availableActions.can_be_deleted.value
                : false);
        this.setAvailableAction(ProjectAvailableActions.canCheckIParapheurStatus,
            data.availableActions.check_signing
                ? data.availableActions.check_signing.value
                : false);
        this.setAvailableAction(ProjectAvailableActions.canBeSendToIParapheur,
            data.availableActions.can_be_send_to_i_parapheur
                ? data.availableActions.can_be_send_to_i_parapheur.value
                : false);
        this.setAvailableAction(ProjectAvailableActions.canBeSendToManualSignature,
            data.availableActions.can_be_send_to_manual_signature
                ? data.availableActions.can_be_send_to_manual_signature.value
                : false);
        this.setAvailableAction(ProjectAvailableActions.isIParapheurRejected,
            data.availableActions.check_refused
                ? data.availableActions.check_refused.value
                : false);
        this.setAvailableAction(ProjectAvailableActions.canBeValidatedInCircuit,
            data.circuit
                ? data.circuit.canPerformAnAction
                : false);
        this.setAvailableAction(ProjectAvailableActions.canValidateInEmergency,
            data.availableActions.can_approve_project
                ? data.availableActions.can_approve_project.value
                : false);
        this.setAvailableAction(ProjectAvailableActions.canBeSendToCircuit,
            data.availableActions.can_be_send_to_workflow
                ? data.availableActions.can_be_send_to_workflow.value
                : false);
        this.setAvailableAction(ProjectAvailableActions.canBeVoted,
            data.availableActions.can_be_voted
                ? data.availableActions.can_be_voted.value
                : false);
        this.setAvailableAction(ProjectAvailableActions.canGenerateActNumber,
            data.availableActions.can_generate_act_number
                ? data.availableActions.can_generate_act_number.value
                : false);
    }

    update(generalInformation: ProjectGeneralInformation,
           complementaryInformation: ProjectComplementaryInformation,
           documentsInformation: ProjectDocumentsInformation) {
        this.name = generalInformation.actName;
        this.stateact_id = generalInformation.stateAct.id;
        this.typesact_id = generalInformation.typeAct.id;

        this.theme_id = complementaryInformation.theme ? complementaryInformation.theme.id : null;
        this.sittings_ids = generalInformation.typeAct.isDeliberative ? complementaryInformation.sittings.map(sitting => sitting.id) : null;
        this.codeAct = complementaryInformation.codeAct;
        this.classification_id = complementaryInformation.classification ? complementaryInformation.classification.id : null;
        this.isMultichannel = generalInformation.typeAct && generalInformation.typeAct.isTransferable
            ? complementaryInformation.isMultichannel
            : null;

        if (documentsInformation) {
            this.mainDocument = documentsInformation.mainDocument;
            this.annexes = documentsInformation.annexes;
            this.projectTexts = documentsInformation.projectTexts;
        }
    }

    updateTransmissionInfo(info: ActTeletransmissionInformation) {
        this.classification = info.classification;
        this.classification_id = info.classification ? info.classification.id : null;
        this.codeAct = info.codeAct;
        this.isMultichannel = info.isMultichannel;

        if (this.mainDocument) {
            this.mainDocument.documentType = info.mainDocumentType;
            this.mainDocument.codeType = info.mainDocumentType ? info.mainDocumentType.codeType : null;
        }

        if (this.annexes && info.annexes) {
            info.annexes.forEach(annex => {
                if (annex.documentType) {
                    annex.codeType = annex.documentType.codeType;
                }
            });
            this.annexes = info.annexes;
        }

    }

    /**
     * Unsafe clone, all array/object properties are shallow-cloned, except for the annexes and classification!
     *
     * Do not use without careful check.
     */
    unsafeClone(): Project {
        const clone: Project = Object.create(this);
        Object.assign(clone, this);

        clone.classification = Object.assign({}, this.classification);

        // We need to ensure the annexes are deep-copied.
        clone.annexes = this.annexes ? this.annexes.map(
            annex => {
                const newAnnex = Object.create(annex);
                Object.assign(newAnnex, annex);
                return newAnnex;
            }
        ) : [];
        return clone;
    }

    setRank(rank) {
        this.rank = rank;
        return this;
    }

    setTypeAct(typeAct) {
        this.typeAct = typeAct;
        return this;
    }

    setStateAct(stateAct) {
        this.stateact = stateAct;
        return this;
    }
}
