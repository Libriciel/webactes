import { HasId } from '../../ls-common/model/has-id';
import { HasName } from '../../ls-common/model/has-name';

export class GenerateTemplate implements HasId, HasName {
  id: number;
  name: string;
  code: string;

  constructor(generateTemplateRaw?: { id: number, name: string}) {
    this.id = generateTemplateRaw.id;
    this.name = generateTemplateRaw.name;
  }
}
