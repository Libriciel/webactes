
export class GenerateTemplateRaw {
  id: number;
  name: string;
  code: string;

  constructor(generateTemplateRaw?: { id: number, name: string, code: string }) {
    this.id = generateTemplateRaw.id;
    this.name = generateTemplateRaw.name;
    this.code = generateTemplateRaw.code;
  }
}
