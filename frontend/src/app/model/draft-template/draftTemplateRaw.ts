
export class DraftTemplateRaw {
  id: number;
  name: string;
  code: string;

  constructor(draftTemplateRaw?: { id: number, name: string, code: string }) {
    this.id = draftTemplateRaw.id;
    this.name = draftTemplateRaw.name;
    this.code = draftTemplateRaw.code;
  }
}
