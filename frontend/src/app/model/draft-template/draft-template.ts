import { HasId, HasName } from '../../ls-common';

export class DraftTemplate implements HasId, HasName {
  id: number;
  name: string;
  code: string;

  constructor(draftTemplateRaw?: { id: number, name: string }) {
    this.id = draftTemplateRaw.id;
    this.name = draftTemplateRaw.name;
  }
}
