import { HasAvailableActions } from '../../ls-common/model/has-available-actions';
import { HasId, HasName } from '../../ls-common';
import { CounterAvailableActions } from './counter-available-actions.enum';
import { CounterAvailableActionsRaw } from '../../ls-common/services/http-services/counters/counter-available-actions-raw';
import { Sequence } from '../sequence/sequence';

export class Counter extends HasAvailableActions implements HasId, HasName {

  id: number;
  name: string;
  comment: string;
  counter_def: string;
  sequence_id?: number;
  sequence?: Sequence;
  reinit_def: string;
  availableActions: CounterAvailableActionsRaw;

  constructor(id?: number,
              name?: string,
              comment?: string,
              counter_def?: string,
              sequence_id?: number,
              sequence?: Sequence,
              reinit_def?: string,
              availableActions?: CounterAvailableActionsRaw) {
    super();
    this.id = id;
    this.name = name;
    this.comment = comment;
    this.counter_def = counter_def ? counter_def : '';
    this.sequence_id = sequence_id;
    this.sequence = sequence;
    this.reinit_def = reinit_def;
    this.setAvailableAction(CounterAvailableActions.isEditable, availableActions ? availableActions.can_be_edited.value : false);
    this.setAvailableAction(CounterAvailableActions.isDeletable, availableActions ? availableActions.can_be_deleted.value : false);
  }

  clone(): Counter {
    return new Counter(
      this.id,
      this.name,
      this.comment,
      this.counter_def,
      this.sequence_id,
      this.sequence,
      this.reinit_def,
      this.availableActions
    );
  }

}
