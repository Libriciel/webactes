export enum CounterAvailableActions {
  isDeletable,
  isEditable
}
