import { Step } from './step';
import { HasName } from '../../ls-common/model/has-name';
import { HasAvailableActions } from '../../ls-common/model/has-available-actions';

export class Circuit extends HasAvailableActions implements HasName {
  id: number;
  name: string;
  description?: string;
  active?: boolean;
  steps: Step[] = [];
}
