import { Choice } from '@libriciel/ls-composants';

export enum StepType_t {
  Simple,
  Concurrent,
  Collaborative
}

// 'simple' | 'concurrent' | 'collaborative'
export class StepType {
  id: StepType_t;
  name: string;
}

export const STEP_TYPE_LIST: StepType[] = [
  {id: StepType_t.Simple, name: 'Simple'},
  {id: StepType_t.Concurrent, name: 'Concurrent'}
  // ,
  // {id: StepType_t.Collaborative, name: 'Collaborative'}
];

export const STEP_TYPE_CHOICES: Choice[] = [
  {name: 'Simple', value: {id: StepType_t.Simple, name: 'Simple'}}, {name: 'Concurrent', value: {id: StepType_t.Concurrent, name: 'Concurrent'}}
];
