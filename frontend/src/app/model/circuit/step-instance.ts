import { HasName } from '../../ls-common/model/has-name';
import { User } from '../user/user';
import { StepType } from './step-type';

export enum StepState {
  PENDING = 'pending',
  CURRENT = 'current',
  PASSED = 'approve',
  REJECTED = 'reject',
  SKIPPED = 'skipped',
  ADMIN_APPROVED = 'approved_admin'
}

export class StepInstance implements HasName {
  id: string;
  name: string;
  type: StepType;
  validators: User[] = [];
  state: StepState = StepState.PENDING;
  validatedBy: User[] = [];
  comment?: string;

  constructor(stepInstData?: {
                id: string,
                name: string,
                validators: User[],
                state: StepState,
                actedUponBy?: User,
                comment?: string,
                actionId?: string
              }
  ) {
    this.id = stepInstData.id;
    this.name = stepInstData.name;
    this.validators = stepInstData.validators ? [...stepInstData.validators] : [];
    this.state = stepInstData.state;
    this.validatedBy = stepInstData.actedUponBy ? [stepInstData.actedUponBy] : [];
    this.comment = stepInstData.comment;
    if (stepInstData.actionId) {
      this.id = stepInstData.actionId;
    }
  }
}
