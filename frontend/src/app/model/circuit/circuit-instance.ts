import { HasName } from '../../ls-common/model/has-name';
import { StepInstance, StepState } from './step-instance';

export class CircuitInstance implements HasName {
  id: number;
  name: string;
  description?: string;
  currentStepIdx = 0;
  steps: StepInstance[];
  rejected = false;
  isOver = false;
  wasValidatedInEmergency = false;
  currentStep?: StepInstance;

  canPerformAnAction = false;

  // This constructor is only valid while we use stub services,
  // It should be replace with a constructor from "circuitRaw"
  // constructor(circuitModel: Circuit, stepInstances?: StepInstance[]) {
  constructor(instData?: {
                id: number;
                name: string;
                description?: string;
                steps: StepInstance[];
                isRejected: boolean;
                isOver: boolean;
                canPerformAnAction: boolean;
                currentStep?: StepInstance;
              }
  ) {
    this.id = instData.id;
    this.name = instData.name;
    this.description = instData.description;
    this.rejected = instData.isRejected;
    this.isOver = instData.isOver;
    this.canPerformAnAction = instData.canPerformAnAction;

    if (instData.steps) {
      this.steps = instData.steps;
      this.currentStepIdx = instData.steps.findIndex(step => step.state === StepState.CURRENT);
      if (this.currentStepIdx === -1) {
        this.currentStepIdx = 0;
      }
      this.wasValidatedInEmergency = this.steps.some(step => step.state === StepState.SKIPPED);
    }
    this.currentStep = instData.currentStep;
  }

  getLastManagedStep(): StepInstance {
    return this.steps.filter(step => step.state !== StepState.PENDING).pop();
  }
}
