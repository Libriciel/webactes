import { StepType } from './step-type';
import { HasName } from '../../ls-common/model/has-name';
import { User } from '../user/user';

export class Step implements HasName {

  name: string;
  type: StepType;
  validators: User[] = [];

}
