import { HasId } from '../ls-common';

export class Search implements HasId {
  id: number;
  project_name?: string;
  code_act?: string;
  typeacts?: string;

  constructor(id?: number, project_name?: string, code_act?: string, typeacts?: string) {
    this.id = 0;
    this.project_name = project_name;
    this.code_act = code_act;
    this.typeacts = typeacts;
  }
}
