export enum StateActCode {
  DRAFT = 'brouillon',
  REFUSED = 'refuse',
  I_PARAPHEUR_REFUSED = 'refuse-i-parapheur',

  VALIDATING = 'en-cours-de-validation',
  VALIDATED = 'valide',
  I_PARAPHEUR_SIGNING = 'en-cours-signature-i-parapheur',
  I_PARAPHEUR_SENDING = 'en-cours-envoi-i-parapheur',
  I_PARAPHEUR_ERROR = 'erreur-i-parapheur',

  DECLARED_SIGNED = 'declare-signe',
  I_PARAPHEUR_SIGNED = 'signe-i-parapheur',
  BEING_SENT_TO_TDT = 'en-cours-depot-tdt',

  TO_BE_TRANSMITTED = 'a-teletransmettre',

  TRANSMISSION_READY = 'a-ordonner',
  TRANSMITTING = 'en-cours-teletransmission',
  TRANSMITTING_ERROR = 'erreur-tdt',

  SETTLED = 'acquitte',
  CANCELLED = 'annule',
  CANCELLING = 'annulation-en-cours',

  VOTED_APPROVED = 'vote-et-approuve',
  VOTED_REFUSED = 'vote-et-rejete',
  TAKE_NOTE = 'prendre-acte',
}
