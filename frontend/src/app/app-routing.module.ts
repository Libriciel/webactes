import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from './wa-common/routing-paths';
import { RgpdPageComponent } from './wa-common/components/rgpd-page/rgpd-page.component';
import { WACommonMessages } from './wa-common/i18n/wa-common-messages';

const routes: Routes = [
  {
    path: '',
    redirectTo: RoutingPaths.PROJECTS_DRAFTS_PATH,
    pathMatch: 'full'
  },
  {
    path: RoutingPaths.RGPD_PAGE_PATH,
    component: RgpdPageComponent,
    data: {
      pageParameters: {
        title: WACommonMessages.PAGE_RGPD_TITLE,
        breadcrumb: [
          {
            name: WACommonMessages.PAGE_RGPD_TITLE,
            location: RoutingPaths.RGPD_PAGE_PATH
          }
        ]
      }
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
