import { Component, OnInit } from '@angular/core';
import { MenuItem } from './ls-common/model/menu-item';
import { RoutingPaths } from './wa-common/routing-paths';
import { AppMessages } from './app-messages';
import { IManageStructureInformationService } from './admin/organization/services/i-manage-structure-information-service';
import { STRUCTURE_USER_INFORMATION } from './wa-common/variables/structure-user-info';
import { UserRole } from './wa-common/rights/user-role.enum';
import { default as ApplicationVersion } from '../../application.json';
import { Version } from './wa-common/model/version';
import { StructureSettingsService } from './core';

@Component({
  selector: 'wa-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  projectMenuAdmin = {
    name: AppMessages.MENU_PROJECTS_NAME,
    ref: RoutingPaths.PROJECTS_DRAFTS_PATH,
    icon: AppMessages.MENU_PROJECTS_ICON,
    subItems: [{
      name: AppMessages.MENU_PROJECTS_DRAFT_NAME,
      ref: RoutingPaths.PROJECTS_DRAFTS_PATH
    }
    ]
  };
  projectMenuNonAdmin = {
    name: AppMessages.MENU_PROJECTS_NAME,
    ref: RoutingPaths.PROJECTS_DRAFTS_PATH,
    icon: AppMessages.MENU_PROJECTS_ICON,
    subItems: [{
      name: AppMessages.MENU_PROJECTS_DRAFT_NAME,
      ref: RoutingPaths.PROJECTS_DRAFTS_PATH
    }]
  };
  sittingMenu = {
    name: AppMessages.MENU_SITTINGS_NAME,
    ref: RoutingPaths.SITTING_RUNNING_PATH,
    icon: AppMessages.MENU_SITTINGS_ICON,
    subItems: [{
      name: AppMessages.MENU_SITTINGS_RUNNING_NAME,
      ref: RoutingPaths.SITTING_RUNNING_PATH
    }, {
      name: AppMessages.MENU_SITTINGS_CLOSED_NAME,
      ref: RoutingPaths.SITTING_CLOSED_PATH
    }]
  };
  teletransmissionMenu = {
    name: AppMessages.MENU_TELETRANSMISSION_NAME,
    ref: RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_TDT_PATH,
    icon: AppMessages.MENU_TELETRANSMISSION_ICON,
    subItems: [{
      name: AppMessages.MENU_TELETRANSMISSION_TO_BE_SENT_NAME,
      ref: RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_TDT_PATH
    }, {
      name: AppMessages.MENU_TELETRANSMISSION_READY_FOR_TELETRANSMISSION_NAME,
      ref: RoutingPaths.TELETRANSMISSION_TO_BE_SEND_TO_PREFECTURE_PATH
    }]
  };
  actesMenu = {
    name: AppMessages.MENU_ACTS_NAME,
    ref: RoutingPaths.ACTS_PATH_LIST_PATH,
    icon: AppMessages.MENU_ACTS_ICON,
    subItems: [{
      name: AppMessages.MENU_ACTS_NAME_LIST_NAME,
      ref: RoutingPaths.ACTS_PATH_LIST_PATH
    }]
  };
  menu: MenuItem[] = [];
  structureName: string = null;
  spinner: string = null;
  logo: ArrayBuffer | string = null;
  protected applicationVersion: Version = new Version(ApplicationVersion);

  constructor(
    protected structureSettingsService: StructureSettingsService,
    protected manageStructureInformationService: IManageStructureInformationService
  ) {
    const role = STRUCTURE_USER_INFORMATION.userInformation.role.name;

    switch (role) {
      case UserRole.SUPER_ADMIN:
      case UserRole.ADMIN:
      case UserRole.ADMIN_FONCTIONNEL:
        if (this.structureSettingsService.getConfig('project', 'project_workflow')) {
          this.projectMenuAdmin.subItems = this.projectMenuAdmin.subItems.concat({
            name: AppMessages.MENU_PROJECTS_VALIDATING_NAME,
            ref: RoutingPaths.PROJECTS_VALIDATING_PATH
          }, {
            name: AppMessages.MENU_PROJECTS_TO_BE_VALIDATED_NAME,
            ref: RoutingPaths.PROJECTS_TO_BE_VALIDATED_PATH
          });
        }
        this.projectMenuAdmin.subItems = this.projectMenuAdmin.subItems.concat({
          name: AppMessages.MENU_PROJECTS_VALIDATED_NAME,
          ref: RoutingPaths.PROJECTS_VALIDATED_PATH
        });
        if (structureSettingsService.getConfig('sitting', 'sitting_enable')) {
          this.projectMenuAdmin.subItems = this.projectMenuAdmin.subItems.concat([{
            name: AppMessages.MENU_SITTINGS_PROJECTS_NAME,
            ref: RoutingPaths.PROJECTS_WITHOUT_SITTING_PATH
          }]);
        }
        this.menu = [this.projectMenuAdmin];
        if (structureSettingsService.getConfig('sitting', 'sitting_enable')) {
          this.menu = this.menu.concat([this.sittingMenu]);
        }
        this.menu = this.menu.concat([this.teletransmissionMenu, this.actesMenu]);
        break;
      case UserRole.SECRETARIAT_GENERAL:
        this.notAdminProjectsMenu();
        this.menu = [this.projectMenuNonAdmin].concat([this.sittingMenu, this.teletransmissionMenu, this.actesMenu]);
        break;
      default:
        this.notAdminProjectsMenu();
        this.menu = [this.projectMenuNonAdmin];
        break;
    }
  }

  notAdminProjectsMenu() {
    if (this.structureSettingsService.getConfig('project', 'project_workflow')) {
      this.projectMenuNonAdmin.subItems = this.projectMenuNonAdmin.subItems.concat({
        name: AppMessages.MENU_PROJECTS_VALIDATING_NAME,
        ref: RoutingPaths.PROJECTS_VALIDATING_PATH
      }, {
        name: AppMessages.MENU_PROJECTS_TO_BE_VALIDATED_NAME,
        ref: RoutingPaths.PROJECTS_TO_BE_VALIDATED_PATH
      });
    }
    this.projectMenuNonAdmin.subItems = this.projectMenuNonAdmin.subItems.concat({
      name: AppMessages.MENU_PROJECTS_VALIDATED_NAME,
      ref: RoutingPaths.PROJECTS_VALIDATED_PATH
    });

    if (this.structureSettingsService.getConfig('sitting', 'sitting_enable')) {
      this.projectMenuNonAdmin.subItems = this.projectMenuNonAdmin.subItems.concat([{
        name: AppMessages.MENU_SITTINGS_PROJECTS_NAME,
        ref: RoutingPaths.PROJECTS_WITHOUT_SITTING_PATH
      }]);
    }
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => this.logo = reader.result, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }

  ngOnInit() {
    this.manageStructureInformationService.getStructureLogo()
      .subscribe(logo => {
        if (logo instanceof Blob) {
          this.createImageFromBlob(logo);
        } else {
          this.logo = logo;
        }
      });

    this.manageStructureInformationService.getCustomName()
      .subscribe(name => this.structureName = name);

    this.manageStructureInformationService.getSpinner()
      .subscribe(spinner => this.spinner = spinner);

  }

  getApplicationName(): string {
    return this.applicationVersion.getFullName();
  }
}
