import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutRoutingModule } from './routing/about-routing/about-routing.module';
import { ApplicationAboutPageComponent } from './components/application-about-page/application-about-page.component';
import { WaCommonModule } from '../wa-common/wa-common.module';

@NgModule({
  declarations: [ApplicationAboutPageComponent],
  imports: [
    CommonModule,
    AboutRoutingModule,
    WaCommonModule
  ]
})
export class AboutModule {
}
