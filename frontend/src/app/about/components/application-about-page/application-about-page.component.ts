import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'wa-application-about-page',
  templateUrl: './application-about-page.component.html',
  styleUrls: ['./application-about-page.component.scss']
})
export class ApplicationAboutPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
