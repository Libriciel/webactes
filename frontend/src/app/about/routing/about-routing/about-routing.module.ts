import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoutingPaths } from '../../../wa-common/routing-paths';
import { PageParameters } from '../../../wa-common/components/page/model/page-parameters';
import { AboutMessages } from '../../i18n/about-messages';
import { ApplicationAboutPageComponent } from '../../components/application-about-page/application-about-page.component';

const routes: Routes = [{
  path: RoutingPaths.ABOUT_PATH,
  component: ApplicationAboutPageComponent,
  data: {
    pageParameters: {
      title: AboutMessages.PAGE_ABOUT_TITLE,
      breadcrumb: [
        {name: AboutMessages.PAGE_ABOUT_TITLE, location: RoutingPaths.ABOUT_PATH}
      ]
    } as PageParameters
  }
}];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class AboutRoutingModule {
}
