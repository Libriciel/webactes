import { Injectable, Injector } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
// Cool library to deal with errors: https://www.stacktracejs.com
import { LogService } from '../ls-common/services/http-services/log-service.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorsService {

  constructor(
    protected injector: Injector,
    protected logService: LogService,
  ) {
  }

  log(error) {
    // Log the error to the console
    console.error(error);
    // Send error to server
    const errorToSend = this.addContextInfo(error);
    return this.logService.send(errorToSend);
  }

  addContextInfo(error) {
    // All the context details that you want (usually coming from other services; Constants, UserService...)
    const name = error.name || null;
    const appId = 'webAct';
    const user = 'CollectivityName';
    const time = new Date().getTime();
    const id = `${appId}-${user}-${time}`;
    const location = this.injector.get(LocationStrategy);
    const url = location instanceof PathLocationStrategy ? location.path() : '';
    const status = error.status || null;
    const message = error.message || error.toString();
    const stack = error instanceof HttpErrorResponse ? null : error;

    return {name, appId, user, time, id, url, status, message, stack};
  }

}
