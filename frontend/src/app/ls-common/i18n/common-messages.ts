import { FileUtils } from '../../utility/Files/file-utils';

export class CommonMessages {
  static readonly CLOSE = 'Fermer';
  static readonly CANCEL = 'Annuler';
  static readonly GO_BACK = 'Retour';
  static readonly SAVE = 'Enregistrer';
  static readonly MODIFY = 'Modifier';
  static readonly ADD = 'Ajouter';
  static readonly YES = 'Oui';
  static readonly NO = 'Non';
  static readonly DELETE = 'Supprimer';
  static readonly CONFIRM = 'Confirmer';
  static readonly ACTIVATE_DEACTIVATE = 'activer / désactiver';
  static readonly ACTIVATE_ALL = 'Tout activer';
  static readonly DEACTIVATE_ALL = 'Tout désactiver';
  static readonly CLEAR_ALL = 'Tout effacer';
  static readonly SEARCH = 'Rechercher';
  static readonly REINIT = 'Réinitialiser';
  static readonly REQUIRED_FIELD = 'Champ obligatoire';
  static readonly HTTPS_PROTOCOL = 'https://';
  // To be used in table headers to avoid extra space for firefox engine
  static readonly NOT_BREAKABLE_SPACE = '&nbsp;';

  static readonly EMPTY_TABLE_MESSAGE = 'Aucune information à afficher.';
  static readonly NO_RESULTS = 'Aucun résultat.';

  static readonly DOCUMENT_DOWNLOAD_TITLE = '';

  static readonly SENDING_STATUS = 'Statut d\'envoi';
  static readonly READING_STATUS = 'Statut de lecture';
  static readonly ACTORS = 'Acteurs';

  static readonly GLOBAL_SPINNER_TEXT = 'Génération en cours';

  static getHumanlyReadableSizeInMo(size: number): string {
    return FileUtils.getHumanlyReadableSizeInMo(size);
  }
}
