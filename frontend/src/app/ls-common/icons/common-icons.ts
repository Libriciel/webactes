import { CommonStylesConstants } from '../../wa-common/common-styles-constants';
import { Style } from '../../wa-common';

export class CommonIcons {
  static SAVE_ICON = 'far fa-save';
  static SHOW_ICON = 'far fa-eye';
  static GO_BACK_ICON = 'fas fa-arrow-left';
  static SPINNER_ICON = 'fas fa-circle-notch fa-spin';
  static ADD_ICON = 'fas fa-plus-circle';
  static CREATE_ICON = 'fas fa-plus';
  static INFO_ICON = 'fas fa-info';
  static WARNING_ICON = 'fas fa-exclamation-triangle';
  static TOOLTIP_ICON = 'fas fa-info-circle';
  static DELETE_ICON = 'fas fa-trash-alt';
  static REMOVE_ICON = 'fas fa-times-circle';
  static CLOSE_ICON = 'fas fa-times';
  static MORE_ACTIONS_ICON = 'fas fa-ellipsis-v';
  static INVALID_FORM_ICON = `fas fa-exclamation-circle ${CommonStylesConstants.getStyleClass(Style.DANGER)}`;
  static DEFAULT_MENU_ICON = 'fas fa-folder-open';
  static SEARCH_ICON = 'fas fa-search';
  static CALENDAR_ICON = 'far fa-calendar-alt';
  static VALIDATED_ICON = 'fas fa-check';
  static REJECTED_ICON = 'fas fa-times';
  static EDIT_ICON = 'fas fa-pencil-alt';
  static REPLACE_ICON = 'fas fa-sync-alt';
  static PREVIOUS_ICON = 'fas fa-arrow-left';
  static NEXT_ICON = 'fas fa-arrow-right';
  static CLOSE_ACCORDION_ICON = 'fas fa-minus-square';
  static EXPAND_ACCORDION_ICON = 'fas fa-plus-square';
  static SIGNED_ACTS_ICON = 'fas fa-university';
  static RESET_ICON = 'fas fa-sync-alt';
  static DOWNLOAD_DOCUMENT_ICON = 'fas fa-download';
  static SCHEDULE_ICON = 'fa fa-list-ol';
  static DOCUMENT_DOWNLOAD_ICON =  'fas fa-download';
  static GET_FROM = 'fas fa-sync-alt';
  static REORDER_LINES_ICON = 'fa fa-bars';
  static VOTE_ICON = 'fas fa-gavel';
}
