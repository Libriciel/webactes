import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/model/user/user';
import { ICreateStepService } from '../../admin/circuits/services/ICreateStepService';
import { IManageUserService } from '../../admin/users/i-manage-user';

@Injectable({
  providedIn: 'root'
})
export class CreateStepService implements ICreateStepService {

  constructor(protected userService: IManageUserService) {
  }

  getValidators(): Observable<{ users: User[] }> {
    return this.userService.getAllActiveUsers();
  }
}
