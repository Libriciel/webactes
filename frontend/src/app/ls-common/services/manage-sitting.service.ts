import { Injectable } from '@angular/core';
import { SittingService } from './http-services/sittings/sitting.service';
import { IManageSittingService } from '../../sittings/services/iManageSittingService';
import { Sitting } from '../../model/Sitting/sitting';
import { Observable } from 'rxjs';
import { SittingState } from '../../model/Sitting/sitting-state.enum';
import { Pagination } from '../../model/pagination';
import { FilterType } from '../../model/util/filter-type';
import { map } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { DocumentService } from './http-services/document/document.service';
import { Vote } from '../../model/vote/vote';
import { TypesAct } from '../../model/type-act/types-act';
import { SittingOverview } from '../../model/Sitting/sitting-overview';

@Injectable({
  providedIn: 'root'
})
export class ManageSittingService implements IManageSittingService {

  constructor(
    protected sittingService: SittingService,
    protected documentService: DocumentService,
  ) {
  }

  getAllSittings(typeAct: TypesAct): Observable<SittingOverview[]> {
    return this.sittingService.getAllSittings()
      .pipe(
        map(sittings =>
          sittings
            .filter(sitting => sitting.typesitting.typeActs
              .filter(typeActSitting => typeActSitting.id === typeAct.id)
              .length > 0)),
        map(sittings => sittings.map(sitting => new SittingOverview(sitting)))
      );
  }

  getSittings(sittingState: SittingState, pageNumber: number, limit: number, filters?: FilterType)
    : Observable<{ sittings: Sitting[], pagination: Pagination }> {
    return this.sittingService.getSittings(sittingState, pageNumber, limit, filters);
  }

  close(sittings: Sitting[]) {
    return this.sittingService.close(sittings);
  }

  deleteSittings(sittings: Sitting[]) {
    return this.sittingService.deleteSittings(sittings);
  }

  get(id: string): Observable<{ sitting: Sitting, pagination: Pagination }> {
    return this.sittingService.get(id);
  }

  getWithProjectVotes(id: number, projectId: number): Observable<{ sitting: Sitting, vote: Vote }> {
    return this.sittingService.getWithProjectVotes(id, projectId);
  }

  private downloadFileFromResponse(response: any, defaultFileName: string): void {
    const contentDispositionHeader = response.headers.get('X-Custom-Filename');
    let extractedFileName = defaultFileName; // Default filename if not found in headers
    if (contentDispositionHeader) {
      extractedFileName = response.headers.get('X-Custom-Filename');
    }

    const blobPart = new Blob([response.body], {type: response.body.type});

    // Trigger file download
    saveAs(blobPart, decodeURIComponent(extractedFileName));
  }

  getDownloadDeliberationsList(sittingId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getDownloadDeliberationsList(sittingId).pipe(
      map(response => {
        this.downloadFileFromResponse(response, 'Liste_des_délibérations.pdf');
      })
    );
  }

  getDownloadVerbalTrial(sittingId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getDownloadVerbalTrial(sittingId).pipe(
      map(response => {
        this.downloadFileFromResponse(response, 'PV_détaillé.pdf');
      })
    );
  }

  generateSittingDocuments(summonId: number): Observable<any> {
    return this.documentService.generateSittingDocument(summonId);
  }

}
