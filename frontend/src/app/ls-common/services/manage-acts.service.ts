import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { IManageActsService } from '../../acts/services/imanage-acts-service';
import { Project } from '../../model/project/project';
import { ActService } from './http-services/act-service';
import { Observable, throwError } from 'rxjs';
import { Config } from 'src/app/Config';
import { TdtReturnType } from './http-services/tdt-return-type';
import { RoutingPaths } from 'src/app/wa-common/routing-paths';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ManageActsService implements IManageActsService {

  private MAX_BUNDLE_SIZE = 150000000;

  constructor(
    protected actService: ActService,
    protected ngLocation: Location,
  ) {
  }

  isCodeActOK(project: Project): boolean {
    return project.codeAct !== null && project.codeAct !== '';
  }

  isClassificationOK(project: Project): boolean {
    return !!project.classification_id;
  }

  isMainDocOk(project: Project): boolean {
    return (project.mainDocument && project.mainDocument.documentType !== null);
  }

  isAnnexesOK(project: Project): boolean {
    return project.annexes.every(annexe => {
      return (annexe.codeType !== null && annexe.toGotTransfer) || !annexe.toGotTransfer;
    });
  }

  isTotalSizeOk(project: Project): boolean {
    if (!project.mainDocument) {
      return false;
    }
    let size = project.mainDocument.size;
    project.annexes.forEach(item => size += item.size);

    return size < this.MAX_BUNDLE_SIZE;
  }

  isTdtOk(project: Project): boolean {
    return this.isCodeActOK(project) &&
      this.isClassificationOK(project) &&
      this.isMainDocOk(project) &&
      this.isAnnexesOK(project) &&
      this.isTotalSizeOk(project);
  }

  sendActToTdt(project: Project): Observable<TdtReturnType> {
    return this.actService.sendActToTdt(project);
  }

  orderTeletransmission(project: Project, callbackUrl: string): Observable<any> {
    // get the URL, from backend or directly on project?
    return this.actService.getTdtOrderUrl(project).pipe(
      tap(result => {
        if (result && result.url) {
          this.redirectToRGSAuthentication(result.url, callbackUrl);
        } else {
          console.warn('Invalid result from getTdtOrderUrl : ', result);
        }
      }),
      catchError(error => {
        console.warn('Error calling getTdtOrderUrl : ', error);
        return throwError('Error calling getTdtOrderUrl');
      })
    );
  }

  teletransmissionWasOrderedCorrectly(projectId: number): Observable<any> {
    return this.actService.teletransmissionWasOrderedCorrectly(projectId);
  }

  redirectToRGSAuthentication(authenticationUrl: string, callbackUrl: string) {
    window.location.href = `${authenticationUrl}&url_return=${callbackUrl}`;
  }

  buildCallbackUrlForPath(projectId: number, targetPath: string): string {
    const pathAndParameters = RoutingPaths.TDT_AUTHENTICATION_RESPONSE_PATH
      + '?' + 'targetRoute=' + targetPath
      + '%26' + `projectId=${projectId}`
      + '%26' + 'error=%%ERROR%%';
    // + '%26' + 'errorMessage=%%MESSAGE%%'

    return window.location.protocol
      + '//' + window.location.hostname
      + this.ngLocation.prepareExternalUrl(Config.baseUrl + pathAndParameters);
  }

}
