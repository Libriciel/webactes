import { Injectable } from '@angular/core';
import { Sitting } from '../../model/Sitting/sitting';
import { IManageContainersSittingService } from '../../sittings/services/iManageContainersSittingService';
import { ContainersSittingsService } from './http-services/sittings/containers-sittings.service';
import { ContainerSitting } from '../../model/Sitting/container-sitting';
import { Observable } from 'rxjs';
import { Project } from '../../model/project/project';

@Injectable({
  providedIn: 'root'
})
export class ManageContainerSittingService implements IManageContainersSittingService {

  constructor(
    protected containerSittingService: ContainersSittingsService,
  ) {
  }

  delete(sittings: Sitting[]) {
    return this.containerSittingService.delete(sittings);
  }

  getAllProjectsBySittingId(sittingId: string): Observable<Project[]> {
    return this.containerSittingService.getAllProjectsBySittingId(sittingId);
  }

  getById(id: string) {
    return this.containerSittingService.getById(id);
  }

  edit(containerSittings: ContainerSitting[]) {

  }

}
