import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { Config } from '../../Config';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(protected toastr: ToastrService) {
  }

  showSuccess(message: string): void {
    this.toastr.success(message, null, {
      closeButton: true, timeOut: Config.TOASTER_TIMEOUT
    });
  }

  showError(message: string, title?: string, technicalMessage?: any): void {
    if (technicalMessage instanceof HttpErrorResponse) {
      console.error(`Error from server`);
      console.table([{
        code: technicalMessage.error.code,
        message: technicalMessage.error.message,
        url: technicalMessage.error.url
      }]);
    }
    if (technicalMessage) {
      console.error(technicalMessage);
    }
    this.toastr.error(message, title, {
      closeButton: true, timeOut: Config.TOASTER_TIMEOUT, enableHtml: true
    });
  }
}
