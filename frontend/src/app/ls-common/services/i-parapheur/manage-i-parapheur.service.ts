import { Injectable } from '@angular/core';
import { IIParapheurService } from '../../../projects/services/i-iparapheur-service';
import { Project } from '../../../model/project/project';
import { Observable } from 'rxjs';
import { IParapheurService } from '../http-services/i-parapheur/iparapheur.service';
import { IParapheurCircuit } from '../../../model/iparapheur/iparapheur-circuit';

@Injectable({
  providedIn: 'root'
})
export class ManageIParapheurService implements IIParapheurService {

  constructor(protected parapheurService: IParapheurService) {
  }

  getIParapheurCircuits(project: Project): Observable<IParapheurCircuit[] | { error: string }> {
    return this.parapheurService.getIParapheurCircuits(project);
  }

  sendToIParapheur(project: Project, iparapheurCircuitName: string): Observable<any> {
    return this.parapheurService.sendToIParapheur(project, iparapheurCircuitName);
  }

  getIParapheurStatus(project: Project): Observable<{ status: string, annotation: string }> {
    return this.parapheurService.getIParapheurStatus(project);
  }
}
