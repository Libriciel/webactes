import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IManageConnexionIdelibreService } from '../../admin/connexionIdelibre/services/imanage-connexion-idelibre-service';
import { map } from 'rxjs/operators';
import { ConnexionIdelibreService } from './http-services/connexionIdelibre/connexion-idelibre.service';
import { ConnexionIdelibre } from '../../admin/connexionIdelibre/model/connexion-idelibre';

@Injectable({
  providedIn: 'root'
})
export class ManageConnexionIdelibreService implements IManageConnexionIdelibreService {

  constructor(protected idelibreService: ConnexionIdelibreService) {
  }

  checkIdelibreConnexion(connexionIdelibre: ConnexionIdelibre): Observable<boolean> {
    return this.idelibreService.checkIdelibreConnexion(connexionIdelibre).pipe(map(value => value.success));
  }

  getIdelibreConnexion(): Observable<ConnexionIdelibre> {
    return this.idelibreService.getIdelibreConnexion();
  }

  saveIdelibreConnexion(connexionIdelibre: ConnexionIdelibre): Observable<any> {
    return this.idelibreService.saveIdelibreConnexion(connexionIdelibre);
  }

}
