import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { TypeSitting } from '../../model/type-sitting/type-sitting';
import { Sitting } from '../../model/Sitting/sitting';
import { SittingService } from './http-services/sittings/sitting.service';
import { ICreateSittingService } from '../../sittings/services/iCreateSittingService';
import { TypeSittingService } from './http-services/type-sittings/type-sitting.service';
import { FilterType } from '../../model/util/filter-type';

@Injectable({
  providedIn: 'root'
})
export class CreateSittingService implements ICreateSittingService {

  constructor(
    protected sittingService: SittingService,
    protected typeSittingService: TypeSittingService,
  ) {
  }

  getAllSittings(): Observable<Sitting[]> {
    return this.sittingService.getAllSittings()
      .pipe(map(sittings => sittings),
        catchError(err => throwError(err)));
  }

  addSitting(sitting: Sitting): Observable<Sitting> {
    return this.sittingService.add(sitting).pipe(
      map(new_sitting => new_sitting.sitting)
    );
  }

  getAllTypes(filters?: FilterType, limit?: number): Observable<TypeSitting[]> {
    return this.typeSittingService.getAll(1, filters, limit)
      .pipe(map((typeSittings: any) => typeSittings.typeSittings));
  }

  updateSitting(sitting: Sitting): Observable<Sitting> {
    return this.sittingService.update(sitting).pipe(
      mergeMap(() => this.getAllSittings()
        .pipe(map(sittings => {
          const sits = sittings.filter(sit => sit.id === sitting.id);
          return sits.length > 0 ? sits[0] : null;
        }))),
    );
  }

}
