import { IManageStructureInformationService } from '../../admin/organization/services/i-manage-structure-information-service';
import { BehaviorSubject, Observable } from 'rxjs';
import { StructureInformationService } from './http-services/structure-information-service';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { STRUCTURE_USER_INFORMATION } from '../../wa-common/variables/structure-user-info';
import { FilterType } from '../../model/util/filter-type';
import { Pagination } from '../../model/pagination';
import { StructureInfo } from '../../model/structure-info';
import { ApiService } from '../../core';

@Injectable({
  providedIn: 'root'
})
export class ManageStructureInformationService implements IManageStructureInformationService {

  private logo = new BehaviorSubject<Blob | string>(null);
  private customName = new BehaviorSubject<string>(STRUCTURE_USER_INFORMATION.structureInformation.customName);
  private spinner = new BehaviorSubject<string>(STRUCTURE_USER_INFORMATION.structureInformation.spinner);

  constructor(protected structureInformationService: StructureInformationService, protected apiService: ApiService) {
  }

  saveOrUpdate(file: File, name: string, spinner: string): Observable<Object> {
    return this.structureInformationService.saveOrUpdate(file, name, spinner)
      .pipe(tap(() => {
        if (file) {
          this.logo.next(file);
        } else {
          this.logo.next('');
        }
        if (name) {
          this.customName.next(name);
          STRUCTURE_USER_INFORMATION.structureInformation.customName = name;
        }
        if (spinner) {
          this.spinner.next(spinner);
          STRUCTURE_USER_INFORMATION.structureInformation.spinner = spinner;
        }
      }));
  }

  getStructureLogo(): Observable<Blob | string> {
    if (!this.logo.getValue()) {
      this.structureInformationService.getLogo().subscribe(logo => this.logo.next(logo));
    }
    return this.logo;
  }

  getCustomName(): Observable<string> {
    return this.customName;
  }

  getSpinner(): Observable<string> {
    return this.spinner;
  }

  getAllWithPagination(numPage?: number, filters?: FilterType, limit?: number): Observable<{ structures: StructureInfo[], pagination: Pagination }> {
    return this.structureInformationService.getAllWithPagination(numPage, filters, limit);
  }

  getAllWithoutPagination(): Observable<{ structures: StructureInfo[] }> {
    return this.structureInformationService.getAllWithoutPagination();
  }

  deleteLogo(): Observable<any> {
    return this.structureInformationService.deleteLogo();
  }
}
