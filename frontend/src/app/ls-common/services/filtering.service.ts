import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pagination } from 'src/app/model/pagination';
import { FilterType } from 'src/app/model/util/filter-type';
import { map } from 'rxjs/operators';
import { Circuit } from 'src/app/model/circuit/circuit';
import { Sitting } from 'src/app/model/Sitting/sitting';

@Injectable({
  providedIn: 'root'
})
export class FilteringService {

  constructor() {
  }

  applyCircuitsFilter(getCircuitsAction$: Observable<{ circuits: Circuit[], pagination: Pagination }>, filters: FilterType):
    Observable<{ circuits: Circuit[], pagination: Pagination }> {
    let filteredResult = getCircuitsAction$;
    if (filters.searchedText) {
      filteredResult = filteredResult.pipe(
        map((circuitsData: { circuits: Circuit[], pagination: Pagination }) => ({
          circuits: this.filterCircuitsOnAllFields(circuitsData.circuits, filters.searchedText),
          pagination: circuitsData.pagination
        }))
      );
    }
    return filteredResult;
  }

  filterCircuitsOnAllFields(circuits: Circuit[], searchedText: string): Circuit[] {
    searchedText = searchedText.toLowerCase();
    return circuits.filter(circuit =>
      circuit.name && circuit.name.toLowerCase().search(searchedText) !== -1
      || circuit.description && circuit.description.search(searchedText) !== -1);
  }

  applySittingsFilter(getSittingsAction$: Observable<{ sittings: Sitting[], pagination: Pagination }>, filters: FilterType):
    Observable<{ sittings: Sitting[], pagination: Pagination }> {
    let filteredResult = getSittingsAction$;
    if (filters.searchedText) {
      filteredResult = filteredResult.pipe(
        map((sittingsData: { sittings: Sitting[], pagination: Pagination }) => {
          return {
            sittings: this.filterSittingsOnAllFields(sittingsData.sittings, filters.searchedText),
            pagination: sittingsData.pagination
          };
        })
      );
    }
    return filteredResult;
  }

  filterSittingsOnAllFields(sittings: Sitting[], searchedText: string): Sitting[] {
    searchedText = searchedText.toLowerCase();
    return sittings.filter(sitting =>
      sitting.typesitting && sitting.typesitting.name
      && sitting.typesitting.name.toLowerCase().search(searchedText) !== -1);
  }

}
