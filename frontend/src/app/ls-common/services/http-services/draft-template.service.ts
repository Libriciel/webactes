import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../../Config';
import { DraftTemplate } from '../../../model/draft-template/draft-template';
import { DraftTemplateRaw } from '../../../model/draft-template/draftTemplateRaw';

@Injectable({
  providedIn: 'root'
})
export class DraftTemplateService {

  private draftTemplateUrl = Config.baseUrl + '/api/v1/draft-templates';

  constructor(protected http: HttpClient) {
  }

  getAllWithPagination(): Observable<DraftTemplate[]> {
    return this.http.get<{ draftTemplates: DraftTemplateRaw[] }>(this.draftTemplateUrl, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map((draftTemplates: { draftTemplates: DraftTemplateRaw[] }) => draftTemplates.draftTemplates
        .map(draftTemplateRaw => {
          return new DraftTemplate(draftTemplateRaw);
        })
      ));
  }

  getAllWithoutPagination(): Observable<DraftTemplate[]> {
    return this.http.get<{ draftTemplates: DraftTemplateRaw[] }>(this.draftTemplateUrl + '?paginate=false', {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map((draftTemplates: { draftTemplates: DraftTemplateRaw[] }) => draftTemplates.draftTemplates
        .map(draftTemplateRaw => {
          return new DraftTemplate(draftTemplateRaw);
        })
      ));
  }
}
