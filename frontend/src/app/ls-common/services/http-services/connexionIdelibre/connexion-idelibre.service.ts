import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../../../../Config';
import { Observable } from 'rxjs';
import { ConnexionIdelibre } from '../../../../admin/connexionIdelibre/model/connexion-idelibre';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConnexionIdelibreService {

  private static connexionIdelibreUrl = Config.baseUrl + '/api/v1/structures';
  private static connexionIdelibreCheckConnexionUrl = ConnexionIdelibreService.connexionIdelibreUrl
    + '/checkIdelibreConnexion';
  private static connexionIdelibreGetConnexionUrl = ConnexionIdelibreService.connexionIdelibreUrl
    + '/getIdelibreConnexion';
  private static saveIdelibreConnexionUrl = ConnexionIdelibreService.connexionIdelibreUrl
    + '/saveIdelibreConnexion';

  constructor(protected http: HttpClient) {
  }

  checkIdelibreConnexion(connexionIdelibre: ConnexionIdelibre): Observable<{ success: boolean }> {
    return this.http.post<{ success: boolean }>(ConnexionIdelibreService.connexionIdelibreCheckConnexionUrl, connexionIdelibre, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  getIdelibreConnexion(): Observable<ConnexionIdelibre> {
    return this.http.get<{ idelibre: ConnexionIdelibre }>(ConnexionIdelibreService.connexionIdelibreGetConnexionUrl, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map(connexionIdelibre => {
      return new ConnexionIdelibre({
        username: connexionIdelibre.idelibre.username,
        password: connexionIdelibre.idelibre.password,
        url: connexionIdelibre.idelibre.url,
        connexion_option: connexionIdelibre.idelibre.connexion_option,
      });
    }));
  }

  saveIdelibreConnexion(connexionIdelibre: ConnexionIdelibre): Observable<any> {
    return this.http.post<{ idelibre: ConnexionIdelibre }>(ConnexionIdelibreService.saveIdelibreConnexionUrl, connexionIdelibre, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }
}
