import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Circuit } from 'src/app/model/circuit/circuit';
import { Step } from 'src/app/model/circuit/step';
import { BackendStep } from 'src/app/model/backend/backend-step';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BackendCircuit } from 'src/app/model/backend/backend-circuit';
import { map } from 'rxjs/operators';
import { Config } from 'src/app/Config';
import { Pagination } from 'src/app/model/pagination';
import { User } from 'src/app/model/user/user';
import { STEP_TYPE_LIST } from 'src/app/model/circuit/step-type';
import { CircuitRight } from '../../../model/circuit/circuit-right.enum';
import { FilterType } from '../../../model/util/filter-type';

export class StepRaw {
  name: string;
  validators: User[] = [];
}

export class CircuitRaw {
  id: number;
  name: string;
  description?: string;
  active: boolean;
  steps: StepRaw[] = [];
  is_deletable: boolean;
  is_editable: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class CircuitService {

  private CIRCUITS_URL = Config.baseUrl + '/api/v1/circuits';
  private REQUEST_OPTIONS = {
    headers: new HttpHeaders({'Accept': 'application/json'})
  };
  private NB_MAX_ACTIVE_CIRCUIT = Config.MAX_RETRIEVABLE_ACTIVE_CIRCUITS;

  constructor(protected http: HttpClient) {
  }

  private static buildStepFromRaw(rawStep: StepRaw): Step {
    const step = new Step();
    step.name = rawStep.name;
    if (!rawStep.validators || rawStep.validators.length === 0) {
      step.type = STEP_TYPE_LIST[0];
      return step;
    }

    step.type = rawStep.validators.length === 1 ? STEP_TYPE_LIST[0] : STEP_TYPE_LIST[1];
    step.validators = [...rawStep.validators];
    return step;
  }

  getAllCircuits(numPage?: number, filters?: FilterType): Observable<{ circuits: Circuit[], pagination: Pagination }> {
    if (!numPage) {
      numPage = 1;
    }
    const url = `${this.CIRCUITS_URL}`;
    return this.http.get<{ circuits: CircuitRaw[], pagination: Pagination }>(
      url + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : ''),
      this.REQUEST_OPTIONS).pipe(
      map(result => {
        const mappedCircuitsList = result.circuits ? result.circuits.map(rawCircuit => this.buildCircuitFromRaw(rawCircuit)) : [];
        return {circuits: mappedCircuitsList, pagination: result.pagination};
      })
    );
  }

  getAllActiveCircuits(): Observable<{ circuits: Circuit[] }> {
    return this.http.get<{ circuits: CircuitRaw[] }>
    (`${this.CIRCUITS_URL}/active`, this.REQUEST_OPTIONS)
      .pipe(
        map(result => {
          const mappedCircuitsList = result.circuits ? result.circuits.map(rawCircuit => this.buildCircuitFromRaw(rawCircuit)) : [];
          return {circuits: mappedCircuitsList};
        })
      );
  }

  getCircuit(id: number): Observable<Circuit> {
    return this.http.get<{ circuit: CircuitRaw }>(`${this.CIRCUITS_URL}/${id}`, this.REQUEST_OPTIONS).pipe(
      map(result => this.buildCircuitFromRaw(result.circuit))
    );
  }

  addCircuit(circuit: Circuit): Observable<Circuit> {
    const circuitData = this.convertCircuitToBackendFormat(circuit);
    return this.http.post<Circuit>(`${this.CIRCUITS_URL}`, circuitData, this.REQUEST_OPTIONS);
  }

  updateCircuit(id: number, circuit: Circuit): Observable<Circuit> {
    const circuitData = this.convertCircuitToBackendFormat(circuit);
    return this.http.put<Circuit>(`${this.CIRCUITS_URL}/${id}`, circuitData, this.REQUEST_OPTIONS);
  }

  deleteCircuits(circuits: Circuit[]): Observable<boolean> {
    if (!circuits || circuits.length === 0) {
      return of(false);
    }
    const id = circuits[0].id;
    return this.http.delete<any>(`${this.CIRCUITS_URL}/${id}`, this.REQUEST_OPTIONS);
  }

  switchCircuitActiveState(id: number, active: boolean): Observable<{ success: boolean, circuit: Circuit }> {
    let url: string;
    if (active) {
      url = `${this.CIRCUITS_URL}/${id}/activate`;
    } else {
      url = `${this.CIRCUITS_URL}/${id}/deactivate`;
    }

    return this.http.post<{ success: boolean, circuit: CircuitRaw }>(url, {}, this.REQUEST_OPTIONS).pipe(
      map(
        (result: { success: boolean, circuit: CircuitRaw }) => {
          return {success: result.success, circuit: this.buildCircuitFromRaw(result.circuit)};
        })
    );
  }

  convertStepToBackendFormat(src: Step): BackendStep {
    const dst = new BackendStep();
    dst.name = src.name;
    dst.users = src.validators.map(user => user.id);
    dst.groups = '';
    return dst;
  }

  convertCircuitToBackendFormat(src: Circuit): BackendCircuit {
    const dst = new BackendCircuit();
    dst.name = src.name;
    dst.description = src.description;
    dst.steps = [];
    for (const step of src.steps) {
      dst.steps.push(this.convertStepToBackendFormat(step));
    }
    return dst;
  }

  private buildCircuitFromRaw(circuitRaw: CircuitRaw): Circuit {
    const circuit = new Circuit();
    circuit.id = circuitRaw.id;
    circuit.name = circuitRaw.name;
    circuit.description = circuitRaw.description;
    circuit.active = circuitRaw.active;
    circuit.steps = [];
    circuit.setAvailableAction(CircuitRight.isDeletable, circuitRaw.is_deletable);
    circuit.setAvailableAction(CircuitRight.isEditable, circuitRaw.is_editable);
    if (circuitRaw.steps) {
      circuitRaw.steps.forEach(rawStep => {
        circuit.steps.push(CircuitService.buildStepFromRaw(rawStep));
      });
    }

    return circuit;
  }
}
