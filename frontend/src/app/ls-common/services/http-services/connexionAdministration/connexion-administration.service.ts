import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../../../../Config';
import { Observable } from 'rxjs';
import { ConnexionPastell } from '../../../../admin/connexionPastell/model/connexion-pastell';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConnexionAdministrationService {

  private static connexionAdministrationUrl = Config.baseUrl + '/api/v1/structures';
  private static connexionAdministrationCheckConnexionUrl = ConnexionAdministrationService.connexionAdministrationUrl
    + '/checkPastellConnexion';
  private static connexionAdministrationGetConnexionUrl = ConnexionAdministrationService.connexionAdministrationUrl
    + '/getPastellConnexion';
  private static saveAdministrationConnexionUrl = ConnexionAdministrationService.connexionAdministrationUrl
    + '/savePastellConnexion';

  constructor(protected http: HttpClient) {
  }

  checkPastellConnexion(connexionPastell: ConnexionPastell): Observable<{ success: boolean }> {
    return this.http.post<{ success: boolean }>(ConnexionAdministrationService.connexionAdministrationCheckConnexionUrl, connexionPastell, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  getPastellConnexion(): Observable<ConnexionPastell> {
    return this.http.get<{ pastell: ConnexionPastell }>(ConnexionAdministrationService.connexionAdministrationGetConnexionUrl, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map(connexionPastell => {
      return new ConnexionPastell({
        username: connexionPastell.pastell.username,
        password: null,
        pastellUrl: connexionPastell.pastell.pastellUrl,
        pastellEntityId: connexionPastell.pastell.pastellEntityId,
        slowUrl: connexionPastell.pastell.slowUrl
      });
    }));
  }

  savePastellConnexion(connexionPastell: ConnexionPastell): Observable<any> {
    return this.http.post<{ pastell: ConnexionPastell }>(ConnexionAdministrationService.saveAdministrationConnexionUrl, connexionPastell, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }
}
