import { Injectable } from '@angular/core';
import { Config } from '../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ClassificationDate } from '../../../model/classification/classification-date';
import { ClassificationSubject } from '../../../model/classification/classification-subject';

@Injectable({
  providedIn: 'root'
})
export class ClassificationService {

  private classificationsUrl = `${Config.baseUrl}/api/v1/structures`;
  private classificationSubjectsUrl = `${Config.baseUrl}/api/v1/matieres`;
  private getClassificationSubjectsUrl = `${this.classificationSubjectsUrl}/getMatieresToStructure`;

  constructor(protected http: HttpClient) {
  }

  getAllClassification(): Observable<null> {
    return this.http.get<{ classifications: any }>(`${this.classificationsUrl}/updateClassification`, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map(classifications => classifications.classifications));
  }

  getClassificationDate(): Observable<ClassificationDate> {
    // noinspection SpellCheckingInspection
    return this.http.get<{ // noinspection SpellCheckingInspection
      dateclassification: Date
    }>(`${this.classificationsUrl}/getLastClassificationDate`, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map(dateclassification => ({date: dateclassification.dateclassification})));
  }

  getClassificationSubjects(): Observable<ClassificationSubject[]> {
    return this.http.get<{ matieres: any[] }>(`${this.getClassificationSubjectsUrl}`, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map(value => value.matieres
      .map(matiereRaw => new ClassificationSubject(matiereRaw))));
  }
}
