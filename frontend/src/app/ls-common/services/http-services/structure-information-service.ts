import { forkJoin, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Config } from '../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { FilterType } from '../../../model/util/filter-type';
import { Pagination } from '../../../model/pagination';
import { StructureRaw } from './info/structure-raw';
import { StructureInfo } from '../../../model/structure-info';

@Injectable({
  providedIn: 'root'
})
export class StructureInformationService {

  private structureInformationUrl = Config.baseUrl + '/api/v1/structures';

  constructor(protected http: HttpClient) {
  }

  saveOrUpdate(file: File, name: string, spinner: string): Observable<null> {

    let observableName: Observable<null> = of(null);
    if (name) {
      const formData: FormData = new FormData();
      formData.append('customname', name);
      observableName = this.http.post<any>(`${this.structureInformationUrl}/setCustomName`, formData, {
        headers: new HttpHeaders({'Accept': 'application/json'})
      });
    }

    let observableSpinner: Observable<null> = of(null);
    if (spinner) {
      const formData: FormData = new FormData();
      formData.append('spinner', spinner);
      observableSpinner = this.http.post<any>(`${this.structureInformationUrl}/setSpinner`, formData, {
        headers: new HttpHeaders({'Accept': 'application/json'})
      });
    }

    let observableLogo: Observable<null> = of(null);
    if (file) {
      const formData: FormData = new FormData();
      formData.append('logo', file, file.name);
      observableLogo = this.http.post<any>(`${this.structureInformationUrl}/setLogo`, formData, {
        headers: new HttpHeaders({'Accept': 'application/json'})
      });
    }
    return forkJoin(observableName, observableSpinner, observableLogo).pipe(map(() => null));
  }

  getLogo(): Observable<Blob | string> {
    return this.http.get(`${this.structureInformationUrl}/getLogo`, {responseType: 'blob'})
      .pipe(
        map(value => value),
        catchError(() => of('assets/images/wa_img_coll.png'))
      );
  }

  getAllWithPagination(numPage?: number, filters?: FilterType, limit?: number): Observable<{ structures: StructureInfo[], pagination: Pagination }> {
    return this.http.get<{ structures: StructureRaw[], pagination: Pagination }>(
      `${this.structureInformationUrl}` + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : '')
      + (filters && filters.active ? `&active=${filters.active}` : '')
      + (limit ? `&limit=${limit}` : `&limit=10`),
      { headers: new HttpHeaders({'Accept': 'application/json'})}).pipe(
        map( rawData => ({
          structures: rawData.structures.map(structureRaw => new StructureInfo(structureRaw)),
          pagination: rawData.pagination
        })
    ));
  }

  getAllWithoutPagination(): Observable<{ structures: StructureInfo[] }> {
    return this.http.get<{ structures: StructureRaw[] }>(
      `${this.structureInformationUrl}` + '?paginate=false',
      { headers: new HttpHeaders({'Accept': 'application/json'})}).pipe(
      map( rawData => ({
          structures: rawData.structures.map(structureRaw => new StructureInfo(structureRaw))
        })
      ));
  }

  deleteLogo() {
    return this.http.delete(this.structureInformationUrl + '/deleteLogo' , {
      headers: new HttpHeaders({'Accept': 'application/json'})});
  }
}

