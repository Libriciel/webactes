export interface TdtReturnType {
  success: boolean;
  teletransmissionUrl?: string;
  errorMessage?: string;
}
