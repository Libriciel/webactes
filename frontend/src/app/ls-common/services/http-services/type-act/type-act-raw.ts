import { Nature } from '../../../../model/Nature/nature';
import { TypeActAvailableActionsRaw } from './type-act-available-actions-raw.enum';
import { DraftTemplate } from '../../../../model/draft-template/draft-template';
import { Counter } from '../../../../model/counter/counter';

export interface TypeActRaw {
  id?: number;
  name: string;
  // noinspection SpellCheckingInspection
  istdt?: boolean;
  // noinspection SpellCheckingInspection
  isdeliberating?: boolean;
  active?: boolean;
  nature?: Nature;
  nature_id?: number;
  generate_template_project_id?: number;
  generate_template_act_id?: number;
  // noinspection SpellCheckingInspection
  isdefault?: boolean;
  availableActions?: TypeActAvailableActionsRaw;
  draftTemplates?: DraftTemplate[];
  counter_id?: number;
  counter?: Counter;
}
