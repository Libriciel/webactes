import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { TypesAct } from '../../../../model/type-act/types-act';
import { catchError, map } from 'rxjs/operators';
import { TypeActRaw } from './type-act-raw';
import { FilterType } from '../../../../model/util/filter-type';
import { Pagination } from '../../../../model/pagination';
import { TypeactFilterType } from '../../../../model/util/typeact-filter-type';

@Injectable({
  providedIn: 'root'
})
export class TypesActService {

  private typesActUrl = Config.baseUrl + '/api/v1/typesacts';

  constructor(protected http: HttpClient) {
  }

  getAllWithPagination(numPage?: number, filters?: FilterType): Observable<{ pagination: Pagination; typeacts: TypesAct[] }> {
    return this.http.get<{ typesacts: TypeActRaw[], pagination: Pagination }>(
      this.typesActUrl + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : '')
      + (filters && filters.active ? `&active=${filters.active}` : ''),
      {
        headers: new HttpHeaders({'Accept': 'application/json'})
      }).pipe(
      map(rawData => ({
        typeacts: rawData.typesacts
          .map(typeActRaw => {
            return new TypesAct(typeActRaw);
          }),
        pagination: rawData.pagination
      })));
  }

  getAllWithoutPagination(filters?: TypeactFilterType): Observable<{ typeacts: TypesAct[] }> {
    return this.http.get<{ typesacts: TypeActRaw[] }>(
      this.typesActUrl + '?paginate=false'
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : '')
      + (filters && filters.active ? `&active=${filters.active}` : '')
      + (filters && filters.isdeliberating ? `&isdeliberating=${filters.isdeliberating}` : ''),
      {
        headers: new HttpHeaders({'Accept': 'application/json'})
      }).pipe(
      map(rawData => ({
        typeacts: rawData.typesacts
          .map(typeActRaw => {
            return new TypesAct(typeActRaw);
          })
      })));
  }

  getTypesActs(): Observable<{ typeacts: TypesAct[] }> {
    return this.http.get<{ typesacts: TypeActRaw[] }>(
      this.typesActUrl + '?'
      + `&limit=200`,
      {
        headers: new HttpHeaders({'Accept': 'application/json'})
      }).pipe(
      map(rawData => ({
        typeacts: rawData.typesacts
          .map(typeActRaw => {
            return new TypesAct(typeActRaw);
          })
      })));
  }

  getAllActive(): Observable<{ typeacts: TypesAct[] }> {
    return this.http.get<{ typesacts: TypeActRaw[] }>(
      this.typesActUrl + '?paginate=false'
      + `&active=true`,
      {
        headers: new HttpHeaders({'Accept': 'application/json'})
      }).pipe(
      map(rawData => ({
        typeacts: rawData.typesacts
          .map(typeActRaw => {
            return new TypesAct(typeActRaw);
          })
      })));
  }

  deleteTypeActs(typesActs: TypesAct[]): Observable<any> {
    return this.http.delete(this.typesActUrl + '/' + typesActs.map(act => act.id), {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  updateTypeAct(typeAct: TypesAct): Observable<any> {
    return this.http.patch<{ typesAct: TypeActRaw }>(this.typesActUrl + '/' + typeAct.id, {
      nature_id: typeAct.nature.id,
      name: typeAct.name,
      istdt: typeAct.isTransferable,
      isdeliberating: typeAct.isDeliberative,
      draft_templates: typeAct.draftTemplates,
      generate_template_project_id: typeAct.generate_template_project_id,
      generate_template_act_id: typeAct.generate_template_act_id,
      counter_id: typeAct.counter ? typeAct.counter.id : null,
    }, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  addTypeAct(typeAct: TypesAct): Observable<any> {
    return this.http.post<{ typesAct: TypeActRaw }>(this.typesActUrl, {
      nature_id: typeAct.nature.id,
      name: typeAct.name,
      istdt: typeAct.isTransferable,
      isdeliberating: typeAct.isDeliberative,
      draft_templates: typeAct.draftTemplates,
      generate_template_project_id: typeAct.generate_template_project_id,
      generate_template_act_id: typeAct.generate_template_act_id,
      counter_id: typeAct.counter ? typeAct.counter.id : null,
    }, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  switchTypeActActiveState(typesAct: TypesAct): Observable<{ typeAct: TypesAct, success: boolean } | { success: boolean }> {
    return this.http.patch<{ typesAct: TypeActRaw }>(this.typesActUrl + '/' + typesAct.id, {
      nature_id: typesAct.nature.id,
      name: typesAct.name,
      istdt: typesAct.isTransferable,
      isdeliberating: typesAct.isDeliberative,
      draft_templates: typesAct.draftTemplates,
      generate_template_project_id: typesAct.generate_template_project_id,
      generate_template_act_id: typesAct.generate_template_act_id,
      active: typesAct.active,
    }, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map(rawData => ({
        typesAct: new TypesAct(rawData.typesAct),
        success: true
      })),
      catchError(() => of({
          success: false
        })
      )
    );
  }
}
