import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Project } from 'src/app/model/project/project';
import { Observable } from 'rxjs';
import { TdtReturnType } from './tdt-return-type';
import { Config } from 'src/app/Config';

@Injectable({
  providedIn: 'root'
})
export class ActService {

  private static PROJECT_URL = Config.baseUrl + '/api/v1/projects';

  constructor(protected http: HttpClient) {
  }

  private static createTeletransmissionData(project: Project): { projectId: number, act_code: string } {
    return {
      projectId: project.id,
      act_code: project.codeAct
    };
  }

  sendActToTdt(project: Project): Observable<TdtReturnType> {
    const teletransmissionData = ActService.createTeletransmissionData(project);
    const payload = [teletransmissionData];
    return this.http.post<{ success: boolean }>(`${ActService.PROJECT_URL}/sendtdt`, payload, {
      responseType: 'json',
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  cancelTdt(project: Project): Observable<TdtReturnType> {
    const teletransmissionData = ActService.createTeletransmissionData(project);
    const payload = [teletransmissionData];
    return this.http.post<{ success: boolean }>(`${ActService.PROJECT_URL}/canceltdt`, payload, {
      responseType: 'json',
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  getTdtOrderUrl(project: Project): Observable<{ url: string }> {
    return this.http.get<{ url: string }>
    (`${ActService.PROJECT_URL}/getTdtUrl/${project.id}`, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  teletransmissionWasOrderedCorrectly(projectId: number): Observable<any> {
    const payload = {projectIds: [projectId]};
    return this.http.post(`${ActService.PROJECT_URL}/teletransmissionOrdered`, payload, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }
}
