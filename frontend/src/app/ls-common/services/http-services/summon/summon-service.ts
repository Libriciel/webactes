import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Summon } from '../../../../model/summon/summon';
import { Observable } from 'rxjs';
import { IManageSummonService } from '../../../../model/summon/imanage-summon-service';


@Injectable({
  providedIn: 'root'
})
export class SummonService implements IManageSummonService {
  private summonUrl = Config.baseUrl + '/api/v1/summons';
  private sendSummonUrl = Config.baseUrl + '/api/v1/summons/send';
  private REQUEST_OPTIONS = {
    headers: new HttpHeaders({'Accept': 'application/json'})
  };

  constructor(protected http: HttpClient) {
  }

  add(summon: Summon): Observable<Summon> {
    return this.http.post<{ summon: Summon }>(this.summonUrl, summon, this.REQUEST_OPTIONS).pipe(
      map(result => result.summon)
    );
  }

  getBySittingId(sittingId: number): Observable<Summon> {
    return this.http.get<Summon[]>
    (`${this.summonUrl}?sitting_id=${sittingId}&format=full`, this.REQUEST_OPTIONS)
      .pipe(map(summons => summons.length === 0 ? null : Object.assign(new Summon(), summons[0])));
  }

  send(summonId: Number): Observable<any> {
    return this.http.get<{ summons: Summon }>(`${this.sendSummonUrl}/${summonId}`, this.REQUEST_OPTIONS);
  }

  sendToIdelibre(sittingId: Number): Observable<any> {
    return this.http.get<{ summons: Summon }>(`${this.summonUrl}/sendIdelibre/${sittingId}`, this.REQUEST_OPTIONS);
  }
}
