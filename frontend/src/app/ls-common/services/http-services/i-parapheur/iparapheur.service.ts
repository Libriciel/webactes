import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../../../../Config';
import { Observable, of } from 'rxjs';
import { Project } from '../../../../model/project/project';
import { IParapheurCircuit } from '../../../../model/iparapheur/iparapheur-circuit';
import { catchError, map } from 'rxjs/operators';
import { ProjectRaw } from '../project/project-raw';
import { ProjectService } from '../project/project.service';

@Injectable({
  providedIn: 'root'
})
export class IParapheurService {

  private static GET_PARAPHEUR_CIRCUIT_URL = Config.baseUrl + '/api/v1/projects/signature/soustypes';
  private static SEND_TO_PARAPHEUR_CIRCUIT_URL = Config.baseUrl + '/api/v1/projects/signature/electronic-signature';
  private static GET_PARAPHEUR_STATUS_URL = Config.baseUrl + '/api/v1/projects/signature/status-iparapheur';

  constructor(protected http: HttpClient) {
  }

  getIParapheurCircuits(project: Project): Observable<IParapheurCircuit[] | { error: string }> {
    return this.http.get(`${IParapheurService.GET_PARAPHEUR_CIRCUIT_URL}/${project.id}`, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map(value => value as IParapheurCircuit[]),
      catchError((error) => of({error: error.error.errors[Object.getOwnPropertyNames(error.error.errors)[0]]}))
    );
  }

  sendToIParapheur(project: Project, iparapheurCircuitName: string): Observable<any> {
    const json = {
      iparapheur_circuit_name: iparapheurCircuitName,
    };
    const formData: FormData = new FormData();
    formData.append('soustypeParapheur', JSON.stringify(json));
    // add main document
    if (project.mainDocument.file instanceof Blob) {
      formData.append('mainDocument', project.mainDocument.file, project.mainDocument.file.name);
    }

    let tmpFormData = ProjectService.createFormData(project);
    formData.append('project', tmpFormData.get('project'));
    formData.append('_method', 'PUT');
    return this.http.post<{ project: ProjectRaw }>
    (`${IParapheurService.SEND_TO_PARAPHEUR_CIRCUIT_URL}/${project.id}`, formData, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  getIParapheurStatus(project: Project): Observable<{ status: string, annotation: string }> {
    return this.http.get<{ status: string, annotation: string }>(`${IParapheurService.GET_PARAPHEUR_STATUS_URL}/${project.id}`, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }
}
