import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Voter } from '../../../../model/voters/voter';
import { VoterListItem } from '../../../../model/voters-list/voter-list-item';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VotersListService {

  private voterListUrl = Config.baseUrl + '/api/v1/liste-presence';
  private sittingUrl = Config.baseUrl + '/api/v1/sittings';

  constructor(protected http: HttpClient) {
  }

  /**
   * Add a vote to the project
   */
  add(voters: Voter[], projectId: number, sittingId: number): Observable<any> {
    return this.http.post(this.voterListUrl, {
      votersList: voters.map(
        voter => ({
          actor_id: voter.id,
          project_id: projectId,
          sitting_id: sittingId,
          is_present: true,
        })
      ),
    }, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  save(sittingId: number, projectId: number, votersListItems: VoterListItem[]): Observable<VoterListItem[]> {
    votersListItems.forEach(votersListItem =>
      votersListItem.mandataire_id = votersListItem.representative ? votersListItem.representative.actor_id : null);
    return this.http.put<{ voterListItems: VoterListItem[] }>
    (`${this.sittingUrl}/${sittingId}/projects/${projectId}/liste-presence/`, votersListItems, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map(data => data.voterListItems));
  }

}
