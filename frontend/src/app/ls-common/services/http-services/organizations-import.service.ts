import { Injectable } from '@angular/core';
import { Config } from '../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrganizationsImportService {

  private organizationsGetUrl = Config.baseUrl + '/api/v1/organizations/getOrchestrationOrganizations';
  private structureCreateurl = Config.baseUrl + '/api/v1/structures/addStructuresWithOrchestration';

  constructor(protected http: HttpClient) {
  }

  getOrganizationsAndStructuresToBeImported():
    Observable<{
      id_e: string,
      denomination: string,
      siren: string,
      type: string,
      centre_de_gestion: string,
      entite_mere: string
    }[]> {
    return this.http.get<{ structures: any }>(this.organizationsGetUrl, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map(structures => structures.structures));
  }

  createOrganizationsAndStructures(organizationsAndStructuresMap: number[][]): Observable<any> {
    const obj = {};
    organizationsAndStructuresMap.forEach((item, index) => obj[index] = item);
    return this.http.put<{ structures: any }>(this.structureCreateurl, JSON.stringify(obj), {
      headers: new HttpHeaders({'Content-Type': 'application/json', 'Accept': 'application/json'})
    });
  }
}
