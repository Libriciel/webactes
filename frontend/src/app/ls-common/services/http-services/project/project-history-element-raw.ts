import { ProjectHistoryElementType } from '../../../../model/project/project-history-element-type.enum';
import { User } from '../../../../model/user/user';

export interface ProjectHistoryElementRaw {
  comment: ProjectHistoryElementType;
  created: Date;
  user: User;
  data: any;
}
