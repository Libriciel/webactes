import {Injectable} from '@angular/core';
import {Config} from '../../../../Config';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs';
import {Project} from '../../../../model/project/project';
import {map} from 'rxjs/operators';
import {Sitting} from '../../../../model/Sitting/sitting';
import {TypesAct} from '../../../../model/type-act/types-act';
import {ProjectRaw} from './project-raw';
import {DocumentService} from '../document/document.service';
import {Pagination} from '../../../../model/pagination';
import {ClassificationSubject} from 'src/app/model/classification/classification-subject';
import {CircuitInstanceRaw} from '../circuits/circuit-instance-raw';
import {CircuitInstance} from 'src/app/model/circuit/circuit-instance';
import {StepInstanceRaw} from '../circuits/step-instance-raw';
import {StepInstance} from 'src/app/model/circuit/step-instance';
import {STEP_TYPE_LIST} from 'src/app/model/circuit/step-type';
import {ProjectHistoryElementRaw} from './project-history-element-raw';
import {ProjectHistoryElement} from '../../../../model/project/project-history-element';
import {ProjectDisplayState} from '../../../../model/project/project-display-state.enum';
import {DateUtils} from '../../../../utility/DateUtils';
import {FilterType} from '../../../../model/util/filter-type';

@Injectable({
    providedIn: 'root'
})
export class ProjectService {

    private static MANUAL_SIGNATURE_PATH = 'manual-signature';
    private static PROJECT_URL = Config.baseUrl + '/api/v1/projects';
    private static MENU_PATH = 'menu';
    private static REQUEST_PROJECTS_OPTIONS_ACCEPT_JSON = {
        headers: new HttpHeaders({'Accept': 'application/json'}),
        observe: 'response' as 'body' // needed to get the httpCode
    };
    private CHECK_CODE_ACT_URL = Config.baseUrl + '/api/v1/checkCodeAct/';
    private generateActNumberUrl = ProjectService.PROJECT_URL + '/generate-act-number/';

    constructor(protected http: HttpClient) {
    }

    static createFormData(project: Project): FormData {
        // noinspection SpellCheckingInspection
        const json = {
            id: project.id,
            typesact_id: project.typesact_id,
            stateact_id: project.stateact_id,
            theme_id: project.theme_id,
            matiere_id: project.classification_id,
            sittings: project.sittings_ids,
            name: project.name,
            ismultichannel: project.isMultichannel,
            code_act: project.codeAct,
            maindocument: {
                codetype: project.mainDocument && project.mainDocument.documentType
                        ? project.mainDocument.documentType.codeType:null
            },
            annexes: project.annexes.map(annexe => ({
                is_generate: annexe.joinGenerate === true,
                istransmissible: annexe.toGotTransfer === true,
                id: annexe.id,
                name: annexe.name,
                codetype: annexe.documentType ? annexe.documentType.codeType:null
            }))
        };

        const formData: FormData = new FormData();
        formData.append('project', JSON.stringify(json));
        // add main document
        if (project.mainDocument.file instanceof Blob) {
            formData.append('mainDocument', project.mainDocument.file, project.mainDocument.file.name);
        }

        // add annexes
        if (project.annexes) {
            project.annexes.forEach(annexe => {
                if (annexe.file instanceof Blob) {
                    formData.append(`annexes[]`, annexe.file, annexe.file.name);
                }
            });
        }
        return formData;
    }

    static createProjectFromProjectRaw(projectRaw: ProjectRaw) {
        // containers from project cake object
        // noinspection SpellCheckingInspection
        const generationError = projectRaw.generationerrors ? projectRaw.generationerrors.find(generationerror => generationerror.type === 'act'):null;

        return new Project({
            name: projectRaw.name,
            id: projectRaw.id,
            code: projectRaw.code_act,
            date: projectRaw.created,
            sittingsId: projectRaw.containers && projectRaw.containers[0].sittings
                    ? projectRaw.containers[0].sittings.map(sitting => sitting.id)
                    :[],
            sittings: projectRaw.containers && projectRaw.containers[0].sittings
                    ? projectRaw.containers[0].sittings.map(sittingRaw => new Sitting(sittingRaw))
                    :[],
            isMultichannel: projectRaw.ismultichannel,
            mainDocument: projectRaw.containers && projectRaw.containers[0].maindocument
                    ? DocumentService.createDocumentFromDocumentRaw(projectRaw.containers[0].maindocument, generationError)
                    :null,
            annexes: projectRaw.containers && projectRaw.containers[0].annexes
                    ? projectRaw.containers[0].annexes.map(annexeRaw => DocumentService.createAnnexeFromAnnexeRaw(annexeRaw))
                    :[],
            projectTexts: projectRaw.project_texts ? this.buildProjectTexts(projectRaw.project_texts):[],
            theme: projectRaw.theme,
            typeact: projectRaw.containers && projectRaw.containers[0].typesact ? new TypesAct(projectRaw.containers[0].typesact):null,
            stateact: projectRaw.containers && projectRaw.containers[0].stateacts ? projectRaw.containers[0].stateacts[0]:null,
            classificationId: projectRaw.matiere && projectRaw.matiere.id ? projectRaw.matiere.id:null,
            classification: projectRaw.matiere && projectRaw.matiere.id
                    ? new ClassificationSubject({id: projectRaw.matiere.id, libelle: projectRaw.matiere.libelle, children: []})
                    :null,
            isStateEditable: projectRaw.isStateEditable,
            circuit: projectRaw.instance ? this.buildCircuitInstanceFromRaw(projectRaw.instance):null,
            signature: projectRaw.signature ? projectRaw.signature:null,
            history: projectRaw.containers && projectRaw.containers[0].histories
                    ? this.buildHistoryFromRaw(projectRaw.containers[0].histories)
                    :[],
            availableActions: projectRaw.availableActions,
            isSignedIParapheur: projectRaw.isSignedIParapheur,
            generationerror: projectRaw.generationerrors
        });
    }

    static buildProjectTexts(projectRaws): [] {
        projectRaws.forEach(
                projectRaw => {
                    if (!projectRaw.files || !projectRaw.files[0]) {
                        return null;
                    }
                    projectRaw.project_text_file = projectRaw.files[0];
                    delete projectRaw.files;
                });

        return projectRaws;
    }

    static buildCircuitInstanceFromRaw(raw: CircuitInstanceRaw): CircuitInstance {
        const steps = [];
        if (raw.steps) {
            raw.steps.forEach(rawStep => {
                steps.push(this.buildStepInstanceFromRaw(rawStep));
            });
        }
        raw.steps = steps;
        return new CircuitInstance(raw as any);
    }

    static buildStepInstanceFromRaw(rawStep: StepInstanceRaw): StepInstance {
        const step = new StepInstance(rawStep);

        // There is currently only 2 possibilities :
        // - there's only one validator, the type of step is "simple"
        // - there are more than one validators, the type of step is "concurrent"
        if (step.validators) {
            if (step.validators.length < 2) {
                step.type = STEP_TYPE_LIST[0];
            } else {
                step.type = STEP_TYPE_LIST[1];
            }
        }
        return step;
    }

    private static buildHistoryFromRaw(histories: ProjectHistoryElementRaw[]): ProjectHistoryElement[] {
        return histories.map(historyElementRaw => new ProjectHistoryElement(historyElementRaw));
    }

    addProject(project: Project): Observable<any> {
        const formData: FormData = ProjectService.createFormData(project);
        return this.http.post(ProjectService.PROJECT_URL, formData, ProjectService.REQUEST_PROJECTS_OPTIONS_ACCEPT_JSON);
    }

    updateProject(project: Project): Observable<any> {
        const formData: FormData = ProjectService.createFormData(project);
        formData.append('_method', 'PUT');
        return this.http.post(`${ProjectService.PROJECT_URL}/${project.id}`, formData, ProjectService.REQUEST_PROJECTS_OPTIONS_ACCEPT_JSON);
    }

    get(id: number): Observable<Project> {
        return forkJoin([
            this.http.get<{ body: { project: ProjectRaw } }>
            (`${ProjectService.PROJECT_URL}/${id}`, ProjectService.REQUEST_PROJECTS_OPTIONS_ACCEPT_JSON)
                    .pipe(map(body => ProjectService.createProjectFromProjectRaw(body.body.project))),
            this.getMenuEntryForProject(id)
        ]).pipe(map(value => {
            value[0].canonicalProjectDisplayState = value[1];
            return value[0];
        }));
    }

    manuallySignProject(project: Project, date: Date): Observable<any> {
        date.toJSON = DateUtils.toJSONPreservingUTC;
        const json = {
            signature_date: date
        };
        const formData: FormData = new FormData();
        formData.append('project', JSON.stringify(json));
        // add main document
        if (project.mainDocument.file instanceof Blob) {
            formData.append('mainDocument', project.mainDocument.file, project.mainDocument.file.name);
        }
        formData.append('_method', 'PUT');
        return this.http.post<{ project: ProjectRaw }>
        (`${ProjectService.PROJECT_URL}/${project.id}/${ProjectService.MANUAL_SIGNATURE_PATH}`,
                formData, ProjectService.REQUEST_PROJECTS_OPTIONS_ACCEPT_JSON);
    }

    checkCode(code): Observable<any> {
        return this.http.get<any>(this.CHECK_CODE_ACT_URL + code, ProjectService.REQUEST_PROJECTS_OPTIONS_ACCEPT_JSON);
    }

    getMenuEntryForProject(id: number): Observable<ProjectDisplayState> {
        return this.http.get<{ body: { menu: string } }>(`${ProjectService.PROJECT_URL}/${id}/${ProjectService.MENU_PATH}`,
                ProjectService.REQUEST_PROJECTS_OPTIONS_ACCEPT_JSON).pipe(map(value => ProjectDisplayState[value.body.menu]));
    }

    deleteProjects(projects: Project[]): Observable<any> {

        if (projects.length > 1) {
            console.warn('deleteProjects - multiple project deletion is not handled yet, we will delete the first only');
            // Use forkJoin op, if really there's no choice
        }

        const projectId = projects[0].id;
        return this.http.delete(`${ProjectService.PROJECT_URL}/${projectId}`, ProjectService.REQUEST_PROJECTS_OPTIONS_ACCEPT_JSON);
    }

    getAllDrafts(numPage: number, filters?: FilterType): Observable<{ projects: Project[], pagination: Pagination }> {
        return this.getAllProjectsForListPath('index_drafts', numPage, filters);
    }

    getAllValidating(numPage: number, filters?: FilterType): Observable<{ projects: Project[], pagination: Pagination }> {
        return this.getAllProjectsForListPath('index_validating', numPage, filters);
    }

    getAllToValidate(numPage: number, filters?: FilterType): Observable<{ projects: Project[], pagination: Pagination }> {
        return this.getAllProjectsForListPath('index_to_validate', numPage, filters);
    }

    getAllValidated(numPage: number, filters?: FilterType): Observable<{ projects: Project[], pagination: Pagination }> {
        return this.getAllProjectsForListPath('index_validated', numPage, filters);
    }

    getAllToTransmit(numPage: number, filters?: FilterType): Observable<{ projects: Project[], pagination: Pagination }> {
        return this.getAllProjectsForListPath('index_to_transmit', numPage, filters);
    }

    getAllReadyToTransmit(numPage: number, filters?: FilterType): Observable<{
        projects: Project[],
        pagination: Pagination
    }> {
        return this.getAllProjectsForListPath('index_ready_to_transmit', numPage, filters);
    }

    getAll(numPage: number, filters?: FilterType): Observable<{ projects: Project[], pagination: Pagination }> {
        return this.getAllProjectsForListPath('index_act', numPage, filters);
    }

    getAllUnAssociated(numPage: number, filters?: FilterType): Observable<{
        projects: Project[],
        pagination: Pagination
    }> {
        return this.getAllProjectsForListPath('index_unassociated', numPage, filters);
    }

    private getAllProjectsForListPath(sub_path: string, numPage: number, filters?: FilterType): Observable<{
        projects: Project[],
        pagination: Pagination
    }> {
        return this.http.get<{ body: { projects: ProjectRaw[], pagination: Pagination } }>
        (`${ProjectService.PROJECT_URL}/${sub_path}?page=${numPage}`
                + (filters && filters.searchedText ? `&name=${filters.searchedText}`
                        + `&typeact_name=${filters.searchedText}` + `&code_act=${filters.searchedText}`:''),
                ProjectService.REQUEST_PROJECTS_OPTIONS_ACCEPT_JSON)
                .pipe(map(body => ({
                    projects: body.body.projects.map(projectRaw => ProjectService.createProjectFromProjectRaw(projectRaw)),
                    pagination: body.body.pagination
                })));
    }

    generateActNumber(projectId: number): Observable<{ success: boolean, generatedActNumber: string }> {
        return this.http.post<{ body: { success: boolean, generatedActNumber: string } }>(
                `${this.generateActNumberUrl}${projectId}`,
                null,
                ProjectService.REQUEST_PROJECTS_OPTIONS_ACCEPT_JSON
        ).pipe(map(response => {
            return ({
                success: response.body.success,
                generatedActNumber: response.body.generatedActNumber
            });
        }));
    }

}
