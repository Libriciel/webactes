export interface ProjectAvailableActionsRaw {
  can_be_send_to_i_parapheur: { value: boolean, data: string };
  can_be_send_to_manual_signature: { value: boolean, data: string };
  check_refused: { value: boolean, data: string };
  check_signing: { value: boolean, data: string };
  can_approve_project: { value: boolean, data: string };
  can_be_send_to_workflow: { value: boolean, data: string };
  can_be_edited: { value: boolean, data: string };
  can_be_deleted: { value: boolean, data: string };
  can_be_voted: { value: boolean, data: string };
  can_generate_act_number: { value: boolean, data: string };
}
