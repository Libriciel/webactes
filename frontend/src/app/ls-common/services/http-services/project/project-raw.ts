import { Sitting } from '../../../../model/Sitting/sitting';
import { Theme } from '../../../../model/theme/theme';
import { StatesAct } from '../../../../model/states-act';
import { MatiereRaw } from '../info/matiere-raw';
import { CircuitInstanceRaw } from '../circuits/circuit-instance-raw';
import { IParapheurHistoryElement } from '../../../../model/iparapheur/iparapheur-history-element';
import { ProjectHistoryElementRaw } from './project-history-element-raw';
import { ProjectAvailableActionsRaw } from './project-available-actions-raw';
import { DocumentRaw } from '../document/document-raw';
import { SittingRaw } from '../sittings/sitting-raw';

export interface ProjectRaw {
  generationerrors: { type: string, message: string; date: Date }[];
  id?: number;
  name: string;
  created: Date;
  // noinspection SpellCheckingInspection
  ismultichannel: boolean;
  sittings?: Sitting[];
  theme?: Theme;
  sittingsId: number[];
  // noinspection SpellCheckingInspection
  matiere: MatiereRaw;
  isStateEditable: boolean;
  signature: IParapheurHistoryElement[];
  project_texts?;
  /**
   *   should have one and only one container in containers
   */
  containers: {
    sittings: SittingRaw[],
    histories: ProjectHistoryElementRaw[];
    // noinspection SpellCheckingInspection
    maindocument?: DocumentRaw,
    annexes?,
    // noinspection SpellCheckingInspection
    typesact,
    // noinspection SpellCheckingInspection
    /**
     * there are several stateActs because we can manage history.
     * Nevertheless, because stateact is mandatory, we should have at least one stateact
     **/
    stateacts: StatesAct[],
    project: ProjectRaw,
  }[];
  code_act: string;

  instance?: CircuitInstanceRaw;
  availableActions?: ProjectAvailableActionsRaw;
  isSignedIParapheur?: boolean;
}
