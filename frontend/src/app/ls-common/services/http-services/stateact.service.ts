import { Injectable } from '@angular/core';
import { Config } from '../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StatesAct } from '../../../model/states-act';

@Injectable({
  providedIn: 'root'
})
export class StateactService {

  private stateactsUrl = Config.baseUrl + '/api/v1/stateacts';

  constructor(protected http: HttpClient) {
  }

  getAll(action: string = 'add', id?: number): Observable<StatesAct[]> {
    const url = id != null ? this.stateactsUrl + '?action=' + action + '&id=' + id : this.stateactsUrl + '?action=' + action ;
    return this.http.get<{ stateacts: StatesAct[] }>(url, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map((stateacts: { stateacts: StatesAct[] }) => {
      return stateacts.stateacts.map(stateact => {
        if (stateact.code === Config.DEFAULT_PROJECT_CREATION_STATE_CODE) {
          stateact.isDefault = true;
        }
        return stateact;
      });
    }));
  }
}
