import { Injectable } from '@angular/core';
import { Config } from '../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Search } from '../../../model/search';
import { map } from 'rxjs/operators';
import { Project } from '../../../model/project/project';
import { TypesAct } from '../../../model/type-act/types-act';
import { Pagination } from '../../../model/pagination';


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private searchUrl = Config.baseUrl + '/api/v1/search';
  private REQUEST_OPTIONS = {
    headers: new HttpHeaders({'Accept': 'application/json'})
  };

  constructor(protected http: HttpClient) {
  }

  postSearch(search: Search, numPage?: number): Observable<{ projects: Project[], pagination: Pagination }> {
    return this.http.post<{ projectsList: any[], pagination: Pagination }>(this.searchUrl + '?'
      + (numPage ? `page=${numPage}` : `page=1`), search, this.REQUEST_OPTIONS)
      .pipe(map(result => {
        return {
          projects: result.projectsList.map(data => {
            return new Project({
              circuit: undefined,
              classification: undefined,
              classificationId: 0,
              history: [],
              isSignedIParapheur: false,
              signature: [],
              sittings: [],
              stateact: undefined,
              theme: undefined,
              id: data.id,
              name: data.name,
              code: data.code_act,
              typeact: new TypesAct(data.Typesacts),
              availableActions: data.availableActions,
              annexes: [],
              projectTexts: [],
              date: undefined,
              isMultichannel: false,
              isStateEditable: false,
              mainDocument: undefined,
              sittingsId: [],
              generationerror: data.generationerror
            });
          }),
          pagination: result.pagination
        };
      }));
  }
}
