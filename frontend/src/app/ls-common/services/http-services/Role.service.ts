import { Injectable } from '@angular/core';
import { Config } from '../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Role } from '../../../model/role';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private roleUrl = Config.baseUrl + '/api/v1/roles/';

  constructor(protected http: HttpClient) {
  }

  getAll(): Observable<{ roles: Role[] }> {
    return this.http.get<{ roles: Role[] }>(this.roleUrl, {
      headers: new HttpHeaders({'Accept': 'application/json'}),
    })
      .pipe(map(rawData => ({
        roles: rawData.roles
          .map(roleRaw => {
            return new Role(
              roleRaw.id,
              roleRaw.name,
            );
          }),
      })));
  }

  getPermissions(id): Observable<any> {
    return this.http.get<{ permissions: any }>(this.roleUrl + id, {
      headers: new HttpHeaders({'Accept': 'application/json'}),
    });
  }
}
