import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FilterType } from '../../../../model/util/filter-type';
import { Observable, of } from 'rxjs';
import { Pagination } from '../../../../model/pagination';
import { map } from 'rxjs/operators';
import { Counter } from '../../../../model/counter/counter';
import { CounterRaw } from './counter-raw';

@Injectable({
  providedIn: 'root'
})
export class CounterService {

  private counterUrl = Config.baseUrl + '/api/v1/counters';
  private REQUEST_OPTIONS = {
    headers: new HttpHeaders({'Accept': 'application/json'})
  };

  constructor(protected http: HttpClient) {
  }

  getAllWithPagination(numPage?: number, filters?: FilterType, limit?: number): Observable<{ counters: Counter[], pagination: Pagination }> {
    return this.http.get<{ counters: CounterRaw[], pagination: Pagination }>(
      this.counterUrl + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : '')
      + (filters && filters.active ? `&active=${filters.active}` : '')
      + (limit ? `&limit=${limit}` : `&limit=10`),
      this.REQUEST_OPTIONS).pipe(
      map(rawData => ({
        counters: rawData.counters
          .map(counterRaw => {
            return new Counter(
              counterRaw.id,
              counterRaw.name,
              counterRaw.comment,
              counterRaw.counter_def,
              counterRaw.sequence_id,
              counterRaw.sequence,
              counterRaw.reinit_def,
              counterRaw.availableActions,
            );
          }),
        pagination: rawData.pagination
      })));
  }

  getAllWithoutPagination(filters?: FilterType): Observable<{ counters: Counter[] }> {
    return this.http.get<{ counters: CounterRaw[], pagination: Pagination }>(
      this.counterUrl + '?paginate=false'
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : '')
      + (filters && filters.active ? `&active=${filters.active}` : ''),
      this.REQUEST_OPTIONS).pipe(
      map(rawData => ({
        counters: rawData.counters
          .map(counterRaw => {
            return new Counter(
              counterRaw.id,
              counterRaw.name,
              counterRaw.comment,
              counterRaw.counter_def,
              counterRaw.sequence_id,
              counterRaw.sequence,
              counterRaw.reinit_def,
              counterRaw.availableActions,
            );
          })
      })));
  }

  addCounter(counter: Counter): Observable<any> {
    return this.http.post<{ counter: CounterRaw }>(this.counterUrl, counter, this.REQUEST_OPTIONS)
      .pipe(
      map(result => {
        return new Counter(
          result.counter.id,
          result.counter.name,
          result.counter.comment,
          result.counter.counter_def,
          result.counter.sequence_id,
          result.counter.sequence,
          result.counter.reinit_def,
          result.counter.availableActions,
        );
      })
    );
  }

  updateCounter(counter: Counter): Observable<any> {
    return this.http.put<{ counter: CounterRaw }>(this.counterUrl + '/' + counter.id, counter, this.REQUEST_OPTIONS)
      .pipe(
      map(result => {
        return new Counter(
          result.counter.id,
          result.counter.name,
          result.counter.comment,
          result.counter.counter_def,
          result.counter.sequence_id,
          result.counter.sequence,
          result.counter.reinit_def,
          result.counter.availableActions,
        );
      })
    );
  }

  deleteCounter(counter: Counter[]): Observable<any> {
    const id = counter[0].id;
    return this.http.delete(this.counterUrl + '/' + id, this.REQUEST_OPTIONS);
  }

}
