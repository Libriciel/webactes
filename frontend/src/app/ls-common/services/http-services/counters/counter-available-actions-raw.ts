export interface CounterAvailableActionsRaw {
  can_be_edited: { value: boolean, data: string };
  can_be_deleted: { value: boolean, data: string };
  can_be_created: { value: boolean, data: string };
}
