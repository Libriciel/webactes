import { CounterAvailableActionsRaw } from './counter-available-actions-raw';
import { Sequence } from '../../../../model/sequence/sequence';

export class CounterRaw {
  id: number;
  name: string;
  comment: string;
  counter_def: string;
  sequence_id: number;
  sequence: Sequence;
  reinit_def: string;
  availableActions: CounterAvailableActionsRaw;
}
