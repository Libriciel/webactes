import { Config } from 'src/app/Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CircuitInstance } from 'src/app/model/circuit/circuit-instance';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Project } from '../../../model/project/project';

@Injectable({
  providedIn: 'root'
})
export class CircuitInstanceService {
  private PROJECT_URL = Config.baseUrl + '/api/v1/projects';
  private TASKS_URL = Config.baseUrl + '/api/v1/tasks';

  private REQUEST_OPTIONS = {
    headers: new HttpHeaders({'Accept': 'application/json'})
  };

  constructor(protected http: HttpClient) {
  }

  sendProjectIntoCircuit(projectId: number, circuitId: number): Observable<{ createdInstanceId: number }> {
    return this.http.post<{ createdInstanceId: number }>
    (`${this.PROJECT_URL}/${projectId}/sendToCircuit/${circuitId}`, {}, this.REQUEST_OPTIONS);
  }

  validateCurrentStep(circuitInstance: CircuitInstance, comment: string): Observable<any> {
    const stepId = circuitInstance.steps[circuitInstance.currentStepIdx].id;
    return this.http.post<any>(`${this.TASKS_URL}/approve/${stepId}`, {
      action: 'validate',
      comment: comment
    }, this.REQUEST_OPTIONS)
      .pipe(
        map(() => ({success: true}))
      );
  }

  rejectCurrentStep(circuitInstance: CircuitInstance, comment: string): Observable<any> {
    const stepId = circuitInstance.steps[circuitInstance.currentStepIdx].id;
    return this.http.post<any>(`${this.TASKS_URL}/reject/${stepId}`, {
      action: 'validate',
      comment: comment
    }, this.REQUEST_OPTIONS)
      .pipe(
        map(() => ({success: true}))
      );
  }

  validateProjectInEmergency(project: Project, comment: string): Observable<any> {
    return this.http.post<any>(`${this.PROJECT_URL}/approve/${project.id}`,
      {
        comment: comment
      },
      this.REQUEST_OPTIONS)
      .pipe(map(() => ({success: true})));
  }

}
