export interface UserRaw {
  id: number;
  // noinspection SpellCheckingInspection
  firstname: string;
  // noinspection SpellCheckingInspection
  lastname: string;
  role: {
    id: 1,
    name: string
  };
}
