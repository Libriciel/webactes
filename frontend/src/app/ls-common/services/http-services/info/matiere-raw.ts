export interface MatiereRaw {
  id: number;
  libelle: string;
  // noinspection SpellCheckingInspection
  codematiere: string;
}
