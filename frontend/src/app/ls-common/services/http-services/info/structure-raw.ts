import { HasId } from '../../../model/has-id';
import { HasName } from '../../../model/has-name';
import { Role } from '../../../../model/role';

export interface StructureRaw extends HasId, HasName {
  id: number;
  name: string;
  business_name?: string;
  // noinspection SpellCheckingInspection
  customname?: string;
  spinner: string;
  active: boolean;
  organization_id?: number;
  roles?: Role[];
}
