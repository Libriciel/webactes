import { SequenceAvailableActionsRaw } from './sequence-available-actions-raw.enum';

export class SequenceRaw {
  id: number;
  name: string;
  comment: string;
  sequence_num: number;
  availableActions: SequenceAvailableActionsRaw;
}
