import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FilterType } from '../../../../model/util/filter-type';
import { Observable } from 'rxjs';
import { Sequence } from '../../../../model/sequence/sequence';
import { Pagination } from '../../../../model/pagination';
import { map } from 'rxjs/operators';
import { SequenceRaw } from './sequence-raw';

@Injectable({
  providedIn: 'root'
})
export class SequenceService {

  private sequenceUrl = Config.baseUrl + '/api/v1/sequences';
  private REQUEST_OPTIONS = {
    headers: new HttpHeaders({'Accept': 'application/json'})
  };

  constructor(protected http: HttpClient) {
  }

  getAll(numPage: number, limit: number, filters: FilterType = {searchedText: ''})
    : Observable<{ sequences: Sequence[], pagination: Pagination }> {
    return this.http.get<{ sequences: SequenceRaw[], pagination: Pagination }>(
      this.sequenceUrl + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters.searchedText ? `&name=${filters.searchedText}` : '')
      + (filters.active ? `&active=${filters.active}` : '')
      + (limit ? `&limit=${limit}` : ``),
      this.REQUEST_OPTIONS).pipe(
      map(rawData => ({
        sequences: rawData.sequences
          .map(sequenceRaw => {
            return new Sequence(
              sequenceRaw.id,
              sequenceRaw.name,
              sequenceRaw.comment,
              sequenceRaw.sequence_num,
              sequenceRaw.availableActions,
            );
          }),
        pagination: rawData.pagination
      })));
  }

  addSequence(sequence: Sequence): Observable<any> {
    return this.http.post<{ sequence: SequenceRaw }>(this.sequenceUrl, sequence, this.REQUEST_OPTIONS).pipe(
      map(result => {
        return new Sequence(
          result.sequence.id,
          result.sequence.name,
          result.sequence.comment,
          result.sequence.sequence_num,
          result.sequence.availableActions,
        );
      })
    );
  }

  updateSequence(updatedSequence: Sequence): Observable<any> {
    return this.http.put<{ sequence: SequenceRaw }>(this.sequenceUrl + '/' + updatedSequence.id, updatedSequence, this.REQUEST_OPTIONS)
      .pipe(
        map(sequence => {
          return new Sequence(
            sequence.sequence.id,
            sequence.sequence.name,
            sequence.sequence.comment,
            sequence.sequence.sequence_num,
            sequence.sequence.availableActions,
          );
        })
      );
  }

  deleteSequence(sequence: Sequence[]): Observable<any> {
    const id = sequence[0].id;
    return this.http.delete(this.sequenceUrl + '/' + id, this.REQUEST_OPTIONS);
  }
}
