import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Theme } from '../../../../model/theme/theme';
import { Pagination } from '../../../../model/pagination';
import { ThemeRight } from '../../../../model/theme/theme-right.enum';
import { FilterType } from '../../../../model/util/filter-type';
import { ThemeRaw } from './theme-raw';


@Injectable({
  providedIn: 'root'
})

export class ThemeService {


  private REQUEST_OPTIONS = {
    headers: new HttpHeaders({'Accept': 'application/json'})
  };
  private THEME_URL = Config.baseUrl + '/api/v1/themes';


  constructor(protected http: HttpClient) {
  }

  getAll(filters?: FilterType, numPage?: number): Observable<{ themes: Theme[], pagination: Pagination }> {
    if (!numPage) {
      numPage = 1;
    }
    const url = `${this.THEME_URL}`;
    return this.http.get<{ themes: ThemeRaw[], pagination: Pagination }>(
      url + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : '')
      + (filters && filters.active ? `&active=${filters.active}` : ''),
      this.REQUEST_OPTIONS).pipe(
      map(result => {
        const mappedThemesList = result.themes ? result.themes.map(rawTheme => this.buildThemeFromRaw(rawTheme)) : [];
        return {themes: mappedThemesList, pagination: result.pagination};
      })
    );
  }

  getAllThemes(filters?: FilterType): Observable<{ themes: Theme[] }> {
    const url = `${this.THEME_URL}`;
    return this.http.get<{ themes: ThemeRaw[] }>(
      url + '?'
      + (filters && filters.active ? `&active=${filters.active}` : '')
      + (`&limit=50`),
      this.REQUEST_OPTIONS).pipe(
      map(result => {
        const mappedThemesList = result.themes ? result.themes.map(rawTheme => this.buildThemeFromRaw(rawTheme)) : [];
        return {themes: mappedThemesList};
      })
    );
  }

  getTheme(id: number): Observable<Theme> {
    return this.http.get<Theme>(this.THEME_URL + `${id}`);
  }

  addTheme(theme: Theme): Observable<Theme> {
    return this.http.post<Theme>(this.THEME_URL, this.buildThemeRawFromTheme(theme), this.REQUEST_OPTIONS);
  }

  updateTheme(theme: Theme): Observable<Theme> {
    return this.http.put<{ theme: Theme }>(`${this.THEME_URL}/${theme.id}`, this.buildThemeRawFromTheme(theme), this.REQUEST_OPTIONS)
      .pipe(
        map(rawTheme => rawTheme.theme)
      );
  }

  deleteThemes(theme: Theme[]): Observable<boolean> {
    if (!theme || theme.length === 0) {
      return of(false);
    }

    const id = theme[0].id;
    return this.http.delete<any>(`${this.THEME_URL}/${id}`, this.REQUEST_OPTIONS);
  }

  switchThemeActiveState(theme: Theme, active: boolean): Observable<{ success: boolean, theme: Theme } | { success: boolean }> {
    let url: string;
    if (active) {
      url = this.THEME_URL + '/' + theme.id + '/' + 'activate';
    } else {
      url = this.THEME_URL + '/' + theme.id + '/' + 'deactivate';
    }

    return this.http.post<{ success: boolean, theme: ThemeRaw }>(url, {}, this.REQUEST_OPTIONS).pipe(
      map(
        (result: { success: boolean, theme: ThemeRaw }) => {
          return {success: result.success, theme: this.buildThemeFromRaw(result.theme)};
        }),
      catchError((error) => of({
        success: error.error.success
      }))
    );
  }

  private buildThemeFromRaw(themeRaw: ThemeRaw): Theme {
    const theme = new Theme(
      {
        id: themeRaw.id,
        name: themeRaw.name,
        structure_id: themeRaw.structure_id,
        active: themeRaw.active,
        parent_id: themeRaw.parent_id,
        childrenRaw: themeRaw.children,
        position: themeRaw.position,
        lft: themeRaw.lft,
        rght: themeRaw.rght,
        is_deletable: themeRaw.is_deletable,
        is_editable: themeRaw.is_editable,
        is_deactivable: themeRaw.is_deactivable
      }
    );
    theme.setAvailableAction(ThemeRight.isDeletable, themeRaw.is_deletable);
    theme.setAvailableAction(ThemeRight.isEditable, themeRaw.is_editable);
    theme.setAvailableAction(ThemeRight.isDeactivable, themeRaw.is_deactivable);

    return theme;
  }

  private buildThemeRawFromTheme(theme: Theme) {
    return {
      id: theme.id,
      name: theme.name,
      structure_id: theme.structureId,
      active: theme.active,
      parent_id: theme.parentId,
      childrenRaw: theme.children,
      position: theme.position,
      lft: theme.lft,
      rght: theme.rght,
    };
  }

}
