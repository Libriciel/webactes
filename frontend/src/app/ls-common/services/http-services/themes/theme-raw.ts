export class ThemeRaw {
  id: number;
  name: string;
  structure_id: number;
  active: boolean;
  parent_id?: number;
  lft: number;
  rght: number;
  children?: ThemeRaw[];
  position: string;
  isDefault?: boolean;
  is_deletable: boolean;
  is_editable: boolean;
  is_deactivable: boolean;
}
