import { TypeActRaw } from '../type-act/type-act-raw';
import { TypeSittingAvailableActionsRaw } from './type-sitting-available-actions-raw.enum';
import { Actorgroup } from '../../../../core';
import { Summon } from '../../../../model/summon/summon';

export class TypeSittingRaw{
  id: number;
  name: string;
  isdeliberating: boolean;
  active: boolean;
  typeActs: TypeActRaw[];
  availableActions: TypeSittingAvailableActionsRaw;
  actorGroups: Actorgroup[];
  generate_template_convocation_id?: number;
  generate_template_executive_summary_id?: number;
  generate_template_deliberations_list_id?: number;
  generate_template_verbal_trial_id?: number;
  summon?: Summon;
}
