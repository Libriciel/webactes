export interface TypeSittingAvailableActionsRaw {
  can_be_edited: { value: boolean, data: string };
  can_be_deleted: { value: boolean, data: string };
  can_be_created: { value: boolean, data: string };
  can_be_deactivate: { value: boolean, data: string };
}
