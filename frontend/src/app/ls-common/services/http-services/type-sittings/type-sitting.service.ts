import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Config } from '../../../../Config';
import { TypeSitting } from '../../../../model/type-sitting/type-sitting';
import { Pagination } from '../../../../model/pagination';
import { catchError, map } from 'rxjs/operators';
import { TypeSittingRaw } from './type-sitting-raw';
import { FilterType } from '../../../../model/util/filter-type';

@Injectable({
  providedIn: 'root'
})
export class TypeSittingService {


  private typeSittingUrl = Config.baseUrl + '/api/v1/typesittings';

  constructor(protected http: HttpClient) {
  }

  getAll(numPage?: number, filters?: FilterType, limit?: number): Observable<{ typeSittings: TypeSitting[], pagination: Pagination }> {
    return this.http.get<{ typeSittings: TypeSittingRaw[], pagination: Pagination }>(
      this.typeSittingUrl + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters.searchedText ? `&name=${filters.searchedText}` : '')
      + (filters.active ? `&active=${filters.active}` : '')
      + (limit ? `&limit=${limit}` : `&limit=10`),
      {
        headers: new HttpHeaders({'Accept': 'application/json'})
      }).pipe(
      map(rawData => ({
        typeSittings: rawData.typeSittings
          .map(typeSittingRaw => {
            return new TypeSitting(
              typeSittingRaw.id,
              typeSittingRaw.name,
              typeSittingRaw.isdeliberating,
              typeSittingRaw.active,
              typeSittingRaw.typeActs,
              typeSittingRaw.availableActions,
              typeSittingRaw.actorGroups,
              typeSittingRaw.generate_template_convocation_id,
              typeSittingRaw.generate_template_executive_summary_id,
              typeSittingRaw.generate_template_deliberations_list_id,
              typeSittingRaw.generate_template_verbal_trial_id,
            );
          }),
        pagination: rawData.pagination
      })));
  }

  addTypeSitting(typeSitting: TypeSitting): Observable<any> {
    return this.http.post<{ typesitting: TypeSittingRaw }>(this.typeSittingUrl, typeSitting, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map(typeSittingRaw => {
        return new TypeSitting(
          typeSittingRaw.typesitting.id,
          typeSittingRaw.typesitting.name,
          typeSittingRaw.typesitting.isdeliberating,
          typeSittingRaw.typesitting.active,
          typeSittingRaw.typesitting.typeActs,
          typeSittingRaw.typesitting.availableActions,
          typeSittingRaw.typesitting.actorGroups,
          typeSittingRaw.typesitting.generate_template_convocation_id,
          typeSittingRaw.typesitting.generate_template_executive_summary_id,
          typeSittingRaw.typesitting.generate_template_deliberations_list_id,
          typeSittingRaw.typesitting.generate_template_verbal_trial_id,
        );
      })
    );
  }

  deleteTypeSittings(typeSittings: TypeSitting[]): Observable<any> {
    return this.http.delete(this.typeSittingUrl + '/' + typeSittings.map(sitting => sitting.id), {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  updateTypeSittings(typeSitting: TypeSitting): Observable<any> {
    return this.http.put<{ typesitting: TypeSittingRaw }>(this.typeSittingUrl + '/' + typeSitting.id, typeSitting, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map(typeSittingRaw => {
        return new TypeSitting(
          typeSittingRaw.typesitting.id,
          typeSittingRaw.typesitting.name,
          typeSittingRaw.typesitting.isdeliberating,
          typeSittingRaw.typesitting.active,
          typeSittingRaw.typesitting.typeActs,
          typeSittingRaw.typesitting.availableActions,
          typeSittingRaw.typesitting.actorGroups,
          typeSittingRaw.typesitting.generate_template_convocation_id,
          typeSittingRaw.typesitting.generate_template_executive_summary_id,
          typeSittingRaw.typesitting.generate_template_deliberations_list_id,
          typeSittingRaw.typesitting.generate_template_verbal_trial_id,
        );
      })
    );
  }

  switchTypeSittingActiveState(typeSitting: TypeSitting, active: boolean):
    Observable<{ typeSitting: TypeSitting, success: boolean }
      | { success: boolean }> {

    let urlToSwitch: string;
    if (active) {
      urlToSwitch = this.typeSittingUrl + '/' + typeSitting.id + '/' + 'activate';
    } else {
      urlToSwitch = this.typeSittingUrl + '/' + typeSitting.id + '/' + 'deactivate';
    }

    return this.http.post<{ typesitting: TypeSittingRaw, success: boolean }>(urlToSwitch, [], {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map(rawData => ({
        typeSitting: new TypeSitting(
          rawData.typesitting.id,
          rawData.typesitting.name,
          rawData.typesitting.isdeliberating,
          rawData.typesitting.active,
          rawData.typesitting.typeActs,
          rawData.typesitting.availableActions,
          rawData.typesitting.actorGroups,
          rawData.typesitting.generate_template_convocation_id,
          rawData.typesitting.generate_template_executive_summary_id,
          rawData.typesitting.generate_template_deliberations_list_id,
          rawData.typesitting.generate_template_verbal_trial_id,
        ),
        success: rawData.success
      })),
      catchError((error) => of({
          success: error.error.success
        })
      )
    );
  }
}
