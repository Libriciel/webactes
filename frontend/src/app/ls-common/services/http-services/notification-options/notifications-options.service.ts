import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WANotificationsOptions } from '../../../../model/notification/notifications-options';
import { Option } from '../../../model/option';
import { Config } from '../../../../Config';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificationsOptionsService {

  private NOTIFICATIONS_OPTIONS_URL = Config.baseUrl + '/api/v1/notifications';
  private NOTIFICATIONS_OPTIONS_ACTIVATE = 'activate';
  private NOTIFICATIONS_OPTIONS_DEACTIVATE = 'deactivate';

  constructor(protected http: HttpClient) {
  }

  getUserNotificationsOptions(): Observable<WANotificationsOptions> {
    return this.http.get<WANotificationsOptions>(`${this.NOTIFICATIONS_OPTIONS_URL}`, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  saveNotificationOption(option: Option): Observable<null> {
    const action = option.active ? this.NOTIFICATIONS_OPTIONS_ACTIVATE : this.NOTIFICATIONS_OPTIONS_DEACTIVATE;
    return this.http.post<{ success: boolean }>(`${this.NOTIFICATIONS_OPTIONS_URL}/${option.id}/${action}`, null, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map(() => null));
  }
}
