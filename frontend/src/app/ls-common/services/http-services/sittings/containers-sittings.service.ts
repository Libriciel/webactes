import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Sitting } from '../../../../model/Sitting/sitting';
import { Observable } from 'rxjs';
import { ContainerSitting } from '../../../../model/Sitting/container-sitting';
import { Pagination } from '../../../../model/pagination';
import { Project } from '../../../../model/project/project';
import { map } from 'rxjs/operators';
import { ProjectService } from '../project/project.service';

@Injectable({
  providedIn: 'root'
})
export class ContainersSittingsService {

  private containerSittingUrl = Config.baseUrl + '/api/v1/containers-sittings';
  projects: Project[];

  constructor(protected http: HttpClient) {
  }

  delete(sittings: Sitting[]) {
    return sittings.map(sitting => this.http.delete(
      this.containerSittingUrl + '/' + sitting.id,
      {headers: new HttpHeaders({'Accept': 'application/json'})}
    ));
  }

  edit(containerSittings: ContainerSitting[]) {
    const schedule = [];
    containerSittings.map(containerSitting => {
      schedule.push({
        id: containerSitting.id,
        rank: containerSitting.rank,
        sitting_id: containerSitting.sitting_id,
        container_id: containerSitting.container_id,
      });
    });
    return this.http.put(
      this.containerSittingUrl, {schedule},
      {headers: new HttpHeaders({'Accept': 'application/json'})});
  }

  getAllProjectsBySittingId(sittingId: string): Observable<Project[]> {
    return this.http.get<{ containersSittings: ContainerSitting[] }>(
      this.containerSittingUrl + '?sitting_id=' + sittingId,
      {headers: new HttpHeaders({'Accept': 'application/json'})}
    ).pipe(
      map(containersSittings =>
        containersSittings.containersSittings
          .map(containerSitting =>
            ProjectService.createProjectFromProjectRaw(containerSitting.container.project)
              .setRank(containerSitting.rank)
              .setTypeAct(containerSitting.container.typesact)
              .setStateAct(containerSitting.container.stateacts[containerSitting.container.stateacts.length - 1])))
    );
  }

  getAll(sittingId: string): Observable<{ containersSittings: ContainerSitting[] }> {
    return this.http.get<{ containersSittings: ContainerSitting[] }>(
      this.containerSittingUrl + '?sitting_id=' + sittingId,
      {headers: new HttpHeaders({'Accept': 'application/json'})}
    );
  }

  getById(id: string): Observable<{ containers_sittings: ContainerSitting[], pagination: Pagination }> {
    return this.http.get<{ containers_sittings: ContainerSitting[], pagination: Pagination }>(
      this.containerSittingUrl + '/' + id,
      {headers: new HttpHeaders({'Accept': 'application/json'})}
    );
  }


}
