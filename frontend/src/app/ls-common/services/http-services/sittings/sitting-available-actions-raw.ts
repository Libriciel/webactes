export interface SittingAvailableActionsRaw {
  can_be_edited: { value: boolean, data: string };
  can_be_deleted: { value: boolean, data: string };
  can_be_created: { value: boolean, data: string };
  can_propose_to_vote: { value: boolean, data: string };
  can_be_closed: { value: boolean, data: string };
}
