import { TypeSitting } from '../../../../model/type-sitting/type-sitting';
import { Project } from '../../../../model/project/project';
import { VoterListItem } from '../../../../model/voters-list/voter-list-item';
import { AttachmentSummonFile } from '../../../../core';
import { Summon } from '../../../../model/summon/summon';
import { SittingAvailableActionsRaw } from './sitting-available-actions-raw';

export class SittingRaw {
  id: number;
  date: Date;
  typesitting?: TypeSitting;
  typesitting_id?: number;
  has_projects?: boolean;
  secretary_id?: number;
  president_id?: number;
  actors_projects_sittings_previous: VoterListItem[];
  containers: any[];
  projects: Project[];
  actors_projects_sittings?: VoterListItem[];
  convocation?: AttachmentSummonFile;
  summons: Summon[];
  availableActions: SittingAvailableActionsRaw;
  generationerrors: { type: string; message: string; date: Date }[];
  date_convocation: Date;
  place?: String;
}
