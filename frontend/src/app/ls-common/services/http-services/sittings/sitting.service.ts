import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../../../../Config';
import { map } from 'rxjs/operators';
import { Pagination } from '../../../../model/pagination';
import { Sitting } from '../../../../model/Sitting/sitting';
import { SittingState } from '../../../../model/Sitting/sitting-state.enum';
import { Vote } from '../../../../model/vote/vote';
import { SittingRaw } from './sitting-raw';
import { FilterType } from '../../../../model/util/filter-type';

@Injectable({
  providedIn: 'root'
})
export class SittingService {

  private sittingUrl = Config.baseUrl + '/api/v1/sittings';

  constructor(protected http: HttpClient) {
  }

  getSittings(sittingState: SittingState = SittingState.ALL, pageNumber = 1, limit = 10, filters?: FilterType)
    : Observable<{ sittings: Sitting[], pagination: Pagination }> {
    let url = this.sittingUrl;
    switch (sittingState) {
      case SittingState.ALL:
        url += `?`;
        break;
      case SittingState.CLOSED:
        url += `?statesitting=${SittingState.CLOSED}`;
        break;
      case SittingState.OPENED:
        url += `?statesitting=${SittingState.OPENED}`;
        break;
    }
    url += `&page=${pageNumber}`;
    url += `&limit=${limit}`;
    url += `&typesitting_name=${filters && filters.searchedText ? filters.searchedText : ''}`;


    return this.http.get<{ sittings: SittingRaw[], pagination: Pagination }>(url, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map(rawData => ({
          sittings: (rawData.sittings.map(sittingRaw => new Sitting(sittingRaw))),
          pagination: rawData.pagination
        })
      ));
  }

  getAllSittings(): Observable<Sitting[]> {
    const url = this.sittingUrl + `?statesitting=${SittingState.OPENED}&format=list`;

    return this.http.get<{ sittings: any[] }>(url, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map(rawData => (rawData.sittings.map(sittingRaw => new Sitting(sittingRaw)))
      )
    );
  }

  add(sitting: Sitting): Observable<any> {
    return this.http.post(this.sittingUrl, sitting, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  deleteSittings(sittings: Sitting[]): Observable<any> {
    return this.http.delete(this.sittingUrl + '?ids=' + sittings.map(sitting => sitting.id).join(','), {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  close(sittings: Sitting[]): Observable<any> {
    return this.http.put(this.sittingUrl + '/close', '[' + sittings.map(sitting => sitting.id).join(',') + ']', {
      headers: new HttpHeaders({'Accept': 'application/json', 'Content-Type': 'application/json'})
    });
  }

  update(sitting: Sitting): Observable<any> {
    return this.http.put(this.sittingUrl + '/' + sitting.id, sitting, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  get(id: string): Observable<{ sitting: Sitting, pagination: Pagination }> {
    return this.http.get<{ sitting: SittingRaw, pagination: Pagination }>(`${this.sittingUrl}/${id}?format=short`, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map(rawData => (
        {
          sitting: new Sitting(rawData.sitting),
          pagination: rawData.pagination
        })));
  }

  getWithProjectVotes(id: number, projectId: number): Observable<{ sitting: Sitting, vote: Vote }> {
    return this.http.get<{ sitting: SittingRaw }>(
      `${this.sittingUrl}/${id}/vote/${projectId}`,
      {headers: new HttpHeaders({'Accept': 'application/json'})}
    ).pipe(
      map(rawData => {
        const vote = rawData.sitting.containers && rawData.sitting.containers[0].project.vote
          ? rawData.sitting.containers[0].project.vote
          : new Vote();
        vote.project_id = projectId;
        return {
          sitting: new Sitting(rawData.sitting),
          vote: vote
            ? new Vote(vote.id,
              vote.president_id,
              vote.project_id,
              vote.yes_counter,
              vote.no_counter,
              vote.abstentions_counter,
              vote.no_words_counter,
              vote.take_act,
              vote.result,
              vote.comment,
              vote.opinion,
              vote.method,
              vote.actors_votes)
            : new Vote()
        };
      }));
  }
}
