import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ReleaseNote } from '../../../wa-common/model/release-note';
import { default as ReleaseNotes } from '../../../../assets/about/releaseNotes.json';
import { default as Licence } from '../../../../assets/about/licence.json';
import { default as ApplicationVersion } from 'application.json';
import { Version } from '../../../wa-common/model/version';

@Injectable({
  providedIn: 'root'
})
export class ApplicationVersionService {
  protected releaseNotes: ReleaseNote[] = ReleaseNotes.versions.map(value => {
    value.version = new Version(value.version);
    return value as unknown as ReleaseNote;
  });
  protected licence: string = Licence.licence;
  protected applicationVersion: Version = new Version(ApplicationVersion);

  constructor() {
  }

  getReleaseNotes(): Observable<ReleaseNote[]> {
    return of(this.releaseNotes);
  }

  getLicence(): Observable<string> {
    return of(this.licence);
  }

  getApplicationVersion(): Observable<Version> {
    return of(this.applicationVersion);
  }
}
