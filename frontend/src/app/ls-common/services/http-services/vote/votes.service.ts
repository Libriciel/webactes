import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pagination } from '../../../../model/pagination';
import { Vote } from '../../../../model/vote/vote';
import { map } from 'rxjs/operators';
import { FilterType } from '../../../../model/util/filter-type';
import { VoteRaw } from './vote-raw';

@Injectable({
  providedIn: 'root'
})
export class VotesService {

  private voteUrl = Config.baseUrl + '/api/v1/votes';
  private voteProjectUrl = Config.baseUrl + this.voteUrl + '/project';

  constructor(protected http: HttpClient) {

  }

  /**
   * Get all votes related to a project
   * @param projectId The project id
   * @param numPage a num page, default 1
   * @param filters filters
   */
  getAll(projectId: number, numPage?: number, filters?: FilterType): Observable<{ pagination: Pagination; votes: Vote[] }> {
    return this.http.get<{ votes: VoteRaw[], pagination: Pagination }>(
      this.voteProjectUrl + '/' + projectId
      + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : ''),
      {
        headers: new HttpHeaders({'Accept': 'application/json'})
      }).pipe(
      map(rawData => ({
        votes: rawData.votes
          .map(vote => {
            return new Vote(
              vote.id,
              vote.president_id,
              vote.project_id,
              vote.yes_counter,
              vote.no_counter,
              vote.abstentions_counter,
              vote.no_words_counter,
              vote.prendre_acte,
              vote.resultat,
              vote.comment,
              vote.opinion,
              vote.method,
            );
          }),
        pagination: rawData.pagination
      })));
  }

  /**
   * Add a vote to the project FAIRE QUE DU PUT !!!!!!!!!!!!!!!!!
   */
  add(vote: Vote): Observable<any> {
    return this.http.post<{ votes: VoteRaw[], method: string, projectId: number }>(this.voteUrl, vote, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  edit(vote: Vote): Observable<any> {
    return this.http.post<{ votes: VoteRaw[], method: string, projectId: number }>(this.voteUrl, vote, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    });
  }

  get(projectId: number): Observable<{ pagination: Pagination; vote: Vote }> {
    return this.http.get<{ votes: VoteRaw[], pagination: Pagination }>(
      this.voteProjectUrl + '/' + projectId
      + '?'
      + `sort=modified`
      + `&direction=desc`
      + `&limit=1`,
      {
        headers: new HttpHeaders({'Accept': 'application/json'})
      }).pipe(
      map(rawData => {
        if (rawData.votes.length > 0) {
          return {
            vote: new Vote(
              rawData.votes[0].id,
              rawData.votes[0].president_id,
              rawData.votes[0].project_id,
              rawData.votes[0].yes_counter,
              rawData.votes[0].no_counter,
              rawData.votes[0].abstentions_counter,
              rawData.votes[0].no_words_counter,
              rawData.votes[0].prendre_acte,
              rawData.votes[0].resultat,
              rawData.votes[0].comment,
              rawData.votes[0].opinion,
              rawData.votes[0].method,
            ),
            pagination: rawData.pagination
          };
        } else {
          return {vote: undefined, pagination: rawData.pagination};
        }
      }));
  }
}
