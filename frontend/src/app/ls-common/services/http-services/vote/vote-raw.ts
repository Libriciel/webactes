
export class VoteRaw {
  id?: number;
  actor_id ?: number;
  president_id : number;
  project_id : number;
  sitting_id: number
  structure_id : number;
  yes_counter : number;
  no_counter : number;
  abstentions_counter : number;
  no_words_counter : number;
  prendre_acte : boolean;
  resultat : boolean;
  comment : string;
  opinion : boolean;
  abstentions: boolean;
  method?: string;


  constructor(president_id: number, project_id: number, sitting_id: number, yes_counter?: number, no_counter?: number, abstentions_counter?: number, no_words_counter?: number, prendre_acte?: boolean, resultat?: boolean, comment?: string, opinion?: boolean, method?: string, id?: number, actor_id?: number) {
    this.id = id;
    this.president_id = president_id;
    this.project_id = project_id;
    this.sitting_id = sitting_id;
    this.yes_counter = yes_counter;
    this.no_counter = no_counter;
    this.abstentions_counter = abstentions_counter;
    this.no_words_counter = no_words_counter;
    this.prendre_acte = prendre_acte;
    this.resultat = resultat;
    this.comment = comment;
    this.opinion = opinion;
    this.method = method;
    this.actor_id = actor_id;
  }
}

