import { Role } from '../../../../model/role';
import { UserAvailableActionsRaw } from './user-available-actions-raw';
import { Organization } from '../../../../model/organization';
import { StructureInfo } from '../../../../model/structure-info';
import { StructureRaw } from '../info/structure-raw';
import { User } from '../../../../model/user/user';

export class UserRaw {
  id: number;
  active: boolean;
  civility: string;
  firstname: string;
  lastname: string;
  email: string;
  nom_complet_court: string;
  nom_complet: string;
  phone: string;
  services: any[];
  role: Role;
  roles: [{ id: number, name: string }];
  structure_id: number;
  structures: StructureInfo[];
  username: string;
  name: string;
  availableActions: UserAvailableActionsRaw;

  constructor(user: User) {
    this.id = user.id;
    this.active = user.active;
    this.civility = user.civility;
    this.firstname = user.firstname;
    this.lastname = user.lastname;
    this.email = user.email;
    this.nom_complet_court = user.nom_complet_court;
    this.nom_complet_court = user.nom_complet_court;
    this.phone = user.phone;
    this.services = user.services;
    this.role = user.role;
    this.roles = [{
        id: parseInt(user.roles.map(role => role.id).toString()),
        name: user.roles.map(role => role.name).toString()
    }];
    this.structure_id = user.structure_id;
    this.structures = user.structures;
    this.username = user.username;
    this.name = user.name;
  }

}


