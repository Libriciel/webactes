export interface UserAvailableActionsRaw{
  can_be_edited: { value: boolean, data: string };
  can_be_deleted: { value: boolean, data: string };
}
