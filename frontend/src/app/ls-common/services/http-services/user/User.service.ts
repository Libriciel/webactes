import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from '../../../../model/user/user';
import { KeycloakService } from 'keycloak-angular';
import { Pagination } from '../../../../model/pagination';
import { UserRaw } from './user-raw';
import { FilterType } from '../../../../model/util/filter-type';
import { StructureInfo } from '../../../../model/structure-info';
import { Role } from '../../../../model/role';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private USERS_PATH = Config.baseUrl + '/api/v1/users';
  private ORGANIZATION_USERS_PATH = this.USERS_PATH + '/getAllOrganizationUsers';
  private ACTIVE_USERS_PATH = this.USERS_PATH + '/getAllActiveUsers';
  private JSON_HEADERS = {
    headers: new HttpHeaders({'Accept': 'application/json'})
  }
  private ACCEPT_JSON_HEADERS = new HttpHeaders({'Accept': 'application/json'});


  constructor(
    protected http: HttpClient,
    protected kcService: KeycloakService,
  ) {
  }

  getAllUsers(numPage?: number, filters?: FilterType, limit?: number): Observable<{ users: User[], pagination: Pagination }> {
    return this.http.get<{ users: UserRaw[],pagination: Pagination }>(
      this.USERS_PATH + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : '')
      + (filters.active ? `&active=${filters.active}` : '')
      + (limit ? `&limit=${limit}` : `&limit=10`),
      this.JSON_HEADERS).pipe(
        map(rawData => ({
          users: rawData.users
            .map(raw => {
              const structuresArray = raw.structures.map(structure => new StructureInfo(structure));
              return new User(
                raw.id,
                raw.active,
                raw.civility,
                raw.firstname,
                raw.lastname,
                raw.email,
                raw.nom_complet_court,
                raw.nom_complet,
                raw.phone,
                raw.services,
                raw.role,
                raw.roles.map(role => { return new Role(role.id, role.name)}),
                raw.structure_id,
                // raw.structures,
                structuresArray,
                raw.username,
                raw.name,
                raw.availableActions
              );
            }),
          pagination: rawData.pagination
        })));
  }

  getAll(filters?: FilterType): Observable<{ users: User[] }> {
    return this.http.get<{ users: UserRaw[] }>(
      this.USERS_PATH + '?'
      + (filters.active ? `&active=${filters.active}` : '')
      + (`&limit=50`),
      this.JSON_HEADERS).pipe(
      map(rawData => ({
        users: rawData.users
          .map(userRaw => {
            const structuresArray = userRaw.structures.map(structure => new StructureInfo(structure));
            return new User(
              userRaw.id,
              userRaw.active,
              userRaw.civility,
              userRaw.firstname,
              userRaw.lastname,
              userRaw.email,
              userRaw.nom_complet_court,
              userRaw.nom_complet,
              userRaw.phone,
              userRaw.services,
              userRaw.role,
              userRaw.roles.map(role => { return new Role(role.id, role.name)}),
              userRaw.structure_id,
              // userRaw.structures,
              structuresArray,
              userRaw.username,
              userRaw.name,
              userRaw.availableActions
            );
          }),
      })));
  }

  getAllActiveUsers(): Observable<{ users: User[] }> {
  return this.http.get<{ users: UserRaw[] }>(this.ACTIVE_USERS_PATH, this.JSON_HEADERS).pipe(
    map(rawData => ({
      users: rawData.users.map(raw => {
        return new User(
          raw.id,
          raw.active,
          raw.civility,
          raw.firstname,
          raw.lastname,
          raw.email,
          raw.nom_complet_court,
          raw.nom_complet,
          raw.username
        )})
    })))
  }

  getAllOrganizationUsers(numPage?: number, filters?: FilterType, limit?: number): Observable<{ users: User[], pagination: Pagination }> {
    return this.http.get<{ users: UserRaw[],pagination: Pagination }>(
      this.ORGANIZATION_USERS_PATH + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : '')
      + (filters.active ? `&active=${filters.active}` : '')
      + (limit ? `&limit=${limit}` : `&limit=10`),
      this.JSON_HEADERS).pipe(
      map(rawData => ({
        users: rawData.users
          .map(raw => {
            const structuresArray = raw.structures.map(structure => new StructureInfo(structure));
            return new User(
              raw.id,
              raw.active,
              raw.civility,
              raw.firstname,
              raw.lastname,
              raw.email,
              raw.nom_complet_court,
              raw.nom_complet,
              raw.phone,
              raw.services,
              raw.role,
              raw.roles.map(role => { return new Role(role.id, role.name)}),
              raw.structure_id,
              // raw.structures,
              structuresArray,
              raw.username,
              raw.name,
              raw.availableActions
            );
          }),
        pagination: rawData.pagination
      })));
  }

  setRole(roleId: number): Observable<any> {
    return this.http.post<any>(this.USERS_PATH + `/setRole/${roleId}`, {roleId: roleId}, {headers: this.ACCEPT_JSON_HEADERS});
  }

  loginStructure(): Observable<any> {
    return this.http.post<any>(this.USERS_PATH + `/loginStructure`, {}, {headers: this.ACCEPT_JSON_HEADERS});
  }

  logout(): Observable<any> {
    return this.http.post<any>(this.USERS_PATH + `/logout`, {}, {headers: this.ACCEPT_JSON_HEADERS})
      .pipe(map(() => this.kcService.logout()));
  }

  getStructureInfo(): Observable<any> {
    return this.http.get<any>(Config.baseUrl + '/api/v1/structures/getStructureInfo', {headers: this.ACCEPT_JSON_HEADERS});
  }

  addUser(user: User) {
    return this.http.post<{ user: User }>(this.USERS_PATH, user, this.JSON_HEADERS);
  }

  updateUser(user: User): Observable<any> {
    const updatedRawUser = new UserRaw(user);
    return this.http.put<{ user: UserRaw }>(`${this.USERS_PATH}/${updatedRawUser.id}`, updatedRawUser, {headers: this.ACCEPT_JSON_HEADERS})
      .pipe(
        map(rawData => {
          // const structuresArray = rawData.userRaw ? rawData.userRaw.structures.map(structure => new StructureInfo(structure)) : null;
          return new User(
            rawData.user.id,
            rawData.user.active,
            rawData.user.civility,
            rawData.user.firstname,
            rawData.user.lastname,
            rawData.user.email,
            rawData.user.nom_complet_court,
            rawData.user.nom_complet,
            rawData.user.phone,
            rawData.user.services,
            rawData.user.role,
            rawData.user.roles.map(role => { return new Role(role.id, role.name)}),
            rawData.user.structure_id,
            rawData.user.structures,
            // structuresArray,
            rawData.user.username,
            rawData.user.name,
            rawData.user.availableActions
          );
        })
    );
  }

  deleteUser(user: User[]): Observable<any> {
    const id = user[0].id;
    return this.http.delete(this.USERS_PATH + '/' + id, this.JSON_HEADERS);
  }

  switchUserActiveState(user: User, active: boolean): Observable<{ success: boolean, user: User } | { success: boolean }> {
    let url: string;
    if (active) {
      url = this.USERS_PATH + '/' + user.id + '/' + 'activate';
    } else {
      url = this.USERS_PATH + '/' + user.id + '/' + 'deactivate';
    }
    return this.http.post<{ user: UserRaw, success: boolean }>(url, {}, this.JSON_HEADERS).pipe(
      map( rawData => ({
        users: new User(rawData.user.id),
        success: true
        })),
      catchError((error) => of({
        success: error.error.success
      }))
    );
  }
}
