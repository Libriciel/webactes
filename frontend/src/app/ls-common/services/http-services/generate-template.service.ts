import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Config } from '../../../Config';
import { GenerateTemplateRaw } from '../../../model/generate-template/generateTemplateRaw';
import { GenerateTemplate } from '../../../model/generate-template/generate-template';

@Injectable({
  providedIn: 'root'
})
export class GenerateTemplateService {

  private generateTemplateUrl = Config.baseUrl + '/api/v1/generate-templates';

  constructor(protected http: HttpClient) {
  }

  getAllByTypeCode($code): Observable<GenerateTemplate[]> {
    return this.http.get<{ generateTemplates: GenerateTemplateRaw[] }>(this.generateTemplateUrl + '/with-type-code/' + $code, {

      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map((generateTemplates: { generateTemplates: GenerateTemplateRaw[] }) => generateTemplates.generateTemplates
        .map(generateTemplateRaw => {
          return new GenerateTemplate(generateTemplateRaw);
        })
      ));
  }
}
