import { Injectable } from '@angular/core';
import { Config } from '../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Organization } from '../../../model/organization';
import { Pagination } from '../../../core';
import { FilterType } from '../../../model/util/filter-type';
import { StructureInfo } from '../../../model/structure-info';

@Injectable({
  providedIn: 'root'
})
export class OrganizationsService {

  private organizationsGetUrl = Config.baseUrl + '/api/v1/organizations';
  private structuresGetUrl = Config.baseUrl + '/api/v1/structures';

  constructor(protected http: HttpClient) {
  }

  getOrganizations(numPage?: number, filters?: FilterType): Observable<{ organizations: Organization[], pagination: Pagination }> {

    const url = this.organizationsGetUrl + '?'
      + 'paginate=false'
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : '');

    return this.http.get<{ organizations: Organization[], pagination: Pagination }>(url, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map(raw => ({
      organizations: raw.organizations,
      pagination: raw.pagination
    })));
  }

  getStructures(numPage: number, filters: FilterType) {
    const url = this.structuresGetUrl + '?'
      + (numPage ? `page=${numPage}` : `page=1`)
      + (filters && filters.searchedText ? `&name=${filters.searchedText}` : '');

    return this.http.get<{ structures: StructureInfo[], pagination: Pagination }>(url, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(map(raw => ({
      structures: raw.structures,
      pagination: raw.pagination
    })));
  }
}
