import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Nature } from '../../../model/Nature/nature';
import { Config } from '../../../Config';
import { NatureRaw } from '../../../model/Nature/natureRaw';

@Injectable({
  providedIn: 'root'
})
export class NatureService {

  private natureUrl = Config.baseUrl + '/api/v1/natures';

  constructor(protected http: HttpClient) {
  }

  getAllWithoutPagination(): Observable<Nature[]> {
    return this.http.get<{ natures: NatureRaw[] }>(this.natureUrl + '?paginate=false', {
      headers: new HttpHeaders({'Accept': 'application/json'})
    }).pipe(
      map((natures: { natures: NatureRaw[] }) => natures.natures
        .map(natureRaw => {
          return new Nature(natureRaw);
        })
      ));
  }
}
