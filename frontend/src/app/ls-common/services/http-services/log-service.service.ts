import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { Config } from '../../../Config';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  private log = Config.baseUrl + '/api/v1/logs';

  constructor(protected http: HttpClient) {
  }

  private static handleError(error: any) {
    console.error('Unable to sed log: ', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(errMessage);
    }
    return throwError('Server error');
  }

  send(log): Observable<any> {
    return this.http.post(this.log, log,
      {
        headers: new HttpHeaders({'Accept': 'application/json'})
      })
      .pipe(catchError(LogService.handleError));
  }
}
