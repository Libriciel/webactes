import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { JoinDocumentType } from '../../../../model/join-document-type';

@Injectable({
  providedIn: 'root'
})
export class JoinDocumentTypeService {

  private documentTypesGetUrl = Config.baseUrl + '/api/v1/structures/getTypesPiecesJointes';

  constructor(protected http: HttpClient) {
  }

  getJoinDocumentsTypes(natureId: number): Observable<JoinDocumentType[]> {
    return this.http.get<{ typespiecesjointes: any }>(`${this.documentTypesGetUrl}/${natureId}`, {
      headers: new HttpHeaders({'Accept': 'application/json'})
    })
      .pipe(
        map(typespiecesjointes => typespiecesjointes.typespiecesjointes
          .map(joinDocumentTypeRaw => new JoinDocumentType(joinDocumentTypeRaw)))
      );
  }
}
