export interface JoinDocumentTypeRaw {
  id: number;
  libelle: string;
  // noinspection SpellCheckingInspection
  codetype: string;
}
