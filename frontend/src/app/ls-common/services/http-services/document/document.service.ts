import { Injectable } from '@angular/core';
import { Config } from '../../../../Config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Annexe } from '../../../../projects/model/annexe';
import { AnnexeRaw } from './annexe-raw';
import { DocumentRaw } from './document-raw';
import { WADocument } from '../../../../projects/model/wa-document';
import { JoinDocumentType } from '../../../../model/join-document-type';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  private annexeUrl = Config.baseUrl + '/api/v1/files';
  private projectUrl = Config.baseUrl + '/api/v1/projects';
  private MAIN_DOCUMENT_IPARAPHEUR_PATH = 'signature/signed-document';
  private SIGNING_FORM_IPARAPHEUR_PATH = 'signature/sign-bordereau';
  private HISTORY_IPARAPHEUR_PATH = 'signature/iparapheur-historique-file';
  private generateUrl = Config.baseUrl + '/api/v1/generate';

  constructor(protected http: HttpClient) {
  }

  static createAnnexeFromAnnexeRaw(annexeRaw: AnnexeRaw): Annexe {
    const annexeFile = annexeRaw.files ? annexeRaw.files[0] : annexeRaw.file;
    return new Annexe(
      {
        rank: annexeRaw.rank,
        joinGenerate: annexeRaw.is_generate,
        toGotTransfer: annexeRaw.istransmissible,
        waId: annexeFile.annex_id,
        fileId: annexeFile.id,
        mimeType: annexeFile.mimetype,
        name: annexeFile.name,
        size: annexeFile.size,
        codeType: annexeRaw.typespiecesjointe ? annexeRaw.typespiecesjointe.codetype : annexeRaw.codetype,
        documentType: annexeRaw.typespiecesjointe ? new JoinDocumentType(annexeRaw.typespiecesjointe) : null,
        hasGenerationProblem: !annexeRaw.hasGenerationProblem
          ? {value: false, message: '', date: null}
          : annexeRaw.hasGenerationProblem
      });
  }

  static createDocumentFromDocumentRaw(documentRaw: DocumentRaw, generationError: { message: string, date: Date }) {
    if (!documentRaw.files || !documentRaw.files[0]) {
      return new WADocument({
        waId: null,
        fileId: null,
        mimeType: null,
        name: null,
        size: null,
        codeType: documentRaw.typespiecesjointe ? documentRaw.typespiecesjointe.codetype : null,
        documentType: documentRaw.typespiecesjointe ? new JoinDocumentType(documentRaw.typespiecesjointe) : null,
        modified: documentRaw.modified,
        generationerror: generationError ? generationError : {message: '', date: null}
      });
    }
    const document = documentRaw.files[0];
    return new WADocument({
      waId: document.maindocument_id,
      fileId: document.id,
      mimeType: document.mimetype,
      name: document.name,
      size: document.size,
      codeType: documentRaw.typespiecesjointe ? documentRaw.typespiecesjointe.codetype : null,
      documentType: documentRaw.typespiecesjointe ? new JoinDocumentType(documentRaw.typespiecesjointe) : null,
      modified: documentRaw.modified,
      generationerror: documentRaw.generationerror
    });
  }

  get(id: number): Observable<any> {
    return this.http.get(`${this.annexeUrl}/${id}`, {responseType: 'blob'});
  }

  getStampedAct(projectId: number): Observable<any> {
    return this.http.get(`${this.projectUrl}/getActeTamponne/${projectId}`, {responseType: 'blob'});
  }

  getActAR(projectId: number): Observable<any> {
    return this.http.get(`${this.projectUrl}/getArActes/${projectId}`, {responseType: 'blob'});
  }

  getBordereau(projectId: number): Observable<any> {
    return this.http.get(`${this.projectUrl}/getBordereau/${projectId}`, {responseType: 'blob'});
  }

  getStampedAnnex(annexId: number, projectId: number): Observable<any> {
    return this.http.get(`${this.projectUrl}/getAnnexTamponne/${projectId}/${annexId}`, {responseType: 'blob'});
  }

  getIParapheurSignedMainDocument(projectId: number): Observable<any> {
    return this.http.get(`${this.projectUrl}/${this.MAIN_DOCUMENT_IPARAPHEUR_PATH}/${projectId}`, {responseType: 'blob'});
  }

  getIParapheurSignedFormDocument(projectId: number): Observable<any> {
    return this.http.get(`${this.projectUrl}/${this.SIGNING_FORM_IPARAPHEUR_PATH}/${projectId}`, {responseType: 'blob'});
  }

  getIParapheurHistory(projectId: number): Observable<any> {
    return this.http.get(`${this.projectUrl}/${this.HISTORY_IPARAPHEUR_PATH}/${projectId}`, {responseType: 'blob'});
  }

  getDownloadProject(projectId: number): Observable<any> {
    return this.http.get(`${this.generateUrl}/file-generate-project/${projectId}`, {responseType: 'blob', observe: 'response'});
  }

  getDownloadAct(projectId: number): Observable<any> {
    return this.http.get(`${this.generateUrl}/file-generate-act/${projectId}`, {responseType: 'blob'});
  }

  getDownloadDeliberationsList(sittingId: number): Observable<any> {
    return this.http.get(`${this.generateUrl}/file-generate-deliberations-list/${sittingId}`, {responseType: 'blob', observe: 'response'});
  }

  getDownloadVerbalTrial(sittingId: number): Observable<any> {
    return this.http.get(`${this.generateUrl}/file-generate-verbal-trial/${sittingId}`, {responseType: 'blob', observe: 'response'});
  }

  generateSittingDocument(summonId: number): Observable<any> {
    return this.http.get(`${this.generateUrl}/file-generate-document-summon/${summonId}`, {headers: new HttpHeaders({'Accept': 'application/json'})});
  }
}
