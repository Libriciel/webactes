export interface FileRaw {
  annex_id: number;
  // noinspection SpellCheckingInspection
  maindocument_id?: number;
  id: number;
  // noinspection SpellCheckingInspection
  mimetype: string;
  name: string;
  size: number;
}
