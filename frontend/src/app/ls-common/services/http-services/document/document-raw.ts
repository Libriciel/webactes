import { FileRaw } from './file-raw';
import { JoinDocumentTypeRaw } from '../joinDocumentType/join-document-type-raw';

export interface DocumentRaw {
  generationerror: { message: string; date: Date };

  files: FileRaw[];
  // noinspection SpellCheckingInspection
  typespiecesjointe: JoinDocumentTypeRaw;
  modified: Date;

}
