import { FileRaw } from './file-raw';
import { JoinDocumentTypeRaw } from '../joinDocumentType/join-document-type-raw';

export interface AnnexeRaw {
  hasGenerationProblem?: { value: boolean; message: string, date: Date };
  id?: number;
  files?: FileRaw[];
  file?: FileRaw;
  rank: number;
  is_generate: boolean;
  // noinspection SpellCheckingInspection
  istransmissible: boolean;
  // noinspection SpellCheckingInspection
  codetype?: string;
  // noinspection SpellCheckingInspection
  typespiecesjointe: JoinDocumentTypeRaw;
}
