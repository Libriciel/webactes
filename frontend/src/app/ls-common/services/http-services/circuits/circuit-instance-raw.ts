import { StepInstanceRaw } from './step-instance-raw';

export class CircuitInstanceRaw {
  id: number;
  circuitId: number;
  name: string;
  description?: string;
  steps: StepInstanceRaw[];
}
