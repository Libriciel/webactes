import { User } from 'src/app/model/user/user';
import { StepState } from 'src/app/model/circuit/step-instance';

export class StepInstanceRaw {
  id: string;
  name: string;
  validators: User[];
  state: StepState;
  comment?: string;
}
