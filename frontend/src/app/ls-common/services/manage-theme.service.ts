import { Theme } from '../../model/theme/theme';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pagination } from 'src/app/model/pagination';
import { FilterType } from 'src/app/model/util/filter-type';
import { ThemeService } from './http-services/themes/theme.service';
import { IManageThemeService } from 'src/app/admin/theme/services/IManageThemeService';
import { FlattenTreeItem } from '../../utility/flatten-tree-item';
import { map } from 'rxjs/operators';
import { TreeUtils } from '../../utility/TreeUtils';


@Injectable({
  providedIn: 'root'
})
export class ManageThemeService implements IManageThemeService {

  constructor(
    protected backendThemeService: ThemeService
  ) {
  }

  getAll(filters?: FilterType, numPage?: number): Observable<{ themes: Theme[], pagination: Pagination }> {
    return this.backendThemeService.getAll(filters, numPage);
  }

  getAllThemes(filters?: FilterType): Observable<{ themes: Theme[] }> {
    return this.backendThemeService.getAllThemes(filters);
  }

  getTheme(id: number): Observable<Theme> {
    return this.backendThemeService.getTheme(id);
  }

  getActiveThemesTree(filters?: FilterType): Observable<FlattenTreeItem[]> {
    return this.getAllThemes(filters)
      .pipe(map(res => TreeUtils.flatten(res.themes, 0, [])));
  }

  addOrUpdateTheme(theme: Theme): Observable<Theme> {
    return theme.id
      ? this.backendThemeService.updateTheme(theme)
      : this.backendThemeService.addTheme(theme);
  }

  deleteThemes(theme: Theme[]): Observable<boolean> {
    return this.backendThemeService.deleteThemes(theme);
  }

  switchThemeActiveState(theme: Theme, active: boolean): Observable<{ success: boolean, theme: Theme } | { success: boolean }> {
    return this.backendThemeService.switchThemeActiveState(theme, active);
  }

}
