import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Role } from '../../model/role';
import { RoleService } from './http-services/Role.service';
import { IManageRoleService } from '../../admin/role/services/IManageRoleService';

@Injectable({
  providedIn: 'root'
})
export class ManageRoleService implements IManageRoleService {

  constructor(protected roleService: RoleService) {
  }

  getRoles(): Observable<{ roles: Role[] }> {
    return this.roleService.getAll();
  }

  getPermissions(id): Observable<any> {
    return this.roleService.getPermissions(id);
  }
}
