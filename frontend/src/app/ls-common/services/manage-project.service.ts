import { Injectable } from '@angular/core';
import { IManageProjectsService } from '../../projects/services/i-manage-projects.service';
import { Observable, of } from 'rxjs';
import { StatesAct } from '../../model/states-act';
import { TypesAct } from '../../model/type-act/types-act';
import { TypesActService } from './http-services/type-act/types-act.service';
import { map, mergeMap } from 'rxjs/operators';
import { StateactService } from './http-services/stateact.service';
import { ProjectService } from './http-services/project/project.service';
import { DocumentService } from './http-services/document/document.service';
import { Project } from '../../model/project/project';
import { saveAs } from 'file-saver';
import { ManageDocumentTypesService } from './manage-document-types.service';
import { JoinDocumentType } from '../../model/join-document-type';
import { Pagination } from '../../model/pagination';
import { IManageCircuitInstanceService } from 'src/app/projects/services/i-manage-circuit-instances';
import { Config } from '../../Config';
import { FileUtils } from '../../utility/Files/file-utils';
import { ActTeletransmissionInformation } from 'src/app/model/act-teletransmission-information';
import { FilterType } from '../../model/util/filter-type';
import { ProjectDisplayState } from 'src/app/model/project/project-display-state.enum';
import { ProjectDocumentsInformation } from '../../projects/components/forms/project-documents/project-documents-information';

@Injectable({
  providedIn: 'root'
})
export class ManageProjectService implements IManageProjectsService {

  constructor(protected typesActService: TypesActService,
              protected stateActService: StateactService,
              protected projectService: ProjectService,
              protected documentService: DocumentService,
              protected manageDocumentTypesService: ManageDocumentTypesService,
              protected circuitInstanceService: IManageCircuitInstanceService) {
  }

  getAllStatesActs(action: string, id?: number): Observable<StatesAct[]> {
    return this.stateActService.getAll(action, id);
  }

  getAllTypesActs(): Observable<{ typeacts: TypesAct[] }> {
    return this.typesActService.getAllWithoutPagination();
  }

  getTypesActs(): Observable<{ typeacts: TypesAct[] }> {
    return this.typesActService.getTypesActs();
  }

  getAllTypesActsActive(): Observable<{ typeacts: TypesAct[] }> {
    return this.typesActService.getAllActive();
  }

  getJoinDocumentTypes(typeAct: TypesAct): Observable<{
    mainDocumentTypes: JoinDocumentType[],
    annexeDocumentTypes: JoinDocumentType[]
  }> {
    return this.manageDocumentTypesService.getJoinDocumentsTypes(typeAct)
      .pipe(
        map(documentTypes => ({
          mainDocumentTypes: documentTypes.filter(documentType => documentType.codeType.startsWith('99')),
          annexeDocumentTypes: documentTypes
        })));
  }

  addProject(project: Project): Observable<any> {
    return this.projectService.addProject(project);
  }

  updateProject(project: Project): Observable<any> {
    return this.projectService.updateProject(project);
  }

  getProject(project_id): Observable<Project> {
    return this.projectService.get(project_id);
  }

  updateTeletransmissionInfo(projectId: number, newInfo: ActTeletransmissionInformation): Observable<Project> {
    let actualProject: Project = null;
    return this.getProject(projectId).pipe(
      mergeMap(project => {
        project.updateTransmissionInfo(newInfo);
        actualProject = project;
        return this.updateProject(project);
      }),
      map(() => actualProject)
    );
  }

  getFile(doc_id: number, mimeType: string, name: string): Observable<any> {
    return this.documentService.get(doc_id).pipe(
      map(blob => {
        const blobPart = new Blob([blob], {type: mimeType});
        saveAs(blobPart, name);
      }));
  }

  checkCodeActUniq(code): Observable<boolean> {
    return code && code.length > 0
      ? this.projectService.checkCode(code).pipe(map(data => data.status === 200 && data.body && data.body.valid))
      : new Observable((observer) => {
        observer.next(true);
        observer.complete();
      });
  }

  getProjects(projectState: ProjectDisplayState, numPage: number, filters?: FilterType):
    Observable<{ projects: Project[], pagination: Pagination }> {

    const s = ProjectDisplayState;
    let getAllProjects$;
    switch (projectState) {
      case s.DRAFT: {
        getAllProjects$ = this.projectService.getAllDrafts(numPage, filters);
        break;
      }
      case s.TO_BE_VALIDATED: {
        getAllProjects$ = this.projectService.getAllToValidate(numPage, filters);
        break;
      }
      case s.VALIDATING: {
        getAllProjects$ = this.projectService.getAllValidating(numPage, filters);
        break;
      }
      case s.VALIDATED_INTO_SITTING:
      case s.VALIDATED: {
        getAllProjects$ = this.projectService.getAllValidated(numPage, filters);
        break;
      }

      case s.TO_BE_TRANSMITTED: {
        getAllProjects$ = this.projectService.getAllToTransmit(numPage, filters);
        break;
      }
      case s.TRANSMISSION_READY: {
        getAllProjects$ = this.projectService.getAllReadyToTransmit(numPage, filters);
        break;
      }
      case s.ACT: {
        getAllProjects$ = this.projectService.getAll(numPage, filters);
        break;
      }

      case s.UNASSOCIATED: {
        getAllProjects$ = this.projectService.getAllUnAssociated(numPage, filters);
        break;
      }

      default: {
        console.warn('ProjectService::getProjects - Invalid project type : ', projectState);
        getAllProjects$ = of({projects: [], pagination: {page: 0, count: 0, perPage: 0}});
      }

    }

    // = this.projectService.getAll(projectState, numPage);

    // Here we artificially map a circuitInstance to each project, if one was set in the stub service.
    return getAllProjects$.pipe(
      map((entry: { projects: Project[], pagination: Pagination }) => {
        const mappedProjects = entry.projects.map(projet => {
          return projet;
        });
        return {projects: mappedProjects, pagination: entry.pagination};
      })
    );

  }

  validateProject(project: Project, comment: string): Observable<any> {
    return this.circuitInstanceService.validateCurrentStep(project.circuit, comment);
  }

  rejectProject(project: Project, comment: string): Observable<any> {
    return this.circuitInstanceService.rejectCurrentStep(project.circuit, comment);
  }

  validateProjectInEmergency(project: Project, comment: string): Observable<any> {
    return this.circuitInstanceService.validateProjectInEmergency(project, comment);
  }

  checkDocumentsSize(project: Project | ProjectDocumentsInformation): boolean {
    let total = 0;
    if (project.mainDocument) {
      total += project.mainDocument.size;
    }
    if (project.annexes && project.annexes.length) {
      for (const annex of project.annexes) {
        total += annex.size;
      }
    }
    return total <= Config.MAX_PROJECT_SIZE;
  }

  hasAcceptableMainDocumentTypeBeforeSigning(file: File): boolean {
    return FileUtils.fileHasAcceptableType(file, Config.mainDocumentAcceptedTypesBeforeSigning);
  }

  hasAcceptableMainDocumentTypeForSigning(file: File): boolean {
    return FileUtils.fileHasAcceptableType(file, Config.mainDocumentAcceptedTypesForSigning);
  }

  hasAcceptableAnnexeType(fileList: FileList): boolean {
    return FileUtils.filesHaveAcceptableTypes(fileList, Config.annexesAcceptedTypes);
  }

  getAnnexesFileExtensions(): string {
    return Config.annexesAcceptedTypes.map(fileType => FileUtils.getFileExtension(fileType)).join(',');
  }

  getMainDocumentFileExtensionsBeforeSigning(): string {
    return Config.mainDocumentAcceptedTypesBeforeSigning.map(fileType => FileUtils.getFileExtension(fileType)).join(',');
  }

  manuallySignProject(project: Project, date: Date): Observable<any> {
    return this.projectService.manuallySignProject(project, date);
  }

  deleteProjects(projects: Project[]): Observable<any> {
    return this.projectService.deleteProjects(projects);
  }

  getStampedAct(projectId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getStampedAct(projectId).pipe(
      map(blob => {
        const blobPart = new Blob([blob], {type: mimeType});
        saveAs(blobPart, fileName);
      }));
  }

  getActAR(projectId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getActAR(projectId).pipe(
      map(blob => {
        const blobPart = new Blob([blob], {type: mimeType});
        saveAs(blobPart, fileName);
      }));
  }

  getBordereau(projectId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getBordereau(projectId).pipe(
      map(blob => {
        const blobPart = new Blob([blob], {type: mimeType});
        saveAs(blobPart, fileName);
      }));
  }

  getStampedAnnex(annexId: number, projectId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getStampedAnnex(annexId, projectId).pipe(
      map(blob => {
        const blobPart = new Blob([blob], {type: mimeType});
        saveAs(blobPart, fileName);
      }));
  }

  getIParapheurSignedMainDocument(projectId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getIParapheurSignedMainDocument(projectId).pipe(
      map(blob => {
        const blobPart = new Blob([blob], {type: mimeType});
        saveAs(blobPart, fileName);
      }));
  }

  getIParapheurSignedFormDocument(projectId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getIParapheurSignedFormDocument(projectId).pipe(
      map(blob => {
        const blobPart = new Blob([blob], {type: mimeType});
        saveAs(blobPart, fileName);
      }));
  }

  getIParapheurHistory(projectId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getIParapheurHistory(projectId).pipe(
      map(blob => {
        const blobPart = new Blob([blob], {type: mimeType});
        saveAs(blobPart, fileName);
      }));
  }

  getDownloadProject(projectId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getDownloadProject(projectId).pipe(
      map(response => {
        console.log('response', response.headers.get('X-Custom-filename'));
        this.downloadFileFromResponse(response, `Projet_${projectId}.pdf`);
      }));
  }

  getDownloadAct(projectId: number, mimeType: string, fileName: string): Observable<any> {
    return this.documentService.getDownloadAct(projectId).pipe(
      map(blob => {
        const blobPart = new Blob([blob], {type: mimeType});
        saveAs(blobPart, fileName);
      }));
  }

  generateActNumber(projectId: number): Observable<{ success: boolean, generatedActNumber: string }> {
    return this.projectService.generateActNumber(projectId);
  }

  private downloadFileFromResponse(response: any, defaultFileName: string): void {
    const contentDispositionHeader = response.headers.get('X-Custom-Filename');
    let extractedFileName = defaultFileName; // Default filename if not found in headers
    if (contentDispositionHeader) {
      extractedFileName = response.headers.get('X-Custom-Filename');
    }

    const blobPart = new Blob([response.body], {type: response.body.type});

    // Trigger file download
    saveAs(blobPart, decodeURIComponent(extractedFileName));
  }
}
