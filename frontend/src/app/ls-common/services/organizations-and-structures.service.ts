import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OrganizationsImportService } from './http-services/organizations-import.service';
import { IOrganizationsAndStructuresService } from '../../admin/organizations/serviceInterfaces/iorganizations-and-structures-service';
import { PastellStructure } from '../../model/pastell-structure';
import { map } from 'rxjs/operators';
import { Organization } from '../../model/organization';
import { OrganizationsService } from './http-services/organizations.service';
import { Pagination } from '../../model/pagination';
import { FilterType } from '../../model/util/filter-type';
import { StructureInfo } from '../../model/structure-info';

@Injectable({
  providedIn: 'root'
})

export class OrganizationsAndStructuresService implements IOrganizationsAndStructuresService {

  constructor(
    protected organizationsImportService: OrganizationsImportService,
    protected organizationsService: OrganizationsService) {
  }

  getOrganizationsAndStructuresToBeImported(): Observable<PastellStructure[]> {
    return this.organizationsImportService.getOrganizationsAndStructuresToBeImported()
      .pipe(map(pastellStructures =>
        pastellStructures
          .map(rawPastellStructure => new PastellStructure(rawPastellStructure))));
  }

  createOrganizationsAndStructures(organizationsAndStructuresMap: number[][]): Observable<any> {
    return this.organizationsImportService.createOrganizationsAndStructures(organizationsAndStructuresMap);
  }

  getOrganizationsAndStructures(numPage?: number, filters?: FilterType): Observable<{ organizations: Organization[], pagination: Pagination }> {
    return this.organizationsService.getOrganizations(numPage, filters)
      .pipe(map(result => {
        return {
          organizations: result.organizations,
          pagination: result.pagination
        };
      }));
  }

  getStructures(numPage?: number, filters?: FilterType): Observable<{ structures: StructureInfo[], pagination: Pagination }> {
    return this.organizationsService.getStructures(numPage, filters)
      .pipe(map(result => {
        return {
          structures: result.structures,
          pagination: result.pagination
        };
      }));
  }
}
