import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ReleaseNote } from '../../wa-common/model/release-note';
import { ApplicationVersionService } from './http-services/application-version-service';
import { IManageApplicationVersionService } from '../../wa-common/application-about/services/imanage-application-version-service';
import { Version } from '../../wa-common/model/version';

@Injectable({
  providedIn: 'root'
})
export class ManageApplicationVersionService implements IManageApplicationVersionService {

  constructor(protected applicationVersionService: ApplicationVersionService) {
  }

  getReleaseNotes(): Observable<ReleaseNote[]> {
    return this.applicationVersionService.getReleaseNotes();
  }

  getLicence(): Observable<string> {
    return this.applicationVersionService.getLicence();
  }

  getApplicationName(): Observable<Version> {
    return this.applicationVersionService.getApplicationVersion();
  }
}
