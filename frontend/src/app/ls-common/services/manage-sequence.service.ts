import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Sequence } from '../../model/sequence/sequence';
import { Pagination } from '../../model/pagination';
import { IManageSequenceService } from '../../admin/sequences/services/IManageSequenceService';
import { SequenceService } from './http-services/sequence/sequence.service';
import { FilterType } from '../../model/util/filter-type';

@Injectable({
  providedIn: 'root'
})
export class ManageSequenceService implements IManageSequenceService {

  constructor(protected sequenceService: SequenceService) {
  }

  addSequence(sequence: Sequence): Observable<Sequence> {
    return this.sequenceService.addSequence(sequence).pipe(
      map(new_sequence => new_sequence.sequence)
    );
  }

  getAll(numPage: number, filters: FilterType = {searchedText: ''}, limit: number): Observable<{ sequences: Sequence[], pagination: Pagination }> {
    return this.sequenceService.getAll(numPage, limit, filters);
  }

  updateSequence(sequence: Sequence) {
    return this.sequenceService.updateSequence(sequence);
  }

  deleteSequence(sequence: Sequence[]) {
    return this.sequenceService.deleteSequence(sequence);
  }

}
