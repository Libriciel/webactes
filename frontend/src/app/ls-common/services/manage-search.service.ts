import { Injectable } from '@angular/core';
import { IManageSearchService } from '../../search/services/IManageSearchService';
import { SearchService } from './http-services/search.service';
import { Observable } from 'rxjs';
import { Search } from '../../model/search';
import { Project } from '../../model/project/project';
import { Pagination } from '../../model/pagination';

@Injectable({
  providedIn: 'root'
})
export class ManageSearchService implements IManageSearchService {

  constructor(protected searchService: SearchService) {
  }

  postSearch(search: Search, numPage?: number): Observable<{ projects: Project[], pagination: Pagination }> {
    return this.searchService.postSearch(search, numPage);
  }
}
