import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IManageVotesService } from '../../projects/services/i-manage-votes.service';
import { Vote } from '../../model/vote/vote';
import { VotesService } from './http-services/vote/votes.service';

@Injectable({
  providedIn: 'root'
})
export class ManageVotesService implements IManageVotesService {



  constructor(protected voteService: VotesService){
  }

  add(vote: Vote, method: string, projectId: number): Observable<any> {
    return this.voteService.add(vote);
  }

}
