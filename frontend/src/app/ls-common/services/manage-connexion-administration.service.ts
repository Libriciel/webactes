import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IManageConnexionAdministrationService } from '../../admin/connexionPastell/services/imanage-connexion-administration-service';
import { map } from 'rxjs/operators';
import { ConnexionAdministrationService } from './http-services/connexionAdministration/connexion-administration.service';
import { ConnexionPastell } from '../../admin/connexionPastell/model/connexion-pastell';

@Injectable({
  providedIn: 'root'
})
export class ManageConnexionAdministrationService implements IManageConnexionAdministrationService {

  constructor(protected administrationService: ConnexionAdministrationService) {
  }

  checkPastellConnexion(connexionPastell: ConnexionPastell): Observable<boolean> {
    return this.administrationService.checkPastellConnexion(connexionPastell).pipe(map(value => value.success));
  }

  getPastellConnexion(): Observable<ConnexionPastell> {
    return this.administrationService.getPastellConnexion();
  }

  savePastellConnexion(connexionPastell: ConnexionPastell): Observable<any> {
    return this.administrationService.savePastellConnexion(connexionPastell);
  }

}
