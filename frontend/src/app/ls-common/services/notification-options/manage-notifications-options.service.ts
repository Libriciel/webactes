import { Injectable } from '@angular/core';
import { NotificationsOptionsService } from '../http-services/notification-options/notifications-options.service';
import { Observable } from 'rxjs';
import { WANotificationsOptions } from '../../../model/notification/notifications-options';
import { IManageNotificationsOptionsService } from '../../../admin/notifications-options/services/imanage-notifications-options-service';
import { Option } from '../../model/option';

@Injectable({
  providedIn: 'root'
})
export class ManageNotificationsOptionsService implements IManageNotificationsOptionsService {

  constructor(protected notificationsOptionsService: NotificationsOptionsService) {
  }

  getUserNotificationsOptions(): Observable<WANotificationsOptions> {
    return this.notificationsOptionsService.getUserNotificationsOptions();
  }

  saveNotificationOption(option: Option): Observable<null> {
    return this.notificationsOptionsService.saveNotificationOption(option);
  }
}
