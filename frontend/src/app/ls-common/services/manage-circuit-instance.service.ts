import { IManageCircuitInstanceService } from 'src/app/projects/services/i-manage-circuit-instances';
import { CircuitInstance } from 'src/app/model/circuit/circuit-instance';
import { Observable } from 'rxjs';
import { Project } from 'src/app/model/project/project';
import { Circuit } from 'src/app/model/circuit/circuit';
import { CircuitInstanceService } from './http-services/circuit-instance.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ManageCircuitInstanceService implements IManageCircuitInstanceService {
  constructor(protected backendCircuitInstanceService: CircuitInstanceService) {
  }

  sendProjectIntoCircuit(project: Project, circuit: Circuit): Observable<{ createdInstanceId: number }> {
    return this.backendCircuitInstanceService.sendProjectIntoCircuit(project.id, circuit.id);
  }

  validateCurrentStep(circuitInstance: CircuitInstance, comment: string): Observable<any> {
    return this.backendCircuitInstanceService.validateCurrentStep(circuitInstance, comment);
  }

  rejectCurrentStep(circuitInstance: CircuitInstance, comment: string): Observable<any> {
    return this.backendCircuitInstanceService.rejectCurrentStep(circuitInstance, comment);
  }

  validateProjectInEmergency(project: Project, comment: string): Observable<any> {
    return this.backendCircuitInstanceService.validateProjectInEmergency(project, comment);
  }
}
