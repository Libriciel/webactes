import { Injectable } from '@angular/core';
import { JoinDocumentTypeService } from './http-services/joinDocumentType/join-document-type.service';
import { Observable, of } from 'rxjs';
import { TypesAct } from '../../model/type-act/types-act';
import { JoinDocumentType } from '../../model/join-document-type';

@Injectable({
  providedIn: 'root'
})
export class ManageDocumentTypesService {

  constructor(protected joinDocumentTypeService: JoinDocumentTypeService) {
  }

  getJoinDocumentsTypes(typesAct: TypesAct): Observable<JoinDocumentType[]> {
    if (typesAct) {
      return this.joinDocumentTypeService.getJoinDocumentsTypes(typesAct.natureId ? typesAct.natureId : typesAct.nature.id);
    } else {
      return of([]);
    }
  }
}
