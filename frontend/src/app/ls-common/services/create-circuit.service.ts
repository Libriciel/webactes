import { Injectable } from '@angular/core';
import { ICreateCircuitService } from 'src/app/admin/circuits/services/ICreateCircuitService';
import { Observable } from 'rxjs';
import { Circuit } from 'src/app/model/circuit/circuit';
import { IManageCircuitService } from 'src/app/admin/circuits/services/IManageCircuitService';
import { CircuitService } from './http-services/circuit.service';
import { Pagination } from 'src/app/model/pagination';
import { FilterType } from 'src/app/model/util/filter-type';

@Injectable({
  providedIn: 'root'
})
export class CreateCircuitService implements ICreateCircuitService, IManageCircuitService {

  constructor(
    protected backendCircuitService: CircuitService,
  ) {
  }

  getAllCircuits(numPage?: number, filters?: FilterType): Observable<{ circuits: Circuit[], pagination: Pagination }> {

    return this.backendCircuitService.getAllCircuits(numPage, filters);

  }

  getAllActiveCircuits(): Observable<{ circuits: Circuit[] }> {
    return this.backendCircuitService.getAllActiveCircuits();
  }

  getCircuit(id: number): Observable<Circuit> {
    return this.backendCircuitService.getCircuit(id);
  }

  addCircuit(circuit: Circuit): Observable<Circuit> {
    return this.backendCircuitService.addCircuit(circuit);
  }

  updateCircuit(id: number, circuit: Circuit): Observable<Circuit> {
    return this.backendCircuitService.updateCircuit(id, circuit);
  }

  deleteCircuits(circuits: Circuit[]): Observable<boolean> {
    return this.backendCircuitService.deleteCircuits(circuits);
  }

  switchCircuitActiveState(circuit: Circuit, active: boolean): Observable<{ success: boolean, circuit: Circuit }> {
    return this.backendCircuitService.switchCircuitActiveState(circuit.id, active);
  }

}
