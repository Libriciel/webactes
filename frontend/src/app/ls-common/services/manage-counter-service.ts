import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FilterType } from '../../model/util/filter-type';
import { Pagination } from '../../model/pagination';
import { IManageCountersService } from '../../admin/counters/services/IManageCountersService';
import { CounterService } from './http-services/counters/counter.service';
import { Counter } from '../../model/counter/counter';
import { SequenceService } from './http-services/sequence/sequence.service';
import { Sequence } from '../../model/sequence/sequence';


@Injectable({
  providedIn: 'root'
})

export class ManageCountersService implements IManageCountersService {


  constructor(protected counterService: CounterService, protected sequenceService: SequenceService) {
  }

  getAllWithPagination(numPage?: number, filters?: FilterType, limit?: number): Observable<{ counters: Counter[]; pagination: Pagination }> {
    return this.counterService.getAllWithPagination(numPage, filters, limit);
  }

  getAllWithoutPagination(filters?: FilterType): Observable<{ counters: Counter[] }> {
    return this.counterService.getAllWithoutPagination(filters);
  }

  addCounter(counter: Counter): Observable<Counter> {
    return this.counterService.addCounter(counter).pipe(
      map(new_counter => new_counter.counter)
    );
  }

  updateCounter(counter: Counter) {
    return this.counterService.updateCounter(counter);
  }

  deleteCounter(counter: Counter[]) {
    return this.counterService.deleteCounter(counter);
  }

}
