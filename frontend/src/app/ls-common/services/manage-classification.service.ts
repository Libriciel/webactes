import { Injectable } from '@angular/core';
import { ClassificationService } from './http-services/classification.service';
import { IManageClassificationService } from '../../classification/services/i-manage-classification-service';
import { BehaviorSubject, Observable } from 'rxjs';
import { ClassificationDate } from '../../model/classification/classification-date';
import { map, tap } from 'rxjs/operators';
import { FlattenTreeItem } from '../../utility/flatten-tree-item';
import { TreeUtils } from '../../utility/TreeUtils';

@Injectable({
  providedIn: 'root'
})
export class ManageClassificationService implements IManageClassificationService {

  private classificationDate = new BehaviorSubject<ClassificationDate>(null);

  constructor(protected classificationService: ClassificationService) {
  }

  updateClassification(): Observable<null> {
    return this.classificationService.getAllClassification()
      .pipe(tap(() => this.classificationService.getClassificationDate()
        .subscribe(value => this.classificationDate.next(value))));
  }

  getClassificationDate(): Observable<ClassificationDate> {
    if (this.classificationDate) {
      this.classificationService.getClassificationDate()
        .subscribe(value => this.classificationDate.next(value));
    }
    return this.classificationDate;
  }

  getClassificationSubjects(): Observable<FlattenTreeItem[]> {
    return this.classificationService.getClassificationSubjects()
      .pipe(map(res => TreeUtils.flatten(res, 0, []).filter(value => value.depth > 0)));
  }

}
