import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from './http-services/user/User.service';
import { User } from '../../model/user/user';
import { IManageUserService } from '../../admin/users/i-manage-user';
import { FilterType } from '../../model/util/filter-type';
import { Pagination } from '../../model/pagination';

@Injectable({
  providedIn: 'root'
})
export class ManageUserService implements IManageUserService {

  constructor(protected userService: UserService) {
  }

  getAll(filters?: FilterType): Observable<{ users: User[] }> {
    return this.userService.getAll(filters);
  }

  getAllUsers(numPage?: number, filters?: FilterType, limit?: number): Observable<{ users: User[], pagination: Pagination  }> {
    return this.userService.getAllUsers(numPage, filters, limit);
  }

  getAllActiveUsers(): Observable<{ users: User[] }> {
    return this.userService.getAllActiveUsers();
  }

  getAllOrganizationUsers(numPage?: number, filters?: FilterType, limit?: number): Observable<{ users: User[], pagination: Pagination  }> {
    return this.userService.getAllOrganizationUsers(numPage, filters, limit);
  }

  setRole(roleId: number): Observable<any> {
    return this.userService.setRole(roleId);
  }

  addUser(user: User): Observable<{ user: User }> {
    return this.userService.addUser(user);
  }

  updateUser(user: User): Observable<{ user: User }> {
    return this.userService.updateUser(user);
  }

  deleteUser(user: User[]) {
    return this.userService.deleteUser(user);
  }

  switchUserActiveState(user: User, active: boolean): Observable<{ success: boolean, user: User } | { success: boolean }> {
    return this.userService.switchUserActiveState(user, active);
  }
}
