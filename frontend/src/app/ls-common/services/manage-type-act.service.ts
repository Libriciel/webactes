import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TypesAct } from '../../model/type-act/types-act';
import { TypesActService } from './http-services/type-act/types-act.service';
import { IManageTypeActService } from '../../admin/type-acts/services/IManageTypeActService';
import { TypeSitting } from '../../model/type-sitting/type-sitting';
import { FilterType } from '../../model/util/filter-type';
import { Pagination } from '../../model/pagination';
import { TypeactFilterType } from '../../model/util/typeact-filter-type';

@Injectable({
  providedIn: 'root'
})
export class ManageTypeActService implements IManageTypeActService {

  constructor(protected typeActService: TypesActService) {
  }

  getAllWithPagination(numPage?: number, filters?: FilterType): Observable<{ pagination: Pagination; typeacts: TypesAct[] }> {
    return this.typeActService.getAllWithPagination(numPage, filters);
  }

  getAllWithoutPagination(filters?: TypeactFilterType): Observable<{ typeacts: TypesAct[] }> {
    return this.typeActService.getAllWithoutPagination(filters);
  }

  getAll(): Observable<TypesAct[]> {
    return undefined;
  }

  deleteTypeActs(typesActs: TypesAct[]) {
    return this.typeActService.deleteTypeActs(typesActs);
  }

  switchTypeActActiveState(typeAct: TypesAct, value: boolean): Observable<{ typeAct: TypesAct; success: boolean } | { success: boolean }> {
    typeAct.active = value;
    return this.typeActService.switchTypeActActiveState(typeAct);
  }

  updateTypeAct(typeAct: TypesAct): Observable<TypesAct> {
    return this.typeActService.updateTypeAct(typeAct);
  }

  addTypeAct(typeAct: TypesAct): Observable<TypesAct> {
    return this.typeActService.addTypeAct(typeAct);
  }
}
