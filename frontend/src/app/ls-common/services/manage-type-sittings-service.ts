import { IManageTypeSittingService } from '../../admin/type-sitting/services/IManageTypeSittingService';
import { TypeSitting } from '../../model/type-sitting/type-sitting';
import { Observable } from 'rxjs';
import { Pagination } from '../../model/pagination';
import { Injectable } from '@angular/core';
import { TypeSittingService } from './http-services/type-sittings/type-sitting.service';
import { map } from 'rxjs/operators';
import { FilterType } from '../../model/util/filter-type';

@Injectable({
  providedIn: 'root'
})

export class ManageTypeSittingsService implements IManageTypeSittingService{


  constructor(protected typeSittingService: TypeSittingService ) {
  }

  addTypeSitting(typeSitting: TypeSitting): Observable<TypeSitting> {
    return this.typeSittingService.addTypeSitting(typeSitting).pipe(
      map(new_type_sitting => new_type_sitting.typeSitting)
    );
  }

  getAll(numPage?: number, filters?: FilterType, limit?: number): Observable<{ typeSittings: TypeSitting[]; pagination: Pagination }> {
    return this.typeSittingService.getAll(numPage, filters, limit);
  }

  updateTypeSitting(typeSitting: TypeSitting) {
    return this.typeSittingService.updateTypeSittings(typeSitting);
  }

  deleteTypeSitting(typeSittings: TypeSitting[]) {
    return this.typeSittingService.deleteTypeSittings(typeSittings);
  }

  switchTypeSittingActiveState(typeSitting: TypeSitting, value: boolean): Observable<{ typeSitting: TypeSitting; success: boolean} | { success: boolean }> {
    return this.typeSittingService.switchTypeSittingActiveState(typeSitting, value);
  }

}
