import { Action } from './action';

export interface ActionSet<T> {
  actions: (Action<T> | Action<void>)[];

}
