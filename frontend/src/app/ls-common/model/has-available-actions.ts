export class HasAvailableActions {
  private actions = new Map();

  hasAvailableAction(action): boolean {
    return this.actions.has(action) ? this.actions.get(action) : false;
  }

  setAvailableAction(action, hasAvailableAction: boolean): void {
    this.actions.set(action, hasAvailableAction);
  }
}
