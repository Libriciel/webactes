import { Observable, of } from 'rxjs';
import { HasId } from './has-id';
import { ActionResult, IActuator } from '../components';
import { Action } from './action';

export class ActionItem<T extends HasId> extends Action<T> {


  setDefaultActuatorIfNeeded() {
    this.actuator = this.actuator
      ? this.actuator
      : new class implements IActuator<T> {
        action(elements: T[]): Observable<ActionResult> {
          // eslint-disable-next-line no-console
          console.debug(self.name + ' clicked on elements ' + elements.map(value => value.id).join(','));
          return of({});
        }
      }();
  }
}
