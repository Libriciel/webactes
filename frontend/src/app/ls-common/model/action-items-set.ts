import { ActionItem } from './action-item';
import { HasId } from './has-id';

export interface ActionItemsSet<T extends HasId> {
  actionItems: (ActionItem<T>)[];
}
