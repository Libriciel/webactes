export class HasGenerationProblem {

    private generationerror: Map<any, any> = new Map();

    hasGenerationProblem(generationProblem: 'generationProblem'): { message: string, modified: Date } {
        return this.generationerror.get(generationProblem) ? this.generationerror.get(generationProblem) : {value: false, message: ''};
    }

    setHasGenerationProblem(generationProblem, problem: { message: string, date: Date }): void {
        this.generationerror.set(generationProblem, problem);
    }
}
