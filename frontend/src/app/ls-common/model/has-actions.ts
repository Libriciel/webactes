import { ListActions } from '../../core/models/list-actions';

export interface HasActions {
  availableActions: ListActions;
}
