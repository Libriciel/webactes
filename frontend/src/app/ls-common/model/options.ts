import {Option} from './option';

export interface Options {
  name: string;
  values: Option [];
}
