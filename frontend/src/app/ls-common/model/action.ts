import { Style } from '../../wa-common';
import { ActionResult, IActuator } from '../components';
import { IValidator } from '../components/tables/validator/i-validator';
import { CommonStylesConstants } from '../../wa-common/common-styles-constants';
import { Observable, of } from 'rxjs';

export class Action<T> {

  name: string;
  longName?: string;
  icon?: string;
  icon_stacked?: string;
  style?: Style;
  actuator: IActuator<T>;
  actionValidator?: IValidator<T>;
  enabled?: boolean;
  fullButton?: boolean;

  constructor(object: {
    name: string;
    longName?: string;
    icon?: string;
    icon_stacked?: string;
    style?: Style;
    actuator?: IActuator<T>;
    enabled?: boolean;
    fullButton?: boolean;
    actionValidator?: IValidator<T>;
  }) {
    this.name = object.name;
    this.longName = object.longName ? object.longName : object.name;
    this.icon = object.icon;
    this.icon_stacked = object.icon_stacked;
    this.style = object.style ? object.style : Style.NORMAL;
    this.actuator = object.actuator;
    this.actionValidator = object.actionValidator;
    this.enabled = object.enabled;
    this.fullButton = object.fullButton;
  }

  getStyleClass(): string {
    return CommonStylesConstants.getStyleClass(this.style);
  }

  setDefaultActuatorIfNeeded() {
    this.actuator = this.actuator
      ? this.actuator
      : new class implements IActuator<T> {
        action(elements: T[]): Observable<ActionResult> {
          // tslint:disable-next-line:no-console
          console.debug(self.name + ' clicked ' + elements.map(value => value).join(','));
          return of({});
        }
      }();
  }

  initializeItem() {
    this.icon = this.icon && this.icon !== '' ? this.icon : 'fa fa-cogs';
    this.enabled = this.enabled ? this.enabled : true;
    this.actionValidator = this.actionValidator
      ? this.actionValidator
      : new class implements IValidator<T> {
        isActionValid = (): boolean => true;
      }();
    this.setDefaultActuatorIfNeeded();
  }
}
