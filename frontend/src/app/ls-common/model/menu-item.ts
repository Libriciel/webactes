export class MenuItem {
  name: string;
  icon?: string;
  ref?: string;
  action?: () => void;
  subItems?: MenuItem[];
}
