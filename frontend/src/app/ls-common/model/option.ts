export interface Option {
  id: number;
  name: string;
  active: boolean;
}
