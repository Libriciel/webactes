import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileBtnComponent } from './components/file-btn/file-btn.component';
import { StepperComponent } from './components/stepper/stepper.component';
import { ListOrButtonComponent } from './components/list-or-button/list-or-button.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmPopupComponent } from './components';
import { TableCommonActionsComponent } from './components/tables/table-common-actions/table-common-actions.component';
import { TableSingleItemActionsComponent } from './components/tables/table-single-item-actions/table-single-item-actions.component';
import { ActionableDirective } from './components/tables/actionable.directive';
import { CheckAllComponent } from './components/tables/check-all/check-all.component';
import { CommonTableComponent } from './components/tables/common-table/common-table.component';
import { RouterModule } from '@angular/router';
import { SliderComponent } from './components/slider/slider.component';
import { ActionIconComponent } from './components/tables/action-icon/action-icon.component';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { CancelButtonComponent } from './components/buttons/cancel-button/cancel-button.component';
import { SaveButtonComponent } from './components/buttons/save-button/save-button.component';
import { MenuTilesComponent } from './components/menu-tiles/menu-tiles/menu-tiles.component';
import { MenuTileItemComponent } from './components/menu-tiles/menu-tile-item/menu-tile-item.component';
import { ToSnakeCasePipe } from './pipes/to-snake-case.pipe';
import { InfoPanelComponent } from './components/info-panel/info-panel.component';
import { TableRowActionComponent } from './components/tables/table-row-action/table-row-action.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { SpinnerBoxComponent } from './components/spinner-box/spinner-box.component';
import { ModalHeaderComponent } from './components/modal-header/modal-header.component';
import { AccordionPanelHeaderComponent } from './components/accordion/accordion-panel-header/accordion-panel-header.component';
import { ButtonComponent } from './components/buttons/button/button.component';
import { ActionButtonsComponent } from './components/action-buttons/action-buttons.component';
import { GoBackButtonComponent } from './components/buttons/go-back-button/go-back-button.component';
import { OptionsListComponent } from './components/options-list/options-list.component';
import { SearchButtonComponent } from './components/buttons/search-button/search-button.component';
import { ReinitButtonComponent } from './components/buttons/reinit-button/reinit-button.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSortModule } from '@angular/material/sort';
import { LsComposantsModule } from '@libriciel/ls-composants';

@NgModule({
  declarations: [
    FileBtnComponent,
    StepperComponent,
    ListOrButtonComponent,
    ConfirmPopupComponent,
    CommonTableComponent,
    ActionIconComponent,
    TableCommonActionsComponent,
    TableSingleItemActionsComponent,
    TableRowActionComponent,
    ActionableDirective,
    CheckAllComponent,
    SliderComponent,
    CancelButtonComponent,
    SaveButtonComponent,
    GoBackButtonComponent,
    ButtonComponent,
    SliderComponent,
    TooltipComponent,
    MenuTilesComponent,
    MenuTileItemComponent,
    ToSnakeCasePipe,
    InfoPanelComponent,
    SearchBarComponent,
    SpinnerBoxComponent,
    ModalHeaderComponent,
    AccordionPanelHeaderComponent,
    ActionButtonsComponent,
    SearchButtonComponent,
    ReinitButtonComponent,
    OptionsListComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    NgSelectModule,
    FormsModule,
    RouterModule,
    NgxSpinnerModule,
    DragDropModule,
    MatSortModule,
    LsComposantsModule
  ],
  exports: [
    FileBtnComponent,
    StepperComponent,
    ListOrButtonComponent,
    ConfirmPopupComponent,
    CommonTableComponent,
    TableCommonActionsComponent,
    TableSingleItemActionsComponent,
    TableRowActionComponent,
    SliderComponent,
    TooltipComponent,
    SliderComponent,
    CancelButtonComponent,
    SaveButtonComponent,
    GoBackButtonComponent,
    ButtonComponent,
    MenuTilesComponent,
    ToSnakeCasePipe,
    InfoPanelComponent,
    SearchBarComponent,
    SpinnerBoxComponent,
    ModalHeaderComponent,
    AccordionPanelHeaderComponent,
    ActionButtonsComponent,
    ActionableDirective,
    OptionsListComponent,
    SearchButtonComponent,
    ReinitButtonComponent
  ]
})
export class LibricielCommonModule {
}
