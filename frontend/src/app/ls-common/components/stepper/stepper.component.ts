import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';
import { StepperMessages } from './StepperMessages';
import { Weight } from '../../../wa-common/weight.enum';
import { CommonIcons } from '../../icons';

@Component({
  selector: 'wa-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
  providers: [{provide: CdkStepper, useExisting: StepperComponent}]
})
export class StepperComponent extends CdkStepper implements OnInit {
  messages = StepperMessages;
  commonIcons = CommonIcons;
  weight = Weight;

  @Input() isValid: boolean;
  @Input() isProcessing: boolean;
  @Output() selectedIndexChange = new EventEmitter();
  @Input() nextStepTitle: string;
  @Output() saveAction = new EventEmitter();
  @Output() closeAction = new EventEmitter();

  onClickNext() {
    this.selectedIndex = this.selectedIndex + 1;
    this.selectedIndexChange.emit(this.selectedIndex);
  }

  onClickPrevious() {
    this.selectedIndex = this.selectedIndex - 1;
    this.selectedIndexChange.emit(this.selectedIndex);
  }

  onClickCancel() {
    this.closeAction.emit();
  }

  onclickSave() {
    this.saveAction.emit();
  }

  ngOnInit(): void {
    this.isValid = false;
    this.isProcessing = false;
  }
}
