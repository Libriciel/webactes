import { Observable } from 'rxjs';
import { ActionResult } from './action-result';

export interface IActuator<T> {
  action: (argument: T[], contextParameters?: Object) => Observable<ActionResult>;
  needReload?: boolean;
}
