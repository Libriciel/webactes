export interface ActionResult {
  error?: boolean;
  needReload?: boolean;
  message?: string;
}
