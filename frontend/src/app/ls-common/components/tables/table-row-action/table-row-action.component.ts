import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActionItem } from '../../../model/action-item';
import { HasId } from '../../../model/has-id';
import { HasName } from '../../../model/has-name';

@Component({
  selector: 'wa-table-row-action',
  templateUrl: './table-row-action.component.html',
  styleUrls: ['./table-row-action.component.scss']
})
export class TableRowActionComponent<T extends HasId & HasName> implements OnInit {

  @Input() action: ActionItem<T>;
  @Input() item: T;
  @Input() label: string;
  @Output() actionResult = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  sendResult($event: any) {
    this.actionResult.emit($event);
  }

}
