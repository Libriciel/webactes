import { Component, EventEmitter, HostBinding, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { ActionItemsSet } from '../../../model/action-items-set';
import { CheckAllComponent } from '../check-all/check-all.component';
import { HasId } from '../../../model';
import { NotificationService } from '../../../services';
import { Pagination } from '../../../../model/pagination';
import { CommonMessages } from '../../../i18n';
import { CdkDragDrop } from '@angular/cdk/drag-drop';

@Component({
  selector: 'wa-table',
  templateUrl: './common-table.component.html',
  styleUrls: ['./common-table.component.scss']
})
export class CommonTableComponent<T extends HasId> implements OnInit {

  commonMessages = CommonMessages;
  @ViewChild(CheckAllComponent, {static: true}) checkAllComponent: CheckAllComponent;
  @Output() reloadElements = new EventEmitter();
  @Output() gotoPage = new EventEmitter();
  @Output() displayThemes = new EventEmitter();
  @Output() dropElement: EventEmitter<any> = new EventEmitter();
  @Input() commonActions: ActionItemsSet<HasId>[] = [];
  @Input() singleActions: ActionItemsSet<HasId> = {actionItems: []};
  @Input() singleActionsOthers: ActionItemsSet<HasId>[] = [];
  @Input() theader: TemplateRef<any>;
  @Input() tbody: TemplateRef<any>;
  @Input() inner = false;
  @Input() filtered = false;
  @Input() loading = true;
  @Input() isDraggable = false;
  @Input() pagination: Pagination = {page: 1, count: 1, perPage: 1};

  @HostBinding('class') class = 'ls-table ';
  @Input() isCommonActionsEnabled: boolean;
  selectedRows = [];

  constructor(
    protected notificationService: NotificationService) {
  }

  private _elements: HasId[];

  get elements(): HasId[] {
    return this._elements;
  }

  @Input() set elements(elements: HasId[]) {
    this.selectedRows = [];
    if (elements) {
      elements.forEach(element => this.selectedRows[element.id] = false);
    }
    this._elements = elements;
    if (this._elements) {
      this.loading = false;
    }
    this.setGroupSelectionCheckboxChecked(false);
  }

  get selectedElements(): HasId[] {
    return !this._elements ? [] : this._elements.filter(element => this.selectedRows [element.id]);
  }

  ngOnInit() {
  }

  selectRow() {
    this.setGroupSelectionCheckboxChecked(this.selectedRows.every(value => value));
    this.checkAvailableActions();
  }

  selectAll(event: any) {
    for (const selectedIndex of Object.keys(this.selectedRows)) {
      this.selectedRows[selectedIndex] = event;
    }
    this.checkAvailableActions();
  }

  isSelected(index: number) {
    return this.selectedRows[index];
  }

  hasCommonActions(): boolean {
    return this.commonActions &&
      this.commonActions.length > 0 &&
      this.commonActions.some(commonActions => commonActions.actionItems.length > 0);
  }

  hasSingleActions(): boolean {
    return (!!this.singleActions && this.singleActions.actionItems.length > 0)
      || (this.singleActionsOthers && this.singleActionsOthers.length > 0);
  }

  getSingleActionsColClass() {
    const nbActions = this.singleActions.actionItems.length + (this.singleActionsOthers.length > 0 ? 1 : 0);
    switch (nbActions) {
      case 1:
        return 'col-1';
      case 2:
        return 'col-1 wa-table-action-container';
      default :
        return 'col-2';
    }
  }

  public onPageChanged(numPage) {
    this.gotoPage.emit(numPage);
  }

  private checkAvailableActions() {
    this.isCommonActionsEnabled = this.selectedRows.filter(value => value).length >= 1;
  }

  public actionDone($event: { error: any; message: string; needReload: any; }) {
    if ($event) {
      if ($event.error) {
        if ($event.message) {
          this.notificationService.showError($event.message);
        }
        this.reloadElements.emit();
      } else {
        if ($event.message) {
          this.notificationService.showSuccess($event.message);
        }
        if ($event.needReload) {
          this.reloadElements.emit();
        }
      }
    }
    this.setGroupSelectionCheckboxChecked(false);
    this.selectAll(false);
  }

  private setGroupSelectionCheckboxChecked(val: boolean) {
    if (this.checkAllComponent) {
      this.checkAllComponent.setChecked(val);
    }
  }

  onDrop($event: CdkDragDrop<HasId>) {
    this.dropElement.emit($event);
    /*moveItemInArray(this._elements, $event.previousIndex, $event.currentIndex);
    this._elements.forEach((element, idx) => {
      element.id = idx + 1;
    });*/
  }
}
