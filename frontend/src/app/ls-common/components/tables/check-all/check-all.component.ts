import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'wa-check-all',
  templateUrl: './check-all.component.html',
  styleUrls: ['./check-all.component.scss']
})
export class CheckAllComponent implements OnInit {

  @Input() elements;
  @Output() selectAll = new EventEmitter();
  value: boolean;
  @ViewChild('inputElement', {static: true}) inputElement: ElementRef;

  constructor() {
  }

  ngOnInit() {
  }

  change(event) {
    this.selectAll.emit(!!event.target.checked);
  }

  setChecked(value: boolean) {
    this.value = value;
  }
}
