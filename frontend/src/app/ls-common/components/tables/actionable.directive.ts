import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { ActionItem, HasId } from '../../model';

@Directive({
  selector: '[waActionable]'
})
export class ActionableDirective<T extends HasId> {

  @Input() action: ActionItem<T>;
  @Input() argument: T[];
  @Output() done = new EventEmitter();
  processing = false;

  constructor() {
  }

  @HostListener('click', ['$event']) onClick() {
    this.processing = true;
    this.action.actuator.action(this.argument)
      .subscribe(actionResult => this.done.emit(actionResult))
      .add(() => this.processing = false);
  }

}
