import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { ActionItemsSet } from '../../../model/action-items-set';
import { ActionItem, HasId } from '../../../model';
import { Weight } from '../../../../wa-common/weight.enum';

@Component({
  selector: 'wa-table-common-actions',
  templateUrl: './table-common-actions.component.html',
  styleUrls: ['./table-common-actions.component.scss'],
})
export class TableCommonActionsComponent<T extends HasId> implements OnInit {

  @Input() @HostBinding() class: string;
  @Input() actionItemsSets: ActionItemsSet<T>[];
  @Input() arguments: any;
  @Output() needReload = new EventEmitter();
  @Output() actionResult = new EventEmitter();
  weight = Weight;

  constructor() {
  }

  private _enabled = true;

  @Input() set enabled(value: boolean) {
    this.setEnable(value);
    this._enabled = value;
  }

  ngOnInit(): void {
    this.actionItemsSets
      .forEach(itemSet => itemSet.actionItems
        .forEach(item => item.initializeItem()));
    this.setEnable(this._enabled);
  }

  sendResult($event: any) {
    this.actionResult.emit($event);
  }

  private forEachItem(f: (ActionItem) => void): void {
    this.actionItemsSets.forEach(itemSet =>
      itemSet.actionItems.forEach(item => f(item)));
  }

  private setEnable(enabled: boolean): void {
    this.forEachItem((item: ActionItem<T>) => item.enabled = enabled);
  }
}
