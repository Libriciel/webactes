import { Component, Input, OnInit } from '@angular/core';
import { ActionItem, HasId } from '../../../model';

@Component({
  selector: 'wa-action-icon',
  templateUrl: './action-icon.component.html',
  styleUrls: ['./action-icon.component.scss']
})
export class ActionIconComponent<T extends HasId> implements OnInit {

  @Input() item: ActionItem<T> = null;
  @Input() showFullButton = false;

  constructor() {
  }

  ngOnInit() {
  }

}
