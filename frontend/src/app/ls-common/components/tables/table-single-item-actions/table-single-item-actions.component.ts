import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';
import {ActionItemsSet} from '../../../model/action-items-set';
import {HasId} from '../../../model';
import {Weight} from '../../../../wa-common/weight.enum';
import {CommonIcons} from '../../../icons';
import {placements} from '@popperjs/core';

@Component({
    selector: 'wa-table-single-item-actions',
    templateUrl: './table-single-item-actions.component.html',
    styleUrls: ['./table-single-item-actions.component.scss'],
})
export class TableSingleItemActionsComponent<T extends HasId> implements OnInit {

    @Input() actionItems: ActionItemsSet<T>;
    @Input() otherActionItemsSets: ActionItemsSet<T>[];
    @Input() argument: any;
    @Input() @HostBinding() class: string;
    @Output() needReload = new EventEmitter();
    @Output() actionResult = new EventEmitter();
    commonIcons = CommonIcons;
    weight = Weight;

    ngOnInit(): void {
        if (this.actionItems) {
            this.actionItems.actionItems.forEach(item => item.initializeItem());
        }
        if (this.otherActionItemsSets) {
            this.otherActionItemsSets
                .forEach(itemSet => itemSet.actionItems
                    .forEach(item => item.initializeItem()));
        }
    }

    sendResult($event: any) {
        this.actionResult.emit($event);
    }

    isOtherActionItemsSetsEmpty(): boolean {
        return !this.otherActionItemsSets
            .some(itemSet => itemSet.actionItems
                .some(item => item.enabled && item.actionValidator.isActionValid([this.argument])));
    }

    protected readonly placements = placements;
}
