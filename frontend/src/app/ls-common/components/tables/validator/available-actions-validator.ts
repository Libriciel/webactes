import { IValidator } from './i-validator';
import { HasActions } from '../../../model/has-actions';

export class AvailableActionsValidator<T extends HasActions> implements IValidator<T> {

  private readonly action: any;

  constructor(action: any) {
    this.action = action;
  }

  isActionValid(items: T[]): boolean {
    switch (this.action) {
      case 'can_read':
        return items[0].availableActions.can_read.value;
      case 'can_modify':
        return items[0].availableActions.can_modify.value;
      case 'can_remove':
        return items[0].availableActions.can_remove.value;
      default:
        return false;
    }
  }
}
