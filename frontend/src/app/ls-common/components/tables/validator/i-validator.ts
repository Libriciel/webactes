export interface IValidator<T> {
  isActionValid: (argument: T[]) => boolean;
}
