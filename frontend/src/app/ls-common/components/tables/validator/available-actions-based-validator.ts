import { HasAvailableActions } from '../../../model/has-available-actions';
import { IValidator } from './i-validator';

export class AvailableActionsBasedValidator<T extends HasAvailableActions> implements IValidator<T> {

  private readonly action: any;

  constructor(action: any) {
    this.action = action;
  }

  isActionValid(items: T[]): boolean {
    return !items || items.length !== 1
      ? false
      : items[0].hasAvailableAction(this.action);

  }
}
