import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from '../../../model/menu-item';

@Component({
  selector: 'wa-menu-tiles',
  templateUrl: './menu-tiles.component.html',
  styleUrls: ['./menu-tiles.component.scss']
})
export class MenuTilesComponent implements OnInit {

  @Input() menu: MenuItem[] = [];

  constructor() {
  }

  ngOnInit() {
  }

}
