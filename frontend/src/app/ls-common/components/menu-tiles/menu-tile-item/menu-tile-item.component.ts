import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from '../../../model/menu-item';

@Component({
  selector: 'wa-menu-tile-item',
  templateUrl: './menu-tile-item.component.html',
  styleUrls: ['./menu-tile-item.component.scss']
})
export class MenuTileItemComponent implements OnInit {

  @Input() menuItem: MenuItem;

  constructor() {
  }

  ngOnInit() {
  }

}
