import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Style } from '../../../wa-common/style.enum';
import { CommonMessages } from '../../i18n/common-messages';
import { CommonIcons } from '../../icons/common-icons';

@Component({
  selector: 'wa-modal-header',
  templateUrl: './modal-header.component.html',
  styleUrls: ['./modal-header.component.scss']
})
export class ModalHeaderComponent implements OnInit {

  @Input() title = '';
  @Input() activeModal: NgbActiveModal;
  style = Style;
  commonMessages = CommonMessages;
  commonIcons = CommonIcons;

  constructor() {
  }

  ngOnInit() {
  }
}
