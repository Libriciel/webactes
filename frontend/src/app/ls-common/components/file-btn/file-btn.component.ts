import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';

@Component({
  selector: 'wa-file-btn',
  templateUrl: './file-btn.component.html',
  styleUrls: ['./file-btn.component.scss']
})
export class FileBtnComponent implements OnInit, AfterViewInit {

  @Input() title;
  @Input() icon;
  @Input() types;
  @Input() multiple;
  @Input() btnStyle = 'btn-primary';
  @Input() name;
  @ViewChild('inputFile', {static: true}) fileInput: ElementRef;

  constructor(
    protected element: ElementRef,
    protected renderer2: Renderer2,
  ) {
  }

  ngOnInit() {
  }

  resetInput() {
    this.fileInput.nativeElement.value = '';
  }

  ngAfterViewInit(): void {
    const id = this.element.nativeElement.getAttribute('id');
    if (id) {
      this.renderer2.removeAttribute(this.element.nativeElement, 'id');
      this.renderer2.setAttribute(this.fileInput.nativeElement, 'id', id);
    }
  }

}
