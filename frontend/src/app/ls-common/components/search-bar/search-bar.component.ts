import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';
import { CommonIcons } from '../../icons/common-icons';

@Component({
  selector: 'wa-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit, AfterViewInit {

  waCommonMessages = WACommonMessages;
  commonIcons = CommonIcons;
  filterString: string;
  filterUpdate = new Subject<string>();
  @Output() applyFilter = new EventEmitter();
  @ViewChild('searchBarInput', {static: true}) searchBarInput: ElementRef;
  @ViewChild('searchBarLabel', {static: true}) searchBarLabel: ElementRef;

  constructor(
    protected element: ElementRef,
    protected renderer2: Renderer2,
  ) {
  }

  ngOnInit() {
    this.filterUpdate.pipe(
      debounceTime(800),
      distinctUntilChanged())
      .subscribe(() => this.applyFilter.emit(this.filterString));
  }

  resetFilters() {
    if (this.filterString && this.filterString !== '') {
      this.filterString = '';
      this.applyFilter.emit(this.filterString);
    }
  }

  ngAfterViewInit(): void {
    const id = this.element.nativeElement.getAttribute('id');
    if (id) {
      this.renderer2.removeAttribute(this.element.nativeElement, 'id');
      this.renderer2.setAttribute(this.searchBarInput.nativeElement, 'id', id);
      this.renderer2.setAttribute(this.searchBarLabel.nativeElement, 'for', id);
    }
  }
}
