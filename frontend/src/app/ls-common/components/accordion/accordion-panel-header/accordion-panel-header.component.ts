import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonIcons } from '../../../icons';

@Component({
  selector: 'wa-accordion-panel-header',
  templateUrl: './accordion-panel-header.component.html',
  styleUrls: ['./accordion-panel-header.component.scss']
})
export class AccordionPanelHeaderComponent implements OnInit {

  readonly commonIcons = CommonIcons;

  @Input() opened = false;
  @Input() title = '';
  @Output() toggle = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

}
