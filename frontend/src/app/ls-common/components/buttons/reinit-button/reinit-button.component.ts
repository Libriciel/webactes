import { Component, OnInit } from '@angular/core';
import { CommonMessages } from '../../../i18n';
import { CommonIcons } from '../../../icons';
import { ButtonComponent } from '../button/button.component';
import { Weight } from '../../../../wa-common/weight.enum';

@Component({
  selector: 'wa-reinit-button',
  templateUrl: '../button/button.component.html',
  styleUrls: ['./reinit-button.component.scss']
})
export class ReinitButtonComponent extends ButtonComponent implements OnInit {

  ngOnInit() {
    this.title = this.title ? this.title : CommonMessages.REINIT;
    this.icon = this.icon ? this.icon : CommonIcons.RESET_ICON;
    this.weight = Weight.SECONDARY;
    super.ngOnInit();
  }

}
