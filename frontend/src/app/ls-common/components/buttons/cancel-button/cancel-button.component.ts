import { Component, OnInit } from '@angular/core';
import { Weight } from '../../../../wa-common/weight.enum';
import { ButtonComponent } from '../button/button.component';
import { CommonMessages } from '../../../i18n';

@Component({
  selector: 'wa-cancel-button',
  templateUrl: '../button/button.component.html',
  styleUrls: ['./cancel-button.component.scss']
})
export class CancelButtonComponent extends ButtonComponent implements OnInit {

  ngOnInit() {
    this.noIcon = true;
    this.title = this.title ? this.title : CommonMessages.CANCEL;
    this.weight = Weight.TERTIARY;
    super.ngOnInit();
  }
}
