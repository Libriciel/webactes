import { Component, OnInit } from '@angular/core';
import { ButtonComponent } from '../button/button.component';
import { CommonMessages } from '../../../i18n/common-messages';
import { CommonIcons } from '../../../icons/common-icons';

@Component({
  selector: 'wa-save-button',
  templateUrl: '../button/button.component.html',
  styleUrls: ['./save-button.component.scss']
})
export class SaveButtonComponent extends ButtonComponent implements OnInit {

  ngOnInit() {
    this.title = this.title ? this.title : CommonMessages.SAVE;
    this.icon = this.icon ? this.icon : CommonIcons.SAVE_ICON;
    super.ngOnInit();
  }
}
