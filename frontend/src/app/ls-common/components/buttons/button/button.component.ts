import { AfterViewInit, Component, ElementRef, EventEmitter, Host, Input, OnInit, Optional, Output, Renderer2, ViewChild } from '@angular/core';
import { Weight } from '../../../../wa-common/weight.enum';
import { Style } from '../../../../wa-common';
import { CommonStylesConstants } from '../../../../wa-common/common-styles-constants';
import { CommonIcons } from '../../../icons';
import { ActionableDirective } from '../../tables/actionable.directive';
import { HasId } from '../../../model';

@Component({
  selector: 'wa-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit, AfterViewInit {
  @Input() title: string;
  @Input() icon: string;
  @Input() noIcon = false;
  @Input() weight: Weight = Weight.PRIMARY;
  @Input() style: Style = Style.NORMAL;
  @Input() additionalClasses = '';
  @Input() disabled = false;
  @Input() processing = false;
  @Input() processingIcon;
  @Input() onlyIcon = false;
  @Input() colorOnHover = false;
  @Input() iconOnLeft = true;
  @Input() iconSize: 'normal' | 'small' | 'large' = 'normal';
  @ViewChild('buttonElement', {static: true}) buttonElement;
  @Output() btnClick = new EventEmitter();

  constructor(
    protected el: ElementRef,
    protected renderer2: Renderer2,
    @Optional() @Host() public actionable: ActionableDirective<HasId>
  ) {
  }

  ngOnInit() {
    this.icon = !this.noIcon
      ? this.icon || this.actionable && this.actionable.action.icon || undefined
      : undefined;
    this.title = this.title || this.actionable && this.actionable.action.name || undefined;
    if (this.icon) {
      this.processingIcon = this.processingIcon || CommonIcons.SPINNER_ICON;
    }
  }

  ngAfterViewInit(): void {
    this.moveAttributeToInput('id');
    this.moveAttributeToInput('tabindex');
    this.moveAttributeToInput('name');
    this.moveAttributeToInput('autofocus');
  }

  getIconClasses(): string {
    const classes = this.weight === Weight.MENU_ITEM ? `${CommonStylesConstants.getStyleClass(this.style)} ` : '';

    switch (this.iconSize) {
      case 'small':
        return ` ${classes}${CommonStylesConstants.getButtonSmallSizeClass()}`;
      case 'large':
        return ` ${classes}${CommonStylesConstants.getButtonLargeSizeClass()}`;
      default:
        return '';
    }
  }

  getBtnClasses(): string {
    const btnClasses = this.additionalClasses ? ` ${this.additionalClasses}` : '';

    if (this.onlyIcon) {
      return `no-text${btnClasses} `
        + (this.colorOnHover
          ? CommonStylesConstants.getOnHoverStyleClass(this.style)
          : CommonStylesConstants.getStyleClass(this.style));
    }
    switch (this.weight) {
      case Weight.PRIMARY:
        return `${CommonStylesConstants.getButtonStyleClass(this.style)}${btnClasses}`;
      case Weight.SECONDARY:
        return `${CommonStylesConstants.getButtonOutlineStyleClass(this.style)}${btnClasses}`;
      case Weight.TERTIARY:
        return `${CommonStylesConstants.getButtonLinkStyleClass()}${btnClasses}`;
      case Weight.MENU_ITEM:
        return `${CommonStylesConstants.getButtonMenuItemStyleClass()}${btnClasses}`;
    }
  }

  private moveAttributeToInput(attributeName: string, defaultValue?: string) {
    const attribute = this.el.nativeElement.getAttribute(attributeName);
    if (attribute !== null) {
      this.renderer2.removeAttribute(this.el.nativeElement, attributeName);
    }
    if (attribute !== null || defaultValue) {
      this.renderer2.setAttribute(this.buttonElement.nativeElement, attributeName, attribute ? attribute : defaultValue);
    }
  }
}
