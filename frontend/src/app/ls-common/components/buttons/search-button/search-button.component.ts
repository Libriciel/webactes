import { Component, OnInit } from '@angular/core';
import { CommonMessages } from '../../../i18n';
import { CommonIcons } from '../../../icons';
import { ButtonComponent } from '../button/button.component';

@Component({
  selector: 'wa-search-button',
  templateUrl: '../button/button.component.html',
  styleUrls: ['./search-button.component.scss']
})

export class SearchButtonComponent extends ButtonComponent implements OnInit {

  ngOnInit() {
    this.title = this.title ? this.title : CommonMessages.SEARCH;
    this.icon = this.icon ? this.icon : CommonIcons.SEARCH_ICON;
    super.ngOnInit();
  }

}
