import { Component, OnInit } from '@angular/core';
import { Weight } from '../../../../wa-common/weight.enum';
import { ButtonComponent } from '../button/button.component';
import { CommonMessages } from '../../../i18n/common-messages';
import { CommonIcons } from '../../../icons/common-icons';

@Component({
  selector: 'wa-go-back-button',
  templateUrl: '../button/button.component.html',
  styleUrls: ['./go-back-button.component.scss']
})
export class GoBackButtonComponent extends ButtonComponent implements OnInit {

  ngOnInit() {
    this.title = this.title ? this.title : CommonMessages.GO_BACK;
    this.iconOnLeft = true;
    this.icon = CommonIcons.GO_BACK_ICON;
    this.weight = Weight.SECONDARY;
    super.ngOnInit();
  }
}
