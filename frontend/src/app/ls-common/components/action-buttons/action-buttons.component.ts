import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Weight } from '../../../wa-common/weight.enum';
import { ActionSet } from '../../model/action-set';

@Component({
  selector: 'wa-action-buttons',
  templateUrl: './action-buttons.component.html',
  styleUrls: ['./action-buttons.component.scss']
})
export class ActionButtonsComponent<T> implements OnInit {

  @Input() secondaryActions: ActionSet<T> = {actions: []};
  @Input() primaryActions: ActionSet<T> = {actions: []};
  @Input() otherActions: ActionSet<T> = {actions: []};
  @Input() argument: T;
  @Output() needReload = new EventEmitter();
  @Output() actionResult = new EventEmitter();

  weight = Weight;

  constructor() {
  }

  ngOnInit(): void {
    if (this.secondaryActions && this.secondaryActions.actions) {
      this.secondaryActions.actions
        .forEach(item => item.initializeItem());
    }
    if (this.primaryActions && this.primaryActions.actions) {
      this.primaryActions.actions
        .forEach(item => item.initializeItem());
    }
    if (this.otherActions && this.otherActions.actions) {
      this.otherActions.actions
        .forEach(item => item.initializeItem());
    }
  }

  sendResult($event: any) {
    this.actionResult.emit($event);
  }
}
