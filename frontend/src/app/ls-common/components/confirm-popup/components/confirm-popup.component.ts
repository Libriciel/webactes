import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Style } from '../../../../wa-common/style.enum';
import { Weight } from '../../../../wa-common/weight.enum';
import { CommonMessages } from '../../../i18n/common-messages';

@Component({
  selector: 'wa-confirm-popup',
  templateUrl: './confirm-popup.component.html',
  styleUrls: ['./confirm-popup.component.scss']
})
export class ConfirmPopupComponent implements OnInit {

  weight = Weight;
  @Input() title: string;
  @Input() content: string;
  @Input() confirmMsg = CommonMessages.CONFIRM;
  @Input() cancelMsg;
  @Input() style: Style = Style.NORMAL;
  @Input() icon: string;
  @Input() cancelButton = true;
  @Input() type: 'normal' | 'info' = 'normal';

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  onConfirm() {
    this.activeModal.close();
  }

  onCancel() {
    this.activeModal.dismiss({isError: false});
  }
}
