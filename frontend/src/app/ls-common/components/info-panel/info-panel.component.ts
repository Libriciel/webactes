import { Component, Input, OnInit } from '@angular/core';
import { Style } from '../../../wa-common/style.enum';
import { CommonStylesConstants } from '../../../wa-common/common-styles-constants';
import { CommonIcons } from '../../icons/common-icons';

@Component({
  selector: 'wa-panel',
  templateUrl: './info-panel.component.html',
  styleUrls: ['./info-panel.component.scss']
})
export class InfoPanelComponent implements OnInit {

  @Input() text: string;
  @Input() htmlText: string;
  @Input() icon: string;
  @Input() style: Style = Style.INFO;
  cssClass = '';

  constructor() {
  }

  ngOnInit() {
    switch (this.style) {
      case Style.DANGER:
        this.icon = CommonIcons.INVALID_FORM_ICON;
        break;
      case Style.WARNING:
        this.icon = CommonIcons.WARNING_ICON;
        break;
      case Style.INFO:
        this.icon = CommonIcons.INFO_ICON;
    }
    this.cssClass += ` ${CommonStylesConstants.getPanelStyleClass(this.style)}`;
  }
}
