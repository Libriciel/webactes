import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Options } from '../../model/options';
import { Option } from '../../model/option';
import { CommonMessages } from '../../i18n';
import { IManageNotificationsOptionsService } from '../../../admin/notifications-options/services/imanage-notifications-options-service';
import { NotificationService } from '../../services';
import { NotificationsOptionsMessages } from '../../../admin/notifications-options/i18n/notifications-options-messages';

@Component({
  selector: 'wa-options-list',
  templateUrl: './options-list.component.html',
  styleUrls: ['./options-list.component.scss']
})
export class OptionsListComponent implements OnInit {

  @HostBinding() class = 'ls-table';

  @Input()
  set optionsList(value: Options) {
    this._optionsList = value;
    this.updateAllChecked();
  }

  get optionsList(): Options {
    return this._optionsList;
  }

  private _optionsList: Options;
  allChecked = false;
  commonMessages = CommonMessages;

  constructor(protected notificationsOptionsService: IManageNotificationsOptionsService,
              protected notificationService: NotificationService) {
  }

  ngOnInit() {
  }

  toggleAll(value: boolean) {
    this._optionsList.values.forEach(option => this.setValue(option, value));
  }

  setValue(option: Option, value: boolean) {
    option.active = value;
    this.notificationsOptionsService.saveNotificationOption(option)
      .subscribe(
        () => {
        },
        () => {
          this.notificationService.showError(NotificationsOptionsMessages.NOTIFICATIONS_OPTIONS_SAVE_ERROR);
          option.active = !value;
        })
      .add(() => this.updateAllChecked());
  }

  valueChanged(option: Option, value: boolean) {
    this.setValue(option, value);
  }

  private updateAllChecked(): void {
    this.allChecked = this._optionsList.values.length > 0 ? this._optionsList.values.every(option => option.active) : false;
  }
}
