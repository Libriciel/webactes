import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, forwardRef, Input, Renderer2, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { WACommonMessages } from '../../../wa-common/i18n/wa-common-messages';
import { Choice, RadioButtonsComponent } from '@libriciel/ls-composants';
import { HasId } from '../../model';

@Component({
  selector: 'wa-list-or-button',
  templateUrl: './list-or-button.component.html',
  styleUrls: ['./list-or-button.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ListOrButtonComponent),
      multi: true
    }
  ]
})
export class ListOrButtonComponent implements ControlValueAccessor, AfterViewInit {

  readonly waCommonMessages = WACommonMessages;

  @Input() placeholder: string;
  @Input() icons: string[];
  @Input() maxLength = 5;
  @Input() required: boolean;
  @ViewChild(RadioButtonsComponent) radioButtons;
  id: string;

  private _value: any | HasId;
  get value() {
    return this._value;
  }

  @Input()
  set value(value: any | HasId) {
    this._value = value;
    if (value === undefined) {
      this.onTouched();
    }
  }

  private _items: Choice[];
  get items(): any[] {
    return this._items;
  }

  @Input()
  set items(items: Choice[]) {
    this._items = items;
    if (this.value && this.items) {
      const temp = this.items.find(item => {
        return item.value && this.value && item.value.id === this.value.id
          ? item.value.id === this.value.id
          : item.value.value === this.value.value;
      });
      this.value = temp.value;
    }
  }

  constructor(private element: ElementRef, private renderer2: Renderer2, protected changeDetectorRef: ChangeDetectorRef) {
  }

  ngAfterViewInit(): void {
    const id = this.element.nativeElement.getAttribute('id');
    this.renderer2.removeAttribute(this.element.nativeElement, 'id');
    if (id) {
      this.renderer2.setAttribute(this.radioButtons.element.nativeElement, 'id', id);
    }
    this.id = id;
    this.changeDetectorRef.detectChanges();

  }

  onChange: any = () => {
  };

  onTouched: any = () => {
  };

  onChangeSelection($event) {
    if ($event !== undefined) {
      // list or button
      this.value = $event.value ? $event.value : $event;
      this.onChange(this.value);
    }
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }
}
