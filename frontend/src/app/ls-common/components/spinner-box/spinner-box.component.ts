import { Component, OnInit } from '@angular/core';
import { CommonIcons } from '../../icons';

@Component({
  selector: 'wa-spinner-box',
  templateUrl: './spinner-box.component.html',
  styleUrls: ['./spinner-box.component.scss']
})
export class SpinnerBoxComponent implements OnInit {

  commonIcons = CommonIcons;

  constructor() {
  }

  ngOnInit() {
  }

}
