import { Component, Input, OnInit } from '@angular/core';
import { CommonIcons } from '../../icons';

@Component({
  selector: 'wa-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements OnInit {

  @Input() message = '';
  commonIcons = CommonIcons;
  @Input() htmlContent = false;

  constructor() {
  }

  ngOnInit() {
  }
}
