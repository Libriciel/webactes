# Changelog

*Les changements notables du projet seront documentés dans ce fichier.*

Le format est basé sur [Tenez un Changelog](https://keepachangelog.com/fr/1.0.0/) et ce projet suit la [Gestion sémantique de version](https://semver.org/lang/fr/spec/v2.0.0.html).

## [2.0.17] -

### Corrections

- Le terme "ordre du jour" a été remplacé partout par le terme "note de synthèse" #1422

## [2.0.16] - 2024-04-24

### Corrections

- Fixer la version de libreofficedocker/libreoffice-unoserver suite au problème d'installation en 2.0.15 #1419
- Impossible de sélectionner un mandataire via chrome #1420
- Correction du problème d'encodage des classifications sur les plateformes s2low de formations #1423

## [2.0.15] - 2024-04-17

### Corrections

- Les éléments "Retraits" n'apparaissaient pas dans la partie "Versions" de la page "À propos - Aide"

### Evolutions

- Ajout de la variable secrétaire de séance dans le modèle d'acte #1392
- Création d'un champ date de convocation, depuis le menu création de séance #1010
- Les noms de fichiers à télécharger sont rendus uniques et plus explicites en fonction de leur type, de leur identifiant, ou de leur date (en fonction du contexte) #1397
- Modification des termes "PV sommaire" en "Liste des délibérations" et "PV détaillé" en "Procès-verbal" &19
- Simplification du démarrage de la pile docker pour le développement (.env.dist plus simple et valeur par défaut reporté dans le fichier docker-compose.yml) #1401

### Suppressions

- Suppression du gabarit de synthese #1404

## [2.0.14] - 2024-03-13

### Suppressions

- Suppression de la possibilité de modifier l'information "Envoi de documents papiers complémentaires"[#914](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/914)

### Corrections

- Amélioration sur les filtres [#1164](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1164)
- Affichage icône "prend acte" dans l'historique du projet [#1221](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1221)
- Affichage des séquences limité à 10 en création et modification de compteur [#1260](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1260)
- Classement des compteurs par noms [#1259](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1259)
- Impossible de modifier un projet sur une structure où l'édition collaborative n'est pas activée[#1261](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1261)
- Gestion de l'unicité du code acte [#1046](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1046)
- Homogénéiser la taille de la colonne "libellé" dans les différents écrans [#1339](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1339)
- Modification de la séquence des compteurs [#1204](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1204)
- Le bouton "annuler" une séance ne fonctionne pas [#1207](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1207)
- Le numéro d'acte ne peut pas commencer par des numéros précédés d'underscore [#1203](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1203)
- Le typage d'une annexe n'est pas pris en compte en modification de projet si retiré [#1209](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1209)
- Problème d'affichage de champ heure dans la création / modification d'une séance[#1216](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1216)
- Problème d'affichage du placard à balais [#1257](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1257)
- Recherche multi-critères : amélioration affichage types d'actes [#1202](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1202)
- Récupération des rôles à la création d'utilisateurs [#1231](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1231)
- Remplacer le document principal avant signature [#647](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/647)
- Structure sans génération : le document d'acte n'est plus visible [#1341](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1341)
- Typologie manquante dans pastell pour dépôt TDT lors de la modification avant envoi [#1363](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1363)

### Evolutions

- Amélioration des services de génération [#1224](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1224)
- Evolutions techniques mineures [#1164](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1164) [#1192](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1192)
- Migration vers Angular 14 [#1079](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1079)
- Prise en compte d'un collabora externe dans la configuration nginx du client [#1384](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1384)
- Rendre obligatoire les modèles à la création/modification d'un type d'acte [#1258](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1258)
- Technique, partie serveur, passage en PHP 8.1 [#1078](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1078)
- Technique, partie serveur, passage du docker en Ubuntu 22.04 [#1077](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1077)
- Technique, partie serveur, passage en CakePHP 4.4 [#1080](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1080)
- Utilisation d'un service externe de conversion d'annexes PDF en ODT, plusieurs conversions sont à présent faites simultanément (par défaut: 4)[#1197](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1197)

## [2.0.13] - 2023-10-10

### Corrections

- Affichage des acteurs mandants et mandataires dans le document d'acte [#1219](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1219)

## [2.0.12] - 2021-07-21

### Ajouts

- Possibilité de changer le spinner de génération pour chaque structure [#1155](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1155)

### Evolutions

- Amélioration de la gestion des erreurs de génération, vérifications et limitations à 1000 pages par projet pour les annexes jointes à la fusion [#1183](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1183)
- Restriction sur clôture séance [#953](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/953)
- Meilleure gestion de la génération des projets contenant des annexes [#1150](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1150)
- Empêcher l'envoi d'un mail sécurisé si la convocation et l'odj n'ont pas été générés [#1165](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1165)
- Changer le libellé "Statut de reception" en "Statut de lecture" sur l'écran de suivi d'envoi des convocations[#1160](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1160)
- Modification de la page d'envoi des documents de séance via mail sécurisé [#990](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/990)
- Prise en compte de la modification d'un acteur au vote [#1096](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1096)
- Notifications utilisateurs non fonctionnelles [#1028](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1028)
- Précision d'aide sur les formats d'annexe autorisés en modification de projet [#1112](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1112)
- Amélioration des performances concernant "Séances en cours > Envoyer la séance" et "Séances en cours > Ordre du jour de la séance" [#1120](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1120)
- Mise en place de la dernière version de collabora online 22.05.14.3.1 [#1106](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1106)
- Amélioration des performances concernant la récupération des séances [#1066](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1066)
- Si les éléments de vote n'ont pas été renseignés, l'enregistrement du vote est impossible [#1094](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1094)

### Corrections

- L'ordre des annexes est désormais correctement pris en compte [#1191](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1191)
- Un valideur ne peut valider qu'une seule fois un projet, après une demande de confirmation [#870](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/870)
- Une séance ne peut être clôturée que si tous les projets qu'elle contient ont été votés [#953](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/953)
- Un secrétaire ne peut pas modifier la liste des élus présents [#1148](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1148)
- Ordre des séances incohérent depuis les séances en cours [#1131](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1131)
- PDF corrompu lorsque l'on utilise des variables president_seance_..., secretaire_seance_... et president_[#1108](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1108)
- Problème sur le vote par groupe d'acteur au détail des voix[#1100](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1100)
- Le super admin n'a pas accès à tous les menus [#1089](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1089)
- Typologie par défaut [#1069](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1069)
- Ordre du jour en double lors de la génération de la séance [#1054](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1054)
- Différence de comportement de l'ordre des projets selon l'étape à laquelle on rattache le projet à la séance [#994](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/994)
- Problème de décompte élus présents [#973](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/973)
- Impossible de voter un projet s'il n'a pas été rattaché à une séance dès sa création [#1107](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1107)
- Restriction mandat pour acteur absent [#1158](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1158)
- Ne pas pouvoir sélectionner un président absent [#1159](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1159)
- Le filtre d'un utilisateur ne fonctionne pas avec le champ Login [#1145](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1145)
- Modification possible du mode de vote [#1035](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1035) et [#1153](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1153)
- Bouton de création ou de modification d'un type d'acte grisé si pas de compteur à renseigner [#1098](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1098)
- Notifications utilisateurs non fonctionnelles [#1028](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1028)
- La variable `annexes_nombre` ne renvoie pas la bonne valeur [#1146](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1146)
- Liste des acteurs erronée pour la sélection du président et secrétaire de séance" [#1141](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1141)
- Correction effectuée sur le nombre d'annexes et l'incorporation des annexes en génération [#1087](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1087)
- Libellé du projet tronqué dans les modales de signature [#1043](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1043)
- Récupération de la liste de présence précédente [#1092](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1092)
- Comportement identique au niveau des rôles lors de la création d'une structure, que ce soit via import CSV ou via Pastell : rôle "Super Administrateur" manquant via Pastell, possibilité d'utiliser le rôle "Secrétariat général" (`secretariat_general`) lors de l'import CSV [#1101](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1101)

## [2.0.11] - 21/04/2023

### Evolutions

- Optimisation de l'affichage des séances [#1066](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1066)
- Ajout des variables disponibles pour la fusion concernant les présidents de séance, secrétaire de séance, président·e de vote [#1097](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1097)

### Corrections

- Les variables `acte_signature` et `acte_signature_date` sont à présent bien envoyées à la fusion des PV suite à une signature du Parapheur [#1075](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1075)
- La structure d'un utilisateur est changée lorsque son rôle est modifié par un super administrateur [#1049](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1049)
- Typologie des actes par défaut [#1069](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1069)
- Mauvais calcul du résultat du vote en cas de vote au total des voix [#1055](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1055)
- Génération de projet possible même si non configuré dans la structure [#996](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/996)
- Rattachement du super administrateur à la structure renseignée lors de sa création [#1036](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1036)
- Un rédacteur/valideur a accès aux menus séances [#980](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/980)
- La génération du numéro d'acte avec la position dans la définition du compteur ne renvoi pas le numéro de position [#1073](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1073)
- Récupération de la liste des projets d'une séance [#1071](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1071)
- Correction du décalage de la date de consultation d'un mail sécurisé et ajout de la possibilité de spécifier l'adresse mail qui figurera dans le champ `to` des mails sécurisés Pastell, ajout des variables d'environnement `PASTELL_TIMEZONE` et `PASTELL_MAILSEC_TO` [#910](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/910)
- Les tâches automatisées ne se lancent plus ou n'aboutissent plus en 2.0.10 [#1074](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1074)

### Suppressions

- Liste de présence d'une délibération précédente [#1013](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1013)

## [2.0.10] - 11/04/2023

### Corrections

- La modification d'une séance duplique l'ordre du jour [#998](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/998)
- Le vote est proposé dans un type de séance non délibérante [#992](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/992)
- Mauvaise date lors de la signature manuelle [#1056](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1056)
- Erreur sur les variables acteurs mandatés / mandataires [#1058](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1058)
- La prise d'acte doit numéroter le projet [#1030](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1030)
- Suppression du document principal dès la modification du projet [#1021](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1021)
- La date de l'acte correspond à la date de dépôt sur le TdT au lieu de correspondre à la date de signature de l'acte ou à la date de séance [#1002](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1002)
- Restriction de doublons sur les types de séance et types d'acte [#924](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/924)
- Message d'erreur explicite en cas de mail déjà utilisé par un autre acteur [#960](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/960)
- Bouton enregistrer grisé à la modification de séquence [#918](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/918)
- Le thème parent ne s'affichait pas lorsqu'on éditait un thème enfant [#841](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/841)
- Mauvais nom d'un menu dans la fenêtre de confirmation de suppression d'une séance [#1029](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1029)
- Le document principal affiche maintenant un message si une génération est en cours [#971](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/971)
- La modification de l'ordre du jour n'est pas prise en compte en génération et envoi vers Idelibre [#1019](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1019)
- Restriction doublon sur types d'actes et types de séances [#924](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/924)
- Notifications reçues sans être activées [#969](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/969)
- Autoriser les caractères spéciaux dans le nom d'étape de circuit [#1006](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1006)
- La définition du compteur doit respecter le protocole @cte [#995](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/995)
- Un acteur désactivé est destinataire de la convocation et de l'ordre du jour [#987](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/987)
- Un rédacteur valideur ne peut pas renseigner la séance en création de projet [#964](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/964)
- Le mail d'un utilisateur doit avoir un format valide [#948](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/948)
- Affichage du nombre de séances limité en création de projet [#1003](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/1003)
- Ne pas faire apparaître les 'documents TDT' lorsque le type d'acte n'est pas transmissible [#991](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/991)
- Problème d'historique et absence de message de confirmation lors de l'envoi en signature [#866](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/866)
- Génération projet : document Txt au lieu de pdf [#976](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/976)
- Problème de décompte élus présents [#973](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/973)
- Cloisonner la modification de la configuration aux administrateurs et super administrateurs [#950](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/950)
- Groupe d'acteurs obligatoire pour les types de séances [#925](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/925)
- Des paramètres sont sauvegardés (en affichage) même en cliquant sur annuler [#999](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/999)
- La définition du compteur doit respecter le protocole @cte [#995](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/995)
- Accès au menu séances pour le secrétariat général [#882](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/882)
- Champs téléphone non obligatoire pour les acteurs [#962](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/962)
- Empêcher la création d'utilisateurs avec le même login [#944](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/944)
- Un acteur désactivé ne peut plus être sélectionné comme président/secrétaire de séance [#951](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/951)
- Basculement de compte super administrateur vers administrateur accès à une page interdite avec blocage [#938](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/938)
- Modification de la politique de confidentialité [#934](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/934)
- Recherche des utilisateurs par le nom et le prénom à la création d'un circuit [#946](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/946)
- Contrôle du format du numéro de téléphone à la création d'un acteur [#939](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/939)
- Suppression logo de la sidebar [#917](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/917)
- Ne plus pouvoir se connecter avec un utilisateur désactivé [#952](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/952)
- Permettre uniquement les pdf en fusion documentaire [#928](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/928)
- Acte non délibérant numéroté à l'état validé [#985](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/985)
- Recherche multicritère limitée à la structure de l'utilisateur [#993](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/993)
- Modification des projets validés par tous les utilisateurs concernés [#965](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/965)
- Création de connecteur Idelibre [#982](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/982)
- Impossible de visualiser un utilisateur rattaché à une structure importée depuis pastell en tant que super administrateur [#972](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/972)
- Respect de la casse à l'enregistrement du prénom d'un utilisateur [#936](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/936)
- Mauvais utilisateur tracé dans l'historique d'un projet [#958](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/958)
- Rendre possible la création de plusieurs utilisateurs ayant le même mail [#937](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/937)
- Affichage de la structure du super admin [#929](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/929)
- Intégration du nouveau logo Libriciel SCOP [#911](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/911)
- Relance automatique de la génération si une erreur s'est produite [#875](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/875)
- Synchronisation des annexes de webactes vers pastell [#913](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/913)
- Suppression et modification de convocation et ordre du jour [#902](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/902)
- Un acteur désactivé apparait tout de même dans la liste des votants[#868](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/868)
- Interdire une définition de compteur de plus de 15 caractères [#828](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/828)
- La date de signature n’apparaît pas lors des générations [#865](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/865)
- Ajouter les retours de mail sécurisé à la supervision [#910](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/910)
- Les mails de notification ne prennent pas la configuration smtp en compte [#846](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/846)
- Erreur d'envoi au TDT et certains actes ne peuvent pas être envoyés en préfecture [#906](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/906)
- Impossible de le renvoyer dans un circuit après modification [#871](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/871)
- Prise en compte des modifications d'un projet après un refus Iparapheur[#886](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/886)
- Le bouton suivant reste grisé malgré si aucune séance n'existe [#884](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/884)
- Même modèle que la convocation au niveau de l'ordre du jour [#901](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/901)
- Génération des documents de convocation [#800](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/800)
- Modification de gabarit  [#867](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/867)

## [2.0.9] - 28/10/2022

### Corrections

- Correction la gestion des actions sur les utilisateurs qui impactait la synchronisation via socle [#863](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/863)

## [2.0.8] - 01/07/2022

### Corrections

- Permettre d'avoir uniquement le document de convocation pour le mail sec [#840](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/840)

## [2.0.7] - 21/06/2022

### Corrections

- Message d'erreur en création de projet non pertinent

## [2.0.6] - 17/06/2022

### Evolutions

- Améliorations techniques

## [2.0.5] - 02/06/2022

### Evolutions

- Amélioration des requêtes Pastell

### Corrections

- Amélioration de la procédure d'installation

## [2.0.4] - 30/05/2022

### Ajouts

* Affichage message d'erreur si définition de compteur supérieure à 15
  caractères [#828](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/828)

### Evolutions

- Créer / modifier un utilisateur [#814](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/814)
- Vérification de la sécurité du conteneur [#834](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/834)

### Corrections

- Spinner de pagination [#773](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/773)
- Pagination séances [#820](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/820)
- Création doublon type de séances [#821](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/821)
- Toaster de confirmation et mise à jour projet [#621](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/621)

## [2.0.3] - 08/12/2021

### Corrections

- Impossible d'envoyer un acte sur le parapheur [#818](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/818)
- Impossible de créer un nouveau thème portant le même libellé qu'un thème supprimé [#819](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/819)

## [2.0.2] - 10/11/2021

### Corrections

- Récupération du type de connecteur parfois erroné [#817](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/817)

## [2.0.1] - 25/10/2021

### Corrections

- Socle MYEC3 Lors d'un update créer une structure si celle-ci n'existe pas [#816](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/816)

## [2.0.0]

### Ajouts

- Voter un projet [#609](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/609)
- Générer un document [#610](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/610)
- L'éditeur en ligne des textes de projet [#608](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/608)
- Créer/Modifier des gabarits de projet [#607](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/607)
- Créer/Modifier un type de séance [#107](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/144)
- Effectuer une recherche multi-critères [#53](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/53)
- Ajout des gabarits de projets dans la rédaction des projets [#679](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/679)
- Placement des documents à générer [#651](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/651)
- Génération du numéro d'acte [#666](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/666)
- Effectuer une recherche multi-critères [#53](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/53)
- Ajout des gabarits de projets dans la rédaction des projets [#679](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/679)
- Ajout des gabarits de projets dans les types d'actes [#678](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/678)
- Intégration de l’éditeur dans l’application [#668](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/668)
- Créer/Modifier un gabarit de projet [#677](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/677)
- Créer/Modifier un modèle de document [#649](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/649)
- Migration cakePHP 4.2 [#664](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/664)
- Mise en place du moteur de génération [#652](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/652)
- Créer/Modifier un groupe d'acteur [#201](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/201)
- Créer/Modifier un thème [#605](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/605)
- Créer/Modifier une séquence [#624](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/624)
- Créer/Modifier un compteur [#623](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/623)
- Recherche multi-critères [#53](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/53)
- Récupérer les structures créées dans Pastell [#682](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/682)

### Corrections

- Impossible d'accéder aux pages de l'application [#613](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/613)
- Impossible d'effacer une classification [#469](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/469)
- Erreur d'orthographe dans variable d'environnement HTTP_PROXY_IP_ADDRESS [#604](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/604)
- Rendre les actes non transmissibles, transmissible [#600](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/600)
- Problème upload d'une image non png [#518](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/518)
- Ne pas limiter le choix des images aux formats pgn lors du choix du logo de la collectivité [#614](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/614)
- Menu "à ordonner" : changer l'ordre des colonnes [#440](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/440)
- Envoi sur pastell acte signé électroniquement [#622](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/622)

## [1.1.5] - 06/01/2021

### Corrections

- L'échec de la mise à jour de la classification initiale [#661](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/661)

## [1.1.4] - 01/12/2020

### Corrections

- Impossible de récupérer le nouvel état PASTELL d'un document s'il y a eu une erreur au préalable [#632](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/632)

## [1.1.3] - 16/11/2020

### Corrections

- Problème d'envoi des typologies vers pastell[#628](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/628)

## [1.1.2] - 12/11/2020

### Corrections

- Impossible d'accéder aux pages de l'application (patch-1) [#613](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/613)

## [1.1.1] - 01/10/2020

### Ajouts

- Ne pas limiter le choix des images aux formats pgn lors du choix du logo de la collectivité[#614](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/614)

### Corrections

- Impossible d'accéder aux pages de l'application [#613](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/613)
- Impossible d'effacer une classification [#469](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/469)
- Erreur d'orthographe dans variable d'environnement HTTP_PROXY_IP_ADDRESS [#604](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/604)
- Rendre les actes non transmissibles, transmissible [#600](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/600)
- Problème upload d'une image non png [#518](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/518)
- Ne pas limiter le choix des images aux formats pgn lors du choix du logo de la collectivité [#614](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/614)
- Menu "à ordonner" : changer l'ordre des colonnes [#440](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/440)
- Envoi sur pastell acte signé électroniquement [#622](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/622)

## [1.1.0] - 24/07/2020

### Ajouts

- Permettre l'utilisation d'un proxy [#576](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/576)
- Mise en place des notifications. [#562 #563 #550](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/562)
- Ajout de la page "à propos" [#457](https://gitlab.libriciel.fr/webACTES/webACTES/issues/457)
- Gestion des abonnements aux notifications [#562](https://gitlab.libriciel.fr/webACTES/webACTES/issues/562), [#563](https://gitlab.libriciel.fr/webACTES/webACTES/issues/563), [#550](https://gitlab.libriciel.fr/webACTES/webACTES/issues/550)

### Corrections

- Pour les écrans à faible hauteur, liste des types d'annexe invisible [#577](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/577)
- Problème d'envoi en signature si les typologies ne sont pas correctes [#603](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/603)
- Mise à jour de la classification [#588](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/588)
- Typologie perdue lorsqu'il y a des annexes transmissibles [#578](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/578)
- Choix validateur paginé [#492](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/492)
- Problème sur la cohérence nature, typologie et type d'acte [#582](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/582)
- Mauvaise typologie "Actes Individuels" [#581](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/581)
- Impossible de récupérer les sous types IP pour le profil secrétariat général [#595](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/595)

## [1.0.2] - 11/06/2020

### Corrections

- Correction sur la typologie "Actes Individuels".[#581](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/581)
- Correction sur la cohérence nature, typologie et type d'acte. [#582](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/582)

## [1.0.1] - 04/06/2020

### Corrections

- Correction d'un bug sur l'envoi des typologies sur PASTELL v3 pour les annexes. [#578](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/578)
- Correction d'un sur la possibilité de supprimer un projet/acte dans l'état "à déposer sur le tdt et acquitté" [#579](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/579)
- Correction pour la récupération de la classification. [#575](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/issues/575)

### Ajout

- Ajout du contexte des logs affichés.

## [1.0.0-1.0.22] - 24/02/2020

### Corrections

- Correction bug historique pour circuits supprimés [#567](https://gitlab.libriciel.fr/webACTES/webACTES/issues/567)

## [1.0.0-1.0.21] - 20/02/2020

### Ajouts

- Logs systématiques des interactions Pastell [#549](https://gitlab.libriciel.fr/webACTES/webACTES/issues/549)

## [1.0.0-1.0.20] - 11/02/2020

### Corrections

- Ajout de suivi des redirections
- Correction renvoi au I-Parapheur

## [1.0.0-1.0.19] - 11/02/2020

### Corrections

- Correction des bugs de relation WA<->PA

## [1.0.0-1.0.18] - 31/01/2020

### Ajouts

- Ajout de l'historique du parapheur [#131](https://gitlab.libriciel.fr/webACTES/webACTES/issues/131)
- Numéro d'acte - transformés en _

### Modifications

- Composant wa-button
- Présélection des circuits

### Corrections

- Entrée d'api pour connaître le menu d'un projet [#478](https://gitlab.libriciel.fr/webACTES/webACTES/issues/478)
- Corrections mineures sur certains boutons [#541](https://gitlab.libriciel.fr/webACTES/webACTES/issues/541)
- Action d'envoyer en circuit disponible en brouillon
- Correction des bugs de relation WA<->PA

## [1.0.0-1.0.17] - 21/01/2020

### Corrections

- Mauvaise date de décision envoyée au TDT [#493](https://gitlab.libriciel.fr/webACTES/webACTES/issues/493)
- Message erreur si document pas présent sur Pastell
- L'état envoi parapheur dès réception de la requête

## [1.0.0-1.0.16] - 21/01/2020

### Ajouts

* Mauvaise date de décision envoyée au TDT [#493](https://gitlab.libriciel.fr/webACTES/webACTES/issues/493)
* Readme correct [#538](https://gitlab.libriciel.fr/webACTES/webACTES/issues/538)

### Corrections

- Bugs sur la signature via I-Parapheur
- Menu du placard à balais par-dessus les pager [#394](https://gitlab.libriciel.fr/webACTES/webACTES/issues/394)

## [1.0.0-1.0.15] - 15/01/2020

### Modifications

- Logs supplémentaires pour la signature parapheur
- Ajout de priorités sur les actions beanstalk

## [1.0.0-1.0.14] - 14/01/2020

### Ajouts

- Logs supplémentaires pour la signature parapheur

## [1.0.0-1.0.13] - 20/12/2019

### Ajouts

- Signature via I-Parapheur
- Historique des actes [#131](https://gitlab.libriciel.fr/webACTES/webACTES/issues/131)

### Modifications

- Restructuration des menus [#482](https://gitlab.libriciel.fr/webACTES/webACTES/issues/482)
- Le type pdf n'est obligatoire qu'à la signature de l'acte [#505](https://gitlab.libriciel.fr/webACTES/webACTES/issues/505)
- Passage à PHP 7.3
- Message explicite en cas d'erreur de connexion à Pastell [#512](https://gitlab.libriciel.fr/webACTES/webACTES/issues/512)
- Entrée API pour connaître l'état d'un projet [#478](https://gitlab.libriciel.fr/webACTES/webACTES/issues/478)
- Restructuration des menus [#482](https://gitlab.libriciel.fr/webACTES/webACTES/issues/482)
- Amélioration de la saisie des heures et dates des séances [#350](https://gitlab.libriciel.fr/webACTES/webACTES/issues/350)
- Migration vers fontawesome 5

### Corrections

- Messages des champs de sélection vides en français [#353](https://gitlab.libriciel.fr/webACTES/webACTES/issues/353)
- Date de l'acte poussée sur slow correcte
- Permettre la sélection d'un utilisateur s'il y en a plus de 20 dans une collectivité [#492](https://gitlab.libriciel.fr/webACTES/webACTES/issues/492)
- Circuit non modifiable lorsque au moins un projet y a été instancié [#495](https://gitlab.libriciel.fr/webACTES/webACTES/issues/495)

## [1.0.0-1.0.12] - 14/11/2019

### Evolutions

- Actions unitaires en opacité réduite

## [1.0.0-1.0.11] - 13/11/2019

### Corrections

- Disparition d'historique pour les projets validés [#491](https://gitlab.libriciel.fr/webACTES/webACTES/issues/491)

## [1.0.0-1.0.10] - 13/11/2019

### Evolutions

- Meilleure gestion d'un projet sorti d'un circuit [#477](https://gitlab.libriciel.fr/webACTES/webACTES/issues/477)

### Modifications

- Montées de version des librairies Angular

### Corrections

- Mauvais droits pour l'édition de projets ou d'actes [#472](https://gitlab.libriciel.fr/webACTES/webACTES/issues/472)
- Validation en urgence valide un autre projet [#488](https://gitlab.libriciel.fr/webACTES/webACTES/issues/488)

## [1.0.0-1.0.9] - 04/11/2019

### Ajouts

- Changelog correct

### Evolutions

- changement de label CONNEXION_PASTELL_SUCCESS_MESSAGE
- changement de label CONNEXION_PASTELL_HELP
- Hover vert sur les titres clickable + modifications css mineures
- Ajout du nom du validateur pour la validation en urgence [#461](https://gitlab.libriciel.fr/webACTES/webACTES/issues/461)

### Corrections

- Breadcrumb de visualisation pour les actes [#408](https://gitlab.libriciel.fr/webACTES/webACTES/issues/408)
- Date de l'acte erronée et fixée au 11 février 2019 [#474](https://gitlab.libriciel.fr/webACTES/webACTES/issues/474)
- Les rédacteurs ne peuvent pas sélectionner de type ni de séances [#475](https://gitlab.libriciel.fr/webACTES/webACTES/issues/475)

## [1.0.0-1.0.8] - 30/10/2019

### Corrections

- Accès correct à la BDD flowable en test

## [1.0.0-1.0.7] - 29/10/2019

### Ajouts

- Visualisation au clic du libellé [#460](https://gitlab.libriciel.fr/webACTES/webACTES/issues/460)
- Admin fonctionnel peut valider en urgence
- Synchronisation du socle Worldline

### Evolutions

- Renommer le bouton déclarer [#462](https://gitlab.libriciel.fr/webACTES/webACTES/issues/462)
- Refonte des couleurs de l'application [#275](https://gitlab.libriciel.fr/webACTES/webACTES/issues/275)
- Amélioration visibilité commentaire [#461](https://gitlab.libriciel.fr/webACTES/webACTES/issues/461)
- Refonte de la barre de filtre [#301](https://gitlab.libriciel.fr/webACTES/webACTES/issues/301)

### Corrections

- Erreurs du serveur avec code http 500 [#454](https://gitlab.libriciel.fr/webACTES/webACTES/issues/454)
- Initialisation des droits corrects pour les structures [#455](https://gitlab.libriciel.fr/webACTES/webACTES/issues/455)
- Api correctement restreinte en fonction des rôles [#439](https://gitlab.libriciel.fr/webACTES/webACTES/issues/439)
- Amélioration des performances dans l'utilisation de flowable
- Utilisation de l'info panel partout
- Erreur orthographe Validation en urgence [#463](https://gitlab.libriciel.fr/webACTES/webACTES/issues/463)
- Liste déroulante : effacer en fr [#361](https://gitlab.libriciel.fr/webACTES/webACTES/issues/361)
- Effacement des personnes dans un circuit provoque la fermeture de la fenêtre "Popup"
- Correction orthographique "Confirmation de suppression d'étape dans un circuit"

## [1.0.0-1.0.6] - 16/10/2019

### Ajouts

- Installation scriptée en prod

### Evolutions

- Renommage menu et titre de page "Prêts pour télétransmission => A ordonner" [#438](https://gitlab.libriciel.fr/webACTES/webACTES/issues/438)
- Affichage des toaster 10s [#392](https://gitlab.libriciel.fr/webACTES/webACTES/issues/392)

### Corrections

- Incohérence pour les droits sur les projets en cours de validation pour l'admin [#453](https://gitlab.libriciel.fr/webACTES/webACTES/issues/453)
- Permissions correctes pour le rôle secrétaire

## [1.0.0-1.0.5] - 15/10/2019

### Ajouts

- Type d'utilisateur : Admin fonctionnel [#436](https://gitlab.libriciel.fr/webACTES/webACTES/issues/436)

### Evolutions

- Visualisation de la validation en urgence
- Quand un projet est dans un circuit, on ne doit pas pouvoir modifier son état [#430](https://gitlab.libriciel.fr/webACTES/webACTES/issues/430)
- Modification des droits sur les menus [#431](https://gitlab.libriciel.fr/webACTES/webACTES/issues/431)
- Circuits utilisés non éditables [#451](https://gitlab.libriciel.fr/webACTES/webACTES/issues/451)
- Les circuits restent actifs après édition

### Corrections

- Suppression de circuit sans erreurs au retour
- Suppression de l'usage de flash dans certains contrôleurs [#258](https://gitlab.libriciel.fr/webACTES/webACTES/issues/258)
- CI - Récupération de l'image correcte lors d'un déploiement

## [1.0.0-1.0.4] - 07/10/2019

### Ajouts

- Détail des circuits actifs [#419](https://gitlab.libriciel.fr/webACTES/webACTES/issues/419)
- Infobulle sur les switch [#429](https://gitlab.libriciel.fr/webACTES/webACTES/issues/429)
- Icônes pour les états dans les tableaux [#181](https://gitlab.libriciel.fr/webACTES/webACTES/issues/181)
- Remise à des éléments relatifs à la classification et au typage en cas de modification de celle-ci [#337](https://gitlab.libriciel.fr/webACTES/webACTES/issues/337)
- Création circuit : vérification des utilisateurs soumis [#376](https://gitlab.libriciel.fr/webACTES/webACTES/issues/376)
- Informations pour client pour l'historique de la validation en urgence

### Evolutions

- Couleurs de l'application [#275](https://gitlab.libriciel.fr/webACTES/webACTES/issues/275)
- Actes signés non transmissibles → changement de titre [#434](https://gitlab.libriciel.fr/webACTES/webACTES/issues/434)
- Modification de titres divers [#438](https://gitlab.libriciel.fr/webACTES/webACTES/issues/438)
- Liste des Actes validés télétransmis - changement de titre [#433](https://gitlab.libriciel.fr/webACTES/webACTES/issues/433)
- Spinner lors de l'enregistrement d'un projet [#386](https://gitlab.libriciel.fr/webACTES/webACTES/issues/386)
- Modification des Toastes pour les erreurs de format des fichiers [#423](https://gitlab.libriciel.fr/webACTES/webACTES/issues/423)
- Édition de modèle de circuit bloquée s'il est instancié [#371](https://gitlab.libriciel.fr/webACTES/webACTES/issues/371)

### Corrections

- Suppression du bouton d'envoi en lot dans un circuit

## [1.0.0-1.0.3] - 30/09/2019

### Ajouts

- Colonnes étape courante [#405](https://gitlab.libriciel.fr/webACTES/webACTES/issues/405)
- Validation en urgence [#403](https://gitlab.libriciel.fr/webACTES/webACTES/issues/403)
- Boutons en fonction du statut du projet [#403](https://gitlab.libriciel.fr/webACTES/webACTES/issues/403)

### Évolutions

- Accepte un connecteur Pastell vide

## [1.0.0-1.0.2] - 30/09/2019

### Évolutions

- Couleur du bouton "Valider en urgence" [#409](https://gitlab.libriciel.fr/webACTES/webACTES/issues/409)
- Modification de comportement pour les documents principaux trop volumineux ou les annexes trop volumineuses
- Masquer la description du circuit lors de la sélection [#402](https://gitlab.libriciel.fr/webACTES/webACTES/issues/402)
- Utiliser le compo ls pour textarea [#351](https://gitlab.libriciel.fr/webACTES/webACTES/issues/351)
- Modification de projet - message d'erreur [#359](https://gitlab.libriciel.fr/webACTES/webACTES/issues/359)

### Corrections

- Refactor Technique : Enlever tous les "return new Observable()" [#378](https://gitlab.libriciel.fr/webACTES/webACTES/issues/378)

## [1.0.0-1.0.1] - 25/09/2019

### Ajouts

- Création et utilisation des circuits
- Validation en urgence que pour les administrateurs [#399](https://gitlab.libriciel.fr/webACTES/webACTES/issues/399)
- Validations unitaires pour les projets [#401](https://gitlab.libriciel.fr/webACTES/webACTES/issues/401)

### Évolutions

- Filtrage sans tenir compte de la casse
- Modification de message pour les informations manquantes de télétransmission
- Amélioration CI et déploiement

### Corrections

- Pas de validation en lot [#400](https://gitlab.libriciel.fr/webACTES/webACTES/issues/400)
- Fil d'Ariane des séances correct

## [1.0.0-1.0.0] - 23/09/2019

### Ajouts

- Contient le cas nominal de création de projets d'actes jusqu'à l'envoi au TDT mais sans la gestion des circuits.
