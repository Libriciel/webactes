<?php

use App\Event\GenerateListener;
use App\Event\NotificationsListener;
use App\Event\PastellDocumentListener;
use App\Event\PastellTdtListener;
use Cake\Core\Configure;
use Cake\Event\EventManager;

define('MAIN_URL', env('MAIN_URL', 'notSet'));
define('API_CLIENT_NAME', 'api-client');
define('WEB_CLIENT_NAME', 'web-client');

Configure::write('annexeAcceptedTypes', [
    'application/pdf',
    'image/png',
    'image/jpeg',
    'application/vnd.oasis.opendocument.text',
    'application/xml',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'application/vnd.oasis.opendocument.spreadsheet',
    'application/vnd.oasis.opendocument.presentation',
]);

Configure::write('mainDocumentAcceptedTypes', [
    'application/pdf',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.oasis.opendocument.text',
]);

Configure::write('mainDocumentAcceptedTypesForSigned', [
    'application/pdf',
]);

Configure::write('generateTemplateAcceptedTypes', [
    'application/vnd.oasis.opendocument.text',
]);

Configure::write('draftTemplateAcceptedTypes', [
    'application/vnd.oasis.opendocument.text',
]);
define('EDITOR_WOPI_CLIENT_URL', env('EDITOR_WOPI_CLIENT_URL'));
define('EDITOR_WOPI_PATH', env('EDITOR_WOPI_PATH'));

Configure::write('projetTextAcceptedTypes', [
    'application/vnd.oasis.opendocument.text',
]);

Configure::write('attachmentSummonAcceptedTypes', [
    'application/pdf',
]);

define('WORKSPACE', '/data/workspace');
define('LOGO', WORKSPACE . '/logo');

define('PAGINATION_LIMIT', 10);
define('MAXIMUM_LIMIT', 500);

//Myec3
define('MYEC3_IP_ALLOWED', env('MYEC3_IP_ALLOWED', null));
define('HAS_PROVISIONING', env('HAS_PROVISIONING', false) === 'true' ? true : false);

define('XSD_SOCLE', env('XSD_SOCLE', TESTS . 'TestCase' . DS . 'Socle' . DS . 'xml' . DS . 'schema.xsd'));

//keycloak
define('SERVEUR_URL', 'https://' . MAIN_URL . '/auth');
define('KEYCLOAK_CONFIG_DIR', CONFIG . 'KeycloakConfig' . DS);
define('KEYCLOAK_API_CLIENT_PATH', KEYCLOAK_CONFIG_DIR . 'keycloak_api_realmWA.json');
define('KEYCLOAK_WEB_CLIENT_PATH', KEYCLOAK_CONFIG_DIR . 'keycloak_web_realmWA.json');
define('KEYCLOAK_MASTER_PATH', KEYCLOAK_CONFIG_DIR . 'keycloakMaster.json');
define('KEYCLOAK_USERNAME', env('KEYCLOAK_USERNAME', 'username'));
define('TEST_KEYCLOAK', false);
define('KEYCLOAK_USER', env('KEYCLOAK_USER', null));
define('KEYCLOAK_PASSWORD', env('KEYCLOAK_PASSWORD', null));
define('KEYCLOAK_TEST_USER', env('KEYCLOAK_TEST_USER', 's.blanc'));
define('KEYCLOAK_TEST_PASSWORD',env('KEYCLOAK_TEST_PASSWORD', 'Aqwxsz123!er'));

Configure::write('KEYCLOAK_DEV_MODE', false);

// ETAT DES SEANCES
define('ETAT_SITTING_OPEN', 1);
define('ETAT_SITTING_CLOSE', 2);

// NATURES
Configure::write('naturesTypeAbregeAccepted', [
    'DE', //Délibérations
    'AR', //Actes réglementaires
    'CC', //Contrats,conventions et avenants
    'AU', //Autres
    'AI', //Actes individuels
]);

// @fixme : déplacer dans des constantes de StateActEntity
define('DRAFT', 1);
define('VALIDATION_PENDING', 2);
define('VALIDATED', 3);
define('REFUSED', 4);
define('DECLARE_SIGNED', 5);
define('PARAPHEUR_SIGNED', 6);
define('PARAPHEUR_REFUSED', 7);
define('PARAPHEUR_SIGNING', 8);
define('TO_TDT', 9);
define('PENDING_TDT', 10);
define('ACQUITTED', 11);
define('TDT_CANCEL', 12);
define('TDT_ERROR', 13);
define('PENDING_PASTELL', 14);
define('TO_ORDER', 15);
define('TDT_CANCEL_PENDING', 16);
define('PARAPHEUR_PENDING', 17);
define('VOTED_APPROVED', 18);
define('VOTED_REJECTED', 19);
define('TAKENOTE', 20);

Configure::write('Flux', [
    'actes-generique' =>
        [
            'name' => 'actes-generique',
            'tdt' => [
                'action' => 'send-tdt',
                'name' => 'tdt',
                'message' =>
                    [
                        'error' =>
                            [
                                'classification_non_a_jour' => 'La classification utilisée n\'est plus à jour',
                                'numero_incorecte' => 'Erreur lors de la transmission, S²low a répondu : KO
        Un acte portant le même numéro interne existe déjà dans la base de données.
        Il faut peut-être ajouter un suffixe au numéro.',
                                'action_send_tdt_non_permise' => 'L\'action « send-tdt »  n\'est pas permise : or_1 n\'est pas vérifiée',
                                'classificationActe' => 'Le formulaire est incomplet : le champ «Classification Actes» est obligatoire.',
                            ],
                    ],
            ],
            'signature-electronique' => [
                'action' => 'send-iparapheur',
                'name' => 'signature-electronique',
                'messages',
            ],
        ],
]);

// # LISTENERS
$PastellTdtLitener = new PastellTdtListener();
EventManager::instance()->on($PastellTdtLitener);

$PastellDocumentLitener = new PastellDocumentListener();
EventManager::instance()->on($PastellDocumentLitener);

$generateListener = new GenerateListener();
EventManager::instance()->on($generateListener);

$NorificationsLitener = new NotificationsListener();
EventManager::instance()->on($NorificationsLitener);

define('S2LOW_URL', 'https://s2low.formations.libriciel.fr/modules/actes/actes_transac_post_confirm_api.php');
define('S2LOW_API', '/modules/actes/actes_transac_post_confirm_api.php');

// Notifications
Configure::write([
    'Email' => [
        'User' => [
            'Type' => ['email', 'push'],
        ],
    ],
]);

// Flowable
Configure::write([
    'Flowable' => [
        'wrapper_mock' => null, // \App\Test\Mock\FlowableWrapperMock
        'host' => env('WEBACTES_FLOWABLE_SERVICE_HOST', 'flowable'),
        'port' => env('WEBACTES_FLOWABLE_SERVICE_PORT', 8080),
        'auth' => [
            'username' => env('FLOWABLE_ADMIN', 'rest-admin'),
            'password' => env('FLOWABLE_PASSWORD', 'test'),
        ],
    ],
]);

/* Configuration pour la manipulation des pdf */
Configure::write('PDFINFO_EXEC', '/usr/bin/pdfinfo');

define('PASTELL_TEST_USER', env('PASTELL_TEST_USER', ''));
define('PASTELL_TEST_PASSWORD', env('PASTELL_TEST_PASSWORD', ''));
define('IDELIBRE_TEST_USER', env('IDELIBRE_TEST_USER', ''));
define('IDELIBRE_TEST_PASSWORD', env('IDELIBRE_TEST_PASSWORD', ''));
define('IDELIBRE_TEST_CONNEXION', env('IDELIBRE_TEST_CONNEXION', ''));
define('TEST_GENERATION', false);
define('TEST_SOCLE', false);

define('PROJECT_VIEW', 'https://' . MAIN_URL . '/projets/');

define('MAIL_FROM', env('MAIL_FROM', 'webact@libriciel.fr'));

Configure::write('PhpOdtApi.export', env('PHP_ODT_API_EXPORT', '') === 'true');
// Nombre combiné maximum de pages d'annexes jointes à la fusion pour un projet
Configure::write('Project.Annexes.Generate.maxPagesCount', (int)env('PROJECT_ANNEXES_GENERATE_MAX_PAGES_COUNT', '1000'));
