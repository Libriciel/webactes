<?php

use Migrations\AbstractSeed;

/**
 * DatabaseSeed seed.
 */
class DatabaseProdSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $this->call('OrganizationsProdSeed');

        // You can use the plugin dot syntax to call seeders from a plugin
        //$this->call('PluginName.FromPluginSeed');
    }
}
