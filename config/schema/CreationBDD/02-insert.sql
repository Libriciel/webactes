BEGIN;

--
-- Insertion de valeur dans la table organizations
--
INSERT INTO organizations(name, created, modified)
VALUES ('Montpellier',
        NOW(),
        NOW());

--
-- Insertion de valeur dans la table structures
--
INSERT INTO structures(organization_id, business_name, siret, address, post_code, city, phone, created, modified)
VALUES ((SELECT id FROM organizations WHERE name = 'Montpellier'),
        'Libriciel SCOP',
        '49101169800025',
        '836 Rue du Mas de Verchant',
        '34000',
        'MONTPELLIER',
        '0467659644',
        NOW(),
        NOW());

--
-- Insertion de valeur dans la table officials
--
INSERT INTO officials(organization_id, structure_id, civility, lastname, firstname, email, created, modified)
VALUES ((SELECT id FROM organizations WHERE name = 'Montpellier'),
        (SELECT id FROM structures WHERE business_name = 'Libriciel SCOP'),
        'M.',
        'LOSSERAND',
        'Frédéric',
        'frederic.losserand@test.fr',
        NOW(),
        NOW());

--
-- Insertion de valeur dans la table dpos
--
INSERT INTO dpos(organization_id, structure_id, civility, lastname, firstname, email, created, modified)
VALUES ((SELECT id FROM organizations WHERE name = 'Montpellier'),
        (SELECT id FROM structures WHERE business_name = 'Libriciel SCOP'),
        'M.',
        'GUILLON',
        'Théo',
        'dpo@test.fr',
        NOW(),
        NOW());

--
-- Insertion de valeur dans la table users
--
INSERT INTO users(organization_id, keycloak_id, civility, lastname, firstname, username, email, data_security_policy,
                  created,
                  modified)
VALUES ((SELECT id FROM organizations WHERE name = 'Montpellier'),
        1,
        'M.',
        'PLAZA',
        'Sébastien',
        'splaza',
        's.plaza@test.fr',
        true,
        NOW(),
        NOW());

--
-- Insertion de valeur dans la table structures_users
--
INSERT INTO structures_users(structure_id, user_id)
VALUES ((SELECT id FROM structures WHERE business_name = 'Libriciel SCOP'),
        (SELECT id FROM users WHERE lastname = 'PLAZA'));

--
-- Insertion de valeur dans la table typesittings
--
INSERT INTO typesittings(organization_id, structure_id, name, created, modified)
VALUES ((SELECT id FROM organizations WHERE name = 'Montpellier'),
        (SELECT id FROM structures WHERE business_name = 'Libriciel SCOP'), 'Conseil Municipal', NOW(), NOW()),
       ((SELECT id FROM organizations WHERE name = 'Montpellier'),
        (SELECT id FROM structures WHERE business_name = 'Libriciel SCOP'), 'Conseil Communautaire', NOW(), NOW());

--
-- Insertion de valeur dans la table actors
--
INSERT INTO actors(organization_id, structure_id, civility, lastname, firstname, email, address, post_code, city,
                   created,
                   modified)
VALUES ((SELECT id FROM organizations WHERE name = 'Montpellier'),
        (SELECT id FROM structures WHERE business_name = 'Libriciel SCOP'), 'Mme.', 'BLANC', 'Louise',
        'l.blanc@test.fr',
        '1 avenue Gambetta', '34000', 'MONTPELLIER', NOW(), NOW()),
       ((SELECT id FROM organizations WHERE name = 'Montpellier'),
        (SELECT id FROM structures WHERE business_name = 'Libriciel SCOP'), 'M.', 'BLEU', 'Etienne', 'e.bleu@test.fr',
        '145 impasse du Levant', '34000', 'MONTPELLIER', NOW(), NOW()),
       ((SELECT id FROM organizations WHERE name = 'Montpellier'),
        (SELECT id FROM structures WHERE business_name = 'Libriciel SCOP'), 'M.', 'VERT', 'Elliot', 'e.vert@test.fr',
        '4 boulevard du Jeu de Paume', '34000', 'MONTPELLIER', NOW(), NOW());

--
-- Insertion de valeur dans la table stateacts
--
INSERT INTO stateacts(name, created, modified)
VALUES ('Brouillon', NOW(), NOW()),
       ('En cours de validation', NOW(), NOW()),
       ('Validé', NOW(), NOW()),
       ('Refusé', NOW(), NOW()),
       ('À faire voter', NOW(), NOW()),
       ('Voté', NOW(), NOW()),
       ('À faire signer', NOW(), NOW()),
       ('En cours de signature', NOW(), NOW()),
       ('Signé', NOW(), NOW()),
       ('Refusé par le i-Parapheur', NOW(), NOW()),
       ('À télétransmettre', NOW(), NOW()),
       ('Télétransmis', NOW(), NOW()),
       ('Acquitté', NOW(), NOW()),
       ('Annulé', NOW(), NOW()),
       ('En erreur sur le TdT', NOW(), NOW());

COMMIT;



INSERT INTO templates(organization_id, structure_id, name, version,
                      created,
                      modified)
VALUES (1,
        1,
        'template',
        1,
        NOW(),
        NOW());



INSERT INTO projects(organization_id, structure_id, theme_id, template_id, name,
                     created,
                     modified)
VALUES (1,
        1,
        1,
        1,
        'mon projet',
        NOW(),
        NOW());



INSERT INTO containers(organization_id, structure_id, project_id,
                     created,
                     modified)
VALUES (1,
        1,
        1,
        NOW(),
        NOW());