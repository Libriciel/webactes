--
-- PostgreSQL database dump
--

BEGIN;

--
-- Création de la table organizations
--
CREATE TABLE organizations
(
  id       SERIAL                      NOT NULL PRIMARY KEY,
  active   BOOLEAN                     NOT NULL DEFAULT TRUE,
  name     VARCHAR(100)                NOT NULL,
  created  timestamp without time zone NOT NULL,
  modified timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX organizations_name_idx ON organizations (name);

--
-- Création de la table structures
--
CREATE TABLE structures
(
  id                SERIAL                      NOT NULL PRIMARY KEY,
  organization_id   INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  active            BOOLEAN                     NOT NULL DEFAULT TRUE,
  logo              TEXT,
  business_name     VARCHAR(100)                NOT NULL,
  siret             VARCHAR(14)                 NOT NULL,
  address           VARCHAR(100)                NOT NULL,
  addresscomplement VARCHAR(100),
  post_code         VARCHAR(5)                  NOT NULL,
  city              VARCHAR(100)                NOT NULL,
  phone             VARCHAR(15)                 NOT NULL,
  created           timestamp without time zone NOT NULL,
  modified          timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX structures_business_name_idx ON structures (business_name);
CREATE UNIQUE INDEX structures_siret_idx ON structures (siret);

-- ALTER TABLE structures ADD CONSTRAINT structures_phone_phone_chk CHECK ( cakephp_validate_phone( phone, NULL, 'fr' ) );
-- ALTER TABLE structures ADD CONSTRAINT structures_siret_alpha_numeric_chk CHECK ( cakephp_validate_alpha_numeric( siret ) );
-- ALTER TABLE structures ADD CONSTRAINT structures_codepostal_alpha_numeric_chk CHECK ( cakephp_validate_alpha_numeric( codepostal ) );

--
-- Création de la table roles
--
CREATE TABLE roles
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(100)                NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX roles_name_idx ON roles (name);

--
-- Création de la table groups
--
CREATE TABLE groups
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  role_id         INTEGER                     NOT NULL REFERENCES roles (id) ON DELETE CASCADE ON UPDATE CASCADE,
  active          BOOLEAN                     NOT NULL DEFAULT TRUE,
  name            VARCHAR(100)                NOT NULL,
  type            VARCHAR(100)                NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX groups_name_idx ON groups (name);

-- ALTER TABLE groups ADD CONSTRAINT groups_type_in_list_chk CHECK (cakephp_validate_in_list(type, ARRAY['superadmin', 'admin', 'redacteur']));

--
-- Création de la table officials
--
CREATE TABLE officials
(
  id                 SERIAL                      NOT NULL PRIMARY KEY,
  organization_id    INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id       INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  civility           VARCHAR(4)                  NOT NULL,
  lastname           VARCHAR(50)                 NOT NULL,
  firstname          VARCHAR(50)                 NOT NULL,
  email              VARCHAR(100)                NOT NULL,
  adresse            VARCHAR(100),
  address_supplement VARCHAR(100),
  post_code          VARCHAR(5),
  city               VARCHAR(100),
  phone              VARCHAR(15),
  created            timestamp without time zone NOT NULL,
  modified           timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX officials_structure_id_email_idx ON officials (structure_id, email);

--
-- Création de la table dpos
--
CREATE TABLE dpos
(
  id                 SERIAL                      NOT NULL PRIMARY KEY,
  organization_id    INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id       INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  civility           VARCHAR(4)                  NOT NULL,
  lastname           VARCHAR(50)                 NOT NULL,
  firstname          VARCHAR(50)                 NOT NULL,
  email              VARCHAR(100)                NOT NULL,
  address            VARCHAR(100),
  address_supplement VARCHAR(100),
  post_code          VARCHAR(5),
  city               VARCHAR(100),
  phone              VARCHAR(15),
  created            timestamp without time zone NOT NULL,
  modified           timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX dpos_structure_id_email_idx ON dpos (structure_id, email);

--
-- Création de la table users
--
CREATE TABLE users
(
  id                   SERIAL                      NOT NULL PRIMARY KEY,
  organization_id      INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  keycloak_id          character varying(255),
  active               BOOLEAN                     NOT NULL DEFAULT TRUE,
  civility             VARCHAR(4)                  NOT NULL,
  lastname             VARCHAR(50)                 NOT NULL,
  firstname            VARCHAR(50)                 NOT NULL,
  username             VARCHAR(50)                 NOT NULL,
  email                VARCHAR(100)                NOT NULL,
  phone                VARCHAR(15),
  group_id             INTEGER                     REFERENCES groups (id) ON DELETE SET NULL,
  data_security_policy BOOLEAN                     NOT NULL DEFAULT FALSE,
  created              timestamp without time zone NOT NULL,
  modified             timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX users_email_idx ON users (email);

-- ALTER TABLE users ADD CONSTRAINT users_civility_in_list_chk CHECK (cakephp_validate_in_list(civility, ARRAY['M', 'Mme']));
-- ALTER TABLE users ADD CONSTRAINT users_email_email_chk CHECK (cakephp_validate_email(email));

--
-- Création de la table de jointure structures Users
--
CREATE TABLE structures_users
(
  id           SERIAL PRIMARY KEY NOT NULL,
  structure_id INTEGER            NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  user_id      INTEGER            NOT NULL REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX structures_users_user_id_societe_id_idx ON structures_users (user_id, structure_id);

--
-- Création de la table services
--
CREATE TABLE services
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  active          BOOLEAN                     NOT NULL DEFAULT TRUE,
  name            VARCHAR(100)                NOT NULL,
  parent_id       INTEGER                              DEFAULT 0,
  lft             INTEGER                              DEFAULT 0,
  rght            INTEGER                              DEFAULT 0,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

CREATE UNIQUE INDEX services_name_structure_id_idx ON services (name, structure_id);

--
-- Création de la table de jointure structures Services
--
CREATE TABLE structures_services
(
  id           SERIAL PRIMARY KEY NOT NULL,
  structure_id INTEGER            NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  service_id   INTEGER            NOT NULL REFERENCES services (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX structures_services_structure_id_service_id_idx ON structures_services (structure_id, service_id);

--
-- Création de la table de jointure Users Services
--
CREATE TABLE users_services
(
  id         SERIAL  NOT NULL PRIMARY KEY,
  user_id    INTEGER NOT NULL REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
  service_id INTEGER NOT NULL REFERENCES services (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE UNIQUE INDEX users_services_user_id_service_id_idx ON users_services (user_id, service_id);


--
-- Création de la table typesittings
--
CREATE TABLE typesittings
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(100)                NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX typesittings_structure_id_name_idx ON typesittings (structure_id, name);

--
-- Création de la table themes
--
CREATE TABLE themes
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  active          BOOLEAN                     NOT NULL DEFAULT TRUE,
  name            VARCHAR(100)                NOT NULL,
  parent_id       INTEGER                              DEFAULT 0,
  lft             INTEGER                              DEFAULT 0,
  rght            INTEGER                              DEFAULT 0,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX themes_structure_id_name_idx ON themes (structure_id, name);

--
-- Création de la table templates
--
CREATE TABLE templates
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(100)                NOT NULL,
  version         INTEGER                     NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX templates_structure_id_name_idx ON templates (structure_id, name);

--
-- Création de la table projects
--
CREATE TABLE projects
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  theme_id        INTEGER                     NOT NULL REFERENCES themes (id) ON DELETE CASCADE ON UPDATE CASCADE,
  template_id     INTEGER REFERENCES templates (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(100)                NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

--
-- Création de la table tabs
--
CREATE TABLE tabs
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  template_id     INTEGER                     NOT NULL REFERENCES templates (id) ON DELETE CASCADE ON UPDATE CASCADE,
  position        INTEGER                     NOT NULL,
  tab             VARCHAR(10)                 NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

--
-- Création de la table fields
--
CREATE TABLE fields
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(100)                NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX fields_structure_id_name_idx ON fields (structure_id, name);

--
-- Création de la table metadatas
--
CREATE TABLE metadatas
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  template_id     INTEGER                     NOT NULL REFERENCES templates (id) ON DELETE CASCADE ON UPDATE CASCADE,
  tab_id          INTEGER                     NOT NULL REFERENCES tabs (id) ON DELETE CASCADE ON UPDATE CASCADE,
  field_id        INTEGER                     NOT NULL REFERENCES fields (id) ON DELETE CASCADE ON UPDATE CASCADE,
  number_line     INTEGER                     NOT NULL,
  number_column   INTEGER                     NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

--
-- Création de la table values
--
CREATE TABLE values
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  template_id     INTEGER                     NOT NULL REFERENCES templates (id) ON DELETE CASCADE ON UPDATE CASCADE,
  json            jsonb,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

--
-- Création de la table acts
--
CREATE TABLE acts
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  template_id     INTEGER                     NOT NULL REFERENCES templates (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(100)                NOT NULL,
  code_act        VARCHAR(15)                 NOT NULL,
  date_send       timestamp without time zone,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX acts_structure_id_name_idx ON acts (structure_id, name);
CREATE UNIQUE INDEX acts_number_idx ON acts (code_act);

--
-- Création de la table natures
--
CREATE TABLE natures
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(100)                NOT NULL,
  code            VARCHAR(5)                  NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);


--
-- Création de la table typesacts
--
CREATE TABLE typesacts
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  nature_id       INTEGER                     NOT NULL REFERENCES natures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(100)                NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX typesacts_structure_id_name_idx ON typesacts (structure_id, name);

--
-- Création de la table actors
--
CREATE TABLE actors
(
  id                 SERIAL                      NOT NULL PRIMARY KEY,
  organization_id    INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id       INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  active             BOOLEAN                     NOT NULL DEFAULT TRUE,
  civility           VARCHAR(4)                  NOT NULL,
  lastname           VARCHAR(50)                 NOT NULL,
  firstname          VARCHAR(50)                 NOT NULL,
  birthday           date,
  email              VARCHAR(100)                NOT NULL,
  title              VARCHAR(250),
  address            VARCHAR(100)                NOT NULL,
  address_supplement VARCHAR(100),
  post_code          VARCHAR(5)                  NOT NULL,
  city               VARCHAR(100)                NOT NULL,
  phone              VARCHAR(15),
  cellphone          VARCHAR(15),
  created            timestamp without time zone NOT NULL,
  modified           timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX actors_email_idx ON actors (email);
CREATE UNIQUE INDEX actors_structure_id_email_idx ON actors (structure_id, email);

--
-- Création de la table de jointure Projects Actors
-- Rapporteur(s) d'un project
--
CREATE TABLE actors_projects
(
  id         SERIAL  NOT NULL PRIMARY KEY,
  project_id INTEGER NOT NULL REFERENCES projects (id) ON DELETE CASCADE ON UPDATE CASCADE,
  actor_id   INTEGER NOT NULL REFERENCES actors (id) ON DELETE CASCADE ON UPDATE CASCADE
);

--
-- Création de la table de jointure Acts Actors
-- Rapporteur(s) d'un acts
--
CREATE TABLE actors_acts
(
  id       SERIAL  NOT NULL PRIMARY KEY,
  act_id   INTEGER NOT NULL REFERENCES acts (id) ON DELETE CASCADE ON UPDATE CASCADE,
  actor_id INTEGER NOT NULL REFERENCES actors (id) ON DELETE CASCADE ON UPDATE CASCADE
);

--
-- Création de la table seances
--
CREATE TABLE sittings
(
  id               SERIAL                      NOT NULL PRIMARY KEY,
  organization_id  INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id     INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  typesitting_id   INTEGER                     NOT NULL REFERENCES typesittings (id) ON DELETE CASCADE ON UPDATE CASCADE,
  date             timestamp without time zone NOT NULL,
  date_convocation timestamp without time zone,
  created          timestamp without time zone NOT NULL,
  modified         timestamp without time zone NOT NULL
);

--
-- Création de la table de jointure Actors sittings
--
CREATE TABLE actors_sittings
(
  id         SERIAL  NOT NULL PRIMARY KEY,
  actor_id   INTEGER NOT NULL REFERENCES actors (id) ON DELETE CASCADE ON UPDATE CASCADE,
  sitting_id INTEGER NOT NULL REFERENCES sittings (id) ON DELETE CASCADE ON UPDATE CASCADE
);

--
-- Création de la table convocations
--
CREATE TABLE convocations
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  actor_id        INTEGER                     NOT NULL REFERENCES actors (id) ON DELETE CASCADE ON UPDATE CASCADE,
  sitting_id      INTEGER                     NOT NULL REFERENCES sittings (id) ON DELETE CASCADE ON UPDATE CASCADE,
  date_send       timestamp without time zone NOT NULL,
  date_open       timestamp without time zone,
  jeton           INTEGER,
  pastell_id      INTEGER                     NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX convocations_actor_id_structure_id_idx ON convocations (actor_id, structure_id);

--
-- Création de la table stateacts
--
CREATE TABLE stateacts
(
  id       SERIAL                      NOT NULL PRIMARY KEY,
  name     VARCHAR(50)                 NOT NULL,
  created  timestamp without time zone NOT NULL,
  modified timestamp without time zone NOT NULL
);
CREATE UNIQUE INDEX stateacts_name_idx ON stateacts (name);

--
-- Création de la table containers
--
CREATE TABLE containers
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  act_id          INTEGER REFERENCES acts (id) ON DELETE CASCADE ON UPDATE CASCADE,
  project_id      INTEGER REFERENCES projects (id) ON DELETE CASCADE ON UPDATE CASCADE,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

--
-- Création de la table de jointure Containers Stateacts
--
CREATE TABLE containers_stateacts
(
  id           SERIAL                      NOT NULL PRIMARY KEY,
  container_id INTEGER                     NOT NULL REFERENCES containers (id) ON DELETE CASCADE ON UPDATE CASCADE,
  stateact_id  INTEGER                     NOT NULL REFERENCES stateacts (id) ON DELETE CASCADE ON UPDATE CASCADE,
  created      timestamp without time zone NOT NULL
);

--
-- Création de la table classifications
--
CREATE TABLE classifications
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  parent_id       VARCHAR(50),
  name            VARCHAR(50)                 NOT NULL,
  libelle         VARCHAR(50)                 NOT NULL,
  lft             INTEGER DEFAULT 0,
  rght            INTEGER DEFAULT 0,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

--
-- Création de la table typologiepieces
--
CREATE TABLE documenttypologies
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(100)                NOT NULL,
  code            VARCHAR(5)                  NOT NULL,
  lft             INTEGER DEFAULT 0,
  rght            INTEGER DEFAULT 0,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

--
-- Création de la table de jointure Natures Typologiepieces
--
CREATE TABLE natures_typologiepieces
(
  id                  SERIAL  NOT NULL PRIMARY KEY,
  nature_id           INTEGER NOT NULL REFERENCES natures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  documenttypology_id INTEGER NOT NULL REFERENCES documenttypologies (id) ON DELETE CASCADE ON UPDATE CASCADE
);

--
-- Création de la table tdt_messages
--
CREATE TABLE tdt_messages
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  tdt_id          INTEGER                     NOT NULL,
  tdt_type        INTEGER                     NOT NULL,
  tdt_etat        INTEGER,
  date_message    date,
  tdt_data        bytea,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

--
-- Création de la table histories
--
CREATE TABLE histories
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  container_id    INTEGER                     NOT NULL REFERENCES containers (id) ON DELETE CASCADE ON UPDATE CASCADE,
  user_id         INTEGER                     NOT NULL REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
  comment         text                        NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

--
-- Création de la table attachments
--
CREATE TABLE attachments
(
  id              SERIAL                      NOT NULL PRIMARY KEY,
  organization_id INTEGER                     NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER                     NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(50)                 NOT NULL,
  created         timestamp without time zone NOT NULL,
  modified        timestamp without time zone NOT NULL
);

--
-- Création de la table de jointure Convocations Attachments
--
CREATE TABLE convocations_attachments
(
  id             SERIAL  NOT NULL PRIMARY KEY,
  convocation_id INTEGER NOT NULL REFERENCES convocations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  attachment_id  INTEGER NOT NULL REFERENCES attachments (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX convocations_attachments_convocation_id_attachment_id_idx ON convocations_attachments (convocation_id, attachment_id);



CREATE TABLE maindocuments
(
  id              SERIAL       NOT NULL PRIMARY KEY,
  organization_id INTEGER      NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER      NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  container_id    INTEGER      NOT NULL REFERENCES containers (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(512) NOT NULL
);


CREATE TABLE annexes
(
  id              SERIAL  NOT NULL PRIMARY KEY,
  organization_id INTEGER NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  container_id    INTEGER NOT NULL REFERENCES containers (id) ON DELETE CASCADE ON UPDATE CASCADE,
  rank            INTEGER NOT NULL,
  istransmissible BOOLEAN NOT NULL default true
);


CREATE TABLE files
(
  id              SERIAL       NOT NULL PRIMARY KEY,
  organization_id INTEGER      NOT NULL REFERENCES organizations (id) ON DELETE CASCADE ON UPDATE CASCADE,
  structure_id    INTEGER      NOT NULL REFERENCES structures (id) ON DELETE CASCADE ON UPDATE CASCADE,
  maindocument_id INTEGER REFERENCES maindocuments (id) ON DELETE CASCADE ON UPDATE CASCADE,
  annex_id        INTEGER REFERENCES annexes (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR(512) NOT NULL,
  path            VARCHAR(512) NOT NULL,
  mimetype        VARCHAR(64)  NOT NULL,
  size            BIGINT
);


CREATE TABLE containers_sittings
(
  id           SERIAL  NOT NULL PRIMARY KEY,
  container_id INTEGER NOT NULL REFERENCES containers (id) ON DELETE CASCADE ON UPDATE CASCADE,
  sitting_id   INTEGER NOT NULL REFERENCES sittings (id) ON DELETE CASCADE ON UPDATE CASCADE
);

COMMIT;
