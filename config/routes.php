<?php
/**
 * Routes configuration.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * It's loaded within the context of `Application::routes()` method which
 * receives a `RouteBuilder` instance `$routes` as method argument.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

return static function (RouteBuilder $routes) {
    /*
     * The default class to use for all routes
     *
     * The following route classes are supplied with CakePHP and are appropriate
     * to set as the default:
     *
     * - Route
     * - InflectedRoute
     * - DashedRoute
     *
     * If no call is made to `Router::defaultRouteClass()`, the class used is
     * `Route` (`Cake\Routing\Route\Route`)
     *
     * Note that `Route` does not do any inflections on URLs which will result in
     * inconsistently cased URLs when used with `{plugin}`, `{controller}` and
     * `{action}` markers.
     */
    $routes->setRouteClass(DashedRoute::class);

    $routes->scope('/api/v1/', function (RouteBuilder $builder) {
        //$builder->setExtensions(['json']);
        // Register scoped middleware for in scopes.
        // $builder->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        //  'httponly' => false,
        //]));

        /**
         * Apply a middleware to the current route scope.
         * Requires middleware to be registered via `Application::routes()` with `registerMiddleware()`
         */
        //$builder->applyMiddleware('csrf');
        $builder->resources('Actors');
        $builder->resources('ActorGroups', ['inflect' => 'dasherize']);
        $builder->resources('Acts');
        $builder->resources('Containers');
        $builder->resources('ContainersSittings');
        $builder->resources('Dpos');
        $builder->resources('Officials');
        $builder->resources('Natures');
        $builder->resources('Services');
        $builder->resources('Themes');
        $builder->resources('Logs');
        $builder->resources('Maindocuments');
        $builder->resources('Matieres');
        $builder->resources('Natures');
        $builder->resources('Officials');
        $builder->resources('Organizations');
        $builder->resources('Projects');
        $builder->resources('Signatures');
        $builder->resources('Services');
        $builder->resources('Sittings');
        $builder->resources('Stateacts');
        $builder->resources('Structures');
        $builder->resources('StructureSettings');
        $builder->resources('Teletransmissions');
        $builder->resources('Themes');
        $builder->resources('Typesacts');
        $builder->resources('Typesittings');
        $builder->resources('Users');
        $builder->resources('Maindocuments');
        $builder->resources('Roles');
        $builder->resources('Files');
        $builder->resources('Wopi');
        $builder->resources('Counters');
        $builder->resources('Sequences');
        $builder->resources('Votes');
        $builder->resources('GenerateTemplates', ['inflect' => 'dasherize']);
        $builder->resources('GenerateTemplateTypes', [
            'only' => ['index'],
            'inflect' => 'dasherize'
        ]);
        $builder->resources('DraftTemplates', ['inflect' => 'dasherize']);
        $builder->resources('DraftTemplateTypes', [
            'only' => ['index'],
            'inflect' => 'dasherize'
        ]);
        $builder->resources('Search');
        $builder->resources('DraftTemplatesTypesacts', ['inflect' => 'dasherize']);
        $builder->resources('Summons');

        $builder->connect('/notifications', ['controller' => 'NotificationsUsers', 'action' => 'index', '_method' => 'GET']);
        $builder->connect('/notifications/{id}/activate', ['controller' => 'NotificationsUsers', 'action' => 'activate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/notifications/{id}/deactivate', ['controller' => 'NotificationsUsers', 'action' => 'deactivate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);

        $builder->resources('Sittings', function (RouteBuilder $builder) {
            $builder->connect('/close', ['controller' => 'Sittings', 'action' => 'close', '_method' => 'PUT']);
        });

        $builder->resources('Services', function (RouteBuilder $builder) {
            $builder->connect('/withChildren', ['controller' => 'Services', 'action' => 'viewWithChildren', '_method' => 'GET']);
        });

        //Themes
        $builder->resources('Themes', function (RouteBuilder $builder) {
            $builder->connect('/withChildren', ['controller' => 'Themes', 'action' => 'viewWithChildren', '_method' => 'GET']);
        });
        $builder->connect('/themes/{id}/activate', ['controller' => 'Themes', 'action' => 'activate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/themes/{id}/deactivate', ['controller' => 'Themes', 'action' => 'deactivate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/themes/active', ['controller' => 'Themes', 'action' => 'active', '_method' => 'GET']);

        $builder->connect('/checkCodeAct/{code}', ['controller' => 'Acts', 'action' => 'checkCode', '_method' => 'GET'], ['pass' => ['code']]);
        $builder->connect('/sittings', ['controller' => 'Sittings', 'action' => 'deleteBatch', '_method' => 'DELETE']);
        $builder->connect('/users/getKcUsers', ['controller' => 'users', 'action' => 'getKcUsers', '_method' => 'get']);
        $builder->connect('/getPermissions/{table}/{id}', ['controller' => 'Roles', 'action' => 'getPermissions', '_method' => 'GET'], ['pass' => ['table', 'id']]);
//    $builder->connect('/getRolesNames', ['controller' => 'Roles', 'action' => 'getRolesNames', '_method' => 'GET']);
        $builder->connect('/users/getAllOrganizationUsers', ['controller' => 'Users', 'action' => 'getAllOrganizationUsers', '_method' => 'GET']);
        $builder->connect('/users/getInfo', ['controller' => 'Users', 'action' => 'getInfo', '_method' => 'GET']);
        $builder->connect('/users/setRole', ['controller' => 'Users', 'action' => 'setRole', '_method' => 'PATCH']);
        $builder->connect('/users/logout', ['controller' => 'Users', 'action' => 'logout', '_method' => 'POST']);
        $builder->connect('/typespiecesjointes/getAssociatedType/{natureId}', ['controller' => 'Typespiecesjointes', 'action' => 'getAssociatedType', '_method' => 'GET'], ['pass' => ['natureId']]);
        $builder->connect('/users/loginStructure', ['controller' => 'Users', 'action' => 'loginStructure', '_method' => 'POST']);
        $builder->connect('/users/{id}/activate', ['controller' => 'Users', 'action' => 'activate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/users/{id}/deactivate', ['controller' => 'Users', 'action' => 'deactivate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);

        $builder->connect('/projects/index_drafts', ['controller' => 'Projects', 'action' => 'index_drafts', '_method' => 'GET']);
        $builder->connect('/projects/index_validating', ['controller' => 'Projects', 'action' => 'index_validating', '_method' => 'GET']);
        $builder->connect('/projects/index_to_validate', ['controller' => 'Projects', 'action' => 'index_to_validate', '_method' => 'GET']);
        $builder->connect('/projects/index_validated', ['controller' => 'Projects', 'action' => 'index_validated', '_method' => 'GET']);
        $builder->connect('/projects/index_to_transmit', ['controller' => 'Projects', 'action' => 'index_to_transmit', '_method' => 'GET']);
        $builder->connect('/projects/index_ready_to_transmit', ['controller' => 'Projects', 'action' => 'index_ready_to_transmit', '_method' => 'GET']);
        $builder->connect('/projects/index_act', ['controller' => 'Projects', 'action' => 'index_act', '_method' => 'GET']);
        $builder->connect('/projects/index_unassociated', ['controller' => 'Projects', 'action' => 'index_unassociated', '_method' => 'GET']);
        $builder->connect('/projects/{id}', ['controller' => 'Projects', 'action' => 'edit'], ['pass' => ['id']])->setMethods(['PATCH', 'PUT']);
        $builder->connect('/projects/{id}/menu', ['controller' => 'Projects', 'action' => 'getMainMenu', '_method' => 'GET'], ['id' => '\d+', 'pass' => ['id']]);

        $builder->connect('/projects/getTdtUrlMultiple', ['controller' => 'Teletransmissions', 'action' => 'getTdtUrlMultiple', '_method' => 'GET']);
        $builder->connect('/projects/cancelTdt', ['controller' => 'Teletransmissions', 'action' => 'cancelTdt', '_method' => 'POST']);
        $builder->connect('/projects/sendtdt', ['controller' => 'Teletransmissions', 'action' => 'sendTdt', '_method' => 'POST']);
        $builder->connect('/projects/getTdtUrl/{id}', ['controller' => 'Teletransmissions', 'action' => 'getTdtUrl', '_method' => 'GET'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/projects/getActeTamponne/{id}', ['controller' => 'Teletransmissions', 'action' => 'getActeTamponne', '_method' => 'GET'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/projects/getBordereau/{id}', ['controller' => 'Teletransmissions', 'action' => 'getBordereau', '_method' => 'GET'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/projects/getArActes/{id}', ['controller' => 'Teletransmissions', 'action' => 'getArActes', '_method' => 'GET'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/projects/teletransmissionOrdered', ['controller' => 'Teletransmissions', 'action' => 'teletransmissionOrdered', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/projects/getAnnexesTamponnee', ['controller' => 'Teletransmissions', 'action' => 'teletransmissionOrdered', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);

        $builder->connect(
            '/structures/withStructures',
            ['controller' => 'Structures', 'action' => 'addStructuresWithOrchestration'],
        )->setPass(['PUT']);

        // Organizations
        $builder->connect(
            '/organizations/get-orchestration-organizations',
            ['controller' => 'Organizations', 'action' => 'getOrchestrationOrganizations'],
        )->setMethods(['GET']);
        $builder->connect(
            '/organizations/{id}/active',
            ['controller' => 'Organizations', 'action' => 'active'],
        )->setMethods(['PUT'])
            ->setPatterns(['id' => '\d+'])
            ->setPass(['id']);

        // Projects
        $builder->connect(
            '/projects/{projectId}/sendToCircuit/{workflowId}',
            [
                'controller' => 'Projects',
                'action' => 'sendToCircuit',
                '_method' => 'POST',
            ],
            [
                'projectId' => '\d+',
                'workflowId' => '\d+',
                'pass' => ['projectId', 'workflowId'],
            ]
        );

        $builder->connect('/projects/signature/soustypes/{id}', ['controller' => 'Signatures', 'action' => 'getIParapheurSousTypes', '_method' => 'GET']);
        $builder->connect('/projects/{id}/manual-signature', ['controller' => 'Signatures', 'action' => 'manualSignature', '_method' => 'PUT'], ['pass' => ['id']]);
        $builder->connect('/projects/signature/electronic-signature/{id}', ['controller' => 'Signatures', 'action' => 'electronicSignature', '_method' => 'PUT'], ['pass' => ['id']]);
        $builder->connect('/projects/signature/status-iparapheur/{id}', ['controller' => 'Signatures', 'action' => 'getStatusIParapheur', '_method' => 'GET'], ['pass' => ['id']]);

        $builder->connect('/projects/signature/signed-document/{id}', ['controller' => 'Signatures', 'action' => 'getSignedDocument', '_method' => 'GET'], ['pass' => ['id']]);
        $builder->connect('/projects/signature/sign-bordereau/{id}', ['controller' => 'Signatures', 'action' => 'getSignBordereau', '_method' => 'GET'], ['pass' => ['id']]);
        $builder->connect('/projects/signature/iparapheur-historique-file/{id}', ['controller' => 'Signatures', 'action' => 'getIparapheurHistoriqueFile', '_method' => 'GET'], ['pass' => ['id']]);

        // Circuits
        $builder->connect('/circuits/', ['controller' => 'Workflows', 'action' => 'index', '_method' => 'GET']);
        $builder->connect('/circuits/{id}', ['controller' => 'Workflows', 'action' => 'view', '_method' => 'GET'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/circuits/', ['controller' => 'Workflows', 'action' => 'add', '_method' => 'POST']);
        $builder->connect('/circuits/{id}', ['controller' => 'Workflows', 'action' => 'edit', '_method' => 'PUT'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/circuits/{id}', ['controller' => 'Workflows', 'action' => 'delete', '_method' => 'DELETE'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/circuits/active', ['controller' => 'Workflows', 'action' => 'active', '_method' => 'GET']);
        $builder->connect('/circuits/{id}/activate', ['controller' => 'Workflows', 'action' => 'activate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/circuits/{id}/deactivate', ['controller' => 'Workflows', 'action' => 'deactivate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);


        // Tasks
        $builder->connect('/tasks/allTasksByCandidateUser', ['controller' => 'WorkflowsTasks', 'action' => 'allTasksByCandidateUser', '_method' => 'GET']);
        $builder->connect('/tasks/approve/{taskId}', ['controller' => 'WorkflowsTasks', 'action' => 'approveAction', '_method' => 'POST'], ['pass' => ['taskId']]);
        $builder->connect('/tasks/reject/{taskId}', ['controller' => 'WorkflowsTasks', 'action' => 'rejectAction', '_method' => 'POST'], ['pass' => ['taskId']]);

        $builder->connect('/projects/approve/{id}', ['controller' => 'WorkflowsTasks', 'action' => 'approveProject', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);

        $builder->connect('/actors/{id}/activate', ['controller' => 'Actors', 'action' => 'activate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/actors/{id}/deactivate', ['controller' => 'Actors', 'action' => 'deactivate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/actor-groups/{id}/activate', ['controller' => 'ActorGroups', 'action' => 'activate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/actor-groups/{id}/deactivate', ['controller' => 'ActorGroups', 'action' => 'deactivate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/typesittings/{id}/activate', ['controller' => 'Typesittings', 'action' => 'activate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/typesittings/{id}/deactivate', ['controller' => 'Typesittings', 'action' => 'deactivate', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/typesacts/{id}/activate', ['controller' => 'Typesacts', 'action' => 'edit', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/typesacts/{id}/deactivate', ['controller' => 'Typesacts', 'action' => 'edit', '_method' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
        $builder->connect('/sequences/availableSequences', ['controller' => 'Sequences', 'action' => 'availableSequences', '_method' => 'GET']);

        $builder->connect(
            '/generate/file-generate-project/{id}',
            ['controller' => 'Generates', 'action' => 'fileGenerateProject'],
        )
            ->setMethods(['GET'])
            ->setPass(['id']);
        $builder->connect(
            '/generate/file-generate-act/{id}',
            ['controller' => 'Generates', 'action' => 'fileGenerateAct'],
        )
            ->setMethods(['GET'])
            ->setPass(['id']);
        $builder->connect(
            '/generate/file-generate-document-summon/{id}',
            ['controller' => 'Generates', 'action' => 'fileGenerateDocumentSummon'],
        )
            ->setMethods(['GET'])
            ->setPass(['id']);
        $builder->connect(
            '/generate/file-generate-deliberations-list/{id}',
            ['controller' => 'Generates', 'action' => 'fileGenerateDeliberationsList'],
        )
            ->setMethods(['GET'])
            ->setPass(['id']);
        $builder->connect(
            '/generate/file-generate-verbal-trial/{id}',
            ['controller' => 'Generates', 'action' => 'fileGenerateVerbalTrial'],
        )
            ->setMethods(['GET'])
            ->setPass(['id']);
        $builder->connect(
            '/generate-templates/with-type-code/{code}',
            ['controller' => 'GenerateTemplates', 'action' => 'indexWithTypeCode'],
        )
            ->setMethods(['GET'])
            ->setPass(['code']);
        $builder->connect(
            '/wopi/files/{name}',
            ['controller' => 'Wopi', 'action' => 'getFileInfo'],
        )
            ->setMethods(['GET'])
            ->setPass(['name'])->setPersist(['access_token']);

        $builder->connect(
            '/wopi/files/{name}/contents',
            ['controller' => 'Wopi', 'action' => 'getFile'],
        )
            ->setMethods(['GET'])
            ->setPass(['name'])->setPersist(['access_token']);

        $builder->connect(
            '/wopi/files/{name}/contents',
            ['controller' => 'Wopi', 'action' => 'putFile'],
        )
            ->setMethods(['POST'])
            ->setPass(['name'])->setPersist(['access_token']);

        $builder->connect(
            '/summons/send/{id}',
            ['controller' => 'Summons', 'action' => 'send'],
        )
            ->setMethods(['GET'])
            ->setPass(['id']);
        $builder->connect(
            '/structure-settings/save-type-boolean',
            ['controller' => 'StructureSettings', 'action' => 'saveTypeBoolean'],
        )->setMethods(['PUT']);

        //Myec3
        //https://soclewl.test.libriciel.fr/sync-myec3/agent
        $builder->connect('/sync-myec3/organism', ['controller' => 'Socles', 'action' => 'addStructure', '_method' => 'POST']);
        $builder->connect('/sync-myec3/organism/{externalId}', ['controller' => 'Socles', 'action' => 'updateStructure', '_method' => 'PUT'], ['pass' => ['externalId']]);
        $builder->connect('/sync-myec3/organism/{externalId}', ['controller' => 'Socles', 'action' => 'deleteStructure', '_method' => 'DELETE'], ['pass' => ['externalId']]);
        $builder->connect('/sync-myec3/agent', ['controller' => 'Socles', 'action' => 'addUser', '_method' => 'POST']);
        $builder->connect('/sync-myec3/agent/{externalId}', ['controller' => 'Socles', 'action' => 'updateUser', '_method' => 'PUT'], ['pass' => ['externalId']]);
        $builder->connect('/sync-myec3/agent/{externalId}', ['controller' => 'Socles', 'action' => 'deleteUser', '_method' => 'DELETE'], ['pass' => ['externalId']]);

        $builder->connect(
            '/sittings/{sittingId}/projects/{projectId}/liste-presence/',
            [
                'controller' => 'ActorsProjectsSittings',
                'action' => 'edit', '_method' => 'PUT'
            ],
            [
                'projectId' => '\d+',
                'sittingId' => '\d+',
                'pass' => ['sittingId', 'projectId']
            ],
        );

        $builder->connect(
            '/cloner-liste-presence/sittings/{sittingId}/vote/{projectId}',
            [
                'controller' => 'ActorsProjectsSittings',
                'action' => 'duplicateListFromPrecedentRank', '_method' => 'GET'
            ],
            ['pass' => ['projectId', 'sittingId']],
        );

        $builder->connect('/sittings/{id}/vote/{projectId}', ['controller' => 'Sittings', 'action' => 'viewWithVotes', '_method' => 'GET'], ['pass' => ['id', 'projectId']]);
        $builder->connect('/votes/project/{projectId}', ['controller' => 'Votes', 'action' => 'getAllProjectsVotes', '_method' => 'GET'], ['pass' => ['projectId']]);

        $builder->connect('/containers-sittings', ['controller' => 'ContainersSittings', 'action' => 'editMany', '_method' => 'PUT']);
        /**
         * Here, we are connecting '/' (base path) to a controller called 'Pages',
         * its action called 'display', and we pass a param to select the view file
         * to use (in this case, src/Template/Pages/home.ctp)...
         */
        $builder->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

        /*
         * Here, we are connecting '/' (base path) to a controller called 'Pages',
         * its action called 'display', and we pass a param to select the view file
         * to use (in this case, templates/Pages/home.php)...
         */
        $builder->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

        /*
         * ...and connect the rest of 'Pages' controller's URLs.
         */
        $builder->connect('/pages/*', 'Pages::display');

        /*
         * Connect catchall routes for all controllers.
         *
         * The `fallbacks` method is a shortcut for
         *
         * ```
         * $builder->connect('/{controller}', ['action' => 'index']);
         * $builder->connect('/{controller}/{action}/*', []);
         * ```
         *
         * You can remove these routes once you've connected the
         * routes you want in your application.
         */
        $builder->fallbacks();
    });

    /*
     * If you need a different set of middleware or none at all,
     * open new scope and define routes there.
     *
     * ```
     * $routes->scope('/api', function (RouteBuilder $builder) {
     *     // No $builder->applyMiddleware() here.
     *
     *     // Parse specified extensions from URLs
     *     // $builder->setExtensions(['json', 'xml']);
     *
     *     // Connect API actions here.
     * });
     * ```
     */
};
