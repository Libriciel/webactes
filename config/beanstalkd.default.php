<?php

use Cake\Core\Configure;

Configure::write([
    'Beanstalk' => [
        /**
         * Connection au serveur Beanstalkd
         */
        'host' => env('WEBACTES_BEANSTALK_SERVICE_HOST','beanstalk'),
        'port' => env('WEBACTES_BEANSTALK_SERVICE_PORT',11300),
        'timeout' => null,
        'persistant' => false,

        /**
         * Chemin des classes de workers
         */
        'workerPaths' => [
            APP . 'Shell' . DS . 'Worker'
        ],

        /**
         * Configurations liées aux tests des workers
         *
         * - timeout: durée maximum en secondes entre l'émission du test et la
         *            consultation du résultat
         */
        'tests' => [
            'timeout' => 10,
        ],

        /**
         * Configuration optionnelle
         */
        'table_jobs' => 'Beanstalk.BeanstalkJobs',
        'table_workers' => 'Beanstalk.BeanstalkWorkers',
        'classname', \Beanstalk\Utility\Beanstalk::class,
        'PheanstalkClassname' => \Pheanstalk\Pheanstalk::class,

        /**
         * Levels of priority
         */
        'Priority' => [
            'VERY_HIGHT' => 0,
            'HIGHT' => 512,
            'NORMAL' => 2048,
            'LOW' => 16384,
        ],
    ],
]);
