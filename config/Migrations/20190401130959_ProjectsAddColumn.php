<?php
use Migrations\AbstractMigration;

class ProjectsAddColumn extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('projects');
        $table->addColumn('key', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true
        ]);

        $table->addIndex(
            [
                'key',
            ],
            ['unique' => true]
        );
    }
}
