<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateStructureSettings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('structure_settings');
        $table->addColumn('structure_id', 'integer', [
            'default' => null,
            'limit' => 10,
            'null' => false,
        ]);
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('value', 'json', [
            'default' => null,
            'limit' => null,
            'null' => true,
        ]);
        $table->addTimestamps('created', 'modified');
        $table->addForeignKey('structure_id', 'structures', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);
        $table->create();
    }
}
