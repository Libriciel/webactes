<?php

declare(strict_types=1);

use App\Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class UpdateVerbalTrialDigestVerbalTrialFull extends AbstractMigration
{
    /**
     * @return void
     */
    public function up()
    {
        $GenerateTemplateTypes = TableRegistry::getTableLocator()->get('GenerateTemplateTypes');
        $GenerateTemplateTypes->updateAll(
            [
                'name' => 'Liste des délibérations',
                'code' => 'deliberations_list',
                'description' => 'Génération d\'une liste des délibérations',
            ],
            ['code' => 'verbal_trial_digest']
        );
        $GenerateTemplateTypes->updateAll(
            [
                'name' => 'Procès-verbal',
                'code' => 'verbal_trial',
                'description' => 'Génération d\'un procès-verbal',
            ],
            ['code' => 'verbal_trial_full']
        );

        // @info: \Phinx\Db\Table::dropForeignKey ne fonctionne pas
        $this->dropConstraint('typesittings', 'typesittings_generate_template_verbal_trial_digest_id_fkey');
        $this->dropConstraint('typesittings', 'typesittings_generate_template_verbal_trial_full_id_fkey');

        $this->table('typesittings')
            ->renameColumn('generate_template_verbal_trial_digest_id', 'generate_template_deliberations_list_id')
            ->renameColumn('generate_template_verbal_trial_full_id', 'generate_template_verbal_trial_id')
            ->addForeignKey('generate_template_deliberations_list_id', 'generate_templates', 'id')
            ->addForeignKey('generate_template_verbal_trial_id', 'generate_templates', 'id')
            ->save();

        $Generationerrors = TableRegistry::getTableLocator()->get('Generationerrors');
        $Generationerrors->updateAll(['type' => 'deliberations-list'], ['type' => 'verbal-trial-digest']);
        $Generationerrors->updateAll(['type' => 'verbal-trial'], ['type' => 'verbal-trial-full']);
    }

    /**
     * @return void
     */
    public function down()
    {
        $GenerateTemplateTypes = TableRegistry::getTableLocator()->get('GenerateTemplateTypes');

        $GenerateTemplateTypes->updateAll(
            [
                'name' => 'PV sommaire',
                'code' => 'verbal_trial_digest',
                'description' => 'Génération d\'un PV sommaire',
            ],
            ['code' => 'deliberations_list']
        );

        $GenerateTemplateTypes->updateAll(
            [
                'name' => 'PV détaillé',
                'code' => 'verbal_trial_full',
                'description' => 'Génération d\'un PV détaillé',
            ],
            ['code' => 'verbal_trial']
        );

        // @info: \Phinx\Db\Table::dropForeignKey ne fonctionne pas
        $this->dropConstraint('typesittings', 'typesittings_generate_template_deliberations_list_id_fkey');
        $this->dropConstraint('typesittings', 'typesittings_generate_template_verbal_trial_id_fkey');

        $this->table('typesittings')
            ->renameColumn('generate_template_deliberations_list_id', 'generate_template_verbal_trial_digest_id')
            ->renameColumn('generate_template_verbal_trial_id', 'generate_template_verbal_trial_full_id')
            ->addForeignKey('generate_template_verbal_trial_digest_id', 'generate_templates', 'id')
            ->addForeignKey('generate_template_verbal_trial_full_id', 'generate_templates', 'id')
            ->save();

        $Generationerrors = TableRegistry::getTableLocator()->get('Generationerrors');
        $Generationerrors->updateAll(['type' => 'verbal-trial-digest'], ['type' => 'deliberations-list']);
        $Generationerrors->updateAll(['type' => 'verbal-trial-full'], ['type' => 'verbal-trial']);
    }
}
