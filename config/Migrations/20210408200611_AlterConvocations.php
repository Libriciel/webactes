<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterConvocations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('convocations');
        $table->changeColumn('date_send', 'timestamp', [
            'null' => true,
        ]);
        $table->changeColumn('pastell_id', 'string', [
            'limit' => 20,
            'null' => true,
        ]);
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']
        );
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->addColumn('summon_id', 'integer', [
            'default' => null,
            'limit' => null,
            'null' => false,
            'after' => 'actor_id',
        ]);
        $table->addForeignKey('summon_id', 'summons', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE',
        ]);

        $table->removeIndex([
            'structure_id',
            'actor_id',
        ]);
        
        $table->update();
    }
}
