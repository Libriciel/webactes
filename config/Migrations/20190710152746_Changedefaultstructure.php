<?php
use Migrations\AbstractMigration;

class Changedefaultstructure extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute("update structures set business_name = 'Web-actes';");
        $this->execute("update structures set id_orchestration = 92;");
        $this->execute("update organizations set name = 'Web-actes';");
    }
}
