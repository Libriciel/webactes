<?php
use Migrations\AbstractMigration;

class Connecteurs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute("
        create table connecteurs (
          id serial primary key not null,
          name varchar(255) not null,
          url varchar (255) not null,
          tdt_url varchar(255) not null,
          username varchar(255) not null,
          password varchar(255) not null,
          structure_id integer not null references structures(id) on delete cascade on update cascade
        )");

    }
}
