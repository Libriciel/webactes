<?php

use Migrations\AbstractMigration;

class AddRankToActors extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('actors');
        $table->addColumn('rank', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'after' => 'active'
        ]);
        $table->update();
    }
}
