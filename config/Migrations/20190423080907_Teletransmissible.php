<?php
use Migrations\AbstractMigration;

class Teletransmissible extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('DELETE FROM typesacts');

        $this->table('typesacts')
            ->addColumn('istdt', 'boolean', [
                'default' => false,
                'null' => false
            ])
            ->update();
    }
}
