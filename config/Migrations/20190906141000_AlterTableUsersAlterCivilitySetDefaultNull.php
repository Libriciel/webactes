<?php
use Migrations\AbstractMigration;

class AlterTableUsersAlterCivilitySetDefaultNull extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('users')
            ->changeColumn('civility', 'string', ['length' => 4, 'default' => null, 'null' => true])
            ->update();
    }
}
