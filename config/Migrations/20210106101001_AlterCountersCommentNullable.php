<?php
use Migrations\AbstractMigration;

class AlterCountersCommentNullable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('counters')
            ->changeColumn('comment', 'string', ['null' => true, 'default' => null, 'limit' => 1500])
            ->update();
    }
}
