<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class DeleteActorsSitting extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->table('actors_sittings')->drop()->save();
    }
}
