<?php
use Migrations\AbstractMigration;

class TypesactsTypesittings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('typesacts_typesittings')
            ->addColumn('typesact_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('typesitting_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'typesact_id',
                ]
            )
            ->addIndex(
                [
                    'typesitting_id',
                ]
            )
            ->addForeignKey(
                'typesitting_id',
                'typesittings',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'typesact_id',
                'typesacts',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->create();
    }
}
