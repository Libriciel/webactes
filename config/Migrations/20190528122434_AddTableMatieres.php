<?php
use Migrations\AbstractMigration;

class AddTableMatieres extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('matieres')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('parent_id', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('codematiere', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('libelle', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('lft', 'integer', [
                'default' => 0
            ])
            ->addColumn('rght', 'integer', [
                'default' => 0
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->addIndex(
                [
                    'organization_id',
                    'structure_id',
                    'codematiere',
                    'libelle',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('matieres')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();
    }
}
