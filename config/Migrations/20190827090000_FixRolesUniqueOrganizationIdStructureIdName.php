<?php

use Migrations\AbstractMigration;

class FixRolesUniqueOrganizationIdStructureIdName extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('DROP INDEX IF EXISTS roles_organization_id_structure_id_name;');
        $this->execute('DROP INDEX IF EXISTS roles_name_organization_id_structure_id;');

        $this->table('roles')
            ->addIndex(
                [
                    'name',
                    'organization_id',
                    'structure_id'
                ],
                [
                    'unique' => true,
                    'name' => 'roles_name_organization_id_structure_id'
                ]
            )
            ->update();
    }
}
