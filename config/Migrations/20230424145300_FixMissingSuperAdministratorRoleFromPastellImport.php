<?php
declare(strict_types=1);

use App\Model\Table\RolesTable;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class FixMissingSuperAdministratorRoleFromPastellImport extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $Structures = TableRegistry::getTableLocator()->get('Structures');
        $sql = $Structures->Roles
            ->find()
            ->select('Roles.id')
            ->where([
                'Roles.name = \'' . RolesTable::SUPER_ADMINISTRATOR . '\'',
                'Roles.structure_id = structures.id'
            ])
            ->sql();

        $structuresWithMissingRole = $Structures
            ->find()
            ->select(['id', 'business_name'])
            ->where([
                "NOT EXISTS ( {$sql} )"
            ])
            ->all();

        $records = [];
        foreach ($structuresWithMissingRole as $structureWithMissingRole) {
            $now = (new FrozenTime())->format('Y-m-d H:i:s.u');
            $records[] = [
                'structure_id' => $structureWithMissingRole->id,
                'name' => RolesTable::SUPER_ADMINISTRATOR,
                'created' => $now,
                'modified' => $now,
                'active' => true,
            ];
        }

        if (empty($records) === false) {
            $this->table('roles')->insert($records)->save();
        }
    }
}
