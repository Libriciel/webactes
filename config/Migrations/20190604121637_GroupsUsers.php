<?php

use Migrations\AbstractMigration;

class GroupsUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('CREATE TABLE groups_users (
                            id       serial,
                            user_id  integer not null references users (id),
                            group_id integer not null references groups (id)
                        );'
        );
        $this->execute('alter table users drop column group_id');
    }
}
