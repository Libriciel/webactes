<?php
use Migrations\AbstractMigration;

class AlterTableClassifications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('classifications')
            ->removeColumn('parent_id')
            ->removeColumn('name')
            ->removeColumn('libelle')
            ->removeColumn('lft')
            ->removeColumn('rght')
            ->save();

        $this->table('classifications')
            ->addColumn('dateclassification', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();
    }
}
