<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterUsersInstancesDropForeignKey extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    /**
     * Migrate Up.
     * @return void
     */
    public function change()
    {
        $table = $this->table('users_instances');
        $table->dropForeignKey('id_flowable_instance')
            ->removeColumn('id_flowable_instance')
            ->addColumn('id_flowable_instance', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ])->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
        ->save();
    }

    /**
     * Migrate Down.
     * @return void
     */
    public function down()
    {

    }
}
