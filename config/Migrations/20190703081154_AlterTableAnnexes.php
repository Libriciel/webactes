<?php
use Migrations\AbstractMigration;

class AlterTableAnnexes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('annexes')
            ->addColumn('codetype', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => true
            ])
            ->update();
    }
}
