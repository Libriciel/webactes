<?php
use Migrations\AbstractMigration;

class AlterTableProject extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('projects')
            ->addColumn('codematiere', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => true,
            ])
            ->update();
    }
}
