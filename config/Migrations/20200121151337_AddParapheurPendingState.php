<?php
use Migrations\AbstractMigration;

class AddParapheurPendingState extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // INSERT TABLE stateacts
        $stateActsInsert = [
            [
                'name' => 'En cours d\'envoi i-parapheur',
                'code' => 'en-cours-envoi-i-parapheur',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y"),
            ],
        ];
        $this->table('stateacts')->insert($stateActsInsert)->save();
    }
}
