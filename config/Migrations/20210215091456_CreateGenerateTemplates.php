<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateGenerateTemplates extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('generate_templates');
        $table->addColumn('structure_id', 'integer', [
            'default' => null,
            'limit' => 10,
            'null' => false,
        ]);

        $table->addColumn('generate_template_type_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addTimestamps('created', 'modified');
        $table->addForeignKey('structure_id', 'structures', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);
        $table->create();
    }
}
