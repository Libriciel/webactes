<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class MinimumData extends AbstractMigration
{
    public function up ()
    {
        // INSERT TABLE stateacts
        $stateActs = [
            [
                'name' => 'Brouillon',
                'code' => 'brouillon',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'En cours de validation',
                'code' => 'en-cours-de-validation',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Validé',
                'code' => 'valide',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Refusé',
                'code' => 'refuse',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'À faire voter',
                'code' => 'a-faire-voter',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Voté',
                'code' => 'vote',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'À faire signer',
                'code' => 'a-faire-signer',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'En cours de signature',
                'code' => 'en-cours-de-signature',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Signé',
                'code' => 'signe',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Refusé par le i-Parapheur',
                'code' => 'refus-parapheur',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'À télétransmettre',
                'code' => 'a-teletransmettre',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Télétransmis',
                'code' => 'teletransmis',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Acquitté',
                'code' => 'acquitte',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Annulé',
                'code' => 'annule',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'En erreur sur le TdT',
                'code' => 'erreur-tdt',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ]
        ];
        $this->table('stateacts')->insert($stateActs)->save();
    }
}
