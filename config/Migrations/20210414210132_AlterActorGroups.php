<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterActorGroups extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('actor_groups');
        $table->changeColumn('active', 'boolean', [
            'default' => false,
            'null' => false,
        ]);
        $table->changeColumn('created', 'timestamp', [
            'default' => 'CURRENT_TIMESTAMP',
            'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();
    }
}
