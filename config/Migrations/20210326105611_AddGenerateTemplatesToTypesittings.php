<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddGenerateTemplatesToTypesittings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('typesittings');
        $table->addColumn('generate_template_convocation_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'after' => 'structure_id'
        ]);
        $table->addColumn('generate_template_schedule_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'after' => 'generate_template_convocation_id'
        ]);
        $table->addColumn('generate_template_verbal_trial_digest_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'after' => 'generate_template_schedule_id'
        ]);
        $table->addColumn('generate_template_verbal_trial_full_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'after' => 'generate_template_verbal_trial_digest_id'
        ]);
        $table->addForeignKey('generate_template_convocation_id', 'generate_templates', 'id', [
            'update' => 'NO_ACTION',
            'delete' => 'NO_ACTION'
        ]);
        $table->addForeignKey('generate_template_schedule_id', 'generate_templates', 'id', [
            'update' => 'NO_ACTION',
            'delete' => 'NO_ACTION'
        ]);
        $table->addForeignKey('generate_template_verbal_trial_digest_id', 'generate_templates', 'id', [
            'update' => 'NO_ACTION',
            'delete' => 'NO_ACTION'
        ]);
        $table->addForeignKey('generate_template_verbal_trial_full_id', 'generate_templates', 'id', [
            'update' => 'NO_ACTION',
            'delete' => 'NO_ACTION'
        ]);
        $table->update();
    }
}
