<?php
use Migrations\AbstractMigration;

class FixRolesNameUnicity extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('roles')
            ->removeIndex(['name'])
            ->update();

        $this->table('roles')
            ->addIndex(['name'])
            ->addIndex(['name', 'organization_id', 'structure_id'], ['unique' => true])
            ->update();
    }
}


