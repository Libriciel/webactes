<?php
use Migrations\AbstractMigration;

class SittingsStatesittings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('sittings_statesittings')
            ->addColumn('sitting_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('statesitting_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'sitting_id',
                ]
            )
            ->addIndex(
                [
                    'statesitting_id',
                ]
            )
            ->create();

        $this->table('sittings_statesittings')
            ->addForeignKey(
                'sitting_id',
                'sittings',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'statesitting_id',
                'statesittings',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();
    }
}
