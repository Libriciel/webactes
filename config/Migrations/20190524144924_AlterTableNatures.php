<?php
use Migrations\AbstractMigration;

class AlterTableNatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // DELETE VALUE natures
        $this->execute("DELETE FROM natures");
        $this->execute("ALTER SEQUENCE natures_id_seq RESTART");

        $this->table('natures')
            ->removeColumn('name')
            ->removeColumn('code')
            ->save();

        $this->table('natures')
            ->addColumn('codenatureacte', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false
            ])
            ->addColumn('libelle', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false
            ])
            ->addColumn('typeabrege', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false
            ])
            ->addIndex(
                [
                    'codenatureacte'
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'libelle'
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'typeabrege'
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'codenatureacte',
                    'libelle',
                    'typeabrege'
                ],
                ['unique' => true]
            )
            ->update();
    }
}
