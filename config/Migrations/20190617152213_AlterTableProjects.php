<?php
use Migrations\AbstractMigration;

class AlterTableProjects extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('projects')
            ->addColumn('ismultichannel', 'boolean', [
                'default' => false,
                'null' => false
            ])
            ->addColumn('signature_date', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();
    }
}
