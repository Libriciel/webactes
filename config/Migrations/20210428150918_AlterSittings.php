<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterSittings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('sittings');
        $table->addColumn('president_id', 'integer', [
            'default' => null,
            'null' => true,
        ])
            ->addForeignKey('president_id', 'actors', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('secretary_id', 'integer', [
            'default' => null,
            'null' => true,
        ])
            ->addForeignKey('secretary_id', 'actors', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->update();
    }
}
