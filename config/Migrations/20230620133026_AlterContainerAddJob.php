<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterContainerAddJob extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('containers');

        $table->addColumn('generation_job_id', 'integer', [
            'default' => null,
            'null' => true,
        ]);

        $table->update();
    }
}
