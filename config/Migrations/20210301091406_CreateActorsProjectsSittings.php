<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateActorsProjectsSittings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('actors_projects_sittings');

        $table->addColumn('actor_id', 'integer', [
            'default' => null,
            'null' => true,
        ])
            ->addForeignKey('actor_id', 'actors', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('mandataire_id', 'integer', [
            'default' => null,
            'null' => true,
        ])
            ->addForeignKey('mandataire_id', 'actors', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('project_id', 'integer', [
            'default' => null,
            'null' => false,
        ])
            ->addForeignKey('project_id', 'projects', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('sitting_id', 'integer', [
            'default' => null,
            'null' => false,
        ])
            ->addForeignKey('sitting_id', 'sittings', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('is_present', 'boolean', [
            'default' => null,
            'null' => false,
        ])
        ->addIndex(
        [
            'actor_id',
            'sitting_id',
            'project_id',
        ],
        ['unique' => true]
    );
        $table->create();
    }
}
