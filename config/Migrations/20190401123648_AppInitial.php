<?php
use Migrations\AbstractMigration;

class AppInitial extends AbstractMigration
{
    public function up()
    {

        $this->table('actors')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('civility', 'string', [
                'default' => null,
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('lastname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('firstname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('birthday', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('title', 'string', [
                'default' => null,
                'limit' => 250,
                'null' => true,
            ])
            ->addColumn('address', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('address_supplement', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('post_code', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('city', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('phone', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('cellphone', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'email',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'structure_id',
                    'email',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('actors_acts')
            ->addColumn('act_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('actor_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'act_id',
                ]
            )
            ->addIndex(
                [
                    'actor_id',
                ]
            )
            ->create();

        $this->table('actors_projects')
            ->addColumn('project_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('actor_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'actor_id',
                ]
            )
            ->addIndex(
                [
                    'project_id',
                ]
            )
            ->create();

        $this->table('actors_sittings')
            ->addColumn('actor_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('sitting_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'actor_id',
                ]
            )
            ->addIndex(
                [
                    'sitting_id',
                ]
            )
            ->create();

        $this->table('acts')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('template_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('code_act', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('date_send', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'code_act',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'structure_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->addIndex(
                [
                    'template_id',
                ]
            )
            ->create();

        $this->table('annexes')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('container_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('rank', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('istransmissible', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'container_id',
                ]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('attachments')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('classifications')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('parent_id', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('libelle', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('lft', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('containers')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('act_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('project_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'act_id',
                ]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'project_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('containers_sittings')
            ->addColumn('container_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('sitting_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'container_id',
                ]
            )
            ->addIndex(
                [
                    'sitting_id',
                ]
            )
            ->create();

        $this->table('containers_stateacts')
            ->addColumn('container_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('stateact_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'container_id',
                ]
            )
            ->addIndex(
                [
                    'stateact_id',
                ]
            )
            ->create();

        $this->table('convocations')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('actor_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('sitting_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('date_send', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('date_open', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('jeton', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('pastell_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'actor_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'actor_id',
                ]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'sitting_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('convocations_attachments')
            ->addColumn('convocation_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('attachment_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'convocation_id',
                    'attachment_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'attachment_id',
                ]
            )
            ->addIndex(
                [
                    'convocation_id',
                ]
            )
            ->create();

        $this->table('documenttypologies')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('code', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('lft', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('dpos')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('civility', 'string', [
                'default' => null,
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('lastname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('firstname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('address', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('address_supplement', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('post_code', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => true,
            ])
            ->addColumn('city', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('phone', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'email',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('fields')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('files')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('maindocument_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('annex_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 512,
                'null' => false,
            ])
            ->addColumn('path', 'string', [
                'default' => null,
                'limit' => 512,
                'null' => false,
            ])
            ->addColumn('mimetype', 'string', [
                'default' => null,
                'limit' => 64,
                'null' => false,
            ])
            ->addColumn('size', 'biginteger', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addIndex(
                [
                    'annex_id',
                ]
            )
            ->addIndex(
                [
                    'maindocument_id',
                ]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('groups')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('role_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('type', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'role_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('histories')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('container_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('comment', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'container_id',
                ]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('maindocuments')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('container_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 512,
                'null' => false,
            ])
            ->addIndex(
                [
                    'container_id',
                ]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('metadatas')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('template_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('tab_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('field_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('number_line', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('number_column', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'field_id',
                ]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->addIndex(
                [
                    'tab_id',
                ]
            )
            ->addIndex(
                [
                    'template_id',
                ]
            )
            ->create();

        $this->table('natures')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('code', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('natures_typologiepieces')
            ->addColumn('nature_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('documenttypology_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'documenttypology_id',
                ]
            )
            ->addIndex(
                [
                    'nature_id',
                ]
            )
            ->create();

        $this->table('officials')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('civility', 'string', [
                'default' => null,
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('lastname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('firstname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('adresse', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('address_supplement', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('post_code', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => true,
            ])
            ->addColumn('city', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('phone', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'email',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('organizations')
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('projects')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('theme_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('template_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->addIndex(
                [
                    'template_id',
                ]
            )
            ->addIndex(
                [
                    'theme_id',
                ]
            )
            ->create();

        $this->table('roles')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

//        $this->table('roles_users')
//            ->addColumn('user_id', 'integer', [
//                'default' => null,
//                'limit' => 10,
//                'null' => false,
//            ])
//            ->addColumn('role_id', 'integer', [
//                'default' => null,
//                'limit' => 10,
//                'null' => false,
//            ])
//            ->addIndex(
//                [
//                    'user_id',
//                ]
//            )
//            ->addIndex(
//                [
//                    'role_id',
//                ]
//            )
//            ->create();

        $this->table('services')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('sittings')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('typesitting_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('date', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('date_convocation', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->addIndex(
                [
                    'typesitting_id',
                ]
            )
            ->create();

        $this->table('stateacts')
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('code', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'code',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'name',
                    'code'
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('structures')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('logo', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('business_name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('siret', 'string', [
                'default' => null,
                'limit' => 14,
                'null' => false,
            ])
            ->addColumn('address', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('addresscomplement', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('post_code', 'string', [
                'default' => null,
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('city', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('phone', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'business_name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'siret',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->create();

        $this->table('structures_services')
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('service_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'service_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'service_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('structures_users')
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'user_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('tabs')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('template_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('position', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('tab', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->addIndex(
                [
                    'template_id',
                ]
            )
            ->create();

        $this->table('tdt_messages')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('tdt_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('tdt_type', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('tdt_etat', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('date_message', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('tdt_data', 'binary', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('templates')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('version', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('themes')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('lft', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('rght', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('typesacts')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('nature_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'nature_id',
                ]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('typesittings')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'structure_id',
                    'name',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->create();

        $this->table('users')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('keycloak_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('active', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('civility', 'string', [
                'default' => null,
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('lastname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('firstname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('username', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('phone', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('group_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('data_security_policy', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'email',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'group_id',
                ]
            )
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->create();

        $this->table('users_services')
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('service_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'user_id',
                    'service_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'service_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('values')
            ->addColumn('organization_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('template_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('json', 'json', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'organization_id',
                ]
            )
            ->addIndex(
                [
                    'structure_id',
                ]
            )
            ->addIndex(
                [
                    'template_id',
                ]
            )
            ->create();

        $this->table('actors')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('actors_acts')
            ->addForeignKey(
                'act_id',
                'acts',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'actor_id',
                'actors',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('actors_projects')
            ->addForeignKey(
                'actor_id',
                'actors',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'project_id',
                'projects',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('actors_sittings')
            ->addForeignKey(
                'actor_id',
                'actors',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'sitting_id',
                'sittings',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('acts')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'template_id',
                'templates',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('annexes')
            ->addForeignKey(
                'container_id',
                'containers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('attachments')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('classifications')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('containers')
            ->addForeignKey(
                'act_id',
                'acts',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'project_id',
                'projects',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('containers_sittings')
            ->addForeignKey(
                'container_id',
                'containers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'sitting_id',
                'sittings',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('containers_stateacts')
            ->addForeignKey(
                'container_id',
                'containers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'stateact_id',
                'stateacts',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('convocations')
            ->addForeignKey(
                'actor_id',
                'actors',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'sitting_id',
                'sittings',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('convocations_attachments')
            ->addForeignKey(
                'attachment_id',
                'attachments',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'convocation_id',
                'convocations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('documenttypologies')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('dpos')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('fields')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('files')
            ->addForeignKey(
                'annex_id',
                'annexes',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'maindocument_id',
                'maindocuments',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('groups')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'role_id',
                'roles',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('histories')
            ->addForeignKey(
                'container_id',
                'containers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('maindocuments')
            ->addForeignKey(
                'container_id',
                'containers',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('metadatas')
            ->addForeignKey(
                'field_id',
                'fields',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'tab_id',
                'tabs',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'template_id',
                'templates',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('natures')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('natures_typologiepieces')
            ->addForeignKey(
                'documenttypology_id',
                'documenttypologies',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'nature_id',
                'natures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('officials')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('projects')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'template_id',
                'templates',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'theme_id',
                'themes',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('roles')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('services')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('sittings')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'typesitting_id',
                'typesittings',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('structures')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('structures_services')
            ->addForeignKey(
                'service_id',
                'services',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('structures_users')
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('tabs')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'template_id',
                'templates',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('tdt_messages')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('templates')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('themes')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('typesacts')
            ->addForeignKey(
                'nature_id',
                'natures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('typesittings')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('users')
            ->addForeignKey(
                'group_id',
                'groups',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'SET_NULL'
                ]
            )
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('users_services')
            ->addForeignKey(
                'service_id',
                'services',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('values')
            ->addForeignKey(
                'organization_id',
                'organizations',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'structure_id',
                'structures',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'template_id',
                'templates',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('actors')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('actors_acts')
            ->dropForeignKey(
                'act_id'
            )
            ->dropForeignKey(
                'actor_id'
            )->save();

        $this->table('actors_projects')
            ->dropForeignKey(
                'actor_id'
            )
            ->dropForeignKey(
                'project_id'
            )->save();

        $this->table('actors_sittings')
            ->dropForeignKey(
                'actor_id'
            )
            ->dropForeignKey(
                'sitting_id'
            )->save();

        $this->table('acts')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )
            ->dropForeignKey(
                'template_id'
            )->save();

        $this->table('annexes')
            ->dropForeignKey(
                'container_id'
            )
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('attachments')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('classifications')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('containers')
            ->dropForeignKey(
                'act_id'
            )
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'project_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('containers_sittings')
            ->dropForeignKey(
                'container_id'
            )
            ->dropForeignKey(
                'sitting_id'
            )->save();

        $this->table('containers_stateacts')
            ->dropForeignKey(
                'container_id'
            )
            ->dropForeignKey(
                'stateact_id'
            )->save();

        $this->table('convocations')
            ->dropForeignKey(
                'actor_id'
            )
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'sitting_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('convocations_attachments')
            ->dropForeignKey(
                'attachment_id'
            )
            ->dropForeignKey(
                'convocation_id'
            )->save();

        $this->table('documenttypologies')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('dpos')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('fields')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('files')
            ->dropForeignKey(
                'annex_id'
            )
            ->dropForeignKey(
                'maindocument_id'
            )
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('groups')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'role_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('histories')
            ->dropForeignKey(
                'container_id'
            )
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('maindocuments')
            ->dropForeignKey(
                'container_id'
            )
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('metadatas')
            ->dropForeignKey(
                'field_id'
            )
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )
            ->dropForeignKey(
                'tab_id'
            )
            ->dropForeignKey(
                'template_id'
            )->save();

        $this->table('natures')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('natures_typologiepieces')
            ->dropForeignKey(
                'documenttypology_id'
            )
            ->dropForeignKey(
                'nature_id'
            )->save();

        $this->table('officials')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('projects')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )
            ->dropForeignKey(
                'template_id'
            )
            ->dropForeignKey(
                'theme_id'
            )->save();

        $this->table('roles')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('services')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('sittings')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )
            ->dropForeignKey(
                'typesitting_id'
            )->save();

        $this->table('structures')
            ->dropForeignKey(
                'organization_id'
            )->save();

        $this->table('structures_services')
            ->dropForeignKey(
                'service_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('structures_users')
            ->dropForeignKey(
                'structure_id'
            )
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('tabs')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )
            ->dropForeignKey(
                'template_id'
            )->save();

        $this->table('tdt_messages')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('templates')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('themes')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('typesacts')
            ->dropForeignKey(
                'nature_id'
            )
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('typesittings')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )->save();

        $this->table('users')
            ->dropForeignKey(
                'group_id'
            )
            ->dropForeignKey(
                'organization_id'
            )->save();

        $this->table('users_services')
            ->dropForeignKey(
                'service_id'
            )
            ->dropForeignKey(
                'user_id'
            )->save();

        $this->table('values')
            ->dropForeignKey(
                'organization_id'
            )
            ->dropForeignKey(
                'structure_id'
            )
            ->dropForeignKey(
                'template_id'
            )->save();

        $this->table('actors')->drop()->save();
        $this->table('actors_acts')->drop()->save();
        $this->table('actors_projects')->drop()->save();
        $this->table('actors_sittings')->drop()->save();
        $this->table('acts')->drop()->save();
        $this->table('annexes')->drop()->save();
        $this->table('attachments')->drop()->save();
        $this->table('classifications')->drop()->save();
        $this->table('containers')->drop()->save();
        $this->table('containers_sittings')->drop()->save();
        $this->table('containers_stateacts')->drop()->save();
        $this->table('convocations')->drop()->save();
        $this->table('convocations_attachments')->drop()->save();
        $this->table('documenttypologies')->drop()->save();
        $this->table('dpos')->drop()->save();
        $this->table('fields')->drop()->save();
        $this->table('files')->drop()->save();
        $this->table('groups')->drop()->save();
        $this->table('histories')->drop()->save();
        $this->table('maindocuments')->drop()->save();
        $this->table('metadatas')->drop()->save();
        $this->table('natures')->drop()->save();
        $this->table('natures_typologiepieces')->drop()->save();
        $this->table('officials')->drop()->save();
        $this->table('organizations')->drop()->save();
        $this->table('projects')->drop()->save();
        $this->table('roles')->drop()->save();
        $this->table('services')->drop()->save();
        $this->table('sittings')->drop()->save();
        $this->table('stateacts')->drop()->save();
        $this->table('structures')->drop()->save();
        $this->table('structures_services')->drop()->save();
        $this->table('structures_users')->drop()->save();
        $this->table('tabs')->drop()->save();
        $this->table('tdt_messages')->drop()->save();
        $this->table('templates')->drop()->save();
        $this->table('themes')->drop()->save();
        $this->table('typesacts')->drop()->save();
        $this->table('typesittings')->drop()->save();
        $this->table('users')->drop()->save();
        $this->table('users_services')->drop()->save();
        $this->table('values')->drop()->save();
    }
}
