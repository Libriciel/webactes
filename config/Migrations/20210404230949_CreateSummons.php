<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateSummons extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('summons');
        $table->addColumn('summon_type_id', 'integer', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('structure_id', 'integer', [
            'default' => null,
            'limit' => 10,
            'null' => false,
        ]);
        $table->addColumn('sitting_id', 'integer', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('date_send', 'date', [
            'default' => null,
            'limit' => null,
            'null' => true,
        ]);
        $table->addColumn('is_sent_individually', 'boolean', [
            'default' => false,
            'limit' => null,
            'null' => false,
        ]);
        $table->addTimestamps('created', 'modified');
        $table->addForeignKey('sitting_id', 'sittings', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);
        $table->addForeignKey('structure_id', 'structures', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);
        $table->create();
    }
}
