<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterConnecteurs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('connecteurs');
        $table->addColumn('connector_type_id', 'integer', [
            'default' => 1,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('connexion_option', 'string', [
            'default' => 1,
            'limit' => null,
            'null' => true,
        ]);
        $table->changeColumn('tdt_url', 'string', [
            'null' => true,
        ]);
        $table->addForeignKey('connector_type_id', 'connector_types', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE',
        ]);
        $table->update();
    }
}
