<?php
use Migrations\AbstractMigration;

class CreateSequences extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('sequences')
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('comment', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => false,
            ])
            ->addColumn('sequence_num', 'integer', [
                'default' => null,
                'null' => false,
            ])
            ->addTimestamps('created', 'modified')
            ->addColumn('starting_date', 'date', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('expiry_date', 'date', [
                'default' => null,
                'null' => true,
            ])
            ->addPrimaryKey('id')
            ->create();
    }
}
