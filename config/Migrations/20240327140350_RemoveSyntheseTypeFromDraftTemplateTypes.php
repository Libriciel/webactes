<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class RemoveSyntheseTypeFromDraftTemplateTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('draft_template_types');
        $this->execute('DELETE FROM draft_template_types WHERE code = \'TXT_SYNT\'');
        $table->update();
    }
}
