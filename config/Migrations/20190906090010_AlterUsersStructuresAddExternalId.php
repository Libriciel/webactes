<?php
use Migrations\AbstractMigration;

class AlterUsersStructuresAddExternalId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('users')
            ->addColumn('id_external', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->update();

        $this->execute("CREATE UNIQUE INDEX users_id_external ON users (id_external) WHERE id_external IS NOT NULL;");

        $this->table('structures')
            ->addColumn('id_external', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->update();

        $this->execute("CREATE UNIQUE INDEX structures_id_external ON structures (id_external) WHERE id_external IS NOT NULL;");
    }
}
