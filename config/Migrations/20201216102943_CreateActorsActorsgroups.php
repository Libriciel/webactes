<?php

use Migrations\AbstractMigration;

class CreateActorsActorsgroups extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('actors_actor_groups');
        $table->addColumn('actor_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('actor_group_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addForeignKey('actor_id', 'actors', 'id', [
            'update' => 'NO_ACTION',
            'delete' => 'NO_ACTION'
        ]);
        $table->addForeignKey('actor_group_id', 'actorsgroups', 'id', [
            'update' => 'NO_ACTION',
            'delete' => 'NO_ACTION'
        ]);
        $table->create();
    }
}
