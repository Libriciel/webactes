<?php
use Migrations\AbstractMigration;

class AlterTableContainers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('containers')
            ->addColumn('typesact_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'typesact_id',
                ]
            )
            ->update();

        $this->table('containers')
            ->addForeignKey(
                'typesact_id',
                'typesacts',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();
    }
}
