<?php
declare(strict_types=1);

use App\Model\Table\RolesTable;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class OptimizePart1AddIndexes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('containers_sittings')->addIndex(['container_id', 'rank'])->update();
        $this->table('containers_stateacts')->addIndex(['container_id', 'created'])->update();
    }
}
