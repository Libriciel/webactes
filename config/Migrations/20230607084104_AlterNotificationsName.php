<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterNotificationsName extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $builder = $this->getQueryBuilder();
        $builder
            ->update('notifications')
            ->set('name', 'Mon projet d\'acte a été validé / refusé lors d\'une étape de circuit.')
            ->where(['name' => 'Mon projet d\'acte a été validé / refusé'])
            ->execute();
    }
}
