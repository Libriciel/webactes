<?php

use Migrations\AbstractMigration;

class AddNotificationsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('notifications')
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('code', 'string', [
                'default' => null,
                'limit' => 25,
                'null' => true,
            ])
            ->addColumn('is_global', 'boolean', [
                'default' => true,
                'null' => false,
            ])
            ->addPrimaryKey('id')
            ->create();

        $acteAttenteValidation = [
            'name' => 'Un projet d\'acte est en attente de validation à mon étape',
            'code' => 'validation_to_do',
            'is_global' => false,
        ];

        $acteValidesDansCircuit = [
            'name' => 'Mon projet d\'acte a été validé / refusé',
            'code' => \App\Model\Table\HistoriesTable::SEND_TO_CIRCUIT,
            'is_global' => false,
        ];

        $acteSigne = [
            'name' => 'Mon acte non télétransmissible a été signé',
            'code' => \App\Service\NotificationService::SIGNED,
            'is_global' => false,
        ];

        $acteAttenteDepotTdt = [
            'name' => 'Mon acte a été signé et est en attente de dépôt sur le TdT',
            'code' => \App\Model\Table\HistoriesTable::TDT_TRANSMISSION,
            'is_global' => false,
        ];

        $notifications = [
            $acteAttenteValidation,
            $acteValidesDansCircuit,
            $acteSigne,
            $acteAttenteDepotTdt,
        ];
        $table = $this->table('notifications');
        $table->insert($notifications);
        $table->saveData();
    }
}
