<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddGenerateTemplatesToTypesacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('typesacts');
        $table->addColumn('generate_template_project_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'after' => 'counter_id'
        ]);
        $table->addColumn('generate_template_act_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'after' => 'generate_template_project_id'
        ]);
        $table->addForeignKey('generate_template_project_id', 'generate_templates', 'id', [
            'update' => 'NO_ACTION',
            'delete' => 'NO_ACTION'
        ]);
        $table->addForeignKey('generate_template_act_id', 'generate_templates', 'id', [
            'update' => 'NO_ACTION',
            'delete' => 'NO_ACTION'
        ]);
        $table->update();
    }
}
