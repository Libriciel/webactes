<?php
use Migrations\AbstractMigration;

class CreateCounters extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('counters')
            ->addColumn('structure_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('comment', 'string', [
                'default' => null,
                'limit' => 1500,
                'null' => false,
            ])
            ->addColumn('counter_def', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('sequence_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('reinit_def', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addTimestamps('created', 'modified')
            ->addPrimaryKey('id')
            ->create();
    }
}
