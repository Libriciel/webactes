<?php
use Migrations\AbstractMigration;

class AddStateacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $stateacts = [
            [
                'name'  => 'en cours dépot tdt',
                'code'  => 'en-cours-depot-tdt',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name'  => 'à ordonner',
                'code'  => 'a-ordonner',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ]
        ];
        $this->table('stateacts')->insert($stateacts)->save();
    }
}
