<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class NewGenerateTemplateTypes extends AbstractMigration
{
    /**
     * Up Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-up-method
     * @return void
     */
    public function up()
    {
        // INSERT TABLE actors
        $data = [
            [
                'name' => 'Projet',
                'code' => 'project',
                'description' => 'Génération d\'un projet d\'acte',
            ],
            [
                'name' => 'Acte',
                'code' => 'act',
                'description' => 'Génération d\'un acte',
            ],
            [
                'name' => 'Convocation',
                'code' => 'convocation',
                'description' => 'Génération d\'une convocation',
            ],
            [
                'name' => 'Ordre du jour',
                'code' => 'schedule',
                'description' => 'Génération d\'un ordre du jour',
            ],
            [
                'name' => 'PV sommaire',
                'code' => 'verbal_trial_digest',
                'description' => 'Génération d\'un PV sommaire',
            ],
            [
                'name' => 'PV détaillé',
                'code' => 'verbal_trial_full',
                'description' => 'Génération d\'un PV détaillé',
            ]
        ];

        $this->table('generate_template_types')->insert($data)->save();
    }

    /**
     * Down Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-down-method
     * @return void
     */
    public function down()
    {
        $this->execute('DELETE FROM generate_template_types');
    }
}
