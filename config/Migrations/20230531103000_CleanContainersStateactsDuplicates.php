<?php
declare(strict_types=1);

use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class CleanContainersStateactsDuplicates extends AbstractMigration
{
    protected function fixTimestamp($row)
    {
        $micro = str_pad((string)$row['id'], 6, '0', STR_PAD_LEFT);
        $value = $row['created']->format('Y-m-d H:i:s') . '.' . $micro;
        return new FrozenTime($value);
    }

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('containers_stateacts')
            ->addColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ]
            )
            ->addIndex(['container_id', 'modified'])
            ->update();

        $ContainersStateacts = TableRegistry::getTableLocator()->get('ContainersStateacts');

        $containers = $ContainersStateacts
            ->find()
            ->select(['container_id'])
            ->where(['modified IS' => null])
            ->distinct('container_id')
            ->orderAsc('container_id')
            ->enableHydration(false)
            ->toArray();

        foreach ($containers as $container) {
            $first = $last = $lastStateactId = null;
            $containersStateacts = $ContainersStateacts
                ->find()
                ->where([
                    'container_id' => $container['container_id'],
                    'modified IS' => null,
                ])
                ->orderAsc('id')
                ->enableHydration(false)
                ->toArray();

            foreach ($containersStateacts as $idx => $containersStateact) {
                if ($first === null) {
                    $first = $containersStateact;
                    $lastStateactId = $containersStateact['stateact_id'];
                }
                if (
                    isset($containersStateacts[$idx + 1]) === false
                    || $containersStateacts[$idx + 1]['stateact_id'] !== $lastStateactId
                ) {
                    $last = $containersStateact;

                    $entity = $ContainersStateacts->get($first['id']);

                    $entity->set('created', $this->fixTimestamp($first));
                    $entity->set('modified', $this->fixTimestamp($last));

                    $ContainersStateacts->save($entity, ['atomic' => false]);
                    $first = $last = $lastStateactId = null;
                }
            }

            $ContainersStateacts->deleteAll([
                'container_id' => $container['container_id'],
                'modified IS' => null,
            ]);
        }

        $this->table('containers_stateacts')
            ->changeColumn(
                'modified',
                'timestamp',
                [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ]
            )
            ->update();
    }
}
