<?php

use Migrations\AbstractMigration;

class FixUniqueIndexNature extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('drop index natures_libelle;');
        $this->execute('drop index natures_typeabrege;');
        $this->execute('drop index natures_codenatureacte;');
        $this->execute('drop index natures_codenatureacte_libelle_typeabrege;');
        $this->execute('create unique index ON natures (libelle, structure_id);');
        $this->execute('create unique index ON natures (typeabrege, structure_id);');
        $this->execute('create unique index ON natures (codenatureacte, structure_id);');
        $this->execute("SELECT setval('classifications_id_seq', COALESCE((SELECT MAX(id)+1 FROM classifications), 1), false);");
        $this->execute("SELECT setval('matieres_id_seq', COALESCE((SELECT MAX(id)+1 FROM matieres), 1), false);");
        $this->execute("SELECT setval('natures_id_seq', COALESCE((SELECT MAX(id)+1 FROM natures), 1), false);");
    }

}
