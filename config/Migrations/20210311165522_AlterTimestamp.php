<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterTimestamp extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */

    public function up()
    {
        $table = $this->table('typesacts');
        $table->changeColumn('created', 'timestamp', [
            'default' => 'CURRENT_TIMESTAMP',
            'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
            ]);

        $table->update();

        $table = $this->table('natures');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('officials');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('dpos');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('themes');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('matieres');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('classifications');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('matieres');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('typesittings');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('stateacts');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('typespiecesjointes');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('sittings');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('statesittings');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('stateacts');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table = $this->table('pastellfluxtypes');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('organizations');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('structures');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('officials');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('users');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('groups');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->exists() ?? $table->update();

        $table = $this->table('roles');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('organizations');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('structures');
        $table->changeColumn('created', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();

        $table = $this->table('users');
        $table->changeColumn('created', 'timestamp', [
            'default' => 'CURRENT_TIMESTAMP',
            'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();
    }
}
