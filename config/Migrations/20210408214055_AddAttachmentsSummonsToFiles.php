<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddAttachmentsSummonsToFiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('files');
        $table->addColumn('attachment_summon_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'after' => 'project_text_id'
        ]);
        $table->addForeignKey('attachment_summon_id', 'attachments_summons', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);
        $table->update();
    }
}
