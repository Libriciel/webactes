<?php

use Migrations\AbstractMigration;

class AlterTablesSetMissingForeignKeys extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('pastellfluxactions')
            ->addColumn('comments', 'string', [
                'default' => null,
                'limit' => 500,
                'null' => false,
            ])
            ->update();

        $this->execute("UPDATE services SET parent_id = NULL WHERE parent_id = 0;");

        $this->table('services')
            ->changeColumn('parent_id', 'integer', ['default' => null, 'null' => true])
            ->update();

        $this->table('services')
            ->addForeignKey(
                'parent_id',
                'services',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('users')
            ->addIndex(
                [
                    'keycloak_id',
                ],
                ['unique' => true]
            )
            ->update();

        $this->execute("UPDATE themes SET parent_id = NULL WHERE parent_id = 0;");

        $this->table('themes')
            ->changeColumn('parent_id', 'integer', ['default' => null, 'null' => true])
            ->update();

        $this->table('themes')
            ->addForeignKey(
                'parent_id',
                'themes',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();
    }
}
