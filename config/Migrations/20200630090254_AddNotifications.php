<?php

use Migrations\AbstractMigration;

class AddNotifications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $notifications = [
            'name' => 'Mon projet d\'acte a été validé et est en attente de signature',
            'code' => \App\Model\Table\HistoriesTable::VALIDATION,
            'is_global' => 'false',
        ];

        $builder = $this->getQueryBuilder();
        $st = $builder
            ->insert(['name', 'code', 'is_global'])
            ->into('notifications')
            ->values($notifications)
            ->execute();

        $notificationId = $st->lastInsertId('notifications', 'id');


        foreach ($notifications as $notification) {
            $notificationsQuery = $this->getQueryBuilder();
            $notificationsQuery
                ->select(['id', $notificationId, 'false'])
                ->from('users');
            $builder = $this->getQueryBuilder();
            $builder
                ->insert(['user_id', 'notification_id', 'active'])
                ->into('notifications_users')
                ->values($notificationsQuery)
                ->execute();
        }
    }
}
