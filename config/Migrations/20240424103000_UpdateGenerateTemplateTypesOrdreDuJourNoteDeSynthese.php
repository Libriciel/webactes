<?php

declare(strict_types=1);

use App\Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class UpdateGenerateTemplateTypesOrdreDuJourNoteDeSynthese extends AbstractMigration
{
    /**
     * @return void
     */
    public function up()
    {
        $GenerateTemplateTypes = TableRegistry::getTableLocator()->get('GenerateTemplateTypes');
        $GenerateTemplateTypes->updateAll(
            [
                'name' => 'Note de synthèse',
                'code' => 'executive_summary',
                'description' => 'Génération d\'une note de synthèse',
            ],
            ['code' => 'schedule']
        );

        // @info: \Phinx\Db\Table::dropForeignKey ne fonctionne pas
        $this->dropConstraint('typesittings', 'typesittings_generate_template_schedule_id_fkey');

        $this->table('typesittings')
            ->renameColumn('generate_template_schedule_id', 'generate_template_executive_summary_id')
            ->addForeignKey('generate_template_executive_summary_id', 'generate_templates', 'id')
            ->save();

        $Generationerrors = TableRegistry::getTableLocator()->get('Generationerrors');
        $Generationerrors->updateAll(['type' => 'executive-summary'], ['type' => 'schedule']);
    }

    /**
     * @return void
     */
    public function down()
    {
        $GenerateTemplateTypes = TableRegistry::getTableLocator()->get('GenerateTemplateTypes');

        $GenerateTemplateTypes->updateAll(
            [
                'name' => 'Ordre du jour',
                'code' => 'schedule',
                'description' => 'Génération d\'un ordre du jour',
            ],
            ['code' => 'executive_summary']
        );

        // @info: \Phinx\Db\Table::dropForeignKey ne fonctionne pas
        $this->dropConstraint('typesittings', 'typesittings_generate_template_executive_summary_id_fkey');

        $this->table('typesittings')
            ->renameColumn('generate_template_executive_summary_id', 'generate_template_schedule_id')
            ->addForeignKey('generate_template_schedule_id', 'generate_templates', 'id')
            ->save();

        $Generationerrors = TableRegistry::getTableLocator()->get('Generationerrors');
        $Generationerrors->updateAll(['type' => 'schedule'], ['type' => 'executive-summary']);
    }
}
