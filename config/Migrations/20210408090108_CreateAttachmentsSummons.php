<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateAttachmentsSummons extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('attachments_summons');
        $table->addColumn('attachment_id', 'integer', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('summon_id', 'integer', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('rank', 'integer', [
        'default' => null,
        'limit' => 10,
        'null' => false,
        ]);
        $table->addForeignKey('attachment_id', 'attachments', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE',
        ]);
        $table->addForeignKey('summon_id', 'summons', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE',
        ]);
        $table->create();
    }
}
