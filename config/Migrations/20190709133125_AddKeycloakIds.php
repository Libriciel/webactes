<?php
use Migrations\AbstractMigration;

class AddKeycloakIds extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute("update users set keycloak_id = '4882bade-72bb-40d8-b896-36a6dd03bbd9' where username = 's.blanc';");
        $this->execute("update users set keycloak_id = '18c45a75-2852-4637-8e78-6ff3ed949164' where username = 'm.vert';");
        $this->execute("update users set keycloak_id = '684dd96c-cdfd-4304-8869-b1567f9c5286' where username = 'j.orange';");
        $this->execute("update users set keycloak_id = 'c8832f5d-4f54-4e95-9714-8715b734c514' where username = 'r.violet';");
    }
}
