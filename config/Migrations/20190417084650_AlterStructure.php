<?php
use Migrations\AbstractMigration;

class AlterStructure extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('structures')
            ->removeColumn('address')
            ->removeColumn('addresscomplement')
            ->removeColumn('post_code')
            ->removeColumn('city')
            ->removeColumn('phone')
            ->save();
    }
}
