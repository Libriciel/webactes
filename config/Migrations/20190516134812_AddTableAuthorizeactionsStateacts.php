<?php

use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class AddTableAuthorizeactionsStateacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('authorizeactions_stateacts')
            ->addColumn('authorizeaction_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('stateact_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'authorizeaction_id',
                ]
            )
            ->addIndex(
                [
                    'stateact_id',
                ]
            )
            ->create();

        $this->table('authorizeactions_stateacts')
            ->addForeignKey(
                'authorizeaction_id',
                'authorizeactions',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'stateact_id',
                'stateacts',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        // DELETE VALUE stateacts
        $this->execute("DELETE FROM stateacts");
        $this->execute("ALTER SEQUENCE stateacts_id_seq RESTART");

        // INSERT TABLE stateacts
        $stateActsInsert = [
            [
                'name' => 'Brouillon',
                'code' => 'brouillon',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'En cours de validation',
                'code' => 'en-cours-de-validation',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Validé',
                'code' => 'valide',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Refusé',
                'code' => 'refuse',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Déclaré signé',
                'code' => 'declare-signe',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Signé par le i-Parapheur',
                'code' => 'signe-i-parapheur',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Refusé par le i-Parapheur',
                'code' => 'refuse-i-parapheur',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'En cours de signature i-Parapheur',
                'code' => 'en-cours-signature-i-parapheur',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'À télétransmettre',
                'code' => 'a-teletransmettre',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'En cours de télétransmission',
                'code' => 'en-cours-teletransmission',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Acquitté',
                'code' => 'acquitte',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'Annulé',
                'code' => 'annule',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'name' => 'En erreur sur le TdT',
                'code' => 'erreur-tdt',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ]
        ];
        $this->table('stateacts')->insert($stateActsInsert)->save();

        $Stateacts = TableRegistry::getTableLocator()->get('Stateacts');
        $Authorizeactions = TableRegistry::getTableLocator()->get('Authorizeactions');

        // INSERT TABLE authorizeactions
        $authorizeactions_stateacts = [
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'add'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'brouillon'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'add'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'valide'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'edit'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'brouillon'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'edit'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'valide'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'connector'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'signe-i-parapheur'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'connector'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'refuse-i-parapheur'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'connector'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'en-cours-signature-i-parapheur'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'connector'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'a-teletransmettre'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'connector'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'en-cours-teletransmission'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'connector'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'acquitte'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'connector'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'annule'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'connector'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'erreur-tdt'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'en-cours-de-validation'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'valide'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'refuse'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'declare-signe'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'signe-i-parapheur'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'refuse-i-parapheur'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'en-cours-signature-i-parapheur'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'a-teletransmettre'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'en-cours-teletransmission'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'acquitte'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'annule'])
                    ->firstOrFail()
                    ->get('id')
            ],
            [
                'authorizeaction_id' => $Authorizeactions->find()
                    ->select('id')
                    ->where(['action' => 'wa'])
                    ->firstOrFail()
                    ->get('id'),
                'stateact_id' => $Stateacts->find()
                    ->select('id')
                    ->where(['code' => 'erreur-tdt'])
                    ->firstOrFail()
                    ->get('id')
            ],
        ];
        $this->table('authorizeactions_stateacts')->insert($authorizeactions_stateacts)->save();
    }
}
