<?php
use Migrations\AbstractMigration;

class DropAcosArosArosAcos extends AbstractMigration
{
    public function up()
    {
        $this->table('aros_acos')->drop()->save();
        $this->table('acos')->drop()->save();
        $this->table('aros')->drop()->save();
    }
}
