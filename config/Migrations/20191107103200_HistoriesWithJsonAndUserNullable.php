<?php

use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class HistoriesWithJsonAndUserNullable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('histories')
            ->addColumn('json', 'json', ['null' => true])
            ->changeColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->update();
    }
}
