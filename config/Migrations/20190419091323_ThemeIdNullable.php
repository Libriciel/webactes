<?php
use Migrations\AbstractMigration;

class ThemeIdNullable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('projects')
            ->changeColumn('theme_id', 'integer', ['null' => true, 'default' => null, 'limit' => 10])
            ->update();
    }
}
