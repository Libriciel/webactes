<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddDraftTemplatesToFiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('files');
        $table->addColumn('draft_template_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'after' => 'generate_template_id'
        ]);
        $table->addForeignKey('draft_template_id', 'draft_templates', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);
        $table->update();
    }
}
