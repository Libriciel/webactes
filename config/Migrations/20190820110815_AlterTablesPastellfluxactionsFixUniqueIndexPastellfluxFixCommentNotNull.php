<?php

use Migrations\AbstractMigration;

class AlterTablesPastellfluxactionsFixUniqueIndexPastellfluxFixCommentNotNull extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('pastellfluxactions')
            ->removeIndex(['organization_id', 'structure_id', 'pastellflux_id'])
            ->save();

        $this->table('pastellfluxs')
            ->changeColumn('comments', 'string', ['length' => 500, 'null' => true])
            ->update();
    }
}
