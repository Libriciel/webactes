<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterTimestamp2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pastellfluxactions');
        $table->changeColumn('created', 'timestamp', [
            'default' => 'CURRENT_TIMESTAMP',
            'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();
        
        $table = $this->table('histories');
        $table->changeColumn('created', 'timestamp', [
            'default' => 'CURRENT_TIMESTAMP',
            'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();
    }
}
