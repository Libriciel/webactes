<?php
use Migrations\AbstractMigration;

class AddCounterToTypesActs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('typesacts')
            ->addColumn('counter_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
                'after' => 'nature_id'
            ])
            ->addIndex(
                [
                    'counter_id',
                ]
            )
            ->update();
    }
}
