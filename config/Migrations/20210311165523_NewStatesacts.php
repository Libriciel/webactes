<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class NewStatesacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // INSERT TABLE stateacts
        $stateActsInsert = [
            [
                'name' => 'Voté et approuvé',
                'code' => 'vote-et-approuve',
            ],
            [
                'name' => 'Voté et rejeté',
                'code' => 'vote-et-rejete',
            ],
            [
                'name' => 'Prendre acte',
                'code' => 'prendre-acte',
            ],
        ];
        $this->table('stateacts')->insert($stateActsInsert)->save();
    }
}
