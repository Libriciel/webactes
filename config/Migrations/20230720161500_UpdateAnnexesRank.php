<?php

declare(strict_types=1);

use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class UpdateAnnexesRank extends AbstractMigration
{
    /**
     * Mise à jour du rank des annexes
     * @return void
     */
    public function change()
    {
        $Annexes = TableRegistry::getTableLocator()->get('Annexes');
        $Containers = TableRegistry::getTableLocator()->get('Containers');

        $containers = $Containers
            ->find()
            ->select(['id'])
            ->orderAsc('id')
            ->enableHydration(false)
            ->toArray();
        foreach ($containers as $container) {
            $annexes = $Annexes
                ->find()
                ->select(['id'])
                ->where(['container_id' => $container['id']])
                ->orderAsc('id')
                ->enableHydration(false)
                ->toArray();
            foreach ($annexes as $index => $annexe) {
                $Annexes->updateAll(['rank' => $index + 1], ['id' => $annexe['id']]);
            }
        }
    }
}
