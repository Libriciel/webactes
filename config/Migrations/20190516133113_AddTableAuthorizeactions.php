<?php
use Migrations\AbstractMigration;

class AddTableAuthorizeactions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('authorizeactions')
            ->addColumn('action', 'string', [
                'default' => null,
                'limit' => 9,
                'null' => false,
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'action',
                ],
                ['unique' => true]
            )
            ->create();

        // INSERT TABLE authorizeactions
        $authorizeactions = [
            [
                'action' => "add",
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'action' => "edit",
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'action' => "connector",
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'action' => "wa",
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ]
        ];
        $this->table('authorizeactions')->insert($authorizeactions)->save();
    }
}
