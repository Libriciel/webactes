<?php
use Migrations\AbstractMigration;

class AnnulationEnCours extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $stateAct = [
                'name' => 'Annulation en cours',
                'code' => 'annulation-en-cours',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ];

        $this->table('stateacts')->insert($stateAct)->save();
    }
}
