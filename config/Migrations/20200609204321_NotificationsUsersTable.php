<?php

use Migrations\AbstractMigration;

class NotificationsUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('CREATE TABLE  notifications_users (
                            id       serial PRIMARY KEY NOT NULL,
                            user_id  integer not null references users (id),
                            notification_id integer not null references notifications (id),
                            active boolean default false
                        );');
    }
}
