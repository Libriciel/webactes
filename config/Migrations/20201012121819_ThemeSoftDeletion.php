<?php
use Migrations\AbstractMigration;

class ThemeSoftDeletion extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('themes');
        $table->addColumn('deleted', 'timestamp', [
            'default' => null,
            'limit' => null,
            'null' => true
        ]);
        $table->update();
    }
}
