<?php
use Migrations\AbstractMigration;

class Statesittings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('statesittings')
            ->addColumn('state', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('code', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false
            ])
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'state',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'code',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'state',
                    'code',
                ],
                ['unique' => true]
            )
            ->create();

        // INSERT TABLE organizations
        $statesittings = [
            [
                'state'  => 'Ouverture de la séance',
                'code'  => 'ouverture_seance',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
            [
                'state'  => 'Séance close',
                'code'  => 'seance_close',
                'created' => date("d/m/Y"),
                'modified' => date("d/m/Y")
            ],
        ];
        $this->table('statesittings')->insert($statesittings)->save();
    }
}
