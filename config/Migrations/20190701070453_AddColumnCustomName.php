<?php
use Migrations\AbstractMigration;

class AddColumnCustomName extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute("alter table structures add customname varchar(255);");
    }
}
