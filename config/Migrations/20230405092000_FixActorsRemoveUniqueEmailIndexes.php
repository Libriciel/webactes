<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class FixActorsRemoveUniqueEmailIndexes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // Correction: suppression des indexes uniques sur l'email des acteurs
        $this->table('actors')
            ->removeIndexByName('actors_email')
            ->removeIndexByName('actors_structure_id_email')
            ->addIndex(['email'])
            ->update();
    }
}
