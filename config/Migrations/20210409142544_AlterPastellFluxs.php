<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterPastellFluxs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pastellfluxs');
        $table->addColumn('summon_id', 'integer', [
            'default' => null,
            'limit' => null,
            'null' => true,
            'after' => 'container_id'
        ]);
        $table->addForeignKey('summon_id', 'summons', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);
        $table->update();
    }
}
