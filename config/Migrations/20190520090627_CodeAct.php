<?php
use Migrations\AbstractMigration;

class CodeAct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('projects')
            ->addColumn('code_act', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true
            ])
            ->update();
    }
}
