<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterActors extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('actors');
        $table->changeColumn('address', 'string', [
            'null' => true,
        ]);
        $table->changeColumn('post_code', 'string', [
            'null' => true,
        ]);
        $table->changeColumn('city', 'string', [
            'null' => true,
        ]);
        $table->changeColumn('phone', 'string', [
            'null' => true,
        ]);
        $table->changeColumn('civility', 'string', [
            'limit' => 50,
            'default' => false,
            'null' => false,
        ]);
        $table->changeColumn('active', 'boolean', [
            'default' => false,
            'null' => false,
        ]);
        $table->changeColumn('created', 'timestamp', [
            'default' => 'CURRENT_TIMESTAMP',
            'update' => '']);
        $table->changeColumn('modified', 'timestamp', [
            'null' => true,
            'default' => null,
            'update' => 'CURRENT_TIMESTAMP',
        ]);
        $table->update();
    }
}
