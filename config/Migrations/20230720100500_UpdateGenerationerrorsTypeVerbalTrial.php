<?php
use Migrations\AbstractMigration;

class UpdateGenerationerrorsTypeVerbalTrial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute("UPDATE generationerrors SET type = 'verbal_trial_digest' WHERE type = 'verbal-trial-digest';");
        $this->execute("UPDATE generationerrors SET type = 'verbal_trial_full' WHERE type = 'verbal-trial-full';");
    }
}
