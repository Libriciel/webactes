<?php
use Migrations\AbstractMigration;

class FixAfterRemoveUnnecessaryOrganizationId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('sittings')
            ->addIndex(
                [
                    'structure_id',
                    'typesitting_id',
                    'date'
                ],
                ['unique' => true]
            )
            ->update();
    }
}
