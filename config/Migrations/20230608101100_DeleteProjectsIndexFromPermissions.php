<?php
declare(strict_types=1);

use Cake\Database\Exception\DatabaseException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Migrations\AbstractMigration;

class DeleteProjectsIndexFromPermissions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        try {
            $Acos = TableRegistry::getTableLocator()->get('Acos');

            $aco = $Acos
                ->find()
                ->select(['id'])
                ->innerJoin(
                    ['Parents' => 'acos'],
                    ['Parents.id = Acos.parent_id'],
                )
                ->innerJoin(
                    ['Roots' => 'acos'],
                    ['Roots.id = Parents.parent_id'],
                )
                ->where([
                    'Roots.alias' => 'controllers',
                    'Parents.alias' => 'Projects',
                    'Acos.alias' => 'index',
                ])
                ->first();

            if (!empty($aco)) {
                $Permissions = TableRegistry::getTableLocator()->get('Permissions', ['table' => 'aros_acos']);
                $permissions = $Permissions
                    ->find()
                    ->select(['id'])
                    ->where(['Permissions.aco_id' => $aco['id']])
                    ->enableHydration(false)
                    ->toArray();

                if (!empty($permissions)) {
                    $Permissions->deleteAll(['id IN' => Hash::extract($permissions, '{n}.id')]);
                }

                $Acos->deleteAll(['id' => $aco['id']]);
            }
        } catch (DatabaseException $exc) {
            if ($exc->getMessage() !== 'Cannot describe acos. It has 0 columns.') {
                throw $exc;
            }
        }
    }
}
