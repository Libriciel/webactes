<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class FixRolesStructureIdWithStructuresUsersStructureId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // Correction: correction de la valeur de roles_users.structure_id incorrecte vis-à-vis de structures_users.structure_id
        $rows = $this->query(
            'SELECT
                        roles_users.id,
                        users.id AS user_id,
                        new_roles.id AS role_id
                    FROM users
                    INNER JOIN roles_users ON (roles_users.user_id = users.id)
                    INNER JOIN roles ON (roles_users.role_id = roles.id)
                    INNER JOIN structures_users ON (structures_users.user_id = users.id)
                    INNER JOIN roles AS new_roles ON (
                        roles.name = new_roles.name
                        AND new_roles.structure_id = structures_users.structure_id
                    )
                    WHERE
                        roles.structure_id <> structures_users.structure_id;'
        );
        foreach ($rows as $row) {
            $this->execute("UPDATE roles_users SET role_id = {$row['role_id']} WHERE id = {$row['id']};");
        }
    }
}
