<?php
use Migrations\AbstractMigration;

class AddCurrentMainDocumentAnnexes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute("alter table maindocuments add current boolean default true;");
        $this->execute("alter table annexes add current boolean default true;");


    }
}
