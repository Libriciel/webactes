<?php

use Migrations\AbstractMigration;

class FixDatabaseConstraints extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('DROP INDEX natures_codenatureacte_structure_id_idx;');
        $this->execute('DROP INDEX natures_typeabrege_structure_id_idx;');
        $this->execute('DROP INDEX natures_libelle_structure_id_idx;');

        $this->execute('ALTER TABLE matieres ALTER COLUMN parent_id TYPE INTEGER USING (parent_id::integer);');

        $this->table('matieres')
            ->addForeignKey(
                'parent_id',
                'matieres',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('natures')
            ->addIndex(
                [
                    'structure_id',
                    'typeabrege'
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'structure_id',
                    'libelle'
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'structure_id',
                    'codenatureacte'
                ],
                ['unique' => true]
            )
            ->update();

        $this->table('roles')
            ->addIndex(
                [
                    'organization_id',
                    'structure_id',
                    'name'
                ],
                ['unique' => true]
            )
            ->update();

        $this->table('sittings')
            ->addIndex(
                [
                    'organization_id',
                    'structure_id',
                    'typesitting_id',
                    'date'
                ],
                ['unique' => true]
            )
            ->update();
    }
}
