<?php
use Migrations\AbstractMigration;

class AlterTypesactsIsdefault extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute("UPDATE typesacts SET isdefault = false;");
        $this->execute("UPDATE typesacts SET isdefault = true WHERE name = 'Délibération';");

        $this->table('typesacts')
            ->changeColumn('isdefault', 'boolean', ['default' => false, 'null' => false])
            ->update();
    }
}
