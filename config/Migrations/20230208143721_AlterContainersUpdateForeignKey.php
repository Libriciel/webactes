<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterContainersUpdateForeignKey extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('containers')
            ->dropForeignKey('typesact_id')
            ->addForeignKey(
                'typesact_id',
                'typesacts',
                'id',
                [
                    'update' => 'NO ACTION',
                    'delete' => 'NO ACTION'
                ]
            )
            ->update();
    }
}
