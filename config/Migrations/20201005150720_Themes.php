<?php
use Migrations\AbstractMigration;

class Themes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('themes');
        $table->addColumn('position', 'string', [
            'default' => null,
            'limit' => 60,
            'null' => true
        ]);
        $table->update();
    }
}
