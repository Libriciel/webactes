<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class NewSummonTypes extends AbstractMigration
{
    /**
     * Up Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-up-method
     * @return void
     */
    public function up()
    {
        // INSERT TABLE summon
        $data = [
            [
                'name' => 'PASTELL',
                'code' => 'pastell',
                'description' => 'Envoyer la convocation des personnes convoqués à une séance par mail sécurisé',
            ],
            [
                'name' => 'IdélibRE',
                'code' => 'idelibre',
                'description' => 'Envoyer la convocation des personnes convoqués à une séance par IdélibRE',
            ],
        ];

        $this->table('summon_types')->insert($data)->save();
    }

    /**
     * Down Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-down-method
     * @return void
     */
    public function down()
    {
        $this->table('summon_types')->drop()->save();
    }
}
