<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class NewDraftTemplateTypes extends AbstractMigration
{
    /**
     * Up Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-up-method
     * @return void
     */
    public function up()
    {
        // INSERT TABLE actors
        $data = [
            [
                'name' => 'Projet',
                'code' => 'TXT_PROJ',
                'description' => 'Gabarit de rédaction d\'un texte de projet',
            ],
            [
                'name' => 'Acte',
                'code' => 'TXT_ACT',
                'description' => 'Gabarit de rédaction d\'un texte d\'acte',
            ],
            [
                'name' => 'Synthèse',
                'code' => 'TXT_SYNT',
                'description' => 'Gabarit de rédaction d\'un texte de synthèse d\'acte',
            ],
        ];

        $this->table('draft_template_types')->insert($data)->save();
    }

    /**
     * Down Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-down-method
     * @return void
     */
    public function down()
    {
        $this->execute('DELETE FROM draft_template_types');
    }
}
