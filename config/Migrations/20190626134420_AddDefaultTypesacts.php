<?php
use Migrations\AbstractMigration;

class AddDefaultTypesacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('alter table typesacts add isdefault boolean;');
        $this->execute('update typesacts set isdefault = false ;');
        $this->execute("update typesacts set isdefault = true where name ilike 'Délibération' ;");
    }
}
