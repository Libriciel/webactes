<?php
use Migrations\AbstractMigration;

class Removeclassification extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('delete from matieres');
        $this->execute('delete from classifications');
    }
}
