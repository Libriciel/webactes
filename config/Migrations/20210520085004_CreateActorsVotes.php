<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateActorsVotes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('actors_votes');

        $table->addColumn('actor_id', 'integer', [
            'default' => null,
            'null' => true,
        ])
        ->addForeignKey('actor_id', 'actors', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('vote_id', 'integer', [
            'default' => null,
            'null' => false,
        ])
        ->addForeignKey('vote_id', 'votes', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('result', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true, //'yes', 'no', 'abstention', 'no_words'
        ]);

        $table->create();
    }
}
