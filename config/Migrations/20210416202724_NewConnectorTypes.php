<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class NewConnectorTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        // INSERT TABLE summon
        $data = [
            [
                'name' => 'PASTELL',
                'code' => 'pastell',
                'description' => 'Pastell',
            ],
            [
                'name' => 'IdélibRE',
                'code' => 'idelibre',
                'description' => 'IdélibRE',
            ],
        ];

        $this->table('connector_types')->insert($data)->save();
    }
}
