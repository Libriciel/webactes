<?php
use Migrations\AbstractMigration;

class DropTableDocumenttypologies extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('natures_typologiepieces')->drop()->save();
        $this->table('documenttypologies')->drop()->save();
    }
}
