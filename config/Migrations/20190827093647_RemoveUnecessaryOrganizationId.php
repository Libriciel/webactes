<?php
use Migrations\AbstractMigration;

class RemoveUnecessaryOrganizationId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('actors')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('acts')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('annexes')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('attachments')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('classifications')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('containers')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('convocations')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('dpos')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('fields')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('files')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('histories')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('maindocuments')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('matieres')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->addIndex(
                [
                    'structure_id',
                    'codematiere',
                    'libelle',
                ],
                ['unique' => true]
            )
            ->update();

        $this->table('metadatas')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('natures')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('officials')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('projects')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('roles')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->addIndex(
                [
                    'name',
                    'structure_id',
                ],
                ['unique' => true]
            )
            ->update();

        $this->table('services')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('sittings')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('systemlogs')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('tabs')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('tdt_messages')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('templates')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('themes')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('typesacts')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('typesittings')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();

        $this->table('typespiecesjointes')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->addIndex(
                [
                    'structure_id',
                    'nature_id',
                    'codetype',
                    'libelle',
                ],
                ['unique' => true]
            )
            ->update();

        $this->table('values')
            ->dropForeignKey('organization_id')
            ->removeColumn('organization_id')
            ->update();
    }
}
