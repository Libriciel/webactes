<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateActorGroupsTypesittings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('actor_groups_typesittings');

        $table->addColumn('actor_group_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])
            ->addForeignKey('actor_group_id', 'actor_groups', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('typesitting_id', 'integer', [
            'default' => null,
            'limit' => 10,
            'null' => false,
        ])
            ->addForeignKey('typesitting_id', 'typesittings', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->create();
    }
}
