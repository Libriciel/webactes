<?php
use Migrations\AbstractMigration;

class Mimetype extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('alter table files alter COLUMN mimetype TYPE varchar(256)');
    }
}
