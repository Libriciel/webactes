<?php

use Migrations\AbstractMigration;

class Votes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('votes');

        $table->addColumn('actor_id', 'integer', [
            'default' => null,
            'null' => true,
        ])
            ->addForeignKey('actor_id', 'actors', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('president_id', 'integer', [
            'default' => null,
            'null' => false,
        ])
            ->addForeignKey('president_id', 'actors', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('project_id', 'integer', [
            'default' => null,
            'null' => false,
        ])
            ->addForeignKey('project_id', 'projects', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('structure_id', 'integer', [
            'default' => null,
            'null' => false,
        ])
            ->addForeignKey('structure_id', 'structures', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);

        $table->addColumn('yes_counter', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('no_counter', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('abstentions_counter', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('no_words_counter', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('prendre_acte', 'boolean', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('resultat', 'boolean', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('opinion', 'boolean', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('comment', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addTimestamps('created', 'modified');

        $table->create();
    }
}
