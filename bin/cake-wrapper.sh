#!/usr/bin/env bash

#-----------------------------------------------------------------------------------------------------------------------
# /!\ nécessite
#   - sudo
#   - un fichier .env à la racine
#   - variables d'environnement (défauts disponibles)
#       - APACHE_GROUP
#       - APACHE_USER
#       - DEBUG
#       - ROOT
#
# Exemples en ligne de commande:
#   bin/cake-wrapper.sh certificates
#   bin/cake-wrapper.sh postgres.postgres_maintenance all
#   bin/cake-wrapper.sh usersresetpasswordtokens
#
# Exemples dans le fichier docker-ressources/cron/worker:
#   */2 * * * * www-data /var/www/webACTES/bin/cake-wrapper.sh UpdateTdtState sync
#   */2 * * * * www-data /var/www/webACTES/bin/cake-wrapper.sh Signature checkStatus
#   */2 * * * * www-data /var/www/webACTES/bin/cake-wrapper.sh UpdateMailSecInformations sync
#
# Exemple avec le mode xtrace, pour débugger:
#   (export DEBUG=1 ; bin/cake-wrapper.sh UpdateMailSecInformations sync)
#-----------------------------------------------------------------------------------------------------------------------

set -o errexit
set -o nounset
set -o pipefail

#-----------------------------------------------------------------------------------------------------------------------

__FILE__="$(realpath "$0")"
__SCRIPT__="$(basename "${__FILE__}")"
__ROOT__="$(dirname "${__FILE__}")"
__ROOT__="$(realpath "${__ROOT__}/..")"

APACHE_GROUP="${APACHE_GROUP:-www-data}"
APACHE_USER="${APACHE_USER:-www-data}"
DEBUG="${DEBUG:-0}"
DATE_FORMAT="%Y-%m-%d %H:%M:%S,%3N"
ROOT="${ROOT:-$__ROOT__}"

#-----------------------------------------------------------------------------------------------------------------------

main()
{
    (
        if [ $DEBUG -eq 1 ]; then
            set -o xtrace
        fi

        user="`whoami`"

        now="`date +"${DATE_FORMAT}"`"
        args="${@}"

        printf "${now} INFO: starting cake shell: ${args}\n"

        start_time=$SECONDS
        set +o errexit
        (
            cd "${ROOT}"
            cmd="${ROOT}/bin/cake ${args}"

            if [ "${user}" = "root" ] ; then
                cmd="sudo --preserve-env -u ${APACHE_USER} ${cmd}"
            fi

            if [ -f ${ROOT}/.env ] ; then
                export $(grep --color=never -v '\(^#\|^[^=]\+=[^"].*\s\)' ${ROOT}/.env | xargs)
            fi
            $cmd
            status_code=$?

            # @info: check at least a command was provided (or we will get cake help with status 0)
            if [ ${#} -eq 0 ] ; then
                printf >&2 "No command provided. Choose one of the available commands.\n"
                exit 1
            fi
            exit ${status_code}
        )
        status_code=$?
        set -o errexit
        stop_time=$SECONDS

        elapsed=$(( stop_time - start_time ))
        now="`date +"${DATE_FORMAT}"`"
        if [ $status_code -eq 0 ] ; then
            status_phrase="${now} INFO: stopping cake shell ${args}: exited with ${status_code} in ${elapsed} s\n"
            printf "${status_phrase}"
        else
            status_phrase="${now} ERROR: stopping cake shell ${args}: exited with ${status_code} in ${elapsed} s\n"
            printf >&2 "${status_phrase}"
        fi


        exit $status_code
    )
}

main "$@"
