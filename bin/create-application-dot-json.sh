#! /bin/sh


# Vérifiez si un argument a été fourni
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 major.minor.patch-preReleaseIdentifier"
    exit
fi

# Décomposez l'argument en quatre parties
major=$(echo "$1" | cut -d. -f1)
minor=$(echo "$1" | cut -d. -f2)
patch=$(echo "$1" | cut -d. -f3 | cut -d- -f1)
if echo "$1" | grep '-'; then
preReleaseIdentifier=$(echo $1 | cut -d- -f2)
fi

# Créez une chaîne JSON et utilisez jq pour la formater
# shellcheck disable=SC2116
json=$(echo "{\"name\": \"webactes\", \"major\": $major, \"minor\": $minor, \"patch\": $patch, \"preReleaseIdentifier\": \"$preReleaseIdentifier\"}")

# Écrivez la chaîne JSON dans le fichier frontend/application.json
echo $json > frontend/application.json
