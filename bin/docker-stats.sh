#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# @fixme: détecter lorsque la fusion échoue et arrêter le script
# @todo: ajouter la méthode utilisée dans lspdf2odt
# bin/docker-stats.sh -a "/home/cbuffin/Bureau/webactes/performances generations/_a tester/annexes_fusionnees.pdf" -t 10

declare -r _FILE_="$(realpath "${0}")"
declare -r _SCRIPT_="$(basename "${_FILE_}")"
declare -r _APP_DIR_="$(dirname "$(dirname ${_FILE_})")"
declare -r _VERSION_="0.1.0"

declare annexe=""
declare force="false"
declare output="/tmp/docker-stats-webactes"
declare times="1"

_help_()
{
    printf "NAME\n"
    printf "  %s\n" "${_SCRIPT_}"
    printf "\nDESCRIPTION\n"
    printf "  ...\n"
    printf "\nSYNOPSIS\n"
    printf "  %s [OPTION]... [FILE]...\n" "${_SCRIPT_}"
    printf "\nOPTIONS\n"
    printf "  -a|--annexe\tThe path to the annexe file from the host that will be copied to the container and used\n"
    printf "  -f|--force\tDo not ask for confirmation\n"
    printf "  -h|--help\tDisplay this help and exit\n"
    printf "  -o|--output\tThe directory to output results to\n"
    printf "  \t\tDefault value: /tmp/docker-stats-webactes\n"
    printf "  -t|--times\tThe number of times to repeat the tests\n"
    printf "  \t\tDefault value: 1\n"
    printf "  -v|--version\tDisplay version information and exit\n"
    printf "\nEXEMPLES\n"
    printf "  %s -h\n" "${_SCRIPT_}"
    printf "  %s -a tests/Fixture/files/pdf/annexe_001.pdf -t 10\n" "${_SCRIPT_}"
}

_version_()
{
    printf "%s version %s\n" "${_SCRIPT_}" "${_VERSION_}"
}

docker_mem()
{
    local service="${1}"
    docker stats --format "{{.Name}} {{.MemUsage}}" --no-stream | grep "${service}" | sed 's/^\(\S\+\)\s\+\(.\+\) \/ \(.\+\)$/\2/g' | sed 's/\./,/g' | sed 's/iB//g' | numfmt --from=iec --to-unit=G --format=%.3f
}

opts=$(getopt --longoptions annexe:,force,help,times:,version --options a:fht:v -- "$@") || ( >&2 _help_ ; exit 1)
eval set -- "$opts"
while true; do
    case "${1}" in
        -a|--annexe)
            annexe="${2}"
            shift 2
            ;;
        -h|--help)
            _help_
            exit 0
            ;;
        -f|--force)
            force="true"
            shift
            ;;
        -t|--times)
            times="${2}"
            shift 2
            ;;
        -v|--version)
            _version_
            exit 0
            ;;
        --)
            shift
            break
            ;;
    esac
done

if [ ! -f "${annexe}" ] ; then
    >&2 printf "Error: annexe \"%s\" does not exist\n" "${annexe}"
    exit 1
fi

dir="$(docker compose exec serveur mktemp -d)"
basename="$(basename "${annexe}")"
docker cp "${annexe}" webactes-serveur-1:"${dir}"
container_annexe="${dir}/${basename}"
docker compose exec serveur chown -R www-data: "${dir}"

# lspdf2odt: grep Stats webactes-lspdf2odt-1.txt | sed 's/^.*: | \([^ ]\+\) .*$/\1/g'
# gedooo: grep "\(\|Temps total d'exécution\)" webactes-gedooo-1.txt
# cloudooo: grep "\(OooConvert: odt\|unlocked\)" webactes-cloudooo-1.txt | sed 's/webactes-cloudooo-1  | //g' | sed 's/ - Cloudooo - DEBUG - /;/g' | sed 's/^\([0-9]\{4\}\)\-\([0-9]\{2\}\)\-\([0-9]\{2\}\)/\3\/\2\/\1/g'

# supervisorctl stop cron worker_basic worker_deletes worker_mail_sec worker_notifications worker_signature worker_tdt

_shutdown_()
{
    docker cp webactes-serveur-1:/data/workspace ${output}
    docker compose logs serveur > "${output}/logs-serveur.txt"
    docker compose logs lspdf2odt > "${output}/logs-lspdf2odt.txt"
    #docker compose logs gedooo > "${output}/logs-gedooo.txt"
    docker compose logs flow > "${output}/logs-flow.txt"
    #docker compose logs cloudooo > "${output}/logs-cloudooo.txt"
    docker compose logs unoserver > "${output}/logs-unoserver.txt"
}

function mem2go()
{
    local usage="${1}"
    local unit="$(echo "${usage}" | sed 's/^\(.*\)\(.\)iB$/\2/g')"
    local value="$(echo "${usage}" | sed 's/^\(.*\)\(.\)iB$/\1/g')"
    # Units: K, M, G
    if [ "${unit}" == "K" ] ; then
        php -r "printf('%.4f', ${value} / (1024*1024));"
        return
    fi
    if [ "${unit}" == "M" ] ; then
        php -r "printf('%.4f', ${value} / 1024);"
        return
    fi
    if [ "${unit}" == "G" ] ; then
        echo "$value"
        return
    fi
}

function get_stats()
{
    stats="$(docker stats --format "{{.Name}} {{.MemUsage}}" --no-stream | grep -v "^CONTAINER" | sed 's/^\(\S\+\)\s\+\(.\+\) \/ \(.\+\)$/\1 \2/g' | sort)"
    line1="|"
    line2="|"
    while IFS= read -r line; do
        container="$(echo "${line}" | sed 's/^\(\S\+\)\s\(.\+\)$/\1/g')"
        mem="$(echo "${line}" | sed 's/^\(\S\+\)\s\(.\+\)$/\2/g')"
        mem="$(mem2go "${mem}")"
        line1="${line1} ${container} |"
        line2="${line2} ${mem} |"
    done <<< "${stats}"
    echo "${line1}"
    echo "${line2}"
}

mkdir -p "${output}"
logfile="${output}/stats.md"
echo ${logfile}

stats="$(get_stats)"
echo "${stats}"
echo "${stats}" > ${logfile}

trap _shutdown_ EXIT INT QUIT KILL

idx=1

while [ $(free --mega | grep --color=never "^Mem:" | awk '{print $7}') -ge 2000 ] && [ ${idx} -le ${times} ] ; do
    work="$(docker compose exec serveur \
            bin/cake-wrapper.sh docker_stats \
            --annexe="${container_annexe}" \
            --structure=Recette_WA_S2 \
            --sitting=9 \
            --user=admin@wa-s2 \
    )" || echo "${work}"
    elapsed="$(echo "${work}" | grep "Fin de la génération du projet" | sed 's/^.* au bout de \(.\+\) secondes$/\1/g' )"

    # @info: no error message, etc...
    continue="$(echo "${elapsed}" | grep "^[0-9]\+$")"

    stats="$(get_stats)"
    line1="$(echo "${stats}" | head -1)"
    line2="$(echo "${stats}" | head -2 | tail -1)"

    echo "${line1} elapsed |"
    echo "${line1} elapsed |"  >> ${logfile}

    echo "${line2} ${elapsed} |"
    echo "${line2} ${elapsed} |"  >> ${logfile}

    idx=$(( ${idx} + 1 ))
done

docker compose exec serveur rm -rf "${dir}"
