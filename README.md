[![Version PHP](http://img.shields.io/badge/php-%208.1-8892BF.svg)](https://php.net/)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.html)
[![pipeline](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/badges/develop-2.0/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/commits/develop-2.0)
[![coverage](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/badges/develop-2.0/coverage.svg)](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/commits/develop-2.0)
[![lines of code](https://sonarqube.libriciel.fr/api/project_badges/measure?project=webACTES&metric=ncloc)](https://sonarqube.libriciel.fr/dashboard?id=webACTES)
[![quality gate](https://sonarqube.libriciel.fr/api/project_badges/measure?project=webACTES&metric=alert_status)](https://sonarqube.libriciel.fr/dashboard?id=webACTES)
[![technical debt](https://sonarqube.libriciel.fr/api/project_badges/measure?project=webACTES&metric=sqale_index)](https://sonarqube.libriciel.fr/dashboard?id=webACTES)

# webactes

Outil de gestion des actes administratifs multi-collectivités.

## TL;DR

Il est nécessaire d'avoir une entrée `webact.local` qui réponde sur `127.0.0.1` (dans le fichier `/etc/hosts` sur Linux)
Il est également nécessaire que les ports `80` et `443` soient libre.

```bash
cp .env.dist .env
```
Il est nécessaire de remplir les mots de passe du `.env`

```bash
make build
make start
```
C'est prêt : l'application est utilisable sur 'https://webact.local'.

La commande `make` montre l'ensemble des possibilités (lancement, construction, tests)
