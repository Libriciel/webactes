<?php
declare(strict_types=1);

namespace App\TestSuite;

use RuntimeException;

trait UsersTrait
{
    /**
     * Retourne l'id de l'utilisateur dont le username est passé en paramètre.
     *
     * @param string $username Le username de l'utilisateur (users.username)
     * @return int
     */
    protected function getUserId($username): int
    {
        $user = $this->fetchTable('Users')
            ->find('all')
            ->select(['id', 'username'])
            ->where(['username' => $username])
            ->enableHydration(false)
            ->firstOrFail();

        return $user['id'];
    }

    /**
     * @return void
     */
    public function setJsonRequest(): void
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
    }

    /**
     * @return void
     */
    public function setXmlRequest(): void
    {
        $this->configRequest(['headers' => ['Accept' => 'application/xml']]);
    }

    /**
     * @return mixed
     */
    public function getResponse(): mixed
    {
        return json_decode((string)$this->_response->getBody(), true);
    }

    /**
     * Set the default configuration for user 1, organization 1, structure 1 ...
     *
     * @return void
     */
    public function setDefaultSession()
    {
        $this->switchUser(1);
    }

    /**
     * Change the connected user
     *
     * @param int $userId L'id de l'utilisateur (users.id)
     * @return array
     */
    public function switchUser(int $userId): array
    {
        $user = $this->fetchTable('Users')
            ->find()
            ->select(['id', 'keycloak_id', 'organization_id', 'username'])
            ->contain([
                'Roles' => [
                    'fields' => ['id', 'name', 'structure_id'],
                ],
                'Structures' => [
                    'fields' => ['id'],
                ],
            ])
            ->where(['id' => $userId])
            ->enableHydration(false)
            ->firstOrFail();

        foreach (['roles', 'structures'] as $table) {
            if (count($user[$table]) === 0) {
                $message = sprintf(
                    'User %d has no matching record in the %s table. Perhaps you forgot to load the fixture ?',
                    $userId,
                    $table
                );
                throw new RuntimeException($message);
            }
        }

        $session = [
            'keycloak_id' => $user['keycloak_id'],
            'user_id' => $user['id'],
            'Auth' => [
                'User' => [// Correspond en réalité à RoleUser
                    'id' => $user['id'], // id du role user
                    'user_id' => $user['id'],
                    'structure_id' => $user['structures'][0]['id'],
                ],
                'Role' => [
                    'id' => $user['roles'][0]['id'],
                    'name' => $user['roles'][0]['name'],
                    'structure_id' => $user['roles'][0]['structure_id'],
                ],
            ],
            'structure_id' => $user['structures'][0]['id'],
            'organization_id' => $user['organization_id'],
        ];

        $this->session($session);

        return $session;
    }

    /**
     * Permet de modifier à la volée un paramètre de configuration d'une structure.
     *
     * @param int $structureId L'id de la structure (structures.id)
     * @param string $name Le nom de la catégorie de paramètres (act, convocation, project, sitting)
     * @param string $key Le nom du paramètre
     * @param mixed $value La valeur du paramètre
     * @return void
     */
    public function switchStructureSettings(int $structureId, string $name, string $key, mixed $value): void
    {
        $structureSettingsTable = $this->fetchTable('StructureSettings');
        $structureSetting = $structureSettingsTable->findByStructureIdAndName($structureId, $name)->first();

        $settings = $structureSetting->value;
        $settings[$key] = $value;

        $structureSettingsTable->patchEntity($structureSetting, [
            'value' => $settings,
        ]);

        $this->assertNotFalse($structureSettingsTable->save($structureSetting));
    }
}
