<?php
declare(strict_types=1);

namespace App\TestSuite;

use ReflectionClass;

/**
 * Permet d'accéder aux méthodes privées d'une classe pour les tests
 *
 * @category TestSuite
 * @author    Ludovic Ganée <ludovic.ganee@libriciel.coop>
 * @copyright (\App\TestSuite\c) 2019, Libriciel
 * @license   http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 */
trait InvokePrivateTrait
{
    /**
     * Call protected/private method of a class.
     *
     * @param object $object     Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     * @return mixed Method return.
     * @throws \ReflectionException
     */
    public function invokeMethod($object, $methodName, array $parameters = [])
    {
        $reflection = new ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * Call protected/private property of a class.
     *
     * @param  object $object  Instantiated object that we will run method on.
     * @param  string $argName Property name to call
     * @param  string $verb    get or set
     * @param  null   $value   if setter, set this value
     * @return mixed
     * @throws \ReflectionException
     */
    public function invokeProperty($object, $argName, string $verb = 'get', $value = null)
    {
        $reflection = new ReflectionClass(get_class($object));
        $property = $reflection->getProperty($argName);
        $property->setAccessible(true);

        if ($verb === 'get') {
            return $property->getValue($object);
        } elseif ($verb === 'set') {
            $property->setValue($object, $value);
        }
    }
}
