<?php
declare(strict_types=1);

namespace App\TestSuite\Fixture;

use RuntimeException;

trait FixtureGroupTrait
{
    /**
     * @param string $controllerName The controller name
     * @param int|null $structure set to -1 to avoid filtering, null to use the structure_id in session
     * @return int
     * @deprecated
     */
    public function countRecords(string $controllerName, ?int $structure = null): int
    {
        deprecationWarning('FixtureTrait::countRecords is deprecated. Use a real number instead.');

        $records = static::$fixtureManager->loaded()['app.' . $controllerName]->records;
        $structure = $structure ?? $this->_session['structure_id'] ?? null;

        if ($structure !== -1) {
            $records = array_filter(
                $records,
                function ($e) use ($structure) {
                    return array_key_exists('structure_id', $e) === false || $e['structure_id'] === $structure;
                }
            );
        }

        return count($records);
    }

    /**
     * @param string $fixture The fixture string (ex: app.group.MyGroup)
     * @return array
     */
    protected function resolveAppFixtureGroup(string $fixture): array
    {
        $groupFixtureName = preg_replace('/^app\.group\./', '', $fixture);

        $className = '\\App\\Test\\FixtureGroup\\' . $groupFixtureName . 'FixtureGroup';
        if (!class_exists($className)) {
            $message = sprintf(
                'Class "%s" does not exist (fixture "%s" was requested in class "%s")',
                $className,
                $fixture,
                static::class
            );
            throw new RuntimeException($message);
        }

        $extends = 'App\TestSuite\Fixture\TestFixtureGroup';
        if (array_search($extends, class_parents($className)) === false) {
            $message = sprintf('Class "%s" does not extend "%s"', $className, $extends);
            throw new RuntimeException($message);
        }

        return $this->resolveFixtures((new $className())->fixtures);
    }

    /**
     * @param array $fixtures The fixtures list
     * @return array
     */
    protected function resolveFixtures(array $fixtures): array
    {
        $result = [];

        foreach ($fixtures as $fixture) {
            if (strpos($fixture, 'app.group.') === 0) {
                $resolvedGroup = $this->resolveAppFixtureGroup($fixture);
                foreach ($resolvedGroup as $resolved) {
                    if (array_search($resolved, $result, true) === false) {
                        $result[] = $resolved;
                    }
                }
            } elseif (array_search($fixture, $result, true) === false) {
                $result[] = $fixture;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getFixtures(): array
    {
        $this->fixtures = $this->resolveFixtures($this->fixtures);

        return parent::getFixtures();
    }
}
