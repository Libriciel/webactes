<?php
declare(strict_types=1);

namespace App\TestSuite\Fixture;

use RuntimeException;

abstract class TestFixtureGroup
{
    /**
     * @var string[]
     */
    public $fixtures = [];

    /**
     * Check that only the "fixtures" attribute and the "__construct" method exist for this class.
     *
     * @throws \RuntimeException
     */
    public function __construct()
    {
        $className = static::class;

        $allowedAttributes = ['fixtures'];
        $attributes = array_diff(array_keys(get_class_vars($className)), $allowedAttributes);
        if (empty($attributes) === false) {
            $message = sprintf(
                'Unexpected class attribute(s) "%s" for class "%s" (allowed: "%s")',
                implode('", "', $attributes),
                $className,
                implode('", "', $allowedAttributes)
            );
            throw new RuntimeException($message);
        }

        $allowedMethods = ['__construct'];
        $methods = array_diff(get_class_methods($this), $allowedMethods);
        if (empty($methods) === false) {
            $message = sprintf(
                'Unexpected class method(s) "%s" for class "%s" (allowed: "%s")',
                implode('", "', $methods),
                $className,
                implode('", "', $allowedMethods)
            );
            throw new RuntimeException($message);
        }
    }
}
