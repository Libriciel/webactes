<?php
declare(strict_types=1);

namespace App\TestSuite\Fixture;

use Cake\TestSuite\Fixture\FixtureHelper;
use Cake\TestSuite\Fixture\FixtureStrategyInterface;

/**
 * Fixture strategy that loads the fixtures only once for a test class.
 * Use App\TestSuite\LoadOncePerClassFixturesTrait in your test classes if you are only querying the data in database.
 */
class LoadOnceStrategy implements FixtureStrategyInterface
{
    /**
     * @var \Cake\TestSuite\Fixture\FixtureHelper
     */
    protected $helper;

    /**
     * @var array<\Cake\Datasource\FixtureInterface>
     */
    protected array $fixtures = [];

    /**
     * @var bool
     */
    protected bool $loaded = false;

    /**
     * Initialize strategy.
     *
     * @param bool $loaded Has the strategy already been loaded by the test class ?
     */
    public function __construct(bool $loaded)
    {
        $this->loaded = $loaded;
        $this->helper = new FixtureHelper();
    }

    /**
     * @inheritDoc
     */
    public function setupTest(array $fixtureNames): void
    {
        if (empty($fixtureNames)) {
            return;
        }

        if (!$this->loaded) {
            $this->fixtures = $this->helper->loadFixtures($fixtureNames);
            $this->helper->insert($this->fixtures);
        }
    }

    /**
     * Called after each test run in each TestCase.
     * Do not truncate, truncation will be done in teardownClass.
     *
     * @return void
     */
    public function teardownTest(): void
    {
    }

    /**
     * Returns the loaded fixtures.
     *
     * @return \Cake\Datasource\FixtureInterface[]
     */
    public function getFixtures(): array
    {
        return $this->fixtures;
    }

    /**
     * Truncate the fixtures at the end of the test class.
     *
     * @param array $fixtures The fixtures list to truncate
     * @return void
     */
    public function teardownClass(array $fixtures): void
    {
        $this->helper->truncate($fixtures);
    }
}
