<?php
declare(strict_types=1);

namespace App\TestSuite;

use App\TestSuite\Fixture\LoadOnceStrategy;
use Cake\TestSuite\Fixture\FixtureStrategyInterface;

/**
 * Ce trait permet de ne charger les données des fixtures qu'une seule fois par classe de test, ce qui permet d'accélérer
 * sensiblement le temps d'exécution des tests. Les données seront tronquées à la fin de la classe de test.
 *
 * À n'utiliser que si la classe de test ne modifie aucune donnée.
 */
trait LoadOncePerClassFixturesTrait
{
    /**
     * @var bool
     */
    protected static bool $loaded = false;

    /**
     * @var \App\TestSuite\Fixture\LoadOnceStrategy
     */
    protected static LoadOnceStrategy $strategy;

    /**
     * Exécuté avant l'exécution du premier test de la classe, permet de savoir que l'on n'a pas encore chargé de fixture.
     *
     * @return void
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        static::$loaded = false;
    }

    /**
     * Nettoyage des données après l'exécution du dernier test de la classe.
     *
     * @return void
     */
    public static function tearDownAfterClass(): void
    {
        (new LoadOnceStrategy(true))->teardownClass(static::$strategy->getFixtures());
        parent::tearDownAfterClass();
    }

    /**
     * Retourne la stratégie de fixture, suivant qu'elle a déjà été chargée dans cette classe ou non.
     *
     * @return \Cake\TestSuite\Fixture\FixtureStrategyInterface
     */
    protected function getFixtureStrategy(): FixtureStrategyInterface
    {
        $loaded = static::$loaded ?? false;
        static::$loaded = true;

        if ($loaded === false) {
            return static::$strategy = new LoadOnceStrategy($loaded);
        } else {
            return new LoadOnceStrategy($loaded);
        }
    }
}
