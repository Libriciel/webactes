<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\Entity\Stateact;
use App\Model\Table\HistoriesTable;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;
use WrapperFlowable\Api\WrapperFactory;

/**
 * @property \App\Model\Table\NotificationsUsersTable $NotificationsUsers
 * @property \App\Model\Table\NotificationsTable $Notifications
 */
class NotificationService
{
    use LocatorAwareTrait;

    public const ACTION_SEND_TO_CIRCUIT = 'sendToCircuit';
    public const ACTION_APPROVE = 'approveAction';

    public const VALDATION_TO_DO = 'validation_to_do';
    public const PROJECT_APPROVE_AT_STEP = 'action_approve';
    public const PROJECT_REJECT_AT_STEP = 'action_reject';
    public const PROJECT_AT_STEP = 'projectAtState';
    public const MY_PROJECT_AT_STATE = 'myProjectAtState';
    public const SIGNED = 'signed';

    /**
     * @inheritDoc
     */
    public function __construct(?array $config = null)
    {
        $this->Notifications = $this->fetchTable('Notifications');
        $this->NotificationsUsers = $this->fetchTable('NotificationsUsers');
    }

    /**
     * @param string $action context
     * @param \Cake\Datasource\EntityInterface $project project
     * @param array $options options
     * @return void|null
     */
    public function fireEvent(string $action, EntityInterface $project, array $options = [])
    {
        switch ($action) {
            case self::ACTION_SEND_TO_CIRCUIT:
            case Stateact::VALIDATION_PENDING:
                $this->validationToDo($project, $options);
                break;
            case self::MY_PROJECT_AT_STATE:
                $this->myProjectAtStep($project, $options);
                break;
            default:
        }
    }

    /**
     * @param \Cake\Datasource\EntityInterface $project project
     * @param array $options options
     * @return void|null
     */
    private function validationToDo(EntityInterface $project, array $options)
    {
        /** @var \App\Controller\Component\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();

        //Get users candidates ids before filtering by accept notifications
        $usersCandidates = $wrapper->getCurrentUsersCadidatesIdsByBusinessKey($project->id_flowable_instance);

        $usersCandidatesNotificationActive = $this->getUsersCandidatesFilteringByNotificationCode(
            $usersCandidates,
            self::VALDATION_TO_DO
        );

        foreach ($usersCandidatesNotificationActive as $usersCandidate) {
            $event = new Event(
                'Model.Project.afterSaveState.Email.' . Stateact::VALIDATION_PENDING,
                $this,
                [
                    'object_id' => $project->id,
                    'user_id' => $usersCandidate['user_id'],
                    'notification_name' => $this->Notifications
                        ->find('all')
                        ->where(['code' => self::VALDATION_TO_DO])
                        ->firstOrFail()
                        ->get('name'),
                    'last_user_id' => $options['last_user_id'] ?? null,
                    'libelle' => $project->name ?? '',
                    'comment' => $options['comment'] ?? null,
                    'structureId' => $project->structure_id,
                    'isMine' => false,
                    'isTdt' => $project->containers[0]->typesact->istdt,
                ]
            );
            EventManager::instance()->dispatch($event);
        }
    }

    /**
     * @param \Cake\Datasource\EntityInterface $project project
     * @param array $options options
     * @return void
     */
    private function myProjectAtStep(EntityInterface $project, array $options)
    {
        Log::write('info', self::class . '*******' . __FUNCTION__ . '*******', 'Notifications');

        $isMine = true;
        /**
         * @var \App\Service\Project $project
         */
        $creater = $this->fetchTable('Histories')
            ->find()
            ->select(['Histories.user_id'])
            ->where(
                [
                    'Histories.comment' => HistoriesTable::CREATE_PROJECT,
                    'Histories.container_id' => $project->containers[0]->id,
                ]
            )
            ->firstOrFail()
            ->get('user_id');

        $NotificationCode = '';
        //get laststateAct
        /** @var \App\Controller\Component\ContainersTable $Containers */
        $Containers = $this->fetchTable('Containers');

        /** @var \App\Model\Entity\Stateact $stateAct */
        $stateAct = $Containers->getLastStateActEntry($project->containers[0]['id'])[0];
        $project->containers[0]->stateacts = $stateAct;

        switch ($stateAct['id']) {
            // En cours dans un circuit + validation d'étape de circuit
            case REFUSED:
            case VALIDATION_PENDING:
                $NotificationCode = HistoriesTable::SEND_TO_CIRCUIT;
                break;
            case VALIDATED:
            case PARAPHEUR_SIGNING:
                $NotificationCode = HistoriesTable::VALIDATION;
                break;
            case DECLARE_SIGNED:
            case PARAPHEUR_SIGNED:
                $NotificationCode = $project->containers[0]->typesact->istdt
                    ? HistoriesTable::TDT_TRANSMISSION
                    : self::SIGNED;
                break;
            default:
        }

        $userNotification = $this->getUsersCandidatesFilteringByNotificationCode(
            [$creater],
            $NotificationCode
        );

        if (!empty($userNotification)) {
            $this->sendEventProjectState(
                $project,
                array_merge(
                    $options,
                    ['userId' => (int)$creater, 'NotificationCode' => $NotificationCode, 'isMine' => $isMine]
                )
            );
        }
    }

    /**
     * @param array $usersCandidates array of user candidates id
     * @param string $notificationCode Code permettant d'identifier la notification
     * @return array
     */
    private function getUsersCandidatesFilteringByNotificationCode(array $usersCandidates, string $notificationCode): array // @codingStandardsIgnoreLine
    {
        $notification = $this->Notifications->find('all')->where(['code' => $notificationCode])->first();

        return empty($usersCandidates) || is_null($notification)
            ? []
            : $this->NotificationsUsers->find('all')
                ->select(['id', 'user_id'])
                ->where(
                    [
                        'user_id IN ' => $usersCandidates,
                        'active' => true,
                        'notification_id IN' => $notification->get('id'),
                    ]
                )->toArray();
    }

    /**
     * @param \Cake\Datasource\EntityInterface $project project
     * @param int $options user id
     * @return void
     */
    private function sendEventProjectState(EntityInterface $project, array $options) // @codingStandardsIgnoreLine
    {
        Log::write('info', self::class . '*******' . __FUNCTION__ . '*******', 'Notifications');

        $notificationUser = $this->NotificationsUsers
            ->find('all')
            ->select(['Notifications.code', 'Notifications.name'])
            ->innerJoin(['Notifications'])
            ->where([
                'user_id' => $options['userId'],
                'active' => true,
                'Notifications.code' => $options['NotificationCode'],
            ])
            ->first();

        if ($notificationUser) {
            $event = new Event(
                'Model.Project.afterSaveState.Email.' . self::MY_PROJECT_AT_STATE,
                $this,
                [
                    'object_id' => $project->id,
                    'object_state_code' => $project->containers[0]->stateacts->code,
                    'notification_name' => $notificationUser['Notifications']['name'],
                    'user_id' => $options['userId'],
                    'last_user_id' => $options['last_user_id'] ?? null,
                    'libelle' => $project->name ?? '',
                    'comment' => $options['comment'] ?? null,
                    'structureId' => $project->structure_id,
                    'isMine' => $options['isMine'] ?? false,
                    'isTdt' => $project->containers[0]->typesact->istdt,
                ]
            );
            EventManager::instance()->dispatch($event);
        }
    }
}
