<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\GenerationException;
use App\Model\Enum\DocumentType;
use App\Model\Table\GenerationerrorsTable;
use App\Utilities\Common\RealTimezone;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Inflector;
use Libriciel\OfficeClients\Conversion\Client\ClientFactory as ConversionClientFactory;
use Throwable;

/**
 * Class GenerationsService
 *
 * @property \App\Model\Table\ContainersTable $Containers
 * @package App\Service
 */
class GenerationsService
{
    use LocatorAwareTrait;
    use ServiceAwareTrait;

    protected ?string $odtFusionResult;
    protected string $exitMimeType;
    protected ?string $fileContent;

    /**
     * conversion
     *
     * @return void
     * @throws \Throwable
     */
    protected function conversion()
    {
        try {
            if ($this->exitMimeType === 'application/pdf') {
                $ext = 'pdf';
            }

            $client = ConversionClientFactory::create();

            if (!(bool)env('CONVERSIONS_TOC_HACK', '0')) {
                $this->fileContent = $client
                    ->conversion((string)$this->odtFusionResult, 'odt', $ext);
            } else {
                $this->fileContent = $client
                    ->conversion((string)$this->odtFusionResult, 'odt', 'odt');
                $this->fileContent = $client
                    ->conversion($this->fileContent, 'odt', 'odt');
                if ($ext === 'pdf') {
                    $this->fileContent = $client
                        ->conversion($this->fileContent, 'odt', 'pdf');
                }
            }
        } catch (Throwable $exc) {
            Log::error(sprintf('Erreur conversion : %d - %s', $exc->getMessage(), $exc->getCode()));
            throw $exc;
        }
    }

    /**
     * Génération d'un document en lien avec un projet.
     *
     * Valeurs de $templateTypeCode possibles: act, project.
     *
     * @param int $projectId L'id du projet (projects.id)
     * @param string $templateTypeCode Le type de template
     * @param string $exitMimeType Le type MIME de sortie
     * @return string
     * @throws \Exception
     */
    protected function _generateByProjectId(
        int $projectId,
        string $templateTypeCode,
        string $exitMimeType
    ) {
        try {
            $this->exitMimeType = $exitMimeType;

            $this->Containers = $this->fetchTable('Containers');
            $container = $this->Containers
                ->find()
                ->select(['id', 'project_id'])
                ->where(['Containers.project_id' => $projectId])
                ->enableHydration(false)
                ->firstOrFail();

            $logMessageDetails = sprintf('avec le gabarit %s pour le projet d\'id %d', $templateTypeCode, $projectId);

            Log::debug(sprintf('Début de la fusion %s', $logMessageDetails));
            $fusionMethodName = 'get' . Inflector::camelize($templateTypeCode);
            $this->odtFusionResult = $this->loadService('Fusion', [], false)
                ->{$fusionMethodName}($container['id']);
            Log::debug(sprintf('Fin de la fusion %s', $logMessageDetails));

            Log::debug(sprintf('Début de la conversion %s', $logMessageDetails));
            $this->conversion();
            Log::debug(sprintf('Fin de la conversion %s', $logMessageDetails));

            GenerationerrorsTable::cleanup($templateTypeCode, $projectId);

            return $this->fileContent;
        } catch (\Throwable $exc) {
            GenerationerrorsTable::persist($templateTypeCode, $projectId, $exc->getMessage());
            throw new GenerationException($templateTypeCode, $projectId, $exc->getMessage());
        }
    }

    /**
     * Génération du document d'acte.
     *
     * @param int $projectId L'id du projet (projects.id)
     * @param string $exitMimeType Le type MIME de sortie
     * @return string
     * @throws \Exception
     */
    public function generateAct(int $projectId, string $exitMimeType): string
    {
        return $this->_generateByProjectId($projectId, 'act', $exitMimeType);
    }

    /**
     * Génération du document de projet.
     *
     * @param int $projectId L'id du projet (projects.id)
     * @param string $exitMimeType Le type MIME de sortie
     * @return string
     * @throws \Exception
     */
    public function generateProject(int $projectId, string $exitMimeType): string
    {
        return $this->_generateByProjectId($projectId, 'project', $exitMimeType);
    }

    /**
     * Génération d'un document en lien avec une séance.
     *
     * Valeurs de $templateTypeCode possibles: convocation, executive_summary, deliberations_list, verbal_trial.
     *
     * @param int $sittingId L'id de la séance (sittings.id)
     * @param string $templateTypeCode Le type de template
     * @param string $exitMimeType Le type MIME de sortie
     * @return string
     * @throws \Exception
     */
    protected function _generateBySittingId(int $sittingId, string $templateTypeCode, string $exitMimeType)
    {
        try {
            $this->exitMimeType = $exitMimeType;

            $logMessageDetails = sprintf('avec le gabarit %s pour la séance d\'id %d', $templateTypeCode, $sittingId);

            Log::debug(sprintf('Début de la fusion %s', $logMessageDetails));
            $fusionMethodName = 'get' . Inflector::camelize($templateTypeCode);
            $this->odtFusionResult = $this->loadService('Fusion', [], false)
                ->{$fusionMethodName}($sittingId);
            Log::debug(sprintf('Fin de la fusion %s', $logMessageDetails));

            Log::debug(sprintf('Début de la conversion %s', $logMessageDetails));
            $this->conversion();
            Log::debug(sprintf('Fin de la conversion %s', $logMessageDetails));

            $content = $this->fileContent;
            $this->fileContent = null;
            $this->odtFusionResult = null;
            GenerationerrorsTable::cleanup($templateTypeCode, $sittingId);

            return $content;
        } catch (\Throwable $exc) {
            GenerationerrorsTable::persist($templateTypeCode, $sittingId, $exc->getMessage());
            throw new GenerationException($templateTypeCode, $sittingId, $exc->getMessage());
        }
    }

    /**
     * Génération du document de convocation.
     *
     * @param int $sittingId L'id de la séance (sittings.id)
     * @param string $exitMimeType Le type MIME de sortie
     * @return string
     * @throws \Exception
     */
    public function generateConvocation(int $sittingId, string $exitMimeType)
    {
        return $this->_generateBySittingId($sittingId, 'convocation', $exitMimeType);
    }

    /**
     * Génération du document de note de synthèse.
     *
     * @param int $sittingId L'id de la séance (sittings.id)
     * @param string $exitMimeType Le type MIME de sortie
     * @return string
     * @throws \Exception
     */
    public function generateExecutiveSummary(int $sittingId, string $exitMimeType)
    {
        return $this->_generateBySittingId($sittingId, 'executive_summary', $exitMimeType);
    }

    /**
     * Génération des documents de convocation et de note de synthèse
     *
     * @param int $summonId L'id de la convocation (summons.id)
     * @return void
     * @throws \Exception
     */
    public function generateSummon(int $summonId): void
    {
        $summon = $this->fetchTable('Summons')
            ->get($summonId, ['contain' => ['AttachmentsSummons', 'AttachmentsSummons.Attachments']]);

        $files = $this->fetchTable('Files');
        $allFiles = $files
            ->find()
            ->select(['Files.id'])
            ->innerJoinWith('AttachmentsSummons')
            ->innerJoinWith('AttachmentsSummons.Summons')
            ->where(['Summons.id' => (int)$summonId]);

        $filesIds = $allFiles->all()->extract('id')->toArray();
        if (!empty($filesIds)) {
            $files->deleteAll(['id IN' => $filesIds]);
        }

        foreach ($summon->attachments_summons as $attachmentSummon) {
            $serviceAttribute = 'GenerateSummons' . Inflector::camelize($attachmentSummon->attachment->name);
            $this->{$serviceAttribute} = $this->loadService('GenerateSummons', [], false);
            $method = 'fileGenerate' . Inflector::camelize($attachmentSummon->attachment->name);
            Log::debug(sprintf('%s::%s(%d)', $serviceAttribute, $method, $attachmentSummon->id));
            $this->{$serviceAttribute}->{$method}($attachmentSummon->id);
            $this->{$serviceAttribute} = null;
        }
    }

    /**
     * Génération du document de liste des délibérations
     *
     * @param int $sittingId L'id de la séance (sittings.id)
     * @param string $exitMimeType Le type MIME de sortie
     * @return string
     * @throws \Exception
     */
    public function generateDeliberationsList(int $sittingId, string $exitMimeType)
    {
        return $this->_generateBySittingId($sittingId, 'deliberations_list', $exitMimeType);
    }

    /**
     * Génération du document de procès-verbal
     *
     * @param int $sittingId L'id de la séance (sittings.id)
     * @param string $exitMimeType Le type MIME de sortie
     * @return string
     * @throws \Exception
     */
    public function generateVerbalTrial(int $sittingId, string $exitMimeType)
    {
        return $this->_generateBySittingId($sittingId, 'verbal_trial', $exitMimeType);
    }

    /**
     * @param string $type Type of the document
     * @param int $primaryKey Primary key of the target document
     * @return string
     */
    public function getDocumentFileName(string $type, int $primaryKey): string
    {
        switch ($type) {
            case DocumentType::ACT:
                return $this->fetchTable('Projects')->get($primaryKey)->get('code_act') ?? 'acte-' . $primaryKey;
            case DocumentType::PROJECT:
                return $this->fetchTable('Projects')->get($primaryKey)->get('code_act') ?? 'projet-' . $primaryKey;
            case DocumentType::DELIBERATIONS_LIST:
                $prefix = 'liste-des-deliberations';

                return $this->buildSittingDocumentFileName($primaryKey, $prefix);
            case DocumentType::VERBAL_TRIAL:
                $prefix = 'proces-verbal';

                return $this->buildSittingDocumentFileName($primaryKey, $prefix);
            default:
                return 'document';
        }
    }

    /**
     * @param int $primaryKey Primary key of the sitting
     * @param string $prefix Prefix for the document name
     * @return string The filename
     */
    private function buildSittingDocumentFileName(int $primaryKey, string $prefix): string
    {
        $sitting = $this->fetchTable('Sittings')
            ->find()
            ->select(['date'])
            ->where(['Sittings.id' => $primaryKey])
            ->contain(['Typesittings' => ['fields' => ['name']]])
            ->enableHydration(false)
            ->toArray();

        // exemple liste-des-deliberations_25_03_2024_18h00
        return $prefix
            . '-'
            . RealTimezone::format($sitting[0]['date'], 'd-m-Y-H\hi')
            . '-' . $primaryKey;
    }
}
