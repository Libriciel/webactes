<?php
declare(strict_types=1);

namespace App\Service;

use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Class FoldersService
 *
 * @package App\Service\Generates
 */
class FoldersService
{
    use ServiceAwareTrait;

    /**
     * @var array
     */
    private array $tmpFolderList = [];
    /**
     * @var string
     */
    private string $tmpFolder;

    /**
     * Constructor
     *
     * @param string $name Foo
     * @return void
     */
    public function __construct(string $name)
    {
        $this->tmpFolder = TMP . 'file' . DS . $name;
        if (!is_dir($this->tmpFolder)) {
            mkdir($this->tmpFolder, 0777, true);
        }
    }

    /**
     * @return string
     */
    public function getTmpFolder(): string
    {
        return $this->tmpFolder;
    }

    /**
     * @param string $folder Foo
     * @return string
     */
    public function setNewTmpFolder(string $folder): string
    {
        array_push($this->tmpFolderList, $this->tmpFolder . DS . $folder);
        if (!file_exists($this->tmpFolder . DS . $folder)) {
            mkdir($this->tmpFolder . DS . $folder, 0777, true);
        }

        return $this->tmpFolder . DS . $folder;
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        foreach ($this->tmpFolderList as $tmpFolder) {
            $it = new RecursiveDirectoryIterator($tmpFolder, RecursiveDirectoryIterator::SKIP_DOTS);
            $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ($files as $file) {
                if ($file->isDir()) {
                    rmdir($file->getRealPath());
                } else {
                    unlink($file->getRealPath());
                }
            }
        }
    }
}
