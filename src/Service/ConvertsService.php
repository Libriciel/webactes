<?php
declare(strict_types=1);

namespace App\Service;

use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Log\Log;
use InvalidArgumentException;
use Libriciel\OfficeClients\Conversion\Client\ClientFactory as ConversionClientFactory;
use Libriciel\OfficeClients\Pdf2Odt\Client\ClientFactory as Pdf2OdtClientFactory;
use RuntimeException;
use function App\safe_mkdir as safe_mkdir;

/**
 * Class ConvertsService
 * AnnexesConvert
 *
 * @property \App\Service\Converts\AnnexesConvertService $AnnexesConvert
 * @property \App\Service\Converts\Folders $Folders
 * @package App\Service
 */
class ConvertsService
{
    use ServiceAwareTrait;

    /**
     * @var int Foo
     */
    private int $modelTypeCode;
    /**
     * @var int Foo
     */
    private int $id;
    /**
     * @var string Foo
     */
    private string $templateTypeCode;
    /**
     * @var string Foo
     */
    private string $exitMimeType;

    /**
     * @var \App\Model\Entity\File[]
     */
    private array $files;
    private array $ids;
    private object $Folders;

    /**
     * Constructor
     *
     * @param string $templateTypeCode Foo
     * @param array $ids Foo
     */
    public function __construct(string $templateTypeCode, array $ids)
    {
        $this->templateTypeCode = $templateTypeCode;
        $this->ids = $ids;
        $this->Folders = $this->loadService('Folders', ['Convert']);

        self::initializeService();
    }

    /**
     * Initialize Service
     *
     * @return void
     */
    private function initializeService()
    {
        switch ($this->templateTypeCode) {
            case 'annex_id':
                $this->AnnexesConvert = $this->loadService('Converts/AnnexesConvert');
                $this->exitMimeType = 'application/vnd.oasis.opendocument.text';
                $this->files = $this->AnnexesConvert->getAnnexesPath($this->ids);
                break;
            default:
                $format = 'Valeur "%s" non disponible. Valeur acceptée: "annex_id"';
                $message = sprintf($format, $this->templateTypeCode);
                throw new RuntimeException($message);
        }
    }

    /**
     * getFile
     *
     * @return void
     * @throws \Exception
     */
    public function convertFiles()
    {
        switch ($this->templateTypeCode) {
            case 'annex_id':
                $this->convertModelAnnex();
                break;
            default:
                $format = 'Valeur "%s" non disponible. Valeur acceptée: "annex_id"';
                $message = sprintf($format, $this->templateTypeCode);
                throw new RuntimeException($message);
        }
    }

    /**
     * generateModelProject
     *
     * @return void
     * @throws \Exception
     */
    private function convertModelAnnex()
    {
        if (!empty($this->files)) {
            foreach ($this->files as $file) {
                $this->toOdt($file);
            }
            // Delete dir temp
            $this->Folders->delete();
        }
    }

    /**
     * Conversion d'un fichier au format ODT pour être inclus dans la fusion.
     *
     * Le fichier résultant se trouvera dans un sous-dossier odt avec le même nom de fichier. Par exemple, si on convertit
     * le fichier /data/workspace/1/4/2023/22/03a0f171-852e-4032-9d78-a4b35260dcb6, alors le fichier converti se trouvera
     * dans /data/workspace/1/4/2023/22/odt/03a0f171-852e-4032-9d78-a4b35260dcb6.
     *
     * @param \App\Model\Entity\File $file Le fichier original à convertir en ODT
     * @return void
     */
    public function toOdt(\App\Model\Entity\File $file)
    {
        $odtPath = dirname($file->path) . DS . 'odt' . DS . basename($file->path);

        Log::debug(sprintf('Début de la conversion du fichier %s en ODT dans %s', $file->path, $odtPath));
        $tmpPdfFile = $this->Folders->setNewTmpFolder($this->templateTypeCode . DS . $file->id) . DS . '_origine.pdf';

        // @info: pour l'instant, dans webactes, on ne peut joindre à la fusion que des fichiers PDF
        if ($file->mimetype != 'application/pdf') {
            $pdfContent = ConversionClientFactory::create()
                ->conversion(file_get_contents($file->path), pathinfo($file->name, PATHINFO_EXTENSION), 'pdf');
            if (!file_put_contents($tmpPdfFile, $pdfContent)) {
                $message = sprintf(
                    'Impossible d\'écrire la conversion en PDF du fichier %s vers %s',
                    $file->path,
                    $tmpPdfFile
                );
                throw new InvalidArgumentException($message);
            }
        } else {
            if (!copy($file->path, $tmpPdfFile)) {
                $message = sprintf('Impossible de copier le fichier %s vers %s', $file->path, $tmpPdfFile);
                throw new InvalidArgumentException($message);
            }
        }

        try {
            $odtContent = Pdf2OdtClientFactory::create()->pdf2odt($tmpPdfFile);
            safe_mkdir(dirname($odtPath));
            if (file_put_contents($odtPath, $odtContent) === false) {
                throw new InvalidArgumentException(sprintf('Impossible d\'écrire dans le fichier %s', $odtPath));
            }
        } catch (\Throwable $exc) {
            throw new InvalidArgumentException($exc->getMessage(), 0, $exc);
        }

        Log::debug(sprintf('Fin de la conversion du fichier %s en ODT dans %s', $file->path, $odtPath));
    }
}
