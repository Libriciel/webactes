<?php
declare(strict_types=1);

namespace App\Service;

use App\Service\FusionPart\ActFusionPart;
use App\Service\FusionPart\ProjectFusionPart;
use App\Service\FusionPart\SittingFusionPart;
use App\Utilities\Common\PhpOdtApi;
use Cake\Core\Configure;
use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Cake\Utility\Inflector;
use Libriciel\OfficeClients\Fusion\Client\ClientFactory as FusionClientFactory;
use Libriciel\OfficeClients\Fusion\Helper\Debugger as FusionDebugger;
use Libriciel\OfficeClients\Fusion\Type\PartType;
use SplFileObject;
use Throwable;

class FusionService
{
    use LocatorAwareTrait;

    /**
     * @param string $code Le code du gabarit (act ou project)
     * @param int $containerId L'id du conteneur (containers.id)
     * @return string
     */
    protected function getProjectTemplatePath(string $code, int $containerId): string
    {
        $typesact = $this->fetchTable('Typesacts')->find()
            ->select(['generate_template_' . $code . '_id'])
            ->innerJoinWith('Containers', function (Query $q) use ($containerId) {
                return $q->where(['Containers.id' => $containerId]);
            })
            ->contain([
                Inflector::camelize($code)
                . 'GenerateTemplates.GenerateTemplateTypes' => function (Query $q) use ($code) {
                    return $q->where(['GenerateTemplateTypes.code' => $code]);
                }])->firstOrFail();
        $generate_template_id = $typesact->{'generate_template_' . $code . '_id'};

        $file = $this->fetchTable('Files')->find()
            ->where(['generate_template_id' => $generate_template_id])
            ->contain(['GenerateTemplates', 'GenerateTemplates.GenerateTemplateTypes'])
            ->firstOrFail();

        return $file->path;
    }

    /**
     * @param string $code Le code du gabarit (convocation, executive_summary, deliberations_list ou verbal_trial)
     * @param int $sittingId L'id de la séance (sittings.id)
     * @return string
     */
    protected function getSittingTemplatePath(string $code, int $sittingId): string
    {
        $typesitting = $this->fetchTable('Typesittings')->find()
            ->select(['generate_template_' . $code . '_id'])
            ->innerJoinWith('Sittings', function (Query $q) use ($sittingId) {
                return $q->where(['Sittings.id' => $sittingId]);
            })
            ->contain(
                [
                    Inflector::camelize($code)
                    . 'GenerateTemplates.GenerateTemplateTypes' => function (Query $q) use ($code) {
                        return $q->where(['GenerateTemplateTypes.code' => $code]);
                    },
                ]
            )->firstOrFail();
        $generate_template_id = $typesitting->{'generate_template_' . $code . '_id'};

        $file = $this->fetchTable('Files')->find()
            ->where(['generate_template_id' => $generate_template_id])
            ->contain(['GenerateTemplates', 'GenerateTemplates.GenerateTemplateTypes'])
            ->firstOrFail();

        return $file->path;
    }

    /**
     * @param string $templatePath Le chemin vers le fichier de gabarit
     * @return \App\Utilities\Common\PhpOdtApi
     * @throws \Exception
     */
    protected function getTemplate(string $templatePath): PhpOdtApi
    {
        $template = new PhpOdtApi();
        $splFile = new SplFileObject($templatePath, 'r');
        $odt = $splFile->fread($splFile->getSize());
        $template->loadFromOdtBin($odt);

        return $template;
    }

    /**
     * Exports the contents of a PartType to 2 CSV files, one with all the paths and values (-allPaths), the other
     * with all the hash paths and thus without the values (-hashPaths).
     *
     * Only works if the env variable "PHP_ODT_API_EXPORT" equals "true" (thus the CakePHP configure variable "PhpOdtApi.export"
     * equals true).
     *
     * @see https://book.cakephp.org/4/en/core-libraries/hash.html#expression-types
     * @param string $templatePath The path to the template file
     * @param \Libriciel\OfficeClients\Fusion\Type\PartType $part The PartType to export
     * @return void
     */
    protected function export(string $templatePath, PartType $part)
    {
        if (Configure::read('PhpOdtApi.export') === true) {
            $prefix = TMP . DS . 'exports';
            if (file_exists($prefix) === false) {
                mkdir($prefix);
            }

            $basepath = sprintf(
                $prefix . DS . '%s-%s-%s',
                basename($templatePath), //@fixme
                date('Ymd-His'),
                microtime(true)
            );
            $paths = [
                $basepath . '-hashPaths.csv' => FusionDebugger::hashPathsToCsv($part),
                $basepath . '-allPaths.csv' => FusionDebugger::allPathsToCsv($part, true),
            ];
            foreach ($paths as $path => $content) {
                $written = file_put_contents($path, $content);
                if ($written !== false) {
                    Log::debug(sprintf('File %s written', $path));
                } else {
                    Log::error(sprintf('Could not write to file %s', $path));
                }
            }
        }
    }

    /**
     * @param string $templatePath Le chemin vers le fichier de gabarit
     * @param \Libriciel\OfficeClients\Fusion\Type\PartType $mainPart La partie principale contenant le données à fusionner
     * @return string
     * @throws \Throwable
     */
    protected function fusion(string $templatePath, PartType $mainPart): string
    {
        static::export($templatePath, $mainPart);

        try {
            return FusionClientFactory::create()->fusion($templatePath, $mainPart);
        } catch (Throwable $exc) {
            Log::error(sprintf('Erreur fusion : %d - %s', $exc->getMessage(), $exc->getCode()));
            throw $exc;
        }
    }

    /**
     * @param int $containerId L'id du conteneur (containers.id)
     * @param int|null $sittingId L'id de la séance (sittings.id)
     * @return string
     * @throws \Throwable
     */
    public function getAct(int $containerId, ?int $sittingId = null): string
    {
        $templatePath = $this->getProjectTemplatePath('act', $containerId);
        $mainPart = (new ActFusionPart($this->getTemplate($templatePath), $containerId, $sittingId))->build();

        return $this->fusion($templatePath, $mainPart);
    }

    /**
     * @param int $containerId L'id du conteneur (containers.id)
     * @return string
     * @throws \Throwable
     */
    public function getProject(int $containerId): string
    {
        $templatePath = $this->getProjectTemplatePath('project', $containerId);
        $mainPart = (new ProjectFusionPart($this->getTemplate($templatePath), $containerId))->build();

        return $this->fusion($templatePath, $mainPart);
    }

    /**
     * @param int $sittingId L'id de la séance (sittings.id)
     * @return string
     * @throws \Throwable
     */
    public function getConvocation(int $sittingId): string
    {
        $templatePath = $this->getSittingTemplatePath('convocation', $sittingId);
        $mainPart = (new SittingFusionPart($this->getTemplate($templatePath), $sittingId))->build();

        return $this->fusion($templatePath, $mainPart);
    }

    /**
     * @param int $sittingId L'id de la séance (sittings.id)
     * @return string
     * @throws \Throwable
     */
    public function getExecutiveSummary(int $sittingId): string
    {
        $templatePath = $this->getSittingTemplatePath('executive_summary', $sittingId);
        $mainPart = (new SittingFusionPart($this->getTemplate($templatePath), $sittingId))->build();

        return $this->fusion($templatePath, $mainPart);
    }

    /**
     * @param int $sittingId L'id de la séance (sittings.id)
     * @return string
     * @throws \Throwable
     */
    public function getDeliberationsList(int $sittingId): string
    {
        $templatePath = $this->getSittingTemplatePath('deliberations_list', $sittingId);
        $mainPart = (new SittingFusionPart($this->getTemplate($templatePath), $sittingId))->build();

        return $this->fusion($templatePath, $mainPart);
    }

    /**
     * @param int $sittingId L'id de la séance (sittings.id)
     * @return string
     * @throws \Throwable
     */
    public function getVerbalTrial(int $sittingId): string
    {
        $templatePath = $this->getSittingTemplatePath('verbal_trial', $sittingId);
        $mainPart = (new SittingFusionPart($this->getTemplate($templatePath), $sittingId))->build();

        return $this->fusion($templatePath, $mainPart);
    }
}
