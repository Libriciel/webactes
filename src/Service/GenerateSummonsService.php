<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\Table\GenerationerrorsTable;
use App\Utilities\Common\RealTimezone;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Class GenerateSummonsService
 *
 * @property \App\Model\Table\SittingsTable $Sittings
 * @property \App\Model\Table\TypesittingsTable $Typesittings
 * @property \App\Model\Table\AnnexesTable $Annexes
 * @property \App\Model\Table\FilesTable $Files
 * @package App\Service
 */
class GenerateSummonsService
{
    use LocatorAwareTrait;
    use ServiceAwareTrait;

    /**
     * @var \App\Model\Table\AttachmentsSummonsTable
     */
    protected $AttachmentsSummons;

    /**
     * @var \App\Model\Table\FilesTable
     */
    protected $Files;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->AttachmentsSummons = $this->fetchTable('AttachmentsSummons');
        $this->Files = $this->fetchTable('Files');
    }

    /**
     * @param string $AttachmentSummonId Identification
     * @return \App\Model\Entity\File|bool
     */
    public function fileGenerateConvocation(int $AttachmentSummonId)
    {
        $attachmentsSummons = $this->AttachmentsSummons
            ->get($AttachmentSummonId, ['contain' => [
                'Files',
                'Summons' => [
                    'Sittings' => [
                        'Typesittings' => ['fields' => ['name']],
                        'fields' => ['date'],
                    ],
                ],
            ]]);
        $sittingId = $attachmentsSummons->summon->sitting_id;

        Log::debug(sprintf('Début de la génération de la convocation pour la séance %d', $sittingId));

        try {
            GenerationerrorsTable::cleanup('convocation', $sittingId);

            $fileName = $this->generateFileName(
                'convocation',
                $attachmentsSummons->summon->sitting->date,
                strval($sittingId),
            );

            $generations = $this->loadService('Generations', [], false);
            $mimetype = 'application/pdf';
            $content = $generations->generateConvocation($attachmentsSummons->summon->sitting_id, $mimetype);
            $data = [
                'name' => $fileName,
                'contents' => $content,
                'size' => strlen((string)$content) * 8,
                'mimetype' => $mimetype,
            ];

            if (empty($attachmentsSummons->Files)) {
                $result = $this->Files->saveInFsNewStream(
                    $data,
                    ['attachment_summon_id' => $attachmentsSummons->id],
                    $attachmentsSummons->id,
                    $attachmentsSummons->summon->structure_id
                );
            } else {
                $result = $this->Files->saveInFsStream(
                    $attachmentsSummons->Files[0]->id,
                    $data,
                    $attachmentsSummons->summon->structure_id
                );
            }
        } catch (\InvalidArgumentException $exc) {
            GenerationerrorsTable::persist('convocation', $sittingId, $exc->getMessage());
            throw $exc;
        } finally {
            // This block will be executed everytime, except for \Cake\Error\FatalErrorException
            $generations = null;
        }

        Log::debug(sprintf('Fin de la génération de la convocation pour la séance %d', $sittingId));

        return $result;
    }

    /**
     * @param string $AttachmentSummonId Identification
     * @return \App\Model\Entity\File|bool
     */
    public function fileGenerateExecutiveSummary(int $AttachmentSummonId)
    {
        $attachmentsSummons = $this->AttachmentsSummons
            ->get($AttachmentSummonId, ['contain' => [
                'Files',
                'Summons' => [
                    'Sittings' => [
                        'Typesittings' => ['fields' => ['name']],
                        'fields' => ['date'],
                    ],
                ],
            ]]);
        $sittingId = $attachmentsSummons->summon->sitting_id;

        Log::debug(sprintf('Début de la génération de la note de synthèse pour la séance %d', $sittingId));

        try {
            GenerationerrorsTable::cleanup('executive_summary', $sittingId);

            $generations = $this->loadService('Generations', [], false);
            $mimetype = 'application/pdf';
            $content = $generations->generateExecutiveSummary($attachmentsSummons->summon->sitting_id, $mimetype);

            $fileName = $this->generateFileName(
                'note-de-synthese',
                $attachmentsSummons->summon->sitting->date,
                strval($sittingId),
            );

            $data = [
                'name' => $fileName,
                'contents' => $content,
                'size' => strlen((string)$content) * 8,
                'mimetype' => $mimetype,
            ];

            if (empty($attachmentsSummons->Files)) {
                return $this->Files->saveInFsNewStream(
                    $data,
                    ['attachment_summon_id' => $attachmentsSummons->id],
                    $attachmentsSummons->id,
                    $attachmentsSummons->summon->structure_id
                );
            } else {
                return $this->Files->saveInFsStream(
                    $attachmentsSummons->Files[0]->id,
                    $data,
                    $attachmentsSummons->summon->structure_id
                );
            }
        } catch (\InvalidArgumentException $exc) {
            GenerationerrorsTable::persist('executive_summary', $sittingId, $exc->getMessage());
            throw $exc;
        } finally {
            // This block will be executed everytime, except for \Cake\Error\FatalErrorException
            $generations = null;
        }

        Log::debug(sprintf('Fin de la génération de la note de synthèse pour la séance %d', $sittingId));
    }

    /**
     * Generate a file name.
     *
     * @param string $prefix Permet de préfixer le nom du fichier
     * @param \DateTimeInterface $date date du la séance
     * @param string $suffix Permet de suffixer le nom du fichier
     * @return string
     */
    public function generateFileName(string $prefix, \DateTimeInterface $date, string $suffix = ''): string
    {
        $formattedDate = RealTimezone::format($date, 'd-m-Y-H\hi');

        return sprintf('%s-%s-%s.pdf', $prefix, $formattedDate, $suffix);
    }
}
