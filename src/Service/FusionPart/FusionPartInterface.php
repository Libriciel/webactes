<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use Libriciel\OfficeClients\Fusion\Type\PartType;

interface FusionPartInterface
{
    /**
     * Crée ou complète un PartType avec les données pour la génération.
     *
     * @param \Libriciel\OfficeClients\Fusion\Type\PartType|null $part Le PartType à compléter
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    public function build(?PartType $part = null): PartType;
}
