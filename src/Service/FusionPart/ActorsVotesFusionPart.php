<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\ActorsVotesTable|\Cake\ORM\Table $ActorsVotes
 */
class ActorsVotesFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected ?int $projectId;
    protected ?int $sittingId;
    protected \App\Utilities\Common\PhpOdtApi $template;
    protected $ActorsVotes;

    /**
     * Les différentes variables concernant les itérations utilisées dans la méthode dataFusionRetrieving
     *
     * @var \string[][]
     */
    protected array $iterations = [
        [
            'iterationName' => 'ActeursContre',
            'prefix' => 'contre',
            'result' => 'no',
            'counter' => 'no_counter',
        ],
        [
            'iterationName' => 'ActeursPour',
            'prefix' => 'pour',
            'result' => 'yes',
            'counter' => 'yes_counter',
        ],
        [
            'iterationName' => 'ActeursAbstention',
            'prefix' => 'abstention',
            'result' => 'abstention',
            'counter' => 'abstentions_counter',
        ],
        [
            'iterationName' => 'ActeursSansParticipation',
            'prefix' => 'sans_participation',
            'result' => 'no_words',
            'counter' => 'no_words_counter',
        ],
    ];

    /**
     * @info: un vote n'est pas associé à une séance car on considère que l'on ne peut voter que dans une seule séance.
     * @see: VotesTable::vote (on n'envoie pas de sitting_id)
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int|null $sittingId L'id de la séance (sittings.id)
     * @param int|null $projectId L'id du projet (projects.id)
     */
    public function __construct(PhpOdtApi $template, ?int $sittingId, ?int $projectId)
    {
        $this->projectId = $projectId;
        $this->sittingId = $sittingId;
        $this->template = $template;

        $this->ActorsVotes = $this->fetchTable('ActorsVotes');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        // @info: il y a potentiellement plusieurs entrées dans la table votes car on enregistre l'historique
        $lastVote = $this->ActorsVotes->Votes
            ->find()
            ->select([
                'id',
                'yes_counter',
                'no_counter',
                'abstentions_counter',
                'no_words_counter',
                'take_act',
                'result',
                'method',
            ])
            ->where([
                'Votes.project_id' => $this->projectId,
            ])
            ->orderDesc('id')
            ->enableHydration(false)
            ->first();

        foreach ($this->iterations as $iteration) {
            $fields = [];

            $variables = [
                'acteur_salutation' => 'civility',
                'acteur_nom' => 'lastname',
                'acteur_prenom' => 'firstname',
                'acteur_email' => 'email',
                'acteur_titre' => 'title',
                'acteur_position' => 'rank',
                'acteur_telmobile' => 'cellphone',
            ];

            // liste des variables présentes dans le modèle d'édition
            foreach ($variables as $variable => $champ) {
                if ($this->template->hasUserFieldDeclared("acteur_{$iteration['prefix']}" . '_' . $variable)) {
                    $fields[] = "Actors.{$champ}";
                } else {
                    unset($variables[$variable]);
                }
            }

            $parts = [];

            if (empty($lastVote) || $lastVote['method'] === 'details') {
                $actorsVotes = $this->ActorsVotes
                    ->find()
                    ->select($fields)
                    ->innerJoinWith('Actors')
                    ->innerJoinWith('Votes')
                    ->where(array_merge(
                        (
                        empty($lastVote) === true
                            ? ['Votes.id IS NULL']
                            : ['Votes.id' => $lastVote['id']]
                        ),
                        ['ActorsVotes.result' => $iteration['result']]
                    ))
                    ->order(['Actors.rank' => 'ASC'])
                    ->enableHydration(false);

                $count = $actorsVotes->count();

                if ($this->template->hasUserFieldDeclared('nombre_acteur_' . $iteration['prefix'])) {
                    $builder->addTextField('nombre_acteur_' . $iteration['prefix'], (string)$count);
                }

                if ($count > 0) {
                    foreach ($actorsVotes as $actorVote) {
                        $iterationBuilder = new FusionBuilder();

                        foreach ($variables as $variable => $field) {
                            $name = "acteur_{$iteration['prefix']}" . '_' . $variable;
                            if ($this->template->hasUserFieldDeclared($name)) {
                                $iterationBuilder->addTextField(
                                    $name,
                                    (string)$actorVote['_matchingData']['Actors'][$field]
                                );
                            }
                        }
                        $parts[] = $iterationBuilder->getPart();
                    }
                } else {
                    $parts[] = $this->getEmpty($iteration['prefix'], $variables);
                }
            } else {
                if ($this->template->hasUserFieldDeclared('nombre_acteur_' . $iteration['prefix'])) {
                    $builder->addTextField(
                        'nombre_acteur_' . $iteration['prefix'],
                        (string)$lastVote[$iteration['counter']]
                    );
                }
                $parts[] = $this->getEmpty($iteration['prefix'], $variables);
            }

            $builder->addIteration($iteration['iterationName'], $parts);
        }

        return $builder->getPart();
    }

    /**
     * @param string $prefix Le préfixe (une valeur parmi "contre", "pour", "abstention", "sans_participation")
     * @param array $variables La liste des suffixes de variables nécessaires pour la fusion
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    protected function getEmpty(string $prefix, array $variables): PartType
    {
        $builder = new FusionBuilder();

        foreach ($variables as $variable => $champ) {
            $builder->addTextField("acteur_{$prefix}" . '_' . $variable, '');
        }

        return $builder->getPart();
    }
}
