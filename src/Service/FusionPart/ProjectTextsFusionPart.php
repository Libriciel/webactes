<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;
use SplFileObject;

/**
 * @property \App\Model\Table\FilesTable|\Cake\ORM\Table $Files
 * @property \App\Model\Table\ProjectTextsTable|\Cake\ORM\Table $ProjectTexts
 */
class ProjectTextsFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected $Files;
    protected int $projectId;
    protected $ProjectTexts;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $projectId L'id du projet (projects.id)
     */
    public function __construct(PhpOdtApi $template, int $projectId)
    {
        $this->projectId = $projectId;
        $this->template = $template;

        $this->Files = $this->fetchTable('Files');
        $this->ProjectTexts = $this->fetchTable('ProjectTexts');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $variables = [
            'TXT_PROJ' => 'texte_projet',
            'TXT_ACT' => 'texte_acte',
        ];

        $builder = new FusionBuilder($part);

        foreach ($variables as $code => $variable) {
            if ($this->template->hasUserField($variable)) {
                $projectTexts = $this->ProjectTexts
                    ->find()
                    ->select(['id'])
                    ->contain(
                        ['DraftTemplates.DraftTemplateTypes' => function (Query $q) use ($code) {
                            return $q->where(['DraftTemplateTypes.code' => $code]);
                        }]
                    )
                    ->where(['project_id' => $this->projectId])
                    ->enableHydration(false)
                    ->first();

                if (empty($projectTexts) === false) {
                    $builder->addOdtContent(
                        $variable,
                        "{$variable}.odt",
                        $this->getProjectTextFileStream($projectTexts['id'])
                    );
                } else {
                    $builder->addOdtContent($variable, "{$variable}.odt", $this->getEmptyProjectTextFileStream());
                }
            } else {
                $builder->addOdtContent($variable, "{$variable}.odt", $this->getEmptyProjectTextFileStream());
            }
        }

        return $builder->getPart();
    }

    /**
     * Retourne le contenu d'un document ODT vide.
     *
     * @return string
     */
    protected function getEmptyProjectTextFileStream(): string
    {
        $splFile = new SplFileObject(CONFIG . 'OdtVide.odt', 'r');

        return $splFile->fread($splFile->getSize());
    }

    /**
     * Retourne le contenu du document ODT.
     *
     * @param int $projectTextId L'id du texte (files.project_text_id)
     * @return string
     */
    protected function getProjectTextFileStream(int $projectTextId): string
    {
        $file = $this->Files
            ->find()
            ->where(['project_text_id' => $projectTextId])
            ->firstOrFail();

        $splFile = new SplFileObject($file->path, 'r');

        return $splFile->fread($splFile->getSize());
    }
}
