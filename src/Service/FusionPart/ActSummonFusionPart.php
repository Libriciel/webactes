<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use App\Utilities\Common\RealTimezone;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\ContainersTable|\Cake\ORM\Table $Containers
 * @property \App\Model\Table\SummonsTable|\Cake\ORM\Table $Summons
 */
class ActSummonFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected int $containerId;
    protected $Containers;
    protected $Summons;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $containerId L'id du conteneur (containers.id)
     */
    public function __construct(PhpOdtApi $template, int $containerId)
    {
        $this->containerId = $containerId;
        $this->template = $template;

        $this->Containers = $this->fetchTable('Containers');
        $this->Summons = $this->fetchTable('Summons');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        if ($this->template->hasUserFieldDeclared('convocation_date_envoi')) {
            $sittingIsDeliberatingId = $this->Containers->getDeliberatingSittingIdByContainerId($this->containerId);
            $dateSend = $sittingIsDeliberatingId !== null
                ? $this->Summons->getConvocationDateBySittingId($sittingIsDeliberatingId)
                : null;

            if ($dateSend instanceof \Cake\I18n\FrozenDate) {
                $builder->addDateField('convocation_date_envoi', RealTimezone::i18nFormat($dateSend, 'dd/MM/Y'));
            } else {
                $builder->addTextField('convocation_date_envoi', '');
            }
        }

        return $builder->getPart();
    }
}
