<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use App\Utilities\Common\RealTimezone;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\ContainersTable|\Cake\ORM\Table $Containers
 */
class ActFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected int $containerId;
    protected $Containers;
    protected ?int $sittingId;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $containerId L'id du conteneur (containers.id)
     * @param int|null $sittingId L'id de la séance (sittings.id)
     */
    public function __construct(PhpOdtApi $template, int $containerId, ?int $sittingId = null)
    {
        $this->containerId = $containerId;
        $this->sittingId = $sittingId;
        $this->template = $template;

        $this->Containers = $this->fetchTable('Containers');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $container = $this->Containers
            ->find()
            ->select(['id', 'project_id'])
            ->contain([
                'Projects' => ['fields' => ['name', 'code_act', 'signature_date', 'created']],
                'Projects.Matieres' => ['fields' => ['libelle']],
            ])
            ->where(['Containers.id' => $this->containerId])
            ->enableHydration(false)
            ->firstOrFail();

        $part = (new ProjectFusionPart($this->template, $this->containerId))->build($part);
        $part = $this->buildAct($container, $part);
        // @info: s'il y a une séance, même si elle n'est pas délibérante (ex: on est tout de même convoqué à une commission)
        $part = (new ActSummonFusionPart($this->template, $this->containerId))->build($part);

        // Est-ce que l'on serait tout de même dans une séance délibérante pour un projet délibérant ?
        if ($this->sittingId === null) {
            $sitting = $this->Containers
                ->find()
                ->select(['Sittings.id'])
                ->innerJoinWith('Typesacts')
                ->innerJoinWith('ContainersSittings')
                ->innerJoinWith('Sittings')
                ->innerJoinWith('Sittings.Typesittings')
                ->where([
                    'Containers.id' => $this->containerId,
                    'Typesacts.isdeliberating' => true,
                    'Typesittings.isdeliberating' => true,
                ])
                ->enableHydration(false)
                ->first();

            if (empty($sitting) === false) {
                $this->sittingId = $sitting['_matchingData']['Sittings']['id'];
            }
        }

        if ($this->sittingId !== null) {
            $part = (new AttendanceListFusionPart($this->template, $this->sittingId, $container['project_id']))
                ->build($part);
            $part = (new VoteFusionPart($this->template, $this->sittingId, $container['project_id']))->build($part);
            $part = (new ActorsVotesFusionPart($this->template, $this->sittingId, $container['project_id']))
                ->build($part);
        } // @info: on aura besoin de ces variables vides seulement pour les modèles de recherche

        return $part;
    }

    /**
     * Complète le PartType avec les données du container.
     *
     * @param array $container Les données du container
     * @param \Libriciel\OfficeClients\Fusion\Type\PartType|null $part Le PartType à compléter
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    protected function buildAct(array $container, ?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        if ($this->template->hasUserFieldDeclared('numero_acte')) {
            $builder->addTextField('numero_acte', (string)$container['project']['code_act']);
        }

        if ($this->template->hasUserFieldDeclared('acte_signature')) {
            $builder->addTextField(
                'acte_signature',
                empty($container['project']['signature_date']) === false ? '1' : '0'
            );
        }

        if ($this->template->hasUserFieldDeclared('acte_signature_date')) {
            $builder->addDateField(
                'acte_signature_date',
                empty($container['project']['signature_date']) === false
                    ? RealTimezone::format($container['project']['signature_date'], 'd/m/Y')
                    : ''
            );
        }

        return $builder->getPart();
    }
}
