<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\ContainersTable|\Cake\ORM\Table $Containers
 */
class TypeactFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected int $containerId;
    protected $Containers;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $containerId L'id du conteneur (containers.id)
     */
    public function __construct(PhpOdtApi $template, int $containerId)
    {
        $this->containerId = $containerId;
        $this->template = $template;

        $this->Containers = $this->fetchTable('Containers');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        if ($this->template->hasUserFieldDeclared('type_acte')) {
            $container = $this->Containers
                ->find()
                ->contain(['Typesacts' => ['fields' => ['name']]])
                ->where(['Containers.id' => $this->containerId])
                ->enableHydration(false)
                ->firstOrFail();
            $builder->addTextField('type_acte', $container['typesact']['name']);
        }

        return $builder->getPart();
    }
}
