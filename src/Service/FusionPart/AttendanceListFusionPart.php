<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\ActorsProjectsSittingsTable|\Cake\ORM\Table $ActorsProjectsSittings
 */
class AttendanceListFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected $ActorsProjectsSittings;
    protected int $projectId;
    protected int $sittingId;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $sittingId L'id de la séance (sittings.id)
     * @param int $projectId L'id du projet (projects.id)
     */
    public function __construct(PhpOdtApi $template, int $sittingId, int $projectId)
    {
        $this->projectId = $projectId;
        $this->sittingId = $sittingId;
        $this->template = $template;

        $this->ActorsProjectsSittings = $this->fetchTable('ActorsProjectsSittings');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $part = $this->buildActeurPresent($part);
        $part = $this->buildActeurMandataire($part);

        return $this->buildActeurAbsent($part);
    }

    /**
     * @param \Libriciel\OfficeClients\Fusion\Type\PartType|null $part Le PartType à compléter
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    protected function buildActeurPresent(?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        $actorsProjectsSittings = $this->ActorsProjectsSittings
            ->find()
            ->where([
                'is_present' => true,
                'mandataire_id IS' => null,
                'sitting_id' => $this->sittingId,
                'project_id' => $this->projectId,
            ])
            ->enableHydration(false)
            ->all();

        if ($this->template->hasUserFieldDeclared('acteur_present_nombre')) {
            $builder->addTextField('acteur_present_nombre', (string)$actorsProjectsSittings->count());
        }

        $parts = [];
        if ($actorsProjectsSittings->count() > 0) {
            foreach ($actorsProjectsSittings as $actorProjectSitting) {
                $parts[] = (new ActorFusionPart($this->template, $actorProjectSitting['actor_id'], 'acteur_present'))
                    ->build();
            }
        } else {
            $parts[] = (new ActorFusionPart($this->template, null, 'acteur_present'))->build();
        }

        $builder->addIteration('ActeursPresents', $parts);

        return $builder->getPart();
    }

    /**
     * @param \Libriciel\OfficeClients\Fusion\Type\PartType|null $part Le PartType à compléter
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    protected function buildActeurMandataire(?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        $actorsProjectsSittings = $this->ActorsProjectsSittings
            ->find()
            ->where([
                'is_present' => false,
                'mandataire_id IS NOT' => null,
                'sitting_id' => $this->sittingId,
                'project_id' => $this->projectId,
            ])
            ->enableHydration(false)
            ->all();

        if ($this->template->hasUserFieldDeclared('acteur_mandataire_nombre')) {
            $builder->addTextField('acteur_mandataire_nombre', (string)$actorsProjectsSittings->count());
        }

        $parts = [];
        if ($actorsProjectsSittings->count() > 0) {
            foreach ($actorsProjectsSittings as $actorProjectSitting) {
                $tmp = (new ActorFusionPart($this->template, $actorProjectSitting['actor_id'], 'acteur_mandant'))
                    ->build();
                $parts[] = (new ActorFusionPart(
                    $this->template,
                    $actorProjectSitting['mandataire_id'],
                    'acteur_mandataire'
                )
                )->build($tmp);
            }
        } else {
            $tmp = (new ActorFusionPart($this->template, null, 'acteur_mandant'))->build();
            $parts[] = (new ActorFusionPart($this->template, null, 'acteur_mandataire'))->build($tmp);
        }

        $builder->addIteration('ActeursMandataires', $parts);

        return $builder->getPart();
    }

    /**
     * @param \Libriciel\OfficeClients\Fusion\Type\PartType|null $part Le PartType à compléter
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    protected function buildActeurAbsent(?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        $actorsProjectsSittings = $this->ActorsProjectsSittings
            ->find()
            ->where([
                'is_present' => false,
                'mandataire_id IS' => null,
                'sitting_id' => $this->sittingId,
                'project_id' => $this->projectId,
            ])
            ->enableHydration(false)
            ->all();

        if ($this->template->hasUserFieldDeclared('acteur_absent_nombre')) {
            $builder->addTextField('acteur_absent_nombre', (string)$actorsProjectsSittings->count());
        }

        $parts = [];
        if ($actorsProjectsSittings->count() > 0) {
            foreach ($actorsProjectsSittings as $actorProjectSitting) {
                $parts[] = (new ActorFusionPart($this->template, $actorProjectSitting['actor_id'], 'acteur_absent'))
                    ->build();
            }
        } else {
            $parts[] = (new ActorFusionPart($this->template, null, 'acteur_absent'))->build();
        }

        $builder->addIteration('ActeursAbsents', $parts);

        return $builder->getPart();
    }
}
