<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

/**
 * Cette classe permet de récupérer les variables pour la fusion pour (suivant le préfixe):
 * - président·e de séance (president_seance)
 * - secrétaire de séance (secretaire_seance)
 * - président·e de vote (president)
 *
 * @package App\Service\Generates
 */
class RepresentativeFusionPart extends AbstractActorFusionPart
{
    /**
     * Liste des variables et alias utilisées dans cette classe.
     *
     * @var array
     */
    protected array $variables = [
        'salutation' => 'civility',
        'nom' => 'lastname',
        'prenom' => 'firstname',
        'email' => 'email',
        'titre' => 'title',
        'telmobile' => 'cellphone',
        'adresse1' => 'address',
        'adresse2' => 'address_supplement',
        'cp' => 'post_code',
        'ville' => 'city',
        'telfixe' => 'phone',
    ];
}
