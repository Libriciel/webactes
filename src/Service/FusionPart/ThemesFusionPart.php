<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Hash;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\ThemesTable|\Cake\ORM\Table $Themes
 */
class ThemesFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected int $projectId;
    protected $Themes;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $projectId L'id du projet (projects.id)
     */
    public function __construct(PhpOdtApi $template, int $projectId)
    {
        $this->projectId = $projectId;
        $this->template = $template;

        $this->Themes = $this->fetchTable('Themes');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $theme = $this->Themes
            ->find()
            ->select(['id', 'name', 'position', 'lft', 'rght'])
            ->innerJoinWith('Projects')
            ->where(['Projects.id' => $this->projectId])
            ->enableHydration(false)
            ->first();

        if (empty($theme) === false) {
            $themes = $this->Themes
                ->find('all')
                ->select(['name'])
                ->where(['lft <=' => $theme['lft'], 'rght >=' => $theme['rght']])
                ->orderAsc('lft')
                ->enableHydration(false)
                ->toArray();
        } else {
            $theme = [];
            $themes = [];
        }

        $builder = new FusionBuilder($part);

        if ($this->template->hasUserFieldDeclared('theme')) {
            $builder->addTextField('theme', (string)Hash::get($theme, 'name'));
        }
        if ($this->template->hasUserFieldDeclared('theme_critere_trie')) {
            $builder->addTextField('theme_critere_trie', (string)Hash::get($theme, 'position'));
        }

        // 10 level max
        for ($idx = 0; $idx < 10; $idx++) {
            $value = (string)Hash::get($themes, "{$idx}.name");
            if (Hash::check($themes, "{$idx}.name") === true) {
                $builder->addTextField('T' . ($idx + 1) . '_theme', $value);
            }
            $builder->addTextField('theme' . ($idx + 1), $value);
        }

        return $builder->getPart();
    }
}
