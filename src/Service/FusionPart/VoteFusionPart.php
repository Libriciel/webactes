<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\VotesTable|\Cake\ORM\Table $Votes
 */
class VoteFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected int $projectId;
    protected int $sittingId;
    protected \App\Utilities\Common\PhpOdtApi $template;
    protected $Votes;

    /**
     * @info: un vote n'est pas associé à une séance car on considère que l'on ne peut voter que dans une seule séance.
     * @see: VotesTable::vote (on n'envoie pas de sitting_id)
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $sittingId L'id de la séance (sittings.id)
     * @param int $projectId L'id du projet (projects.id)
     */
    public function __construct(PhpOdtApi $template, int $sittingId, int $projectId)
    {
        $this->projectId = $projectId;
        $this->sittingId = $sittingId;
        $this->template = $template;

        $this->Votes = $this->fetchTable('Votes');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        $vote = $this->Votes
            ->find()
            ->select(['comment', 'president_id'])
            ->where(['Votes.project_id' => $this->projectId])
            ->orderDesc('Votes.id')
            ->enableHydration(false)
            ->first();

        if (empty($vote)) {
            return $builder->getPart();
        }

        $result = $this->Votes->result($this->projectId);

        if ($this->template->hasUserFieldDeclared('vote_type')) {
            $builder->addTextField(
                'vote_type',
                array_key_exists($result['method'], $this->Votes->correspondances)
                    ? $this->Votes->correspondances[$result['method']]
                    : $result['method']
            );
        }

        if ($this->template->hasUserFieldDeclared('acte_adopte') && isset($result['approved'])) {
            $builder->addTextField('acte_adopte', $result['approved'] === true ? '1' : '0');
        }

        if ($this->template->hasUserFieldDeclared('prendre_acte') && isset($result['take_act'])) {
            $builder->addTextField('prendre_acte', (string)$result['take_act']);
        }

        if ($this->template->hasUserFieldDeclared('nombre_pour') && isset($result['yes_counter'])) {
            $builder->addTextField('nombre_pour', (string)$result['yes_counter']);
        }

        if ($this->template->hasUserFieldDeclared('nombre_abstention') && isset($result['abstentions_counter'])) {
            $builder->addTextField('nombre_abstention', (string)$result['abstentions_counter']);
        }

        if ($this->template->hasUserFieldDeclared('nombre_contre') && isset($result['no_counter'])) {
            $builder->addTextField('nombre_contre', (string)$result['no_counter']);
        }

        if (
            $this->template->hasUserFieldDeclared('nombre_sans_participation')
            && isset($result['no_words_counter'])
        ) {
            $builder->addTextField('nombre_sans_participation', (string)$result['no_words_counter']);
        }

        if ($this->template->hasUserFieldDeclared('nombre_votant') && isset($result['vote_counter'])) {
            $builder->addTextField('nombre_votant', (string)$result['vote_counter']);
        }

        if ($this->template->hasUserFieldDeclared('commentaire_vote')) {
            $builder->addTextField('commentaire_vote', $vote['comment'] ?? '');
        }

        $sitting = $this->fetchTable('Sittings')
            ->find()
            ->select(['secretary_id','president_id'])
            ->where(['Sittings.id' => $this->sittingId])
            ->enableHydration(false)
            ->firstOrFail();

        $part = (new RepresentativeFusionPart($this->template, $vote['president_id'], 'president_vote'))
            ->build($builder->getPart());

        $part = (new RepresentativeFusionPart($this->template, $sitting['president_id'], 'president_seance'))
            ->build($part);

        return (new RepresentativeFusionPart($this->template, $sitting['secretary_id'], 'secretaire_seance'))
            ->build($part);
    }
}
