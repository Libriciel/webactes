<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;
use Psr\Log\InvalidArgumentException;
use SplFileObject;

/**
 * @property \App\Model\Table\AnnexesTable|\Cake\ORM\Table $Annexes
 * @property \App\Model\Table\FilesTable|\Cake\ORM\Table $Files
 */
class AnnexesFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected $Annexes;
    protected $Files;
    protected int $containerId;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $containerId L'id du conteneur (containers.id)
     */
    public function __construct(PhpOdtApi $template, int $containerId)
    {
        $this->containerId = $containerId;
        $this->template = $template;

        $this->Annexes = $this->fetchTable('Annexes');
        $this->Files = $this->fetchTable('Files');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $sectionAnnexes = array_filter(
            $this->template->getSections(),
            function ($value) {
                return preg_match('/^Annexes|^Annexes_.*([0-9])/', $value);
            },
            ARRAY_FILTER_USE_BOTH
        );

        $countAnnexes = $this->Annexes
            ->find()
            ->where([
                'container_id' => $this->containerId,
                'current' => true,
                'is_generate' => true,
            ])
            ->count();

        $builder = new FusionBuilder($part);

        if ($this->template->hasUserFieldDeclared('annexes_nombre')) {
            $builder->addTextField('annexes_nombre', (string)$countAnnexes);
        }

        foreach ($sectionAnnexes as $sectionName) {
            $parts = [];

            $suffixe = '';
            if ($sectionName != 'Annexes' && strpos($sectionName, '_')) {
                $suffixe = substr($sectionName, strpos($sectionName, '_'), strlen($sectionName));
            }

            if (empty($countAnnexes)) {
                //Gedooo a besoin de "fields" non null pour correctement effectuer la fusion.
                $parts[] = $this->getEmptyPart($suffixe);
            } else {
                //Récupération des champs pour le select
                $fields = $this->getListVariablesForFusion($suffixe);
                if (!empty($fields)) {
                    $annexes = $this->getAnnexesForProject($this->containerId, $fields + ['path' => 'path']);
                    // fusion des variables pour chaque annexe
                    foreach ($annexes as $annexe) {
                        $sectionBuilder = new FusionBuilder();

                        if (in_array('name', $fields, true)) {
                            $sectionBuilder->addTextField('nom_fichier' . $suffixe, $annexe['name']);
                        }

                        if (file_exists($annexe['path'])) {
                            $file = new SplFileObject($annexe['path'], 'r');
                        } else {
                            $message = sprintf('Lecture du fichier impossible: %s', $annexe['path']);
                            throw new InvalidArgumentException($message, 404);
                        }

                        if (file_exists($file->getPath() . DS . 'odt' . DS . $file->getFilename())) {
                            $file = new SplFileObject($file->getPath() . DS . 'odt' . DS . $file->getFilename(), 'r');
                            $sectionBuilder->addOdtContent(
                                'fichier' . $suffixe,
                                'fichier' . $suffixe . '.odt',
                                (string)$file->fread($file->getSize())
                            );
                        } else {
                            $message = sprintf(
                                'Toutes les annexes du projet de container.id "%d" ne sont pas encore converties. '
                                . 'Veuillez réessayer dans quelques instants ...',
                                $this->containerId
                            );
                            throw new InvalidArgumentException($message, 449);
                        }
                        $parts[] = $sectionBuilder->getPart();
                    }
                } else {
                    $parts[] = $this->getEmptyPart($suffixe);
                }
            }

            $builder->addIteration($sectionName, $parts);
        }

        return $builder->getPart();
    }

    /**
     * Retourne une TypePart dont toutes les champs sont vides.
     *
     * @param string $suffixe Le suffixe à utiliser
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    protected function getEmptyPart($suffixe = ''): PartType
    {
        $builder = new FusionBuilder();

        if ($this->template->hasUserFieldDeclared('nom_fichier' . $suffixe)) {
            $builder->addTextField('nom_fichier' . $suffixe, '');
        }
        if ($this->template->hasUserFieldDeclared('fichier' . $suffixe)) {
            $splFile = new SplFileObject(CONFIG . 'OdtVide.odt', 'r');
            $builder->addOdtContent(
                'fichier' . $suffixe,
                'fichier' . $suffixe . '.odt',
                (string)$splFile->fread($splFile->getSize())
            );
        }

        return $builder->getPart();
    }

    /**
     * Retourne les annexes à joindre à la fusion pour un projet donné.
     *
     * @param int $projectId L'id du projet (projects.id)
     * @param array $fields Les champs à récupérer
     * @return \Cake\ORM\Query
     */
    protected function getAnnexesForProject(int $projectId, array $fields): Query
    {
        return $this->Files
            ->find()
            ->select($fields)
            ->innerJoinWith('Annexes', function (Query $q) use ($projectId) {
                return $q->innerJoinWith('Containers', function (Query $q) use ($projectId) {
                    return $q->where(['Containers.project_id' => $projectId]);
                });
            })
            ->where(['current' => true, 'is_generate' => true])
            ->orderAsc('rank')
            ->enableHydration(false);
    }

    /**
     * Liste des variables de fusion utilisées dans le template
     *
     * @param string $suffixe Le suffixe à utiliser
     * @return array
     */
    protected function getListVariablesForFusion($suffixe = ''): array
    {
        $fields = [];
        if ($this->template->hasUserFieldDeclared('nom_fichier' . $suffixe)) {
            $fields[] = 'name';
        }
        if ($this->template->hasUserField('fichier' . $suffixe)) {
            $fields[] = 'path';
        }
        if ($this->template->hasUserFieldDeclared('nombre_annexe')) {
            $fields[] = 'id';
        }

        return $fields;
    }
}
