<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\DateLetters;
use App\Utilities\Common\PhpOdtApi;
use App\Utilities\Common\RealTimezone;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\SittingsTable|\Cake\ORM\Table $Sittings
 */
class ProjectSittingsFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected int $containerId;
    protected $Sittings;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $containerId L'id du conteneur (containers.id)
     */
    public function __construct(PhpOdtApi $template, int $containerId)
    {
        $this->containerId = $containerId;
        $this->template = $template;

        $this->Sittings = $this->fetchTable('Sittings');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        $builder = $this->buildAllSittings($builder);
        $builder = $this->buildDeliberatingSitting($builder);

        return $builder->getPart();
    }

    /**
     * Retourne le contenu d'une itération de la section Seances pour une séance donnée.
     *
     * @param array|null $sitting La séance à insérer dans la section Seances
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    protected function buildAllSittingsSittingPart(?array $sitting): PartType
    {
        $sittingBuilder = new FusionBuilder();

        if (!is_null($sitting)) {
            if ($this->template->hasUserFieldDeclared('seances_date')) {
                $sittingBuilder->addDateField(
                    'seances_date',
                    $sitting['date'] ? RealTimezone::format($sitting['date'], 'd/m/Y') : ''
                );
            }
            if ($this->template->hasUserFieldDeclared('seances_date_lettres')) {
                $sittingBuilder->addTextField(
                    'seances_date_lettres',
                    $sitting['date'] ? DateLetters::format($sitting['date']) : ''
                );
            }
            if ($this->template->hasUserFieldDeclared('seances_hh')) {
                $sittingBuilder->addTextField(
                    'seances_hh',
                    $sitting['date'] ? RealTimezone::i18nFormat($sitting['date'], 'HH') : ''
                );
            }
            if ($this->template->hasUserFieldDeclared('seances_mm')) {
                $sittingBuilder->addTextField(
                    'seances_mm',
                    $sitting['date'] ? RealTimezone::i18nFormat($sitting['date'], 'mm') : ''
                );
            }
            if ($this->template->hasUserFieldDeclared('seances_position')) {
                $sittingBuilder->addTextField(
                    'seances_position',
                    $sitting['_matchingData']['ContainersSittings']['rank']
                        ? (string)$sitting['_matchingData']['ContainersSittings']['rank']
                        : ''
                );
            }
            if ($this->template->hasUserFieldDeclared('seances_type')) {
                $sittingBuilder->addTextField(
                    'seances_type',
                    $sitting['typesitting']['name'] ? $sitting['typesitting']['name'] : ''
                );
            }
            if ($this->template->hasUserFieldDeclared('seances_date_convocation')) {
                if ($sitting['date_convocation'] && $sitting['date_convocation'] instanceof \Cake\I18n\FrozenTime) {
                    $sittingBuilder->addDateField(
                        'seances_date_convocation',
                        RealTimezone::i18nFormat($sitting['date_convocation'], 'dd/MM/Y')
                    );
                } else {
                    $sittingBuilder->addTextField('seances_date_convocation', '');
                }
            }
            if ($this->template->hasUserFieldDeclared('seances_lieu')) {
                $sittingBuilder->addStringField('seances_lieu', $sitting['place'] ?? '');
            }
        }

        return $sittingBuilder->getPart();
    }

    /**
     * Complète le builder avec les données de toutes les séances.
     *
     * @param \Libriciel\OfficeClients\Fusion\Helper\Builder $builder Le builder
     * @return \Libriciel\OfficeClients\Fusion\Helper\Builder
     */
    protected function buildAllSittings(FusionBuilder $builder): FusionBuilder
    {
        $sittings = $this->Sittings
            ->find('all')
            ->select(['date', 'date_convocation', 'Typesittings.name', 'place' ,'ContainersSittings.rank'])
            ->contain(['Typesittings'])
            ->innerJoinWith('Containers')
            ->where(['Containers.id' => $this->containerId])
            ->orderAsc('date', true)
            ->enableHydration(false);

        $builder->addTextField('seances_nombre', (string)$sittings->count());

        $parts = [];
        if (!$sittings->isEmpty()) {
            foreach ($sittings as $sitting) {
                $parts[] = $this->buildAllSittingsSittingPart($sitting);
            }
        } else {
            $parts[] = $this->buildAllSittingsSittingPart(null);
        }

        $builder->addIteration('Seances', $parts);

        return $builder;
    }

    /**
     * Complète le builder avec les données de la séance délibérante.
     *
     * @param \Libriciel\OfficeClients\Fusion\Helper\Builder $builder Le builder
     * @return \Libriciel\OfficeClients\Fusion\Helper\Builder
     */
    protected function buildDeliberatingSitting(FusionBuilder $builder): FusionBuilder
    {
        $sitting = $this->Sittings
            ->find()
            ->select(['id', 'date','date_convocation', 'place', 'Typesittings.name', 'ContainersSittings.rank'])
            ->matching('ContainersSittings')
            ->matching('Typesittings')
            ->where([
                'Typesittings.isdeliberating' => true,
                'ContainersSittings.container_id' => $this->containerId,
            ])
            ->first();

        if ($this->template->hasUserFieldDeclared('seance_deliberante_date')) {
            $builder->addDateField(
                'seance_deliberante_date',
                !empty($sitting->date) ? RealTimezone::format($sitting->date, 'd/m/Y') : ''
            );
        }
        if ($this->template->hasUserFieldDeclared('seance_deliberante_type')) {
            $builder->addTextField('seance_deliberante_type', $sitting->_matchingData['Typesittings']->name ?? '');
        }
        if ($this->template->hasUserFieldDeclared('seance_deliberante_position')) {
            $builder->addTextField(
                'seance_deliberante_position',
                (string)($sitting->_matchingData['ContainersSittings']->rank ?? '')
            );
        }

        if ($this->template->hasUserFieldDeclared('seance_deliberante_date_convocation')) {
            if (!empty($sitting->date) && $sitting->date_convocation instanceof \Cake\I18n\FrozenTime) {
                $builder->addDateField(
                    'seance_deliberante_date_convocation',
                    RealTimezone::i18nFormat($sitting->date_convocation, 'dd/MM/Y')
                );
            } else {
                $builder->addTextField('seance_deliberante_date_convocation', '');
            }
        }

        if ($this->template->hasUserFieldDeclared('seance_deliberante_lieu')) {
            $builder->addStringField('seance_deliberante_lieu', $sitting['place'] ?? '');
        }

        return $builder;
    }
}
