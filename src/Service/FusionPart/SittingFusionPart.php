<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\DateLetters;
use App\Utilities\Common\PhpOdtApi;
use App\Utilities\Common\RealTimezone;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\SittingsTable|\Cake\ORM\Table $Sittings
 */
class SittingFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected int $sittingId;
    protected $Sittings;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $sittingId L'identifiant de la séance (sittings.id)
     */
    public function __construct(PhpOdtApi $template, int $sittingId)
    {
        $this->sittingId = $sittingId;
        $this->template = $template;

        $this->Sittings = $this->fetchTable('Sittings');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        $sitting = $this->Sittings
            ->find()
            ->select(['date', 'date_convocation', 'president_id', 'secretary_id', 'place'])
            ->contain([
                'Typesittings' => [
                    'fields' => ['name'],
                ],
            ])
            ->where(['Sittings.id' => $this->sittingId])
            ->enableHydration(false)
            ->firstOrFail();

        if ($this->template->hasUserFieldDeclared('seance_date')) {
            $builder->addDateField('seance_date', RealTimezone::format($sitting['date'], 'd/m/Y'));
        }

        if ($this->template->hasUserFieldDeclared('seance_date_lettres')) {
            $builder->addTextField('seance_date_lettres', DateLetters::format($sitting['date']));
        }

        if ($this->template->hasUserFieldDeclared('seance_hh')) {
            $builder->addTextField('seance_hh', RealTimezone::i18nFormat($sitting['date'], 'HH'));
        }

        if ($this->template->hasUserFieldDeclared('seance_mm')) {
            $builder->addTextField('seance_mm', RealTimezone::i18nFormat($sitting['date'], 'mm'));
        }

        if ($this->template->hasUserFieldDeclared('seance_type')) {
            $builder->addTextField('seance_type', $sitting['typesitting']['name']);
        }

        if ($this->template->hasUserFieldDeclared('seance_lieu')) {
            $builder->addStringField('seance_lieu', $sitting['place'] ?? '');
        }

        if ($this->template->hasUserFieldDeclared('seance_date_convocation')) {
            if ($sitting['date_convocation'] instanceof \Cake\I18n\FrozenTime) {
                $builder->addDateField(
                    'seance_date_convocation',
                    RealTimezone::i18nFormat($sitting['date_convocation'], 'dd/MM/Y')
                );
            } else {
                $builder->addTextField('seance_date_convocation', '');
            }
        }

        $part = (new RepresentativeFusionPart($this->template, $sitting['president_id'], 'president_seance'))
            ->build($builder->getPart());
        $part = (new RepresentativeFusionPart($this->template, $sitting['secretary_id'], 'secretaire_seance'))
            ->build($part);

        return (new SittingsActsFusionPart($this->template, $this->sittingId))->build($part);
    }
}
