<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * Class abstraite concernant les Actors, utilisée dans les classes ActorsFusionPart et RepresentativesFusionPart.
 * Permet de simpifier ces dernières en renseignant uniquement l'attribut $variables.
 *
 * @property \App\Model\Table\VotesTable|\Cake\ORM\Table $Actors
 */
abstract class AbstractActorFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected ?int $actorId;
    protected $Actors;
    protected ?string $prefix;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * Liste des variables et alias à renseigner dans les classes filles.
     *
     * @var array
     */
    protected array $variables = [];

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int|null $actorId l'id de l'acteur (actors.id)
     * @param string $prefix Le préfixe à utiliser
     */
    public function __construct(PhpOdtApi $template, ?int $actorId, ?string $prefix = null)
    {
        $this->actorId = $actorId;
        $this->prefix = $prefix;
        $this->template = $template;

        $this->Actors = $this->fetchTable('Actors');
    }

    /**
     * Retourne la liste des variables nécessaires au PhpOdtApi, sous forme
     * (prefix_)variable(_suffix) => champ aliasé.
     *
     * @param array $variables Le préfixe des noms de variables, si nécessaire.
     * @param string $prefix Le préfixe des noms de variables, si nécessaire.
     * @param string $suffix Le suffixe des noms de variables, si nécessaire.
     * @param string $alias L'alias de la table, si nécessaire.
     * @return array
     */
    protected function getFiltered(
        array $variables,
        ?string $prefix = null,
        ?string $suffix = null,
        ?string $alias = null
    ) {
        $prefix = empty($prefix) === false ? $prefix . '_' : '';
        $suffix = empty($suffix) === false ? '_' . $suffix : '';
        $alias = empty($alias) === false ? $alias . '.' : '';
        $return = [];

        foreach ($variables as $variable => $field) {
            $key = $prefix . $variable . $suffix;
            if ($this->template->hasUserFieldDeclared($key)) {
                $return[$key] = $alias . $field;
            }
        }

        return $return;
    }

    /**
     * Retourne un champ texte dont la valeur est vide, pour chacune des variables demandées.
     *
     * @param array $variables La liste sous le forme variable => champ, déjà filtrée.
     * @param \Libriciel\OfficeClients\Fusion\Type\PartType|null $part Le PartType à compléter
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    protected function getEmpty(array $variables, ?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        foreach (array_keys($variables) as $target) {
            $builder->addTextField($target, '');
        }

        return $builder->getPart();
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        $variables = $this->getFiltered($this->variables, $this->prefix);

        if (empty($variables)) {
            return $builder->getPart();
        }

        if ($this->actorId === null) {
            return $this->getEmpty($variables, $part);
        }

        $actor = $this->Actors
            ->find()
            ->select(array_values($variables) + ['id' => 'id'])
            ->where(['id' => $this->actorId])
            ->enableHydration(false)
            ->firstOrFail();

        foreach ($variables as $variable => $champ) {
            $builder->addTextField($variable, (string)$actor[$champ]);
        }

        return $builder->getPart();
    }
}
