<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

class ActorFusionPart extends AbstractActorFusionPart
{
    /**
     * Liste des variables et alias utilisées dans cette classe.
     *
     * @var array
     */
    protected array $variables = [
        'acteur_salutation' => 'civility',
        'acteur_nom' => 'lastname',
        'acteur_prenom' => 'firstname',
        'acteur_email' => 'email',
        'acteur_titre' => 'title',
        'acteur_position' => 'rank',
        'acteur_telmobile' => 'cellphone',
    ];
}
