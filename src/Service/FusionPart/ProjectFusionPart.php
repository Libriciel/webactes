<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use App\Utilities\Common\RealTimezone;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\ContainersTable|\Cake\ORM\Table $Containers
 */
class ProjectFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected int $containerId;
    protected $Containers;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $containerId L'id du conteneur (containers.id)
     */
    public function __construct(PhpOdtApi $template, int $containerId)
    {
        $this->containerId = $containerId;
        $this->template = $template;

        $this->Containers = $this->fetchTable('Containers');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $container = $this->Containers
            ->find()
            ->select(['id', 'project_id'])
            ->contain([
                'Projects' => ['fields' => ['id', 'name', 'created']],
                'Projects.Matieres' => ['fields' => ['libelle']],
            ])
            ->where(['Containers.id' => $this->containerId])
            ->enableHydration(false)
            ->firstOrFail();

        $part = $this->buildProject($container, $part);
        $part = (new ThemesFusionPart($this->template, $container['project_id']))->build($part);
        $part = (new ProjectTextsFusionPart($this->template, $container['project_id']))->build($part);
        $part = (new AnnexesFusionPart($this->template, $container['id']))->build($part);
        $part = (new TypeactFusionPart($this->template, $container['id']))->build($part);

        return (new ProjectSittingsFusionPart($this->template, $container['id']))->build($part);
    }

    /**
     * Complète le PartType avec les données du projet.
     *
     * @param array $container Les données du conteneur
     * @param \Libriciel\OfficeClients\Fusion\Type\PartType|null $part Le PartType à compléter
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    protected function buildProject(array $container, ?PartType $part = null): PartType
    {
        $builder = new FusionBuilder($part);

        if ($this->template->hasUserFieldDeclared('libelle')) {
            $builder->addStringField('libelle', (string)$container['project']['name']);
        }
        if ($this->template->hasUserFieldDeclared('classification')) {
            $builder->addTextField('classification', $container['project']['matiere']['libelle'] ?? '');
        }
        if ($this->template->hasUserFieldDeclared('date_creation')) {
            $builder->addDateField('date_creation', RealTimezone::format($container['project']['created'], 'd/m/Y'));
        }

        return $builder->getPart();
    }
}
