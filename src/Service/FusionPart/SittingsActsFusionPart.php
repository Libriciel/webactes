<?php
declare(strict_types=1);

namespace App\Service\FusionPart;

use App\Utilities\Common\PhpOdtApi;
use Cake\ORM\Locator\LocatorAwareTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @property \App\Model\Table\ContainersSittingsTable|\Cake\ORM\Table $Sittings
 */
class SittingsActsFusionPart implements FusionPartInterface
{
    use LocatorAwareTrait;

    protected $ContainersSittings;
    protected int $sittingId;
    protected \App\Utilities\Common\PhpOdtApi $template;

    /**
     * @param \App\Utilities\Common\PhpOdtApi $template Le template utilisé
     * @param int $sittingId L'identifiant de la séance (sittings.id)
     */
    public function __construct(PhpOdtApi $template, int $sittingId)
    {
        $this->sittingId = $sittingId;
        $this->template = $template;

        $this->ContainersSittings = $this->fetchTable('ContainersSittings');
    }

    /**
     * @inheritDoc
     */
    public function build(?PartType $part = null): PartType
    {
        $containersSittings = $this->ContainersSittings
            ->find()
            ->select(['container_id'])
            ->where(['sitting_id' => $this->sittingId])
            ->order(['rank' => 'ASC'])
            ->enableHydration(false)
            ->all();

        $parts = [];
        foreach ($containersSittings as $containerSitting) {
            $parts[] = (new ActFusionPart($this->template, $containerSitting['container_id'], $this->sittingId))
                ->build();
        }

        $builder = new FusionBuilder($part);
        $builder->addIteration('Projets', $parts);

        return $builder->getPart();
    }
}
