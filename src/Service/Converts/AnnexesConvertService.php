<?php
declare(strict_types=1);

namespace App\Service\Converts;

use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;

/**
 * Class AnnexesConvertService
 *
 * @property \App\Model\Table\AnnexesTable $Annexes
 * @property \App\Model\Table\FilesTable $Files
 * @package App\Service\Generates
 */
class AnnexesConvertService
{
    use LocatorAwareTrait;
    use ServiceAwareTrait;

    /**
     * @var \App\Model\Table\AnnexesTable|\Cake\Datasource\RepositoryInterface
     */
    private $Annexes;
    /**
     * @var int
     */
    private $id;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->Annexes = $this->fetchTable('Annexes');
        $this->Files = $this->fetchTable('Files');
    }

    /**
     * @param array $ids Id list of annex
     * @return array
     */
    public function getAnnexesPath($ids): array
    {
        return $this->Files->find('all')
            ->select(['id','path', 'name', 'mimetype'])
            ->innerJoinWith('Annexes', function (Query $q) {
                return $q->where(['Annexes.is_generate' => true]);
            })
            ->where(['annex_id IN' => $ids])->toArray();
    }
}
