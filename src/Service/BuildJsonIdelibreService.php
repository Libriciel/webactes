<?php
declare(strict_types=1);

namespace App\Service;

use App\Utilities\Common\RealTimezone;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Hash;
use InvalidArgumentException;
use SplFileObject;

/**
 * Class BuildJsonIdelibreService
 *
 * @property \App\Model\Table\SittingsTable $Sittings
 * @property \App\Model\Table\ContainersSittingsTable $containersSittings
 * @property \App\Model\Table\TypesittingsTable $Typesittings
 * @property \App\Model\Table\AnnexesTable $Annexes
 * @property \App\Model\Table\FilesTable $Files
 * @property \App\Model\Table\SummonsTable $Summons
 * @property \App\Model\Table\AttachmentsSummonsTable $attachmentsSummons
 * @property \App\Model\Table\ThemesTable $Themes
 * @property \App\Service\FoldersService $Folders
 * @property \App\Service\GeneratesService $Generates
 * @package App\Service
 */
class BuildJsonIdelibreService
{
    use LocatorAwareTrait;
    use ServiceAwareTrait;

    private $json;
    private int $id;
    private object $Folders;

    /**
     * Constructor
     *
     * @param int $id Foo
     */
    public function __construct(int $id)
    {
        $this->id = $id;
        $this->Folders = $this->loadService('Folders', ['sendIdelibre']);

        self::initializeService();
    }

    /**
     * Initialize Service
     *
     * @return void
     */
    private function initializeService()
    {
        $this->Sittings = $this->fetchTable('Sittings');
        $this->Typesittings = $this->fetchTable('Typesittings');
        $this->Annexes = $this->fetchTable('Annexes');
        $this->Files = $this->fetchTable('Files');
        $this->Summons = $this->fetchTable('Summons');
        $this->Themes = $this->fetchTable('Themes');
    }

    /**
     * @return array
     */
    private function build()
    {
        $sitting = $this->Sittings->find()
            ->contain(
                [
                    'Typesittings',
                    'ContainersSittings' => function ($q) {
                        return $q->order(['rank' => 'ASC']);
                    },
                    'ContainersSittings.Containers',
                    'ContainersSittings.Containers.Projects',
                    'ContainersSittings.Containers.Projects.Themes' => [
                        'conditions' => ['active' => true],
                    ],
                    'Summons' => [
                        'conditions' => ['summon_type_id' => 2],
                    ],
                ]
            )
            ->where(['Sittings.id' => $this->id])->firstOrFail();

        $data = [];
        $acteurs_convoques = $this->actorSummonByTypeSitting($sitting->get('typesitting_id'));
        $jsonData = [
            'date_seance' => RealTimezone::format($sitting['date'], 'Y-m-d H:i'),
            'type_seance' => Hash::get($sitting->get('typesitting'), 'name'),
            'acteurs_convoques' => json_encode($acteurs_convoques),
        ];

        $summons = Hash::extract($sitting->get('summons'), '{n}.id');

        if (!$summons) {
            $dataSummon = [
                'summon_type_id' => 2,
                'sitting_id' => $this->id,
                'structure_id' => $sitting->get('structure_id'),
                'is_sent_individually' => false,
                'attachments_summons' => [
                    [
                        'rank' => 1,
                        'attachment' => [
                            'structure_id' => $sitting->get('structure_id'),
                            'name' => 'convocation',
                        ],
                    ],
                ],
            ];

            $summon = $this->Summons->newEntity($dataSummon, [
                'associated' => [
                    'AttachmentsSummons',
                    'AttachmentsSummons.Attachments',
                ],
            ]);
            if (
                $this->Summons->save($summon, [
                'associated' => ['AttachmentsSummons','AttachmentsSummons.Attachments'],
                ])
            ) {
                $summonId = $summon->id;
                // Génération de la convocation
                $this->loadService('GenerateSummons');
                $this->GenerateSummons->fileGenerateConvocation($summon->attachments_summons[0]->id);
            }
        } else {
            $summonId = $summons[0];
        }

        $fileSummon = $this->Files
            ->find()
            ->innerJoinWith('AttachmentsSummons')
            ->innerJoinWith('AttachmentsSummons.Summons')
            ->contain('AttachmentsSummons.Attachments')
            ->where(['Summons.id' => (int)$summonId])
            ->first();
        if (empty($fileSummon)) {
            $message = sprintf('Impossible de générer la convocation pour la séance %d', $this->id);
            throw new InvalidArgumentException($message, 449);
        }

        $data['convocation'] = curl_file_create(
            $fileSummon['path'],
            'application/pdf',
            $fileSummon['attachments_summon']['attachment']['name'] . '.pdf'
        );

        $i = 0;
        foreach ($sitting->get('containers_sittings') as $containers_sittings) {
            $project = $containers_sittings->container->project;
            $projectGenerated = $this->generateProject($project['id']);

            $folderTmp = $this->Folders->setNewTmpFolder($summonId . DS . $project['id']);
            $fileGenerated = new SplFileObject(
                $folderTmp . DS . $project['id'] . '.pdf',
                'w+'
            );
            $fileGenerated->fwrite($projectGenerated['content']);

            $data['projet_' . $i . '_rapport'] = curl_file_create(
                $fileGenerated->getPathname(),
                'application/pdf',
                $fileGenerated->getFilename()
            );

            $projet = [
                'ordre' => $i,
                'libelle' => $project['name'],
                //'etat' => $project['etat'],// A faire
                'theme' => $this->themeByThemeId((int)$project['theme_id']),
//                'Rapporteur' => [
//                    'rapporteurlastname' => 'GUILLAUME',// A faire
//                     'rapporteurfirstname' => 'Didier',// A faire
//                ],
                //'theme' => $this->themeListByThemeId($project->get('theme_id')),
            ];

            //Annexe
            $annexesToSend = [];
            $annexes = $this->Annexes->find('all')
                ->where(['container_id' => $containers_sittings['container']->id])
                ->orderAsc('rank');
            $j = 0;
            foreach ($annexes as $annex) {
                $file = $this->Files->find()->where(['annex_id' => $annex->id])->firstOrFail()->toArray();
                $data['projet_' . $i . '_' . $j . '_annexe'] = curl_file_create(
                    $file['path'],
                    'application/pdf',
                    $file['name']
                );
                $annexesToSend[] = [
                    'libelle' => $file['name'],
                    'ordre' => $j,
                ];
                $j++;
            }

            $projet['annexes'] = $annexesToSend;
            $jsonData['projets'][] = $projet;
            $i++;
        }
        // Encodage en json
        $data['jsonData'] = json_encode($jsonData);

        return $data;
    }

    /**
     * @return array Foo
     */
    public function getJson(): array
    {
        return self::build();
    }

    /**
     * @param int $id Foo
     * @return array Foo
     */
    private function actorSummonByTypeSitting(int $id): array
    {
        $acteursSummons = [];
        $typesitting = $this->Typesittings->find()
            ->contain(
                [
                    'ActorGroups' => [
                        'conditions' => ['ActorGroups.active' => true],
                    ],
                    'ActorGroups.Actors' => function ($q) {
                        return $q
                            ->where(['Actors.active' => true]);
                    },
                ]
            )
            ->where(['Typesittings.id' => $id])->firstOrFail()->toArray();

        $acteurs = Hash::extract($typesitting, 'actorGroups.{n}.actors.{n}');
        foreach ($acteurs as $acteur) {
            $acteursSummons[] = [
                'Acteur' => [
                    'nom' => $acteur['lastname'],
                    'prenom' => $acteur['firstname'],
                    'salutation' => $acteur['civility'],
                    'titre' => $acteur['title'],
                    'email' => $acteur['email'],
                    'telmobile' => $acteur['cellphone'],
                    'position' => $acteur['rank'],
                    'actif' => true,
                    'suppleant_id' => null,
                ],
            ];
        }

        return $acteursSummons;
    }

    /**
     * @param int $id Foo
     * @return array File
     * @throws \Exception
     */
    private function generateProject(int $id): array
    {
        $generations = $this->loadService('Generations', [], false);
        $content = $generations->generateProject($id, 'application/pdf');
        // @see https://stackoverflow.com/a/11367034
        $size = strlen($content) * 8;

        return compact('content', 'size');
    }

    /**
     * @return void
     */
    public function deleteSend(): void
    {
        $this->Folders->delete();
    }

    private function themeByThemeId(int $theme_id): ?string
    {
        if (empty($theme_id)) {
            return '';
        }
        $parents = $this->Themes->find('path', ['for' => $theme_id])->toArray();
        $themeParents = Hash::extract($parents, '{n}.name');

        return implode(',', $themeParents);
    }
}
