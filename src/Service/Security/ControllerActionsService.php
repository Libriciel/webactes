<?php
declare(strict_types=1);

namespace App\Service\Security;

use Cake\Controller\Controller;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Filesystem\Folder;
use Cake\Log\Log;
use Cake\Utility\Hash;
use ReflectionClass;

/**
 * ATTENTION
 *  - ne gère pas les contrôleurs se trouvant dans les plugin (ça tombe bien, on n'en a pas au 29/05/2024).
 *  - ne gère pas les préfixes (ça tombe bien, on n'en a pas au 29/05/2024).
 *
 * Fortement inspiré par https://github.com/cakephp/acl/blob/527bee49285cb6ea84201b903054d292131c3269/src/AclExtras.php.
 */
class ControllerActionsService
{
    /**
     * La liste des contrôleurs à ne pas prendre en compte.
     *
     * Rechercher aussi Auth->allow() dans les contrôleurs.
     *
     * @var string[]
     */
    protected static $hiddenControllers = [
        'controllers/Error',
        'controllers/Generates',
        'controllers/Logs',
        'controllers/Pages',
        'controllers/Socles',
        'controllers/Wopi',
    ];

    /**
     * La liste des actions à ne pas prendre en compte.
     *
     * Rechercher aussi Auth->allow() dans les contrôleurs.
     *
     * @var string[]
     */
    protected static $hiddenActions = [
        'applyConfigureIniSettings',
        'controllers/Users/loginStructure',
        'controllers/Users/logout',
        'controllers/Users/notLogged',
        'getConfigureIniSettings',
        'getServiceLocator',
        'handleFatalErrorMessage',
        'isAuthorized',
        'loadService',
        'persistFatalErrorMessage',
        'setServiceLocator',
    ];

    protected static function getControllers(): array
    {
        $path = App::classPath('Controller');

        return (new Folder($path[0]))->find('.*Controller\.php');
    }

    protected static function getNamespace($className)
    {
        $namespace = preg_replace('/(.*)Controller\//', '', $className);
        $namespace = preg_replace('/\//', '\\', $namespace);
        $namespace = preg_replace('/\.php/', '', $namespace);
        $rootNamespace = Configure::read('App.namespace');
        $namespace = [$rootNamespace, 'Controller', $namespace];

        return implode('\\', Hash::filter($namespace));
    }

    protected static function getControllersPaths($rootNode, array $controllers)
    {
        // look at each controller
        $result = [];
        foreach ($controllers as $controller) {
            $tmp = explode('/', $controller);
            $controllerName = str_replace('Controller.php', '', array_pop($tmp));
            // Always skip the App controller
            if ($controllerName === 'App') {
                continue;
            }
            // Skip anything that is not a concrete controller
            $namespace = static::getNamespace($controller);
            if (!(new ReflectionClass($namespace))->isInstantiable()) {
                continue;
            }

            if (!in_array("{$rootNode}/{$controllerName}", static::$hiddenControllers)) {
                $result = array_merge($result, static::getMethods($controller, $controllerName, $rootNode));
            }
        }

        return $result;
    }

    protected static function getCallbacks($className)
    {
        $callbacks = [];
        $namespace = static::getNamespace($className);
        $reflection = new ReflectionClass($namespace);
        if ($reflection->isAbstract()) {
            return $callbacks;
        }
        try {
            $method = $reflection->getMethod('implementedEvents');
        } catch (\ReflectionException $e) {
            return $callbacks;
        }
        if (version_compare(phpversion(), '5.4', '>=')) {
            $object = $reflection->newInstanceWithoutConstructor();
        } else {
            $object = unserialize(sprintf('O:%d:"%s":0:{}', strlen($className), $className));
        }
        $implementedEvents = $method->invoke($object);
        foreach ($implementedEvents as $event => $callable) {
            if (is_string($callable)) {
                $callbacks[] = $callable;
            }
            if (is_array($callable) && isset($callable['callable'])) {
                $callbacks[] = $callable['callable'];
            }
        }

        return $callbacks;
    }

    protected static function getMethods($className, $controllerName, $rootNode)
    {
        $result = [];

        $excludes = static::getCallbacks($className);
        $baseMethods = get_class_methods(new Controller());
        $namespace = static::getNamespace($className);
        $methods = get_class_methods($namespace);
        if ($methods == null) {
            Log::error(__d('cake_acl', 'Unable to get methods for {0}', $className));

            return false;
        }
        $actions = array_diff($methods, $baseMethods);
        $actions = array_diff($actions, $excludes);
        foreach ($actions as $key => $action) {
            if (strpos($action, '_', 0) === 0) {
                continue;
            }
            $path = [
                $rootNode,
                $controllerName,
                $action,
            ];
            $path = implode('/', Hash::filter($path));
            if (!in_array($action, static::$hiddenActions) && !in_array($path, static::$hiddenActions)) {
                $result[] = $path;
            }
        }

        return $result;
    }

    /**
     * Retourne la liste des actions des contrôleurs (ex: controllers/Users/index).
     *
     * Attention: ni les plugins, ni les préfixes ne sont pris en compte.
     *
     * @return array
     */
    public static function getControllerActions(): array
    {
        $controllers = static::getControllers();
        sort($controllers);

        $result = static::getControllersPaths('controllers', $controllers);
        sort($result);

        return $result;
    }
}
