<?php
declare(strict_types=1);

namespace App\Service\Security;

use App\Model\Enum\RolesName;
use Cake\Http\ServerRequest;

class PermissionsService
{
    /**
     * Vérifie si le rôle de l'utilisateur a bien accès à la ressource pointée par la requête.
     *
     * @param string $role Le nom du rôle
     * @param \Cake\Http\ServerRequest $request La requête à vérifier
     * @return bool
     */
    public static function isAuthorized(string $role, ServerRequest $request): bool
    {
        $keys = [
            'controllers',
            'controllers/' . $request->getParam('controller'),
            'controllers/' . $request->getParam('controller') . '/' . $request->getParam('action'),
        ];

        $permissions = static::getByRole($role);

        return !empty(array_intersect($keys, $permissions));
    }

    /**
     * @return array
     */
    public static function getByRole(string $role): array
    {
        switch ($role) {
            case RolesName::ADMINISTRATOR:
                return static::getAdministrator();
            case RolesName::FUNCTIONAL_ADMINISTRATOR:
                return static::getFunctionalAdministrator();
            case RolesName::VALIDATOR_WRITER:
                return static::getValidatorWriter();
            case RolesName::GENERAL_SECRETARIAT:
                return static::getGeneralSecretariat();
            case RolesName::SUPER_ADMINISTRATOR:
                return static::getSuperAdministrator();
        }

        return [];
    }

    /**
     * @return array
     */
    protected static function getSuperAdministrator(): array
    {
        $superAdminRights = ['controllers/Users/getAllOrganizationUsers'];

        return array_merge(static::getAdministrator(), $superAdminRights);
    }

    /**
     * @return array
     */
    protected static function getAdministrator(): array
    {
        return [
            'controllers/Users',
            'controllers/Actors',
            'controllers/ActorGroups',
            'controllers/Structures',
            'controllers/StructureSettings',
            'controllers/Sittings',
            'controllers/Acts',
            'controllers/Projects',
            'controllers/Typesacts',
            'controllers/Roles',
            'controllers/Files',
            'controllers/Workflows',
            'controllers/WorkflowsTasks',
            'controllers/Dpos',
            'controllers/Services',
            'controllers/Stateacts',
            'controllers/Themes',
            'controllers/Themes/active',
            'controllers/Typesittings',
            'controllers/Typespiecesjointes',
            'controllers/Logs',
            'controllers/Matieres/getMatieresToStructure',
            'controllers/Natures/index',
            'controllers/Natures/view',
            'controllers/NotificationsUsers/activate',
            'controllers/NotificationsUsers/deactivate',
            'controllers/NotificationsUsers/index',
            'controllers/NotificationsUsers/view',
            'controllers/Officials',
            'controllers/Organizations',
            'controllers/Signatures',
            'controllers/Summons',
            'controllers/Teletransmissions',
            'controllers/Counters',
            'controllers/Sequences',
            'controllers/Votes',
            'controllers/Generates',
            'controllers/GenerateTemplates',
            'controllers/GenerateTemplateTypes',
            'controllers/DraftTemplates',
            'controllers/DraftTemplateTypes',
            'controllers/Search',
            'controllers/ActorsProjectsSittings',
            'controllers/ContainersSittings',
        ];
    }

    /**
     * @return array
     */
    protected static function getFunctionalAdministrator(): array
    {
        return [
            'controllers/Users',
            'controllers/Actors',
            'controllers/ActorGroups',
            'controllers/Structures',
            'controllers/Sittings',
            'controllers/Projects',
            'controllers/Acts',
            'controllers/Typesacts',
            'controllers/Typesittings/index',
            'controllers/Stateacts',
            'controllers/Themes/index',
            'controllers/Files/view',
            'controllers/Matieres/getMatieresToStructure',
            'controllers/Natures/index',
            'controllers/Natures/view',
            'controllers/NotificationsUsers/activate',
            'controllers/NotificationsUsers/deactivate',
            'controllers/NotificationsUsers/index',
            'controllers/NotificationsUsers/view',
            'controllers/Officials',
            'controllers/Typespiecesjointes',
            'controllers/Workflows',
            'controllers/WorkflowsTasks',
            'controllers/Dpos',
            'controllers/Signatures',
            'controllers/Summons',
            'controllers/Teletransmissions',
            'controllers/Votes',
            'controllers/Generates',
            'controllers/GenerateTemplates',
            'controllers/GenerateTemplateTypes',
            'controllers/DraftTemplates',
            'controllers/DraftTemplateTypes',
            'controllers/Search',
            'controllers/ActorsProjectsSittings',
            'controllers/ContainersSittings',
        ];
    }

    /**
     * @return array
     */
    protected static function getGeneralSecretariat(): array
    {
        return [
            'controllers/Users',
            'controllers/Actors',
            'controllers/Structures',
            'controllers/Sittings',
            'controllers/Projects',
            'controllers/Acts',
            'controllers/Typesacts',
            'controllers/Typesittings/index',
            'controllers/Stateacts',
            'controllers/Themes/index',
            'controllers/Files/view',
            'controllers/Matieres/getMatieresToStructure',
            'controllers/Natures/index',
            'controllers/Natures/view',
            'controllers/NotificationsUsers/activate',
            'controllers/NotificationsUsers/deactivate',
            'controllers/NotificationsUsers/index',
            'controllers/NotificationsUsers/view',
            'controllers/Officials',
            'controllers/Typespiecesjointes',
            'controllers/Workflows',
            'controllers/WorkflowsTasks',
            'controllers/Dpos',
            'controllers/Signatures',
            'controllers/Summons',
            'controllers/Teletransmissions',
            'controllers/Votes',
            'controllers/Generates',
            'controllers/Search',
            'controllers/ActorsProjectsSittings',
            'controllers/ContainersSittings',
        ];
    }

    /**
     * @return array
     */
    protected static function getValidatorWriter(): array
    {
        return [
            'controllers/Projects/indexAct',
            'controllers/Projects/indexDrafts',
            'controllers/Projects/indexValidating',
            'controllers/Projects/indexToValidate',
            'controllers/Projects/indexValidated',
            'controllers/Projects/indexToTransmit',
            'controllers/Projects/indexReadyToTransmit',
            'controllers/Projects/indexUnassociated',
            'controllers/Projects/getMainMenu',
            'controllers/Acts/checkCode',
            //        'controllers/Projects/manualSignature',
            'controllers/Signatures/getStatusIParapheur',
            'controllers/Teletransmissions',
            'controllers/Projects/view',
            'controllers/Projects/add',
            'controllers/Projects/edit',
            'controllers/Projects/delete',
            'controllers/Projects/sendToCircuit',
            'controllers/Themes/index',
            'controllers/Matieres/getMatieresToStructure',
            'controllers/Natures/index',
            'controllers/Natures/view',
            'controllers/NotificationsUsers/activate',
            'controllers/NotificationsUsers/deactivate',
            'controllers/NotificationsUsers/index',
            'controllers/NotificationsUsers/view',
            'controllers/Officials',
            'controllers/Stateacts',
            'controllers/Typesittings/index',
            'controllers/Files/view',
            'controllers/Workflows/active',
            'controllers/Workflows/view',
            'controllers/WorkflowsTasks/allTasksByCandidateUser',
            'controllers/WorkflowsTasks/approveAction',
            'controllers/WorkflowsTasks/rejectAction',
            'controllers/WorkflowsTasks/executeActionForward',
            'controllers/Structures/getTypesPiecesJointes',
            'controllers/Dpos',
            'controllers/Typesacts',
            'controllers/Generates',
            'controllers/Search',
            'controllers/ContainersSittings',
            'controllers/Sittings/index',
        ];
    }
}
