<?php
/**
 * @codingStandardsIgnoreFile
 * @SuppressWarnings(PHPMD)
 */

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use App\Model\Entity\Auth;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\UnauthorizedException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;

define("TIME_TO_CHECK", 60);

class KeycloakMiddleware implements MiddlewareInterface
{

    const ALLOWED_ROUTES = [
        "/api/v1/users/userSuffix",
        "/api/v1/wopi",
    ];

    const MYEC3_PATTERN = '/^\/api\/v1\/sync-myec3\/.*/';

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return mixed
     */
    public function process(
        ServerRequestInterface  $request,
        RequestHandlerInterface $handler
    ): ResponseInterface
    {

        if (Configure::read('KEYCLOAK_DEV_MODE')) {
            return $handler->handle($request);
        }

        if ($this->isAllowedRoutes($request->getRequestTarget())) {
            return $handler->handle($request);
        }


        $bearer = $request->getHeaderLine('Authorization', null);

        if (empty($bearer)) {
            $matches = null;
            //myec3
            preg_match(self::MYEC3_PATTERN, $request->getRequestTarget(), $matches);

            $ips = isset($request->getServerParams()['HTTP_X_FORWARDED_FOR'])
                ? array_values(array_filter(explode(', ', trim($request->getServerParams()['HTTP_X_FORWARDED_FOR']))))
                : [];

            if (empty($ips)) {
                $ips[0] = isset($request->getServerParams()['X_REAL_IP']) ? $request->getServerParams()['X_REAL_IP'] : null;
            }

            if (count($matches) === 1) {
                if (
                    (!empty($request->getServerParams()['HTTP_X_FORWARDED_FOR']) || !empty($request->getServerParams()['X_REAL_IP']))
                    && MYEC3_IP_ALLOWED !== $ips[0]) {
                    throw new ForbiddenException('IP not allowed: ' . $ips[0]);
                }
                return $handler->handle($request);
            }
        }

        if (empty($bearer) && isset($request->getQueryParams()['access_token'])) {
            $bearer = urldecode($request->getQueryParams()['access_token']);
        }

        if (empty($bearer)) {
            throw new UnauthorizedException("no token"); //401
        }
        $responseCheck = $this->checkToken($bearer);
        if (empty($responseCheck)) {
            throw new UnauthorizedException("bad token");
        }

        $session = $request->getAttribute('session');
        $session->write("keycloak_id", $responseCheck['sub']);
        $session->write("username", $responseCheck[KEYCLOAK_USERNAME]);

        return $handler->handle($request);
    }


    private function isAllowedRoutes(string $route): bool
    {
        foreach (self::ALLOWED_ROUTES as $allowedRoute) {
            if (stripos($route, $allowedRoute) !== false) {
                return true;
            }
        }
        return false;
    }

    function checkToken($bearer)
    {
        $token = str_replace(['bearer ', 'Bearer '], '', $bearer);

        return $this->checkOnline($token);
    }

    function checkOffline($jwt)
    {
        try {
            $decoded = JWT::decode($jwt, new Key(KEY, 'HS256'));
        } catch (SignatureInvalidException $exception) { // bad secret
            throw new UnauthorizedException("token bad secret");
        } catch (ExpiredException $exception) { // too late
            throw new UnauthorizedException("token too late");
        } catch (BeforeValidException $exception) { // too early
            throw new UnauthorizedException("token too early");
        }

        $decodedArray = (array)$decoded;

        if (isset($decodedArray["checkUntil"])) {
            if ($decodedArray["checkUntil"] - time() <= 0) {
                $this->checkOnline($jwt);
            } else {
                // set when to check
                $decodedArray["checkUntil"] = time() + TIME_TO_CHECK;
                $newToken = JWT::encode($decodedArray, KEY, 'HS256');

                return $newToken;
            }
        }

        return null;
    }

    /**
     * @param string $token token
     * @return bool
     */
    function checkOnline($token)
    {
        $auth = new Auth();
        $res = $auth->verifyToken($token);
        if (empty($res["success"])) {
            return false;
        }
        return $res['data'];
    }
}
