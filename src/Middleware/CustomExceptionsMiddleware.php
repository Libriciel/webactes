<?php
declare(strict_types=1);

namespace App\Middleware;

use App\Exception\BadRequestGetParametersException;
use App\Exception\GenerationException;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Cette classe permet de renvoyer certaines exceptions telles que BadRequestGetParametersException sous forme de
 * réponse au format JSON.
 */
class CustomExceptionsMiddleware implements MiddlewareInterface
{
    /**
     * Traitement de la requête et retour d'un JSON suite à un BadRequestGetParametersException le cas échéant.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request La requête
     * @param \Psr\Http\Server\RequestHandlerInterface $handler La réponse
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        try {
            return $handler->handle($request);
        } catch (BadRequestGetParametersException $exc) {
            return new JsonResponse(['errors' => $exc->getErrors()], $exc->getCode());
        } catch (GenerationException $exc) {
            $response = new JsonResponse(['message' => $exc->getMessage()]);

            return $response->withStatus($exc->getCode() ?? 400, $exc->getMessage());
        }
    }
}
