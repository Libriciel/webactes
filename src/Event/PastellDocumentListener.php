<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: mpastor
 * Date: 12/07/19
 * Time: 10:07
 */

namespace App\Event;

use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;

class PastellDocumentListener implements EventListenerInterface
{
    /**
     * @return array Associative array or event key names pointing to the function
     * that should be called in the object when the respective event is fired
     */
    public function implementedEvents(): array
    {
        return [
            'Model.Project.afterSaveState' => 'createDocumentOnPastell',
            'Model.Project.afterSaveEdit.PatchPastell' => 'createDocumentOnPastell',
            'Model.Project.before.sendParapheurAgain' => 'createDocumentOnPastellAndFiredSendParapheur',
        ];
    }

    /**
     * Trigger "send-tdt" action
     *
     * @param \Cake\Event\Event $event Evènement lancé
     * @return mixed Beanstalk id when succeed Exception otherwise.
     */
    public function createDocumentOnPastell(Event $event)
    {
        $beanstalk = new Beanstalk('basic-pastell');
        $data = [
            'flux' => Configure::read('Flux.actes-generique.name'),
            'flux_name' => Configure::read('Flux.actes-generique.name'),
            'controller' => 'containers',
            'object_id' => $event->getData('id_d'),
            'structure_id' => $event->getData('structureId'),
            'organization_id' => $event->getData('organizationId'),
            'action' => 'prepareDocumentOnPastell',
        ];

        return $beanstalk->emit($data);
    }

    /**
     * Trigger "send-tdt" action
     *
     * @param \Cake\Event\Event $event Evènement lancé
     * @return mixed Beanstalk id when succeed Exception otherwise.
     */
    public function createDocumentOnPastellAndFiredSendParapheur(Event $event)
    {
        $beanstalk = new Beanstalk('basic-pastell');
        $data = [
            'flux' => Configure::read('Flux.actes-generique.name'),
            'flux_name' => Configure::read('Flux.actes-generique.name'),
            'controller' => 'containers',
            'object_id' => $event->getData('id_d'),
            'structure_id' => $event->getData('structureId'),
            'organization_id' => $event->getData('organizationId'),
            'action' => 'prepareDocumentOnPastell',
            'user_id' => $event->getData('user_id'),
            'sousTypeParapheur' => $event->getData('sous-type-parapheur')['iparapheur_circuit_name'],
        ];

        return $beanstalk->emit($data, Configure::read('Beanstalk.Priority.VERY_HIGHT'));
    }
}
