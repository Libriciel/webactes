<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: mpastor
 * Date: 12/07/19
 * Time: 10:07
 */

namespace App\Event;

use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;

class PastellTdtListener implements EventListenerInterface
{
    /**
     * Returns a list of events this object is implementing. When the class is registered
     * in an event manager, each individual method will be associated with the respective event.
     *
     * ### Example:
     *
     * ```
     *  public function implementedEvents()
     *  {
     *      return [
     *          'Order.complete' => 'sendEmail',
     *          'Article.afterBuy' => 'decrementInventory',
     *          'User.onRegister' => ['callable' => 'logRegistration', 'priority' => 20, 'passParams' => true]
     *      ];
     *  }
     * ```
     *
     * @return array Associative array or event key names pointing to the function
     * that should be called in the object when the respective event is fired
     */
    public function implementedEvents(): array
    {
        return [
            'Model.Container.afterSavePastellFlux' => 'sendTdt',
        ];
    }

    /**
     * Trigger "send-tdt" action
     *
     * @param \Cake\Event\Event $event Evènement lancé
     * @return mixed Beanstalk id when succeed Exception otherwise.
     */
    public function sendTdt(Event $event)
    {
        $beanstalk = new Beanstalk('tdt-pastell');
        $data = [
            'flux' => Configure::read('Flux.actes-generique.name'),
            'controller' => 'containers',
            'object_id' => $event->getData('id_d'),
            'structure_id' => $event->getData('structureId'),
            'organization_id' => $event->getData('organizationId'),
            'pastellFlux_id' => $event->getData('pastellFluxId'),
            'action' => 'sendTdt',
        ];
        $beanstalk->emit($data, Configure::read('Beanstalk.Priority.HIGHT'));
    }
}
