<?php
declare(strict_types=1);

namespace App\Event;

use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;
use Pheanstalk\PheanstalkInterface;

class GenerateListener implements EventListenerInterface
{
    use LocatorAwareTrait;

    /**
     * @return array Associative array or event key names pointing to the function
     * that should be called in the object when the respective event is fired
     */
    public function implementedEvents(): array
    {
        return [
            'Model.Project.afterSave.generateProject' => 'generateProject',
            'Model.Project.afterSave.generateAct' => 'generateAct',
            'Model.Annexe.afterSave.convertFile' => 'convertFileAnnex',
            'Model.Annexe.afterSave.deleteFile' => 'deleteFileAnnex',
        ];
    }

    /**
     * Trigger "send email" action
     *
     * @param \Cake\Event\Event $event Evènement lancé
     * @return mixed Beanstalk id when succeed Exception otherwise.
     */
    public function generateProject(Event $event)
    {
        Log::write('info', self::class . '*******' . __FUNCTION__ . '*******', 'Generates');

        Log::write(
            'info',
            'Préparation de la génération pour : ' . $event->getData('project_id')
        );

        $beanstalk = new Beanstalk('file-generates');
        $data = [
            'project_id' => $event->getData('project_id'),
            'action' => 'generateProject',
        ];

        return $beanstalk->emit($data, Configure::read('Beanstalk.Priority.HIGHT'), 30);
    }

    /**
     * Trigger "send email" action
     *
     * @param \Cake\Event\Event $event Evènement lancé
     * @return mixed Beanstalk id when succeed Exception otherwise.
     */
    public function generateAct(Event $event)
    {
        Log::write('info', self::class . '*******' . __FUNCTION__ . '*******', 'Generates');

        Log::write(
            'info',
            'Préparation de la génération pour : ' . $event->getData('container_id')
        );

        $beanstalk = new Beanstalk('file-generates');
        $data = [
            'container_id' => $event->getData('container_id'),
            'action' => 'generateAct',
        ];

        $containers = $this->fetchTable('Containers');
        $container = $containers->get($event->getData('container_id'));

        if ($container->generation_job_id != null) {
            //Retourne une exception si le job n'existe plus.
            try {
                Log::info('Le job  : ' . $container->generation_job_id);

                //Suppression du job dans la queue
                $beanstalk->selectJob($container->generation_job_id);
                $beanstalk->getPheanstalk()->delete($beanstalk->getJob());
                $Jobs = $this->fetchTable($beanstalk->params['table_jobs']);
                $Jobs->deleteAll(['jobid' => $container->generation_job_id]);
            } catch (\Exception $e) {
                Log::info($e->getMessage(), 'debug');
                Log::info('Le job a été consommé : ' . $container->generation_job_id, 'debug');
            }
        }
        $jobId = $beanstalk->emit($data, Configure::read('Beanstalk.Priority.HIGHT'));

        $containers->patchEntity($container, ['generation_job_id' => $jobId]);

        $containers->save($container);

        return $jobId;
    }

    /**
     * Trigger "send email" action
     *
     * @param \Cake\Event\Event $event Evènement lancé
     * @return mixed Beanstalk id when succeed Exception otherwise.
     */
    public function convertFileAnnex(Event $event)
    {
        Log::write('info', self::class . '*******' . __FUNCTION__ . '*******', 'ConvertFileAnnex');

        Log::write(
            'info',
            'Conversion pour la génération des annexes : ' . implode(',', $event->getData('ids'))
        );

        $beanstalk = new Beanstalk('file-converts');
        $data = [
            'ids' => $event->getData('ids'),
            'action' => 'convertFileAnnex',
        ];

        return $beanstalk->emit(
            $data,
            Configure::read('Beanstalk.Priority.VERY_HIGHT'),
            PheanstalkInterface::DEFAULT_DELAY,
            (int)env('PDF2ODT_BEANSTALK_TTR', 60 * 60 * 4) // 4 heures par défaut
        );
    }

    /**
     * Trigger "send email" action
     *
     * @param \Cake\Event\Event $event Evènement lancé
     * @return mixed Beanstalk id when succeed Exception otherwise.
     */
    public function deleteFileAnnex(Event $event)
    {
        Log::write('info', self::class . '*******' . __FUNCTION__ . '*******', 'DeleteFileAnnex');

        Log::write(
            'info',
            'Suppression des conversions des annexes : ' . implode(',', $event->getData('ids'))
        );

        $beanstalk = new Beanstalk('file-deletes');
        $data = [
            'ids' => $event->getData('ids'),
            'action' => 'deleteFileAnnex',
        ];

        return $beanstalk->emit($data, Configure::read('Beanstalk.Priority.VERY_HIGHT'));
    }
}
