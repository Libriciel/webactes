<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: mpastor
 * Date: 12/07/19
 * Time: 10:07
 */

namespace App\Event;

use App\Model\Entity\Stateact;
use Beanstalk\Utility\Beanstalk;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;

class NotificationsListener implements EventListenerInterface
{
    /**
     * @return array Associative array or event key names pointing to the function
     * that should be called in the object when the respective event is fired
     */
    public function implementedEvents(): array
    {
        return [
            'Model.Project.afterSaveState.Email.' . Stateact::VALIDATION_PENDING => 'validationToDo',
            'Model.Project.afterSaveState.Email.' . 'myProjectAtState' => 'myProjectAtState',
        ];
    }

    /**
     * Trigger "send email" action
     *
     * @param \Cake\Event\Event $event Evènement lancé
     * @return mixed Beanstalk id when succeed Exception otherwise.
     */
    public function validationToDo(Event $event)
    {
        Log::write('info', self::class . '*******' . __FUNCTION__ . '*******', 'Notifications');

        Log::write(
            'info',
            'Préparation de la notifications pour : '
            . $event->getData('user_id') . 'de la strucutre : '
            . $event->getData('structureId'),
            'Notifications'
        );

        $beanstalk = new Beanstalk('email-notifications');
        $data = [
            'object_id' => $event->getData('object_id'),
            'user_id' => $event->getData('user_id'),
            'last_user_id' => $event->getData('last_user_id'),
            'libelle' => $event->getData('libelle'),
            'comment' => $event->getData('comment') ?? null,
            'structure_id' => $event->getData('structureId'),
            'type' => 'email',
            'action' => 'validationToDo',
        ];

        return $beanstalk->emit($data);
    }

    /**
     * Trigger "send email" action
     *
     * @param \Cake\Event\Event $event Evènement lancé
     * @return mixed Beanstalk id when succeed Exception otherwise.
     */
    public function myProjectAtState(Event $event)
    {
        Log::write('info', self::class . '*******' . __FUNCTION__ . '*******', 'Notifications');

        Log::write(
            'info',
            'Préparation de la notifications pour : '
            . $event->getData('user_id') . 'de la strucutre : '
            . $event->getData('structureId'),
            'Notifications'
        );

        $beanstalk = new Beanstalk('email-notifications');
        $data = [
            'object_id' => $event->getData('object_id'),
            'object_state_code' => $event->getData('object_state_code'),
            'user_id' => $event->getData('user_id'),
            'last_user_id' => $event->getData('last_user_id'),
            'libelle' => $event->getData('libelle'),
            'comment' => $event->getData('comment') ?? null,
            'structure_id' => $event->getData('structureId'),
            'type' => 'email',
            'notification_name' => $event->getData('notification_name'),
            'isMine' => $event->getData('isMine'),
            'isTdt' => $event->getData('isTdt'),
            'action' => 'myProjectAtState',
        ];

        return $beanstalk->emit($data);
    }
}
