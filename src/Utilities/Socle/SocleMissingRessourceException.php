<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace App\Utilities\Socle;

use Cake\Log\Log;

/**
 * Exception raised when a particular record was not found
 */
class SocleMissingRessourceException extends SocleException
{
    /**
     * SocleMissingRessourceException constructor.
     *
     * @param string $resource   resource
     * @param string $resourceId resourceId
     * @param string $methodType method
     */
    public function __construct($resource, $resourceId, $methodType)
    {
        Log::info('RESSOURCE_MISSING', 'EXCEPTION_SOCLE');

        parent::__construct(
            $resource,
            '003',
            'RESOURCE_MISSING',
            'La ressource spécifiée est introuvable',
            $methodType,
            $resourceId,
            $code = 404,
            $previous = null
        );
    }
}
