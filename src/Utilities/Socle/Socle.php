<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 09/10/19
 * Time: 10:35
 */

namespace App\Utilities\Socle;

use App\Model\Table\StructuresTable;
use App\Model\Table\UsersTable;
use App\Utilities\Common\XSDValidator;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\Log\Log;
use Exception;
use SimpleXMLElement;

class Socle
{
    /**
     * @var $User UsersTable
     */
    protected $Users;

    /**
     * @var $Structures StructuresTable
     */
    protected $Structures;

    /**
     * Socle constructor.
     *
     * @param \App\Model\Table\UsersTable $Users UsersTable
     * @param \App\Model\Table\StructuresTable $Structures StructuresTable
     */
    public function __construct(?UsersTable $Users = null, ?StructuresTable $Structures = null)
    {
        $this->Users = $Users;
        $this->Structures = $Structures;
    }

    /**
     * Add a structure (Attention La creation des type d'actes nécéssite d'avoir la matiere)
     *
     * @param string $xml xml
     * @return mixed
     * @throws \Exception
     * @throws \App\Utilities\Socle\SocleAlreadyExitsRessourceException
     */
    public function addStructureXml($xml)
    {
        $this->validateXml($xml);
        $xml = new SimpleXMLElement($xml);

        $externalId = strval($xml->externalId);

        //check if $structure already exists
        $count = $this->Structures->find()->where(['id_external' => $externalId])->count();
        if ($count > 0) {
            throw new SocleAlreadyExitsRessourceException('ORGANISM', $externalId);
        }

        $structureArray = $this->formatStructureXml($xml);
        $structureArray['roles'] = $this->Structures->addMinimumRoles();

        $structure = $this->Structures->newEntity($structureArray);
        $res = $this->Structures->save($structure);

        if ($structure->hasErrors()) {
            Log::error(var_export($structure->hasErrors(), true));
            throw new SocleInternalErrorException(
                SocleException::ORGANISM,
                $externalId,
                SocleException::POST
            );
        }

        $this->Structures->addMinimumTypeSittings($res['id']);
        $this->Structures->addMinimumPastellFluxTypes($res['id']);

        return $res;
    }

    /**
     * update a structure
     *
     * @param string $xml xml
     * @return mixed
     * @throws \Exception
     * @throws \App\Utilities\Socle\SocleMissingRessourceException
     */
    public function updateStructureXml($xml)
    {
        $this->validateXml($xml);
        $simpleXml = new SimpleXMLElement($xml);

        $externalId = strval($simpleXml->externalId);

        $structure = $this->Structures->find()->where(['id_external' => $externalId])->first();

        if (!$structure) {
            return $this->addStructureXml($xml);
        }

        $data = $this->formatStructureXml($simpleXml);
        $structure = $this->Structures->patchEntity($structure, $data);
        $res = $this->Structures->save($structure);
        if ($structure->hasErrors()) {
            Log::error(var_export($structure->getErrors(), true));
            throw new SocleInternalErrorException(
                SocleException::ORGANISM,
                $externalId,
                SocleException::PUT
            );
        }

        return $res;
    }

    /**
     * delete a structure : On ne fait que la désactiver
     *
     * @param string $id externalId
     * @return mixed
     * @throws \App\Utilities\Socle\SocleMissingRessourceException
     */
    public function deleteStructure($id)
    {
        try {
            $structure = $this->Structures->find()->where(['id_external' => $id])->firstOrFail();
        } catch (RecordNotFoundException $e) {
            Log::error($e->getMessage());
            throw new SocleMissingRessourceException(
                SocleException::ORGANISM,
                $id,
                SocleException::DELETE
            );
        }
        $structure = $this->Structures->patchEntity($structure, ['active' => false]);
        $res = $this->Structures->save($structure);
        if ($structure->hasErrors()) {
            throw new BadRequestException(var_export($structure->getErrors(), true));
        }

        return $res;
    }

    /**
     * Ajoute un utilisateur
     *
     * @param string $xml xml
     * @return mixed
     * @throws \App\Utilities\Socle\SocleRelationMissingException
     * @throws \Exception
     * @throws \App\Utilities\Socle\SocleAlreadyExitsRessourceException
     */
    public function addUserXml($xml)
    {
        $this->validateXml($xml);
        $xml = new SimpleXMLElement($xml);
        $externalId = strval($xml->user->externalId);

        $count = $this->Users->find()->where(['id_external' => $externalId])->count();

        if ($count > 0) {
            throw new SocleAlreadyExitsRessourceException(SocleException::AGENT, $externalId);
        }

        try {
            $structure = $this->getStructure($xml);
        } catch (RecordNotFoundException $e) {
            Log::error($e->getMessage());
            throw new SocleRelationMissingException(
                SocleException::AGENT,
                $externalId,
                SocleException::POST
            );
        }

        try {
            $role = $this->getRole($xml, $structure['id']);
        } catch (RecordNotFoundException $e) {
            Log::error($e->getMessage());
            throw new SocleRelationMissingException(
                SocleException::AGENT,
                $externalId,
                SocleException::POST
            );
        }

        $userArray = $this->formatUserXml($xml);
        $userArray['roles'] = [
            ['id' => $role['id']],
        ];
        $userArray['structures'] = [
            ['id' => $structure['id']],
        ];
        $userArray['structure_id'] = $structure['id'];

        $user = $this->Users->newEntity(
            $userArray,
            [
                'associated' => ['Roles', 'Structures'],
            ]
        );
        $res = $this->Users->save($user);
        if ($user->hasErrors()) {
            Log::error(var_export($user->getErrors(), true));
            throw new SocleInternalErrorException(
                SocleException::AGENT,
                $externalId,
                SocleException::POST
            );
        }

        return $res;
    }

    /**
     * update an user : Nous avons fait le choix que cela ne mette pas à jour la collectivité
     *
     * @param string $xml xml
     * @return mixed
     * @throws \App\Utilities\Socle\SocleRelationMissingException
     * @throws \Exception
     * @throws \App\Utilities\Socle\SocleMissingRessourceException
     */
    public function updateUserXml($xml)
    {
        $this->validateXml($xml);
        $simpleXml = new SimpleXMLElement($xml);
        $externalId = strval($simpleXml->user->externalId);
        $user = $this->Users->find()->where(['id_external' => $externalId])
            ->contain(['Structures'])->first();

        if (empty($user)) {
            return $this->addUserXml($xml);
        }

        try {
            $role = $this->getRole($simpleXml, $user['structures'][0]['id']);
        } catch (Exception $e) {
            Log::error(var_export($e->getMessage(), true));
            throw new SocleRelationMissingException(
                SocleException::AGENT,
                $externalId,
                SocleException::PUT
            );
        }

        $userArray = $this->formatUserXml($simpleXml);
        $userArray['roles'] = [
            ['id' => $role['id']],
        ];

        $user = $this->Users->patchEntity(
            $user,
            $userArray,
            [
                'associated' => ['Roles'],
            ]
        );
        $res = $this->Users->save($user);
        if ($user->hasErrors()) {
            Log::error(var_export($user->getErrors(), true));
            throw new SocleInternalErrorException(
                SocleException::AGENT,
                $externalId,
                SocleException::PUT
            );
        }

        return $res;
    }

    /**
     * delete an user : Par choix on ne fait que desactiver l'utilisateur !
     *
     * @param string $id externalId
     * @return \App\Model\Entity\User|bool
     * @throws \App\Utilities\Socle\SocleInternalErrorException
     * @throws \App\Utilities\Socle\SocleMissingRessourceException
     */
    public function deleteUser($id)
    {
        try {
            $user = $this->Users->find()->where(['id_external' => $id])->firstOrFail();
        } catch (RecordNotFoundException $e) {
            Log::error(var_export($e->getMessage(), true));
            throw new SocleMissingRessourceException(
                SocleException::AGENT,
                $id,
                SocleException::DELETE
            );
        }
        $user = $this->Users->patchEntity($user, ['active' => false]);
        $res = $this->Users->save($user);
        if ($user->hasErrors()) {
            Log::error(var_export($user->getErrors(), true));
            throw new SocleInternalErrorException(
                SocleException::AGENT,
                $id,
                SocleException::DELETE
            );
        }

        return $res;
    }

    /**
     * format user xml
     *
     * @param \SimpleXMLElement $xml xml
     * @return array
     */
    protected function formatUserXml($xml)
    {
        return [
            'id_external' => strval($xml->user->externalId),
            'firstname' => strval($xml->user->firstname),
            'lastname' => strval($xml->user->lastname),
            'enabled' => strval($xml->enabled),
            'username' => strval($xml->username),
            'email' => strval($xml->email),
            'active' => boolval($xml->enabled),
            'data_security_policy' => false,
            'organization_id' => 1,
        ];
    }

    /**
     * format structure xml
     *
     * @param \SimpleXMLElement $xml xml
     * @return array
     */
    protected function formatStructureXml($xml)
    {
        return [
            'organization_id' => 1,
            'active' => boolval($xml->enabled),
            'business_name' => strval($xml->name),
            'id_external' => strval($xml->externalId),
        ];
    }

    /**
     * get corresponding structure
     *
     * @param \SimpleXMLElement $xml xml
     * @return array|\Cake\Datasource\EntityInterface|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    protected function getStructure($xml)
    {
        $structureExternalId = strval($xml->organismDepartment->organism->externalId);

        $structure = $this->Users->Structures->find()
            ->where(['id_external' => $structureExternalId])->firstOrFail();

        return $structure;
    }

    /**
     * @param \SimpleXMLElement $xml xml
     * @param int $structureId structureId
     * @return mixed
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     */
    protected function getRole($xml, $structureId)
    {
        $roleName = strval($xml->roles->role->name);

        $role = $this->Users->Roles->findByName($roleName, $structureId);

        return $role;
    }

    /**
     * Validate xml with xsd
     *
     * @param string $xml xml
     * @return void
     * @throws \Exception
     */
    protected function validateXml($xml)
    {
        try {
            XSDValidator::schemaValidate(XSD_SOCLE, $xml);
        } catch (Exception $e) {
            Log::error(var_export($e->getMessage(), true));
            throw new SocleSyntaxException();
        }
    }
}
