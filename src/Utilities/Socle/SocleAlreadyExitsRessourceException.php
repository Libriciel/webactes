<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace App\Utilities\Socle;

use Cake\Log\Log;

/**
 * Exception raised when a particular record was not found
 */
class SocleAlreadyExitsRessourceException extends SocleException
{
    /**
     * SocleAlreadyExitsRessourceException.
     *
     * @param string $resource   resource
     * @param string $resourceId resourceId
     */
    public function __construct($resource, $resourceId)
    {
        Log::info('RESOURCE_ALREADY_EXISTS', 'EXCEPTION_SOCLE');
        parent::__construct(
            $resource,
            '005',
            'RESOURCE_ALREADY_EXISTS',
            'La resource existe déjà',
            'POST',
            $resourceId,
            $code = 409,
            $previous = null
        );
    }
}
