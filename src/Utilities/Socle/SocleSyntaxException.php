<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace App\Utilities\Socle;

use Cake\Log\Log;

/**
 * Exception raised when a particular record was not found
 */
class SocleSyntaxException extends SocleException
{
    /**
     * SocleSyntaxException.
     */
    public function __construct()
    {
        Log::info('SYNTAX_ERROR', 'EXCEPTION_SOCLE');
        parent::__construct(
            'unknown',
            '001',
            'SYNTAX_ERROR',
            'Syntaxe du message en entrée incorrecte',
            'unknown',
            'unknown',
            $code = 400,
            $previous = null
        );
    }
}
