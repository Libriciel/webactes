<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace App\Utilities\Socle;

use Cake\Log\Log;

/**
 * Exception raised when a particular record was not found
 */
class SocleRelationMissingException extends SocleException
{
    /**
     * SocleRelationMissingException.
     *
     * @param string $resource   resource
     * @param string $resourceId resourceId
     * @param string $methodType method
     */
    public function __construct($resource, $resourceId, $methodType)
    {
        Log::info('RELATION_MISSING', 'EXCEPTION_SOCLE');
        parent::__construct(
            $resource,
            '004',
            'RELATION_MISSING',
            'Une relation avec l\'entité est manquante',
            $methodType,
            $resourceId,
            $code = 400,
            $previous = null
        );
    }
}
