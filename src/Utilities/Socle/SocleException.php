<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace App\Utilities\Socle;

use Cake\Log\Log;
use DOMDocument;
use Exception;

/**
 * Exception raised when a particular record was not found
 */
class SocleException extends Exception
{
    public const ORGANISM = 'ORGANISM';
    public const AGENT = 'AGENT';
    public const PUT = 'PUT';
    public const POST = 'POST';
    public const DELETE = 'DELETE';

    /**
     * Constructor.
     * SYNTAX_ERROR
     *
     * @param string    $resource     Resource
     * @param int       $errorCode    The code of the error
     * @param string    $errorLabel   error label.
     * @param string    $errorMessage error Message.
     * @param string    $methodType   method type.
     * @param string    $resourceId   resourceId.
     * @param int       $code         http code.
     * @param \Throwable $previous previous
     */
    public function __construct(
        $resource,
        $errorCode,
        $errorLabel,
        $errorMessage,
        $methodType,
        $resourceId,
        $code = null,
        $previous = null
    ) {
        Log::info($errorMessage, 'EXCEPTION_SOCLE');
        $domtree = new DOMDocument('1.0', 'UTF-8');
        $xmlRootElt = $domtree->createElement('error');
        $xmlRootElt->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $xmlRootElt->setAttribute('xsi:noNamespaceSchemaLocation', 'error.xsd');
        $xmlRoot = $domtree->appendChild($xmlRootElt);

        $classTypeElt = $domtree->createElement('classType', $resource);
        $xmlRoot->appendChild($classTypeElt);

        $errorCodeElt = $domtree->createElement('errorCode', $errorCode);
        $xmlRoot->appendChild($errorCodeElt);

        $errorLabelElt = $domtree->createElement('errorLabel', $errorLabel);
        $xmlRoot->appendChild($errorLabelElt);

        $errorMessageElt = $domtree->createElement('errorMessage', $errorMessage);
        $xmlRoot->appendChild($errorMessageElt);

        $methodTypeElt = $domtree->createElement('methodType', $methodType);
        $xmlRoot->appendChild($methodTypeElt);

        $resourceIdElt = $domtree->createElement('resourceId', $resourceId);
        $xmlRoot->appendChild($resourceIdElt);

        $message = $domtree->saveXML();

        parent::__construct($message, $code, $previous);
    }
}
