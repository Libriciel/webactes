<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace App\Utilities\Socle;

use Cake\Log\Log;

/**
 * Exception raised when a particular record was not found
 */
class SocleInternalErrorException extends SocleException
{
    /**
     * SocleAlreadyExitsRessourceException.
     *
     * @param string $resource   resource
     * @param string $resourceId resourceId
     * @param string $methodType method
     */
    public function __construct($resource, $resourceId, $methodType)
    {
        Log::info('INTERNAL_SERVER_ERROR', 'EXCEPTION_SOCLE');

        parent::__construct(
            $resource,
            '006',
            'INTERNAL_SERVER_ERROR',
            'Erreur interne au serveur',
            $methodType,
            $resourceId,
            $code = 500,
            $previous = null
        );
    }
}
