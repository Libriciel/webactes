<?php
declare(strict_types=1);

namespace App\Utilities\Api;

use Cake\Utility\Hash;
use InvalidArgumentException;

/**
 * Base client class
 *
 * @package App\Model\Logic\Http
 */
abstract class Client
{
    protected $http;
    protected $base_url;
    protected $base_api;
    protected $username;
    protected $password;
    protected array $options;

    /**
     * Client constructor.
     *
     * @param \Cake\Http\Client $http    The HTTP object
     * @param string            $url     The URL to query
     * @param string            $apiBase The base uri of the API actions
     */
    public function __construct($http, $url, $apiBase = '/')
    {
        $this->http = $http;
        $this->base_url = $url;
        $this->base_api = $apiBase;
        $this->options = ['type' => 'json'];
    }

    /**
     * Execute the request corresponding to the HTTP method
     *
     * @param  string $endpoint   The API endpoint
     * @param  string $httpMethod The HTTP method
     * @param  mixed  $urlParams  The parameters to be set in the URL
     * @param  mixed  $bodyParams The parameters to be set in the body request
     * @param  array  $options    The options
     * @return \Cake\Http\Client\Response The response of the query
     */
    private function execute($endpoint, $httpMethod, $urlParams, $bodyParams = null, $options = [])
    {
        $options = $this->mergeOptions($options);
        $url = $this->base_url . '/' . $this->base_api . '/' . $endpoint;

        if (!empty($urlParams)) {
            $url .= strpos($url, '?') === false ? '?' : '&';
            $url .= is_string($urlParams) ? $urlParams : http_build_query($urlParams);
        }

        //Log::write('debug', self::class . ' *** ' . __FUNCTION__, 'pastell');
        //Log::write('debug', $url, 'pastell');

        if ($httpMethod === 'GET') {
            $result = $this->http->get($url, null, $this->options);
        } elseif ($httpMethod === 'POST') {
            $result = $this->http->post($url, $bodyParams, $this->options);
        } elseif ($httpMethod === 'PUT') {
            $result = $this->http->put($url, $bodyParams, $this->options);
        } elseif ($httpMethod === 'PATCH') {
            $result = $this->http->patch($url, $bodyParams, $this->options);
        } elseif ($httpMethod === 'DELETE') {
            $result = $this->http->delete($url);
        } else {
            throw new InvalidArgumentException('Unsupported HTTP method provided :  ' . $httpMethod);
        }

        // Log::write('debug', '[HTTP REQUEST BEGIN] ', 'HTTP');
        /*$bodyParams != null && isset($bodyParams['file_content'])
            ? $bodyParams['file_content'] = 'Suppression du binaire pour affichage des logs'
            : null;
*/
        /* Log::write('debug', '[HTTP REQUEST METHOD] ' . $httpMethod, 'HTTP');
        // Log::write('debug', '[HTTP REQUEST OPTIONS] ' . var_export($this->options,true), 'HTTP');
        //Log::write('debug', '[HTTP REQUEST BODY PARAMS] ' . var_export($bodyParams, true), 'HTTP');
        Log::write('debug', '[HTTP REQUEST URL] ' . $url, 'HTTP');
        Log::write('debug', '[HTTP REQUEST END]', 'HTTP');

        Log::write('debug', '[HTTP RESPONSE BEGIN] ', 'HTTP');
        Log::write('debug', '[HTTP STATUS CODE] ' . $result->getStatusCode(), 'HTTP');
        Log::write('debug', var_export($result->getHeader('Content-Type'), true));
        Log::write('debug', '[HTTP CONTENT  TYPE ] ' . ($result->getHeader('Content-Type') != []
                ? $result->getHeader('Content-Type')[0]
                : 'Non spécifié'), 'HTTP');
        Log::write('debug', '[HTTP BODY] ' .
        ($result->getHeader('Content-Type') != [] && $result->getHeader('Content-Type')[0] == 'application/pdf'
        ? $result->getHeader('Content-disposition')[0]
        : $result->getStringBody()), 'HTTP');
        Log::write('debug', '[HTTP RESPONSE END]', 'HTTP');
*/
        return $result;
    }

    /**
     * Set credentials for the request
     *
     * @param  string $username The login
     * @param  string $password The password
     * @return void
     */
    public function setCredentials($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Send a HTTP get request
     *
     * @param  string $endpoint  The API endpoint
     * @param  mixed  $urlParams The parameters to be set in the URL
     * @param  mixed  $options The parameters to be set in the body request
     * @return \Cake\Http\Client\Response The response of the query
     */
    protected function get($endpoint, $urlParams, $options = [])
    {
        return $this->execute($endpoint, 'GET', $urlParams, null, $options);
    }

    /**
     * Send a HTTP POST request
     *
     * @param  string $endpoint   The API endpoint
     * @param  mixed  $urlParams  The parameters to be set in the URL
     * @param  mixed  $bodyParams The parameters to be set in the body request
     * @param  mixed  $options The parameters to be set in the body request
     * @return \Cake\Http\Client\Response The response of the query
     */
    protected function post($endpoint, $urlParams = '', $bodyParams = '', $options = [])
    {
        return $this->execute($endpoint, 'POST', $urlParams, $bodyParams, $options);
    }

    /**
     * Send a HTTP PUT request
     *
     * @param  string $endpoint   The API endpoint
     * @param  mixed  $urlParams  The parameters to be set in the URL
     * @param  mixed  $bodyParams The parameters to be set in the body request
     * @param  mixed  $options The parameters to be set in the body request
     * @return \Cake\Http\Client\Response The response of the query
     */
    protected function put($endpoint, $urlParams, $bodyParams, $options = [])
    {
        return $this->execute($endpoint, 'PUT', $urlParams, $bodyParams, $options);
    }

    /**
     * Send a HTTP PATCH request
     *
     * @param  string $endpoint   The API endpoint
     * @param  mixed  $urlParams  The parameters to be set in the URL
     * @param  mixed  $bodyParams The parameters to be set in the body request
     * @param  mixed  $options The parameters to be set in the body request
     * @return \Cake\Http\Client\Response The response of the query
     */
    protected function patch($endpoint, $urlParams, $bodyParams, $options = [])
    {
        return $this->execute($endpoint, 'PATCH', $urlParams, $bodyParams, $options);
    }

    /**
     * Send a HTTP DELETE request
     *
     * @param  string $endpoint  The API endpoint
     * @param  mixed  $urlParams The parameters to be set in the URL
     * @return \Cake\Http\Client\Response The response of the query
     */
    protected function delete($endpoint, $urlParams)
    {
        return $this->execute($endpoint, 'DELETE', $urlParams);
    }

    /**
     * Does a recursive merge of the parameter with the scope config.
     *
     * @param array $options Options to merge.
     * @return array Options merged with set config.
     */
    private function mergeOptions(array $options): array
    {
        return Hash::merge($this->options, $options);
    }
}
