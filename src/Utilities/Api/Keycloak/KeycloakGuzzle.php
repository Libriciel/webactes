<?php
/**
 * @codingStandardsIgnoreFile
 * @SuppressWarnings(PHPMD)
 */

namespace App\Utilities\Api\Keycloak;

use Cake\Http\Response;
use Cake\Log\Log;
use GuzzleHttp\Client;


/**
 * Class KeycloakGuzzle
 * @package App\Utility\Keycloak
 *
 */
class KeycloakGuzzle
{

    protected $realm;
    protected $client_id;
    protected $client_secret;
    protected $base_uri;
    protected $admin_uri;
    public $token;
    public $server_url;
    public $client;


    public function __construct($config, $isAdmin = false, $token = null)
    {
        $this->token = $token;
        $this->realm = $config['realm'];
        $this->client_id = $config['resource'];
        $this->client_secret = $config['credentials']['secret'];
        $this->server_url = $config['auth-server-url'];

        $this->admin_uri = "/admin/realms/{$this->realm}/";
        $this->base_uri = "/realms/{$this->realm}/protocol/openid-connect/";
    }


    public function logout($user)
    {
        return $this->post('logout', $user->token, null, [
            'client_id' => $this->client_id,
            'refresh_token' => $user->refresh_token,
            'client_secret' => $this->client_secret,
        ]);
    }

    public function getUserByRefreshToken($refresh_token)
    {
        return $this->post('token', null, [
            'client_id' => $this->client_id,
            'refresh_token' => $refresh_token,
            'grant_type' => 'refresh_token',
            'client_secret' => $this->client_secret,
        ]);
    }


    public function getToken($username, $password)
    {
        return $this->post($this->base_uri, 'token', null, [
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'username' => $username,
            'password' => $password,
            'grant_type' => 'password',
        ]);
    }


    public function verifyToken($token)
    {
        return $this->get($this->base_uri, 'userinfo', $token);
    }


    function refreshToken()
    {
        return $this->post($this->base_uri, 'token', null, [
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,

            'grant_type' => 'password',
        ]);
    }

// https://webact.local/auth/admin/realms/realmTest2/users/2369994d-b1a4-433c-a768-20fa71cccb4f/groups/c02fcd05-27bc-4030-aa85-91ec4e52a15b
    function associateGroupToUser($user_id, $group_id){
        return $this->put($this->admin_uri, "users/{$user_id}/groups/{$group_id}", $this->token);
    }


    function getClients()
    {
        return $this->get($this->admin_uri, "clients", $this->token);
    }

    function getClientSecret($clientId)
    {
        return $this->get($this->admin_uri, "clients/{$clientId}/client-secret", $this->token);
    }


    function getClientJsonSchema($clientId)
    {
        return $this->get($this->admin_uri, "clients/{$clientId}/installation/providers/keycloak-oidc-keycloak-json", $this->token);
    }

    function getServerInfo()
    {
        return $this->get($this->admin_uri, "admin/serverinfo", $this->token);

    }

    public function getUsers()
    {
        return $this->get($this->admin_uri, 'users', $this->token);
    }

    public function getUsersQuery($query)
    {
        return $this->get($this->admin_uri, "users{$query}", $this->token);
    }

    public function getUser($user_id)
    {
        return $this->get($this->admin_uri, "users/{$user_id}", $this->token);
    }



    public function getRealms()
    {
        return $this->get($this->admin_uri, "", $this->token);
    }


    public function createRealm($realmJson)
    {
        return $this->post('/admin/', "realms", $this->token, null, $realmJson);
    }


    public function deleteUser($user_id)
    {
        return $this->delete($this->admin_uri , "users/{$user_id}", $this->token);
    }

    public function updateUser($user_id, $data)
    {
        return $this->put($this->admin_uri,"users/{$user_id}", $this->token, null, $data);
    }

    public function createNewUser($data)
    {
        return $this->post($this->admin_uri, 'users', $this->token, null, $data);
    }

    protected function delete($uri, $path, $token = null, $form_params = null, $json = null)
    {
        return $this->request('delete', $uri, $path, $token, $form_params, $json);
    }

    protected function put($uri, $path, $token = null, $form_params = null, $json = null)
    {
        return $this->request('put', $uri, $path, $token, $form_params, $json);
    }

    protected function post($uri, $path, $token = null, $form_params = null, $json = null)
    {
        return $this->request('post', $uri, $path, $token, $form_params, $json);
    }

    protected function get($uri, $path, $token = null, $form_params = null, $json = null)
    {
        return $this->request('get', $uri, $path, $token, $form_params, $json);
    }

    protected function request($method, $uri, $path, $token = null, $form_params = null, $json = null)
    {


        $info = [
            'verify' => false,
            'headers' => [],
        ];
        if (!is_null($token)) {
            $info['headers']['Authorization'] = 'Bearer ' . $token;
        }
        if (!is_null($json)) {
            $info['json'] = $json;
        }
        if (!is_null($form_params)) {
            $info['form_params'] = $form_params;
        }

        /** @var Response $response */
//Log::info(var_export($info, true));
        $response = $this->client($uri)->{$method}($path, $info);

        if (in_array($response->getStatusCode(), [200, 201, 204])) {

            $data = json_decode($response->getBody()->getContents(), true);
            $ret = [
                "success" => true,
                "data" => $data,
                "code" => $response->getStatusCode()
            ];

            //if a new ressource is created
            if ($response->getStatusCode() == 201) {
                $location = $response->getHeaderLine('Location');
                $uuidPattern = '/[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}/';
                preg_match($uuidPattern, $location, $matches, PREG_OFFSET_CAPTURE);
                if (isset ($matches[0][0])) {
                    $ret["id"] = $matches[0][0];
                }
            }

            return $ret;
        }

        return [
            "success" => false,
            "code" => $response->getStatusCode(),
            "message" => $response->getBody()->getContents(),
            "header" => $response->getHeaders()
        ];


    }

    /**
    * @param $uri
    * @return Client
     */
    protected function client($uri)
    {
        $defaults = [
            'base_uri' => $this->server_url . $uri,
            'defaults' => [
                'exceptions' => false
            ],
            'http_errors' => false,
        ];
        if (getenv('HTTP_PROXY_IP_ADDRESS')) {
            $defaults['proxy'] = getenv('HTTP_PROXY_IP_ADDRESS');
        }
        return $this->client = new Client($defaults);
    }
}
