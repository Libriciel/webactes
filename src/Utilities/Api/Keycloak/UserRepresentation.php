<?php
/**
 * @codingStandardsIgnoreFile
 * @SuppressWarnings(PHPMD)
 */
namespace App\Utilities\Api\Keycloak;

class UserRepresentation
{

    /**
     * Commentaire : aucun champ n'est obligatoire pour la création d'un utilisateur
     *
     */

    /**
     * @var string
     */
    public $self;

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $origin;


    /**
     * @var integer
     */
    public $createdTimestamp;

    /**
     * @var string
     */
    public $username;

    /**
     * @var boolean
     */
    public $enabled;

    /**
     * @var boolean
     */
    public $emailVerified;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $federationLink;

    /**
     * @var [Map]
     */
    public $attributes;

    /**
     * @var CredentialRepresentation array
     */
    public $credentials;

    /**
     * @var string array
     */
    public $disableableCredentialTypes;


    /**
     * @var string array
     */
    public $requiredActions;
//
//    /**
//     * @var FederatedIdentityRepresentation array  (TO Create)
//     */
//    public $federatedIdentities;

    /**
     * @var string array
     */
    public $realmRoles;

    /**
     * @var [Map]
     */
    public $clientRoles;

//    /**
//     * @var UserConsentRepresentation array
//     */
//    public $clientConsents;

    /**
     * @var string array
     */
    public $groups;


}
