<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 30/04/19
 * Time: 17:11
 */

namespace App\Utility\KeycloakApi;

class Realms
{
    private $realms = [
        'wa' => 'realmWA',
    ];

    /**
     * get the realm
     *
     * @param  string $suffix realm suffix
     * @return string|null
     */
    public function getRealm($suffix)
    {
        if (isset($this->realms[$suffix])) {
            return $this->realms[$suffix];
        }

        return null;
    }
}
