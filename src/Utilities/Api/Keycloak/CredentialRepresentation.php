<?php
/**
 * @codingStandardsIgnoreFile
 */
namespace App\Utilities\Api\Keycloak;

class CredentialRepresentation
{
    /**
    "credentials": [
    {
    "type": "password",
    "value": "myPassword"
     "temporary" : true or false (default ??)
    }
     **/

    /**
     * @var string
     */
    public $type;


    /**
     * @var string
     */
    public $device;

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $hashedSaltedValue;

    /**
     * @var string
     */
    public $salt;

    /**
     * @var integer
     */
    public $hashIterations;

    /**
     * @var integer
     */
    public $counter;

    /**
     * @var string
     */
    public $algorithm;

    /**
     * @var integer
     */
    public $digits;


    /**
     * @var integer
     */
    public $period;


    /**
     * @var integer
     */
    public $createdDate;


     /**
     * @var boolean
     */
    public $temporary;

}
