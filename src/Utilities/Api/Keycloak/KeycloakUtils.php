<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 29/04/19
 * Time: 08:42
 */

namespace App\Utilities\Api\Keycloak;

use Exception;
use Ramsey\Uuid\Uuid;

//define('API_CLIENT_NAME', 'api-client');
//define('WEB_CLIENT_NAME', 'web-client');
//define('SERVEUR_URL', 'https://webact.local/auth');
//define('KEYCLOAK_CONFIG_DIR', CONFIG . 'KeycloakConfig' . DS);

class KeycloakUtils
{
    /**
     * @return bool
     * @param  string $realm realm name
     */
    public static function generateRealmJson($realm)
    {
        try {
            $apiClientId = Uuid::uuid4()->toString();
            $apiSecret = Uuid::uuid4()->toString();
        } catch (Exception $e) {
           // var_dump($e->getMessage());

            return false;
        }

        //create realm Json
        $realmTemplate = __DIR__ . DS . 'keycloakTemplate.json';
        $realmJson = KEYCLOAK_CONFIG_DIR . 'keycloak_' . $realm . '.json';
        $file_contents = file_get_contents($realmTemplate);

        $file_contents = str_replace('$REALM', $realm, $file_contents);
        $file_contents = str_replace('$API_CLIENT_ADMIN_ROLE_ID', $apiClientId, $file_contents);
        $file_contents = str_replace('$API_CLIENT_SECRET', $apiSecret, $file_contents);
        $file_contents = str_replace('$API_CLIENT', API_CLIENT_NAME, $file_contents);
        $file_contents = str_replace('$WEB_CLIENT', WEB_CLIENT_NAME, $file_contents);

        $res = file_put_contents($realmJson, $file_contents);

        if (!$res) {
            return false;
        }

        // create api client connexion json
        $templateConnection = __DIR__ . DS . 'keycloakConnectionTemplate.json';
        $apiConnection = KEYCLOAK_CONFIG_DIR . 'keycloak_api_' . $realm . '.json';
        $apiContent = file_get_contents($templateConnection);
        $apiContent = str_replace('$REALM', $realm, $apiContent);
        $apiContent = str_replace('$SECRET', $apiSecret, $apiContent);
        $apiContent = str_replace('$CLIENT_NAME', API_CLIENT_NAME, $apiContent);
        $apiContent = str_replace('$SERVEUR_URL', SERVEUR_URL, $apiContent);
        $res = file_put_contents($apiConnection, $apiContent);

        if (!$res) {
            return false;
        }

        // create api client connexion json
        $templateConnection = __DIR__ . DS . 'keycloakConnectionTemplate.json';
        $apiConnection = KEYCLOAK_CONFIG_DIR . 'keycloak_web_' . $realm . '.json';
        $apiContent = file_get_contents($templateConnection);
        $apiContent = str_replace('$REALM', $realm, $apiContent);
        $apiContent = str_replace('$SECRET', $apiSecret, $apiContent);
        $apiContent = str_replace('$CLIENT_NAME', WEB_CLIENT_NAME, $apiContent);
        $apiContent = str_replace('$SERVEUR_URL', SERVEUR_URL, $apiContent);
        $res = file_put_contents($apiConnection, $apiContent);

        return $res;
    }
}
