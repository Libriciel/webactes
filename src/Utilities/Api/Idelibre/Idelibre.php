<?php
declare(strict_types=1);

namespace App\Utilities\Api\Idelibre;

use App\Utilities\Api\Client;
use Cake\Utility\Hash;
use Exception;

/**
 * Pastell API client class
 *
 * @package App\Model\Logic\Http\Client
 */
class Idelibre extends Client
{
    private $auth;
    private string $url;

    /**
     * {@inheritDoc}
     * Pastell constructor.
     */
    public function __construct($http, $url, $auth, $apiBase = 'Api300')
    {
        $this->auth = $auth;
        $this->url = $url;
        parent::__construct($http, $url, $apiBase);
    }

    /**
     * checkIdelibre connexion
     *
     * @return mixed
     */
    public function checkConnexion()
    {
        $endpoint = 'check';

        return $this->post($endpoint, [], $this->auth, ['type' => 'multipart/form-data']);
    }

    /**
     * checkIdelibre connexion
     *
     * @param array $data Foo
     * @return mixed
     * @throws \Exception
     */
    public function send(array $data)
    {
        $data = $this->mergeAuth($data);
        // Initialisation des paramètres curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . '/seances.json');

//        if (Configure::read('IDELIBRE_USEPROXY')) {
//            curl_setopt($ch, CURLOPT_PROXY, Configure::read('IDELIBRE_PROXYHOST'));
//        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        //Log::info(var_export($data , true));

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        // Exécution de la requête
        $retour = curl_exec($ch);
        // Décodage en tableau du retour
        $retour = json_decode($retour, true);
        // Fermeture de la connexion
        curl_close($ch);

        if (empty($retour)) {
            throw new Exception(__('Erreur de communication avec i-delibRE.'));
        } elseif (!empty($retour['error'])) {
            throw new Exception(__('L\'export vers i-delibRE a échoué.'));
        } elseif (!empty($retour['success'])) {
                return $retour;
        } else {
            throw new Exception(__('Réponse i-delibRE incorrecte') . ': ' . $retour['message']);
        }
    }

    /**
     * Does a recursive merge of the parameter with the scope config.
     *
     * @param array $data Options to merge.
     * @return array Options merged with set config.
     */
    private function mergeAuth(array $data): array
    {
        return Hash::merge($this->auth, $data);
    }
}
