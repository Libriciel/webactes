<?php
declare(strict_types=1);

namespace App\Utilities\Api\Pastell;

use App\Utilities\Api\Client;
use Cake\Core\Configure;
use Cake\Http\Client\Response;
use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use Cake\Utility\Security;

/**
 * Pastell API client class
 *
 * @package App\Model\Logic\Http\Client
 */
class Pastell extends Client
{
    public const ELEMENT_ARRETE = 'arrete';
    public const ELEMENT_ANNEXE = 'autre_document_attache';

    /**
     * {@inheritDoc}
     * Pastell constructor.
     */
    public function __construct($http, $url, $apiBase = 'api/v2')
    {
        parent::__construct($http, $url, $apiBase);
    }

    /**
     * Get the entity
     *
     * @param  string $entityId the id of the entity
     * @return \Cake\Http\Client\Response the list of pastell connectors associated with the entity $id_e
     */
    public function getEntity($entityId)
    {
        $endpoint = "entite/$entityId";

        return $this->get($endpoint, []);
    }

    /**
     * Get a Pastell entity by its name
     *
     * @param  string $name the name of the entity
     * @return mixed the entry of the entity if it exists, otherwise null
     */
    public function getEntityByName($name)
    {
        $entities = $this->getEntities();
        if ($entities->isOk()) {
            return collection($entities->getJson())->firstMatch(['denomination' => $name]);
        }
    }

    /**
     * Get the list of Pastell entities
     *
     * @return \Cake\Http\Client\Response the list of all the entities
     */
    public function getEntities()
    {
        $endpoint = 'entite';

        return $this->get($endpoint, []);
    }

    /**
     * Get the connectors associated to an entity
     *
     * @param  string $entityId the id of the entity
     * @return \Cake\Http\Client\Response the list of pastell connectors associated with the entity $id_e
     */
    public function getConnectorsOfEntity($entityId)
    {
        $endpoint = "entite/$entityId/connecteur";

        return $this->get($endpoint, []);
    }

    /**
     * Creates a new entity of the entity with the name $name doesn't exist
     *
     * @param  string $name  The name of the entity
     * @param  string $siren The siren of the entity
     * @param  string $type  The type of the entity
     * @return \Cake\Http\Client\Response the entry of the entity if it exists otherwise the id of the newly created entity
     */
    public function createEntity($name, $siren, $type = 'collectivite')
    {
        $endpoint = 'entite';

        $data = [
            'denomination' => $name,
            'siren' => $siren,
            'type' => $type,
        ];

        return $this->post($endpoint, [], $data);
    }

    /**
     * Updates the siren of the entity $name
     *
     * @param  string $entityId The pastell id of the entity
     * @param  string $siren    The siren of the entity
     * @param  string $type     The type of the entity
     * @return \Cake\Http\Client\Response
     */
    public function updateEntity($entityId, $siren, $type = 'collectivite')
    {
        $endpoint = "entite/$entityId";

        $data = [
            'siren' => $siren,
            'type' => $type,
        ];

        return $this->patch($endpoint, [], $data);
    }

    /**
     * Get a Pastell user by its name
     *
     * @param  string $username the username of the user
     * @return mixed the entry of the user if it exists, otherwise null
     */
    public function getUserByName($username)
    {
        $users = $this->getUsers();
        if ($users->isOk()) {
            return collection($users->getJson())->firstMatch(['login' => $username]);
        }
    }

    /**
     * Get the list of the Pastell users
     *
     * @return \Cake\Http\Client\Response the list of all users
     */
    public function getUsers()
    {
        $endpoint = '/utilisateur';

        return $this->get($endpoint, []);
    }

    /**
     * Creates a new user if the user with the login $username doesn't exist
     *
     * @param  string $username  The username
     * @param  string $email     The email address
     * @param  string $firstname The firstname
     * @param  string $lastname  The lastname
     * @param  string $password  The password
     * @return \Cake\Http\Client\Response the entry of the user if it exists, otherwise the id of the newly created user
     */
    public function createUser($username, $email, $firstname, $lastname, $password = null)
    {
        $endpoint = '/utilisateur';

        if (empty($password)) {
            $password = Security::randomString();
        }

        $data = [
            'login' => $username,
            'prenom' => $firstname,
            'nom' => $lastname,
            'email' => $email,
            'password' => $password,
        ];

        return $this->post($endpoint, [], $data);
    }

    /**
     * Updates the mail, firstname and lastname of the user with the login $username
     *
     * @param  string $userId    The pastell id of the user
     * @param  string $email     The email address
     * @param  string $firstname The firstname
     * @param  string $lastname  The lastname
     * @param  string $password  The password
     * @return \Cake\Http\Client\Response
     */
    public function updateUser($userId, $email, $firstname, $lastname, $password = null)
    {
        $endpoint = "/utilisateur/$userId";

        $data = [
            'prenom' => $firstname,
            'nom' => $lastname,
            'email' => $email,
            'password' => $password,
        ];

        return $this->patch($endpoint, [], $data);
    }

    /**
     * Deletes a user by its id
     *
     * @param  string $userId The pastell id of the user
     * @return \Cake\Http\Client\Response
     */
    public function deleteUser($userId)
    {
        $endpoint = "/utilisateur/$userId";

        return $this->delete($endpoint, []);
    }

    /**
     * Add a role to a user for an entity
     *
     * @param  string $userId   The user id
     * @param  string $entityId The entity id
     * @param  string $role     The role
     * @return \Cake\Http\Client\Response
     */
    public function addRoleToUser($userId, $entityId, $role)
    {
        $endpoint = "/utilisateur/$userId/role/add";

        $data = [
            'id_e' => $entityId,
            'role' => $role,
        ];

        return $this->post($endpoint, [], $data);
    }

    /**
     * Delete a role from a user of an entity
     *
     * @param  string $userId   The user id
     * @param  string $entityId The entity id
     * @param  string $role     The role
     * @return \Cake\Http\Client\Response
     */
    public function deleteRoleFromUser($userId, $entityId, $role)
    {
        $endpoint = "/utilisateur/$userId/role/delete";

        $data = [
            'id_e' => $entityId,
            'role' => $role,
        ];

        return $this->delete($endpoint, $data);
    }

    /**
     * Creates the connector with the id $id_entity to the entity $id_entity
     *
     * @param  string $entityId       the id of the entity
     * @param  string $connectorName  the name (id) of the connector
     * @param  string $connectorLabel the label of the connector
     * @return \Cake\Http\Client\Response the id of the newly created connector
     */
    public function createConnector($entityId, $connectorName, $connectorLabel)
    {
        $endpoint = "entite/$entityId/connecteur";

        $data = [
            'id_connecteur' => $connectorName,
            'libelle' => $connectorLabel,
        ];

        return $this->post($endpoint, [], $data);
    }

    /**
     * Configures the connector $id_connector for the entity $id_entity with the data available in $parameters
     *
     * @param  string $entityId    The entity id
     * @param  string $connectorId The connector id
     * @param  array  $parameters  A list of parameters to be set in the connector
     * @return \Cake\Http\Client\Response
     */
    public function configureConnector($entityId, $connectorId, $parameters = [])
    {
        $endpoint = "entite/$entityId/connecteur/$connectorId/content";

        $data = $parameters;

        return $this->patch($endpoint, [], $data);
    }

    /**
     * Associates the connector of an entity to a flux
     *
     * @param  string $entityId    The entity id
     * @param  string $connectorId The connector id
     * @param  string $flux        the name of the flux
     * @param  string $type        the type of the connector
     * @return \Cake\Http\Client\Response
     */
    public function associateFluxWithConnector($entityId, $connectorId, $flux, $type)
    {
        $endpoint = "entite/$entityId/flux/$flux/connecteur/$connectorId";

        $data = [
            'type' => $type,
        ];

        return $this->post($endpoint, [], $data);
    }

    /**
     * Associates the connector of an entity to a flux
     *
     * @param  string $entityId The entity id
     * @return \Cake\Http\Client\Response
     */
    public function getFluxActesGeneriqueWithConnectorTdT($entityId)
    {
        $endpoint = "entite/$entityId/flux?flux=actes-generique&type=TdT";

        return $this->get($endpoint, []);
    }

    /**
     * Récupération du fichier XML de la classification
     *
     * @param  string $entityId The entity id
     * @param  string $idCe     The entity id Ce
     * @return \Cake\Http\Client\Response
     */
    public function getClassificationFile($entityId, $idCe)
    {
        return $this->get("entite/$entityId/connecteur/$idCe/file/classification_file", []);
    }

    /**
     * Triggers an action on a connector
     *
     * @param  string $entityId    The entity id
     * @param  string $connectorId The connector id
     * @param  string $action      the name of the action
     * @return \Cake\Http\Client\Response
     */
    public function triggerActionOnConnector($entityId, $connectorId, $action)
    {
        $endpoint = "entite/$entityId/connecteur/$connectorId/action/$action";

        return $this->post($endpoint, [], []);
    }

    /**
     * Get details of a connector
     *
     * @param  string $entityId    The entity id
     * @param  string $connectorId The connector id
     * @return \Cake\Http\Client\Response
     */
    public function getConnectorDetails($entityId, $connectorId)
    {
        $endpoint = "entite/$entityId/connecteur/$connectorId";

        return $this->get($endpoint, []);
    }

    /**
     * Upload a file to an element of a connector
     *
     * @param  string $entityId    The entity id
     * @param  string $connectorId The connector id
     * @param  string $field       The name of the element which contains a file4
     * @param  mixed  $fileContent The content of the file
     * @param  string $filename    The name of the file
     * @return \Cake\Http\Client\Response
     */
    public function uploadFileToConnector($entityId, $connectorId, $field, $fileContent, $filename = 'unnamed')
    {
        $endpoint = "entite/$entityId/connecteur/$connectorId/file/$field";

        $data = [
            'file_name' => $filename,
            'file_content' => $fileContent,
        ];

        return $this->post($endpoint, [], $data);
    }

    /**
     * @param  string $entityId    The entity id
     * @param  string $connectorId The connector id
     * @param  string $field       The desired element file wanted
     * @return \Cake\Http\Client\Response
     */
    public function getConnectorElementFile($entityId, $connectorId, $field): Response
    {
        $endpoint = "entite/$entityId/connecteur/$connectorId/file/$field";

        return $this->get($endpoint, []);
    }

    /**
     * Récuperer toutes les connecteurs d'une entité
     * Pastell Documenttation https://gitlab.libriciel.fr/pastell/Documentations/PA-DOC-API/blob/v3.0/07_instance_connecteur.md#lister-toutes-les-instances-dune-entit%C3%A9
     *
     * @param  int $entityID Identifiant de l'entité
     * @return \Cake\Http\Client\Response
     */
    public function getConnectors(int $entityID): Response
    {
        $endpoint = "entite/$entityID/connecteur/";

        return $this->get($endpoint, []);
    }

    /**
     * Récuperer tous les flux d'une entité
     *
     * @param  int $entityID Identifiant de l'entité
     * @return \Cake\Http\Client\Response
     */
    public function getFlux(int $entityID): Response
    {
        $endpoint = "entite/$entityID/flux/";

        return $this->get($endpoint, []);
    }

    /**
     * Créer un document PASTELL
     *
     * @param  int    $idOrchestration ID de la structure sous pastell (id_e)
     * @param  string $type            Type de document crée sous pastell par défaut
     *                                 actes-generique
     * @return \Cake\Http\Client\Response
     */
    public function createDocument($idOrchestration, $type = 'actes-generique'): Response
    {
        $endpoint = "entite/$idOrchestration/document";

        return $this->post($endpoint, [], ['type' => $type]);
    }

    /**
     * @param  int   $idOrchestration ID de la structure sous pastell (id_e)
     * @param  int   $idDocument      ID du document sous pastell (id_d)
     * @param  array $data            Les données nécessaire à la modification du
     *                                document.
     * @return \Cake\Http\Client\Response
     */
    public function editDocument($idOrchestration, $idDocument, $data): Response
    {
        $endpoint = "entite/$idOrchestration/document/$idDocument";

        return $this->patch($endpoint, [], $data);
    }

    /**
     * Envoie du document principal sur le document PASTELL
     *
     * @param  int   $idOrchestration ID de la structure sous pastell (id_e)
     * @param  int   $idDocument      ID du document sous pastell (id_d)
     * @param  array $data            Les données nécessaire à l'envoi du
     *                                fichier.
     * @return \Cake\Http\Client\Response
     */
    public function sendMainFileToDocument($idOrchestration, $idDocument, $data): Response
    {
        $endpoint = "entite/$idOrchestration/document/$idDocument/file/arrete";

        return $this->post($endpoint, [], $data);
    }

    /**
     * @param  int   $idOrchestration ID de la structure sous pastell (id_e)
     * @param  int   $idDocument      ID du document sous pastell (id_d)
     * @param  array $data            Les données nécessaire à l'envoi du fichier en
     *                                annexe.
     * @param  int   $key             Numero de l'annexe
     * @return \Cake\Http\Client\Response
     */
    public function sendAnnexeFileToDocument($idOrchestration, $idDocument, $data, $key): Response
    {
        $endpoint = "entite/$idOrchestration/document/$idDocument/file/autre_document_attache/$key";

        return $this->post($endpoint, [], $data);
    }

    /**
     * @param  int   $orchestrationId ID de la structure sous pastell (id_e)
     * @param  int   $documentId      ID du document sous pastell (id_d)
     * @param  array $data            Les données nécessaire à l'envoi du fichier en
     *                                annexe.
     * @param  int   $key             Numero de l'annexe
     * @return \Cake\Http\Client\Response
     */
    public function sendAttachmentFileToDocument($orchestrationId, $documentId, $data, $key): Response
    {
        $endPoint = "entite/$orchestrationId/document/$documentId/file/document_attache/$key";

        return $this->post($endPoint, [], $data);
    }

    /**
     * Get documents
     *
     * @param  int   $entityId The entity id
     * @param  array $filters  Use to build the query string
     *                         List of all filters here
     *                         https://gitlab.libriciel.fr/pastell/Documentations/PA-DOC-API/blob/master/10_document.md#liste-des-documents
     * @return \Cake\Http\Client\Response
     */
    public function getDocumentsBy($entityId, $filters)
    {
        $endpoint = "entite/$entityId/document";

        return $this->get($endpoint, $filters);
    }

    /**
     * Récupére le détail d'un document PASTELL
     *
     * @param  int $entityId          The entity id
     * @param  string $documentPastellId The id in PASTELL
     * @return \Cake\Http\Client\Response
     */
    public function getDocument($entityId, $documentPastellId)
    {
        $endpoint = "entite/$entityId/document/" . $documentPastellId;

        return $this->get($endpoint, []);
    }

    /**
     * @param  int    $entityId   The entity id.
     * @param  int    $documentId The document id.
     * @param  string $actionName The action to execute.
     * @return \Cake\Http\Client\Response
     */
    public function triggerActionOnDocument($entityId, $documentId, $actionName): Response
    {
        $endpoint = "entite/$entityId/document/$documentId/action/$actionName";

        return $this->post($endpoint, '', ['action' => $actionName]);
    }

    /**
     * checkPastell connexion
     *
     * @param  int $entityId entityId
     * @return \Cake\Http\Client\Response
     */
    public function checkConnexion($entityId)
    {
        $endpoint = "entite/$entityId";

        Log::info(self::class . ' *** ' . __FUNCTION__, 'pastell');

        return $this->get($endpoint, []);
    }

    /**
     * get the aracte.xml
     *
     * @param  int $entityId   entityId
     * @param  int $documentId documentId
     * @return \Cake\Http\Client\Response
     */
    public function getArActes($entityId, $documentId)
    {
        $endpoint = "entite/$entityId/document/$documentId/file/aractes/0";

        return $this->get($endpoint, []);
    }

    /**
     * get the bordereau.pdf
     *
     * @param  int $entityId   entityId
     * @param  int $documentId documentId
     * @return \Cake\Http\Client\Response
     */
    public function getBordereau($entityId, $documentId)
    {
        $endpoint = "entite/$entityId/document/$documentId/file/bordereau/0";

        return $this->get($endpoint, []);
    }

    /**
     * get acteTamponnee.pdf
     *
     * @param  int $entityId   entityId
     * @param  int $documentId documentId
     * @return \Cake\Http\Client\Response
     */
    public function getActeTamponne($entityId, $documentId)
    {
        $endpoint = "entite/$entityId/document/$documentId/file/acte_tamponne/0";

        return $this->get($endpoint, []);
    }

    /**
     * get annexeTamponnee
     *
     * @param  int $entityId   entityId
     * @param  int $documentId documentId
     * @param  int $rank       annexeRank
     * @return \Cake\Http\Client\Response
     */
    public function getAnnexesTamponnee($entityId, $documentId, $rank)
    {
        $endpoint = "entite/$entityId/document/$documentId/file/annexes_tamponnees/$rank";

        return $this->get($endpoint, []);
    }

    /**
     * get documentSigné
     *
     * @param  int $entityId   The id of the structure on pastell
     * @param  int $documentId The id of the document on PASTELL
     * @return \Cake\Http\Client\Response
     */
    public function getSignedDocument($entityId, $documentId)
    {
        $endpoint = "entite/$entityId/document/$documentId/file/signature";

        return $this->get($endpoint, []);
    }

    /**
     * get documentSigné
     *
     * @param  int $entityId   The id of the structure on pastell
     * @param  int $documentId The id of the document on PASTELL
     * @return \Cake\Http\Client\Response
     */
    public function getSignBordereau($entityId, $documentId)
    {
        $endpoint = "entite/$entityId/document/$documentId/file/document_signe";

        return $this->get($endpoint, []);
    }

    /**
     * get Annexe sortie I-Parapheur
     *
     * @param  int $entityId   The id of the structure on pastell
     * @param  int $documentId The id of the document on PASTELL
     * @return \Cake\Http\Client\Response
     */
    public function getIparapheurAnnexeSortie(int $entityId, string $documentId)
    {
        $endpoint = "entite/$entityId/document/$documentId/file/iparapheur_annexe_sortie/";

        return $this->get($endpoint, []);
    }

    /**
     * get IparapheurHistorique last status except the Archive Status
     * when the return status is "Signe", then the key "signature_date" will be filled with the signature date
     *
     * @param  int $entityId   entityId
     * @param  string $documentId documentId
     * @return array containing status and annotation (and sometimes signature_date)
     */
    public function getIParapheurStatus($entityId, $documentId)
    {
        Log::write('info', '*******' . __METHOD__ . '*******', 'signature');

        $response = $this->getIparapheurHistoriqueFile($entityId, $documentId);

        $status = [];

        if ($response->getStatusCode() == '200' && !empty($response->getXml())) {
            $xml = simplexml_load_string($response->getStringBody());
            if ($xml->LogDossier->LogDossier[count($xml->LogDossier->LogDossier) - 1]->status == 'Archive') {
                // On suppose que l'étape de signature est l'étape précédant le statut Archive
                $logLine = $xml->LogDossier->LogDossier[count($xml->LogDossier->LogDossier) - 2];
                $status['status'] = $logLine->status->__toString();
                $status['annotation'] = $logLine->annotation->__toString();
                $status['signature_date'] = (new FrozenTime($logLine->timestamp->__toString()))
                    ->setTimezone(Configure::read('App.defaultTimezone'));
            } else {
                $logLine = $xml->LogDossier->LogDossier[count($xml->LogDossier->LogDossier) - 1];
                $status['status'] = $logLine->status->__toString();
                $status['annotation'] = $logLine->annotation->__toString();
            }
        } else {
            $status['status'] = 'erreur';
            $status['annotation'] = "L'historique n'est pas accessible pour le document";
        }

        return $status;
    }

    /**
     * get Annexe sortie I-Parapheur
     *
     * @param  int $entityId   The id of the structure on pastell
     * @param  int $documentId The id of the document on PASTELL
     * @return \Cake\Http\Client\Response
     */
    public function getIparapheurHistoriqueFile(int $entityId, string $documentId)
    {
        $endpoint = "entite/$entityId/document/$documentId/file/iparapheur_historique";

        return $this->get($endpoint, []);
    }

    /**
     * @param  string $idOrchestration The id of the entity
     * @param  string $idDocument      The id of the document on PASTELL
     * @return \Cake\Http\Client\Response
     */
    public function getIparapheurSousTypesByDocument($idOrchestration, $idDocument): Response
    {
        $endpoint = "entite/$idOrchestration/document/$idDocument/externalData/iparapheur_sous_type";

        return $this->get($endpoint, []);
    }

    /**
     * @param  int $idOchestration Identifiant de la structure sur PASTELL
     * @param  int    $documentId     Identifiant du document sur PASTELL
     * @param  string $field          Label du champ à
     *                                modifier
     * @param  array  $data           Données
     *                                à
     *                                modifier
     * @return \Cake\Http\Client\Response
     */
    public function patchExternalData(int $idOchestration, string $documentId, string $field, $data): Response
    {
        $endpoint = "entite/$idOchestration/document/$documentId/externalData/$field";

        return $this->patch($endpoint, [], $data);
    }

    /**
     * Supprime une annexe (autre_document_attache) sur PASTELL
     *
     * @param int $idOrchestration Identifiant de la structure sur PASTELL
     * @param string $idDocument  Identifiant du document sur PASTELL
     * @param int $key Numéro sur PASTELL du document attache
     * @return \Cake\Http\Client\Response
     */
    public function deleteAnnexe(int $idOrchestration, string $idDocument, int $key)
    {
        $endpoint = "entite/$idOrchestration/document/$idDocument/file/autre_document_attache/$key";

        return $this->delete($endpoint, []);
    }

    /**
     * @param  string $idOrchestration The id of the entity on pastell
     * @param  string $type            The type of connector
     * @return \Cake\Http\Client\Response
     */
    private function getConnectorByType(string $idOrchestration, string $type): Response
    {
        $endpoint = "entite/$idOrchestration/flux?flux=actes-generique&type=$type";

        return $this->get($endpoint, []);
    }
}
