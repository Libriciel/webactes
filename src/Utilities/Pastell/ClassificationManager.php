<?php
declare(strict_types=1);

namespace App\Utilities\Pastell;

use Cake\Utility\Hash;
use Cake\Utility\Xml;

class ClassificationManager
{
    public function getClassificationPastell(string $responseClassification_body): array
    {
        $xmlArray = Xml::toArray(Xml::build($responseClassification_body));

        return [
            'DateClassification' => $xmlArray['RetourClassification']['actes:DateClassification'],
            'NaturesActes' => $this->getNatures(
                $xmlArray['RetourClassification']['actes:NaturesActes']['actes:NatureActe']
            ),
            'Matieres' => $this->getMatieres(
                1,
                null,
                $xmlArray['RetourClassification']['actes:Matieres']['actes:Matiere1']
            ),
        ];
    }

    private function getNatures(array $natures): array
    {
        $liste = [];
        foreach ($natures as $nature) {
            $liste[] = [
                'name' => $nature['@actes:Libelle'],
                'code' => $nature['@actes:TypeAbrege'],
                'codenatureacte' => $nature['@actes:CodeNatureActe'],
                'typesPJNatureActe' => $this->getTypePJNatureActes($nature['actes:TypePJNatureActe']),
            ];
        }

        return $liste;
    }

    private function getTypePJNatureActes(array $typePJNatureActes): array
    {
        $liste = [];
        foreach ($typePJNatureActes as $typePJNatureActe) {
            $liste[] = [
                'name' => $typePJNatureActe['@actes:Libelle'],
                'code' => $typePJNatureActe['@actes:CodeTypePJ'],
            ];
        }

        return $liste;
    }

    private function getMatieres($start, $key, array $aMatiere): array
    {
        $liste = [];
        $i = 0;
        foreach ($aMatiere as $matiere) {
            $name = (!empty($key) ? $key . '.' : '') . $matiere['@actes:CodeMatiere'];
            $liste[$i] = [
                'name' => $name,
                'libelle' => $name . ' ' . $matiere['@actes:Libelle'],
            ];

            $rangChild = $start + 1;
            $matiereIndex = 'actes:Matiere' . $rangChild;
            if (isset($matiere[$matiereIndex])) {
                $matiereOrdonne = $this->getMatieresOrdonne(
                    $matiere['actes:Matiere' . $rangChild],
                    'actes:Matiere' . $rangChild
                );
                $liste[$i]['children'] = $this->getMatieres($rangChild, $name, $matiereOrdonne);
            }
            $i++;
        }

        return $liste;
    }

    private function getMatieresOrdonne($matiere, $key): array
    {
        if (count($matiere) === 1) {
            if (count($matiere[$key]) === 1) {
                return [$matiere[$key]];
            }

            return $matiere[$key];
        }
        if (isset($matiere['@actes:CodeMatiere'])) {
            return [$matiere];
        }

        return Hash::sort($matiere, '{n}.@actes:CodeMatiere', SORT_ASC);
    }
}
