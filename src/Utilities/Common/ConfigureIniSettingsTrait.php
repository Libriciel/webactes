<?php
declare(strict_types=1);

namespace App\Utilities\Common;

use Cake\Core\Configure;
use Cake\Log\Log;

trait ConfigureIniSettingsTrait
{
    /**
     * @var string
     */
    protected static $configureIniSettingsPrefix = 'ConfigureIniSettings';

    /**
     * Gets the possible ini_set values with respect to the partial namespace.
     * The values are configured with the ConfigureIniSettings prefix using the Configure class.
     *
     * @param string $method The namespaced prefixed method name
     * @return array
     */
    public static function getConfigureIniSettings(string $method): array
    {
        $tokens = preg_split('/::/', $method);
        if (isset($tokens[1])) {
            $methodName = $tokens[1];
        } else {
            $methodName = null;
        }

        $tokens = array_filter(preg_split('/\\\\/', $tokens[0]));
        $keys = [];
        $accumulator = '';
        foreach ($tokens as $token) {
            $accumulator .= '\\' . $token;
            $keys[] = static::$configureIniSettingsPrefix . '.'
                . preg_replace('/^\\\\/', '', $accumulator);
        }
        if (empty($methodName) === false) {
            $keys[] = static::$configureIniSettingsPrefix . '.'
                . preg_replace('/^\\\\/', '', $accumulator) . '::' . $methodName;
        }

        $settings = [];
        foreach ($keys as $key) {
            if (Configure::check($key)) {
                $settings = array_merge($settings, (array)Configure::read($key));
            }
        }

        return $settings;
    }

    /**
     * Applies the possible ini_set values with respect to the partial namespace.
     * The values are configured with the ConfigureIniSettings prefix using the Configure class.
     *
     * @param string $method The namespaced prefixed method name
     * @return bool
     */
    public function applyConfigureIniSettings(string $method): bool
    {
        $result = true;
        $values = $this->getConfigureIniSettings($method);
        foreach ($values as $key => $value) {
            $before = ini_set($key, $value);
            if ($before !== false) {
                Log::debug(sprintf('ini_set %s: %s for method %s succeeded', $key, (string)$value, $method));
            } else {
                Log::error(sprintf('ini_set %s: %s for method %s failed', $key, (string)$value, $method));
            }
            $result = $result && $before !== false;
        }

        return $result;
    }
}
