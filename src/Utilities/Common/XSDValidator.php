<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 29/08/19
 * Time: 17:21
 */

namespace App\Utilities\Common;

use Cake\Log\Log;
use DOMDocument;
use Exception;

class XSDValidator
{
    /**
     * @param string $schema The path to the schema file
     * @param string $xml The xml
     * @return bool
     * @throws \Exception
     */
    public static function schemaValidate(string $schema, string $xml): bool
    {
        $tempXml = tempnam(WORKSPACE, 'socle');
        //$pathXml = stream_get_meta_data($tempXml)['uri'];
        $handle = fopen($tempXml, 'w');
        fwrite($handle, $xml);

        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->load($tempXml);
        $err = $dom->schemaValidate($schema);
        if (!$err) {
            Log::info('SCHEMA VALIDATE', 'EXCEPTION_SOCLE');

            $last_error = libxml_get_errors();
            $msg = ' ';
            foreach ($last_error as $err) {
                $msg .= "[Erreur #{$err->code}] " . $err->message . "\n";
            }
            fclose($handle);
            throw new Exception($msg);
        }
        fclose($handle);

        return true;
    }
}
