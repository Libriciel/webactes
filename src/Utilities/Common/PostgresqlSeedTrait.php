<?php
declare(strict_types=1);

namespace App\Utilities\Common;

trait PostgresqlSeedTrait
{
    /**
     * Affiche la requête complète en cas d'exception.
     *
     * @param string $sql La reqête SQL
     * @return int
     * @throws \App\Utilities\Common\Throwable
     */
    protected function postgresqlSafeExecute($sql)
    {
        try {
            return parent::execute($sql);
        } catch (\Throwable $exc) {
            print_r("\n{$sql}\n");
            throw $exc;
        }
    }

    /**
     * Récupère les informations concernant les tables et leurs séquences.
     *
     * @param array $tables Une liste de tables ou un array vide si on les veut toutes
     * @return mixed
     */
    protected function postgresqlGetTablesSequencesInfos(array $tables = [])
    {
        $conditions = '';
        if (empty($tables) === false) {
            $conditions = "AND table_name IN ('" . implode("', '", $tables) . "')";
        }

        $sql = "SELECT
                    table_name AS \"table\",
                    column_name	AS \"column\",
                    column_default AS \"sequence\"
                    FROM information_schema.columns
                    WHERE table_schema = 'public'
                        AND column_default LIKE 'nextval(%::regclass)'
                        " . $conditions . "
                    ORDER BY table_name, column_name";

        return $this->fetchAll($sql);
    }

    /**
     * Met à jour la séquence pour une table donnée.
     *
     * @param string $table Le nom de la table
     * @param string $column Le nom de la colonne
     * @param string $sequence Le nom ou la définition de la séquence
     * @return int
     * @throws \App\Utilities\Common\Throwable
     */
    protected function postgresqlFixTableSequence($table, $column = null, $sequence = null)
    {
        if ($column === null || $sequence === null) {
            $row = $this->postgresqlGetTablesSequencesInfos([$table]);
            $column = $row['column'];
            $sequence = $row['sequence'];
        }
        $sequence = preg_replace('/^nextval\(\'(.*)\'.*\)$/', '\1', $sequence);
        $sql = "SELECT setval('{$sequence}', COALESCE(MAX({$column}),0)+1, false) FROM {$table};";

        return $this->postgresqlSafeExecute($sql);
    }

    /**
     * Mise à jour des séquences postgresql vis-à-vis de la dernière valeur des clés primaires.
     *
     * @return void
     */
    protected function sequences()
    {
        $this->postgresqlSafeExecute('BEGIN;');
        $sql = "SELECT table_name AS \"table\",
						column_name	AS \"column\",
						column_default AS \"sequence\"
						FROM information_schema.columns
						WHERE table_schema = 'public'
							AND column_default LIKE 'nextval(%::regclass)'
						ORDER BY table_name, column_name";
        foreach ($this->postgresqlGetTablesSequencesInfos() as $row) {
            $this->postgresqlFixTableSequence($row['table'], $row['column'], $row['sequence']);
        }
        $this->postgresqlSafeExecute('COMMIT;');
    }
}
