<?php
declare(strict_types=1);

namespace App\Utilities\Common;

use InvalidArgumentException;

abstract class Bytesize
{
    /**
     * The regular expression that matches a byte size value.
     */
    protected const VALUE_REGEXP = '/^(?<value>([0-9]+)(\.[0-9]+){0,1})(?<unit>[a-z]*b{0,1})$/';

    /**
     * @var string[]
     */
    protected static $units = [
        1 => 'k',
        2 => 'm',
        3 => 'g',
        4 => 't',
        5 => 'p',
        5 => 'e',
    ];

    /**
     * Checks that the given value is valid (no unit or one of K|KB, M|MB, G|GB, T|TB, P|PB, E|EB), for example to use
     * with ini_set('memory_limit', $value);
     *
     * @param float|int|string $value The value to check
     * @return bool
     */
    public static function isValid($value): bool
    {
        return preg_match(static::VALUE_REGEXP, mb_strtolower((string)$value), $matches) === 1;
    }

    /**
     * Converts a value (no unit or one of K|KB, M|MB, G|GB, T|TB, P|PB, E|EB) to bytes.
     *
     * @param float|int|string $value The value to convert
     * @return int
     * @throws \InvalidArgumentException
     */
    public static function to($value): int
    {
        $matches = [];
        if (preg_match(static::VALUE_REGEXP, mb_strtolower((string)$value), $matches) === 1) {
            $matches = array_filter($matches, 'is_string', ARRAY_FILTER_USE_KEY);

            return (int)($matches['value'] * pow(1024, array_search($matches['unit'], static::$units)));
        }

        throw new InvalidArgumentException(sprintf('Cannot convert value "%s" to bytes', (string)$value));
    }

    /**
     * Converts a value in bytes to its human-readable form.
     *
     * @see https://stackoverflow.com/a/2510540
     * @param int $bytes The value in bytes
     * @param int $precision The precision needed
     * @return string
     */
    public static function from(int $bytes, int $precision = 2)
    {
        $base = log($bytes, 1024);
        $suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB'];

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }
}
