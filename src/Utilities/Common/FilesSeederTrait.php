<?php
declare(strict_types=1);
// @codingStandardsIgnoreFile

namespace App\Utilities\Common;

use Cake\Log\Log;
use Ramsey\Uuid\Uuid;
use RuntimeException;
use SplFileInfo;

/**
 * @package App\Utilities\Common
 */
trait FilesSeederTrait
{

    /**
     * @var string
     */
    protected string $pathFile = TESTS . 'Fixture' . DS . 'files' . DS;

    /**
     * @param $pathFile
     * @param $key
     * @param $organizationId
     * @param $structureId
     * @param $Id
     * @return SplFileInfo
     */
    protected function createFile($pathFile, $key , $organizationId , $structureId, $Id): SplFileInfo
    {
        $file = $this->putFilePath(
            $pathFile,
            '/data/workspace/'. $organizationId . DS . $structureId . DS . $key . DS .  $Id . DS
        );
        return $this->fileInfo($file);
    }

    /**
     * @param $pathOrigin
     * @param $path
     * @return string
     */
    protected function putFilePath($pathOrigin, $path): string
    {
        if (file_exists($path) === false) {
            mkdir($path, 0777, true);
        }
        try {
            $uuid = Uuid::uuid4()->toString();
        } catch (\Exception $e) {
            return '';
        }

        copy($pathOrigin, $path . $uuid);

        return $path . $uuid;
    }

    protected function fileInfo($path): SplFileInfo
    {
        $file = new SplFileInfo($path);

        if (!$file->isFile()) {
            $message = sprintf('%s:%d file is empty: %s', __FILE__, __LINE__, $path);
            throw new RuntimeException($message);
        }

        return $file->getFileInfo();
    }

}
