<?php
declare(strict_types=1);

namespace App\Utilities\Common;

use Cake\I18n\I18nDateTimeInterface;
use DateTime;
use DateTimeZone;

/**
 * Classe utilitaire permettant d'utiliser le fuseau horaire correct car par défaut, dans l'application le fuseau
 * horaire doit être UTC (base de données, envoi au front-end angular).
 * - pour les générations et l'envoi au connecteur idelibre
 * - pour la récupération des dates de lecture des mails sécurisés Pastell
 *
 * Il faut utiliser les variables d'environnement ci-dessous pour utiliser cette fonctionnalité:
 * - APP_DEFAULT_TIMEZONE (UTC par défaut)
 * - APP_REAL_TIMEZONE (Europe/Paris par défaut)
 * - PASTELL_TIMEZONE (Europe/Paris par défaut)
 */
abstract class RealTimezone
{
    /**
     * Retourne la valeur de la variable d'environnement APP_REAL_TIMEZONE ou la valeur par défaut Europe/Paris.
     *
     * @return string
     */
    protected static function getRealTimezone()
    {
        return (string)env('APP_REAL_TIMEZONE', 'Europe/Paris');
    }

    /**
     * Retourne la valeur de la variable d'environnement APP_DEFAULT_TIMEZONE ou la valeur par défaut UTC.
     *
     * @return string
     */
    protected static function getAppTimezone()
    {
        return (string)env('APP_DEFAULT_TIMEZONE', 'UTC');
    }

    /**
     * Retourne la valeur de la variable d'environnement PASTELL_TIMEZONE ou la valeur par défaut Europe/Paris.
     *
     * @return string
     */
    protected static function getPastellTimezone()
    {
        return (string)env('PASTELL_TIMEZONE', 'Europe/Paris');
    }

    /**
     * Conversion fuseau horaire, Pastell renvoie en Europe/Paris (PASTELL_TIMEZONE) et on veut enregistrer en UTC
     * (APP_DEFAULT_TIMEZONE).
     *
     * @param string $datetime Une chaîne de caractères contenant une date et une heure.
     * @return string
     * @throws \Exception
     */
    public static function fromPastellToSql($datetime)
    {
        $result = new DateTime($datetime, new DateTimeZone(static::getPastellTimezone()));
        $result->setTimezone(new DateTimeZone(static::getAppTimezone()));

        return $result->format('Y-m-d H:i:s');
    }

    /**
     * Formate une date en fonction de APP_REAL_TIMEZONE.
     *
     * @param \Cake\I18n\I18nDateTimeInterface $value La valeur à formater
     * @param string $format Format accepté par {@link https://secure.php.net/manual/en/function.date.php date()}.
     * @return string
     */
    public static function format(I18nDateTimeInterface $value, string $format)
    {
        return $value->setTimezone(static::getRealTimezone())->format($format);
    }

    /**
     * Formate et traduit une date en fonction de APP_REAL_TIMEZONE.
     *
     * @param \Cake\I18n\I18nDateTimeInterface $value La valeur à formater
     * @param string $format Format accepté par
     *  {@link https://unicode-org.github.io/icu/userguide/format_parse/datetime/#date-field-symbol-table
     *  IntlDateFormatter::format()}.
     * @param string|\DateTimeZone|null $timezone $timezone Le fuseau horaire (APP_REAL_TIMEZONE) par défaut
     * @param string|null $locale Le nom de la locale à utiliser (ex. pt-BR)
     * @return int|string
     */
    public static function i18nFormat(I18nDateTimeInterface $value, $format = null, $timezone = null, $locale = null)
    {
        return $value->i18nFormat($format, $timezone ?? static::getRealTimezone(), $locale);
    }
}
