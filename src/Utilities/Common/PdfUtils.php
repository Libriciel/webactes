<?php
declare(strict_types=1);

namespace App\Utilities\Common;

use Cake\Core\Configure;
use Cake\Log\Log;
use RuntimeException;
use function App\safe_shell_exec as safe_shell_exec;

abstract class PdfUtils
{
    /**
     * Retourne le nombre de pages d'un document PDF, grâce à la commande pdfinfo dont le chemin doit être configuré
     * grâce à la classe Configure de CakePHP sous le chemin PDFINFO_EXEC.
     *
     * @param string $path Le chemin vers le document PDF
     * @return int
     * @throws \RuntimeException
     */
    public static function getPageCount(string $path): int
    {
        try {
            $cmd = sprintf('%s %s 2>&1', escapeshellarg(Configure::read('PDFINFO_EXEC')), escapeshellarg($path));
            $output = safe_shell_exec($cmd);

            $matches = [];
            $pattern = '/^Pages:\s+([0-9]+)\s*$/m';
            if (preg_match($pattern, $output, $matches) !== 1) {
                $message = sprintf(
                    "Could not find pattern \"%s\" in command \"%s\" output\n%s",
                    $pattern,
                    $cmd,
                    $output
                );
                throw new RuntimeException($message);
            }

            return (int)$matches[1];
        } catch (\Throwable $exc) {
            $excMessage = $exc->getMessage();
            Log::error($excMessage);

            $message = 'Erreur lors de l\'extraction du nombre de pages du PDF';
            if (preg_match('/May not be a PDF file/im', $excMessage) === 1) {
                $message = "{$message} (le fichier ne semble pas être un fichier PDF)";
            } elseif (preg_match('/Incorrect password/im', $excMessage) === 1) {
                $message = "{$message} (le fichier est protégé par mot de passe)";
            }

            throw new RuntimeException($message);
        }
    }
}
