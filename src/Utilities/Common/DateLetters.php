<?php
declare(strict_types=1);

namespace App\Utilities\Common;

use Cake\I18n\FrozenTime;

abstract class DateLetters
{
    /**
     * Retourne la date en francais et en toute lettre de la date. Seules les dates à quatre chiffres sont gérées.
     *
     * @param \Cake\I18n\FrozenTime $value La valeur à formater
     * @return string date en toute lettre
     */
    public static function format(FrozenTime $value): string
    {
        $timestamp = $value->toUnixString();

        $days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
        $months = ['', 'janvier', 'février', 'mars', 'avril', 'mai', 'juin',
            'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
        $jour = $days[date('w', (int)$timestamp)];
        $nbJour = date('d', (int)$timestamp);
        // jour en toute lettre
        $nbJour = static::dizaines($nbJour);
        $mois = $months[date('n', (int)$timestamp)];
        // année en toute lettre
        $nbAnnee = date('Y', (int)$timestamp);
        if (substr($nbAnnee, 0, 1) == 0) {
            $annee = '';
        } elseif (substr($nbAnnee, 0, 1) == 1) {
            $annee = 'mille' . ' ';
        } else {
            $annee = static::unite(substr($nbAnnee, 0, 1)) . ' ' . 'mille' . ' ';
        }
        if (substr($nbAnnee, 1, 1) == 0) {
            $annee .= '';
        } elseif (substr($nbAnnee, 1, 1) == 1) {
            $annee .= 'cent' . ' ';
        } else {
            $annee .= static::unite(substr($nbAnnee, 1, 1)) . ' ' . 'cent' . ' ';
        }
        $annee .= static::dizaines(substr($nbAnnee, 2));

        return "L'an $annee, le $nbJour $mois ";
    }

    /**
     * Renvoie les dizaines en toutes lettres
     *
     * @param string $number le chiffre à transformer
     * @return string|bool soit le chiffre voulu en lettre soit false
     */
    protected static function dizaines(string $number)
    {
        $ret = static::latinIrregular($number);
        if (!$ret) {
            $et = '';
            if (substr($number, 1) == 1) {
                $et = 'et ';
            }
            switch (substr($number, 0, 1)) {
                case '0':
                    return '' . static::unite(substr($number, 1));
                case '1':
                    return 'dix' . ' ' . static::unite(substr($number, 1));
                case '2':
                    return 'vingt' . ' ' . $et . static::unite(substr($number, 1));
                case '3':
                    return 'trente' . ' ' . $et . static::unite(substr($number, 1));
                case '4':
                    return 'quarante' . ' ' . $et . static::unite(substr($number, 1));
                case '5':
                    return 'cinquante' . ' ' . $et . static::unite(substr($number, 1));
                case '6':
                    return 'soixante' . ' ' . $et . static::unite(substr($number, 1));
                case '7':
                    return 'soixante' . ' ' . $et . static::dizaines('1' . substr($number, 1));
                case '8':
                    return 'quatre vingt' . ' ' . static::unite(substr($number, 1));
                case '9':
                    return 'quatre vingt' . ' ' . static::dizaines('1' . substr($number, 1));
            }
        } else {
            return $ret;
        }

        return false;
    }

    /**
     * Renvoie les unitées en lettre
     *
     * @param string $number le chiffre à transformer
     * @return string|bool soit le chiffre voulu en lettre soit false
     */
    protected static function unite($number)
    {
        switch ($number) {
            case 0:
                return '';
            case 1:
                return 'un';
            case 2:
                return 'deux';
            case 3:
                return 'trois';
            case 4:
                return 'quatre';
            case 5:
                return 'cinq';
            case 6:
                return 'six';
            case 7:
                return 'sept';
            case 8:
                return 'huit';
            case 9:
                return 'neuf';
        }

        return false;
    }

    /**
     * change des chiffres précis en lettre
     *
     * @param string $number le chiffre à transformer
     * @return string|bool soit le chiffre voulu en lettre soit false
     */
    protected static function latinIrregular(string $number)
    {
        switch ($number) {
            case '11':
                return 'onze';
            case '12':
                return 'douze';
            case '13':
                return 'treize';
            case '14':
                return 'quatorze';
            case '15':
                return 'quinze';
            case '16':
                return 'seize';
        }

        return false;
    }
}
