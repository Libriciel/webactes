<?php
declare(strict_types=1);

namespace App\Validation;

use Cake\Utility\Inflector;
use Cake\Validation\Validator as CakeValidator;

/**
 * Surcharge de la classe Cake\Validation\Validator, ajout de
 * - messages d'erreur plus explicites par défaut pour les règles de validation
 *   - comparison (equals, greaterThan, greaterThanOrEqual, lessThan, lessThanOrEqual, notEquals)
 *   - inList
 * - ajout des méthodes
 *   - allowPresence: permet la présence d'un champ (à utiliser avec la méthode "only")
 *   - allowPagination: ajout de règles de validation concernant les paramètres de pagination (direction, limit, page, sort)
 *   - only: vérifie que les données ne possèdent que les clés / noms de champs possédant des règles de validation, ou
 *       dont la présence est permise (par exemple grâce à la méthode "allowPresence")
 */
class Validator extends CakeValidator
{
    /**
     * La valeur maximale d'un entier pour les paramètres de pagination (limit et page).
     */
    public const MAX_PAGINATION_INT = 32767;

    /**
     * Permet de savoir si on accepte uniquement les champs pour lesquels il existe au moins une règle de validation.
     *
     * @var bool
     */
    protected $_only = false;

    /**
     * Change la valeur de l'attribut $only.
     *
     * @param bool $value La nouvelle valeur (true par défaut)
     * @return $this
     */
    public function only(bool $value = true)
    {
        $this->_only = $value;

        return $this;
    }

    /**
     * Surcharge de la méthode validate pour permettre l'utilisation de "only".
     *
     * @param array $data The data to be checked for errors
     * @param bool $newRecord whether the data to be validated is new or to be updated.
     * @return array[] Array of failed fields
     */
    public function validate(array $data, bool $newRecord = true): array
    {
        $errors = parent::validate($data, $newRecord);

        if ($this->_only === true) {
            $expected = array_keys($this->_fields);
            $actual = array_keys($data);
            $diffs = array_diff($actual, $expected);

            sort($expected);
            sort($diffs);

            if (empty($diffs) === false) {
                foreach ($diffs as $diff) {
                    if (empty($expected) === true) {
                        $errors[$diff]['_unexpected'] = sprintf(
                            'Unexpected parameter "%s", there is no accepted parameter',
                            $diff,
                        );
                    } else {
                        $errors[$diff]['_unexpected'] = sprintf(
                            'Unexpected parameter "%s", accepted parameters: "%s"',
                            $diff,
                            implode('", "', $expected),
                        );
                    }
                }
            }
        }

        return $errors;
    }

    /**
     * Retourne le message d'erreur par défaut associé à une règle de validation comparison.
     *
     * @param string $field Le champ concerné par la règle de validation
     * @param array $rule La règle de validation
     * @return string|null
     */
    protected function getDefaultMessageComparison(string $field, array $rule)
    {
        return sprintf(
            'The provided value is invalid, must be %s %s',
            (string)$rule['rule'][1],
            (string)$rule['rule'][2],
        );
    }

    /**
     * Retourne le message d'erreur par défaut associé à une règle de validation inList.
     *
     * @param string $field Le champ concerné par la règle de validation
     * @param array $rule La règle de validation
     * @return string|null
     */
    protected function getDefaultMessageInList(string $field, array $rule)
    {
        return sprintf(
            'The provided value is invalid, use one of "%s"',
            implode('", "', $rule['rule'][1])
        );
    }

    /**
     * Retourne le message d'erreur par défaut associé à une règle de validation.
     *
     * @param string $field Le champ concerné par la règle de validation
     * @param array $rule La règle de validation
     * @return string|null
     */
    protected function getDefaultMessage(string $field, array $rule)
    {
        $name = is_string($rule['rule']) ? $rule['rule'] : $rule['rule'][0];
        $method = __FUNCTION__ . Inflector::camelize($name);

        if (method_exists($this, $method) === true) {
            return $this->{$method}($field, $rule);
        } else {
            return null;
        }
    }

    /**
     * Surcharge de la méthode add afin d'ajouter les messages d'erreurs par défaut (si non spécifiés dans cet appel de
     * fonction).
     *
     * @see Validator::getDefaultMessage()
     * @param string $field The name of the field from which the rule will be added
     * @param array|string $name The alias for a single rule or multiple rules array
     * @param array|\Cake\Validation\ValidationRule $rule the rule to add
     * @throws \InvalidArgumentException If numeric index cannot be resolved to a string one
     * @return $this
     */
    public function add(string $field, $name, $rule = [])
    {
        if (!is_array($name)) {
            $rules = [$name => $rule];
        } else {
            $rules = $name;
        }

        foreach ($rules as $name => $rule) {
            if (is_array($rule)) {
                $rule += [
                    'rule' => $name,
                    'message' => null,
                ];
                if ($rule['message'] === null) {
                    $rule['message'] = $this->getDefaultMessage($field, $rule);
                }
                $rules[$name] = $rule;
            }
        }

        return parent::add($field, $rules);
    }

    /**
     * Doit-on permettre les paramètres de pagination: direction, limit, page et sort, avec les règles de gestion
     * associées: nombre entier positif et plus petit que MAX_PAGINATION_INT (32767) pour "limit" et "page", valeur de
     * "direction" soit "asc" soit "desc" ?
     *
     * @param bool $allowPagination true pour accepter les paramètres de pagination
     * @return $this
     */
    public function allowPagination(bool $allowPagination)
    {
        if ($allowPagination === true) {
            $this
                ->allowPresence('direction')
                ->inList('direction', ['asc', 'desc'])
                ->allowPresence('limit')
                ->add('limit', 'integer', ['last' => true, 'rule' => 'isInteger'])
                ->greaterThan('limit', 0)
                ->lessThanOrEqual('limit', static::MAX_PAGINATION_INT)
                ->allowPresence('offset')
                ->add('offset', 'integer', ['last' => true, 'rule' => 'isInteger'])
                ->allowPresence('page')
                ->add('page', 'integer', ['last' => true, 'rule' => 'isInteger'])
                ->greaterThan('page', 0)
                ->lessThanOrEqual('page', static::MAX_PAGINATION_INT)
                ->allowPresence('sort');
        }

        return $this;
    }

    /**
     * Permettre la présence du champ, utilisé avec "only".
     *
     * @param string|array $field the name of the field or list of fields.
     * @return $this
     */
    public function allowPresence($field)
    {
        return $this->requirePresence($field, false);
    }
}
