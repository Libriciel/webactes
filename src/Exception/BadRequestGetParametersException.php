<?php
declare(strict_types=1);

namespace App\Exception;

use Exception;
use Throwable;

/**
 * Cette classe permet de signaler des erreurs dans les paramètres GET envoyés, sous la forme d'une erreur 400 au format
 * JSON contenant la liste des erreurs dans le corps de la réponse sous la clé "errors", sous la même forme que les
 * erreurs de validation d'entité.
 */
class BadRequestGetParametersException extends Exception
{
    /**
     * @var array[]
     */
    protected $errors = [];

    /**
     * Constructeur de l'exception, dans lequel on passe la liste des erreurs de validation des paramètres get?
     *
     * @param array $message Les erreurs de validation à transmettre dans le corps de la réponse
     * @param int $code Le code HTTP à retourner (par défaut: 400)
     * @param \Throwable|null $previous L'erreur précédente
     */
    public function __construct($message = '', $code = 400, ?Throwable $previous = null)
    {
        $this->errors = $message;
        parent::__construct('', $code, $previous);
    }

    /**
     * Retourne la liste des erreurs, pour chacun des paramètres GET en erreur.
     *
     * @return array[]
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
