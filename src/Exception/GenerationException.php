<?php
declare(strict_types=1);

namespace App\Exception;

use Cake\Core\Exception\CakeException;

class GenerationException extends CakeException
{
    /**
     * @inheritDoc
     */
    protected $_messageTemplate = 'Generation failure: %s';

    /**
     * Passed in foreign key
     *
     * @var int
     */
    protected ?int $_foreignKey;

    /**
     * Passed in type
     *
     * @var string
     */
    protected ?string $_type;

    /**
     * Constructs the exception.
     *
     * @param string $type The type of generation. Look at the inList validation rule to get accepted values.
     * @param int $foreignKey The primary key for the generation (projects.id, sittings.id)
     * @param string|array $message Either the string of the error message, or an array of attributes
     *   that are made available in the view, and sprintf()'d into Exception::$_messageTemplate
     * @param ?int $code The code of the error, is also the HTTP status code for the error.
     * @param ?\Throwable|null $previous the previous exception.
     */
    public function __construct(string $type, int $foreignKey, $message, ?int $code = null, ?Throwable $previous = null)
    {
        $this->_foreignKey = $foreignKey;
        $this->_type = $type;

        parent::__construct($message, $code ?? 400, $previous);
    }

    /**
     * Get the passed in foreign key
     *
     * @return int
     */
    public function getForeignKey(): int
    {
        return $this->_foreignKey;
    }

    /**
     * Get the passed in type
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->_type;
    }
}
