<?php
declare(strict_types=1);

namespace App\ORM;

use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;

abstract class AbstractEntityWithAvailableActions extends Entity
{
    use LocatorAwareTrait;

    /**
     * Liste des sous-champs virtuels retournés par _getAvailableActions()
     *
     * En clé, le nom du champ, en valeur le nom de la méthode public permettant de récupérer l'information.
     *
     * @var string[]
     */
    protected $_availableActions = [];

    /**
     * Retourne la liste des availableActions disponibles pour cette entité sous forme de couple clé pour l'API, nom de
     * méthode.
     *
     * @return string[]
     */
    public function getAvailableActionsList()
    {
        return $this->_availableActions;
    }

    /**
     * Tableau d'actions possibles par l'utilisateur
     *
     * @return array
     */
    protected function _getAvailableActions(): array
    {
        $return = [];
        foreach ($this->_availableActions as $key => $method) {
            $return[$key] = $this->{$method}();
        }

        return $return;
    }
}
