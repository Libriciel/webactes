<?php
declare(strict_types=1);

namespace App\Log\Engine;

class ConsoleLog extends \Cake\Log\Engine\ConsoleLog
{
    /**
     * @inheritDoc
     */
    public function log($level, $message, array $context = [])
    {
        $scope = isset($context['scope']) === true && empty($context['scope']) === false
            ? (array)$context['scope']
            : null;
        $output = $this->formatter->format(
            strtoupper($level),
            (empty($scope) === true ? '' : json_encode(compact('scope')) . ' ')
            . $this->interpolate($message, $context),
            $context
        );
        $this->_output->write($output);
    }
}
