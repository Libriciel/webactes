<?php
declare(strict_types=1);

namespace App\Controller;

use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;

/**
 * Containers Controller
 *
 * @property \App\Model\Table\ContainersTable $Containers
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Model\Entity\Container[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContainersController extends AppController
{
    /**
     * @throws \Beanstalk\Exception\CantWorkException
     * @throws \ReflectionException
     * @return mixed
     */
    public function prepareDocumentTdt()
    {
        $this->request->allowMethod('put');

        $beanstalk = new Beanstalk('basic-pastell');

        $containerList = $this->getRequest()->getData();

        foreach ($containerList['data'] as $container) {
            $data = [
                'flux' => Configure::read('Flux.actes-generique.name'),
                'flux_name' => Configure::read('Flux.actes-generique.name'),
                'controller' => 'containers',
                'structure_id' => $this->Multico->getCurrentStructureId(),
                'object_id' => $container['id'],
                'action' => 'prepareDocumentOnPastell',
            ];
            $responseId = $beanstalk->emit($data);
            $this->set(compact('responseId'));
        }
    }
}
