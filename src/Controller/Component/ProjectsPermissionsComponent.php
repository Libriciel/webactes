<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Http\ServerRequest;

/**
 * Class ProjectsPermissionsComponent
 *
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\PermissionsComponent $Permissions
 * @package App\Controller\Component
 */
class ProjectsPermissionsComponent extends AppComponent
{
    /**
     * Components used by this component.
     *
     * @var array
     */
    public $components = ['Multico'];

    /**
     * @var array
     */
    protected $liveCache = [];

    /**
     * @param array $project project array
     * @return void
     */
    public function addCanApproveProject(array &$project)
    {
        $value = $this->Multico->isAdminInCurrentOrganization()
            && in_array($project['containers'][0]['stateacts'][0]['id'], [VALIDATION_PENDING]);
        $reason = $value ? null : 'Vous ne pouvez pas valider le projet';

        $project['availableActions']['can_approve_project'] = [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * set is deletable key to project
     *
     * @param array $project p
     * @return void
     */
    public function addCanBeDeleted(array &$project): void
    {
        $project['availableActions']['can_be_deleted']['value'] =
            (
                $this->Multico->isAdminInCurrentOrganization()
                ||
                in_array($project['containers'][0]['stateacts'][0]['id'], [DRAFT, REFUSED])
            )
            && $this->fetchTable('Containers')->isDeletable($project['containers'][0]['id']);

        $project['availableActions']['can_be_deleted']['data'] = $project['availableActions']['can_be_deleted']['value']
            ? null
            : 'le projet ne peut pas être supprimé.';
    }

    /**
     * set is editable key to project :
     *   User is admin
     *   or project is draft or refused
     *   or user is a current validator
     *
     * @param array $project p
     * @return void
     */
    public function addCanBeEdited(array &$project): void
    {
        $project['availableActions']['can_be_edited']['value'] =
            (
                $this->Multico->isAdminInCurrentOrganization()
                &&
                in_array(
                    $project['containers'][0]['stateacts'][0]['id'],
                    [
                        DRAFT,
                        VALIDATION_PENDING,
                        VALIDATED,
                        REFUSED,
                        PARAPHEUR_REFUSED,
                        VOTED_REJECTED,
                        VOTED_APPROVED,
                    ]
                )
            )
            ||
            in_array($project['containers'][0]['stateacts'][0]['id'], [DRAFT, REFUSED, VALIDATED])
            ||
            (
                !empty($project['instance'])
                &&
                $project['instance']['canPerformAnAction']
            );
        $project['availableActions']['can_be_edited']['data'] = $project['availableActions']['can_be_edited']['value']
            ? null
            : 'Le projet ne peut pas être modifié.';
    }

    /**
     * Return true if the user have the permission
     *
     * @param array $request mandatory data to calculate
     * @return bool
     */
    protected function checkPermissionForOneAction($request)
    {
        $userId = $this->Multico->getCurrentUserId();
        if (isset($this->liveCache[$userId][$request['url']]) === false) {
            $serverRequest = new ServerRequest(['url' => $request['url']]);
            $serverRequest = $serverRequest->withAttribute('params', [
                'plugin' => null,
                'controller' => $request['controller'],
                'action' => $request['action'],
            ]);

            $this->liveCache[$userId][$request['url']] = $this->getController()->isAuthorized(
                null,
                $serverRequest
            );
        }

        return $this->liveCache[$userId][$request['url']];
    }

    /**
     * Compute data for permission on available actions
     *
     * @param array $data data
     * @param array $availableActions list of available actions to compute
     * @return void
     */
    public function addChecks(array &$data, array $availableActions)
    {
        // Si l'état de la donnée permet l'action
        // On calcule si l'utilisateur peut effectuer l'action via les acl
        foreach ($availableActions as $key => $availableAction) {
            $data['availableActions'][$key] = $data['availableActions'][$key]['value']
                ? ['value' => $this->checkPermissionForOneAction($availableAction)]
                : $data['availableActions'][$key];
        }
    }
}
