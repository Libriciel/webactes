<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Exception\BadRequestGetParametersException;
use App\Model\Entity\Project;
use App\Model\Table\RolesTable;
use App\Validation\Validator;
use Cake\ORM\Query;
use WrapperFlowable\Api\WrapperFactory;

/**
 * This class manages pagination and searches for the GET /projects API entry.
 *
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\ProjectsPermissionsComponent $ProjectsPermissions
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @package App\Controller\Component
 */
class ProjectsIndexComponent extends AppComponent
{
    /**
     * Components used by this component.
     *
     * @var array
     */
    public $components = [
        'Multico',
        'ProjectsPermissions',
        'QueryUtils',
    ];

    /**
     * @var \App\Controller\Component\ProjectsTable
     */
    public $Projects = null;

    /**
     * @inheritDoc
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->Projects = $this->fetchTable('Projects');
    }

    /**
     * Returns the base query with filters for the GET parameters code_act (Projects.code_act), name (Projects.name) and
     * typeact_name (Typesacts.name).
     *
     * @param array $conditions The base conditions (usually on Stateacts.code)
     * @return \Cake\ORM\Query
     * @throws \App\Exception\BadRequestGetParametersException
     */
    protected function getQuery(array $conditions)
    {
        $query = $this->Projects
            ->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()])
            ->select([
                'Projects.code_act',
                'Projects.codematiere',
                'Projects.id',
                'Projects.name',
                'Projects.ismultichannel',
                'Projects.signature_date',
                'Projects.structure_id',
                'Matieres.id',
                'Matieres.structure_id',
                'Matieres.parent_id',
                'Matieres.codematiere',
                'Matieres.libelle',
            ])
            ->contain([
                'Generationerrors' => [
                    'fields' => [
                        'Generationerrors.type',
                        'Generationerrors.foreign_key',
                        'Generationerrors.message',
                        'Generationerrors.modified',
                    ],
                ],
                'Containers' => [
                    'fields' => ['Containers.id', 'Containers.project_id'],
                ],
                'Containers.Histories' => [
                    'fields' => [
                        'Histories.id',
                        'Histories.comment',
                        'Histories.container_id',
                    ],
                    'queryBuilder' => function (Query $query) {
                        return $query
                            ->where([
                                'Histories.id' => $this->Projects->Containers->Histories
                                    ->getLastByContainersIdSubquery('Histories.container_id'),
                            ]);
                    },
                ],
                'Containers.Stateacts' => [
                    'fields' => [
                        'Stateacts.id',
                        'Stateacts.name',
                        'Stateacts.code',
                    ],
                    'queryBuilder' => function (Query $query) {
                        return $query
                            ->where([
                                'ContainersStateacts.id' => $this->Projects->Containers->ContainersStateacts
                                    ->getLastByContainersIdSubquery('ContainersStateacts.container_id'),
                            ]);
                    },
                ],
                'Containers.Typesacts' => [
                    'queryBuilder' => function ($q) {
                        return $q
                            ->select(['Typesacts.id', 'Typesacts.name']);
                    },
                ],
                'Containers.Typesacts.Natures' => [
                    'queryBuilder' => function ($q) {
                        return $q
                            ->select([
                                'Natures.id',
                                'Natures.codenatureacte',
                                'Natures.libelle',
                                'Natures.typeabrege',
                            ]);
                    },
                ],
                'Matieres',
            ])
            ->matching('Containers.Stateacts')
            ->matching('Containers.Typesacts')
            ->where([
                'ContainersStateacts.id' => $this->fetchTable('ContainersStateacts')
                    ->getLastByContainersIdSubquery(),
            ])
            ->where($conditions)
            ->orderDesc('Projects.created');

        $filters = [
            'Projects.code_act' => 'code_act',
            'Projects.name' => 'name',
            'Typesacts.name' => 'typeact_name',
        ];

        return $this->QueryUtils->applyOrFilters($query, $filters, $this->getValidatedGetParameters());
    }

    /**
     * Return projects in state 'en-cours-de-validation' for which the user is involved (or is admin).
     *
     * @param array $conditions Supplementary conditions to be applied
     * @return \Cake\ORM\Query
     * @throws \App\Exception\BadRequestGetParametersException
     */
    protected function getQueryValidatingOrToValidateWhereUserIsInvolved(array $conditions = [])
    {
        $query = $this->getQuery([
            'Stateacts.code IN' => ['en-cours-de-validation'],
            'Projects.id_flowable_instance IS NOT NULL',
        ]);

        if (!empty($conditions)) {
            $query->where($conditions);
        }

        // si admin, on voit tout les projets (ceux où il est le current validator seront filtrés ensuite)
        // donc on peut se passer des conditions
//
//        if ($this->Multico->isValidatorRedactorInCurrentOrganization()) {
//            $query->find(
//                'AddProjectCreatedByUser',
//                ['userId' => $this->Multico->getCurrentUserId()]
//            );
//        } elseif (!$this->Multico->isAdminInCurrentOrganization()
//            && !$this->Multico->isValidatorRedactorInCurrentOrganization()) {
//            $query->find(
//                'AddClauseProjectValidatingWhereUserIsInvolved',
//                ['userId' => $this->Multico->getCurrentUserId()]
//            );
//        }

        if (!$this->Multico->isAdminInCurrentOrganization()) {
            $query->find(
                'AddClauseProjectValidatingWhereUserIsInvolved',
                ['userId' => $this->Multico->getCurrentUserId()]
            );
        }

        return $query;
    }

    /**
     * Returns a list of Projects.id_flowable_instance for projects whose stateact is "en-cours-de-validation" and
     * for which the user is involved (or is admin).
     *
     * @return array
     */
    protected function getInstancesWhereUserIsCurrent()
    {
        $query = $this->Projects
            ->find(
                'list',
                [
                    'keyField' => 'id',
                    'valueField' => 'id_flowable_instance',
                    'limitToStructure' => $this->Multico->getCurrentStructureId(),
                ]
            )
            ->select(['id_flowable_instance'])
            ->distinct(['id_flowable_instance'])
            ->matching('Containers.Stateacts')
            ->where([
                'id_flowable_instance IS NOT' => null,
                'ContainersStateacts.id' => $this->fetchTable('ContainersStateacts')
                    ->getLastByContainersIdSubquery(),
                'Stateacts.code IN' => ['en-cours-de-validation'],
            ])
            ->enableHydration(false);

        if (!$this->Multico->isAdminInCurrentOrganization()) {
            $query->find(
                'AddClauseProjectValidatingWhereUserIsInvolved',
                ['userId' => $this->Multico->getCurrentUserId()]
            );
        }

        return $query->toArray();
    }

    /**
     * Limits the projects to the one where the user is the creator when its role is "Rédacteur / Valideur"
     *
     * @param \Cake\ORM\Query $query The query to complete.
     * @return \Cake\ORM\Query
     * @see PermissionBehavior::beforeFind
     */
    protected function limitQueryToProjectsCreatedByValidatorRedactor(Query $query)
    {
        if ($this->Multico->getRoleName() === RolesTable::VALIDATOR_REDACTOR_NAME) {
            $query->find(
                'all',
                [
                    'businessFilters' => [
                        'limitOnProjectsOwnByCurrentUser' => $this->Multico->getCurrentUserId(),
                    ],
                ]
            );
        }

        return $query;
    }

    /**
     * Returns the query for the /projects/index_drafts API entry ("Brouillons").
     *
     * @return \Cake\ORM\Query
     * @throws \App\Exception\BadRequestGetParametersException
     */
    public function getQueryDrafts()
    {
        $query = $this->getQuery(['Stateacts.code IN' => ['brouillon', 'refuse']]);
        $query = $this->limitQueryToProjectsCreatedByValidatorRedactor($query);

        return $query;
    }

    /**
     * Returns the query for the /projects/index_validating API entry ("En cours dans un circuit").
     *
     * @return \Cake\ORM\Query
     * @throws \App\Exception\BadRequestGetParametersException
     */
    public function getQueryValidating()
    {
        $wrapper = WrapperFactory::createWrapper();
        $whereUserIsCurrent = $wrapper->filterInstancesWhereUserIsCurrent(
            $this->Multico->getCurrentUserId(),
            $this->getInstancesWhereUserIsCurrent()
        );

        if (!empty($whereUserIsCurrent)) {
            return $this->getQueryValidatingOrToValidateWhereUserIsInvolved([
                'Projects.id_flowable_instance NOT IN' => $whereUserIsCurrent,
            ]);
        } else {
            return $this->getQueryValidatingOrToValidateWhereUserIsInvolved();
        }
    }

    /**
     * Returns the query for the /projects/index_to_validate API entry ("À traiter").
     *
     * @return \Cake\ORM\Query
     * @throws \App\Exception\BadRequestGetParametersException
     */
    public function getQueryToValidate()
    {
        $wrapper = WrapperFactory::createWrapper();
        $whereUserIsCurrent = $wrapper->filterInstancesWhereUserIsCurrent(
            $this->Multico->getCurrentUserId(),
            $this->getInstancesWhereUserIsCurrent()
        );

        if (!empty($whereUserIsCurrent)) {
            return $this->getQueryValidatingOrToValidateWhereUserIsInvolved([
                'Projects.id_flowable_instance IN' => $whereUserIsCurrent,
            ]);
        } else {
            return $this->getQueryValidatingOrToValidateWhereUserIsInvolved(['1 = 0']);
        }
    }

    /**
     * Returns the query for the /projects/index_validated API entry ("Validés").
     *
     * @return \Cake\ORM\Query
     * @throws \App\Exception\BadRequestGetParametersException
     */
    public function getQueryValidated()
    {
        return $this->getQuery([
            'Stateacts.code IN' => [
                'en-cours-signature-i-parapheur',
                'refuse-i-parapheur',
                'valide',
                'en-cours-envoi-i-parapheur',
                'vote-et-approuve',
                'vote-et-rejete',
                'prendre-acte',
            ],
        ]);
    }

    /**
     * Returns the query for the /projects/index_to_transmit API entry ("À déposer sur le Tdt").
     *
     * @return \Cake\ORM\Query
     * @throws \App\Exception\BadRequestGetParametersException
     */
    public function getQueryToTransmit()
    {
        $conditions = [
            'OR' => [
                ['Stateacts.code IN' => ['a-teletransmettre', 'en-cours-depot-tdt', 'erreur-tdt']],
                [
                    'Stateacts.code IN' => ['declare-signe'],
                    'Typesacts.istdt' => true,
                ],
                [
                    'Stateacts.code IN' => ['signe-i-parapheur'],
                    'Typesacts.istdt' => true,
                ],
            ],
        ];

        $query = $this->getQuery($conditions);
        $query->contain([
            'Containers.Maindocuments' => [
                'queryBuilder' => function ($q) {
                    return $q->select(['Maindocuments.id']);
                },
            ],
            'Containers.Annexes' => [
                'conditions' => ['current' => true],
                'sort' => ['rank' => 'ASC'],
            ],
            'Containers.Annexes.Files',
            'Containers.Annexes.Typespiecesjointes' => [
                'fields' => ['id', 'codetype', 'libelle'],
            ],
            'Containers.Maindocuments.Typespiecesjointes' => [
                'queryBuilder' => function ($q) {
                    return $q
                        ->select([
                            'Typespiecesjointes.id',
                            'Typespiecesjointes.structure_id',
                            'Typespiecesjointes.nature_id',
                            'Typespiecesjointes.codetype',
                            'Typespiecesjointes.libelle',
                        ]);
                },
            ],
        ]);

        return $query;
    }

    /**
     * Returns the query for the /projects/index_ready_to_transmit API entry ("À envoyer en préfecture").
     *
     * @return \Cake\ORM\Query
     * @throws \App\Exception\BadRequestGetParametersException
     */
    public function getQueryReadyToTransmit()
    {
        return $this->getQuery(['Stateacts.code IN' => ['a-ordonner']]);
    }

    /**
     * Returns the query for the /projects/index_unassociated API entry ("Projets sans séances").
     *
     * @return \Cake\ORM\Query
     * @throws \App\Exception\BadRequestGetParametersException
     */
    public function getQueryUnassociated()
    {
        $conditions = [
            'Stateacts.code IN' => ['brouillon', 'valide', 'refuse', 'en-cours-de-validation', 'annule'],
            'Typesacts.nature_id IN' => $this->fetchTable('Natures')
                ->find()
                ->select(['id'])
                ->where(
                    [
                        'typeabrege' => 'DE',
                        'structure_id' => $this->Multico->getCurrentStructureId(),
                    ]
                )
                ->limit(1)
                ->orderAsc('id'),
            'Projects.signature_date IS NULL',
            'Containers.id NOT IN' => $this->fetchTable('ContainersSittings')
                ->find()
                ->select(['ContainersSittings.container_id'])
                ->where(['ContainersSittings.container_id = Containers.id']),
        ];

        return $this->getQuery($conditions);
    }

    /**
     * Returns the query for the /projects/index_act API entry ("Liste des actes").
     *
     * @return \Cake\ORM\Query
     * @throws \App\Exception\BadRequestGetParametersException
     */
    public function getQueryAct()
    {
        $conditions = [
            'OR' => [
                [
                    'Stateacts.code IN' => [
                        'en-cours-teletransmission',
                        'acquitte',
                        'annule',
                        'annulation-en-cours'],
                ],
                [
                    'Stateacts.code IN' => ['declare-signe'],
                    'Typesacts.istdt' => false,
                ],
                [
                    'Stateacts.code IN' => ['signe-i-parapheur'],
                    'Typesacts.istdt' => false,
                ],
            ],
        ];

        return $this->getQuery($conditions);
    }

    /**
     * Completes the project availableActions with can_be_edited, can_be_approved, can_be_deleted and checks permissions
     * for can_be_send_to_i_parapheur, can_be_send_to_manual_signature, can_generate_act_number.
     *
     * @param array $project The project for which to complete the availableActions
     * @return void
     */
    public function completeProjectAvailableActions(array &$project)
    {
        $this->ProjectsPermissions->addCanBeEdited($project);
        $this->ProjectsPermissions->addCanApproveProject($project);
        $this->ProjectsPermissions->addCanBeDeleted($project);

        // Liste des actions disponibles que l'on souhaite recalculer selon les droits de l'utilisateur.
        $availableActions = [
            'can_be_send_to_i_parapheur' => [
                'url' => '/projects/signature/electronic-signature/',
                'controller' => 'Signatures',
                'action' => 'electronicSignature',
            ],
            'can_be_send_to_manual_signature' => [
                'url' => 'projects/:id/manualSignature',
                'controller' => 'Signatures',
                'action' => 'manualSignature',
            ],
            'can_generate_act_number' => [
                'url' => '/projects/generateActNumber',
                'controller' => 'Projects',
                'action' => 'generateActNumber',
            ],
        ];
        $this->ProjectsPermissions->addChecks($project, $availableActions);
    }

    /**
     * Formats and adds data to the list of projects returned by the GET /projects pagination.
     *
     * Only the virtual fields for the Project entity will be added, the project's instance attribute will be added,
     * the project entity will be transformed to an array, all unnecessary _matchingData and _joinData will be removed
     * and the availableActions virtual fields will be completed.
     *
     * @param array $projects The list of projects
     * @param array|string[] $virtual The list of virtual fields to get for the Project entity ("availableActions"
     * will always be gotten)
     * @return array
     * @throws \Exception
     */
    public function map($projects, array $virtual = [])
    {
        $virtual = array_unique(array_merge(['availableActions'], $virtual));
        $currentUserId = $this->Multico->getCurrentUserId();

        $projects = $projects->map(function (Project $project) use ($currentUserId, $virtual) {
            $project->setVirtual($virtual);

            foreach ($project->containers as $containers) {
                $containers->setVirtual([]);
                $containers->typesact->setVirtual([]);
                foreach ($containers->histories as $history) {
                    $history->setVirtual([]);
                }
                foreach ($containers->stateacts as $stateact) {
                    $stateact->setVirtual([]);
                }
            }

            $project->instance = $project->getInstanceDetails($currentUserId);
            $project = $project->toArray();

            unset($project['_matchingData']);
            foreach ($project['containers'] as $idxContainer => $containers) {
                foreach ($containers['stateacts'] as $idxStateact => $stateact) {
                    unset($project['containers'][$idxContainer]['stateacts'][$idxStateact]['_joinData']);
                }
            }

            $this->completeProjectAvailableActions($project);

            return $project;
        });

        return $projects;
    }

    /**
     * Prepares common pagination parameters for the controller.
     *
     * @return void
     */
    protected function preparePagination()
    {
        $this->getController()->paginate = [
            'maxLimit' => PAGINATION_LIMIT,
            'order' => ['Projects.created' => 'DESC'],
        ];
    }

    /**
     * Validates and returns the get parameters (code_act, name and typeact_name).
     *
     * @return mixed
     * @throws \App\Exception\BadRequestGetParametersException
     */
    protected function getValidatedGetParameters()
    {
        $controller = $this->getController();

        $params = $controller->getRequest()->getQuery();

        $validator = (new Validator())
            ->allowPresence('name')
            ->allowEmptyString('name')
            ->allowPresence('typeact_name')
            ->allowEmptyString('typeact_name')
            ->allowPresence('code_act')
            ->allowEmptyString('code_act')
            ->allowPagination(true)
            ->only();

        $errors = $validator->validate($params);
        if (empty($errors) === false) {
            throw new BadRequestGetParametersException($errors);
        }

        return $params + ['code_act' => null, 'name' => null, 'typeact_name' => null];
    }

    /**
     * Paginates on projects whose state is "brouillon" or "refuse" for the current user.
     *
     * @return void
     * @throws \Exception
     */
    public function paginateDrafts()
    {
        $this->preparePagination();
        $controller = $this->getController();

        $query = $this->getQueryDrafts();
        $projects = $this->map($controller->paginate($query));
        $controller->set(compact('projects'));
    }

    /**
     * Paginates on projects whose state is "en-cours-de-validation" created by the user OR where the user is involved
     * in the circuit and for which there is currently no action in the validation circuit for the current user.
     *
     * @return void
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function paginateValidating()
    {
        $this->preparePagination();
        $controller = $this->getController();

        $query = $this->getQueryValidating();
        $projects = $this->map($controller->paginate($query));
        $controller->set(compact('projects'));
    }

    /**
     * Paginates on projects whose state is "en-cours-de-validation" and for which there is currently an action in the
     * validation circuit for the current user.
     *
     * @return void
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function paginateToValidate()
    {
        $this->preparePagination();
        $controller = $this->getController();

        $query = $this->getQueryToValidate();
        $projects = $this->map($controller->paginate($query));
        $controller->set(compact('projects'));
    }

    /**
     * Paginates on projects whose state is "en-cours-signature-i-parapheur" or "refuse-i-parapheur" or "valide" for the
     * current user.
     *
     * @return void
     * @throws \Exception
     */
    public function paginateValidated()
    {
        $this->preparePagination();
        $controller = $this->getController();

        $query = $this->getQueryValidated();
        $projects = $this->map($controller->paginate($query));
        $controller->set(compact('projects'));
    }

    /**
     * Paginates on projects whose state is not amongst "en-cours-de-validation", "valide", whose typeact has a nature
     * type of "DE", that are not signed and that are not associated to a sitting for the current user.
     *
     * @return void
     * @throws \Exception
     */
    public function paginateUnassociated()
    {
        $this->preparePagination();
        $controller = $this->getController();

        $query = $this->getQueryUnassociated();
        $projects = $this->map($controller->paginate($query));
        $controller->set(compact('projects'));
    }

    /**
     * Paginates on projects whose state is "a-teletransmettre" or "en-cours-depot-tdt" or 'erreur-tdt', or whose state
     * is "declare-signe" while being transmissible, or whose state is "signe-i-parapheur" while being transmissible for
     * the current user.
     *
     * @return void
     * @throws \Exception
     */
    public function paginateToTransmit()
    {
        $this->preparePagination();
        $controller = $this->getController();

        $query = $this->getQueryToTransmit();
        $projects = $this->map($controller->paginate($query));
        $controller->set(compact('projects'));
    }

    /**
     * Paginates on projects whose state is "a-ordonner" for the current user.
     *
     * @return void
     * @throws \Exception
     */
    public function paginateReadyToTransmit()
    {
        $this->preparePagination();
        $controller = $this->getController();

        $query = $this->getQueryReadyToTransmit();
        $projects = $this->map($controller->paginate($query));
        $controller->set(compact('projects'));
    }

    /**
     * Regroups indexTransmitted and indexUntransmittable
     *
     * @return void
     * @throws \Exception
     */
    public function paginateAct()
    {
        $this->preparePagination();
        $controller = $this->getController();

        $query = $this->getQueryAct();
        $projects = $this->map($controller->paginate($query), ['receiptDate']);
        $controller->set(compact('projects'));
    }
}
