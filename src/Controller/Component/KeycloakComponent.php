<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Model\Entity\Auth;
use App\Model\Entity\User;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Utility\Hash;
use Exception;

/**
 * @property \Cake\Controller\Component\AuthComponent $Auth
 * @property \Cake\Network\Session $Session
 */
class KeycloakComponent extends AppComponent
{
    public $components = ['Auth', 'Session'];

    protected $_keycloakIds = [];

    protected $_keycloakAuth = null;

    /**
     * get the keycloakAuth client
     *
     * @return \App\Model\Entity\Auth|null
     */
    protected function _getKeycloakAuth()
    {
        if ($this->_keycloakAuth === null) {
            $auth = new Auth();
            $token = $auth->getToken(KEYCLOAK_USER, KEYCLOAK_PASSWORD);
            $auth->setToken($token);
            $this->_keycloakAuth = $auth;
        }

        return $this->_keycloakAuth;
    }

    /**
     * Rollback
     *
     * @return void
     */
    public function onRollback()
    {
        $auth = $this->_getKeycloakAuth();
        foreach ($this->_keycloakIds as $id) {
            $auth->deleteUser($id);
        }
    }

    /**
     * create the user in keycloak
     *
     * @param \App\Controller\Component\App\Model\Entity\User $user user
     * @return int
     */
    public function createKeycloakUser(User $user): string
    {
        $userKc = [
            'firstName' => $user['firstname'],
            'lastName' => $user['lastname'],
            'username' => $user['username'],
            'email' => $user['email'],
            'enabled' => $user['active'],
        ];

        $auth = $this->_getKeycloakAuth();

        $res = $auth->addUser($userKc);
        if (!$res['success']) {
            throw new Exception('impossible de creer l\'utilisateur dans keycloak : ' .
                str_replace("\n", ' ', var_export($res, true)));
        }

        $this->_keycloakIds[] = $res['id'];

        return $res['id'];
    }

    /**
     * Update a user.
     *
     * @param \App\Controller\Component\App\Model\Entity\User $user user
     * @return mixed
     */
    public function updateUser(User $user)
    {
        $auth = $this->_getKeycloakAuth();
        $userKc = [
            'firstName' => $user['firstname'],
            'lastName' => $user['lastname'],
            'username' => $user['username'],
            'email' => $user['email'],
            'enabled' => $user['active'],
        ];

        $response = $auth->updateUser($user['keycloak_id'], $userKc);

        if (!$response['success']) {
            throw new ForbiddenException('impossible de modifier l\'utilisateur dans keycloak : ' .
                str_replace("\n", ' ', var_export($response, true)));
        }

        return $response['success'];
    }

    /**
     * @param string $token Authentification token
     * @return bool
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function loginByToken($token)
    {
        $responseCheck = $this->checkOnline($token);

        if (empty($responseCheck)) {
            throw new UnauthorizedException('bad token');
        }
        $controller = $this->getController();

        $users = $controller->fetchTable('Users');
        $user = $users->find()->where(['username' => $responseCheck[KEYCLOAK_USERNAME]])->first();

        if (!empty($user)) {
            $controller->getRequest()->getSession()->write('user_id', $user->id);
            $controller->getRequest()->getSession()->write('keycloak_id', $responseCheck['sub']);
            $controller->getRequest()->getSession()->write('username', $responseCheck[KEYCLOAK_USERNAME]);
            $controller->getRequest()->getSession()->write('lastname', $user->lastname);
            $controller->getRequest()->getSession()->write('firstname', $user->firstname);
            $controller->getRequest()->getSession()->write('email', $user->email);

            // @info: pas de multico ternum
            $RolesUsers = $controller->fetchTable('RolesUsers');
            $rolesUser = $RolesUsers->find()->where(['user_id' => $user->id])->toArray();
            if (count($rolesUser) != 1) {
                throw new Exception(sprintf('nombre de roles_users !=1 pour %d', $user->id));
            }

            $structureId = Hash::get($RolesUsers->Roles
                ->find()
                ->select(['Roles.structure_id'])
                ->where(
                    [
                        'Roles.id' => $rolesUser[0]['role_id'],
                    ]
                )
                ->disableHydration()
                ->firstOrFail(), 'structure_id');

            $this->Auth->logout();
            $this->Auth->setUser($rolesUser[0]);

            $auth = $controller->getRequest()->getSession()->read('Auth');
            $controller->getRequest()->getSession()->write('structure_id', $structureId);
            $controller->getRequest()->getSession()->write('organization_id', $user->organization_id);

            $controller->getRequest()->getSession()->delete('Auth.Role');
            try {
                $role = $RolesUsers->Roles
                    ->find()
                    ->select(['Roles.id', 'Roles.structure_id', 'Roles.name'])
                    ->innerJoinWith('Users')
                    ->where(
                        [
                            'Roles.structure_id' => $structureId,
                            'Roles.active' => true,
                            'RolesUsers.user_id' => $user->id,
                        ]
                    )
                    ->disableHydration()
                    ->firstOrFail();

                if (empty($role) === false) {
                    $controller->getRequest()->getSession()->write('Auth.Role', $role);
                }
            } catch (Exception $exception) {
                $this->log($exception->getMessage());
            }

            return true;
        }

        return false;
    }

    /**
     * @param string $token token
     * @return array|bool
     */
    private function checkOnline($token)
    {
        $auth = new Auth();
        $res = $auth->verifyToken($token);
        if (empty($res['success'])) {
            return false;
        }

        return $res['data'];
    }
}
