<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Utilities\Api\Idelibre\Idelibre;
use Cake\Http\Client;
use Cake\Http\Exception\ForbiddenException;
use Exception;

/**
 * Class PastellComponent
 *
 * Ne peut fonctionner sans les données de connection Pastell (accessibles via $structure->pastellConnector
 *
 * @package App\Controller\Component
 */
class IdelibreComponent extends AppComponent
{
    /**
     * @var $http Idelibre
     */
    private $http;

    /**
     * @param  array $config config
     * @return void
     */
    public function initialize(array $config): void
    {
        if (
            empty($config['username'])
            || empty($config['password'])
            || empty($config['url'])
            || empty($config['conn'])
        ) {
            throw new ForbiddenException('Connecteur Idelibre non configuré.');
        }

        $connexion = [
            'username' => $config['username'],
            'password' => $config['password'],
            'url' => $config['url'],
            'conn' => $config['conn'],
        ];

        parent::initialize($config);

        $client = new Client(
            [
            'ssl_verify_peer' => false,
            'redirect' => 2,
            ]
        );

        $this->http = new Idelibre($client, $config['url'], $connexion);
    }

    /**
     * check if connexion ok
     *
     * @return bool
     */
    public function checkConnexion(): bool
    {
        $response = $this->http->checkConnexion();
        if ($response->getStatusCode() === 200) {
            return true;
        }

        return false;
    }

    /**
     * check if connexion ok
     *
     * @param string $json Foo
     * @return bool
     * @throws \Exception
     */
    public function send($json): bool
    {
        $response = $this->http->send($json);
        if ($response['success']) {
            if (!$response['success']) {
                throw new Exception($response['code'] . ': ' . $response['message']);
            }

            return true;
        }

        return false;
    }
}
