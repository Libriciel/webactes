<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Model\Table\RolesTable;

/**
 * @property \App\Model\Table\RolesTable $Roles
 */
class MulticoComponent extends AppComponent
{
    public const ROLE_NAME = 'Auth.Role.name';

    /**
     * @param array $config config
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->Roles = $this->fetchTable('Roles');

        parent::initialize($config);
    }

    /**
     * @return int
     */
    public function getCurrentStructureId()
    {
        return $this->getController()->getRequest()->getSession()->read('structure_id');
    }

    /**
     * @return int
     */
    public function getCurrentUserId()
    {
        return $this->getController()->getRequest()->getSession()->read('user_id');
    }

    /**
     * @return int
     */
    public function getCurrentOrganizationId()
    {
        return $this->getController()->getRequest()->getSession()->read('organization_id');
    }

    /**
     * Checks that the current role in the current structure is admin or functional admin.
     *
     * @fixme : waiting for a more complex right system, admin functionnal is admin except for access to admin menu (client computed)
     * @return bool
     */
    public function isAdminInCurrentOrganization()
    {
        return in_array(
            $this->getRoleName(),
            [RolesTable::SUPER_ADMINISTRATOR, RolesTable::ADMINISTRATOR, RolesTable::FUNCTIONNAL_ADMINISTRATOR]
        );
    }

    /**
     * Checks that the current role in the current structure is super admin.
     *
     * @return bool
     */
    public function isSuperAdminInCurrentOrganization()
    {
        return $this->getRoleName() === RolesTable::SUPER_ADMINISTRATOR;
    }

    /**
     * Checks that the current role in the current structure is functional admin.
     *
     * @return bool
     */
    public function isFunctionalAdminInCurrentOrganization()
    {
        return $this->getRoleName() === RolesTable::FUNCTIONNAL_ADMINISTRATOR;
    }

    /**
     * Checks that the current role in the current structure is functional admin.
     *
     * @return bool
     */
    public function isValidatorRedactorInCurrentOrganization()
    {
        return $this->getRoleName() === RolesTable::VALIDATOR_REDACTOR_NAME;
    }

    /**
     * Get the current Role Name.
     *
     * @return string
     */
    public function getRoleName()
    {
        return $this->getController()->getRequest()->getSession()->read(self::ROLE_NAME);
    }

    /**
     * Get roles without super admin
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->Roles->getRolesNames();
    }

    /**
     * Get one structure roles
     *
     * @param int $id structureId
     * @return array
     */
    public function getStructureRoles(int $id)
    {
        return $this->Roles->find()->select(['id', 'name'])
            ->where(['Roles.structure_id' => $id])
            ->toArray();
    }
}
