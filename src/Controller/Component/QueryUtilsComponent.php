<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

/**
 * This class offers utility methods for querying from the controllers and components.
 * It shouldn't even be in a component because it doesn't use the session.
 *
 * @package App\Controller\Component
 */
class QueryUtilsComponent extends AppComponent
{
    /**
     * Returns a closure that will be used as a Query condition equivalent to an ILIKE condition.
     *
     * @param string $field The field
     * @param string $value The value
     * @return \Closure
     */
    public function getIlikeQueryExpression(string $field, string $value)
    {
        return function (QueryExpression $exp, QueryExpression $q) use ($field, $value) {
            return $exp->like("LOWER({$field})", mb_strtolower($value, 'UTF-8'));
        };
    }

    /**
     * Applies the GET parameter conditions to the query (OR will be used).
     *
     * @param \Cake\ORM\Query $query The query on which the filters will be applied.
     * @param array $filters An array with GET field / parameter pairs to be applied as conditions
     *    (e.g. ['Projects.name' => 'name'])
     * @param array $params An array with GET parameter / value pairs to be applied as conditions
     *     (e.g. ['name' => 'REFUSÉ+PAR+5'])
     * @return \Cake\ORM\Query
     */
    public function applyOrFilters(Query $query, array $filters, array $params)
    {
        $conditions = [];
        foreach ($filters as $field => $param) {
            if (isset($params[$param]) && !empty($params[$param])) {
                $conditions[] = $this->getIlikeQueryExpression($field, '%' . $params[$param] . '%');
            }
        }

        if (!empty($conditions)) {
            $query->where(['OR' => $conditions]);
        }

        return $query;
    }
}
