<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Exception\BadRequestGetParametersException;
use App\Validation\Validator;

/**
 * Cette classe contient des méthodes utilitaires pour valider les paramètres GET de la même façon qu'une entité est
 * validée.
 *
 * @package App\Controller\Component
 */
class QueryParamsComponent extends AppComponent
{
    /**
     * Vérifie que seuls les paramètres GET attendus soient utilisés, avec la possibilité ou non d'utiliser la pagination
     * (paramètre paginate, valeurs vide (true), true et false.
     * Dans le cas contraire, la liste des erreurs de validation rencontrée sera renvoyée avec le statut HTTP 400.
     *
     * ATTENTION: l'intérêt de cette méthode par-rapport à la méthode validate, c'est la possible présence du paramètre
     * GET "paginate", valeurs possibles true/false (true par défaut)), ce qui permet ensuite dans le code d'utiliser ou
     * non la pagination.
     *
     * @param \App\Validation\Validator $validator Un validateur possédant les autres règles nécessaires (le cas échéant)
     * @param array $defaults Les valeurs par défaut à ajouter aux paramètres GET
     * @return array
     * @throws \App\Exception\BadRequestGetParametersException
     */
    public function validateIndexWithPaginationTrueFalse(Validator $validator = new Validator(), array $defaults = [])
    {
        $defaults = ['paginate' => 'true'] + $defaults;
        $params = $this->getController()->getRequest()->getQuery() + $defaults;

        return $this->validate(
            $validator->allowPresence('paginate')
                ->inList('paginate', ['true', 'false'])
                ->allowPagination($params['paginate'] === 'true'),
            $defaults
        );
    }

    /**
     * Vérifie que seuls les paramètres GET attendus soient utilisés.
     * Dans le cas contraire, la liste des erreurs de validation rencontrée sera renvoyée avec le statut HTTP 400.
     *
     * @param \App\Validation\Validator $validator Un validateur possédant les autres règles nécessaires (le cas échéant)
     * @param array $defaults Les valeurs par défaut à ajouter aux paramètres GET
     * @return array
     * @throws \App\Exception\BadRequestGetParametersException
     */
    public function validate(Validator $validator = new Validator(), array $defaults = [])
    {
        $params = $this->getController()->getRequest()->getQuery() + $defaults;

        $validator->only();

        $errors = $validator->validate($params);
        if (empty($errors) === false) {
            throw new BadRequestGetParametersException($errors);
        }

        return $params;
    }
}
