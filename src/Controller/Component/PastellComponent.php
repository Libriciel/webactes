<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Utilities\Api\Pastell\Pastell;
use App\Utilities\Pastell\ClassificationManager;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Http\Client;
use Cake\Http\Client\Response;
use Cake\Http\Exception\ForbiddenException;
use Cake\Log\Log;
use Cake\Utility\Hash;
use DOMDocument;
use Exception;
use InvalidArgumentException;

/**
 * Class PastellComponent
 *
 * Ne peut fonctionner sans les données de connection Pastell (accessibles via $structure->pastellConnector
 *
 * @package App\Controller\Component
 */
class PastellComponent extends AppComponent
{
    /**
     * @var $http Pastell
     */
    private $http;

    /**
     * @param  array $config config
     * @return void
     */
    public function initialize(array $config): void
    {
        if (empty($config['pastell_user']) || empty($config['pastell_password']) || empty($config['pastell_url'])) {
            throw new ForbiddenException('Connecteur Pastell non configuré.');
        }

        parent::initialize($config);

        $client = new Client(
            [
            'auth' => [
                'username' => $config['pastell_user'],
                'password' => $config['pastell_password'],
            ],
            'ssl_verify_peer' => false,
            'redirect' => 2,
            ]
        );

        $url = $config['pastell_url'];
        $this->http = new Pastell($client, $url);
    }

    /**
     * Retourne toutes les structures présentes sous notre orchestrateur (PASTELL)
     *
     * @return array|null
     * @access  public
     * @created 03/05/2019
     * @version V0.0.9
     */
    public function getStructures()
    {
        $structures = $this->http->getEntities();

        return $structures->getJson();
    }

    /**
     * Fonction qui ajoute les structures à l'application
     *
     * @param  array $structures_ids Tableau des IDs sous l'orchestrateur "id_e" (PASTELL)
     * @return bool
     * @access  public
     * @created 03/05/2019
     * @version V0.0.9
     */
    public function addStructures($structures_ids)
    {
        $success = false;

        if (!empty($structures_ids)) {
            $Organization = $this->fetchTable('Organizations');
            $Structure = $this->fetchTable('Structures');
            $Pastellfluxtypes = $this->fetchTable('Pastellfluxtypes');
            $Structure->getConnection()->begin();

            $success = true;

            foreach ($structures_ids as $key => $structures_id) {
                if (is_array($structures_id) === true) {
                    if (count($structures_id) != count($structures_id, COUNT_RECURSIVE)) {
                        $success = false;
                        break;
                    }

                    $structureOrchestrationOrganisation = $this->getEntityPastellId($key);

                    if (isset($structureOrchestrationOrganisation['status'])) {
                        $success = false;
                        break;
                    }

                    $lastOrganizationSave = $this->saveOrganisation(
                        $Organization,
                        $structureOrchestrationOrganisation['denomination']
                    );
                    $success = $success && $lastOrganizationSave !== false;

                    if (!$success) {
                        $success = false;
                        break;
                    }

                    $organizationID = $lastOrganizationSave->id;
                    $lastStructureSave = $this->saveStructure(
                        $Structure,
                        $organizationID,
                        $structureOrchestrationOrganisation['denomination'],
                        $structureOrchestrationOrganisation['id_e']
                    );
                    $success = $success && $lastStructureSave !== false;

                    if ($success === true) {
                        $structureID = $lastStructureSave->id;
                        $success = $success && $this->saveMinimumValuePastellFluxTypes(
                            $Pastellfluxtypes,
                            $organizationID,
                            $structureID
                        ) !== false;
                    }

                    if (!$success) {
                        $success = false;
                        break;
                    }

                    foreach ($structures_id as $structure_id_e) {
                        $structureOrchestration = $this->getEntityPastellId($structure_id_e);

                        if (isset($structureOrchestration['status'])) {
                            $success = false;
                            break;
                        }

                        if ($structureOrchestration['entite_mere'] === $structureOrchestrationOrganisation['id_e']) {
                            if ($success === true) {
                                $lastStructureSave = $this->saveStructure(
                                    $Structure,
                                    $organizationID,
                                    $structureOrchestration['denomination'],
                                    $structureOrchestration['id_e']
                                );
                                $success = $success && $lastStructureSave !== false;

                                if ($success === true) {
                                    $success = $success && $this->saveMinimumValuePastellFluxTypes(
                                        $Pastellfluxtypes,
                                        $organizationID,
                                        $lastStructureSave->id
                                    ) !== false;
                                }
                            }
                        } else {
                            $success = false;
                            break;
                        }
                    }
                } else {
                    $structureOrganisation = $this->getEntityPastellId($structures_id);

                    if (isset($structureOrganisation['status'])) {
                        $success = false;
                        break;
                    }

                    $lastOrganizationSave = $this->saveOrganisation(
                        $Organization,
                        $structureOrganisation['denomination']
                    );
                    $success = $success && $lastOrganizationSave !== false;

                    if ($success === true) {
                        $organizationID = $lastOrganizationSave->id;

                        $lastStructureSave = $this->saveStructure(
                            $Structure,
                            $organizationID,
                            $structureOrganisation['denomination'],
                            $structureOrganisation['id_e']
                        );
                        $success = $success && $lastStructureSave !== false;

                        if ($success === true) {
                            $structureID = $lastStructureSave->id;

                            $success = $success && $this->saveMinimumValuePastellFluxTypes(
                                $Pastellfluxtypes,
                                $organizationID,
                                $structureID
                            ) !== false;
                        }
                    }
                }
            }

            if ($success === true) {
                $Structure->getConnection()->commit();
            } else {
                $Structure->getConnection()->rollback();
            }
        }

        return $success;
    }

    /**
     * Fonction permettant de faire appel à PASTELL en lui demandant l'entité avec id_e sous pastell
     *
     * @param  int $id id_e sous pastell
     * @return mixed
     * @access  protected
     * @created 14/05/2019
     * @version V0.0.9
     */
    protected function getEntityPastellId($id)
    {
        $structureOrchestrationJson = $this->http->getEntity($id);

        return $structureOrchestrationJson->getJson();
    }

    /**
     * Fonction permettant d'enregistrer l'organization en BDD
     *
     * @param  object $Organization Organization
     * @param  string $denomination Nom de l'organization
     * @return mixed
     * @access  protected
     * @created 14/05/2019
     * @version V0.0.9
     */
    protected function saveOrganisation($Organization, $denomination)
    {
        $organization = $Organization->newEntity(
            [
            'name' => $denomination,
            ]
        );

        return $Organization->save($organization, ['atomic' => false]);
    }

    /**
     * Fonction permettant d'enregistrer l'organization en BDD
     *
     * @param  object $Structure             Structure
     * @param  int    $organization_id       id de l'organisation
     * @param  string $structureDenomination nom de la structure
     * @param  string $id_e                  id de la structure sous pastell
     * @return mixed
     * @access  protected
     * @created 14/05/2019
     * @version V0.0.9
     */
    protected function saveStructure($Structure, $organization_id, $structureDenomination, $id_e)
    {
        $structure = $Structure->newEntity(
            [
            'organization_id' => $organization_id,
            'business_name' => $structureDenomination,
            'id_orchestration' => $id_e,
                'roles' => $Structure->addMinimumRoles(),
            ]
        );

        $Structure = $this->fetchTable('Structures');

        $structureSave = $Structure->save($structure, ['atomic' => false]);
        $Structure->addMinimumTypeSittings($structureSave->id);
        $Structure->addMinimumPastellFluxTypes($structureSave->id);

        return $structureSave;
    }

    /**
     * Mise à jour de la classification
     *
     * @param  int $structure_id ID de la structure
     * @param  int $id_e         ID de la structure sous Pastell
     * @return bool
     * @access  public
     * @created 29/05/2019
     * @throws \Exception
     * @version V0.0.9
     */
    public function updateClassification($structure_id, $id_e)
    {
        try {
            $configNaturesTypeAbregeAccepted = Configure::read('naturesTypeAbregeAccepted');
            if (empty($configNaturesTypeAbregeAccepted)) {
                return false;
            }

            $classifications = $this->getClassificationPastell($id_e);

            $Classifications = $this->fetchTable('Classifications');

            $success = true;

            $Natures = $this->fetchTable('Natures');

            $conn = $Natures->getConnection();
            $conn->begin();

            $success = $success && $this->saveNatureActes(
                $Natures,
                $classifications['NaturesActes'],
                $structure_id,
                $configNaturesTypeAbregeAccepted
            ) !== false;

            $Matieres = $this->fetchTable('Matieres');

            $success = $success && $this->saveMatieres(
                $Matieres,
                $classifications['Matieres'],
                $structure_id
            ) !== false;

            //MAJ de la date de classification en BDD
            $success = $success && $this->saveDateClassification(
                $Classifications,
                $classifications['DateClassification'],
                $structure_id
            ) !== false;

            if ($success === true) {
                $conn->commit();
                //Update typeacts
                //$this->updateTypeactes($structure_id);
            } else {
                $conn->rollback();
            }

            // clear associated project classification
            $this->clearProjectsClassification($structure_id);

            return $success;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Fonction permettant d'enregistrer les natures en BDD en fonction du fichier de configuration
     *
     * @param  object $Natures                         Natures
     * @param  array  $naturesActes                    Nature extraite du fichier XML de la classification
     * @param  int    $structure_id                    ID de la structure
     * @param  array  $configNaturesTypeAbregeAccepted Type de nature accepter (voir fichier de configuration)
     * @return bool
     * @access  protected
     * @created 29/05/2019
     * @version V0.0.9
     */
    protected function saveNatureActes($Natures, $naturesActes, $structure_id, $configNaturesTypeAbregeAccepted)
    {
        $success = true;
        $naturesenBdd = $Natures->find()
            ->select(['typeabrege'])
            ->where(
                [
                'structure_id' => $structure_id,
                ]
            )
            ->toArray();

        $existingTypeAbrege = Hash::extract($naturesenBdd, '{n}.typeabrege');

        $Typespiecesjointes = $this->fetchTable('Typespiecesjointes');

        $valuesTypespiecesjointes = $Typespiecesjointes->find()
            ->where(
                [
                'structure_id' => $structure_id,
                ]
            )
            ->toArray();

        if (!empty($valuesTypespiecesjointes)) {
            $success = $success && $Typespiecesjointes->deleteAll(['structure_id' => $structure_id]) !== false;
            if ($success === false) {
                return false;
            }
        }

        foreach ($naturesActes as $natureActe) {
            if (!in_array($natureActe['code'], $existingTypeAbrege)) {
                if (in_array($natureActe['code'], $configNaturesTypeAbregeAccepted)) {
                    $nature = $Natures->newEntity(
                        [
                        'structure_id' => $structure_id,
                        'libelle' => $natureActe['name'],
                        'typeabrege' => $natureActe['code'],
                        'codenatureacte' => $natureActe['codenatureacte'],
                        ]
                    );

                    $natureSave = $Natures->save($nature, ['atomic' => false]);
                    $success = $success && $natureSave !== false;

                    if (!$success) {
                        return false;
                    }
                }
            }
        }

        return $this->saveTypespiecesjointes($naturesActes, $structure_id);
    }

    /**
     * Sauvegarde les pieces jointes associé à la nature
     *
     * @param  array $naturesActes Natures with typespiecesjointes
     * @param  int   $structure_id structureId
     * @return bool
     */
    protected function saveTypespiecesjointes($naturesActes, $structure_id)
    {
        //delete structure typePiecesjointe
        $Typespiecesjointes = $this->fetchTable('Typespiecesjointes');
        $success = $Typespiecesjointes->deleteAll(['structure_id' => $structure_id]) !== false;

        $Nature = $this->fetchTable('Natures');
        foreach ($naturesActes as $natureActe) {
            if (!in_array($natureActe['code'], Configure::read('naturesTypeAbregeAccepted'))) {
                continue;
            }

            //get natureId
            $nature = $Nature->find()
                ->select(['id'])
                ->where(['codenatureacte' => $natureActe['codenatureacte'], 'structure_id' => $structure_id])
                ->first();

            if (empty($nature)) {
                throw new InvalidArgumentException("nature ${natureActe['codenatureacte']} not find ");
            }

            foreach ($natureActe['typesPJNatureActe'] as $typepiecejointe) {
                if (!$success) {
                    return false;
                }

                $typePiece = $Typespiecesjointes->newEntity(
                    [
                    'structure_id' => $structure_id,
                    'nature_id' => $nature->id,
                    'codetype' => $typepiecejointe['code'],
                    'libelle' => $typepiecejointe['name'],
                    ]
                );

                $success = $success && $Typespiecesjointes->save($typePiece, ['atomic' => false]) !== false;
            }
        }

        return $success;
    }

    /**
     * Fonction permettant d'enregistrer les matieres en BDD
     *
     * @param  object $Matieres               Matieres
     * @param  array  $classificationMatieres Matieres et sous matiere extraite du fichier XML de la classification
     * @param  int    $structure_id           ID de la structure
     * @return bool
     * @access  protected
     * @created 29/05/2019
     * @version V0.0.9
     */
    protected function saveMatieres($Matieres, $classificationMatieres, $structure_id)
    {
        $success = $Matieres->deleteAll(['structure_id' => $structure_id], ['atomic' => false]) !== false;

        $valuesMatieres = $Matieres->find()
            ->where(
                [
                'structure_id' => $structure_id,
                ]
            )
            ->toArray();

        if (!empty($valuesMatieres)) {
            $success = $success && $Matieres->deleteAll($valuesMatieres, ['atomic' => false]) !== false;

            if ($success === false) {
                return false;
            }
        }

        foreach ($classificationMatieres as $classificationMatiere) {
            $matiere = $Matieres->newEntity(
                [
                'structure_id' => $structure_id,
                'parent_id' => null,
                'codematiere' => $classificationMatiere['name'],
                'libelle' => $classificationMatiere['libelle'],
                ]
            );

            $lastSaveMatiere = $Matieres->save($matiere, ['atomic' => false]);
            $success = $success && $lastSaveMatiere !== false;

            if (!$success) {
                throw new InvalidArgumentException($matiere->getErrors());
            }

            if ($success === false) {
                break;
            }

            if (array_key_exists('children', $classificationMatiere)) {
                $this->saveChildrenMatieres(
                    $classificationMatiere['children'],
                    $lastSaveMatiere->id,
                    $structure_id
                );
            }
        }

        return $success;
    }

    /**
     * Fonction permettant d'enregistrer les enfants des matieres en BDD
     *
     * @param  array $matiereChildrens Tableau des sous matieres
     * @param  int   $parentMatiereId  ID du parent matiere
     * @param  int   $structure_id     ID de la structure
     * @return bool
     * @access  protected
     * @created 29/05/2019
     * @version V0.0.9
     */
    protected function saveChildrenMatieres($matiereChildrens, $parentMatiereId, $structure_id)
    {
        $success = true;

        $Matieres = $this->fetchTable('Matieres');

        foreach ($matiereChildrens as $matiereChildren) {
            $matiere = $Matieres->newEntity(
                [
                'structure_id' => $structure_id,
                'parent_id' => $parentMatiereId,
                'codematiere' => $matiereChildren['name'],
                'libelle' => $matiereChildren['libelle'],
                ]
            );

            $success = $success && $Matieres->save($matiere, ['atomic' => false]) !== false;

            if ($success === false) {
                break;
            }
        }

        return $success;
    }

    /**
     * Fonction permettant de faire la MAJ de la date de classification en BDD
     *
     * @param  object $Classifications       Classifications
     * @param  string $newDateClassification Date de la classification extraite du fichier XML
     * @param  int    $structure_id          ID de la structure
     * @return mixed
     * @access  protected
     * @created 29/05/2019
     * @version V0.0.9
     */
    protected function saveDateClassification($Classifications, $newDateClassification, $structure_id)
    {
        $classification = $Classifications->newEntity(
            [
            'structure_id' => $structure_id,
            'dateclassification' => $newDateClassification,
            ]
        );

        return $Classifications->save($classification, ['atomic' => false]);
    }

    /**
     * @param  int $id_e ID de la structure sous Pastell
     * @return array|bool
     * @access  protected
     * @created 29/05/2019
     * @throws \Exception
     * @version V0.0.9
     */
    public function getClassificationPastell($id_e)
    {
        try {
            $responseClassification = $this->getClassification($id_e);

            return (new ClassificationManager())->getClassificationPastell($responseClassification->getStringBody());
        } catch (Exception $e) {
            throw new Exception(
                sprintf('Erreur de récupération de la classification : %s', $e->getMessage()),
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * Récupère le tableau de classifications
     *
     * @param  int $idEntite id_e sous Pastell
     * @return mixed
     * @access  public
     * @created 29/05/2019
     * @version V0.0.9
     */
    public function getClassification($idEntite): Response
    {
        $idCe = $this->getIdCeByEntiteAndConnecteur($idEntite);

        return $this->http->getClassificationFile($idEntite, $idCe);
    }

    /**
     * Fonction permettant de faire appel à l'API de Pastell pour récupéré le connecteur TdT courant
     *
     * @param  int $idEntite id_e sous Pastell
     * @return |null
     * @access  public
     * @created 29/05/2019
     * @version V0.0.9
     */
    public function getIdCeByEntiteAndConnecteur($idEntite)
    {
        $connecteur = $this->http->getFluxActesGeneriqueWithConnectorTdT($idEntite);
        $connecteur = $connecteur->getJson();

        if (count($connecteur) === 1) {
            return $connecteur[0]['id_ce'];
        }

        return null;
    }

    /**
     * Conversion du fichier XML en UTF-8 et enregistrement sur le disque
     *
     * @param \Cake\Http\Client\Response $response response
     * @return     bool|string
     * @deprecated
     * @access     public
     * @created    29/05/2019
     * @version    V0.0.9
     */
    public function saveClassification(Response $response)
    {
        $success = true;

        $response = $response->getStringBody();
        $response = str_replace('ISO-8859-1', 'UTF-8', utf8_encode($response));
        $xml = simplexml_load_string($response);

        if ($xml === false && $success) {
            return false;
        }

        $dom_xml = dom_import_simplexml($xml);
        if ($xml === false && $success) {
            return false;
        }

        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom_xml = $dom->importNode($dom_xml, true);
        $dom->appendChild($dom_xml);

        $folderTmp = new Folder(WORKSPACE . DS . 'classification', true, 0777);
        $file = new File($folderTmp->pwd() . DS . 'classification.xml', true, 0777);

        $file->delete();
        $file->create();

        if ($file->writable()) {
            $success = true;
            $file->write($dom->saveXML());
        } else {
            $success = false;
        }

        $file->close();

        if ($success === true) {
            return $file->pwd();
        }

        return false;
    }

    /**
     * @param  object $pastellFluxTypes Pastellfluxtype
     * @param  int    $organization_id id de l'organisation
     * @param  int    $structure_id    id de la structure
     * @return mixed
     * @access  protected
     * @created 09/07/2019
     * @version V0.0.9
     */
    protected function saveMinimumValuePastellFluxTypes($pastellFluxTypes, $organization_id, $structure_id)
    {
        $pastellfluxtypes = $pastellFluxTypes->newEntities(
            [
            'name' => 'actes-generique',
            'active' => true,
            'organization_id' => $organization_id,
            'structure_id' => $structure_id,
            ],
            [
                'name' => 'mailsec',
                'active' => true,
                'organization_id' => $organization_id,
                'structure_id' => $structure_id,
            ]
        );

        return $pastellFluxTypes->saveMany($pastellfluxtypes, ['atomic' => false]);
    }

    /**
     * get the aracte.xml file
     *
     * @param  int $documentId       documentId
     * @param  int $id_orchestration pastell entityId
     * @return \Cake\Http\Client\Response
     */
    public function getArAct($documentId, $id_orchestration)
    {
        return $this->http->getArActes($id_orchestration, $documentId);
    }

    /**
     * get the bordereau.pdf file
     *
     * @param  int $documentId       documentId
     * @param  int $id_orchestration pastell entityId
     * @return \Cake\Http\Client\Response
     */
    public function getBordereau($documentId, $id_orchestration)
    {
        return $this->http->getBordereau($id_orchestration, $documentId);
    }

    /**
     * get the acte_tamponne.pdf file
     *
     * @param  int $documentId       documentId
     * @param  int $id_orchestration pastell entityId
     * @return \Cake\Http\Client\Response
     */
    public function getActeTamponne($documentId, $id_orchestration)
    {
        return $this->http->getActeTamponne($id_orchestration, $documentId);
    }

    /**
     * get the annexe_tamponne.pdf file
     *
     * @param  int $documentId       documentId
     * @param  int $id_orchestration pastell entityId
     * @param  int $annexeRank       annexeRank
     * @return \Cake\Http\Client\Response
     */
    public function getAnnexesTamponnee($documentId, $id_orchestration, $annexeRank)
    {
        return $this->http->getAnnexesTamponnee($id_orchestration, $documentId, $annexeRank);
    }

    /**
     * get the signed document file
     *
     * @param  int $id_orchestration documentId
     * @param  int $documentId       pastell entityId
     * @return \Cake\Http\Client\Response
     */
    public function getSignedDocument($id_orchestration, $documentId)
    {
        return $this->http->getSignedDocument($id_orchestration, $documentId);
    }

    /**
     * get documentSigné
     *
     * @param  int $entityId   The id of the structure on pastell
     * @param  int $documentId The id of the document on PASTELL
     * @return \Cake\Http\Client\Response
     */
    public function getSignBordereau($entityId, $documentId)
    {
        return $this->http->getSignBordereau($entityId, $documentId);
    }

    /**
     * get Annexe sortie I-Parapheur
     *
     * @param  int $entityId   The id of the structure on pastell
     * @param  int $documentId The id of the document on PASTELL
     * @return \Cake\Http\Client\Response
     */
    public function getIparapheurAnnexeSortie(int $entityId, string $documentId)
    {
        return $this->http->getIparapheurAnnexeSortie($entityId, $documentId);
    }

    /**
     * get Annexe sortie I-Parapheur
     *
     * @param  int $entityId   The id of the structure on pastell
     * @param  int $documentId The id of the document on PASTELL
     * @return \Cake\Http\Client\Response
     */
    public function getIparapheurHistoriqueFile(int $entityId, string $documentId)
    {
        return $this->http->getIparapheurHistoriqueFile($entityId, $documentId);
    }

    /**
     * @fixme  vérifier l'utilité de cette fonction !
     * get the bordereau.pdf file
     * @param  int $id_orchestration documentId
     * @param  int $documentId       pastell entityId
     * @return \Cake\Http\Client\Response
     */
    public function get($id_orchestration, $documentId)
    {
        return $this->http->getb($id_orchestration, $documentId);
    }

    /**
     * check if connexion ok
     *
     * @param  int $entityId entityId
     * @return bool
     */
    public function checkPastellConnexion($entityId)
    {
        $response = $this->http->checkConnexion($entityId);
        $data = $response->getJson();
        if (isset($data['id_e'])) {
            return true;
        }

        return false;
    }

    /**
     * remove classification to project whith status : brouillon, en-cours-de-validation, valide,
     * refuse, declare-signe,  signe-i-parapheur, refuse-i-parapheur, en-cours-signature-i-parapheur, a-teletransmettre
     *
     * @param  int $structureId structureId
     * @return void
     */
    public function clearProjectsClassification($structureId)
    {
        $ContainersStateacts = $this->fetchTable('ContainersStateacts');
        /**
         * @var \App\Controller\Component\ContainersTable $Containers
         */
        $Containers = $this->fetchTable('Containers');

        //liste des containers
        $containers = $Containers->find()->select(['Containers.id', 'Containers.project_id'])
            ->innerJoinWith('Stateacts')
            ->where(
                [
                'Containers.structure_id' => $structureId,
                'Stateacts.id in' => [DRAFT, VALIDATION_PENDING, VALIDATED, REFUSED, DECLARE_SIGNED,
                    PARAPHEUR_SIGNED, PARAPHEUR_REFUSED, PARAPHEUR_SIGNING, TO_TDT],
                //Permet de mettre une condition sur le id du dernier état du container.
                'ContainersStateacts.id' => $ContainersStateacts->getLastByContainersIdSubquery(),
                ]
            )->disableHydration()->toArray();

        if (empty($containers)) {
            return;
        }

        $projectIds = Hash::extract($containers, '{n}.project_id');
        $containerIds = Hash::extract($containers, '{n}.id');

        $Containers->getConnection()->begin();
        try {
            $Containers->Projects->removeNature($projectIds);
            $Containers->Maindocuments->removeCodetype($containerIds);
            $Containers->Annexes->removeCodetype($containerIds);
        } catch (Exception $e) {
            Log::error('impossible supprimer la classification associée aux projets : ' . $e->getMessage());
            $Containers->getConnection()->rollback();
        }
        $Containers->getConnection()->commit();
    }

    /**
     * Maj types actes
     * TO @FIXME ASAP
     *
     * @param  int $structureId id structure
     * @return void
     */
    private function updateTypeactes(int $structureId)
    {
        $Natures = $this->fetchTable('Natures');
        $typesactsTable = $this->fetchTable('Typesacts');

        $typeacts = $typesactsTable->find('all')
            ->where(['Typesacts.structure_id' => $structureId])
            ->toArray();

        $correspondance = [
            'Autre' => 'AU',
            'Contrat et convention' => 'CC',
            'Arrêté réglementaire' => 'AR',
            'Délibération' => 'DE',
            'Arrêté individuel' => 'AI',
        ];
        foreach ($typeacts as $typeact) {
            $nature_id = $Natures
                ->find('all')
                ->select('id')
                ->where(
                    ['structure_id' => $structureId,
                    ['typeabrege' => $correspondance[$typeact->name]],
                    ]
                )->firstOrFail()->get('id');
            $typeact->nature_id = $nature_id;
            $typesactsTable->save($typeact, ['checkRules' => false]);
        }
    }
}
