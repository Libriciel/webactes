<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Event\EventInterface;
use Cake\Utility\Hash;
use Psr\Log\LogLevel;
use function App\hash_get_call_user_func_array as hash_get_call_user_func_array;

/**
 * The class ApiLoggingComponent can be used to record API accesses in the systemlogs table.
 *
 * Example component usage, in a controller.
 * ```php
 *  public function initialize()
 *  {
 *      parent::initialize();
 *
 *      $this->loadComponent(
 *          'ApiLogging',
 *          [
 *              'add' => [
 *                  'code' => 201,
 *                  'extra' => [
 *                      'response.id'
 *                  ],
 *                  'message' => [
 *                      'Ajout du projet "%s" (%d) par "%s" (%d)',
 *                      'request.data.project|json_decode,true.name',
 *                      'response.id',
 *                      'session.keycloak_id',
 *                      'session.Auth.User.id'
 *                  ]
 *              ],
 *              'edit' => [
 *                  'code' => [200, 204],
 *                  'extra' => [
 *                      'request.params.pass.0'
 *                  ],
 *                  'message' => [
 *                      'Modification du projet "%s" (%d) par "%s" (%d)',
 *                      'request.data.project|json_decode,true.name',
 *                      'request.params.pass.0',
 *                      'session.keycloak_id',
 *                      'session.Auth.User.id'
 *                  ]
 *              ],
 *              'delete' => [
 *                  'code' => 204,
 *                  'extra' => [
 *                      'request.params.pass.0'
 *                  ],
 *                  'message' => [
 *                      'Suppression du projet "%s" (%d) par "%s" (%d)',
 *                      'request.data.project|json_decode,true.name',
 *                      'request.params.pass.0',
 *                      'session.keycloak_id',
 *                      'session.Auth.User.id'
 *                  ]
 *              ],
 *          ]
 *      );
 *  }
 * ```
 *
 * The keys "code" and "extra" are not mandatory.
 * - "code" accepts any HTTP return code (true, by default) or an integer (200) or an array of integers [200, 204]
 * - the "extra" key can be used to make queries afterwards (see below)
 *
 * The "extra" and "message" keys can contain paths to extract data (using Hash::get path syntax):
 * - request.data, from the request data
 * - request.params, from the request params
 * - response, from the response variables sent to the view
 * - session to read from the session
 *
 * Functions can also be used in paths, for instance: request.data.project|json_decode,true.name
 * - extract "request.data.project"
 * - call json_decode($result, true)
 * - extract "name" from the result
 *
 * Example on how to get systemlogs for the current project (used parameter: id)
 * ```php
 *  $Systemlogs = $this->fetchTable('Systemlogs');
 *  $query = $Systemlogs
 *      ->find()
 *      ->select([
 *          'id',
 *          'action',
 *          'created',
 *          'modified',
 *          'Users.civility',
 *          'Users.lastname',
 *          'Users.firstname',
 *          'Users.username']
 *      )
 *      ->contain(['Users'])
 *      ->where(
 *          [
 *              'plugin IS NULL',
 *              'controller' => 'Projects',
 *              'OR' => [
 *                  ['extra' => serialize([(int)$id])],
 *                  ['extra' => serialize([(string)$id])],
 *              ]
 *          ]
 *      )
 *      ->enableHydration(false)
 *      ->order([
 *          'Systemlogs.created' => 'ASC',
 *          'Systemlogs.id' => 'ASC'
 *      ]);
 * ```
 *
 * For this to work, the following relation has to be created in the SystemlogsTable class
 * ```php
 *  public function initialize(array $config)
 *  {
 *      parent::initialize($config);
 *
 *      // ...
 *
 *      $this->belongsTo('Users', [
 *          'bindingKey' => 'keycloak_id',
 *          'foreignKey' => 'keycloak_id'
 *      ]);
 *  }
 * ```
 *
 * Example on how to get a user record with all it's systemlogs activities:
 * ```php
 *  $Users = $this->fetchTable('Users');
 *  $query = $Users
 *      ->find()
 *      ->contain(
 *          [
 *              'Systemlogs' => [
 *                  'sort' => [
 *                      'created' => 'ASC',
 *                      'id' => 'ASC'
 *                  ]
 *              ]
 *          ]
 *      )
 *      ->where(['Users.keycloak_id' => 'azdaae'])
 *      ->enableHydration(false);
 * ```
 *
 * For this to work, the following relation has to be created in the UsersTable class
 * ```php
 *  public function initialize(array $config)
 *  {
 *      parent::initialize($config);
 *
 *      // ...
 *
 *      $this->hasMany('Systemlogs', [
 *          'bindingKey' => 'keycloak_id',
 *          'foreignKey' => 'keycloak_id',
 *          'dependant' => false
 *      ]);
 *  }
 * ```
 *
 * @package App\Controller\Component
 */
class ApiLoggingComponent extends AppComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * Cache for _getParams;
     *
     * @var array
     */
    protected $_paramsCache = [];

    /**
     * Extract the needed parameters for the given keys, using a live cache for the current request.
     *
     * keys can contain paths to extract data (using Hash::get path syntax):
     * - request.data, from the request data
     * - request.params, from the request params
     * - response, from the response variables sent to the view
     * - session to read from the session
     *
     * Functions can also be used in paths, for instance: request.data.project|json_decode,true.name
     * - extract "request.data.project"
     * - call json_decode($result, true)
     * - extract "name" from the result
     *
     * @param  array $keys The keys to extract data for.
     * @return array
     */
    protected function _getParams(array $keys)
    {
        $controller = $this->getController();

        $return = [];
        foreach (array_unique($keys) as $path) {
            if (array_key_exists($path, $this->_paramsCache) === true) {
                $return[$path] = $this->_paramsCache[$path];
            } else {
                if (strpos($path, 'request.data.') === 0) {
                    $return[$path] = hash_get_call_user_func_array(
                        $controller->getRequest()->getData(),
                        substr($path, 13)
                    );
                } elseif (strpos($path, 'request.params.') === 0) {
                    $return[$path] = hash_get_call_user_func_array(
                        $controller->getRequest()->getAttribute('params'),
                        substr($path, 15)
                    );
                } elseif (strpos($path, 'response.') === 0) {
                    $return[$path] = Hash::get(
                        $controller->viewBuilder()->getVars(),
                        substr($path, 9)
                    );
                } elseif (strpos($path, 'session.') === 0) {
                    $return[$path] = hash_get_call_user_func_array(
                        $controller->getRequest()->getSession()->read(),
                        substr($path, 8)
                    );
                }
                $this->_paramsCache[$path] = $return[$path];
            }
        }

        return $return;
    }

    /**
     * Before render handler, examine the config and the status code of the request for the current action, insert an
     * entry in the systemlogs table if needed.
     *
     * @param \Cake\Event\Event $event The event.
     * @return void
     */
    public function beforeRender(EventInterface $event)
    {
        $controller = $this->getController();

        $action = Hash::get($controller->getRequest()->getAttribute('params'), 'action');
        $config = $this->getConfig($action);

        if (empty($config) === false) {
            $config += ['code' => true, 'extra' => null];

            $code = $controller->getResponse()->getStatusCode();

            if ($config['code'] === true || in_array($code, (array)$config['code'], true) === true) {
                // Message
                $message = (array)Hash::get($config, 'message');
                $format = $message[0];
                $params = count($message) > 1 ? array_slice($message, 1) : [];
                $msgstr = vsprintf($format, $this->_getParams($params));

                // Extra
                $extra = $config['extra'];
                if (empty($extra) === false) {
                    $extra = serialize(array_values($this->_getParams($extra)));
                }

                // Record
                $record = [
                    'organization_id' => $controller->getRequest()->getSession()->read('organization_id'),
                    'structure_id' => $controller->getRequest()->getSession()->read('Auth.Role.structure_id'),
                    'keycloak_id' => $controller->getRequest()->getSession()->read('keycloak_id'),
                    'plugin' => Hash::get($controller->getRequest()->getAttribute('params'), 'plugin'),
                    'controller' => Hash::get($controller->getRequest()->getAttribute('params'), 'controller'),// @codingStandardsIgnoreLine
                    'action' => Hash::get($controller->getRequest()->getAttribute('params'), 'action'),
                    'pass' => serialize(Hash::get($controller->getRequest()->getAttribute('params'), 'pass')),// @codingStandardsIgnoreLine
                    'code' => $code,
                    'message' => $msgstr,
                    'extra' => $extra,
                ];

                $Systemlogs = $this->fetchTable('Systemlogs');
                $systemlog = $Systemlogs->newEntity($record);

                if ($Systemlogs->save($systemlog, ['atomic' => false]) === false) {
                    $msgid = 'Cannot save the following record in systemlogs: %s (error was: %s)';
                    $this->log(
                        sprintf(
                            $msgid,
                            serialize($record),
                            serialize($systemlog->getErrors())
                        ),
                        LogLevel::ERROR
                    );
                }
            }
        }
    }
}
