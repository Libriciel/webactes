<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Workflow;
use App\Validation\Validator;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Utility\Hash;
use Exception;

/**
 * Workflow Controller
 *
 * @property WorkflowsTable Workflows
 * @property ProjectsTable Projects
 * @property UsersTable Users
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\Workflow[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WorkflowsController extends AppController
{
    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryParams');
        $this->loadComponent('QueryUtils');
    }

    protected function getList(array $params): void
    {
        $query = $this->Workflows
            ->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()])
            ->where([
                isset($params['active']) && ($params['active'] === 'false' || $params['active'] === 'true')
                    ? ['Workflows.active' => $params['active'] === 'true' ? true : false]
                    : [],
                isset($params['name'])
                    ? $this->QueryUtils->getIlikeQueryExpression('Workflows.name', "%{$params['name']}%")
                    : '',
            ])
            ->orderAsc('Workflows.name');

        if ($params['paginate'] === 'true') {
            $this->paginate += ['maxLimit' => PAGINATION_LIMIT];
            $circuits = $this->paginate($query);
        } else {
            $circuits = $query->all();
        }

        // @info: 500 -> 2925ms vs. 14837ms
        if ($params['steps'] === 'true') {
            foreach ($circuits as $circuit) {
                $steps = $circuit->getSteps();
                Workflow::completeUserData($steps);
                $circuit->steps = $steps;
            }
        }

        $this->set(compact('circuits'));
    }

    /**
     * Index method
     *
     * @return \App\Controller\Response|void
     * @access  public
     * @created 29/03/2019
     * @version V0.0.9
     */
    public function index()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can access to circuits');
        }

        $params = $this->QueryParams->validateIndexWithPaginationTrueFalse(
            (new Validator())
                ->allowPresence('active')
                ->allowEmptyString('active')
                ->inList('active', ['true', 'false'])
                ->allowPresence('steps')
                ->allowEmptyString('steps')
                ->inList('steps', ['true', 'false'])
                ->allowPresence('name')
                ->allowEmptyString('name'),
            ['active' => null, 'name' => null, 'steps' => 'false'],
        );

        $this->getList($params);
    }

    /**
     * Get the full list of active workflows, for any role.
     */
    public function active()
    {
        $this->QueryParams->validate(
            (new Validator())
                ->allowPresence('name')
                ->allowEmptyString('name'),
            ['name' => null],
        );

        $overwrite = ['active' => 'true', 'paginate' => 'false', 'steps' => 'true'];
        $this->getList($this->getRequest()->getQueryParams() + $overwrite);
    }

    /**
     * View method
     *
     * @param string|null $id Workflow id.
     * @return \Cake\Http\Response|void
     * @throws \App\Controller\RecordNotFoundException When record not found.
     * @access  public
     * @created 29/03/2019
     * @version V0.0.9
     */
    public function view(?string $id = null)
    {
        $workflow = $this->Workflows->get(
            (int)$id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $steps = $workflow->getSteps();
        Workflow::completeUserData($steps);
        $workflow->steps = $steps;

        $this->set('circuit', $workflow);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     * @throws \Cake\Http\Exception\UnauthorizedException|\Exception
     * @access  public
     * @created 29/03/2019
     * @version V0.0.9
     */
    public function add()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can add a circuit');
        }

        $workflow = $this->Workflows->newEntity($this->request->getData());

        $deployment = $this->createNewDeployment($workflow, $this->request->getData());
        if ($deployment === false) {
            return;
        }

        //Insert Flowable id into webActes database
        // AFAIRE: refacto ? en l'état patchEntity avec le deployment est pas super élégant, un generateWorkflowFromDeployment serait pas mal
        $workflow = $this->Workflows->patchEntity($workflow, $deployment);
        if ($this->Workflows->save($workflow)) {
            $steps = $workflow->getSteps();

            Workflow::completeUserData($steps);

            $workflow->steps = $steps;

            $this->setResponse($this->getResponse()->withStatus(201));

            $this->set('circuit', $workflow);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $workflow->getErrors());
        }
    }

    /**
     * @param int $id id
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException|\Exception
     */
    public function edit(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can edit a circuit');
        }

        $data = $this->request->getData();

        $workflow = $this->Workflows->get(
            (int)$id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        // interdiction d'edit @fixme : déplacer dans le buildRules de WorkflowsTable une fois le fixme (*) corrigé
        if (!$workflow->is_editable) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', ['id' => ['Cannot edit a workflow with instanced projects']]);

            return;
        }

        $deployment = $this->createNewDeployment($workflow, $data);
        if ($deployment === false) {
            $this->setResponse($this->getResponse()->withStatus(400));

            return;
        }

        $deployment['active'] = $workflow['active'];

        $workflow = $this->Workflows->patchEntity($workflow, $deployment);
        if ($this->Workflows->save($workflow)) {
            $steps = $workflow->getSteps();

            Workflow::completeUserData($steps);

            $workflow->steps = $steps;

            $this->setResponse($this->getResponse()->withStatus(201));

            $this->set('circuit', $workflow);
        } else { // @fixme (*) : si problème à la sauvegarde du circuit, supprimer les infos flowable
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $workflow->getErrors());
        }
    }

    /**
     * Active un circuit
     *
     * @param int $id circuit
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function activate(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can activate a circuit');
        }

        $workflow = $this->Workflows->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $workflow = $this->Workflows->patchEntity($workflow, ['active' => true]);
        if ($this->Workflows->save($workflow)) {
            $this->setResponse($this->getResponse()->withStatus(201));

            $this->set('circuit', $workflow);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $workflow->getErrors());
        }
    }

    /**
     * Désactive un circuit
     *
     * @param int $id circuit
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function deactivate(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can deactivate a circuit');
        }

        $workflow = $this->Workflows->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $workflow = $this->Workflows->patchEntity($workflow, ['active' => false]);
        if ($this->Workflows->save($workflow)) {
            $this->setResponse($this->getResponse()->withStatus(201));

            $this->set('circuit', $workflow);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $workflow->getErrors());
        }
    }

    /**
     * Supprime un circuit si possible : suppression logique uniquement afin de conserver les données pour l'historique
     *
     * @param int $id circuit
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function delete(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can delete a circuit');
        }

        $workflow = $this->Workflows->get(
            (int)$id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $workflow = $this->Workflows->patchEntity($workflow, ['deleted' => time()]);

        if ($this->Workflows->save($workflow)) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set('errors', $workflow->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * retourne un deployment qui peut être utilisé directement pour patcher le workflow
     *
     * @param \App\Model\Entity\Workflow $workflow workflow
     * @param array $data data
     * @return bool|array
     * @throws \Exception
     */
    protected function createNewDeployment($workflow, $data)
    {
        if (!$this->checkData($data, $workflow)) {
            return false;
        }

        // Set the right structure and project
        $data['organization_id'] = $this->Multico->getCurrentOrganizationId();
        $data['structure_id'] = $this->Multico->getCurrentStructureId();

        // Création et engregistrement d'un fichier bpm20.xml pour l'envoit à flowable
        $dataSendToFlowable = $workflow->convertJsonToXml($data);

        if ($dataSendToFlowable === false) {
            $this->setResponse($this->getResponse()->withStatus(500)); // AFAIRE: changer code d'erreur ?
            $this->set('errors', ['file' => 'error when saving file']); // AFAIRE: changer message d'erreur ?

            throw new Exception('error when saving file'); // AFAIRE: à voir
        }

        //Create the deployment in Flowable
        $deployment = $workflow->createDeployment($dataSendToFlowable);

        // ajout du nom ert description du circuit au données pour le patch
        $deployment['name'] = $data['name'];
        $deployment['description'] = $data['description'] ?? null;
        $deployment['active'] = false; //default to false

        //Insert Flowable id into webActes database
        // AFAIRE: refacto ? en l'état patchEntity avec le deployment est pas super élégant, un generateWorkflowFromDeployment serait pas mal

        return $deployment;
    }

    /**
     * Check the data validity, set the response code and the error if needed
     *
     * @param array $data data
     * @return bool
     */
    protected function checkData(array $data): bool
    {
        // Check si le tableau reçu contient les keys "name" et "steps"
        if (empty($data['name'])) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', ['name' => 'missing or empty value']);

            return false;
        }
        if (empty($data['steps']) || !is_array($data['steps'])) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', ['steps' => 'missing, empty or invalid value']);

            return false;
        }

        $structureId = $this->Multico->getCurrentStructureId();
        /** @var \App\Controller\StructuresUsersTable $StructuresUsers */
        $StructuresUsers = $this->fetchTable('StructuresUsers');

        // Check si aucun "name" dans la key "steps" n'est vide
        foreach ($data['steps'] as $idx => $step) {
            if (empty($step['name'])) {
                $this->setResponse($this->getResponse()->withStatus(400));
                $this->set('errors', ['steps' => 'missing name data for step #' . $idx]);

                return false;
            }

            if (empty($step['users'])) {
                $this->setResponse($this->getResponse()->withStatus(400));
                $this->set('errors', ['steps' => 'missing users data for step #' . $idx . ' : ' . $step['name']]);

                return false;
            }

            $userCount = $StructuresUsers->find()
                ->select('StructuresUsers.user_id')
                ->where(
                    [
                        'StructuresUsers.structure_id' => $structureId,
                        'StructuresUsers.user_id IN' => $step['users'],

                    ]
                )
                ->count();

            if (count($step['users']) !== $userCount) {
                $this->setResponse($this->getResponse()->withStatus(400));
                $this->set('errors', ['steps' => 'invalid users data for step #' . $idx . ' : ' . $step['name']]);

                return false;
            }
        }

        // unicité des noms des étapes
        $names = Hash::extract($data, 'steps.{n}.name');
        if (count($names) > 1 && count($names) !== count(array_unique($names))) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', ['steps' => 'duplicates in steps names']);

            return false;
        }

        return true;
    }

    //    /**
    //     * If at least an instance exists based on the project key given return all instances of the instance based on the project key
    //     * @param int $projectId projectId
    //     *
    //     * @return Response|void
    //     */
    //    public function historyInstance(int $projectId)
    //    {
    //        $this->request->allowMethod('GET');
    //
    //        $this->Projects = $this->fetchTable('Projects');
    //        $project = $this->Projects->find()
    //            ->select([
    //                'id_flowable_instance',
    //                'workflow_id',
    //            ])
    //            ->where([
    //                'id' => $projectId,
    //                'Projects.structure_id' => $this->Multico->getCurrentStructureId(),
    //            ])
    //            ->firstOrFail();
    //        // project not into a circuit :
    //        if (!isset($project->id_flowable_instance)) {
    //            throw new NotFoundException();
    //        }
    //
    //        $workflow = $this->Workflows->find()
    //            ->select([
    //                'id',
    //                'name',
    //                'description',
    //                'process_definition_key',
    //                'file',
    //            ])
    //            ->where([
    //                'id' => $project->workflow_id,
    //                'structure_id' => $this->Multico->getCurrentStructureId(),
    //            ])
    //            ->firstOrFail();
    //
    //        $response = $workflow->getTaskHistoryByProjectInstanceId($project->id_flowable_instance);
    //
    //        if ($response->getStatusCode() === 200) {
    //            $this->setResponse($this->getResponse()->withStatus($response->getStatusCode()));
    //
    //            $steps = $workflow->getSteps(); // tout
    //            $tasks = $response->getJson()['data']; // passé
    //
    //            $this->Users = $this->fetchTable('Users');
    //
    //            foreach ($steps as &$step) {
    //                // recuperation de la tache aayant le même nom que le step
    //                $matchingTask = Hash::extract($tasks, '{n}[name=/' . $step['name'] . '/]');
    //
    //                if (count($matchingTask)) {
    //                    $task = $matchingTask[0];
    //
    //                    $validation = Hash::extract($task['variables'], '{n}[name=/userId/]');
    //
    //                    if ($validation !== []) { // si on a une variable 'userId', c'est du validé (passé)
    //                        $userId = $validation[0]['value'];
    //                        $user = $this->Users
    //                            ->find()
    //                            ->select([
    //                                'id',
    //                                'firstname',
    //                                'lastname',
    //                            ])
    //                            ->where([
    //                                'Users.id' => $userId,
    //                                // AFAIRE: 'structure_id' => $this->Multico->getCurrentStructureId()
    //                            ])
    //                            ->enableHydration(false)
    //                            ->firstOrFail();
    //
    //                        $step['actedUponBy'] = $user;
    //                        $step['state'] = Hash::extract($task['variables'], '{n}[name=/action/]')[0]['value'];
    //                        $step['comment'] = Hash::extract($task['variables'], '{n}[name=/comment/]')[0]['value'] ?? null;
    //                        $date = new \DateTime($task['endTime']);
    //                        $step['actedTime'] = $date->format(DATE_ATOM);
    //                    } else { // sinon c'est du 'en cours' (présent)
    //                        $step['state'] = FlowableFacade::STATUS_CURRENT;
    //                    }
    //                    $step['id'] = $task['id'];
    ////                    }
    //                } else {
    //                    $step['state'] = FlowableFacade::STATUS_PENDING;
    //                }
    //            }
    //
    //            $endActivityId = $workflow->getInstanceHistoryByProjectInstanceId($project->id_flowable_instance)->getJson()['data'][0]['endActivityId'];
    //            $isOver = $endActivityId !== null;
    //            $rejected = $endActivityId === FlowableFacade::STATUS_REJECTED;
    //
    //            $this->set('instance', [
    //                'circuitId' => $workflow->id,
    //                'name' => $workflow->name,
    //                'description' => $workflow->description,
    //                'isOver' => $isOver,
    //                'isRejected' => $rejected,
    //                'steps' => $steps,
    //            ]);
    //
    //            return;
    //
    //            // AFAIRE: formater pour récupérer la liste des tâches avec les infos utilisateurs, l'action en cours (penser à filtrer les actions admin)
    //            // boolean circuit_fini, (cf front, le format de donnée est déjà défini)
    //        }
    ////        }
    //        $this->setResponse($this->getResponse()->withStatus(400));
    //
    //        return $this->response;
    //    }
}
