<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Typesitting;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Utility\Hash;

/**
 * Typesittings Controller
 *
 * @property \App\Model\Table\TypesittingsTable $Typesittings
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 */
class TypesittingsController extends AppController
{
    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryUtils');
    }

    /**
     * Function permettant d'afficher la liste des types de session
     *
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V2.0
     */
    public function index()
    {
        $queryParams = $this->getRequest()->getQueryParams();

        $this->paginate = [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            'limit' => $queryParams['limit'] ?? PAGINATION_LIMIT,
            'maxLimit' => MAXIMUM_LIMIT,
            'conditions' => [
                'OR' => [
                    isset($queryParams['name'])
                        ? $this->QueryUtils->getIlikeQueryExpression('Typesittings.name', "%{$queryParams['name']}%")
                        : '',
                ],
                'AND' => [
                    isset($queryParams['active'])
                    && ($queryParams['active'] === 'false' || $queryParams['active'] === 'true')
                        ? ['Typesittings.active' => boolval($queryParams['active'])]
                        : [],
                ],
            ],
        ];

        $typesittings =
            $this->paginate($this->Typesittings->find('all')->contain(['Typesacts', 'ActorGroups'])->orderAsc('name'));
        $this->set('typeSittings', $typesittings);
    }

    /**
     * Function permettant d'afficher les informations sur le type d'une session
     *
     * @param int $id Typesitting id.
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V2.0
     */
    public function view($id)
    {
        $typesitting = $this->Typesittings->get($id, ['limitToStructure' => $this->Multico->getCurrentStructureId()]);
        $this->set('typesitting', $typesitting);
    }

    /**
     * Function permettant l'ajout d'un type de session
     *
     * @return void Redirects on successful add, renders view otherwise.
     * @access  public
     * @created 12/02/2019
     * @version V2.0
     */
    public function add()
    {
        $this->request->allowMethod('post');
        $data = $this->getRequest()->getData();

        if (!empty($data['actorGroups'])) {
            $data['actorGroups']['_ids'] = Hash::extract($data['actorGroups'], '{n}.id');
        }
        if (!empty($data['typeActs'])) {
            $data['typeActs']['_ids'] = Hash::extract($data['typeActs'], '{n}.id');
        }
        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();

        $typesitting = $this->Typesittings->newEntity(
            $data,
            [
                'associated' => [
                    'Typesacts' => ['onlyIds' => true],
                    'ActorGroups' => ['onlyIds' => true],
                ],
            ]
        );
        if ($this->Typesittings->save($typesitting, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set(compact('typesitting'));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $typesitting->getErrors());
        }
    }

    /**
     * Function permettant la modification d'un type de session
     *
     * @param int $id Typesitting id.
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V2.0
     */
    public function edit(string $id)
    {
        $structureId = $this->Multico->getCurrentStructureId();
        $typesitting = $this->Typesittings->get(
            $id,
            [
                'contain' => [],
                'limitToStructure' => $structureId,
            ]
        );

        $data = $this->request->getData();

        if (!empty($data['actorGroups'])) {
            $data['actorGroups']['_ids'] = Hash::extract($data['actorGroups'], '{n}.id');
        }

        if (!empty($data['typeActs'])) {
            $data['typeActs']['_ids'] = Hash::extract($data['typeActs'], '{n}.id');
        }

        if ($this->request->is(['patch', 'put'])) {
            $typesitting = $this->Typesittings->patchEntity(
                $typesitting,
                $data,
                [
                    'associated' => [
                        'Typesacts' => ['onlyIds' => true],
                        'ActorGroups' => ['onlyIds' => true],
                    ],
                ]
            );

            if ($this->request->is('patch')) {
                $this->editPatch($typesitting);

                return;
            }

            if ($this->request->is('put')) {
                $this->editPut($typesitting);

                return;
            }

            //log the error in debug mode
            $this->setResponse($this->getResponse()->withStatus(400));
        }

        $this->set(compact('typesitting'));
    }

    /**
     * Fonction permettant la modification d'un type de session en mode PATCH
     *
     * @param \App\Model\Entity\Typesitting $typesitting typesitting
     * @return void
     * @access  private
     * @created 12/02/2019
     * @version 2.0
     */
    private function editPatch(Typesitting $typesitting)
    {
        if ($this->Typesittings->save($typesitting, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set('errors', $typesitting->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Fonction permettant la modification d'un type de session en mode PUT
     *
     * @param \App\Model\Entity\Typesitting $typesitting typesitting
     * @return void
     * @access  private
     * @created 12/02/2019
     * @version v2.0
     */
    private function editPut(Typesitting $typesitting)
    {
        if ($this->Typesittings->save($typesitting, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set('typesitting', $typesitting);
        } else {
            $this->set('errors', $typesitting->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Fonction permettant la suppression d'un type de session
     *
     * @param int $id Typesitting id.
     * @return void Redirects to index.
     * @access  public
     * @created 12/02/2019
     * @version v2.0
     */
    public function delete($id)
    {
        if ($this->request->is('delete')) {
            $typesitting = $this->Typesittings->get(
                $id,
                ['limitToStructure' => $this->Multico->getCurrentStructureId()]
            );

            if ($this->Typesittings->delete($typesitting)) {
                $this->setResponse($this->getResponse()->withStatus(204));
            } else {
                $this->setResponse($this->getResponse()->withStatus(400));
            }

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(404));
    }

    /**
     * Active un typeSitting
     *
     * @param int $id typeSitting
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function activate(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can activate a type sitting');
        }

        $structureId = $this->Multico->getCurrentStructureId();

        $typesitting = $this->Typesittings->get(
            $id,
            [
                'contain' => [],
                'limitToStructure' => $structureId,
            ]
        );

        $typesitting = $this->Typesittings->patchEntity($typesitting, ['active' => true]);
        if ($this->Typesittings->save($typesitting)) {
            $this->setResponse($this->getResponse()->withStatus(201));

            $this->set('typesitting', $typesitting);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $typesitting->getErrors());
        }
    }

    /**
     * Désactive un typeSitting
     *
     * @param int $id typeSitting
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function deactivate(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can deactivate a type sitting');
        }

        $structureId = $this->Multico->getCurrentStructureId();

        $typesitting = $this->Typesittings->get(
            $id,
            [
                'contain' => [],
                'limitToStructure' => $structureId,
            ]
        );

        $typesitting = $this->Typesittings->patchEntity($typesitting, ['active' => false]);

        if ($this->Typesittings->save($typesitting)) {
            $this->set('typesitting', $typesitting);
            $this->set('success', true);
            $this->setResponse($this->getResponse()->withStatus(201));
        } else {
            $this->set('success', false);
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }
}
