<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Maindocuments Controller
 *
 * @property \App\Model\Table\MaindocumentsTable $Maindocuments
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Model\Entity\Maindocument[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MaindocumentsController extends AppController
{
    //    /**
    //     * Index method
    //     *
    //     * @return \Cake\Http\Response|void
    //     */
    //    public function index()
    //    {
    //        $this->paginate = [
    //            'contain' => ['Organizations', 'Structures', 'Containers']
    //        ];
    //        $maindocuments = $this->paginate($this->Maindocuments);
    //
    //        $this->set(compact('maindocuments'));
    //    }

    /**
     * View method
     *
     * @param  string|null $id Maindocument id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @access  public
     * @created 11/03/2019
     * @version V0.0.9
     */
    public function view($id = null)
    {
        $maindocument = $this->Maindocuments->get(
            $id,
            [
            'contain' => ['Files'],
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $file = $maindocument['files'][0];
        $response = $this->response->withFile(
            $file['path'],
            ['download' => true, 'name' => 'foo']
        );

        return $response;
    }

    //    /**
    //     * Add method
    //     *
    //     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
    //     */
    //    public function add()
    //    {
    //        $maindocument = $this->Maindocuments->newEntity();
    //        if ($this->request->is('post')) {
    //            $maindocument = $this->Maindocuments->patchEntity($maindocument, $this->request->getData());
    //            if ($this->Maindocuments->save($maindocument)) {
    //                $this->Flash->success(__('The maindocument has been saved.'));
    //
    //                return $this->redirect(['action' => 'index']);
    //            }
    //            $this->Flash->error(__('The maindocument could not be saved. Please, try again.'));
    //        }
    //        $organizations = $this->Maindocuments->Organizations->find('list', ['limit' => 200]);
    //        $structures = $this->Maindocuments->Structures->find('list', ['limit' => 200]);
    //        $containers = $this->Maindocuments->Containers->find('list', ['limit' => 200]);
    //        $this->set(compact('maindocument', 'organizations', 'structures', 'containers'));
    //    }
    //
    //    /**
    //     * Edit method
    //     *
    //     * @param string|null $id Maindocument id.
    //     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
    //     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //     */
    //    public function edit($id = null)
    //    {
    //        $maindocument = $this->Maindocuments->get($id, [
    //            'contain' => []
    //        ]);
    //        if ($this->request->is(['patch', 'post', 'put'])) {
    //            $maindocument = $this->Maindocuments->patchEntity($maindocument, $this->request->getData());
    //            if ($this->Maindocuments->save($maindocument)) {
    //                $this->Flash->success(__('The maindocument has been saved.'));
    //
    //                return $this->redirect(['action' => 'index']);
    //            }
    //            $this->Flash->error(__('The maindocument could not be saved. Please, try again.'));
    //        }
    //        $organizations = $this->Maindocuments->Organizations->find('list', ['limit' => 200]);
    //        $structures = $this->Maindocuments->Structures->find('list', ['limit' => 200]);
    //        $containers = $this->Maindocuments->Containers->find('list', ['limit' => 200]);
    //        $this->set(compact('maindocument', 'organizations', 'structures', 'containers'));
    //    }
    //
    //    /**
    //     * Delete method
    //     *
    //     * @param string|null $id Maindocument id.
    //     * @return \Cake\Http\Response|null Redirects to index.
    //     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //     */
    //    public function delete($id = null)
    //    {
    //        $this->request->allowMethod(['post', 'delete']);
    //        $maindocument = $this->Maindocuments->get($id);
    //        if ($this->Maindocuments->delete($maindocument)) {
    //            $this->Flash->success(__('The maindocument has been deleted.'));
    //        } else {
    //            $this->Flash->error(__('The maindocument could not be deleted. Please, try again.'));
    //        }
    //
    //        return $this->redirect(['action' => 'index']);
    //    }
}
