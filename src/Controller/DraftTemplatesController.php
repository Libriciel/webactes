<?php
declare(strict_types=1);

namespace App\Controller;

use App\Validation\Validator;
use Cake\Utility\Hash;

/**
 * DraftTemplates Controller
 *
 * @property \App\Model\Table\DraftTemplatesTable $DraftTemplates
 * @property \App\Model\Table\TypesactsTable $Typesacts
 * @property \App\Model\Table\ProjectTextsTable $ProjectTexts
 * @property \App\Controller\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\DraftTemplate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DraftTemplatesController extends AppController
{
    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->Typesacts = $this->fetchTable('Typesacts');
        $this->ProjectTexts = $this->fetchTable('ProjectTexts');
        $this->loadComponent('QueryParams');
        $this->loadComponent('QueryUtils');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $params = $this->QueryParams->validateIndexWithPaginationTrueFalse(
            (new Validator())
                ->allowPresence('name')
                ->allowEmptyString('name'),
            ['name' => null]
        );

        $query = $this->DraftTemplates
            ->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()])
            ->contain([
                'DraftTemplateTypes',
                'Files' => function ($q) {
                    return $q->formatResults(function (\Cake\Collection\CollectionInterface $files) {
                        return $files->map(function ($file) {
                            $file['base_url'] = EDITOR_WOPI_CLIENT_URL;
                            $file['path_wopi'] = EDITOR_WOPI_PATH;

                            return $file;
                        });
                    });
                },
            ])
            ->where([
                isset($params['name'])
                    ? $this->QueryUtils->getIlikeQueryExpression('DraftTemplates.name', "%{$params['name']}%")
                    : '',
            ])
            ->orderAsc('DraftTemplates.name');

        if ($params['paginate'] === 'true') {
            $this->paginate = [
                'limit' => $params['limit'] ?? PAGINATION_LIMIT,
                'maxLimit' => isset($params['limit']) && ($params['limit'] <= PAGINATION_LIMIT)
                    ? $params['limit']
                    : PAGINATION_LIMIT,
                'sortableFields' => ['name'],
            ];
            $draftTemplates = $this->paginate($query);
        } else {
            $draftTemplates = $query->all();
        }

        $draftTemplates = Hash::map($draftTemplates->toArray(), '{n}', function ($array) {
            $array['draft_template_file'] = $array['files'][0] ?? [];
            unset($array['files']);

            return $array;
        });

        $this->set(compact('draftTemplates'));
    }

    /**
     * View method
     *
     * @param string|null $id Draft Template id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $draftTemplate = $this->DraftTemplates->get($id, [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            'contain' => ['DraftTemplateTypes', 'Files'],
        ]);

        $this->set(compact('draftTemplate'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod('post');
        $data = json_decode((string)$this->request->getBody(), true);

        $data = Hash::get($data, 'draftTemplate');

        if (isset($data['draft_template_type'])) {
            $data['draft_template_type_id'] = $data['draft_template_type']['id'];
        }

        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();
        $data['organization_id'] = $this->Multico->getCurrentOrganizationId();

        $draftTemplate = $this->DraftTemplates->newEntity($data, ['associated' => []]);

        if (
            $this->DraftTemplates->save(
                $draftTemplate,
                ['limitToStructure' => $this->Multico->getCurrentStructureId()]
            )
        ) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('draftTemplate', $draftTemplate);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $draftTemplate->getErrors());
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Draft Template id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['patch', 'put']);
        $data = json_decode((string)$this->request->getBody(), true);

        $data = Hash::get($data, 'draftTemplate');

        if (isset($data['draft_template_type'])) {
            $data['draft_template_type_id'] = $data['draft_template_type']['id'];
        }

        $draftTemplate = $this->DraftTemplates->get(
            $id,
            ['limitToStructure' => $this->Multico->getCurrentStructureId()]
        );
        $draftTemplate = $this->DraftTemplates->patchEntity($draftTemplate, $data, ['associated' => []]);

        if (
            $this->DraftTemplates->save(
                $draftTemplate,
                ['limitToStructure' => $this->Multico->getCurrentStructureId()]
            )
        ) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set('draftTemplate', $draftTemplate);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $draftTemplate->getErrors());
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Draft Template id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);
        $draftTemplate = $this->DraftTemplates->get(
            $id,
            ['limitToStructure' => $this->Multico->getCurrentStructureId()]
        );

        if (!$this->DraftTemplates->delete($draftTemplate)) {
            $this->setResponse($this->getResponse()->withStatus(400));
        }

        $this->setResponse($this->getResponse()->withStatus(204));
    }
}
