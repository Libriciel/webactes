<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * GenerateTemplateTypes Controller
 *
 * @property \App\Model\Table\GenerateTemplateTypesTable $GenerateTemplateTypes
 * @method \App\Model\Entity\GenerateTemplateType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GenerateTemplateTypesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'sortableFields' => ['name'],
            'order' => ['name' => 'asc'],
        ];

        $generateTemplateTypes = $this->paginate($this->GenerateTemplateTypes);

        $this->set(compact('generateTemplateTypes'));
    }
}
