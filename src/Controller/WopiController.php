<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Response;
use Cake\Routing\Router;
use Exception;

/**
 * Files Controller
 *
 * @property \App\Model\Table\FilesTable $Files
 * @property \App\Controller\Component\MulticoComponent $Multico
 */
class WopiController extends AppController
{
    public const APPLICATIONJSON = 'application/json';

    /**
     * @return void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->Auth->allow();
        $this->Files = $this->fetchTable('Files');
        $this->loadComponent('Keycloak');
    }

    /**
     * @param string $id Foo
     * @return \Cake\Http\Response
     * @throws \JsonException
     */
    public function getFileInfo(string $id): Response
    {
        $this->isWopiAuthorized();

        $this->request->allowMethod('get');

        $file = $this->Files->get(
            (int)$id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        if (file_exists($file['path'])) {
            $handle = fopen($file['path'], 'rb');
            $size = filesize($file['path']);
            $contents = fread($handle, $size);
            $SHA256 = base64_encode(hash('sha256', $contents, true));
            $file = [
                'BaseFileName' => $file['name'],
                'UserCanWrite' => true,
                'UserId' => $this->getRequest()->getSession()->read('user_id'),
                'UserFriendlyName' => sprintf(
                    '%s %s',
                    $this->getRequest()->getSession()->read('firstname'),
                    $this->getRequest()->getSession()->read('lastname')
                ),
                'Size' => $size,
                'SHA256' => $SHA256,
                //'Version' => '',
                'ReadOnly' => false,
                'FileExtension' => '.odt',
                'LastModifiedTime' => !empty($file->modified) ? $file->modified->format(DATE_ATOM) : '',
                'SupportsFileCreation' => false,
                'UserExtraInfo' => [
                    'avatar' => Router::fullBaseUrl() . '/img/cake.logo.png',
                    'mail' => $this->getRequest()->getSession()->read('email'),
                ],
            ];

            return $this->response
                ->withType(self::APPLICATIONJSON)
                ->withStringBody(json_encode($file, JSON_THROW_ON_ERROR));
        }

        $this->setResponse($this->getResponse()->withStatus(404));

        return $this->response
            ->withType(self::APPLICATIONJSON)
            ->withStringBody(json_encode(['errors' => ['File does not exist']], JSON_THROW_ON_ERROR));
    }

    /**
     * @param string $id Foo
     * @return \Cake\Http\Response
     */
    public function getFile(string $id): Response
    {
        $this->isWopiAuthorized();

        $this->request->allowMethod('get');

        $file = $this->Files->get(
            (int)$id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        return $this->response->withFile(
            $file['path'],
            ['type' => 'application/vnd.oasis.opendocument.text']
        );
    }

    /**
     *  Given a request access token and a document id, replaces the files with the POST request body.
     *  The PutFile wopi endpoint is triggered by a request with a POST verb at
     *  https://HOSTNAME/api/v1/wopi/files/<document_id>/contents
     *
     * @param int $id Foo
     * @return \Cake\Http\Response
     * @throws \JsonException
     */
    public function putFile(string $id): Response
    {
        $this->isWopiAuthorized();

        $this->request->allowMethod('post');

        $uploadedFilesStream = [
            'size' => $this->getRequest()->getBody()->getSize(),
            'contents' => $this->getRequest()->getBody()->getContents(),
        ];

        try {
            $file = $this->Files->saveInFsStream(
                (int)$id,
                $uploadedFilesStream,
                $this->Multico->getCurrentStructureId()
            );
        } catch (Exception $e) {
            return $this->response
                ->withType(self::APPLICATIONJSON)
                ->withStatus(401);
        }

            return $this->response
            ->withStringBody(json_encode([
                'LastModifiedTime' => $file->modified->format(DATE_ATOM),
            ], JSON_THROW_ON_ERROR))
            ->withStatus(200);
    }

    /**
     * @return void
     */
    public function isWopiAuthorized(): void
    {
        if (isset($this->request->getQueryParams()['access_token'])) {
            $token = urldecode($this->request->getQueryParams()['access_token']);

            if ($this->Keycloak->loginByToken($token)) {
                return;
            }
        }

        throw new UnauthorizedException('Token is invalid', 401);
    }

    /**
     * @return \Cake\Http\Response
     */
    public function settingsCollabora(): Response
    {
        $this->request->allowMethod('get');

        $configuration = [
            'kind' => 'configuration',
//            'remote_font_config' => [
//                'url' => Router::url(['controller' => 'Wopi', 'action' => 'fontsCollabora'], true),
//            ],
            'storage' => [
                'wopi' => [
                    'alias_groups' => [
                        'mode' => 'groups',
                        'groups' => [
                            ['host' => MAIN_URL, 'allow' => true],
                        ],
                    ],
                ],
            ],
        ];

        return $this->response
            ->withType(self::APPLICATIONJSON)
            ->withStringBody(json_encode($configuration));
    }

    /**
     * @return \Cake\Http\Response
     */
    public function fontsCollabora(): Response
    {
        $this->request->allowMethod('get');

        $configuration = [
            'kind' => 'fontconfiguration',
            'server' => 'webactes fonts service',
            'fonts' => [
                ['uri' => ''],
            ],
        ];

        return $this->response
            ->withType(self::APPLICATIONJSON)
            ->withStringBody(json_encode($configuration));
    }
}
