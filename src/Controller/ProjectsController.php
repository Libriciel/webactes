<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Stateact;
use App\Model\Table\HistoriesTable;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Http\Exception\NotFoundException;
use Cake\Utility\Hash;
use WrapperFlowable\Api\FlowableException;
use WrapperFlowable\Api\WrapperFactory;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\PermissionsComponent $Permissions
 * @property \App\Controller\Component\ProjectsIndexComponent $ProjectsIndex
 * @property \App\Controller\Component\ProjectsPermissionsComponent $ProjectsPermissions
 * @method \App\Model\Entity\Project[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProjectsController extends AppController
{
    use ServiceAwareTrait;

    public const ARACTE = 'arActes';
    public const BORDEREAU = 'bordereau';
    public const ACTE_TAMPONNE = 'acte_tamponne';
    public const ANNEXES_TAMPONNEES = 'annexes_tamponnee';

    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent(
            'ApiLogging',
            [
                'add' => [
                    'code' => 201,
                    'extra' => [
                        'response.id',
                    ],
                    'message' => [
                        'Ajout du projet "%s" (%d) par "%s" (%d)',
                        'request.data.project|json_decode,true.name',
                        'response.id',
                        'session.keycloak_id',
                        'session.Auth.User.id',
                    ],
                ],
                'edit' => [
                    'code' => 204,
                    'extra' => [
                        'request.params.pass.0',
                    ],
                    'message' => [
                        'Modification du projet "%s" (%d) par "%s" (%d)',
                        'request.data.project|json_decode,true.name',
                        'request.params.pass.0',
                        'session.keycloak_id',
                        'session.Auth.User.id',
                    ],
                ],
                'delete' => [
                    'code' => 204,
                    'extra' => [
                        'request.params.pass.0',
                    ],
                    'message' => [
                        'Suppression du projet "%s" (%d) par "%s" (%d)',
                        'request.data.project|json_decode,true.name',
                        'request.params.pass.0',
                        'session.keycloak_id',
                        'session.Auth.User.id',
                    ],
                ],
            ]
        );
        $this->loadComponent('ProjectsIndex');
        $this->loadComponent('ProjectsPermissions');
    }

    /**
     * Paginates projects for the GET /projects/index_drafts API entry ("Brouillons").
     *
     * @return void
     * @throws \Exception
     * @see \App\Controller\Component\ProjectsIndexComponent::paginateDrafts()
     */
    public function indexDrafts()
    {
        $this->ProjectsIndex->paginateDrafts();
    }

    /**
     * Paginates projects for the GET /projects/index_validating API entry ("En cours dans un circuit").
     *
     * @return void
     * @throws \WrapperFlowable\Api\FlowableException
     * @see \App\Controller\Component\ProjectsIndexComponent::paginateValidating()
     */
    public function indexValidating()
    {
        $this->ProjectsIndex->paginateValidating();
    }

    /**
     * Paginates projects for the GET /projects/index_to_validate API entry ("À traiter").
     *
     * @return void
     * @throws \WrapperFlowable\Api\FlowableException
     * @see \App\Controller\Component\ProjectsIndexComponent::paginateToValidate()
     */
    public function indexToValidate()
    {
        $this->ProjectsIndex->paginateToValidate();
    }

    /**
     * Paginates projects for the GET /projects/index_validated API entry ("Validés").
     *
     * @return void
     * @throws \Exception
     * @see \App\Controller\Component\ProjectsIndexComponent::paginateValidated()
     */
    public function indexValidated()
    {
        $this->ProjectsIndex->paginateValidated();
    }

    /**
     * Paginates projects for the GET /projects/index_to_transmit API entry ("À déposer sur le Tdt").
     *
     * @return void
     * @throws \Exception
     * @see \App\Controller\Component\ProjectsIndexComponent::paginateToTransmit()
     */
    public function indexToTransmit()
    {
        $this->ProjectsIndex->paginateToTransmit();
    }

    /**
     * Paginates projects for the GET /projects/index_ready_to_transmit API entry ("À envoyer en préfecture").
     *
     * @return void
     * @throws \Exception
     * @see \App\Controller\Component\ProjectsIndexComponent::paginateReadyToTransmit()
     */
    public function indexReadyToTransmit()
    {
        $this->ProjectsIndex->paginateReadyToTransmit();
    }

    /**
     * Paginates projects for the GET /projects/index_act API entry ("Liste des actes").
     *
     * @return void
     * @throws \Exception
     * @see \App\Controller\Component\ProjectsIndexComponent::paginateAct()
     */
    public function indexAct()
    {
        $this->ProjectsIndex->paginateAct();
    }

    /**
     * Paginates projects for the GET /projects/index_unassociated API entry ("Projets sans séances").
     *
     * @return void
     * @throws \Exception
     * @see \App\Controller\Component\ProjectsIndexComponent::paginateUnassociated()
     */
    public function indexUnassociated()
    {
        $this->ProjectsIndex->paginateUnassociated();
    }

    /**
     * Get the given project with completed data
     *
     * @param int $id projectId
     * @return array
     * @throws \Exception
     */
    protected function getProject(int $id): array
    {
        $project = $this->Projects->get(
            $id,
            [
                'contain' => [
                    'Themes',
                    'Containers',
                    'Containers.Stateacts',
                    'Containers.Sittings' => ['sort' => ['Sittings.id' => 'ASC']],
                    'Containers.Sittings.Typesittings',
                    'Containers.Typesacts',
                    'Containers.Annexes' => [
                        'conditions' => ['current' => true],
                        'sort' => ['rank' => 'ASC'],
                    ],
                    'Containers.Annexes.Files',
                    'Containers.Annexes.Typespiecesjointes' => [
                        'fields' => ['id', 'codetype', 'libelle'],
                    ],
                    'Containers.Maindocuments' => [
                        'conditions' => ['current' => true],
                    ],
                    'Containers.Maindocuments.Files',
                    'Containers.Maindocuments.Typespiecesjointes' => [
                        'fields' => ['id', 'codetype', 'libelle'],
                    ],
                    'Containers.Histories' => ['sort' => ['Histories.id' => 'ASC']],
                    'Containers.Histories.Users' => ['fields' => [
                        'Users.id',
                        'Users.civility',
                        'Users.firstname',
                        'Users.lastname']],
                    'ProjectTexts',
                    'ProjectTexts.DraftTemplates',
                    'ProjectTexts.DraftTemplates.DraftTemplateTypes',
                    'ProjectTexts.Files' => function ($q) {
                        return $q->formatResults(function (\Cake\Collection\CollectionInterface $files) {
                            return $files->map(function ($file) {
                                $file['base_url'] = EDITOR_WOPI_CLIENT_URL;
                                $file['path_wopi'] = EDITOR_WOPI_PATH;

                                return $file;
                            });
                        });
                    },
                    'Workflows',
                    'Generationerrors' => [
                        'fields' => [
                            'Generationerrors.type',
                            'Generationerrors.foreign_key',
                            'Generationerrors.message',
                            'Generationerrors.modified',
                        ],
                    ],
                ],
                'fields' => [
                    'Projects.id',
                    'Projects.structure_id',
                    'Projects.theme_id',
                    'Projects.template_id',
                    'Projects.id_flowable_instance',
                    'Projects.workflow_id',
                    'Projects.name',
                    'Projects.code_act',
                    'Projects.ismultichannel',
                    'Projects.created',
                    'Projects.modified',
                    'Projects.codematiere',
                    'Projects.signature_date',
                    'Projects.id_flowable_instance',
                    'Themes.id',
                    'Themes.active',
                    'Themes.name',
                    'Workflows.id',
                    'Workflows.name', // FIXME: dégager données workflows ici, faire une requete dans $facade->getInstanceDetails(
                    'Workflows.description',
                    'Workflows.file',
                    'Workflows.process_definition_key',
                ],
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $project->setVirtual(['isSignedIParapheur'], true);

        //get laststateAct
        /** @var \App\Controller\ContainersTable $Containers */
        $Containers = $this->fetchTable('Containers');
        $stateAct = $Containers->getLastStateActEntry($project->containers[0]['id']);

        $project->containers[0]['stateacts'] = $stateAct;

        if ($this->Projects->isStateEditable($project->containers[0]['id'])) { //BEN non editable que si etat
            $project['isStateEditable'] = true;
        } else {
            $project['isStateEditable'] = false;
        }

        // workflow
        $project->instance = $project->getInstanceDetails($this->Multico->getCurrentUserId());

        $project = $project->toArray();
        $this->ProjectsIndex->completeProjectAvailableActions($project);

        return $project;
    }

    /**
     * Function permettant d'afficher les informations d'un projet
     *
     * @param string $id id
     * @return void
     * @throws \Exception
     */
    public function view(string $id)
    {
        // @fixme: lorsque les services seront en place, limiter la visibilité d'un projet au service de l'user
        $project = $this->getProject((int)$id);

        $this->set('project', $project);
    }

    /**
     * Function permettant de créer un projet
     *
     * @return void
     * @access  public
     * @created 15/02/2019
     * @version V0.0.9
     */
    public function add()
    {
        $this->request->allowMethod(['post']);

        $data = $this->getRequest()->getData();
        $data['project'] = json_decode($data['project'], true);

        $project = $this->Projects->generateProject(
            $this->Multico->getCurrentStructureId(),
            $this->Multico->getCurrentUserId(),
            $data
        );

        if ($project->hasErrors() === false) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('id', $project->id);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $project->getErrors());
        }
    }

    /**
     * Edit method
     *
     * @param int $id Project id.
     * @return void
     * @access  public
     * @created 20/02/2019
     * @version V0.0.9
     */
    public function edit(string $id)
    {
        $this->request->allowMethod(['patch', 'put']);

        // @fixme: en attendant gestion fine des droits, on fait confiance au client
        //        if (!$this->getProject($id)['is_editable']) {
        //            $this->set('errors', ['id' => 'Project is not editable']);
        //            $this->setResponse($this->getResponse()->withStatus(400));
        //
        //            return;
        //        }

        // to assure correct 404
        $this->getProject((int)$id);
        $jsonProject = Hash::get($this->getRequest()->getData(), 'project');
        $rawProject = json_decode($jsonProject, true);
        $mainDocument = Hash::get($this->getRequest()->getData(), 'mainDocument');
        $annexes = (array)Hash::get($this->getRequest()->getData(), 'annexes');
        $options = $this->getRequest()->getSession()->read();

        $project = $this->Projects->modifyProject($id, $rawProject, $mainDocument, $annexes, $options);

        if ($project->hasErrors() === false) {
            $this->setResponse($this->getResponse()->withStatus(204));
            $this->set(compact('project'));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $project->getErrors());
        }
    }

    /**
     * Retourne le menu dans lequel le projet peut être visualisé par l'utilisateur,
     * en fonction de son état, de son éventuel avancement dans un circuit
     * Sur projet non visible par l'utilisateur (autre structure, aucun droit de visualisation), 404
     *
     * @param string $id projet
     * @return void
     */
    public function getMainMenu(string $id)
    {
        $this->set(
            'menu',
            $this->Projects->getMainMenu(
                (int)$id,
                $this->getRequest()->getSession()->read()
            )
        );
    }

    /**
     * Fonction permettant la suppression d'un project
     *
     * @param string $id Project id.
     * @return void
     * @throws \Exception
     */
    public function delete(string $id)
    {
        $this->request->allowMethod('delete');

        if (!$this->getProject((int)$id)['availableActions']['can_be_deleted']['value']) {
            $this->set('errors', ['id' => 'Project is not deletable']);
            $this->setResponse($this->getResponse()->withStatus(400));

            return;
        }

        $project = $this->Projects->get(
            $id,
            [
                'contain' => ['Containers'],
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        if ($this->Projects->delete($project)) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set('errors', $project->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * getPastell history
     *
     * @param int $projectId projectId
     * @return void
     */
    public function historyPastell($projectId)
    {
        $container = $this->Projects->getContainerByProject(
            $projectId,
            $this->Multico->getCurrentStructureId()
        );

        $history = $this->Projects->Containers
            ->find()
            ->select([
                'containers.id',
                'pastellfluxs.id_d',
                'pastellfluxactions.state',
                'pastellfluxactions.comments'])
            ->innerJoinWith('Pastellfluxs')
            ->innerJoinWith('Pastellfluxs.Pastellfluxactions')
            ->where(['Containers.id' => $container->get('id')])
            ->toArray();

        $this->set('history', $history);
    }

    /**
     * @param int $projectId project
     * @param int $workflowId workflow
     * @return void
     * @throws \Exception
     */
    public function sendToCircuit($projectId, $workflowId)
    {
        $notificationService = $this->loadService('Notification', [], false);

        $this->request->allowMethod('post');

        if (preg_match('/^[0-9]+$/', (string)$projectId) !== 1 || preg_match('/^[0-9]+$/', (string)$workflowId) !== 1) {
            throw new NotFoundException();
        }

        /** @var \App\Controller\WorkflowsTable $Workflows */
        $Workflows = $this->fetchTable('Workflows');
        /** @var \App\Controller\Workflow $workflow */
        $workflow = $Workflows->get(
            $workflowId,
            [
                'active' => true,
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $project = $this->Projects->get(
            $projectId,
            [
                'contain' => [
                    'Containers',
                    'Containers.Typesacts',
                ],
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        if (!isset($workflow) || !isset($project)) {
            $this->setResponse($this->getResponse()->withStatus(404));

            return;
        }

        // @fixme : tout déplacer dans ProjectTable->sentToCircuit
        /** @var \App\Controller\ContainersTable $Containers */
        $Containers = $this->fetchTable('Containers');
        $stateAct = $Containers->getLastStateActEntry($project->containers[0]['id']);
        $stateActId = $stateAct[0]['id'];

        if (!in_array($stateActId, [DRAFT, REFUSED])) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set(['errors' => ['stateAct' => 'Project StateAct is not draft or refused']]);

            return;
        }

        $containerId = $Containers->find()
            ->select(['id'])
            ->where(['project_id' => $projectId])
            ->firstOrFail()
            ->get('id');

        /** @var \App\Controller\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();

        try {
            $instance = $wrapper->createInstance($workflow->getProcessDefinitionKey());
        } catch (FlowableException $e) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $e->getMessage());

            return;
        }

        $flowInstanceId = $instance['businessKey']; // @fixme : $wrapper->createInstance pourrait ne retourner que la businessKey directement ?

        $this->Projects->getConnection()->begin();

        $Containers->addStateact($containerId, VALIDATION_PENDING, false);

        $project->workflow_id = $workflowId;
        $project->id_flowable_instance = $flowInstanceId;

        $history = $this->Projects->Containers->Histories->newEntity(
            [
                'structure_id' => $this->Multico->getCurrentStructureId(),
                'container_id' => $project->containers[0]->id,
                'user_id' => $this->Multico->getCurrentUserId(),
                'comment' => HistoriesTable::SEND_TO_CIRCUIT,
                'json' => ['workflow_id' => (int)$workflowId, 'id_flowable_instance' => (string)$flowInstanceId],
            ]
        );

        $success = $this->Projects->save($project) && $this->Projects->Containers->Histories->save($history);

        $steps = $workflow->getSteps();
        $validatorsId = array_unique(Hash::extract($steps, '{n}.validators.{n}'));

        $UsersInstances = $this->fetchTable('UsersInstances');
        foreach ($validatorsId as $validator) {
            if ($success === false) {
                break;
            }

            $jointure = $UsersInstances->newEntity([
                'user_id' => $validator,
                'id_flowable_instance' => $flowInstanceId]);

            if ($UsersInstances->save($jointure) === false) {
                $project->setError('users_instances', $jointure->getErrors());
                $success = false;
            }
        }

        if ($success !== false) {
            $this->Projects->getConnection()->commit();

            $notificationService->fireEvent(
                Stateact::VALIDATION_PENDING,
                $project,
                ['last_user_id' => $this->Multico->getCurrentUserId()]
            );

            $this->set('instance', $flowInstanceId);
            $this->setResponse($this->getResponse()->withStatus(201));
        } else {
            $this->Projects->getConnection()->rollback();
            $wrapper->deleteInstance($flowInstanceId);

            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $project->getErrors());
        }
    }

    /**
     * @param int $projectId Project id
     * @return void
     */
    public function generateActNumber($projectId)
    {
        $this->request->allowMethod(['post', 'put']);
        $success = false;
        $generatedActNumber = $this->Projects->generateActNumber((int)$projectId);
        if ($generatedActNumber !== '') {
            $success = true;
            $this->setResponse($this->getResponse()->withStatus(201));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
        }

        $this->set(compact('success', 'generatedActNumber'));
    }
}
