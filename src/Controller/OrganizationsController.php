<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\UnauthorizedException;

/**
 * Organizations Controller
 *
 * @property \App\Model\Table\OrganizationsTable $Organizations
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\PastellComponent $Pastell
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @method \App\Model\Entity\Organization[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrganizationsController extends AppController
{
    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryParams');
    }

    /**
     * Function permettant d'afficher la liste des organisations
     */
    public function index(): void
    {
        if (!$this->Multico->isSuperAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $params = $this->QueryParams->validateIndexWithPaginationTrueFalse();

        $query = $this->Organizations
            ->find()
            ->contain(['Structures' => ['sort' => ['Structures.business_name' => 'ASC']]])
            ->orderAsc('name');

        if ($params['paginate'] === 'true') {
            $this->paginate = ['maxLimit' => PAGINATION_LIMIT];
            $organizations = $this->paginate($query);
        } else {
            $organizations = $query->all();
        }

        $this->set(compact('organizations'));
    }

    /**
     * Function permettant d'afficher les informations d'une organisation
     *
     * @param  string|null $id Organization id.
     * @return \Cake\Http\Response|void
     * @access  public
     * @created 11/02/2019
     * @version V0.0.9
     */
    public function view(string $id)
    {
        if (!$this->Multico->isSuperAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $organization = $this->Organizations->get(
            $id,
            ['limitToStructure' => $this->Multico->getCurrentStructureId(), 'contain' => ['Structures']]
        );
        $this->set('organization', $organization);
    }

    /**
     * Add method
     *
     * @return void
     * @access  public
     * @created 11/02/2019
     * @version V0.0.9
     */
    public function add()
    {
        $this->request->allowMethod('post');

        if (!$this->Multico->isSuperAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only an admin can add an organization');
        }

        $organization = $this->Organizations->newEntity($this->getRequest()->getData());

        if ($this->Organizations->save($organization)) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('id', $organization->id);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $organization->getErrors());
        }
    }

    /**
     * Edit method
     *
     * @param  string $id Organization id.
     * @return void
     * @access  public
     * @created 11/02/2019
     * @version V0.0.9
     */
    public function edit(string $id)
    {
        $this->request->allowMethod(['patch', 'put']);

        if (!$this->Multico->isSuperAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only an admin can edit an organization');
        }

        $organization = $this->Organizations->patchEntity(
            $this->Organizations->get($id),
            $this->getRequest()->getData()
        );

        if ($this->Organizations->save($organization)) {
            $code = $this->getRequest()->is('patch') ? 204 : 200;
            $this->setResponse($this->getResponse()->withStatus($code));
            if ($this->getRequest()->is('put') === true) {
                $this->set(compact('organization'));
            }
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $organization->getErrors());
        }
    }

    /**
     * Fonction permettant la suppression d'une Organization
     *
     * @param  string $id Organization id.
     * @return void Redirects to index.
     * @access  public
     * @created 11/02/2019
     * @version V0.0.9
     */
    public function delete(string $id)
    {
        $this->request->allowMethod('delete');

        if (!$this->Multico->isSuperAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only an admin can delete an organization');
        }

        $organization = $this->Organizations->get($id);
        if ($this->Organizations->delete($organization)) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $organization->getErrors());
        }
    }

    /**
     * Fonction permettant d'activer ou de désactiver une Organization en mode PUT
     *
     * Si une Structure est reliée à une Organization, et que l'Organization doit être désactivé elle sera aussi
     * désactivée.
     *
     * Si l'Organization doit être activé, aucune Structure rattachée à l'Organization ne sera activée.
     *
     * @param string $id Foo
     * @return void Redirects to index.
     * @access  public
     * @created 11/02/2019
     * @version V0.0.9
     */
    public function active(string $id)
    {
        $this->request->allowMethod('put');

        if (!$this->Multico->isSuperAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $active = (bool)$this->getRequest()->getData('active');

        $organization = $this->Organizations->get(
            (int)$id,
            [
                'contain' => [
                    'Structures',
                ],
            ]
        );

        $this->Organizations->getConnection()->begin();
        $success = true;

        if (empty($organization['structures']) === false) {
            if ($active === false) {
                foreach ($organization->structures as $structure) {
                    if ($structure->active === true && $success === true) {
                        $structure->active = false;

                        if ($this->Organizations->Structures->save($structure, ['atomic' => false]) === false) {
                            $success = false;
                            $this->setResponse($this->getResponse()->withStatus(400));
                            $this->set('errors', $structure->getErrors());
                        }
                    }
                }
            }
        }

        if ($success === true) {
            $organization = $this->Organizations->get((int)$id);
            $organization['active'] = $active;

            if ($this->Organizations->save($organization, ['atomic' => false])) {
                $this->Organizations->getConnection()->commit();
                $this->setResponse($this->getResponse()->withStatus(204));
            } else {
                $this->Organizations->getConnection()->rollback();
                $this->setResponse($this->getResponse()->withStatus(400));
                $this->set('errors', $organization->getErrors());
            }
        } else {
            $this->Organizations->getConnection()->rollback();
        }
    }

    /**
     * Fonction qui récupère les entités présente sous l'orchestrateur (PASTELL)
     * Nécessite un utilisateur présent dans toutes les collectivités
     *
     * @return void
     * @access  public
     * @created 03/05/2019
     * @version V0.0.9
     */
    public function getOrchestrationOrganizations()
    {
        $this->request->allowMethod(['get']);

        if (!$this->Multico->isSuperAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $structure = $this
            ->Organizations
            ->Structures
            ->get(
                $this->Multico->getCurrentStructureId(),
                ['contain' => ['Connecteurs' => ['conditions' => ['connector_type_id' => 1]]]]
            );

        $this->loadComponent('Pastell', $structure->pastellConnector);

        $structures = $this->Pastell->getStructures();

        if (isset($structures['error']) === false && !in_array('error', $structures, true)) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set('structures', $structures);
        } else {
            $this->setResponse($this->getResponse()->withStatus(404));
        }
    }
}
