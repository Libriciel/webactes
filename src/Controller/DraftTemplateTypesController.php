<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * DraftTemplateTypes Controller
 *
 * @property \App\Model\Table\DraftTemplateTypesTable $DraftTemplateTypes
 * @method \App\Model\Entity\DraftTemplateType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DraftTemplateTypesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'sortableFields' => ['name'],
            'order' => ['name' => 'asc'],
        ];

        $draftTemplateTypes = $this->paginate($this->DraftTemplateTypes);

        $this->set(compact('draftTemplateTypes'));
    }
}
