<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 11/10/19
 * Time: 10:07
 */

namespace App\Controller;

use App\Utilities\Socle\Socle;
use Exception;

class SoclesController extends AppController
{
    /*
    Agent : https://soclewl.test.libriciel.fr/sync-myec3/agent
    Organism : https://soclewl.test.libriciel.fr/sync-myec3/organism
    Department : https://soclewl.test.libriciel.fr/sync-myec3/service
    */

    /**
     * @var $socle Socle
     */
    protected $socle;

    /**
     * initialize
     *
     * @return void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();
        $Structures = $this->fetchTable('Structures');
        $Users = $this->fetchTable('Users');
        $this->socle = new Socle($Users, $Structures);
        $this->Auth->allow();
    }

    /**
     * Perform action
     *
     * @param  callable $callback function to perform
     * @return bool
     */
    private function perform(callable $callback)
    {
        $this->disableAutoRender();
        $xml = $this->getRequest()->getBody()->getContents();
        try {
            $callback($xml);
        } catch (Exception $e) {
            $this->setResponse($this->getResponse()->withStatus($e->getCode()));
            $this->setResponse($this->getResponse()->withStringBody($e->getMessage()));

            return false;
        }

        return true;
    }

    /**
     * add structure
     *
     * @return void
     */
    public function addStructure()
    {
        $success = $this->perform(
            function ($xml) {
                $this->socle->addStructureXml($xml);
            }
        );

        if ($success) {
            $this->setResponse($this->getResponse()->withStatus(201));
        }
    }

    /**
     * UpdateStructure
     *
     * @return void
     */
    public function updateStructure()
    {
        $success = $this->perform(
            function ($xml) {
                $this->socle->updateStructureXml($xml);
            }
        );
        if ($success) {
            $this->setResponse($this->getResponse()->withStatus(200));
        }
    }

    /**
     * delete structure
     *
     * @param  string $externalId externalId
     * @return void
     */
    public function deleteStructure($externalId)
    {
        $this->disableAutoRender();

        try {
            $this->socle->deleteStructure($externalId);
        } catch (Exception $e) {
            $this->setResponse($this->getResponse()->withStatus($e->getCode()));
            $this->setResponse($this->getResponse()->withStringBody($e->getMessage()));

            return;
        }
        $this->setResponse($this->getResponse()->withStatus(204));
    }

    /**
     * Add user
     *
     * @return void
     */
    public function addUser()
    {
        $success = $this->perform(
            function ($xml) {
                $this->socle->addUserXml($xml);
            }
        );
        if ($success) {
            $this->setResponse($this->getResponse()->withStatus(201));
        }
    }

    /**
     * update user
     *
     * @return void
     */
    public function updateUser()
    {
        $success = $this->perform(
            function ($xml) {
                $this->socle->updateUserXml($xml);
            }
        );
        if ($success) {
            $this->setResponse($this->getResponse()->withStatus(200));
        }
    }

    /**
     * delete user
     *
     * @param  string $externalId externalId
     * @return void
     */
    public function deleteUser($externalId)
    {
        $this->disableAutoRender();

        try {
            $this->socle->deleteUser($externalId);
        } catch (Exception $e) {
            $this->setResponse($this->getResponse()->withStatus($e->getCode()));
            $this->setResponse($this->getResponse()->withStringBody($e->getMessage()));

            return;
        }
        $this->setResponse($this->getResponse()->withStatus(204));
    }
}
