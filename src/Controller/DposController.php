<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Dpos Controller
 *
 * @property \App\Model\Table\DposTable $Dpos
 * @property \App\Controller\Component\MulticoComponent $Multico
 */
class DposController extends AppController
{
    /**
     * Function permettant d'afficher le DPO de la structure
     *
     * @return \App\Controller\Response|void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function index()
    {
        $this->paginate = [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
        ];
        $dpos = $this->paginate($this->Dpos)->sortBy('name', SORT_ASC)->toArray(false);
        $this->set(compact('dpos'));
    }

    /**
     * Function permettant d'afficher le DPO de la structure
     *
     * @param  int $id Dpo id.
     * @return \App\Controller\Response|void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function view($id)
    {
        $dpo = $this->Dpos->get(
            $id,
            [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );
        $this->set(compact('dpo'));
    }

    /**
     * Function permettant de créer le DPO d'une structure
     *
     * @return \Cake\Http\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod('post');
        $dpo = $this->Dpos->newEntity($this->request->getData());

        if ($this->Dpos->save($dpo, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('id', $dpo->id);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $dpo->getErrors());
        }
    }

    /**
     * Function permettant de modifier uu DPO
     *
     * @param  string|null $id Dpo id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function edit($id = null)
    {
        $this->request->allowMethod('patch');

        $structureId = $this->Multico->getCurrentStructureId();
        $dpo = $this->Dpos->patchEntity(
            $this->Dpos->get($id, ['limitToStructure' => $structureId]),
            $this->request->getData()
        );

        if ($this->Dpos->save($dpo, ['limitToStructure' => $structureId])) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set(compact('dpo'));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $dpo->getErrors());
        }
    }
}
