<?php
declare(strict_types=1);

namespace App\Controller;

use App\Validation\Validator;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Actors Controller
 *
 * @property \App\Model\Table\ActorGroupsTable $ActorGroups
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\ActorGroups[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActorGroupsController extends AppController
{
    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryParams');
        $this->loadComponent('QueryUtils');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $params = $this->QueryParams->validateIndexWithPaginationTrueFalse(
            (new Validator())
                ->allowPresence('active')
                ->allowEmptyString('active')
                ->inList('active', ['true', 'false'])
                ->allowPresence('name')
                ->allowEmptyString('name'),
            ['active' => null, 'name' => null],
        );

        $query = $this->ActorGroups
            ->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()])
            ->where([
                isset($params['name'])
                    ? $this->QueryUtils->getIlikeQueryExpression('ActorGroups.name', "%{$params['name']}%")
                    : '',
                isset($params['active']) && ($params['active'] === 'false' || $params['active'] === 'true')
                    ? ['ActorGroups.active' => $params['active'] === 'true' ? true : false]
                    : [],
            ])
            ->orderAsc('ActorGroups.name');

        if ($params['paginate'] === 'true') {
            $this->paginate = [
                'limit' => isset($params['limit']) && ($params['limit'] >= PAGINATION_LIMIT)
                    ? $params['limit']
                    : PAGINATION_LIMIT,
                'maxLimit' => MAXIMUM_LIMIT,
            ];
            $actorGroups = $this->paginate($query);
        } else {
            $actorGroups = $query->all();
        }

        $this->set(compact('actorGroups'));
    }

    /**
     * View method
     *
     * @param string|null $id Actorgroup id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $actorGroups = $this->ActorGroups->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $this->set('actorGroup', $actorGroups);
    }

    /**
     * Add method
     *
     * @return void
     */
    public function add()
    {
        $this->request->allowMethod('post');
        $data = json_decode((string)$this->request->getBody(), true);
        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();
        $data['organization_id'] = $this->Multico->getCurrentOrganizationId();
        $data['limitToStructure'] = $this->Multico->getCurrentStructureId();

        $actorGroups = $this->ActorGroups->newEntity($data);

        if (!$this->ActorGroups->save($actorGroups, $data)) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $actorGroups->getErrors());

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(201));
        $this->set('id', $actorGroups->id);
    }

    /**
     * Edit method
     *
     * @param string|null $id Actorgroup id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }
        $this->request->allowMethod(['patch', 'put']);
        $structureId = $this->Multico->getCurrentStructureId();
        $actorGroups = $this->ActorGroups->patchEntity(
            $this->ActorGroups->get($id, ['limitToStructure' => $structureId]),
            json_decode((string)$this->request->getBody(), true)
        );

        if (!$this->ActorGroups->save($actorGroups, ['limitToStructure' => $structureId])) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $actorGroups->getErrors());

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(200));
        $this->set('actorGroup', $actorGroups);
    }

    /**
     * [activate description]
     *
     * @param int $id [description]
     * @return void
     */
    public function activate(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can activate a actors group');
        }

        $actorGroups = $this->ActorGroups->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $actorGroupss = $this->ActorGroups->patchEntity($actorGroups, ['active' => true]);
        if ($this->ActorGroups->save($actorGroupss)) {
            $this->setResponse($this->getResponse()->withStatus(201));

            $this->set('actorgroup', $actorGroupss);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $actorGroupss->getErrors());
        }
    }

    /**
     * [deactivate description]
     *
     * @param int $id [description]
     * @return void
     */
    public function deactivate(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can deactivate a actors group');
        }

        $actorGroups = $this->ActorGroups->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $actorGroups = $this->ActorGroups->patchEntity($actorGroups, ['active' => false]);
        if ($this->ActorGroups->save($actorGroups)) {
            $this->setResponse($this->getResponse()->withStatus(201));

            $this->set('actorgroup', $actorGroups);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $actorGroups->getErrors());
        }
    }

    /**
     * Fonction permettant la suppression d'un groupe d'acteur
     *
     * @param int $id Typesitting id.
     * @return void Redirects to index.
     * @access  public
     * @created 12/02/2019
     * @version v2.0
     */
    public function delete($id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $this->request->allowMethod(['delete']);
        $actorGroups = $this->ActorGroups->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId()]
        );

        if (!$this->ActorGroups->delete($actorGroups)) {
            $this->setResponse($this->getResponse()->withStatus(400));
        }

        $this->setResponse($this->getResponse()->withStatus(204));
    }
}
