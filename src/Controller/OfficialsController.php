<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Officials Controller
 *
 * @property \App\Model\Table\OfficialsTable $Officials
 * @property \App\Controller\Component\MulticoComponent $Multico
 */
class OfficialsController extends AppController
{
    /**
     * Function permettant d'afficher le responsable de la structure
     *
     * @return \App\Controller\Response|void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function index()
    {
        $this->paginate = [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
        ];
        $officials = $this->paginate($this->Officials)->sortBy('lastname', SORT_ASC)->toArray(false);

        $this->set(compact('officials'));
    }

    /**
     * Function permettant d'afficher le responsable de la structure
     *
     * @param  int $id Official id.
     * @return \App\Controller\Response|void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function view($id)
    {
        $official = $this->Officials->get(
            $id,
            [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );
        $this->set(compact('official'));
    }

    /**
     * Function permettant de créer le responsable d'une structure
     *
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function add()
    {
        $this->request->allowMethod('post');
        $official = $this->Officials->newEntity($this->request->getData());

        if ($this->Officials->save($official, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('id', $official->id);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $official->getErrors());
        }
    }

    /**
     * Function permettant de modifier un responsable d'une structure
     *
     * @param  string|null $id Official id.
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function edit($id = null)
    {
        $this->request->allowMethod('patch');

        $structureId = $this->Multico->getCurrentStructureId();
        $official = $this->Officials->patchEntity(
            $this->Officials->get($id, ['limitToStructure' => $structureId]),
            $this->request->getData()
        );

        if ($this->Officials->save($official, ['limitToStructure' => $structureId])) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set(compact('official'));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $official->getErrors());
        }
    }
}
