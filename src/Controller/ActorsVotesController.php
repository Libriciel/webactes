<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * ActorsVotes Controller
 *
 * @method \App\Model\Entity\ActorsVote[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActorsVotesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $actorsVotes = $this->paginate($this->ActorsVotes);

        $this->set(compact('actorsVotes'));
    }

    /**
     * View method
     *
     * @param string|null $id Actors Projects Vote id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $actorsProjectsVote = $this->ActorsVotes->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('actorsProjectsVote'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $actorsProjectsVote = $this->ActorsVotes->newEmptyEntity();
        if ($this->request->is('post')) {
            $actorsProjectsVote = $this->ActorsVotes->patchEntity($actorsProjectsVote, $this->request->getData());
            if ($this->ActorsVotes->save($actorsProjectsVote)) {
                $this->Flash->success(__('The actors projects vote has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The actors projects vote could not be saved. Please, try again.'));
        }
        $this->set(compact('actorsProjectsVote'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Actors Projects Vote id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $actorsProjectsVote = $this->ActorsVotes->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $actorsProjectsVote = $this->ActorsVotes->patchEntity($actorsProjectsVote, $this->request->getData());
            if ($this->ActorsVotes->save($actorsProjectsVote)) {
                $this->Flash->success(__('The actors projects vote has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The actors projects vote could not be saved. Please, try again.'));
        }
        $this->set(compact('actorsProjectsVote'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Actors Projects Vote id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $actorsProjectsVote = $this->ActorsVotes->get($id);
        if ($this->ActorsVotes->delete($actorsProjectsVote)) {
            $this->Flash->success(__('The actors projects vote has been deleted.'));
        } else {
            $this->Flash->error(__('The actors projects vote could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
