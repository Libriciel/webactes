<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\UnauthorizedException;

/**
 * ActorgroupsSittings Controller
 *
 * @property \App\Model\Table\ActorgroupsSittingsTable $ActorgroupsSittings
 * @method \App\Model\Entity\ActorgroupsSitting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActorGroupsSittingsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ActorGroups', 'Typesittings', 'Structures'],
        ];
        $actorgroupsSittings = $this->paginate($this->ActorgroupsSittings);

        $this->set(compact('actorgroupsSittings'));
    }

    /**
     * View method
     *
     * @param string|null $id Actorsgroups Sitting id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $actorsgroupsSitting = $this->ActorgroupsSittings->get($id, [
            'contain' => ['ActorGroups', 'Typesittings', 'Structures'],
        ]);

        $this->set(compact('actorsgroupsSitting'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }
        $actorsgroupsSitting = $this->ActorgroupsSittings->newEmptyEntity();
        if ($this->request->is('post')) {
            $actorsgroupsSitting = $this->ActorgroupsSittings->patchEntity(
                $actorsgroupsSitting,
                $this->request->getData()
            );
            if ($this->ActorgroupsSittings->save($actorsgroupsSitting)) {
                $this->Flash->success(__('The actorsgroups sitting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The actorsgroups sitting could not be saved. Please, try again.'));
        }
        $actorGroups = $this->ActorgroupsSittings->ActorGroups->find('list', ['limit' => 200]);
        $typesittings = $this->ActorgroupsSittings->Typesittings->find('list', ['limit' => 200]);
        $structures = $this->ActorgroupsSittings->Structures->find('list', ['limit' => 200]);
        $this->set(compact('actorsgroupsSitting', 'actorGroups', 'typesittings', 'structures'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Actorsgroups Sitting id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }
        $actorsgroupsSitting = $this->ActorgroupsSittings->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $actorsgroupsSitting = $this->ActorgroupsSittings->patchEntity(
                $actorsgroupsSitting,
                $this->request->getData()
            );
            if ($this->ActorgroupsSittings->save($actorsgroupsSitting)) {
                $this->Flash->success(__('The actorsgroups sitting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The actorsgroups sitting could not be saved. Please, try again.'));
        }
        $actorGroups = $this->ActorgroupsSittings->ActorGroups->find('list', ['limit' => 200]);
        $typesittings = $this->ActorgroupsSittings->Typesittings->find('list', ['limit' => 200]);
        $structures = $this->ActorgroupsSittings->Structures->find('list', ['limit' => 200]);
        $this->set(compact('actorsgroupsSitting', 'actorGroups', 'typesittings', 'structures'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Actorsgroups Sitting id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }
        $this->request->allowMethod(['post', 'delete']);
        $actorsgroupsSitting = $this->ActorgroupsSittings->get($id);
        if ($this->ActorgroupsSittings->delete($actorsgroupsSitting)) {
            $this->Flash->success(__('The actorsgroups sitting has been deleted.'));
        } else {
            $this->Flash->error(__('The actorsgroups sitting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
