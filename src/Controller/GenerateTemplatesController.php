<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Utility\Hash;

/**
 * GenerateTemplates Controller
 *
 * @property \App\Model\Table\GenerateTemplatesTable $GenerateTemplates
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\GenerateTemplate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GenerateTemplatesController extends AppController
{
    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->Auth->allowedActions = [
            'indexWithTypeCode',
        ];
        $this->loadComponent('QueryUtils');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $queryParams = $this->getRequest()->getQueryParams();
        $this->paginate = [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            'maxLimit' => isset($queryParams['limit'])
            && ($queryParams['limit'] <= PAGINATION_LIMIT) ? $queryParams['limit'] : PAGINATION_LIMIT,
            'contain' => [
                'GenerateTemplateTypes',
                'Files' => function ($q) {
                    return $q->formatResults(function (\Cake\Collection\CollectionInterface $files) {
                        return $files->map(function ($file) {
                            $file['base_url'] = EDITOR_WOPI_CLIENT_URL;
                            $file['path_wopi'] = EDITOR_WOPI_PATH;

                            return $file;
                        });
                    });
                },
            ],
            'conditions' => [
                'OR' => [
                    isset($queryParams['name'])
                        ? $this->QueryUtils->getIlikeQueryExpression(
                            'GenerateTemplates.name',
                            "%{$queryParams['name']}%"
                        )
                        : '',
                    isset($queryParams['name'])
                        ? $this->QueryUtils->getIlikeQueryExpression(
                            'GenerateTemplates.name',
                            "%{$queryParams['name']}%"
                        )
                        : '',
                ],
                'AND' => ['GenerateTemplates.id >=' => '1'],
            ],
            'sortableFields' => ['name'],
            'order' => ['name' => 'asc'],
        ];

        $generateTemplates = $this->paginate($this->GenerateTemplates);

        $generateTemplates = Hash::map($generateTemplates->toArray(), '{n}', function ($array) {
            $array['generate_template_file'] = $array['files'][0] ?? [];
            unset($array['files']);

            return $array;
        });

        $this->set(compact('generateTemplates'));
    }

    /**
     * View method
     *
     * @param string|null $id Generate Template id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $generateTemplate = $this->GenerateTemplates->get($id, [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            'contain' => ['GenerateTemplateTypes', 'Files'],
        ]);

        $this->set(compact('generateTemplate'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod('post');
        $data = json_decode((string)$this->request->getBody(), true);

        $data = Hash::get($data, 'generateTemplate');

        if (isset($data['generate_template_type'])) {
            $data['generate_template_type_id'] = $data['generate_template_type']['id'];
        }

        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();
        $data['organization_id'] = $this->Multico->getCurrentOrganizationId();

        $generateTemplate = $this->GenerateTemplates->newEntity($data, ['associated' => []]);

        if (
            $this->GenerateTemplates->save(
                $generateTemplate,
                ['limitToStructure' => $this->Multico->getCurrentStructureId()]
            )
        ) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('generateTemplate', $generateTemplate);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $generateTemplate->getErrors());
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Generate Template id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['patch', 'put']);
        $data = json_decode((string)$this->request->getBody(), true);

        $data = Hash::get($data, 'generateTemplate');

        if (isset($data['generate_template_type'])) {
            $data['generate_template_type_id'] = $data['generate_template_type']['id'];
        }

        $generateTemplate = $this->GenerateTemplates->get(
            $id,
            ['limitToStructure' => $this->Multico->getCurrentStructureId()]
        );
        $generateTemplate = $this->GenerateTemplates->patchEntity($generateTemplate, $data, ['associated' => []]);

        if (
            $this->GenerateTemplates->save(
                $generateTemplate,
                ['limitToStructure' => $this->Multico->getCurrentStructureId()]
            )
        ) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set('generateTemplate', $generateTemplate);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $generateTemplate->getErrors());
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Generate Template id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);
        $generateTemplate = $this->GenerateTemplates->get(
            $id,
            ['limitToStructure' => $this->Multico->getCurrentStructureId()]
        );

        if (!$this->GenerateTemplates->delete($generateTemplate)) {
            $this->setResponse($this->getResponse()->withStatus(400));
        }

        $this->setResponse($this->getResponse()->withStatus(204));
    }

    /**
     * @param string $code Foo
     * @return \Cake\Http\Response|null|void Redirects to index.
     */
    public function indexWithTypeCode(string $code)
    {
        $generateTemplates = $this->GenerateTemplates->find('all', [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
        ])
            ->innerJoinWith('GenerateTemplateTypes', function ($q) use ($code) {
                return $q->where(['GenerateTemplateTypes.code' => $code]);
            })
            ->orderAsc('GenerateTemplates.name');

        $this->set(compact('generateTemplates'));
    }
}
