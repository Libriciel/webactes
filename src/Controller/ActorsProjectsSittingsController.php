<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\BadRequestException;

/**
 * ActorsProjectsSittings Controller
 *
 * @property \App\Model\Table\ActorsProjectsSittingsTable $ActorsProjectsSittings
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Model\Entity\ActorsProjectsSitting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActorsProjectsSittingsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Actors', 'Projects', 'Sittings'],
        ];
        $actorsProjectsSittings = $this->paginate($this->ActorsProjectsSittings);

        $this->set(compact('actorsProjectsSittings'));
    }

    /**
     * View method
     *
     * @param string|null $id Actors List Projects Sitting id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $actorsProjectsSitting = $this->ActorsProjectsSittings->get($id, [
            'contain' => ['Actors', 'Projects', 'Sittings'],
        ]);

        $this->set(compact('actorsProjectsSitting'));
    }

    /**
     * @param string $sittingId sittingId
     * @param string $projectId projectId
     * @return void
     */
    public function edit(string $sittingId, string $projectId)
    {
        $this->request->allowMethod('put');

        $missingActorsIds = [];
        foreach ($this->getRequest()->getData() as $row) {
            if (!$row['is_present']) {
                $missingActorsIds[] = $row['actor_id'];
            }
        }

        try {
            // @info: on nettoie les données POST sinon ça crée de nouveaux actors même si l'id est fourni
            $data = [];
            foreach ($this->request->getData() as $requestData) {
                if (
                    !empty($requestData['mandataire_id'])
                    && in_array($requestData['mandataire_id'], $missingActorsIds)
                ) {
                    throw new BadRequestException('Un acteur déclaré absent ne peut être mandataire d\'un autre.');
                }

                $row = ['project_id' => $projectId, 'sitting_id' => $sittingId];
                foreach (['id', 'actor_id', 'is_present', 'mandataire_id'] as $field) {
                    if ($field === 'mandataire_id') {
                        $row[$field] = $requestData[$field] ?? null;
                    } else {
                        $row[$field] = $requestData[$field];
                    }
                }
                $data[] = $row;
            }

            $oldAttendanceList = $this->ActorsProjectsSittings
                ->find()
                ->where(['sitting_id' => $sittingId, 'project_id' => $projectId])
                ->all()
                ->toList();

            $patchedActorsProjectsSittings = $this->ActorsProjectsSittings->patchEntities(
                $oldAttendanceList,
                $data
            );
            $this->ActorsProjectsSittings->saveMany($patchedActorsProjectsSittings);
        } catch (\Exception $exception) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', true);
            $this->set('message', $exception->getMessage());
        }

        $actorsProjectsSittings = $this->ActorsProjectsSittings
            ->find()
            ->where(['project_id' => $projectId, 'sitting_id' => $sittingId]);

        $this->set(compact('actorsProjectsSittings'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Actors List Projects Sitting id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);
        $actorsListProjectsSitting = $this->ActorsProjectsSittings->get($id);

        if ($this->ActorsProjectsSittings->delete($actorsListProjectsSitting)) {
            $this->setResponse($this->getResponse()->withStatus(400));
        }
        $this->setResponse($this->getResponse()->withStatus(204));
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function duplicateListFromPrecedentRank()
    {
        $this->request->allowMethod('get');

        $projectId = $this->getRequest()->getParam('projectId');
        $sittingId = $this->getRequest()->getParam('sittingId');

        if ($this->ActorsProjectsSittings->updateFromPrecedentRank($projectId, $sittingId)) {
            $actorsProjectsSittings = $this->paginate(
                $this->ActorsProjectsSittings
                    ->find()
                    ->contain(['Actors'])
                    ->where(['project_id' => $projectId, 'sitting_id' => $sittingId])
            );
            $this->set(compact('actorsProjectsSittings'));
            $this->setResponse($this->getResponse()->withStatus(200));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }
}
