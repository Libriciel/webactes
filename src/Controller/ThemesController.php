<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Theme;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Themes Controller
 *
 * @property \App\Model\Table\ThemesTable $Themes
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\Theme[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ThemesController extends AppController
{
    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryUtils');
    }

    /**
     * Methode index
     *
     * @return \Cake\Http\Response|void
     * @access  public
     * @created 25/11/2020
     * @version V2
     */
    public function index()
    {
        $queryParams = $this->getRequest()->getQueryParams();

        $this->paginate = [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            'maxLimit' => $queryParams['limit'] ?? PAGINATION_LIMIT,
            'order' => ['Themes.position' => 'asc'],
            'conditions' => [
                'OR' => [
                    isset($queryParams['name'])
                        ? $this->QueryUtils->getIlikeQueryExpression('Themes.name', "%{$queryParams['name']}%")
                        : '',
                ],
            ],
        ];
        $conditions = ['structure_id' => $this->Multico->getCurrentStructureId()];

        isset($queryParams['active'])
            ? $conditions['active'] = boolval($queryParams['active'])
            : null;

        $themes = $this->Themes->generateTreeListByOrder($conditions);
        $this->set('themes', $this->paginate($themes));
    }

    /**
     * Methode view
     *
     * @param string|null $id Theme id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view(?string $id = null)
    {
        $theme = $this->Themes->get($id, ['limitToStructure' => $this->Multico->getCurrentStructureId()]);
        $this->set('theme', $theme);
    }

    /**
     * Theme et ses enfants
     *
     * @return void
     */
    public function viewWithChildren()
    {
        $theme_id = $this->getRequest()->getParam('theme_id');
        $theme = $this
            ->Themes
            ->getWithChildren(
                $theme_id,
                ['limitToStructure' => $this->Multico->getCurrentStructureId()]
            );
        $this->set(compact('theme'));
    }

    /**
     * Méthode add
     *
     * @return void
     * @access  public
     * @created 25/11/2020
     * @throws \Cake\Http\Exception\UnauthorizedException|\App\Controller\Exception
     * @version V2
     */
    public function add()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can add a theme');
        }

        $this->request->allowMethod('post');

        $data = $this->getRequest()->getData();
        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();
        $data['active'] = false;

        $theme = $this->Themes->newEntity($data);

        if ($this->Themes->save($theme, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('Themes', $theme);
        } else {
            $this->set('errors', $theme->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Méthode edit
     *
     * @param string|null $id Theme id.
     * @return void
     * @access  public
     * @created 25/11/2020
     * @version V2
     */
    public function edit(string $id)
    {
        $this->request->allowMethod(['patch', 'put']);

        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can edit a theme');
        }

        $theme = $this->Themes->get(
            $id,
            [
                ['limitToStructure' => $this->Multico->getCurrentStructureId()],
            ]
        );
        $data = $this->getRequest()->getData();

        $theme = $this->Themes->patchEntity($theme, $data);

        if ($this->getRequest()->is('patch')) {
            $this->editPatch($theme);

            return;
        }

        if ($this->getRequest()->is('put')) {
            $this->editPut($theme);

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(400));
    }

    /**
     * Fonction permettant l'enregistrement en mode PATCH
     *
     * @param \App\Model\Entity\Theme $theme theme
     * @return void
     * @access  private
     * @created 25/11/2020
     * @version V2
     */
    private function editPatch(Theme $theme)
    {
        if ($this->Themes->save($theme, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(204));
            $this->set('theme', $theme);
        } else {
            $this->set(['errors' => $theme->getErrors()]);
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Fonction permettant l'enregistrement en mode PUT
     *
     * @param \App\Model\Entity\Theme $theme theme
     * @return void
     * @access  private
     * @created 25/11/2020
     * @version V2
     */
    private function editPut(Theme $theme)
    {
        if ($this->Themes->save($theme, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set('theme', $theme);
        } else {
            $this->set(['errors' => $theme->getErrors()]);
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Supprime un thème si possible : suppression logique uniquement afin de conserver les données pour l'historique
     *
     * @param string $id thème
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     * @access  public
     * @created 25/11/2020
     * @version V2
     */
    public function delete(string $id)
    {
        $this->request->allowMethod('delete');

        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can delete a theme');
        }
        $theme = $this->Themes->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        if (!$theme->isDeletable()) {
            throw new UnauthorizedException('Ce thème est déjà en cours d\'utilisation');
        }

        $theme = $this->Themes->patchEntity($theme, ['active' => false, 'deleted' => time()]);

        if ($this->Themes->save($theme)) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set('errors', $theme->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Active un thème
     *
     * @param int $id theme
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     * @access  public
     * @created 25/11/2020
     * @version V2
     */
    public function activate(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can activate a theme');
        }

        $theme = $this->Themes->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $theme = $this->Themes->patchEntity($theme, ['active' => true]);

        if ($this->Themes->save($theme)) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('theme', $theme);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $theme->getErrors());
            $this->set('success', false);
        }
    }

    /**
     * Désactive un thème
     *
     * @param int $id theme
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     * @access  public
     * @created 25/11/2020
     * @version V2
     */
    public function deactivate(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can deactivate a theme');
        }

        $theme = $this->Themes->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $theme = $this->Themes->patchEntity($theme, ['active' => false]);

        if ($this->Themes->save($theme)) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('theme', $theme);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $theme->getErrors());
            $this->set('success', false);
        }
    }

    /**
     * Retrouve tous les thèmes actifs
     *
     * @return \Cake\Http\Response|void
     * @access  public
     * @created 25/11/2020
     * @version V2
     */
    public function active()
    {
        $query = $this->Themes->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()]);

        $themes = $query->order($query->func()->coalesce(['id' => 'identifier', 'parent_id' => 'identifier']))
            ->order($query->expr()->isNotNull('parent_id'))
            ->where(['active' => true]);

        $this->set('themes', $this->paginate($themes));
    }
}
