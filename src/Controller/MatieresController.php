<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Matieres Controller
 *
 * @property \App\Model\Table\MatieresTable $Matieres
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Model\Entity\Matiere[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MatieresController extends AppController
{
    /**
     * Fonction permettant de recupere les matieres de la classification en fonction de la structure
     *
     * @return void
     * @access  public
     * @created 03/06/2019
     * @version V0.0.9
     */
    public function getMatieresToStructure()
    {
        $this->request->allowMethod('get');

        $matieres = $this->Matieres
            ->find('threaded')
            ->where(['structure_id' => $this->Multico->getCurrentStructureId()])
            ->order(['id']);

        $this->set(compact('matieres'));
    }
}
