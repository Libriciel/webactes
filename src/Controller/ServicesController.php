<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 * @property \App\Controller\Component\MulticoComponent $Multico
 */
class ServicesController extends AppController
{
    /**
     * Function permettant d'afficher la liste des services et sous-services
     *
     * @return \App\Controller\Response|void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function index()
    {
        $services = $this
            ->Services
            ->generateTreeListByOrder(
                ['structure_id' => $this->Multico->getCurrentStructureId()]
            );
        $this->set(compact('services'));
    }

    /**
     * Function permettant d'afficher les informations d'un service
     *
     * @param  int $id Service id.
     * @return \App\Controller\Response|void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function view($id)
    {
        $service = $this->Services->get(
            $id,
            [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );
        $this->set(compact('service'));
    }

    /**
     * Function permettant d'afficher les informations d'un service avec ses enfants
     *
     * @return \App\Controller\Response|void
     * @access  public
     * @created 14/02/2019
     * @version V0.0.9
     */
    public function viewWithChildren()
    {
        $service_id = $this->getRequest()->getParam('service_id');
        $service = $this->Services->getWithChildren($service_id);
        $this->set(compact('service'));
    }

    /**
     * Function permettant de créer un service dans une structure
     *
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function add()
    {
        $this->request->allowMethod('post');

        $service = $this->Services->newEntity($this->request->getData());

        if ($this->Services->save($service, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('id', $service->id);
        } else {
            $this->set('errors', $service->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Function permettant de modifier un service
     *
     * @param  string|null $id Service id.
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['patch', 'put']);

        $structureId = $this->Multico->getCurrentStructureId();
        $service = $this->Services->patchEntity(
            $this->Services->get($id, ['limitToStructure' => $structureId]),
            $this->request->getData()
        );

        if ($this->Services->save($service, ['limitToStructure' => $structureId])) {
            if ($this->request->is('patch')) {
                $this->setResponse($this->getResponse()->withStatus(204));
            } else {
                $this->setResponse($this->getResponse()->withStatus(200));
                $this->set('service', $service);
            }
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $service->getErrors());
        }
    }

    /**
     * Fonction permettant la suppression d'un service
     *
     * @param  int $id Service id.
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function delete($id)
    {
        $this->request->allowMethod('delete');

        $service = $this->Services->get($id, ['limitToStructure' => $this->Multico->getCurrentStructureId()]);

        if ($this->Services->delete($service)) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $service->getErrors());
        }
    }
}
