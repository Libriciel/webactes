<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Project;
use App\Validation\Validator;
use Cake\ORM\Query;

/**
 * ContainersSittings Controller
 *
 * @property \App\Model\Table\ContainersSittingsTable $ContainersSittings
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @method \App\Model\Entity\ContainersSitting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContainersSittingsController extends AppController
{
    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryParams');
    }

    /**
     * Index method
     *
     * Paramètre GET (obligatoire):
     * - sitting_id: l'id de la séance
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $params = $this->QueryParams->validate(
            (new Validator())
                ->requirePresence('sitting_id')
                ->integer('sitting_id'),
            ['sitting_id' => null]
        );

        $isDeliberating = $this->ContainersSittings->Sittings->Typesittings
            ->find()
            ->select(['isdeliberating'])
            ->matching('Sittings')
            ->where(['Sittings.id' => $params['sitting_id']])
            ->enableHydration(false)
            ->firstOrFail()['isdeliberating'];

        $containersSittings = $this->ContainersSittings
            ->find()
            ->contain([
                'Containers.Projects.Generationerrors' => [
                    'fields' => [
                        'Generationerrors.type',
                        'Generationerrors.foreign_key',
                        'Generationerrors.message',
                        'Generationerrors.modified',
                    ],
                ],
                'Containers.Projects.Themes',
                'Containers.Typesacts',
                'Containers.Stateacts' => function (Query $query) {
                    return $query
                        ->where([
                            'ContainersStateacts.id' => $this->ContainersSittings->Containers->ContainersStateacts
                                ->getLastByContainersIdSubquery('ContainersStateacts.container_id'),
                        ]);
                },
            ])
            ->where([
                'Containers.structure_id' => $this->Multico->getCurrentStructureId(),
                'sitting_id' => $params['sitting_id'],
            ])
            ->orderAsc('rank')
            ->enableHydration(false)
            ->toArray();

        foreach ($containersSittings as $idxContainerSitting => $containerSitting) {
            $project = new Project($containerSitting['container']['project']);
            $containersSittings[$idxContainerSitting]['container']['project']['availableActions'] = [
                // @see ProjectsController::addIsEditable pour la vraie valeur, mais ce n'est pas utilisé depuis cette entrée d'API
                'can_be_edited' => ['value' => false, 'reason' => ''],
                'can_be_voted' => $isDeliberating
                    ? $project->canBeVoted()
                    : ['value' => false, 'reason' => '']
                ,
            ];

            foreach ($this->ContainersSittings->calculatesNewRank((int)$params['sitting_id']) as $container) {
                if ($containerSitting['id'] === $container['id']) {
                    $containersSittings[$idxContainerSitting]['rank'] = $container['rank'];
                }
            }
        }

        $this->set(compact('containersSittings'));
    }

    /**
     * View method
     *
     * @param string|null $id Containers Sitting id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $containersSitting = $this->ContainersSittings->get($id, [
            'contain' => ['Containers', 'Sittings'],
        ]);

        $this->set(compact('containersSitting'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $containersSitting = $this->ContainersSittings->newEmptyEntity();
        if ($this->request->is('post')) {
            $containersSitting = $this->ContainersSittings->patchEntity($containersSitting, $this->request->getData());
            if ($this->ContainersSittings->save($containersSitting)) {
                $this->Flash->success(__('The containers sitting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The containers sitting could not be saved. Please, try again.'));
        }
        $containers = $this->ContainersSittings->Containers->find('list', ['limit' => 200]);
        $sittings = $this->ContainersSittings->Sittings->find('list', ['limit' => 200]);
        $this->set(compact('containersSitting', 'containers', 'sittings'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Containers Sitting id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $containersSitting = $this->ContainersSittings->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $containersSitting = $this->ContainersSittings->patchEntity($containersSitting, $this->request->getData());
            if ($this->ContainersSittings->save($containersSitting)) {
                $this->Flash->success(__('The containers sitting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The containers sitting could not be saved. Please, try again.'));
        }
        $containers = $this->ContainersSittings->Containers->find('list', ['limit' => 200]);
        $sittings = $this->ContainersSittings->Sittings->find('list', ['limit' => 200]);
        $this->set(compact('containersSitting', 'containers', 'sittings'));
    }

    /**
     * Edit many method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     */
    public function editMany()
    {
        $this->request->allowMethod('put');
        $data = json_decode((string)$this->request->getBody(), true);

        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();
        $data['limitToStructure'] = $this->Multico->getCurrentStructureId();

        $schedule = $this->ContainersSittings->newEntities($data['schedule']);
        $schedule = $this->ContainersSittings->patchEntities($schedule, $data['schedule']);

        try {
            $this->ContainersSittings->saveMany($schedule);

            $this->setResponse($this->getResponse()->withStatus(200));

            $this->set(compact('schedule'));
            $this->set('success', true);
        } catch (\Exception $e) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', true);
            $this->set('message', $e->getMessage());
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Containers Sitting id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $containersSitting = $this->ContainersSittings->get($id);
        if ($this->ContainersSittings->delete($containersSitting)) {
            $this->Flash->success(__('The containers sitting has been deleted.'));
        } else {
            $this->Flash->error(__('The containers sitting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
