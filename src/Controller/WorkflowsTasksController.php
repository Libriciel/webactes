<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Project;
use App\Model\Entity\Stateact;
use App\Service\NotificationService;
use Cake\Http\Exception\UnauthorizedException;
use Cake\ORM\Query;
use Cake\Utility\Hash;
use WrapperFlowable\Api\FlowableException;
use WrapperFlowable\Api\FlowableFacade;
use WrapperFlowable\Api\WrapperFactory;

/**
 * Class WorkflowsTasksController
 *
 * @package  App\Controller
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\NotificationService $Notification
 * @property \App\Model\Entity\Project $Projects
 */
class WorkflowsTasksController extends AppController
{
    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function allTasksByCandidateUser()
    {
        $userId = $this->Multico->getCurrentUserId();

        /** @var \App\Controller\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();
        try {
            $tasks = $wrapper->getTasksByCandidateUser($userId);
        } catch (FlowableException $e) {
            $this->setResponse($this->getResponse()->withStatus($e->getCode()));

            return;
        }
        $results = $this->associateTasksWithProjects($tasks);

        $this->setResponse($this->getResponse()->withStatus(200));
        $this->set('WorkflowTasks', $results);
    }

    /**
     * Complete les tâches avec les infos de projet
     *
     * @param array $tasks from flowable
     * @return array
     */
    protected function associateTasksWithProjects(array $tasks): array
    {
        if (empty($tasks)) {
            return $tasks;
        }

        $allBusinessIds = [];

        foreach ($tasks as $index => $task) {
            $businessId = Hash::extract($task['variables'], '{n}[name=/^business_id$/]')[0]['value'];

            $allBusinessIds[] = $businessId;

            $tasks[$index] = [
                'id' => $task['id'],
                'name' => $task['name'],
                'createTime' => $task['createTime'],
                'taskDefinitionKey' => $task['taskDefinitionKey'],
                'parentTaskId' => $task['parentTaskId'],
                'executionId' => $task['executionId'],
                'processInstanceId' => $task['processInstanceId'],
                'processDefinitionId' => $task['processDefinitionId'],
                'business_id' => $businessId,
            ];
        }
        $projects = $this->fetchTable('Projects');
        $projects = $projects->find()
            ->select(
                [
                    'id',
                    'name',
                    'code_act',
                    'id_flowable_instance',
                ]
            )
            ->contain(
                [
                    'Containers' => [
                        'fields' => ['id', 'project_id'],
                        'Stateacts' => function (Query $query) {
                            return $query->select(['id', 'Stateacts.name']);
                        },
                        'Typesacts' => function (Query $query) {
                            return $query->select(['id', 'Typesacts.name']);
                        },
                    ],
                ]
            )
            ->where(
                [
                    'Projects.id_flowable_instance IN' => $allBusinessIds,
                    'structure_id' => $this->Multico->getCurrentStructureId(),
                ]
            )
            ->disableHydration()
            ->toArray();

        foreach ($tasks as &$task) {
            $projectForTask = null;
            foreach ($projects as $project) {
                if ($project['id_flowable_instance'] === $task['business_id']) {
                    $projectForTask = $project;
                }
            }
            $task['project'] = $projectForTask;
        }

        return $tasks;
    }

    /**
     * @param string $taskId id
     * @return \App\Model\Entity\Project
     * @throws \WrapperFlowable\Api\FlowableException
     */
    protected function getProjectForTask(string $taskId): Project // @fixme : déplacer dans façade
    {
        /** @var \App\Controller\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();

        $task = $wrapper->getHistoryTaskById($taskId);

        $businessId = Hash::extract($task['variables'], '{n}[name=/^business_id$/]')[0]['value'];

        $projectTable = $this->fetchTable('Projects');
        /** @var \App\Model\Entity\Project $project */
        $project = $projectTable->find()
            ->select(['id', 'structure_id', 'name'])
            ->contain(['Containers', 'Containers.Typesacts',])
            ->where(
                [
                    'Projects.id_flowable_instance' => $businessId,
                    'structure_id' => $this->Multico->getCurrentStructureId(),
                ]
            )
            ->firstOrFail();

        $response = $wrapper->getInstanceHistoryByBusinessKey($businessId);
        $project->endActivityId = $response['endActivityId'];
        $project->id_flowable_instance = $businessId;

        return $project;
    }

    /**
     * Validation d'une étape
     *
     * @param string $taskId id flowable
     * @return void
     * @throws \Exception
     */
    public function approveAction(string $taskId)
    {
        $success = $this->executeAction($taskId, FlowableFacade::ACTION_APPROVE);

        if ($success) {
            $project = $this->getProjectForTask($taskId);
            /**
             * @var \App\Service\NotificationService $notificationService
             */
            $notificationService = $this->loadService('Notification', [], false);

            if ($project->endActivityId === FlowableFacade::STATUS_APPROVED) {
                /** @var \App\Controller\ProjectsTable $Projects */
                $Projects = $this->fetchTable('Projects');
                $success = $Projects->approveProject($project, $this->Multico->getCurrentUserId());

                // @fixme : if problem : on rollback dans flowable. cf #486, #487
                //            if (!success) {
                //            $task = $workflowTask->getAdminTaskByBusinessId($businessId);
                //            $taskId = $task[0]['id'];
                //            $this->rollbackSystemAction($taskId);
            } else {
                $notificationService->fireEvent(
                    Stateact::VALIDATION_PENDING,
                    $project,
                    [
                        'last_user_id' => $this->Multico->getCurrentUserId(),
                        'comment' => $this->request->getData('comment'),
                    ]
                );
            }
            $notificationService->fireEvent(
                NotificationService::MY_PROJECT_AT_STATE,
                $project,
                [
                    'last_user_id' => $this->Multico->getCurrentUserId(),
                ]
            );
        }
        $this->set(compact('success'));
    }

    /**
     * Rejet d'étape
     *
     * @param string $taskId id flowable
     * @return void
     * @throws \Exception
     */
    public function rejectAction(string $taskId)
    {
        $success = $this->executeAction($taskId, FlowableFacade::ACTION_REJECT);

        if ($success) {
            /**
             * @var \App\Service\NotificationService $notificationService
             */
            $notificationService = $this->loadService('Notification', [], false);

            $project = $this->getProjectForTask($taskId);

            /** @var \App\Controller\ProjectsTable $Projects */
            $Projects = $this->fetchTable('Projects');
            $this->setResponse($this->getResponse()->withStatus(200));

            $success = $Projects->rejectProject($project, $this->Multico->getCurrentUserId());

            $notificationService->fireEvent(
                NotificationService::MY_PROJECT_AT_STATE,
                $project,
                [
                    'last_user_id' => $this->Multico->getCurrentUserId(),
                ]
            );
        }
        $this->set(compact('success'));

        // @fixme : if problem : on rollback dans flowable. cf #486, #487
        //            if (!success) {
        //            $task = $workflowTask->getAdminTaskByBusinessId($businessId);
        //            $taskId = $task[0]['id'];
        //            $this->rollbackSystemAction($taskId);
    }

    /**
     * Validation en urgence
     *
     * @param string $projectId Identifiant du projet que l'on souhaite valider
     * @throws \Exception
     * @return void
     */
    public function approveProject(string $projectId)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        /** @var \App\Controller\ProjectsTable $Projects */
        $Projects = $this->fetchTable('Projects');
        $idFlowableInstance = $Projects->find()
            ->select(
                [
                    'id',
                    'id_flowable_instance',
                ]
            )
            ->where(['id' => $projectId])
            ->firstOrFail()
            ->get('id_flowable_instance');

        /** @var \App\Controller\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();

        $task = $wrapper->getAdminTaskByBusinessId($idFlowableInstance);
        $taskId = $task[0]['id'];

        $success = $this->executeAction($taskId, FlowableFacade::ACTION_APPROVE); // fixme : récupération projet en doublon

        /** @var \App\Controller\UsersInstancesTable $usersInstances */
        $usersInstances = $this->fetchTable('UsersInstances');
        $instances = $usersInstances->find('all');

        if ($success) {
            $instances->delete()->where(['id_flowable_instance' => $idFlowableInstance])->execute();
            $project = $this->getProjectForTask($taskId);
            $Projects->approveProject($project, $this->Multico->getCurrentUserId(), true);

            /**
             * @var \App\Service\NotificationService $notificationService
             */
            $notificationService = $this->loadService('Notification', [], false);

            $notificationService->fireEvent(
                NotificationService::MY_PROJECT_AT_STATE,
                $project,
                [
                    'last_user_id' => $this->Multico->getCurrentUserId(),
                ]
            );
            $this->setResponse($this->getResponse()->withStatus(200));
        }

        // @fixme : if problem : on rollback dans flowable. cf #486, #487
        //            if (!success) {
        //            $task = $workflowTask->getAdminTaskByBusinessId($businessId);
        //            $taskId = $task[0]['id'];
        //            $this->rollbackSystemAction($taskId);
        //            }
    }

    // AFAIRE
    //    public function cancelAction(string $taskId)
    //    {
    //        if (!$this->Multico->isAdminInCurrentOrganization()) {
    //            throw new UnauthorizedException();
    //        }
    //
    //        $this->executeAction($taskId, FlowableFacade::ACTION_CANCEL);
    //    }


    // AFAIRE
    //    /**
    //     * @param string $taskId id flowable
    //     * @throws \Exception
    //     * @return void
    //     */
    //    public function rollbackAction(string $taskId)
    //    {
    //        $this->request->allowMethod('post');
    //
    //        $this->executeAction($taskId, FlowableFacade::ACTION_R);
    //    }

    /**
     * @param string $taskId flowable id
     * @param string $action action name
     * @return bool
     */
    protected function executeAction(string $taskId, string $action): bool
    {
        $this->request->allowMethod('post');

        $comment = $this->request->getData()['comment'] ?? null;

        $userId = $this->Multico->getCurrentUserId();

        try {
            $isLegit = $this->isTaskSubmissionLegit($taskId, $userId);

            if (!$isLegit) {
                $this->setResponse($this->getResponse()->withStatus(401));

                return false;
            }

            $data = [
                'userId' => $userId,
                'task' => $taskId,
                'action' => $action,
                'comment' => $comment,
            ];

            /** @var \App\Controller\FlowableWrapper $wrapper */
            $wrapper = WrapperFactory::createWrapper();
            $wrapper->executeAnActionOnTask($data);
        } catch (FlowableException $e) {
            $this->setResponse($this->getResponse()->withStatus($e->getCode()));

            return false;
        }

        return true;
    }

    /**
     * @param string $taskId id
     * @return void
     * @throws \WrapperFlowable\Api\FlowableException
     */
    protected function rollbackSystemAction(string $taskId)
    {
        // @fixme : cf #487
        $data = [
            'task' => $taskId,
            'action' => 'rollback_system', // fixme: déplacer dans une constante
        ];

        /** @var \App\Controller\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();
        $wrapper->executeAnActionOnTask($data);
    }

    /**
     * @param string $taskId flowable
     * @param int $userId id
     * @return bool
     * @throws \WrapperFlowable\Api\FlowableException
     */
    protected function isTaskSubmissionLegit(string $taskId, int $userId): bool
    {
        if ($this->Multico->isAdminInCurrentOrganization()) {
            return true;
        }

        /** @var \App\Controller\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();
        $variables = $wrapper->getTaskVariables($taskId);
        $usersCandidate = explode(',', Hash::extract($variables, '{n}[name=/^users_candidate$/]')[0]['value']);

        return in_array((string)$userId, $usersCandidate);
    }

    /**
     * @return void
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function executeActionForward()
    {
        $this->request->allowMethod('post');

        $datas = $this->request->getData();

        /** @var \App\Controller\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();
        $wrapper->executeAnActionForwardOnTask($datas['task']);

        $this->set('success', true);
        $this->setResponse($this->getResponse()->withStatus(200));
    }
}
