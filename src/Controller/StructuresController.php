<?php
declare(strict_types=1);

namespace App\Controller;

use App\Validation\Validator;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\ORM\Locator\TableLocator;
use Cake\Utility\Hash;
use Exception;

/**
 * Structures Controller
 *
 * @property \App\Model\Table\StructuresTable $Structures
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\PastellComponent $Pastell
 * @property \App\Model\Table\RolesTable $Roles
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @method \App\Controller\Structure[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StructuresController extends AppController
{
    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->Auth->allowedActions = [
            'getStructureInfo',
            'getLogo',
            'getCustomName',
        ];
        $this->loadComponent('QueryParams');
        $this->loadComponent('QueryUtils');
    }

    /**
     * Index method
     */
    public function index(): void
    {
        if (!$this->Multico->isSuperAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $params = $this->QueryParams->validateIndexWithPaginationTrueFalse(
            (new Validator())
                ->allowPresence('name')
                ->allowEmptyString('name'),
            ['name' => null]
        );

        $query = $this->Structures
            ->find()
            ->contain(['Roles' => ['sort' => ['Roles.name' => 'ASC']]])
            ->where(
                !empty($params['name'])
                ? [
                    'OR' => [
                        $this->QueryUtils->getIlikeQueryExpression('Structures.business_name', "%{$params['name']}%"),
                        $this->QueryUtils->getIlikeQueryExpression('Structures.customname', "%{$params['name']}%"),
                    ],
                ]
                : []
            )
            ->orderAsc('Structures.business_name');

        if ($params['paginate'] === 'true') {
            $this->paginate = ['maxLimit' => PAGINATION_LIMIT];
            $structures = $this->paginate($query);
        } else {
            $structures = $query->all();
        }

        $this->set(compact('structures'));
    }

    /**
     * Visualisation des informations d'une structure avec celle de l'organisation rattaché
     *
     * @param int $id Structure id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @access  public
     * @created 12/04/2019
     * @version V0.0.9
     */
    public function view(string $id)
    {
        if ((int)$id !== $this->Multico->getCurrentStructureId()) {
            throw new RecordNotFoundException('Record not found in table "structures"'); // FIXME: vérification de droit au lieu de limiter à la structure courante
        }
        $structure = $this->Structures->get(
            $id,
            [
                'contain' => ['Organizations', 'Roles'],
            ]
        );

        $this->set('structure', $structure);
    }

    /**
     * Ajout d'une structure
     *
     * @return \App\Controller\Response|void Redirects on successful add, renders view otherwise.
     * @access  public
     * @created 12/04/2019
     * @version V0.0.9
     */
    public function add()
    {
        throw new UnauthorizedException(); // FIXME: vérification de droit

        //        $structure = $this->Structures->newEntity();
        //        if ($this->request->is('post')) {
        //            $structure = $this->Structures->patchEntity($structure, $this->request->getData());
        //
        //            if ($this->Structures->save($structure)) {
        //                $this->setResponse($this->getResponse()->withStatus(201));
        //                $this->set("id", $structure->id);
        //
        //                return;
        //            }
        //        }
        //
        //        $this->setResponse($this->getResponse()->withStatus(400));
        //        $this->set($structure->getErrors());
    }

    /**
     * Modification d'une structure
     *
     * @param int $id Structure id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @access  public
     * @created 12/04/2019
     * @version V0.0.9
     */
    public function edit($id)
    {
        throw new UnauthorizedException(); // FIXME: vérification de droit
        //
        //        $structure = $this->Structures->get($id);
        //        if ($this->request->is(['patch', 'put'])) {
        //            $structure = $this->Structures->patchEntity($structure, $this->request->getData());
        //
        //            if ($this->Structures->save($structure)) {
        //                $this->setResponse($this->getResponse()->withStatus(200));
        //                $this->set("structure", $structure);
        //
        //                return;
        //            }
        //            $this->setResponse($this->getResponse()->withStatus(400));
        //            $this->set($structure->getErrors());
        //        }
    }

    /**
     * Delete method
     *
     * @param int $id Structure id.
     * @return void
     * @access  public
     * @created 12/04/2019
     * @version V0.0.9
     */
    public function delete($id)
    {
        throw new UnauthorizedException(); // FIXME: vérification de droit

        //        if ($this->request->is('delete')) {
        //            $structure = $this->Structures->get($id);
        //
        //            if ($this->Structures->delete($structure)) {
        //                $this->setResponse($this->getResponse()->withStatus(204));
        //            } else {
        //                $this->setResponse($this->getResponse()->withStatus(400));
        //            }
        //
        //            return;
        //        }
    }

    /**
     * Fonction qui récupère les entitées présente sous l'orchestrateur (PASTELL) grace au id_e en parametre puis les
     * sauvegarde en bdd
     * Nécésite un utilisateur présent dans toutes les collectivités
     *
     * @return void
     * @access  public
     * @created 03/05/2019
     * @version V0.0.9
     */
    public function addStructuresWithOrchestration()
    {
        $this->request->allowMethod('put');

        $success = false;
        $structuresOrchestration_ids = $this->request->getData();

        if (is_array($structuresOrchestration_ids) === true && empty($structuresOrchestration_ids) === false) {
            $structure = $this->Structures->get(
                $this->Multico->getCurrentStructureId(),
                ['contain' => ['Connecteurs' => ['conditions' => ['connector_type_id' => 1]]]]
            );

            $this->loadComponent('Pastell', $structure->pastellConnector);

            $success = $this->Pastell->addStructures($structuresOrchestration_ids);
        }

        if ($success === true) {
            $this->setResponse($this->getResponse()->withStatus(201));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Fonction permettant de faire appel à la récupération et l'enregistrement de la classification
     *
     * @return void
     * @access  public
     * @created 29/05/2019
     * @version V0.0.9
     */
    public function updateClassification()
    {
        if ($this->getRequest()->is('get')) {
            $structureId = $this->Multico->getCurrentStructureId();
            $structure = $this->Structures->get($structureId);

            if ($structure['id_orchestration'] === null) {
                $this->setResponse($this->getResponse()->withStatus(404));

                return;
            }

            $structure = $this->Structures->get(
                $this->Multico->getCurrentStructureId(),
                ['contain' => ['Connecteurs' => ['conditions' => ['connector_type_id' => 1]]]]
            );
            $this->loadComponent('Pastell', $structure->pastellConnector);

            if ($this->Pastell->updateClassification($structureId, $structure['id_orchestration'])) {
                $this->set('success', true);
                $this->setResponse($this->getResponse()->withStatus(200));
            } else {
                $this->setResponse($this->getResponse()->withStatus(400));
            }

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(404));
    }

    /**
     * set the structure logo
     *
     * @return void
     */
    public function setLogo()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can set a logo');
        }

        $structureId = $this->Multico->getCurrentStructureId();
        $structure = $this->Structures->get($structureId);

        $success = true;

        $data = $this->getRequest()->getData();

        if (empty($data['logo'])) {
            $this->set('error', 'No logo file');
            $this->setResponse($this->getResponse()->withStatus(400));

            return;
        }

        $logoFile = $data['logo'];

        //On vérifie si le type du ficher chargé et une image  , jpg,png,gif
        if (!preg_match('/image\/(jpeg|jpg|png|gif)/', $logoFile->getClientMediaType())) {
            $this->set('error', 'mimetype should be image/*');
            $this->setResponse($this->getResponse()->withStatus(400));
        }

        $folder = LOGO . DS . $structureId;

        if (file_exists($folder) === false) {
            $success = $success && mkdir($folder, 0777, true);
        }

        $path_parts = pathinfo($logoFile->getClientFilename());
        $fileExtension = $path_parts['extension'];

        $logoFile->moveTo($folder . DS . 'logo.' . $fileExtension);

        $structure->logo = $folder . DS . 'logo.' . $fileExtension;
        $this->Structures->save($structure);

        if (!$success) {
            $this->setResponse($this->getResponse()->withStatus(500));

            return;
        }

        $this->set('success', 'logo saved');
    }

    /**
     * delete structure logo
     *
     * @return void
     */
    public function deleteLogo()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only an admin can delete the logo');
        }

        $structureId = $this->Multico->getCurrentStructureId();
        $structure = $this->Structures->get($structureId);

        $logo = $structure->logo;

        if (empty($logo)) {
            $this->set('success', 'no logo');

            return;
        }

        $res = unlink($logo);

        if (!$res) {
            $this->setResponse($this->getResponse()->withStatus(500));

            return;
        }

        $structure->logo = null;
        $this->Structures->save($structure);

        $this->set('success', 'logo deleted');
    }

    /**
     * set the structure logo
     *
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function setCustomName()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only an admin can set a custom name');
        }

        $structureId = $this->Multico->getCurrentStructureId();
        $structure = $this->Structures->get($structureId);

        $data = $this->getRequest()->getData();

        if (empty($data['customname'])) {
            $this->set('error', 'No name provided');
            $this->setResponse($this->getResponse()->withStatus(400));

            return;
        }

        $structure->customname = $data['customname'];

        $success = $this->Structures->save($structure);

        if (!$success) {
            $this->setResponse($this->getResponse()->withStatus(500));

            return;
        }

        $this->set('success', 'name saved');
    }

    /**
     * set the structure spinner
     *
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function setSpinner()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only an admin can set a custom name');
        }

        $structureId = $this->Multico->getCurrentStructureId();
        $structure = $this->Structures->get($structureId);

        $data = $this->getRequest()->getData();

        if (empty($data['spinner'])) {
            $this->set('error', 'No spinner name');
            $this->setResponse($this->getResponse()->withStatus(400));

            return;
        }

        $structure->spinner = $data['spinner'];

        $success = $this->Structures->save($structure);

        if (!$success) {
            $this->setResponse($this->getResponse()->withStatus(500));

            return;
        }

        $this->set('success', 'spinner saved');
    }

    /**
     * get the structure logo
     *
     * @return \Cake\Http\Response
     */
    public function getLogo()
    {
        $structureId = $this->Multico->getCurrentStructureId();

        $structure = $this->Structures->get($structureId);

        if (empty($structure) || $structure->logo === null) {
            $this->setResponse($this->getResponse()->withStatus(404));

            return $this->getResponse();
        } else {
            return $this->response->withFile(
                $structure['logo'],
                ['download' => true, 'name' => 'logo.png']
            );
        }
    }

    /**
     * Fonction permettant de récupérer le type de piece jointe en fonction de la nature selectionnée
     *
     * @param int $nature_id ID de la nature selectionnée
     * @return void
     * @access  public
     * @created 29/05/2019
     * @version V0.0.9
     */
    public function getTypesPiecesJointes($nature_id)
    {
        // FIXME: dégager le structure_id du path cf. #309

        $this->request->allowMethod('get');

        $tableLocatorTypespiecesjointes = new TableLocator();
        $Typespiecesjointes = $tableLocatorTypespiecesjointes->get('Typespiecesjointes');

        $valuesTypespiecesjointes = $Typespiecesjointes->find()
            ->select(
                [
                    'id',
                    'structure_id',
                    'nature_id',
                    'codetype',
                    'libelle',
                ]
            )
            ->where(
                [
                    'structure_id' => $this->Multico->getCurrentStructureId(),
                    'nature_id' => $nature_id,
                ]
            )
            ->order(['id' => 'asc'])
            ->toArray();

        $this->set('typespiecesjointes', $valuesTypespiecesjointes);
    }

    /**
     * get the structure infos
     *
     * @return void
     */
    public function getStructureInfo()
    {
        $roles = $this->fetchTable('Roles');

        if ($this->Multico->isSuperAdminInCurrentOrganization()) {
            $organizationId = $this->Multico->getCurrentOrganizationId();
            $structuresOrganization = $this->Structures->findStructuresOrganization($organizationId);
            $userId = $this->Multico->getCurrentUserId();

            $Users = $this->LoadModel('Users');
            $user = $Users->get($userId, ['contain' => ['Roles']]);

            $rolesToSend = $roles->find()
                ->where(['roles.structure_id IN' => Hash::extract($structuresOrganization, '{n}.id')])
                ->toArray();

            $userToSend = [
                'id' => $user->id,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'role' => [
                    'id' => $user->roles[0]['id'],
                    'name' => $user->roles[0]['name'],
                ],
            ];

            $structuresUsersTable = $this->fetchTable('StructuresUsers');
            $superAdminStructure =
                $structuresUsersTable->find()
                    ->where(['user_id' => $user->id])
                    ->first()
                    ->toArray();

            $structure = $this->Structures->get($superAdminStructure['structure_id'])->toArray();
            $structureToSend = [
                'id' => $structure['id'],
                'name' => $structure['business_name'],
                'customname' =>
                    empty($structure['customname'])
                        ? $structure['business_name']
                        : $structure['customname'],
                'spinner' => $structure['spinner'],
                'active' => $structure['active'],
                'organization_id' => $this->Multico->getCurrentOrganizationId(),
                'roles' => $this->Multico->getStructureRoles($structure['id']),
            ];

            $StructureSettings = $this->LoadModel('StructureSettings');
            $structureSetting = $StructureSettings
                ->getSettingsByStructureId($structure['id']);

            $this->set('user', $userToSend);
            $this->set('structure', $structureToSend);
            $this->set('structureSetting', $structureSetting);
            $this->set('roles', $rolesToSend);
        } else {
            $structureId = $this->Multico->getCurrentStructureId();
            $userId = $this->Multico->getCurrentUserId();

            $structure = $this->Structures->get($structureId);

            $Users = $this->LoadModel('Users');
            $user = $Users->get($userId, ['contain' => ['Roles']]);

            $rolesToSend = $roles->find()
                ->where(['roles.structure_id' => $structureId])
                ->toArray();

            $structureToSend = [
                'id' => $structure->id,
                'name' => $structure->business_name,
                'customname' => empty($structure->customname) ? $structure->business_name : $structure->customname,
                'spinner' => $structure->spinner,
                'active' => $structure->active,
                'organization_id' => $structure->organization_id,
                'roles' => $this->Multico->getStructureRoles($structure->id),
            ];

            $userToSend = [
                'id' => $user->id,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'role' => [
                    'id' => $user->roles[0]['id'],
                    'name' => $user->roles[0]['name'],
                ],
            ];

            $StructureSettings = $this->LoadModel('StructureSettings');
            $structureSetting = $StructureSettings
                ->getSettingsByStructureId($structure->id);

            $this->set('user', $userToSend);
            $this->set('structure', $structureToSend);
            $this->set('structureSetting', $structureSetting);
            $this->set('roles', $rolesToSend);
        }
    }

    /**
     * get last classification date
     *
     * @return void
     */
    public function getLastClassificationDate()
    {
        //get Last classification
        $Classifications = $this->fetchTable('Classifications');
        $classification = $Classifications->find()
            ->select(['dateclassification'])
            ->where(['structure_id' => $this->Multico->getCurrentStructureId()])
            ->order(['id' => 'DESC'])
            ->limit(1)
            ->toArray();

        if (empty($classification)) {
            $this->set('dateclassification', null);

            return;
        }
        $this->set('dateclassification', $classification[0]['dateclassification']);
    }

    /**
     * get the structure custom name
     *
     * @return void
     */
    public function getCustomName()
    {
        $structureId = $this->Multico->getCurrentStructureId();
        $structure = $this->Structures->get($structureId);

        if (empty($structure)) {
            $this->setResponse($this->getResponse()->withStatus(404));
        }

        $this->set('customname', $structure->customname);
    }

    /**
     * check if pastell connexion is ok
     * post data {username, password, pastellUrl, pastellEntityId }
     *
     * @return void
     * @throws \Exception
     */
    public function checkPastellConnexion()
    {
        $data = $this->getRequest()->getData();

        if (empty($data['password'])) {
            $Connecteurs = $this->fetchTable('Connecteurs');
            $connecteur = $Connecteurs->find()->where([
                'connector_type_id' => 1,
                'structure_id' => $this->Multico->getCurrentStructureId(),
            ])->first();

            if (!empty($connecteur)) {
                $data['password'] = $connecteur->get('password');
            }
        }

        $pastellInitInfo = [
            'pastell_user' => $data['username'],
            'pastell_password' => $data['password'],
            'pastell_url' => $data['pastellUrl'],
        ];
        $this->loadComponent('Pastell', $pastellInitInfo);

        try {
            $isValid = $this->Pastell->checkPastellConnexion($data['pastellEntityId']);
            $this->set('success', $isValid);
        } catch (Exception $e) {
            $this->set('success', false);
            $this->set('error', $e->getMessage());
        }
    }

    /**
     * savePastellConnexion
     * post data {username, password, pastellUrl, pastellEntityId, slowUrl }
     *
     * @return void
     */
    public function savePastellConnexion()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException(); # Must be Forbidden (403) instead
        }

        $data = $this->getRequest()->getData();

        //get current structure
        $structure = $this->Structures->get($this->Multico->getCurrentStructureId());
        $structureArray = $structure->toArray();
        $structureArray['id_orchestration'] = $data['pastellEntityId'];

        $Connecteurs = $this->fetchTable('Connecteurs');
        $connecteur = $Connecteurs->find()->where([
            'connector_type_id' => 1,
            'structure_id' => $this->Multico->getCurrentStructureId(),
        ])->first();

        $connecteurData = [
            'name' => 'pastell',
            'username' => $data['username'],
            'connector_type_id' => 1,
            'structure_id' => $this->Multico->getCurrentStructureId(),
            'url' => $data['pastellUrl'],
            'tdt_url' => $data['slowUrl'] . S2LOW_API,
        ];
        if (! empty($data['password'])) {
            $connecteurData['password'] = $data['password'];
        }

        if (empty($connecteur)) {
            $connecteurs = $Connecteurs->newEntity($connecteurData);
        } else {
            $connecteurs = $Connecteurs->patchEntity($connecteur, $connecteurData);
        }

        $this->Structures->getConnection()->begin();

        $structure = $this->Structures->patchEntity($structure, $structureArray);

        $resCon = $Connecteurs->save($connecteurs);
        $resStruc = $this->Structures->save($structure);

        if (!$resCon) {
            $this->set('success', false);
            $this->set('error', $connecteurs->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->Structures->getConnection()->rollback();

            return;
        }

        if (!$resStruc) {
            $this->set('success', false);
            $this->set('error', $structure->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->Structures->getConnection()->rollback();

            return;
        }

        $this->Structures->getConnection()->commit();
        $this->set('success', true);
    }

    /**
     * get pastell connexion info
     *
     * @return void
     */
    public function getPastellConnexion()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $structure = $this->Structures->get($this->Multico->getCurrentStructureId());
        $Connecteurs = $this->fetchTable('Connecteurs');
        $connecteur = $Connecteurs->find()->where([
            'connector_type_id' => 1,
            'structure_id' => $this->Multico->getCurrentStructureId(),
        ])->first();

        if (empty($connecteur)) {
            $this->set('pastell', []);

            return;
        }

        $slowUrl = preg_replace('/^(https{0,1}:\/\/){0,1}([^\/]+)\/.*$/', '\1\2', $connecteur->get('tdt_url'));

        $data = [
            'username' => $connecteur->get('username'),
            'password' => '',
            'pastellUrl' => $connecteur->get('url'),
            'pastellEntityId' => $structure->get('id_orchestration'),
            'slowUrl' => $slowUrl ? $slowUrl : '',
        ];
        $this->set('pastell', $data);
    }

    /**
     * get pastell connexion info
     *
     * @return void
     */
    public function getIdelibreConnexion()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $Connecteurs = $this->fetchTable('Connecteurs');
        $connecteur = $Connecteurs->find()->where([
            'connector_type_id' => 2,
            'structure_id' => $this->Multico->getCurrentStructureId(),
        ])->first();

        if (empty($connecteur)) {
            $this->set('idelibre', []);

            return;
        }

        $data = [
            'username' => $connecteur->get('username'),
            'url' => $connecteur->get('url'),
            'connexion_option' => $connecteur->get('connexion_option'),
        ];
        $this->set('idelibre', $data);
    }

    /**
     * savePastellConnexion
     * post data {username, password, pastellUrl, pastellEntityId, slowUrl }
     *
     * @return void
     */
    public function saveIdelibreConnexion()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $data = $this->getRequest()->getData();

        $Connecteurs = $this->fetchTable('Connecteurs');
        $connecteur = $Connecteurs->find()->where([
            'connector_type_id' => 2,
            'structure_id' => $this->Multico->getCurrentStructureId(),
        ])->first();

        $connecteurData = [
            'name' => 'idelibre',
            'username' => $data['username'],
            'connector_type_id' => 2,
            'structure_id' => $this->Multico->getCurrentStructureId(),
            'url' => $data['url'],
            'connexion_option' => $data['connexion_option'],
        ];
        if (! empty($data['password'])) {
            $connecteurData['password'] = $data['password'];
        }
        if (empty($connecteur)) {
            $connecteurs = $Connecteurs->newEntity($connecteurData);
        } else {
            $connecteurs = $Connecteurs->patchEntity($connecteur, $connecteurData);
        }

        $resCon = $Connecteurs->save($connecteurs);

        if (!$resCon) {
            $this->set('success', false);
            $this->set('error', $connecteurs->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));

            return;
        }

        $this->set('success', true);
    }

    /**
     * check if pastell connexion is ok
     * post data {username, password, pastellUrl, pastellEntityId }
     *
     * @return void
     * @throws \Exception
     */
    public function checkIdelibreConnexion()
    {
        $data = $this->getRequest()->getData();

        if (empty($data['password'])) {
            $Connecteurs = $this->fetchTable('Connecteurs');
            $connecteur = $Connecteurs->find()->where([
                'connector_type_id' => 2,
                'structure_id' => $this->Multico->getCurrentStructureId(),
            ])->first();

            if (!empty($connecteur)) {
                $data['password'] = $connecteur->get('password');
            }
        }

        $idelibreInitInfo = [
            'username' => $data['username'],
            'password' => $data['password'],
            'url' => $data['url'],
            'conn' => $data['connexion_option'],
        ];

        $this->loadComponent('Idelibre', $idelibreInitInfo);

        try {
            $isValid = $this->Idelibre->checkConnexion();
            $this->set('success', $isValid);
        } catch (Exception $e) {
            $this->set('success', false);
            $this->set('error', $e->getMessage());
        }
    }
}
