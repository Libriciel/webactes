<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\RolesTable;

/**
 * Roles Controller
 *
 * @property \App\Model\Table\RolesTable $Roles
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Controller\Role[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RolesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->Roles->behaviors()->get('VirtualFields')->enableAuto(true);

        if ($this->Multico->isSuperAdminInCurrentOrganization()) {
            $this->paginate = [
                'order' => ['name'],
            ];
            $query = $this->Roles
                ->find()
                ->select()
                ->enableHydration(false);
        } else {
            $this->paginate = [
                'order' => ['name'],
            ];

            $query = $this->Roles->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()])
                ->where(function ($exp) {
                    return $exp
                        ->notEq('name', RolesTable::SUPER_ADMINISTRATOR);
                })
                ->enableHydration(false);
        }

        $roles = $this->paginate($query);

        $this->set(compact('roles'));
    }

    /**
     * View method
     *
     * @deprecated
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $role = $this->Roles->get($id, ['limitToStructure' => $this->Multico->getCurrentStructureId()]);

        $this->set(compact('role'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|void
     */
    public function add()
    {
        $this->edit();
    }

    /**
     * Edit method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $isAdd = $this->request->getParam('action') === 'add';
        $this->request->allowMethod($isAdd === true ? ['post'] : ['put']);
        $options = ['limitToStructure' => $this->Multico->getCurrentStructureId()];
        if ($isAdd) {
            $role = $this->Roles->newEntity($this->request->getData(), $options);
        } else {
            $role = $this->Roles->patchEntity($this->Roles->get($id, $options), $this->request->getData());
        }

        if ($this->Roles->save($role, $options) === true) {
            $this->setResponse($this->getResponse()->withStatus($isAdd === true ? 201 : 200));
            $this->set(compact('role'));
        } else {
            $this->set('errors', $role->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Retourne tous les rôles pour la création d'utilisateurs
     *
     * @return \App\Controller\RolesController
     */
    public function getRolesNames()
    {
        $this->paginate = [
            'maxLimit' => MAXIMUM_LIMIT,
        ];
        $roles = $this->paginate($this->Roles->getRolesNames());

        return $this->set(compact('roles'));
    }
}
