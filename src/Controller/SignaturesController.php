<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\NotificationService;
use Beanstalk\Utility\Beanstalk;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Utility\Hash;
use Exception;
use Throwable;

/**
 * Signatures Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 * @property \App\Controller\Component\MulticoComponent $Multico
 */
class SignaturesController extends AppController
{
    use ServiceAwareTrait;

    public const IPARAPHEUR_HISTORIQUE = 'iparapheur_historique';
    public const IPARAPHEUR_ACTE_SIGNE = 'Acte_signé';
    public const IPARAPHEUR_BORDEREAU = 'Bordereau_signature';

    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        $this->Projects = $this->fetchTable('Projects');
        parent::initialize();
    }

    /**
     * Permet de déclarer le document signé manuscritement
     *
     * @param string $id identifiant du projet à signer
     * @return void
     */
    public function manualSignature(string $id)
    {
        $this->request->allowMethod(['patch', 'put']);

        $jsonProject = Hash::get($this->getRequest()->getData(), 'project');
        $rawProject = json_decode($jsonProject, true);
        $mainDocument = Hash::get($this->getRequest()->getData(), 'mainDocument');

        $rawProject['stateact_id'] = DECLARE_SIGNED;
        $options = $this->getRequest()->getSession()->read();

        $project = $this->Projects->manualSignature($id, $rawProject, $mainDocument, $options);

        if ($project->hasErrors() === false) {
            $code = $this->request->is('patch') ? 204 : 200;
            /**
             * @var \App\Service\NotificationService $notificationService
             */
            $notificationService = $this->loadService('Notification', [], false);
            $notificationService->fireEvent(
                NotificationService::MY_PROJECT_AT_STATE,
                $project,
                [
                    'last_user_id' => $this->Multico->getCurrentUserId(),
                ]
            );
            $this->setResponse($this->getResponse()->withStatus($code));
            if ($this->request->is('put') === true) {
                $this->set(compact('project'));
            }
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $project->getErrors());
        }
    }

    /**
     * Get iparapheur sous type for a project
     *
     * @return void
     */
    public function getIParapheurSousTypes()
    {
        $project = $this->Projects->get(
            $this->request->getParam('id'),
            [
                'contain' => [
                    'Containers',
                ],
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        if (!empty($project)) {
            $sousTypesParapheur = $this
                ->Projects
                ->Containers
                ->getIparapheurSousTypes(
                    $project->containers[0]->id,
                    $project->containers[0]->structure_id
                );

            $sousTypesParapheurList = [];

            if (!isset($sousTypesParapheur['status'])) {
                foreach ($sousTypesParapheur as $key => $sousTypes) {
                    $sousTypesParapheurList[] = [
                        'id' => $key,
                        'name' => $sousTypes];
                }
            }
        }

        if (!empty($sousTypesParapheurList)) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set($sousTypesParapheur);
        } else {
            $this->setResponse($this->getResponse()->withStatus(404));
            $this->set(
                [
                    'errors' => ['sousTypesParapheurs' => 'Aucun sous-types parapheur pour ce document']]
            );
        }
    }

    /**
     * Action for electronic signature
     *
     * @return \Cake\Http\Response|void
     */
    public function electronicSignature()
    {
        /**
         * @var \App\Service\NotificationService $notificationService
         */
        $notificationService = $this->loadService('Notification', [], false);

        $this->request->allowMethod(['post', 'put']);

        $sousTypeParapheur = json_decode($this->getRequest()->getData('soustypeParapheur'), true);
        $jsonProject = Hash::get($this->getRequest()->getData(), 'project');
        $rawProject = json_decode($jsonProject, true);
        $mainDocument = Hash::get($this->getRequest()->getData(), 'mainDocument');

        $projectId = $this->getRequest()->getParam('id');
        $structureId = $this->Multico->getCurrentStructureId();
        //A AMELIORER !!!!!
        //get current project
        $projectServer = $this->Projects->find()
            ->where(['Projects.id' => $projectId])
            ->contain(
                ['Containers.Annexes',
                    'Containers.Maindocuments' => [
                        'conditions' => ['current' => true],
                    ],
                    'Containers.Typesacts',
                ]
            )
            ->first();

        $options = $this->getRequest()->getSession()->read();

        try {
            if (!empty($mainDocument)) {
                $accepted = (array)Configure::read('mainDocumentAcceptedTypesForSigned');
                if (!in_array($mainDocument->getClientMediaType(), $accepted)) {
                    $errors = [
                        'id' => [
                            sprintf(
                                '"%s" MIME type is not amongst the accepted types: "%s"',
                                $mainDocument['type'],
                                implode('"", "', $accepted)
                            ),
                        ],
                    ];

                    $this->set(compact('errors'));
                    $this->setResponse($this->getResponse()->withStatus(400));

                    return;
                }

                $this->Projects->replaceTheMainDocument($projectId, $rawProject, $mainDocument, $options);
            }
        } catch (Throwable $exception) {
        }

        // sendParapheurAgain
        if ($this->Projects->isCurrentStateEqualsTo($projectServer->containers[0]->id, PARAPHEUR_REFUSED)) {
            $this->Projects->Containers->addStateact($projectServer->containers[0]->id, PARAPHEUR_PENDING);
            $event = new Event(
                'Model.Project.before.sendParapheurAgain',
                $this,
                [
                    'id_d' => $projectServer->containers[0]->id,
                    'object_id' => $projectServer->containers[0]->id,
                    'flux' => Configure::read('Flux.actes-generique.name'),
                    'flux_name' => Configure::read('Flux.actes-generique.name'),
                    'structureId' => $projectServer->containers[0]->structure_id,
                    'organizationId' => $this->Projects->Structures->getOrganizationId(
                        $projectServer->containers[0]->structure_id
                    ),
                    'sous-type-parapheur' => $sousTypeParapheur,
                    'user_id' => $this->Multico->getCurrentUserId(),
                ]
            );
            if ($this->getEventManager()->dispatch($event)) {
                $notificationService->fireEvent(
                    NotificationService::MY_PROJECT_AT_STATE,
                    $projectServer,
                    [
                        'last_user_id' => $this->Multico->getCurrentUserId(),
                    ]
                );
                $this->setResponse($this->getResponse()->withStatus(201));
            } else {
                $this->setResponse($this->getResponse()->withStatus(400));
            }
        } else {
            $this->Projects->generateActNumber((int)$projectId);
            $this->sendMessageParapheur((int)$projectId, $structureId, $sousTypeParapheur);
        }

        return $this->getResponse();
    }

    /**
     * Get the current container's status on iparapheur via PASTELL
     *
     * @return void
     */
    public function getStatusIParapheur()
    {
        $this->request->allowMethod(['get']);

        $projectId = $this->getRequest()->getParam('id');

        /**
         * @var \App\Controller\ContainersTable $Containers
         */
        $Containers = $this->fetchTable('Containers');
        $containerId = $Containers->find()
            ->select(['id'])
            ->where(['project_id' => $projectId])
            ->firstOrFail()->get('id');

        if (!$Containers->Projects->isCurrentStateEqualsTo($containerId, PARAPHEUR_REFUSED)) {
            $beanstalk = new Beanstalk('signature-pastell');
            $message = [
                'flux' => 'actes-generique',
                'flux_name' => 'actes-generique',
                'action' => 'checkStatusParapheur',
                'structure_id' => $this->Multico->getCurrentStructureId(),
                'container_id' => $containerId,
            ];
            $beanstalk->emit($message, Configure::read('Beanstalk.Priority.VERY_HIGHT'));
        }

        $this->set($Containers->getIParapheurStatus($this->Multico->getCurrentStructureId(), $containerId));
    }

    /**
     * get the return documents of signatures (document signé, annexes) from pastell
     *
     * @param int $projectId projectId
     * @param string $type document type
     * @return \Cake\Http\Response|null
     * @throws \Exception
     */
    private function getSignDocuments($projectId, $type)
    {
        $container = $this->Projects->getContainerByProject($projectId, $this->Multico->getCurrentStructureId());
        $Structures = $this->fetchTable('Structures');
        $structure = $Structures->get(
            $this->Multico->getCurrentStructureId(),
            ['contain' => ['Connecteurs' => ['conditions' => ['connector_type_id' => 1]]]]
        );

        $Pastellfluxs = $this->fetchTable('Pastellfluxs');

        $flux = $Pastellfluxs->find()->where(['container_id' => $container->get('id')])->orderDesc('id')->firstOrFail();

        $pastellDocumentId = $flux->get('id_d');

        $data = [
            'pastell_user' => $structure['connecteur']['username'],
            'pastell_password' => $structure['connecteur']['password'],
            'pastell_url' => $structure['connecteur']['url'],
        ];

        $this->loadComponent('Pastell', $data);

        $documentName = null;
        /**
         * @var \Cake\Http\Client\Response $res
         */
        $res = null;
        switch ($type) {
            case self::IPARAPHEUR_ACTE_SIGNE:
                $res = $this->Pastell->getSignedDocument($structure->get('id_orchestration'), $pastellDocumentId);
                $documentName = self::IPARAPHEUR_ACTE_SIGNE . 'pdf';
                break;
            case self::IPARAPHEUR_BORDEREAU:
                $res = $this->Pastell->getSignBordereau($structure->get('id_orchestration'), $pastellDocumentId);
                $documentName = self::IPARAPHEUR_BORDEREAU . 'pdf';
                break;
            case self::IPARAPHEUR_HISTORIQUE:
                $res = $this->Pastell->getIparapheurHistoriqueFile(
                    $structure->get('id_orchestration'),
                    $pastellDocumentId
                );
                $documentName = self::IPARAPHEUR_HISTORIQUE . 'xml';
                break;
            default:
                throw new Exception('not existing type');
        }

        $temp = tmpfile();
        fwrite($temp, $res->getBody()->getContents());
        $path = stream_get_meta_data($temp)['uri'];

        $this->autoRender = false;
        $response = $this->response->withFile(
            $path,
            ['download' => true, 'name' => $documentName /*, 'type'=> $res->getHeader('Content-Type')[0]*/]
        );

        fclose($temp);

        return $response;
    }

    /**
     * Get the signed Document
     *
     * @param int $projectId project id
     * @return \Cake\Http\Response|null
     * @throws \Exception
     */
    public function getSignedDocument($projectId)
    {
        return $this->getSignDocuments($projectId, self::IPARAPHEUR_ACTE_SIGNE);
    }

    /**
     * Get the signed Bordereau
     *
     * @param int $projectId project id
     * @return \Cake\Http\Response|null
     * @throws \Exception
     */
    public function getSignBordereau($projectId)
    {
        return $this->getSignDocuments($projectId, self::IPARAPHEUR_BORDEREAU);
    }

    /**
     * Get the signed Histo
     *
     * @param int $projectId project id
     * @return \Cake\Http\Response|null
     * @throws \Exception
     */
    public function getIparapheurHistoriqueFile($projectId)
    {
        return $this->getSignDocuments($projectId, self::IPARAPHEUR_HISTORIQUE);
    }

    /**
     * @param int $projectId project id
     * @param int $structureId structure id
     * @param array $sousTypeParapheur sous-type parapheur
     * @return void
     */
    private function sendMessageParapheur(int $projectId, int $structureId, array $sousTypeParapheur)
    {
        $beanstalkSignature = new Beanstalk('signature-pastell');

        /**
         * @var \App\Controller\ContainersTable $Containers
         */
        $Containers = $this->fetchTable('Containers');
        $containerId = $Containers->find()
            ->select(['id'])
            ->where(['project_id' => $projectId])
            ->firstOrFail()->get('id');

        $data = [
            'flux' => Configure::read('Flux.actes-generique.name'),
            'flux_name' => Configure::read('Flux.actes-generique.name'),
            'controller' => 'projects',
            'structure_id' => $structureId,
            'object_id' => $containerId,
            'action' => 'sendParapheur',
            'sous-type' => $sousTypeParapheur['iparapheur_circuit_name'],
            'user_id' => $this->Multico->getCurrentUserId(),
        ];

        $responseId = $beanstalkSignature->emit($data, Configure::read('Beanstalk.Priority.VERY_HIGHT'));

        $this->set(compact('responseId'));
    }
}
