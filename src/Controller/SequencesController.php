<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Sequence;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Sequences Controller
 *
 * @property \App\Model\Table\SequencesTable $Sequences
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\Sequence[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SequencesController extends AppController
{
    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryUtils');
    }

    /**
     * Index method
     *
     * @return void
     * @created 17/12/2020
     * @version V2
     */
    public function index()
    {
        $queryParams = $this->getRequest()->getQueryParams();

        $this->paginate = [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            'limit' => $queryParams['limit'] ?? MAXIMUM_LIMIT,
            'order' => ['name' => 'asc'],
            'maxLimit' => MAXIMUM_LIMIT,
            'conditions' => [
                'OR' => [
                    isset($queryParams['name'])
                        ? $this->QueryUtils->getIlikeQueryExpression('Sequences.name', "%{$queryParams['name']}%")
                        : '',
                ],
            ],
        ];
        $sequences = $this->paginate($this->Sequences);

        $this->set(compact('sequences'));
    }

    /**
     * View method
     *
     * @param string|null $id Sequence id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @created 17/12/2020
     * @version V2
     */
    public function view($id = null)
    {
        $sequence = $this->Sequences->get($id, ['limitToStructure' => $this->Multico->getCurrentStructureId()]);
        $this->set('sequence', $sequence);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     * @created 17/12/2020
     * @version V2
     */
    public function add()
    {
        $this->request->allowMethod('post');

        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can add a sequence');
        }

        $data = $this->getRequest()->getData();
        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();

        $sequence = $this->Sequences->newEntity($data);

        if ($this->Sequences->save($sequence, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set(compact('sequence'));
        } else {
            $this->set('errors', $sequence->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Sequence id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @created 17/12/2020
     * @version V2
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['patch', 'put']);

        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can edit a sequence');
        }

        $sequence = $this->Sequences->get($id, [
            'contain' => [],
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
        ]);

        $data = $this->getRequest()->getData();

        $sequence = $this->Sequences->patchEntity($sequence, $data);

        if ($this->request->is('patch')) {
            $this->editPatch($sequence);

            return;
        }

        if ($this->request->is('put')) {
            $this->editPut($sequence);

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(400));
    }

    /**
     * EditPatch method
     *
     * @param \App\Model\Entity\Sequence $sequence sequence
     * @return void
     * @access private
     * @created 17/12/2020
     * @version V2
     */
    private function editPatch(Sequence $sequence)
    {
        if ($this->Sequences->save($sequence, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set(['errors' => $sequence->getErrors()]);
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * EditPut method
     *
     * @param \App\Model\Entity\Sequence $sequence sequence
     * @return void
     * @access private
     * @created 17/12/2020
     * @version V2
     */
    private function editPut(Sequence $sequence)
    {
        if ($this->Sequences->save($sequence, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set('sequence', $sequence);
        } else {
            $this->set(['errors' => $sequence->getErrors()]);
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Sequence id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @created 17/12/2020
     * @version V2
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);

        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can delete a sequence');
        }

        $sequence = $this->Sequences->get($id, [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
        ]);

        if ($this->Sequences->delete($sequence)) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set('errors', $sequence->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Returns not used sequences by counters
     *
     * @return void
     */
    public function availableSequences()
    {
        $queryParams = $this->getRequest()->getQueryParams();

        $this->paginate = [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            'limit' => $queryParams['limit'] ?? PAGINATION_LIMIT,
            'maxLimit' => MAXIMUM_LIMIT,
        ];

        $sequences = $this->paginate($this->Sequences->find('availableSequences'));

        $this->set(compact('sequences'));
    }
}
