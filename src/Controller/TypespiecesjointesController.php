<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Typespiecesjointes Controller
 *
 * @property \App\Model\Table\TypespiecesjointesTable $Typespiecesjointes
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Model\Entity\Typespiecesjointe[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TypespiecesjointesController extends AppController
{
    /**
     * get the Typespiecesjointes with nature natureId
     *
     * @param  int $natureId nature_id
     * @return void
     */
    public function getAssociatedType($natureId)
    {
        $typespiecesjointes = $this->Typespiecesjointes->find()
            ->where(['nature_id' => $natureId])
            ->order('codetype')
            ->toArray();

        $this->set(compact('typespiecesjointes'));
    }
}
