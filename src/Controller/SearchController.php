<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Utility\Hash;

/**
 * Search Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 * @property \App\Model\Table\TypesactsTable $TypesActs
 * @property \App\Model\Table\StateactsTable $StateActs
 * @property \App\Model\Table\ActsTable $Acts
 * @property \App\Model\Table\ThemesTable $Themes
 * @property \App\Model\Table\TypesittingsTable $TypeSittings
 * @property \App\Model\Table\SittingsTable $Sittings
 * @property \App\Model\Table\ContainersTable $Containers
 * @property \App\Model\Table\ContainersStateactsTable $ContainersStateacts
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\Search[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SearchController extends AppController
{
    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryUtils');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function add()
    {
        $this->request->allowMethod('post');

        $data = $this->getRequest()->getData();

        $code_act = [];
        if (isset($data['code_act'])) {
            if (str_contains($data['code_act'], ',')) {
                $codes = explode(',', mb_strtolower($data['code_act'], 'UTF-8'));
                $code_act = ['LOWER(Projects.code_act) IN' => $codes];
            } else {
                $code_act = $this->QueryUtils->getIlikeQueryExpression('Projects.code_act', "%{$data['code_act']}%");
            }
        }

        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();

        $projectsTable = $this->fetchTable('Projects');

        $this->paginate = [
            'maxLimit' => isset($data['limit'])
            && ($data['limit'] <= PAGINATION_LIMIT) ? $data['limit'] : PAGINATION_LIMIT,
            'order' => [
                'Projects.id' => 'desc',
            ],
        ];

        $projects = $projectsTable->find()
            ->select([
                'Projects.id',
                'Projects.name',
                'Projects.code_act',
                'Projects.structure_id',
                'Themes.name',
                'Containers.id',
                'Typesacts.id',
                'Typesacts.name',
                'Typesacts.nature_id',
            ])
            ->leftJoin(
                ['Themes' => 'themes'],
                ['Themes.id = Projects.theme_id']
            )
            ->innerJoin(
                ['Containers' => 'containers'],
                ['Projects.id = Containers.project_id']
            )
            ->leftJoin(['Typesacts' => 'typesacts'], [
                'Typesacts.id = Containers.typesact_id',
            ])
            ->where([
                'AND' => [
                    isset($data['project_name'])
                        ? $this->QueryUtils->getIlikeQueryExpression('Projects.name', "%{$data['project_name']}%")
                        : '',
                    isset($data['code_act'])
                        ? $code_act
                        : '',
                    isset($data['typeacts']) && is_array($data['typeacts']) && count($data['typeacts']) > 0 ?
                        ['Typesacts.id IN' => Hash::extract($data['typeacts'], '{n}.id')] : '',
                ],
                'Projects.id >=' => 1,
            ]);

        if (!$this->Multico->isSuperAdminInCurrentOrganization()) {
            $projects->where([
                'Projects.structure_id' => $this->Multico->getCurrentStructureId(),
            ]);
        }

        $projectsList = $this->paginate($projects);

        if ($projectsList) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set(compact('projectsList'));
        }
    }
}
