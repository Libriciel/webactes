<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Sitting;
use App\Validation\Validator;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\Query;
use Cake\Utility\Hash;

/**
 * Sittings Controller
 *
 * @property \App\Model\Table\SittingsTable $Sittings
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 */
class SittingsController extends AppController
{
    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryParams');
        $this->loadComponent('QueryUtils');
    }

    /**
     * Récupération de la liste des séances, triée par date de séance descendante.
     *
     * Paramètres GET acceptés:
     * - format
     *   - list: pour affichage dans un select (sans pagination)
     *   - full ou non spécifié: pour affichage dans un tableau (avec pagination)
     * - statesitting
     *   - ouverture_seance: pour des séances ouvertes
     *   - seance_close: pour des séances fermées
     *
     * @return void
     */
    public function index()
    {
        $defaults = ['format' => 'full', 'statesitting' => null];
        $queryParams = $this->getRequest()->getQueryParams() + $defaults;
        $params = $this->QueryParams->validate(
            (new Validator())
                ->allowPresence('format')
                ->inList('format', ['full', 'list'])
                ->requirePresence('statesitting')
                ->inList('statesitting', ['ouverture_seance', 'seance_close'])
                ->allowPresence('typesitting_name')
                ->allowPagination($queryParams['format'] === 'full'),
            $defaults
        );

        $query = $this->Sittings
            ->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()])
            ->matching('Statesittings')
            ->where([
                'SittingsStatesittings.id' => $this->Sittings->SittingsStatesittings
                    ->getLastBySittingsIdSubquery(),
                'Statesittings.code' => $params['statesitting'],
            ]);

        if ($params['format'] === 'list') {
            $sittings = $query
                ->select([
                    'Sittings.id',
                    'Sittings.date',
                    'Sittings.date_convocation',
                    'Sittings.typesitting_id',
                    'Sittings.president_id',
                    'Sittings.secretary_id',
                    'Sittings.place',
                    'Typesittings.id',
                    'Typesittings.name',
                    'Typesittings.isdeliberating',
                    'Typesittings.active',
                ])
                ->contain([
                    'Typesittings',
                    'Typesittings.Typesacts' => [
                        'fields' => ['id'],
                    ],
                ])
                ->orderDesc('Sittings.date')
                ->enableHydration(false)
                ->all();
        } else {
            $query->select([
                'Sittings.id',
                'Sittings.date',
                'Sittings.date_convocation',
                'Sittings.typesitting_id',
                'Sittings.president_id',
                'Sittings.secretary_id',
                'Sittings.place',
                'Sittings__has_projects' => $query->expr()->exists(
                    $this->Sittings->ContainersSittings->getBySittingsIdSubquery()
                ),
                'Typesittings.id',
                'Typesittings.name',
                'Typesittings.isdeliberating',
                'Typesittings.active',
                'Typesittings.generate_template_convocation_id',
                'Typesittings.generate_template_executive_summary_id',
                'Typesittings.generate_template_deliberations_list_id',
                'Typesittings.generate_template_verbal_trial_id',
            ])
                ->contain([
                    'Generationerrors' => [
                        'fields' => [
                            'Generationerrors.type',
                            'Generationerrors.foreign_key',
                            'Generationerrors.message',
                            'Generationerrors.modified',
                        ],
                    ],
                    'Typesittings',
                    'Typesittings.Typesacts' => [
                        'fields' => [
                            'Typesacts.id',
                            'Typesacts.name',
                            'Typesacts.istdt',
                            'Typesacts.isdeliberating',
                            'Typesacts.active',
                            'Typesacts.nature_id',
                            'Typesacts.generate_template_project_id',
                            'Typesacts.generate_template_act_id',
                            'Typesacts.isdefault',
                            'Typesacts.counter_id',
                        ],
                    ],
                ]);
            $this->paginate = [
                'order' => ['Sittings.date' => 'desc'],
                'conditions' => [
                    'OR' => [
                        isset($params['typesitting_name']) && !empty($params['typesitting_name'])
                            ? $this->QueryUtils->getIlikeQueryExpression(
                                'Typesittings.name',
                                "%{$params['typesitting_name']}%"
                            )
                            : '',
                    ],
                ],
            ];
            $sittings = $this->paginate($query);
        }
        $this->set(compact('sittings'));
    }

    /**
     * Function permettant d'afficher les informations sur une séance
     *
     * @param string $id Sitting id.
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function view(string $id)
    {
        // à priori, ce n'est jamais appelé sans paramètre (avec le format full)
        $params = $this->QueryParams->validate(
            (new Validator())
                ->allowPresence('format')
                ->inList('format', ['full', 'short']),
            ['format' => 'full']
        );

        if ($params['format'] === 'short') {
            $contain = [
                'Generationerrors' => [
                    'fields' => [
                        'Generationerrors.type',
                        'Generationerrors.foreign_key',
                        'Generationerrors.message',
                        'Generationerrors.modified',
                    ],
                ],
                'Typesittings',
            ];
        } else {
            $contain = [
                'Generationerrors' => [
                    'fields' => [
                        'Generationerrors.type',
                        'Generationerrors.foreign_key',
                        'Generationerrors.message',
                        'Generationerrors.modified',
                    ],
                ],
                'Typesittings',
                'Containers',
                'Containers.ContainersSittings',
                'Containers.Projects',
                'Containers.Stateacts',
                'Statesittings' => [
                    'queryBuilder' => function (Query $query) {
                        return $query->order(['SittingsStatesittings.id' => 'ASC']);
                    },
                ],
                'ActorsProjectsSittings',
                'ActorsProjectsSittings.Actors',
                'ActorsProjectsSittings.Representatives',
                'Typesittings.ActorGroups',
                'Typesittings.ActorGroups.Actors',
            ];
        }
        $sitting = $this->Sittings->get(
            $id,
            [
                'contain' => $contain,
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $this->set(compact('sitting'));
    }

    /**
     * Function permettant d'afficher les informations sur une séance
     *
     * @param string $id Sitting id.
     * @param string $projectId project Id
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function viewWithVotes(string $id, string $projectId)
    {
        $this->getRequest()->allowMethod('get');
        $project = $this->fetchTable('Projects')->get($projectId);

        //@TODO Faire un finder
        $sitting = $this->Sittings->get(
            $id,
            [
                'contain' => [
                    'Typesittings',
                    'Containers' => isset($projectId) ? function ($q) use ($projectId) {
                        return $q
                            ->where(['Containers.project_id' => $projectId]);
                    } : [],
                    'Containers.Projects.Votes' => isset($projectId) && $project->isVoted['value'] ?
                        function ($q) use ($projectId) {
                            return $q
                                ->where([
                                    'Votes.id' => $this->fetchTable('Votes')
                                        ->find()
                                        ->where(['project_id' => $projectId])
                                        ->all()
                                        ->max('id')['id'],
                                ]);
                        } : [],
                    'Containers.Projects.Votes.ActorsVotes' => isset($projectId) && $project->isVoted['value'] ?
                        function ($q) use ($projectId) {
                            return $q
                                ->where([
                                    'ActorsVotes.vote_id' => $this->fetchTable('Votes')
                                        ->find()
                                        ->where(['project_id' => $projectId])
                                        ->all()
                                        ->max('id')['id'],
                                ]);
                        } : [],
                    'Statesittings',
                    'ActorsProjectsSittings.Actors' => isset($projectId) ? function ($q) use ($projectId) {
                        return $q
                            ->where(['Actors.active' => true])
                            ->order(['Actors.rank' => 'ASC']);
                    } : [],
                    'ActorsProjectsSittings' => ['conditions' => ['ActorsProjectsSittings.project_id' => $projectId]],
                    'Typesittings.ActorGroups',
                    'Typesittings.ActorGroups.Actors',
                ],
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        if (empty($sitting['actors_projects_sittings'])) {
            $votersList = [];
            foreach ($sitting['typesitting']['actorGroups'] as $actorGroup) {
                foreach ($actorGroup['actors'] as $actor) {
                    $votersList[] = [
                        'actor_id' => $actor['id'],
                        'project_id' => $projectId,
                        'sitting_id' => $id,
                        'is_present' => true,
                    ];
                }
            }

            $sitting['actors_projects_sittings'] = $this->fetchTable('ActorsProjectsSittings')
                ->initializeAttendanceList(
                    $this->Multico->getCurrentStructureId(),
                    (int)$id,
                    (int)$projectId,
                    $votersList
                );
        }

        $sitting->actors_projects_sittings_previous = $this->fetchTable('ActorsProjectsSittings')
            ->getPrevious((int)$id, (int)$projectId)
            ->enableHydration(false)
            ->all();

        $this->set(compact('sitting'));
    }

    /**
     * Function permettant de créer une séance
     *
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function add()
    {
        if ($this->getRequest()->is('post')) {
            $data = $this->getRequest()->getData();

            if (!isset($data['date'])) {
                $this->setResponse($this->getResponse()->withStatus(400));
                $this->set('errors', 'missing date');

                return;
            }

            $data['date'] = date('Y-m-d H:i:s', strtotime($data['date']));
            if (isset($data['date_convocation'])) {
                $data['date_convocation'] = date('Y-m-d H:i:s', strtotime($data['date_convocation']));
            }
            $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();

            $conn = $this->Sittings->getConnection();
            $conn->begin();
            $success = true;

            $sitting = $this->Sittings->newEntity($data);

            $success = $success
                && $this->Sittings->save(
                    $sitting,
                    ['limitToStructure' => $this->Multico->getCurrentStructureId(),
                        'atomic' => false]
                ) !== false;

            if ($success === true) {
                $tableLocator = new TableLocator();
                $SittingsStatesittings = $tableLocator->get('SittingsStatesittings');

                $data = [
                    'sitting_id' => $sitting->id,
                    'statesitting_id' => ETAT_SITTING_OPEN,
                ];

                $entity = $SittingsStatesittings->newEntity($data);
                $success = $success && $SittingsStatesittings->save($entity, ['atomic' => false]) !== false;
            }

            if ($success === true) {
                $conn->commit();
                $this->setResponse($this->getResponse()->withStatus(201));
                $this->view((string)$sitting->id);

                return;
            } else {
                $conn->rollback();
            }
        }
        $this->setResponse($this->getResponse()->withStatus(400));
        $this->set('errors', $sitting->getErrors());
    }

    /**
     * Function permettant de modifier une séance
     *
     * @param int $id Sitting id.
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function edit($id)
    {
        $sitting = $this->Sittings->get(
            $id,
            [
                'contain' => ['Containers'],
                ['limitToStructure' => $this->Multico->getCurrentStructureId()],
            ]
        );
        $data = $this->getRequest()->getData();

        if (isset($data['date'])) {
            $data['date'] = date('Y-m-d H:i:s', strtotime($data['date']));
        }

        if (isset($data['date_convocation'])) {
            $data['date_convocation'] = date('Y-m-d H:i:s', strtotime($data['date_convocation']));
        }

        $this->request->allowMethod(['patch', 'put']);

        $sitting = $this->Sittings->patchEntity($sitting, $data);

        if ($this->getRequest()->is('patch')) {
            $this->editPatch($sitting);

            return;
        }

        if ($this->getRequest()->is('put')) {
            $this->editPut($sitting);

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(400));
    }

    /**
     * Fonction permettant l'enregistrement en mode PATCH
     *
     * @param \App\Model\Entity\Sitting $sitting sitting
     * @return void
     * @access  private
     * @created 08/02/2019
     * @version V0.0.9
     */
    private function editPatch(Sitting $sitting)
    {
        if ($this->Sittings->save($sitting, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->Sittings->updateSittingInGeneratedActDocument($sitting['id']);

            $this->setResponse($this->getResponse()->withStatus(204));

            return;
        }
        $this->set(['errors' => $sitting->getErrors()]);
        $this->setResponse($this->getResponse()->withStatus(400));
    }

    /**
     * Fonction permettant l'enregistrement en mode PUT
     *
     * @param \App\Model\Entity\Sitting $sitting sitting
     * @return void
     * @access  private
     * @created 08/02/2019
     * @version V0.0.9
     */
    private function editPut(Sitting $sitting)
    {
        if ($this->Sittings->save($sitting, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->Sittings->updateSittingInGeneratedActDocument($sitting['id']);

            $this->setResponse($this->getResponse()->withStatus(200));
            $this->view((string)$sitting->id);

            return;
        }

        $this->set(['errors' => $sitting->getErrors()]);
        $this->setResponse($this->getResponse()->withStatus(400));
    }

    /**
     * Fonction permettant de supprimer une seance avec le mode DELETE
     *
     * @param int $id Sitting id.
     * @return void
     * @access  public
     * @created 12/02/2019
     * @version V0.0.9
     */
    public function delete($id)
    {
        $this->request->allowMethod(['delete']);

        $sitting = $this->Sittings->get($id, [
            'fields' => ['id'],
            'limitToStructure' => $this->Multico->getCurrentStructureId()]);

        if (!$this->Sittings->delete($sitting)) {
            $this->setResponse($this->getResponse()->withStatus(400));

            return;
        }

        $this->Sittings->updateSittingInGeneratedActDocument((int)$id);
        $this->setResponse($this->getResponse()->withStatus(204));
    }

    /**
     * Fonction permettant de cloturer un seance
     *
     * @return void
     * @access  public
     * @created 23/04/2019
     * @version V0.0.9
     */
    public function close()
    {
        if ($this->request->is('put')) {
            $sittingsCloseId = $this->request->getData();

            if (is_array($sittingsCloseId) && !empty($sittingsCloseId)) {
                // On s'assure que toutes les séances appartiennent à notre structure
                $query = $this->Sittings->find('all')
                    ->where(
                        ['Sittings.id IN' => $sittingsCloseId,
                            'Sittings.structure_id' => $this->Multico->getCurrentStructureId()]
                    );
                $nbSittings = $query->count();

                if (count($sittingsCloseId) === $nbSittings) {
                    $tableLocator = new TableLocator();
                    $SittingsStatesittings = $tableLocator->get('SittingsStatesittings');

                    $conn = $SittingsStatesittings->getConnection();
                    $conn->begin();
                    $success = true;
                    $cannotBeClosedIds = [];

                    foreach ($sittingsCloseId as $sittingCloseId) {
                        $sittingCloseId = (int)$sittingCloseId;
                        $cannotBeClosedIds[$sittingCloseId] = $this->Sittings->isSittingCanBeClosed($sittingCloseId);
                        $success = $success && $cannotBeClosedIds[$sittingCloseId];
                        if ($success === true) {
                            $data = [
                                'sitting_id' => $sittingCloseId,
                                'statesitting_id' => ETAT_SITTING_CLOSE,
                            ];

                            $entity = $SittingsStatesittings->newEntity($data);
                            $success = $success && $SittingsStatesittings->save($entity, ['atomic' => false]) !== false;
                        }
                    }

                    if ($success === true) {
                        $conn->commit();
                        $this->setResponse($this->getResponse()->withStatus(200));

                        return;
                    } else {
                        $conn->rollback();
                        $count = count($cannotBeClosedIds);
                        $format = __n(
                            'Abandon: %d séance ne peut pas être clôturée',
                            'Abandon: %d séances ne peuvent pas être clôturées',
                            $count
                        );
                        $this->set('message', sprintf($format, $count));
                        $this->setResponse($this->getResponse()->withStatus(400));

                        return;
                    }
                }
            }
        }

        $this->setResponse($this->getResponse()->withStatus(404));
    }

    /**
     * DELETE /sittings?ids=1,2,3'
     *
     * @return void
     */
    public function deleteBatch()
    {
        $stringIds = $this->getRequest()->getQuery('ids');
        if (empty($stringIds)) {
            return;
        }
        $ids = explode(',', $stringIds);

        $sittings = $this->Sittings
            ->find()
            ->select(['id'])
            ->contain(['Containers' => ['fields' => ['project_id']]])
            ->where(
                [
                    'id IN' => (array)$ids,
                    'structure_id' => $this->Multico->getCurrentStructureId(),
                ]
            )
            ->all()
            ->toArray();

        $count = count($sittings);

        if (count($ids) !== $count) {
            throw new NotFoundException();
        }

        $allCanBeDeleted = !in_array(
            false,
            Hash::extract($sittings, '{n}.availableActions.can_be_deleted.value')
        );

        if (!$allCanBeDeleted) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('error', 'Au moins une séance contient un projet voté');

            return;
        }

        $projectsIds = Hash::extract($sittings, '{n}.containers.{n}.project_id');

        $connection = $this->Sittings->getConnection();
        $connection->begin();

        try {
            $res = $this->Sittings->deleteAll(
                ['id in ' => $ids,
                    'structure_id' => $this->Multico->getCurrentStructureId()]
            );
        } catch (\Throwable $throwable) {
            $res = false;
        }

        if ($res) {
            $connection->commit();

            /** @var \App\Model\Table\ProjectsTable $Projects */
            $Projects = $this->fetchTable('Projects');

            foreach (array_unique($projectsIds) as $projectId) {
                $Projects->generateProjectFiles($projectId);
            }

            $this->setResponse($this->getResponse()->withStatus(204));

            return;
        }

        $connection->rollback();
        $this->setResponse($this->getResponse()->withStatus(400));
        $this->set('error', 'Impossible de supprimer les séances');
    }
}
