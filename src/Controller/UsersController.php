<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Auth;
use App\Model\Entity\User;
use App\Model\Enum\RolesName;
use App\Validation\Validator;
use Cake\Datasource\EntityInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Utility\Hash;
use Exception;
use Throwable;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @property \App\Model\Table\StructuresUsersTable $StructuresUsers
 * @property \App\Model\Table\StructuresTable $Structures
 * @property \App\Model\Table\OrganizationsTable $Organizations
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\KeycloakComponent $Keycloak
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->Auth->allowedActions = [
            'loginStructure',
            'logout',
        ];
        $this->loadComponent('Keycloak');
        $this->loadComponent('QueryParams');
        $this->loadComponent('QueryUtils');
    }

    /**
     * Code commun à "Liste d'utilisateurs" et à "Liste d'utilisateurs des structures".
     *
     * @param bool $allOrganizationUsers Doit-on enlever la limite à la structure de l'utilisateur connecté ?
     * @return void
     * @throws \App\Exception\BadRequestGetParametersException
     */
    protected function paginateUsers(bool $allOrganizationUsers): void
    {
        $params = $this->QueryParams->validate(
            (new Validator())
                ->allowPresence('name')
                ->allowEmptyString('name')
                ->allowPagination(true),
            ['name' => null]
        );

        $query = $this->Users
            ->find('allUsersSuperAdminDisabledOrNot')
            ->select(
                [
                    'id',
                    'organization_id',
                    'active',
                    'civility',
                    'lastname',
                    'firstname',
                    'username',
                    'email',
                    'phone',
                ]
            )
            ->contain(['Structures', 'Roles'])
            ->innerJoinWith('Structures')
            ->innerJoinWith('Roles')
            ->order(['Users.lastname' => 'ASC']);

        if ($allOrganizationUsers === false) {
            $query->where(['StructuresUsers.structure_id' => $this->Multico->getCurrentStructureId()]);
        }

        $filters = [
            'Roles.name' => 'name',
            'Structures.business_name' => 'name',
            'Users.firstname' => 'name',
            'Users.lastname' => 'name',
            'Users.username' => 'name',
        ];

        $query = $this->QueryUtils->applyOrFilters($query, $filters, $params);

        $this->paginate = [
            'limit' => $params['limit'] ?? PAGINATION_LIMIT,
            'maxLimit' => MAXIMUM_LIMIT,
        ];

        $users = $this->paginate($query);

        if ($this->Multico->isSuperAdminInCurrentOrganization() === false) {
            $users = $users->map(function ($user) {
                // Si l'utilisateur est un administrateur, on lui enlève les droits de modification et de suppression sur les utilisateurs ayant le role super-administrateurs
                // En ajoutant des champs virtuels permettant de calculer les availableActions
                if ($user->roles[0]->name === RolesName::SUPER_ADMINISTRATOR) {
                    $user->editActionPermission = false;
                    $user->deleteActionPermission = false;
                }

                return $user;
            });
        }

        $this->set(compact('users'));
    }

    /**
     * Liste d'utilisateurs
     *
     * @return void
     * @throws \App\Exception\BadRequestGetParametersException
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function index(): void
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only an admin can access users');
        }

        $this->paginateUsers(false);
    }

    /**
     * Liste d'utilisateurs des structures
     *
     * @return void
     * @throws \App\Exception\BadRequestGetParametersException
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function getAllOrganizationUsers(): void
    {
        if (!$this->Multico->isSuperAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $this->paginateUsers(true);
    }

    /**
     * get All actives Users (restrict to current structure) without paginate
     *
     * @return void
     */
    public function getAllActiveUsers(): void
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }
        $structureId = $this->Multico->getCurrentStructureId();

        $StructuresUsers = $this->fetchTable('StructuresUsers');
        $subQuery = $StructuresUsers->find()
            ->select('StructuresUsers.user_id')
            ->where(['StructuresUsers.structure_id' => $structureId]);

        $users = $this->Users->find()
            ->select(
                [
                    'id',
                    'active',
                    'civility',
                    'lastname',
                    'firstname',
                    'username',
                    'email',
                ]
            )
            ->where(
                [
                    'Users.id IN' => $subQuery,
                    'Users.active' => true,
                ]
            )
            ->order(['Users.lastname' => 'ASC', 'Users.firstname' => 'ASC'])->toArray();

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param int $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @access public
     * @created 15/04/2019
     * @version V0.0.9
     */
    public function view($id): void
    {
        $user = $this->Users->get(
            $id,
            [
                'contain' => [
                    //                'Organizations',
                    'NotificationsUsers',
                ],
                'fields' => [
                    'id',
                    'organization_id',
                    'active',
                    'civility',
                    'lastname',
                    'firstname',
                    'username',
                    'email',
                    'phone',
                ],
            ]
        );

        $this->set('user', $user);
    }

    /**
     * get users from keycloak
     *
     * @return void
     */
    public function getKcUsers(): void
    {
        $bearer = $this->getRequest()->getHeaderLine('Authorization');
        $token = str_replace('bearer ', '', $bearer);
        $auth = new Auth($token);

        $res = $auth->getUsers();

        if ($res === null) {
            $this->set('error', 'cannot get list');
            $this->setResponse($this->getResponse()->withStatus(403));

            return;
        }
        $this->set('users', $auth->getUsers());
    }

    /**
     * Add method
     *
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     * @access public
     * @version V2.0.0
     */
    public function add(): void
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only an admin can add a user');
        }

        if ($this->request->is('post')) {
            $datas = $this->getRequest()->getData();

            $datas['organization_id'] = $this->Multico->getCurrentOrganizationId();
            $datas['active'] = true;
            $datas['data_security_policy'] = false;

            if ($this->Multico->isSuperAdminInCurrentOrganization()) {
                $datas += ['structure_id' => $datas['structure_id']];
                $datas['structures'] = [
                    ['id' => $datas['structure_id']],
                ];
            } else {
                $datas['structures'] = [
                    ['id' => $this->Multico->getCurrentStructureId()],
                ];
                $datas += ['structure_id' => $this->Multico->getCurrentStructureId()];
            }

            // récupère le nom du rôle
            $roleName = isset($datas['role']) ? $datas['role']['name'] : '';

            // récupère les rôles de la structure
            $structureRoles = $this->Multico->getStructureRoles((int)$datas['structure_id']);
            // récupère la liste des rôles en fonction de la structure du user
            foreach ($structureRoles as $role) {
                if ($role['name'] == $roleName) {
                    $datas['roles'] = [
                        [
                            'id' => $role['id'],
                            'name' => $role['name'],
                        ],
                    ];
                }
            }
            $user = $this->Users->newEntity(
                $datas,
                [
                    'associated' => ['Roles', 'Structures'],
                ]
            );

            if ($this->Multico->isSuperAdminInCurrentOrganization()) {
                $this->addUserSuperAdminRights($user);
            } else {
                $this->addUserAdminRights($user);
            }
        }
    }

    /**
     * Adds user with limit to structure
     *
     * @param \App\Controller\App\Model\Entity\User $user user
     * @return void
     */
    public function addUserAdminRights(User $user): void
    {
        $keycloackUser = !isset($user['keycloak_id']) ? $this->addUserToKeycloak($user) : $user;

        $user = $this->Users->newEntity($keycloackUser->toArray(), [
            'associated' => ['Roles', 'Structures'],
        ]);

        if ($this->Users->save($user)) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set(compact('user'));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set($user->getErrors());
        }
    }

    /**
     * Adds user without limit to structure
     *
     * @param \App\Controller\App\Model\Entity\User $user user
     * @return void
     */
    public function addUserSuperAdminRights(User $user): void
    {
        $keycloackUser = !isset($user['keycloak_id']) ? $this->addUserToKeycloak($user) : $user;

        $user = $this->Users->newEntity($keycloackUser->toArray());

        if ($this->Users->save($user)) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set(compact('user'));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set($user->getErrors());
        }
    }

    /**
     * @param \App\Controller\App\Model\Entity\User $user user
     * @return \Cake\Datasource\EntityInterface|void
     */
    protected function addUserToKeycloak(User $user): ?EntityInterface
    {
        try {
            $kcId = !isset($user['keycloak_id']) ? $this->Keycloak->createKeycloakUser($user) : $user;
            $data = [];
            $data['keycloak_id'] = $kcId;

            return $this->Users->patchEntity($user, $data);
        } catch (Throwable $exception) {
            $user->setError('keycloak_id', $exception->getMessage());
        }
    }

    /**
     * Edit method
     *
     * @param int $id User id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @access public
     * @created 15/04/2019
     * @version V0.0.9
     */
    public function edit($id = null): void
    {
        $this->request->allowMethod(['patch', 'put']);

        $user = $this->Users->get($id, ['contain' => ['Structures', 'Roles']]);

        if (
            !$this->Multico->isAdminInCurrentOrganization()
            || ( !$this->Multico->isSuperAdminInCurrentOrganization()
                && $user->roles[0]->name === RolesName::SUPER_ADMINISTRATOR)
        ) {
            throw new UnauthorizedException('You are not allowed to edit this user');
        }

        if ($this->request->is(['patch', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            !HAS_PROVISIONING ? $this->Keycloak->updateUser($user) : null;

            if ($this->Users->save($user)) {
                $this->setResponse($this->getResponse()->withStatus(200));
                $this->set('user', $user);
            } else {
                $this->setResponse($this->getResponse()->withStatus(400));
                $this->set(['errors' => $user->getErrors()]);
            }
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @access public
     * @created 15/04/2019
     * @version V0.0.9
     */
    public function delete(string $id): void
    {
        $this->request->allowMethod(['delete']);

        $user = $this->Users->get($id, ['contain' => ['Roles']]);

        if (
            !$this->Multico->isAdminInCurrentOrganization()
            || ( !$this->Multico->isSuperAdminInCurrentOrganization()
                && $user->roles[0]->name === RolesName::SUPER_ADMINISTRATOR)
        ) {
            throw new UnauthorizedException('Only a user with minimum admin role can delete a user');
        }

        $user = $this->Users->patchEntity($user, ['active' => false, 'deleted' => time()]);

        !HAS_PROVISIONING ? $this->Keycloak->updateUser($user) : null;

        if ($this->Users->save($user)) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set('errors', $user->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * set role to user
     *
     * @return void
     */
    public function setRole(): void
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }
        // tdo get this values in session
        $user_id = $this->Multico->getCurrentUserId();
        $structure_id = $this->Multico->getCurrentStructureId();

        $roleId = $this->request->getData('roleId');
        if (empty($roleId)) {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set(['error' => 'missing roleId']);

            return;
        }

        $this->Users->getConnection()->begin();
        $success = true;
        //checkCurrentRole
        $this->RolesUsers = $this->fetchTable('RolesUsers');

        $rolesUser = $this->RolesUsers->find()
            ->select(['RolesUsers.id'])
            ->where(['RolesUsers.user_id' => $user_id])
            ->contain(
                ['Roles' => [
                    'conditions' => ['Roles.structure_id' => $structure_id],
                ]]
            )->first();

        if (!empty($rolesUser)) {
            $success = $success && $this->RolesUsers->deleteAll(['id' => $rolesUser->id]);
        }

        $toSave = ['user_id' => $user_id, 'role_id' => $roleId];
        $rolesUser = $this->RolesUsers->newEmptyEntity();
        $rolesUser = $this->RolesUsers->patchEntity($rolesUser, $toSave);
        $success = $success && $this->RolesUsers->save($rolesUser);

        if (!$success) {
            $this->Users->getConnection()->rollback();
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('error', $rolesUser->getErrors());

            return;
        }
        $this->Users->getConnection()->commit();
        $this->setResponse($this->getResponse()->withStatus(200));
    }

    /**
     * get authorized structures
     *
     * @return void
     */
    public function getStructures(): void
    {
        $username = $this->getRequest()->getSession()->read('username');
        $user = $this->Users->find('all')
            ->where(['username' => $username])
            ->contain(['Structures'])
            ->toArray();

        $this->getRequest()->getSession()->write('user_id', $user[0]['id']);
        $structures = Hash::extract($user, '{n}.structures.{n}');

        $this->setResponse($this->getResponse()->withStatus(200));
        $this->set('structures', $structures);
    }

    /**
     * login in structure
     *
     * @return void
     */
    public function loginStructure(): void
    {
        if ($this->request->is('post')) {
            $username = $this->getRequest()->getSession()->read('username');

            // find user
            $Users = $this->fetchTable('Users');
            $user = $Users->find()
                ->select(['id', 'organization_id'])
                ->where(['username' => $username])
                ->first();

            if (empty($user)) {
                $msgid = 'cet utilisateur n\'existe pas dans la base de données %s';
                throw new BadRequestException(sprintf($msgid, $username));
            }

            $userId = $user->id;
            $this->getRequest()->getSession()->write('user_id', $userId);

            $userId = $this->getRequest()->getSession()->read('user_id');

            // @info: pas de multico ternum
            $RolesUsers = $this->fetchTable('RolesUsers');
            $rolesUser = $RolesUsers->find()
                ->where(['user_id' => $userId])
                ->toArray();
            if (count($rolesUser) != 1) {
                $msgid = 'nombre de roles_users !=1 pour %d';
                throw new Exception(sprintf($msgid, $userId));
            }

            $structureId = Hash::get($this->Users->Roles
                ->find()
                ->select(['Roles.structure_id'])
                ->where(
                    [
                        'Roles.id' => $rolesUser[0]['role_id'],
                    ]
                )
                ->disableHydration()
                ->firstOrFail(), 'structure_id');

            $this->Auth->logout();
            $this->Auth->setUser($rolesUser[0]); // C'est RoleUsers et non pas User

            $auth = $this->getRequest()->getSession()->read('Auth');
            $this->getRequest()->getSession()->write('structure_id', $structureId);
            $this->getRequest()->getSession()->write('organization_id', $user->organization_id);

            $this->getRequest()->getSession()->delete('Auth.Role');
            try {
                $role = $this->Users->Roles
                    ->find()
                    ->select(['Roles.id', 'Roles.structure_id', 'Roles.name'])
                    ->innerJoinWith('Users')
                    ->where(
                        [
                            'Roles.structure_id' => $structureId,
                            'Roles.active' => true,
                            'RolesUsers.user_id' => $userId,
                        ]
                    )
                    ->disableHydration()
                    ->firstOrFail();

                if (empty($role) === false) {
                    $this->getRequest()->getSession()->write('Auth.Role', $role);
                }
            } catch (Throwable $exception) {
                $this->log($exception->getMessage());
            }

            return;
        }
        $this->setResponse($this->getResponse()->withStatus(400));
    }

    /**
     * logout user
     *
     * @return void
     */
    public function logout(): void
    {
        $this->Auth->logout();
        $this->set('success', true);
    }

    /**
     * logout user
     *
     * @return void
     */
    public function notLogged(): void
    {
        throw new UnauthorizedException('not Logged', 401);
    }

    /**
     * get connected user info
     *
     * @return void
     */
    public function getInfo(): void
    {
        $structureId = $this->Multico->getCurrentStructureId();
        $userId = $this->Multico->getCurrentUserId();

        $Structures = $this->fetchTable('Structures');
        $structure = $Structures->get($structureId);

        $user = $this->Users->get($userId);

        $this->set('userName', $user->nom_complet_court);
        $this->set('structureName', $structure->business_name);
    }

    /**
     * Activates a user
     *
     * @param int $id user
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     * @access public
     * @version 2.0.0
     */
    public function activate(string $id): void
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can activate a user');
        }

        if ($id == $this->Multico->getCurrentUserId()) {
            throw new UnauthorizedException('An admin cannot deactivate himself');
        }

        $user = $this->Users->get($id);

        $user = $this->Users->patchEntity($user, ['active' => true]);

        !HAS_PROVISIONING ? $this->Keycloak->updateUser($user) : null;

        if ($this->Users->save($user)) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('user', $user);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $user->getErrors());
            $this->set('success', false);
        }
    }

    /**
     * Deactivates a user
     *
     * @param int $id user
     * @return true
     * @throws \Cake\Http\Exception\UnauthorizedException
     * @access public
     * @version 2.0.0
     */
    public function deactivate(string $id): void
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can deactivate a user');
        }

        $user = $this->Users->get($id);

        if ($id == $this->Multico->getCurrentUserId()) {
            throw new UnauthorizedException('An admin cannot deactivate himself');
        }

        $user = $this->Users->patchEntity($user, ['active' => false]);

        !HAS_PROVISIONING ? $this->Keycloak->updateUser($user) : null;

        if ($this->Users->save($user)) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('user', $user);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $user->getErrors());
            $this->set('success', false);
        }
    }

    /**
     * Finds all active users
     *
     * @return \Cake\Http\Response|void
     * @access public
     * @version 2.0.0
     */
    public function active(): void
    {
        if ($this->Multico->isSuperAdminInCurrentOrganization()) {
            $users = $this->Users->find('all')
                ->where(['active' => true, 'organization_id' => $this->Multico->getCurrentOrganizationId()]);
            $this->set('users', $this->paginate($users));
        } else {
            $query = $this->Users->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()]);
            $users = $query->find('')->where(['active' => true]);
            $this->set('users', $this->paginate($users));
        }
    }
}
