<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Natures Controller
 *
 * @property \App\Model\Table\NaturesTable $Natures
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @method \App\Controller\Nature[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NaturesController extends AppController
{
    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryParams');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $params = $this->QueryParams->validateIndexWithPaginationTrueFalse();

        $query = $this->Natures
            ->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()])
            ->select(['id', 'libelle', 'typeabrege'])
            ->orderAsc('Natures.libelle');

        if ($params['paginate'] === 'true') {
            $this->paginate = ['limit' => $params['limit'] ?? PAGINATION_LIMIT, 'maxLimit' => MAXIMUM_LIMIT];
            $natures = $this->paginate($query);
        } else {
            $natures = $query->all();
        }

        $this->set(compact('natures'));
    }

    /**
     * View method
     *
     * @param  string|null $id Nature id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $nature = $this->Natures->get(
            $id,
            [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $this->set('nature', $nature);
    }
}
