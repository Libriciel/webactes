<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Acts Controller
 *
 * @property \App\Model\Table\ActsTable $Acts
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Model\Entity\Act[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
        ];
        $acts = $this->paginate($this->Acts);

        $this->set(compact('acts'));
    }

    /**
     * View method
     *
     * @param  string|null $id Act id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $act = $this->Acts->get(
            $id,
            [
            'contain' => ['Organizations', 'Structures', 'templates', 'Actors', 'Containers'],
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $this->set('act', $act);
    }

    /**
     * Add method
     *
     * @return void
     */
    public function add()
    {
        $this->request->allowMethod('post');

        $act = $this->Acts->newEntity();
        $act = $this->Acts->patchEntity($act, $this->request->getData());
        if ($this->Acts->save($act, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('id', $act->id);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $act->getErrors());
        }
    }

    /**
     * Edit method
     *
     * @param  string|null $id Act id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['patch', 'put']);
        $structureId = $this->Multico->getCurrentStructureId();
        $act = $this->Acts->get(
            $id,
            [
            'contain' => ['Actors'],
            ['limitToStructure' => $structureId],
            ]
        );
        $act = $this->Acts->patchEntity($act, $this->request->getData());
        if ($this->Acts->save($act, ['limitToStructure' => $structureId])) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set('act', $act);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $act->getErrors());
        }
    }

    /**
     * Delete method
     *
     * @param  string|null $id Act id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod('delete');
        $act = $this->Acts->get(
            $id,
            [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );
        if ($this->Acts->delete($act)) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set('errors', $act->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Check if cde_act already exist
     *
     * @param  string $code code_act
     * @return void
     */
    public function checkCode($code)
    {
        $structure_id = $this->getRequest()->getSession()->read('structure_id');
        $res = $this->Acts->find()
            ->where(['Acts.code_act' => $code, 'Acts.structure_id' => $structure_id])
            ->first();

        if (!empty($res)) {
            $this->set('valid', false);
            $this->setResponse($this->getResponse()->withStatus(200));

            return;
        }

        $this->Projects = $this->fetchTable('Projects');

        $res = $this->Projects->find()
            ->where(['Projects.code_act' => $code, 'Projects.structure_id' => $structure_id])
            ->first();

        if (!empty($res)) {
            $this->set('valid', false);
            $this->setResponse($this->getResponse()->withStatus(200));

            return;
        }

        $this->set('valid', true);
        $this->setResponse($this->getResponse()->withStatus(200));
    }
}
