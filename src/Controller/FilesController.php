<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\File;
use Cake\Utility\Hash;

/**
 * Files Controller
 *
 * @property \App\Model\Table\FilesTable $Files
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Model\Entity\File[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FilesController extends AppController
{
    public const APPLICATIONJSON = 'application/json';

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->request->allowMethod('get');

        $this->paginate = [
            'contain' => ['Structures', 'Maindocuments', 'Annexes'],
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
        ];
        $files = $this->paginate($this->Files);

        $this->set(compact('files'));
    }

    /**
     * View method
     *
     * @param string|null $id File id.
     * @return \App\Controller\Reponse  Foo
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $file = $this->Files->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        return $this->response->withFile(
            $file['path'],
            ['download' => true, 'name' => $file['name'], 'type' => $file['mimetype']]
        );
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response
     */
    public function add()
    {
        $this->request->allowMethod('post');
        $file = false;
        $generateTemplateId = Hash::get($this->getRequest()->getData(), 'generate_template_id');
        if (!empty($generateTemplateId)) {
            $file = $this->addGenerateTemplateFile((int)$generateTemplateId);
        }
        $draftTemplateId = Hash::get($this->getRequest()->getData(), 'draft_template_id');
        if (!empty($draftTemplateId)) {
            $file = $this->addDraftTemplateFile((int)$draftTemplateId);
        }

        $attachmentSummonId = Hash::get($this->getRequest()->getData(), 'attachment_summon_id');
        if (!empty($attachmentSummonId)) {
            $file = $this->addAttachmentSummonFile((int)$attachmentSummonId);
        }

        if ($file) {
            $this->setResponse($this->getResponse()->withStatus(201));

            return $this->response
                ->withType(self::APPLICATIONJSON)
                ->withStringBody(json_encode(['file' => $file]));
        }

        $this->setResponse($this->getResponse()->withStatus(404));

        return $this->response
            ->withType(self::APPLICATIONJSON)
            ->withStringBody(json_encode(['errors' => $file->getErrors()]));
    }

    /**
     * Add method
     *
     * @param int $generateTemplateId Id generateTemplate
     * @return \App\Model\Entity\File
     */
    private function addGenerateTemplateFile(int $generateTemplateId): File
    {
        $data = $this->getRequest()->getData();
        $data['generate_template_id'] = $generateTemplateId;
        $data['generate_template_file'] = Hash::get($this->getRequest()->getData(), 'generate_template_file');
        $uploadedFileData = [
            'name' => $data['generate_template_file']->getClientFilename(),
            'tmp_name' => $data['generate_template_file']->getStream()->getMetadata('uri'),
            'path' => $data['generate_template_file']->getStream()->getMetadata('uri'),
            'type' => $data['generate_template_file']->getClientMediaType(),
            'size' => $data['generate_template_file']->getSize(),
        ];

        return $this->Files->saveInFs(
            $uploadedFileData,
            ['generate_template_id' => $generateTemplateId],
            DS . 'generate_template_id' . DS . $generateTemplateId,
            $data['structure_id'] ?? $this->Multico->getCurrentStructureId()
        );
    }

    /**
     * Add method
     *
     * @param int $draftTemplateId Foo
     * @return \App\Model\Entity\File
     */
    private function addDraftTemplateFile(int $draftTemplateId): File
    {
        $data = $this->getRequest()->getData();
        $data['draft_template_id'] = $draftTemplateId;
        $data['draft_template_file'] = Hash::get($this->getRequest()->getData(), 'draft_template_file');
        $uploadedFileData = [
            'name' => $data['draft_template_file']->getClientFilename(),
            'tmp_name' => $data['draft_template_file']->getStream()->getMetadata('uri'),
            'path' => $data['draft_template_file']->getStream()->getMetadata('uri'),
            'type' => $data['draft_template_file']->getClientMediaType(),
            'size' => $data['draft_template_file']->getSize(),
        ];

        return $this->Files->saveInFs(
            $uploadedFileData,
            ['draft_template_id' => $draftTemplateId],
            DS . 'draft_template_id' . DS . $draftTemplateId,
            $data['structure_id'] ?? $this->Multico->getCurrentStructureId()
        );
    }

    /**
     * Add method
     *
     * @param int $attachmentSummonId Foo
     * @return \App\Model\Entity\File
     */
    private function addAttachmentSummonFile(int $attachmentSummonId): File
    {
        $data = $this->getRequest()->getData();

        $existingFile =
            $this->Files
                ->find()
                ->select(['id', 'name', 'attachment_summon_id', 'path', 'size'])
                ->where(['attachment_summon_id' => $attachmentSummonId])
                ->first();

        if ($existingFile != null) {
            $this->Files->delete(
                $this->Files->get(
                    $existingFile['id'],
                    ['limitToStructure' => $this->Multico->getCurrentStructureId()]
                )
            );
            $this->setResponse($this->getResponse()->withStatus(204));
        }

        $uploadedFileData = [
            'name' => $data['attachment_summon_file']->getClientFilename(),
            'tmp_name' => $data['attachment_summon_file']->getStream()->getMetadata('uri'),
            'path' => $data['attachment_summon_file']->getStream()->getMetadata('uri'),
            'type' => $data['attachment_summon_file']->getClientMediaType(),
            'size' => $data['attachment_summon_file']->getSize(),
        ];

        return $this->Files->saveInFs(
            $uploadedFileData,
            ['attachment_summon_id' => $attachmentSummonId],
            DS . 'attachment_summon_id' . DS . $attachmentSummonId,
            $data['structure_id'] ?? $this->Multico->getCurrentStructureId()
        );
    }

    /**
     * Edit method
     *
     * @param string|null $id File id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['patch', 'post', 'put']);

        $structureId = $this->Multico->getCurrentStructureId();
        $file = $this->Files->patchEntity(
            $this->Files->get($id, ['limitToStructure' => $structureId]),
            $this->request->getData()
        );

        if ($this->Files->save($file, ['limitToStructure' => $structureId])) {
            $this->setResponse($this->getResponse()->withStatus(204));
//            $structures = $this->Files->Structures->find('list', ['limit' => 200]);
//            $maindocuments = $this->Files->Maindocuments->find('list', ['limit' => 200]);
//            $annexes = $this->Files->Annexes->find('list', ['limit' => 200]);
            // $this->set(compact('file', 'structures', 'maindocuments', 'annexes'));
        } else {
            $this->set(['errors' => $file->getErrors()]);
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id File id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod('delete');
        $file = $this->Files->get($id, ['limitToStructure' => $this->Multico->getCurrentStructureId()]);
        if ($this->Files->delete($file)) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set('errors', $file->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }
}
