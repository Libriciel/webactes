<?php
declare(strict_types=1);

namespace App\Controller;

use App\Exception\GenerationException;
use App\Model\Table\GenerationerrorsTable;
use App\Utilities\Common\ConfigureIniSettingsTrait;
use App\Validation\Validator;
use Beanstalk\Utility\Beanstalk;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Hash;
use Exception;
use Psr\Http\Message\RequestInterface;

/**
 * Summons Controller
 *
 * @property \App\Model\Table\SummonsTable $Summons
 * @property \App\Model\Table\ConvocationsTable $Convocations
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @method \App\Model\Entity\Summon[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SummonsController extends AppController
{
    use ConfigureIniSettingsTrait;

    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryParams');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $params = $this->QueryParams->validate(
            (new Validator())
                ->allowPresence('format')
                ->inList('format', ['full', 'short'])
                ->allowPresence('sitting_id')
                ->integer('sitting_id')
                ->allowPresence('summon_type_id')
                ->integer('summon_type_id')
                ->allowPagination(true),
            ['format' => 'full']
        );

        if ($params['format'] === 'short') {
            $contain = [
                'AttachmentsSummons',
                'Convocations.Actors',
            ];
        } else {
            $contain = [
                'Convocations',
                'Convocations.Actors',
                'AttachmentsSummons',
                'AttachmentsSummons.Files',
                'Generationerrors' => [
                    'fields' => [
                        'Generationerrors.type',
                        'Generationerrors.foreign_key',
                        'Generationerrors.message',
                        'Generationerrors.modified',
                    ],
                ],
            ];
        }

        $params = $this->getRequest()->getQueryParams();

        $this->paginate = [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            'maxLimit' => isset($params['limit'])
            && ($params['limit'] <= PAGINATION_LIMIT) ? $params['limit'] : PAGINATION_LIMIT,
            'contain' => $contain,
            'conditions' => [
                'AND' => [
                    isset($params['sitting_id'])
                        ? ['Summons.sitting_id' => $params['sitting_id']]
                        : [],
                    isset($params['summon_type_id'])
                        ? ['Summons.summon_type_id' => $params['summon_type_id']]
                        : [],
                ],
            ],
        ];

        $summon = $this->Summons->find('all')
            ->where(
                [
                    'Summons.sitting_id' => $params['sitting_id'] ?? null,
                    'Summons.structure_id' => $this->Multico->getCurrentStructureId(),
                    'Summons.summon_type_id' => $params['summon_type_id'] ?? 1,
                ]
            )
            ->contain(
                [
                    'Convocations',
                    'Convocations.Actors' => ['conditions' => ['active' => true]],
                    'AttachmentsSummons' => ['sort' => ['AttachmentsSummons.rank' => 'ASC']],
                    'AttachmentsSummons.Files',
                ]
            );

        if ($summon->all()->isEmpty()) {
            $summon = $this->Summons->newEntity(
                [
                    'structure_id' => $this->Multico->getCurrentStructureId(),
                    'limitToStructure' => $this->Multico->getCurrentStructureId(),
                    'sitting_id' => $params['sitting_id'] ?? null,
                    'summon_type_id' => 1,
                    'attachments_summons' => [
                        [
                            'rank' => 1,
                            'attachment' => [
                                'structure_id' => $this->Multico->getCurrentStructureId(),
                                'name' => 'convocation',
                            ],
                        ],
                        [
                            'rank' => 2,
                            'attachment' => [
                                'structure_id' => $this->Multico->getCurrentStructureId(),
                                'name' => 'executive_summary',
                            ],
                        ],
                    ],
                ],
                ['associated' =>
                    [
                        'AttachmentsSummons',
                        'AttachmentsSummons.Attachments',
                    ],
                ]
            );
            if (
                $this->Summons->save($summon, [
                    'associated' => ['AttachmentsSummons', 'AttachmentsSummons.Attachments'],
                    'limitToStructure' => $this->Multico->getCurrentStructureId()])
            ) {
                $summon = $this->Summons->find()
                    ->where(['id' => $summon->id])
                    ->contain(
                        [
                            'Convocations',
                            'Convocations.Actors' => ['conditions' => ['active' => true]],
                            'AttachmentsSummons' => ['sort' => ['AttachmentsSummons.rank' => 'ASC']],
                            'AttachmentsSummons.Files',
                            'Generationerrors' => [
                                'fields' => [
                                    'Generationerrors.type',
                                    'Generationerrors.foreign_key',
                                    'Generationerrors.message',
                                    'Generationerrors.modified',
                                ],
                            ],
                        ]
                    );

                $this->setResponse($this->getResponse()->withStatus(201));
            }
        } else {
            $summon = $this->paginate($summon);
        }

        return $this->getResponse()->withStringBody(json_encode($summon));
    }

    /**
     * View method
     *
     * @param string|null $id Summon id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $summon = $this->Summons->get($id, [
            'contain' => [
                'Convocations',
                'Convocations.Actors',
                'AttachmentsSummons',
                'AttachmentsSummons.Files',
                'Structures',
            ],
        ]);

        $this->set(compact('summon'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod('post');
        $data = json_decode((string)$this->request->getBody(), true);

        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();
        $data['limitToStructure'] = $this->Multico->getCurrentStructureId();

        $data = Hash::insert($data, 'attachments_summons.{n}.attachment.structure_id', $data['structure_id']);
        $summon = $this->Summons->newEntity($data, [
            'associated' => [
                'AttachmentsSummons',
                'AttachmentsSummons.Attachments',
            ],
        ]);

        if (
            $this->Summons->save($summon, [
                'associated' => ['AttachmentsSummons', 'AttachmentsSummons.Attachments'],
                'limitToStructure' => $this->Multico->getCurrentStructureId()])
        ) {
            $summon = $this->Summons->get($summon->id, [
                'contain' => ['Convocations', 'Convocations.Actors', 'AttachmentsSummons', 'AttachmentsSummons.Files'],
            ]);
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('summon', $summon);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $summon->getErrors());
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Summon id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['patch', 'put']);
        $structureId = $this->Multico->getCurrentStructureId();
        $summon = $this->Summons->patchEntity(
            $this->Summons->get($id, ['limitToStructure' => $structureId]),
            $this->request->getData(),
        );

        if (
            !$this->Summons->save(
                $summon,
                [
                    'associated' => ['AttachmentsSummons'],
                    'limitToStructure' => $structureId,
                ]
            )
        ) {
            $summon = $this->Summons->saveWithAttachments(
                (int)$id,
                $this->request->getData(),
                $structureId
            );
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $summon->getErrors());

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(200));
        $this->set('summon', $summon);
    }

    /**
     * Delete method
     *
     * @param string|null $id Summon id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $summon = $this->Summons->get($id);
        if ($this->Summons->delete($summon)) {
            $this->Flash->success(__('The summon has been deleted.'));
        } else {
            $this->Flash->error(__('The summon could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * @param string $id Foo
     * @return void Foo
     */
    public function send(string $id)
    {
        $this->request->allowMethod(['get']);
        $pastellFluxTypes = $this->fetchTable('Pastellfluxtypes');
        $pastellFluxTypeId = $pastellFluxTypes->find()
            ->select('id')
            ->where(
                [
                    'structure_id' => $this->Multico->getCurrentStructureId(),
                    'name' => 'mailsec',
                ]
            )->firstOrFail()->get('id');

        $beanstalk = new Beanstalk('mail-sec-pastell');
        $data = [
            'action' => 'sendMailSec',
            'flux' => 'mailSec',
            'summonId' => (int)$id,
            'pastellFluxId' => $pastellFluxTypeId,
            'currentUserId' => $this->Multico->getCurrentStructureId(),
        ];

        if ($beanstalk->emit($data)) {
            $this->setResponse($this->getResponse()->withStatus(200));

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(400));
    }

    /**
     * @param string $id Foo
     * @return void Foo
     */
    public function sendIdelibre(string $id)
    {
        $this->applyConfigureIniSettings(__METHOD__);

        if (function_exists('xdebug_disable')) {
            xdebug_disable();
        }
        $this->request->allowMethod(['get']);
        $id = (int)$id;

        $connecteurs = $this->fetchTable('Connecteurs');
        $connecteur = $connecteurs->find()
            ->where(
                [
                    'connector_type_id' => 2,
                    'structure_id' => $this->Multico->getCurrentStructureId(),
                ]
            )->firstOrFail();

        $idelibreInitInfo = [
            'username' => $connecteur->get('username'),
            'password' => $connecteur->get('password'),
            'url' => $connecteur->get('url'),
            'conn' => $connecteur->get('connexion_option'),
        ];

        $this->loadComponent('Idelibre', $idelibreInitInfo);

        try {
            $this->loadService('BuildJsonIdelibre', ['id' => $id]);

            $json = $this->BuildJsonIdelibre->getJson();
            $this->Idelibre->send($json);
            $this->BuildJsonIdelibre->deleteSend();

            GenerationerrorsTable::cleanup('convocation', $id);
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set('success', true);
        } catch (\InvalidArgumentException $exc) {
            GenerationerrorsTable::persist('convocation', $id, $exc->getMessage());
            throw new GenerationException('convocation', $id, $exc->getMessage());
        } catch (RecordNotFoundException $exc) {
            throw $exc;
        } catch (Exception $exc) {
            GenerationerrorsTable::persist('convocation', $id, $exc->getMessage());
            $this->setResponse($this->getResponse()->withStatus(400, $exc->getMessage()));
            $this->set('errors', $exc->getMessage());
        }
    }

    /**
     * Try to persist a fatal error exception due to a document generation.
     *
     * @param \Psr\Http\Message\RequestInterface $request The request leading to the fatal error
     * @param string $message The fatal error message
     * @return void
     */
    public static function persistFatalErrorMessage(RequestInterface $request, string $message): void
    {
        if ($request->getParam('action') === 'sendIdelibre') {
            GenerationerrorsTable::persist('convocation', (int)$request->getParam('pass')[0], $message);
        } else {
            parent::persistFatalErrorMessage($request, $message);
        }
    }
}
