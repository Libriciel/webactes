<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Logs Controller
 *
 * @method \App\Model\Entity|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LogsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->set('success', true);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $res = $this->log($this->request->getData(), 'error', 'clientErrors');
        if ($res) {
            $this->setResponse($this->getResponse()->withStatus(201));
        } else {
            $this->setResponse($this->getResponse()->withStatus(500));
        }
    }
}
