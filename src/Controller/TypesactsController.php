<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Typesact;
use App\Validation\Validator;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Utility\Hash;

/**
 * Typesacts Controller
 *
 * @property \App\Model\Table\TypesactsTable $Typesacts
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\Typesact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TypesactsController extends AppController
{
    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('QueryParams');
        $this->loadComponent('QueryUtils');
        $this->loadComponent(
            'ApiLogging',
            [
                'add' => [
                    'code' => 201,
                    'extra' => [
                        'response.id',
                    ],
                    'message' => [
                        'Ajout du type d\'acte "%s" (%d) par "%s" (%d)',
                        'request.data.name',
                        'response.id',
                        'session.keycloak_id',
                        'session.Auth.User.id',
                    ],
                ],
                'edit' => [
                    'code' => 200,
                    'extra' => [
                        'request.params.pass.0',
                    ],
                    'message' => [
                        'Modification du type d\'acte "%s" (%d) par "%s" (%d)',
                        'request.data.name',
                        'request.params.pass.0',
                        'session.keycloak_id',
                        'session.Auth.User.id',
                    ],
                ],
                'delete' => [
                    'code' => 204,
                    'extra' => [
                        'request.params.pass.0',
                    ],
                    'message' => [
                        'Suppression du type d\'acte "%s" (%d) par "%s" (%d)',
                        'request.data.name',
                        'request.params.pass.0',
                        'session.keycloak_id',
                        'session.Auth.User.id',
                    ],
                ],
            ]
        );
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->request->allowMethod('get');

        $params = $this->QueryParams->validateIndexWithPaginationTrueFalse(
            (new Validator())
                ->allowPresence('active')
                ->allowEmptyString('active')
                ->inList('active', ['true', 'false'])
                ->allowPresence('isdeliberating')
                ->allowEmptyString('isdeliberating')
                ->inList('isdeliberating', ['true', 'false'])
                ->allowPresence('name')
                ->allowEmptyString('name'),
            ['active' => null, 'isdeliberating' => null, 'name' => null],
        );

        $query = $this->Typesacts
            ->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()])
            ->select(
                [
                    'id',
                    'name',
                    'nature_id',
                    'active',
                    'istdt',
                    'isdefault',
                    'isdeliberating',
                    'counter_id',
                    'generate_template_project_id',
                    'generate_template_act_id',
                ]
            )
            ->contain(
                [
                    'Natures' => [
                        'fields' => ['id', 'libelle', 'typeabrege'],
                    ],
                    'DraftTemplates' => [
                        'fields' => ['id', 'name', 'draft_template_type_id'],
                    ],
                ]
            )
            ->where([
                isset($params['name'])
                    ? $this->QueryUtils->getIlikeQueryExpression('Typesacts.name', "%{$params['name']}%")
                    : '',
                isset($params['active']) && !empty($params['active'])
                    ? ['Typesacts.active' => $params['active'] === 'true' ? true : false]
                    : [],
                isset($params['isdeliberating']) && !empty($params['isdeliberating'])
                    ? ['Typesacts.isdeliberating' => $params['isdeliberating'] === 'true' ? true : false]
                    : [],
            ])
            ->orderAsc('Typesacts.name');

        if ($params['paginate'] === 'true') {
            $this->paginate = [
                'limit' => isset($params['limit']) && ($params['limit'] >= PAGINATION_LIMIT)
                    ? $params['limit']
                    : PAGINATION_LIMIT,
                'maxLimit' => MAXIMUM_LIMIT,
                'sortableFields' => [
                    'id',
                    'active',
                    'name',
                    'istdt',
                    'Natures.name',
                ],
            ];
            $typesacts = $this->paginate($query);
        } else {
            $typesacts = $query->all();
        }

        $typesacts = Hash::map($typesacts->toArray(), '{n}', function ($array) {
            $array['draftTemplates'] = $array['draft_templates'];
            unset($array['draft_templates']);

            return $array;
        });

        $this->set(compact('typesacts'));
    }

    /**
     * Add method
     *
     * @return void
     */
    public function add()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $this->request->allowMethod('post');
        $data = $this->request->getData();

        if (isset($data['draft_templates'])) {
            $data['draft_templates']['_ids'] = Hash::extract($data['draft_templates'], '{n}.id');
        }

        $typesact = $this->Typesacts->newEntity($data, ['associated' => ['DraftTemplates' => ['onlyIds' => true]]]);

        $typesact['structure_id'] = $this->Multico->getCurrentStructureId();

        if (
            $this->Typesacts->save($typesact, [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
                'validate' => 'generate'])
        ) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('id', $typesact->id);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $typesact->getErrors());
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Typesact id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $this->request->allowMethod('patch');

        $data = $this->request->getData();

        if (isset($data['draft_templates'])) {
            $data['draft_templates']['_ids'] = Hash::extract($data['draft_templates'], '{n}.id');
        }

        $typesact = $this->Typesacts->get(
            $id,
            ['contain' => ['DraftTemplates'],
                'limitToStructure' => $this->Multico->getCurrentStructureId()]
        );

        $typesact = $this->patchEntityAndValidation($typesact, $data);

        if (
            $this->Typesacts->save(
                $typesact,
                [
                    'limitToStructure' => $this->Multico->getCurrentStructureId(),
                ]
            )
        ) {
            $typesact['draftTemplates'] = $typesact['draft_templates'];
            unset($typesact['draft_templates']);

            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set(compact('typesact'));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $typesact->getErrors());
        }
    }

    /**
     * Delete method
     *
     * @param int $id Typesact id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $this->request->allowMethod('delete');

        $typesact = $this->Typesacts->get($id, ['limitToStructure' => $this->Multico->getCurrentStructureId()]);

        if ($this->Typesacts->delete($typesact)) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set('errors', $typesact->getErrors());
            $this->setResponse($this->getResponse()->withStatus(403));
        }
    }

    /**
     * @param \App\Model\Entity\Typesact $typesact typeact
     * @param array $data data
     * @return \App\Model\Entity\Typesact
     */
    public function patchEntityAndValidation(Typesact $typesact, array $data)
    {
        $isActGenerate = $this->fetchTable('StructureSettings')
            ->getValueByModelAndStructureId(
                'act',
                'act_generate',
                $this->Multico->getCurrentStructureId(),
            );
        $isProjectGenerate = $this->fetchTable('StructureSettings')
            ->getValueByModelAndStructureId(
                'project',
                'project_generate',
                $this->Multico->getCurrentStructureId(),
            );

        $isProjectWriting = $this->fetchTable('StructureSettings')
            ->getValueByModelAndStructureId(
                'project',
                'project_writing',
                $this->Multico->getCurrentStructureId(),
            );

        if ($isActGenerate) {
            $this->Typesacts->getValidator('default')
                ->requirePresence(
                    'generate_template_act_id',
                    'create',
                    'Le modèle de génération de l\'acte est obligatoire'
                )
                ->requirePresence(
                    'generate_template_act_id',
                    'update',
                    'Le modèle de génération de l\'acte est obligatoire'
                )
                ->add('generate_template_act_id', 'required', [
                    'rule' => 'notBlank',
                    'message' => 'Le modèle de génération de l\'acte est obligatoire',
                ]);
        }
        if ($isProjectGenerate) {
            $this->Typesacts->getValidator('default')
                ->requirePresence(
                    'generate_template_project_id',
                    'create',
                    'Le modèle de génération de projet est obligatoire'
                )
                ->requirePresence(
                    'generate_template_project_id',
                    'update',
                    'Le modèle de génération de projet est obligatoire'
                )
                ->add('generate_template_project_id', 'required', [
                    'rule' => 'notBlank',
                    'message' => 'Le modèle de génération de projet est obligatoire',
                ]);
        }

        if ($isProjectWriting) {
            $this->Typesacts->getValidator('default')
                ->requirePresence('draft_templates', 'create', 'Au moins un gabarit est obligatoire')
                ->requirePresence('draft_templates', 'update', 'Au moins un gabarit est obligatoire')
                ->add('draft_templates', 'required', [
                    'rule' => function () use ($data) {
                        return !empty($data['draft_templates']);
                    },
                    'message' => 'Au moins un gabarit est obligatoire',
                ]);
        }

        $typesact = $this->Typesacts->patchEntity(
            $typesact,
            $data,
            ['associated' =>
                [
                    'DraftTemplates' => [
                    ],
                ],
            ],
        );

        return $typesact;
    }
}
