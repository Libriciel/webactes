<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Counter;
use App\Validation\Validator;
use Cake\Http\Exception\UnauthorizedException;

/**
 * Counters Controller
 *
 * @property \App\Model\Table\CountersTable $Counters
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryParamsComponent $QueryParams
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\Counter[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CountersController extends AppController
{
    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryParams');
        $this->loadComponent('QueryUtils');
    }

    /**
     * Index method
     *
     * @return void
     * @created 17/12/2020
     * @version V2
     */
    public function index()
    {
        $params = $this->QueryParams->validateIndexWithPaginationTrueFalse(
            (new Validator())
                ->allowPresence('name')
                ->allowEmptyString('name'),
            ['name' => null]
        );

        $query = $this->Counters
            ->find('all', ['limitToStructure' => $this->Multico->getCurrentStructureId()])
            ->contain(['Sequences' => ['sort' => ['Sequences.name' => 'ASC']]])
            ->where(
                !empty($params['name'])
                    ? [
                    'OR' => [
                        $this->QueryUtils->getIlikeQueryExpression('Counters.name', "%{$params['name']}%"),
                    ],
                ]
                    : []
            )
            ->orderAsc('Counters.name');

        if ($params['paginate'] === 'true') {
            $this->paginate = ['limit' => $params['limit'] ?? PAGINATION_LIMIT, 'maxLimit' => MAXIMUM_LIMIT];
            $counters = $this->paginate($query);
        } else {
            $counters = $query->all();
        }

        $this->set(compact('counters'));
    }

    /**
     * View method
     *
     * @param string|null $id counter id.
     * @return \Cake\Http\Response|void
     * @created 17/12/2020
     * @version V2
     */
    public function view($id = null)
    {
        $counter = $this->Counters->get($id, [
            'contain' => ['Sequences'],
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
        ]);

        $this->set('counter', $counter);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     * @throws \Exception
     * @created 17/12/2020
     * @version V2
     */
    public function add()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }
        $this->request->allowMethod('post');

        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can add a counter');
        }

        $data = $this->getRequest()->getData();
        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();

        $counter = $this->Counters->newEntity($data);

        if ($this->Counters->save($counter, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set(compact('counter'));
        } else {
            $this->set('errors', $counter->getErrors());
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Counter id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @created 17/12/2020
     * @version V2
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['patch', 'put']);

        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only an admin can edit a counter');
        }

        $counter = $this->Counters->get($id, [
            'contain' => ['Sequences'],
            ['limitToStructure' => $this->Multico->getCurrentStructureId()],
        ]);

        $data = $this->getRequest()->getData();

        $counter = $this->Counters->patchEntity(
            $counter,
            $data,
            ['associated' => ['Sequences' => ['onlyIds' => true]]]
        );

        if ($this->request->is('patch')) {
            $this->editPatch($counter);

            return;
        }

        if ($this->request->is('put')) {
            $this->editPut($counter);

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(400));
    }

    /**
     * EditPatch method
     *
     * @param \App\Model\Entity\Counter $counter counter
     * @return void
     * @created 17/12/2020
     * @version V2
     */
    private function editPatch(Counter $counter)
    {
        if (
            $this->Counters->save(
                $counter,
                ['limitToStructure' => $this->Multico->getCurrentStructureId(),
                    'associated' => ['Sequences' => ['onlyIds' => 'true']]]
            )
        ) {
            $this->setResponse($this->getResponse()->withStatus(204));
        } else {
            $this->set(['errors' => $counter->getErrors()]);
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * EditPut method
     *
     * @param \App\Model\Entity\Counter $counter counter
     * @return void
     * @created 17/12/2020
     * @version V2
     */
    private function editPut(Counter &$counter)
    {
        if (
            $this->Counters->save(
                $counter,
                ['limitToStructure' => $this->Multico->getCurrentStructureId(),
                    'associated' => ['Sequences' => ['onlyIds' => 'true']]]
            )
        ) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set('counter', $counter);
        } else {
            $this->set(['errors' => $counter->getErrors()]);
            $this->setResponse($this->getResponse()->withStatus(400));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Counter id.
     * @return void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     * @created 17/12/2020
     * @version V2
     */
    public function delete($id = null)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can delete a counter');
        }

        if ($this->request->is('delete')) {
            $counter = $this->Counters->get($id, ['limitToStructure' => $this->Multico->getCurrentStructureId(),]);
            if ($this->Counters->delete($counter)) {
                $this->setResponse($this->getResponse()->withStatus(204));
            } else {
                $this->set('errors', $counter->getErrors());
                $this->setResponse($this->getResponse()->withStatus(400));
            }

            return;
        }

        $this->setResponse($this->getResponse()->withStatus(404));
    }
}
