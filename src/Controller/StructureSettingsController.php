<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\UnauthorizedException;

/**
 * StructureSettings Controller
 *
 * @property \App\Model\Table\StructureSettingsTable $StructureSettings
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Model\Entity\StructureSetting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StructureSettingsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

            $structureSetting = $this
            ->StructureSettings
            ->getSettingsByStructureId($this->Multico->getCurrentStructureId());

        $this->set(compact('structureSetting'));
    }

    /**
     * saveTypeBoolean method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function saveTypeBoolean()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $this->request->allowMethod('put');
        $data = json_decode((string)$this->request->getBody(), true);

        $structureSetting = $this->StructureSettings->find()
            ->where([
                'name' => $data['groupCode'],
                'structure_id' => $this->Multico->getCurrentStructureId()])
            ->first();

        if (!empty($structureSetting)) {
            //Group exist
            if (
                $this->StructureSettings->getValueByModelAndStructureId(
                    $data['groupCode'],
                    $data['keyCode'],
                    $this->Multico->getCurrentStructureId()
                )
                !== false
            ) {
                //value not exist
                $this->StructureSettings->patchEntity($structureSetting, [
                    'value' => array_merge($structureSetting->value, [$data['keyCode'] => $data['value']]),
                ]);
            } else {
                // value exist
                $structureSetting->value[$data['keyCode']] = $data['value'];
                $this->StructureSettings->patchEntity($structureSetting, [
                    'value' => $structureSetting->value,
                ]);
            }
        } else {
            $structureSetting = $this->StructureSettings->newEntity([
                'name' => $data['groupCode'],
                'value' => [$data['keyCode'] => $data['value']],
                'structure_id' => $this->Multico->getCurrentStructureId(),
            ]);
        }

        if ($this->StructureSettings->save($structureSetting)) {
            $this->setResponse($this->getResponse()->withStatus(201));

            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $structureSetting->getErrors());
        }
    }
}
