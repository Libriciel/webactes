<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * NotificationsUsers Controller
 *
 * @property \App\Model\Table\NotificationsUsersTable $NotificationsUsers
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Model\Entity\NotificationsUser[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NotificationsUsersController extends AppController
{
    public const PERSONAL_NOTIFICATIONS = 'Notifications qui me concernent';
    public const GLOBAL_NOTIFICATIONS = 'Autres notifications';

    /**
     * List of notifications for the current user
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'conditions' => [
                'user_id' => $this->Multico->getCurrentUserId(),
            ],
            'contain' => ['Notifications'],
        ];
        $userNotifications = $otherNotifications = [];
        $userNotifications['name'] = self::PERSONAL_NOTIFICATIONS;
        $userNotifications['values'] = $this->paginate($this->NotificationsUsers->find('personal'));
        $otherNotifications['name'] = self::GLOBAL_NOTIFICATIONS;
        $otherNotifications['values'] = $this->paginate($this->NotificationsUsers->find('global'));

        $this->set(compact('userNotifications'));
        $this->set(compact('otherNotifications'));
    }

    /**
     * View method
     *
     * @param  string|null $id Notifications User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $notificationsUser = $this->NotificationsUsers->get(
            $id,
            [
            ]
        );

        $this->set('notificationsUser', $notificationsUser);
    }

    /**
     * @param  int $id id of relationship between user and notification to activate
     * @return \Cake\Http\Response|void
     */
    public function activate(string $id)
    {
        $notification = $this
            ->NotificationsUsers
            ->get(
                $id,
                ['conditions' => ['user_id' => $this->Multico->getCurrentUserId()]]
            );

        $notification = $this->NotificationsUsers->patchEntity($notification, ['active' => true]);
        if ($this->NotificationsUsers->save($notification)) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('notification', $notification);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $notification->getErrors());
        }
    }

    /**
     * @param  int $id id of relationship between user and notification to deactivate
     * @return \Cake\Http\Response|void
     */
    public function deactivate(string $id)
    {
        $notification = $this
            ->NotificationsUsers
            ->get(
                $id,
                ['conditions' => ['user_id' => $this->Multico->getCurrentUserId()]]
            );

        $notification = $this->NotificationsUsers->patchEntity($notification, ['active' => false]);
        if ($this->NotificationsUsers->save($notification)) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('notification', $notification);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $notification->getErrors());
        }
    }
}
