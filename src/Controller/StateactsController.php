<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Stateacts Controller
 *
 * @property \App\Model\Table\StateactsTable $Stateacts
 * @method \App\Model\Entity\Stateact[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StateactsController extends AppController
{
    /**
     * Function permettant d'afficher la liste des états d'un acte
     *
     * @return \Cake\Http\Response|void
     * @access  public
     * @created 18/02/2019
     * @version V0.0.9
     */
    public function index()
    {
        $action = $this->getRequest()->getQuery('action');
        $id = $this->getRequest()->getQuery('id');

        if (isset($id)) {
            $project = $this->fetchTable('Projects')
                ->find()
                ->where(['Projects.id' => $id])
                ->contain(['Containers'])
                ->first();

            /**
             * @var \App\Controller\ContainersTable $Container
             */
            $Container = $this->fetchTable('Containers');
            $additionnaStateactId = $Container->getLastStateActEntry($project['containers'][0]->id)[0]->id;
        }

        $query = $this->Stateacts->find('all');
        if (!empty($action)) {
            $query->innerJoinWith('Authorizeactions')
                ->where(['OR' =>
                    [
                        'Authorizeactions.action' => $action,

                    isset($id) ? ['Stateacts.id' => $additionnaStateactId] : [],
                        ],
                ])->distinct('Stateacts.id');
        }

        $stateacts = $this->paginate($query);

        if ($stateacts->count() === 0) {
            $this->setResponse($this->getResponse()->withStatus(400));

            return;
        }

        $this->set(compact('stateacts'));
    }
}
