<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\BadRequestException;
use Cake\Utility\Hash;

/**
 * Votes Controller
 *
 * @property \App\Model\Table\VotesTable $Votes
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @method \App\Model\Entity\Vote[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VotesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Actors', 'Projects'],
        ];
        $votes = $this->paginate($this->Votes);

        $this->set(compact('votes'));
    }

    /**
     * GetAllForProjects method
     *
     * @param string|null $projectId id of the project.
     * @return \Cake\Http\Response|void
     */
    public function getAllProjectsVotes($projectId)
    {
        $this->request->getQuery('order');
        $this->paginate = [
            'conditions' => isset($projectId) ? ['project_id' => $projectId] : [],
        ];
        $votes = $this->paginate($this->Votes);

        $this->set(compact('votes'));
    }

    /**
     * View method
     *
     * @param string|null $id Vote id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $vote = $this->Votes->get($id, [
            'contain' => ['Projects'],
        ]);

        $this->set('vote', $vote);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->getRequest()->allowMethod('post');

        $vote = $this->vote();

        if (isset($vote['state']['state']['value'])) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set(compact('vote'));
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', 'Le projet n\'a pas pu être voté.');
        }
        $this->set(compact('vote'));
    }

    /**
     * Vote du projet
     *
     * @return array
     */
    private function vote(): array
    {
        $options = [];
        $options['userId'] = $this->Multico->getCurrentUserId();
        $options['structureId'] = $this->Multico->getCurrentStructureId();

        /**
         * @var \App\Controller\ActorsProjectsSittingsTable $ActorsProjectsSittings
         */
        $ActorsProjectsSittings = $this->fetchTable('ActorsProjectsSittings');
        $missingActors = $ActorsProjectsSittings->getMissingVoters(
            $options['structureId'],
            (int)$this->request->getData()['project_id']
        );

        $missingActorsIds = Hash::extract($missingActors, '{n}.actor_id');

        if (in_array($this->request->getData()['president_id'], $missingActorsIds)) {
            throw new BadRequestException('Le président de vote ne peut pas être un acteur déclaré absent.');
        }

        if (isset($this->request->getData()['actors_votes'])) {
            foreach ($this->request->getData()['actors_votes'] as $data) {
                if (isset($data['result']) && $data['result'] != null) {
                    $this->Votes->ActorsVotes->checkVoteValidity(
                        (int)$data['actor_id'],
                        (int)$this->request->getData()['project_id']
                    );
                }
            }
        }

        return $this->Votes->vote($this->request->getData(), $options);
    }

    /**
     * Edit method
     *
     * @param string|null $id Vote id.
     * @return \Cake\Http\Response|void Redirects on successful add, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->getRequest()->allowMethod('put');

        $vote = $this->Votes->get($id, [
            'contain' => [],
        ]);
        $vote = $this->Votes->newEntity($vote, $this->request->getData());
        if ($this->Votes->save($vote)) {
            $this->Flash->success(__('The vote has been saved.'));
        }
        $actors = $this->Votes->Actors->find('list', ['limit' => 200]);
        $projects = $this->Votes->Projects->find('list', ['limit' => 200]);
        $this->set(compact('vote', 'actors', 'projects'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Vote id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $vote = $this->Votes->get($id);
        if ($this->Votes->delete($vote)) {
            $this->Flash->success(__('The vote has been deleted.'));
        } else {
            $this->Flash->error(__('The vote could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
