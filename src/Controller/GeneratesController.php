<?php
declare(strict_types=1);

namespace App\Controller;

use App\Exception\GenerationException;
use App\Model\Table\GenerationerrorsTable;
use App\Utilities\Common\ConfigureIniSettingsTrait;
use Cake\Http\Response;
use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\TableRegistry;
use Psr\Http\Message\RequestInterface;

/**
 * Files Controller
 *
 * @property \App\Model\Table\FilesTable $Files
 * @property \App\Model\Table\AttachmentsTable $AttachmentsTable
 * @property \App\Model\Table\SummonsTable $Summons
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Service\GeneratesService $Generates
 * @property \App\Service\GenerateSummonsService $GenerateSummons
 * @method \App\Model\Entity\File[]|\App\Controller\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GeneratesController extends AppController
{
    use ConfigureIniSettingsTrait;
    use LocatorAwareTrait;

    public const APPLICATIONPDF = 'application/pdf';

    /**
     * @var \App\Service\GenerationsService
     */
    protected $Generations;

    /**
     * Pour chaque méthode publique de cette classe, liste la méthode de la classe GenerationsService à utiliser ainsi
     * que le type de document généré. Utilisé également pour la gestion des erreurs.
     *
     * Pour fileGenerateDocumentSummon, on "triche" en prétendant qu'il ne s'agit que de la convocation.
     *
     * @var \string[][]
     */
    protected static array $actions = [
        'fileGenerateAct' => ['method' => 'generateAct', 'type' => 'act'],
        'fileGenerateProject' => ['method' => 'generateProject', 'type' => 'project'],
        'fileGenerateDocumentSummon' => ['method' => 'generateSummon', 'type' => 'convocation'],
        'fileGenerateDeliberationsList' => ['method' => 'generateDeliberationsList', 'type' => 'deliberations_list'],
        'fileGenerateVerbalTrial' => ['method' => 'generateVerbalTrial', 'type' => 'verbal_trial'],
    ];

    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        if (function_exists('xdebug_disable')) {
            xdebug_disable();
        }
        parent::initialize();
        $this->Auth->allow();
    }

    /**
     * Try to persist a fatal error exception due to a document generation.
     *
     * @param \Psr\Http\Message\RequestInterface $request The request leading to the fatal error
     * @param string $message The fatal error message
     * @return void
     */
    public static function persistFatalErrorMessage(RequestInterface $request, string $message): void
    {
        $controller = $request->getParam('controller');
        $action = $request->getParam('action');
        $id = (int)$request->getParam('pass')[0];

        if (array_key_exists($action, static::$actions)) {
            $type = static::$actions[$action]['type'];
            GenerationerrorsTable::persist($type, $id, $message);
        } elseif ($action === 'fileGenerateDocumentSummon') {
            $sittingId = TableRegistry::getTableLocator()
                ->get('Summons')
                ->find()
                ->select(['sitting_id'])
                ->where(['id' => $id])
                ->enableHydration(false)
                ->first()['sitting_id'] ?? null;
            if (empty($sittingId) === false) {
                GenerationerrorsTable::persist('convocation', $sittingId, $message);
                GenerationerrorsTable::persist('executive_summary', $sittingId, $message);
            } else {
                $message = 'Error while persisting exception for %sController::%s.'
                    . ' Could not get sitting_id from table summons where id = %d. FatalErrorException message was: %s';
                Log::critical(sprintf($controller, $action, $id, $message));
            }
        } else {
            parent::persistFatalErrorMessage($request, $message);
        }
    }

    /**
     * Génération synchrone d'un document.
     *
     * @param string $primaryKey La clé primaire (projects.id, sittings.id)
     * @param string $type Le type de génération
     * @param string $method La méthode de la classe GenerationsService à utiliser
     * @param string $fileName Nom du fichier à générer par défaut vide
     * @return \Cake\Http\Response
     */
    protected function generate(string $primaryKey, string $type, string $method, $fileName = ''): Response
    {
        $primaryKey = (int)$primaryKey;
        try {
            $this->Generations = $this->loadService('Generations', [], false);
            $mimeType = self::APPLICATIONPDF;
            $content = $this->Generations->{$method}($primaryKey, $mimeType);

            if (empty($fileName)) {
                $fileName = $this->Generations->getDocumentFileName($type, $primaryKey);
            }
            $this->Generations = null;

            $response = $this->response
                ->withHeader('X-Custom-Filename', $fileName)
                ->withStringBody($content)
                ->withType($mimeType);
            GenerationerrorsTable::cleanup($type, $primaryKey);

            return $response;
        } catch (\InvalidArgumentException $exc) {
            GenerationerrorsTable::persist($type, $primaryKey, $exc->getMessage());

            return $this->response->withStatus($exc->getCode(), $exc->getMessage());
        }
    }

    /**
     * Génération synchrone du document d'acte.
     *
     * @param string $projectId Identifiant du projet (projects.id)
     * @return \Cake\Http\Response
     */
    public function fileGenerateAct(string $projectId): Response
    {
        $this->applyConfigureIniSettings(__METHOD__);

        return $this->generate(
            $projectId,
            static::$actions[__FUNCTION__]['type'],
            static::$actions[__FUNCTION__]['method']
        );
    }

    /**
     * Génération synchrone du document de projet.
     *
     * @param string $projectId Identifiant du projet (projects.id)
     * @return \Cake\Http\Response
     * @throws \Exception
     */
    public function fileGenerateProject(string $projectId): Response
    {
        $this->applyConfigureIniSettings(__METHOD__);

        return $this->generate(
            $projectId,
            static::$actions[__FUNCTION__]['type'],
            static::$actions[__FUNCTION__]['method']
        );
    }

    /**
     * Génération synchrone des documents de convocation et de note de synthèse
     *
     * @param string $summonId Identifiant de la convocation (summons.id)
     * @return \Cake\Http\Response
     */
    public function fileGenerateDocumentSummon(string $summonId): Response
    {
        $this->applyConfigureIniSettings(__METHOD__);
        $summonId = (int)$summonId;

        try {
            $this->Generations = $this->loadService('Generations', [], false);
            $this->Generations->{static::$actions[__FUNCTION__]['method']}($summonId);
            $this->Generations = null;
            $response = $this->getResponse()->withStatus(200);
            GenerationerrorsTable::cleanup('convocation', $summonId);
            GenerationerrorsTable::cleanup('executive_summary', $summonId);

            return $response;
        } catch (\InvalidArgumentException $exc) {
            GenerationerrorsTable::persist('convocation', $summonId, $exc->getMessage());
            GenerationerrorsTable::persist('executive_summary', $summonId, $exc->getMessage());
            throw new GenerationException(static::$actions[__FUNCTION__]['type'], $summonId, $exc->getMessage());
        }
    }

    /**
     * Génération synchrone du document de liste des délibérations.
     *
     * @param string $sittingId L'identifiant de la séance (sittings.id)
     * @return \Cake\Http\Response
     * @throws \Exception
     */
    public function fileGenerateDeliberationsList(string $sittingId): Response
    {
        $this->applyConfigureIniSettings(__METHOD__);

        return $this->generate(
            $sittingId,
            static::$actions[__FUNCTION__]['type'],
            static::$actions[__FUNCTION__]['method']
        );
    }

    /**
     * Génération synchrone du document de procès-verbal.
     *
     * @param string $sittingId L'identifiant de la séance (sittings.id)
     * @return \Cake\Http\Response
     * @throws \Exception
     */
    public function fileGenerateVerbalTrial(string $sittingId): Response
    {
        $this->applyConfigureIniSettings(__METHOD__);

        return $this->generate(
            $sittingId,
            static::$actions[__FUNCTION__]['type'],
            static::$actions[__FUNCTION__]['method']
        );
    }
}
