<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\UnauthorizedException;
use Cake\Utility\Hash;

/**
 * Actors Controller
 *
 * @property \App\Model\Table\ActorsTable $Actors
 * @property \App\Controller\Component\MulticoComponent $Multico
 * @property \App\Controller\Component\QueryUtilsComponent $QueryUtils
 * @method \App\Model\Entity\Actor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActorsController extends AppController
{
    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('QueryUtils');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $queryParams = $this->getRequest()->getQueryParams();
        $this->paginate = [
            'limitToStructure' => $this->Multico->getCurrentStructureId(),
            'limit' => isset($queryParams['limit'])
            && ($queryParams['limit'] <= PAGINATION_LIMIT) ? $queryParams['limit'] : PAGINATION_LIMIT,
            'maxLimit' => MAXIMUM_LIMIT,
            'contain' => [
                'ActorGroups' => function ($q) {
                    return $q
                        ->select(['ActorGroups.id', 'ActorGroups.name'])
                        ->where(['ActorGroups.active' => true])
                        ->order(['ActorGroups.name' => 'ASC'])
                        ->enableAutoFields(false);
                },
            ],
            'conditions' => [
                'OR' => [
                    isset($queryParams['name'])
                        ? $this->QueryUtils->getIlikeQueryExpression('Actors.lastname', "%{$queryParams['name']}%")
                        : '',
                    isset($queryParams['name'])
                        ? $this->QueryUtils->getIlikeQueryExpression('Actors.firstname', "%{$queryParams['name']}%")
                        : '',
                ],
                'AND' => [
                    isset($queryParams['active'])
                    && ($queryParams['active'] === 'false' || $queryParams['active'] === 'true')
                        ? ['Actors.active' => (bool)$queryParams['active']]
                        : [],
                ],
            ],
            'sortableFields' => ['rank', 'lastname'],
            'order' => ['rank' => 'asc'],
        ];

        $actors =
            $this->paginate($this->Actors->find('all'));

        $this->set(compact('actors'));
    }

    /**
     * View method
     *
     * @param string|null $id Actor id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $actor = $this->Actors->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
                'contain' => ['ActorGroups'],
            ]
        );

        $this->set('actor', $actor);
    }

    /**
     * Add method
     *
     * @return void
     */
    public function add()
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }
        $this->request->allowMethod('post');
        $data = json_decode((string)$this->request->getBody(), true);

        $data = Hash::get($data, 'actor');

        if (isset($data['actor_groups'])) {
            $data['actor_groups']['_ids'] = Hash::extract($data['actor_groups'], '{n}.id');
        }

        $data['structure_id'] = $data['structure_id'] ?? $this->Multico->getCurrentStructureId();
        $data['organization_id'] = $this->Multico->getCurrentOrganizationId();
        $data['limitToStructure'] = $this->Multico->getCurrentStructureId();

        $actor = $this->Actors->newEntity($data, ['associated' => ['ActorGroups' => ['onlyIds' => true]]]);

        if ($this->Actors->save($actor, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(201));
            $this->set('actor', $actor);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $actor->getErrors());
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Actor id.
     * @return void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }
        $this->request->allowMethod(['patch', 'put']);
        $data = json_decode((string)$this->request->getBody(), true);

        $data = Hash::get($data, 'actor');

        if (isset($data['actor_groups'])) {
            $data['actor_groups']['_ids'] = Hash::extract($data['actor_groups'], '{n}.id');
        }

        $actor = $this->Actors->get(
            $id,
            ['contain' => ['ActorGroups'],
                'limitToStructure' => $this->Multico->getCurrentStructureId()]
        );
        $actor = $this->Actors->patchEntity($actor, $data, ['associated' => ['ActorGroups' => ['onlyIds' => true]]]);

        if ($this->Actors->save($actor, ['limitToStructure' => $this->Multico->getCurrentStructureId()])) {
            $this->setResponse($this->getResponse()->withStatus(200));
            $this->set('actor', $actor);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $actor->getErrors());
        }
    }

    /**
     * [activate description]
     *
     * @param int $id [description]
     * @return void
     */
    public function activate(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can activate a actor');
        }

        $actor = $this->Actors->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $actor = $this->Actors->patchEntity($actor, ['active' => true]);
        if ($this->Actors->save($actor)) {
            $this->setResponse($this->getResponse()->withStatus(201));

            $this->set('actor', $actor);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $actor->getErrors());
        }
    }

    /**
     * [deactivate description]
     *
     * @param int $id [description]
     * @return void
     */
    public function deactivate(string $id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException('Only admin can deactivate a actor');
        }

        $actor = $this->Actors->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId(),
            ]
        );

        $actor = $this->Actors->patchEntity($actor, ['active' => false]);
        if ($this->Actors->save($actor)) {
            $this->setResponse($this->getResponse()->withStatus(201));

            $this->set('actor', $actor);
            $this->set('success', true);
        } else {
            $this->setResponse($this->getResponse()->withStatus(400));
            $this->set('errors', $actor->getErrors());
        }
    }

    /**
     * Fonction permettant la suppression d'un groupe d'acteur
     *
     * @param int $id Actor id.
     * @return void Redirects to index.
     * @access  public
     * @created 12/02/2019
     * @version v2.0
     */
    public function delete($id)
    {
        if (!$this->Multico->isAdminInCurrentOrganization()) {
            throw new UnauthorizedException();
        }

        $this->request->allowMethod(['delete']);
        $actors = $this->Actors->get(
            $id,
            [
                'limitToStructure' => $this->Multico->getCurrentStructureId()]
        );

        if (!$this->Actors->delete($actors)) {
            $this->setResponse($this->getResponse()->withStatus(400));
        }

        $this->setResponse($this->getResponse()->withStatus(204));
    }
}
