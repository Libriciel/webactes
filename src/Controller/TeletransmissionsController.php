<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\HistoriesTable;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;
use Cake\Utility\Hash;
use Exception;

/**
 * Teletransmissions Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 * @property \App\Controller\Component\MulticoComponent $Multico
 */
class TeletransmissionsController extends AppController
{
    public const ARACTE = 'arActes';
    public const BORDEREAU = 'bordereau';
    public const ACTE_TAMPONNE = 'acte_tamponne';
    public const ANNEXES_TAMPONNEES = 'annexes_tamponnee';

    /**
     * Initialize the controller and load the required components.
     *
     * @return \Cake\Http\Response|void
     * @throws \Exception
     */
    public function initialize(): void
    {
        $this->Projects = $this->fetchTable('Projects');
        parent::initialize();
    }

    /**
     * @return void
     * @throws \ReflectionException
     * @throws \Beanstalk\Exception\CantWorkException
     */
    public function sendTdt()
    {
        $this->request->allowMethod('post');

        $beanstalk = new Beanstalk('basic-pastell');

        $projectList = $this->getRequest()->getData();
        $responseIds = [];

        foreach ($projectList as $project) {
            // fixme check if everything is ok to teletrans

            /**
 * @var \App\Controller\ContainersTable $Containers
*/
            $Containers = $this->fetchTable('Containers');
            $containerId = $Containers->find()
                ->select(['id'])
                ->where(['project_id' => $project['projectId']])
                ->firstOrFail()->get('id');

            $data = [
                'flux' => Configure::read('Flux.actes-generique.name'),
                'flux_name' => Configure::read('Flux.actes-generique.name'),
                'controller' => 'projects',
                'structure_id' => $this->Multico->getCurrentStructureId(),
                'object_id' => $containerId,
                'action' => 'prepareDocumentOnPastell',
            ];

            $Containers->addStateact($containerId, PENDING_PASTELL);

            $history = $Containers->Histories->newEntity(
                [
                'structure_id' => $this->Multico->getCurrentStructureId(),
                'container_id' => $containerId,
                'user_id' => $this->Multico->getCurrentUserId(),
                'comment' => HistoriesTable::SEND_TDT,
                ]
            );

            $Containers->Histories->save($history);

            $responseIds[] = $beanstalk->emit($data);
        }

        $this->set(compact('responseIds'));
    }

    /**
     * cancel transaction tdt
     *
     * @return void
     */
    public function cancelTdt()
    {
        $this->request->allowMethod('post');

        $beanstalk = new Beanstalk('basic-pastell');

        $projectList = $this->getRequest()->getData();

        $this->Projects->areCancelable(Hash::extract($projectList, '{n}.projectId'));

        foreach ($projectList as $project) {

            /**
             * @var \App\Controller\ContainersTable $Containers
             */
            $Containers = $this->fetchTable('Containers');
            $containerId = $Containers->find()
                ->select(['id'])
                ->where(['project_id' => $project['projectId']])
                ->firstOrFail()->get('id');

            $data = [
                'flux' => Configure::read('Flux.actes-generique.name'),
                'flux_name' => Configure::read('Flux.actes-generique.name'),
                'controller' => 'projects',
                'structure_id' => $this->Multico->getCurrentStructureId(),
                'object_id' => $containerId,
                'action' => 'cancelDocumentTdt',
            ];

            $Containers->addStateact($containerId, PENDING_PASTELL);

            $history = $Containers->Histories->newEntity(
                [
                'structure_id' => $this->Multico->getCurrentStructureId(),
                'container_id' => $containerId,
                'user_id' => $this->Multico->getCurrentUserId(),
                'comment' => HistoriesTable::TDT_CANCEL,
                ]
            );

            $Containers->Histories->save($history);

            $responseId = $beanstalk->emit($data);
            $this->set(compact('responseId'));
        }
    }

    /**
     * return the slowurl to order
     *
     * @param  int $projectId projectId
     * @return void
     */
    public function getTdtUrl($projectId)
    {
        $structureId = $this->Multico->getCurrentStructureId();

        /**
         * @var \App\Controller\ConnecteursTable $Connecteurs
         */
        $Connecteurs = $this->fetchTable('Connecteurs');
        $connecteur = $Connecteurs->find()->where([
            'connector_type_id' => 1,
            'structure_id' => $structureId,
        ])->firstOrFail();

        /**
         * @var \App\Controller\ContainersTable $Containers
         */
        $Containers = $this->fetchTable('Containers');
        $container = $Containers->find()->where(['project_id' => $projectId])->firstOrFail();
        $this->set('url', $connecteur->get('tdt_url') . '?id=' . $container->get('tdt_id'));
    }

    /**
     * return the slowurl to order multiple projects
     * query ids[]
     *
     * @return void
     */
    public function getTdtUrlMultiple()
    {
        $structureId = $this->Multico->getCurrentStructureId();
        $projectIds = $this->getRequest()->getQuery('ids');

        if (empty($projectIds)) {
            throw new BadRequestException('missing ids');
        }

        /**
         * @var \App\Controller\ContainersTable $Containers
         */
        $Containers = $this->fetchTable('Containers');

        $containers = $Containers->find()->where(['project_id in ' => $projectIds])->toArray();

        $ids = '';

        foreach ($containers as $key => $container) {
            if ($key === 0) {
                $ids = '?id[]=' . $container->get('tdt_id');
            } else {
                $ids .= '&id[]=' . $container->get('tdt_id');
            }
        }

        /**
         * @var \App\Controller\ConnecteursTable $Connecteurs
         */
        $Connecteurs = $this->fetchTable('Connecteurs');
        $connecteur = $Connecteurs->find()->where([
            'connector_type_id' => 1,
            'structure_id' => $structureId,
        ])->firstOrFail();

        $this->set('url', $connecteur->get('tdt_url') . $ids);
    }

    /**
     * return the slowurl to order
     *
     * @return void
     */
    public function teletransmissionOrdered()
    {
        $this->request->allowMethod('POST');
        $projectIds = $this->getRequest()->getData('projectIds');

        /**
         * @var \App\Controller\ContainersTable $Containers
         */
        $Containers = $this->fetchTable('Containers');
        foreach ($projectIds as $projectId) {
            $container = $Containers->find()
                ->select(['id'])
                ->where(['project_id' => $projectId, 'structure_id' => $this->Multico->getCurrentStructureId()])
                ->firstOrFail();
            $Containers->addStateact($container->get('id'), PENDING_TDT);

            $history = $Containers->Histories->newEntity(
                [
                'structure_id' => $this->Multico->getCurrentStructureId(),
                'container_id' => $container->get('id'),
                'user_id' => $this->Multico->getCurrentUserId(),
                'comment' => HistoriesTable::TDT_TRANSMISSION,
                ]
            );

            $Containers->Histories->save($history);
        }
        $this->set('success', true);
    }

    /**
     * get the return tdt document (aractes, bordereau ...) from pastell
     *
     * @param  int      $projectId  projectId
     * @param  string   $type       document type
     * @param  null|int $annexeRank annexeRank
     * @return \Cake\Http\Response|null
     * @throws \Exception
     */
    private function getTdtDocument($projectId, $type, $annexeRank = null)
    {
        $container = $this->Projects->getContainerByProject($projectId, $this->Multico->getCurrentStructureId());
        $Structures = $this->fetchTable('Structures');
        $structure = $Structures->get(
            $this->Multico->getCurrentStructureId(),
            ['contain' => ['Connecteurs' => ['conditions' => ['connector_type_id' => 1]]]]
        );

        $this->loadComponent('Pastell', $structure->pastellConnector);

        $Pastellfluxs = $this->fetchTable('Pastellfluxs');

        $flux = $Pastellfluxs->find()->where(['container_id' => $container->get('id')])->orderDesc('id')->firstOrFail();

        $pastellDocumentId = $flux->get('id_d');

        $documentName = null;

        /**
         * @var \Cake\Http\Client\Response $res
         */
        $res = null;
        switch ($type) {
            case self::ARACTE:
                $res = $this->Pastell->getArAct($pastellDocumentId, $structure->get('id_orchestration'));
                $documentName = 'Aractes.xml';
                break;
            case self::BORDEREAU:
                $res = $this->Pastell->getBordereau($pastellDocumentId, $structure->get('id_orchestration'));
                $documentName = 'Bordereau.pdf';
                break;
            case self::ACTE_TAMPONNE:
                $res = $this->Pastell->getActeTamponne($pastellDocumentId, $structure->get('id_orchestration'));
                $documentName = 'acte_tamponne.pdf';
                break;
            case self::ANNEXES_TAMPONNEES:
                $res = $this
                    ->Pastell
                    ->getAnnexesTamponnee(
                        $pastellDocumentId,
                        $structure->get('id_orchestration'),
                        $annexeRank
                    );
                $documentName = 'annexe_tamponne_' . $annexeRank . '.pdf';
                break;
            default:
                throw new Exception('not existing type');
        }

        $temp = tmpfile();
        fwrite($temp, $res->getBody()->getContents());
        $path = stream_get_meta_data($temp)['uri'];

        $this->autoRender = false;
        $response = $this->response->withFile(
            $path,
            ['download' => true, 'name' => $documentName /*, 'type'=> $res->getHeader('Content-Type')[0]*/]
        );

        fclose($temp); // ceci va effacer le fichier

        return $response;
    }

    /**
     * get Bordereau
     *
     * @param  int $projectId projectId
     * @return \Cake\Http\Response|null
     * @throws \Exception
     */
    public function getBordereau($projectId)
    {
        return $this->getTdtDocument($projectId, self::BORDEREAU);
    }

    /**
     * get acte tamponné
     *
     * @param  int $projectId projectId
     * @return \Cake\Http\Response|null
     * @throws \Exception
     */
    public function getActeTamponne($projectId)
    {
        return $this->getTdtDocument($projectId, self::ACTE_TAMPONNE);
    }

    /**
     * get arActes.xml
     *
     * @param  int $projectId projectId
     * @return \Cake\Http\Response|null
     * @throws \Exception
     */
    public function getArActes($projectId)
    {
        return $this->getTdtDocument($projectId, self::ARACTE);
    }

    /**
     * get annexe tamponnée
     *
     * @param  int $projectId projectId
     * @param  int $annexeId  annexeId
     * @return \Cake\Http\Response|null
     * @throws \Exception
     */
    public function getAnnexesTamponnee($projectId, $annexeId)
    {
        /**
         * @var \App\Controller\AnnexesTable $Annexes
         */
        $Annexes = $this->fetchTable('Annexes');
        $annexe = $Annexes->get($annexeId);

        return $this->getTdtDocument($projectId, self::ANNEXES_TAMPONNEES, $annexe->rank);
    }
}
