<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Service\Security\PermissionsService;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Cake\Utility\Hash;
use Psr\Http\Message\RequestInterface;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    use ServiceAwareTrait;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');

        $this->loadComponent('Multico');
        $this->loadComponent(
            'Auth',
            [
                'authorize' => 'Controller',
                'loginAction' => [
                    'plugin' => false,
                    'controller' => 'Users',
                    'action' => 'notLogged',
                ],
                'logoutRedirect' => [
                    'plugin' => false,
                    'controller' => 'Users',
                    'action' => 'loggedOut',
                ],
                'unauthorizedRedirect' => false,
            ]
        );

        /*
         * Enable the following component for recommended CakePHP form protection settings.
         * see https://book.cakephp.org/4/en/controllers/components/form-protection.html
         */
        //$this->loadComponent('FormProtection');

        // @info: Make sure no error is displayed to not mess with the JSON or PDF response
        ini_set('display_errors', '0');
    }

    /**
     * Vérifie si l'utilisateur a le droit d'accéder à l'URL (courante).
     *
     * @param \ArrayAccess|array|null $user Paramètre inutile, on vérifie l'utilisateur courant seulement
     * @param \Cake\Http\ServerRequest|null $request The request to authenticate for.
     *   If empty, the current request will be used.
     * @return bool True if the user is authorized, otherwise false
     * @see https://book.cakephp.org/4/fr/controllers/components/authentication.html#utilisation-de-controllerauthorize
     */
    public function isAuthorized($user = null, ?ServerRequest $request = null): bool
    {
        return PermissionsService::isAuthorized(
            $this->request->getSession()->read('Auth.Role.name'),
            $request ?? $this->request
        );
    }

    /**
     * @param \Cake\Event\Event $event cakeEvent
     * @return void
     */
    public function beforeRender(EventInterface $event)
    {
        if ($this->request->contentType() == 'blob') {
            $this->autoRender = false;
        } else {
            $this->viewBuilder()->setOption('serialize', true);
        }
    }

    /**
     * Override cakephp paginator : add pagination info
     *
     * @param object $object entity
     * @param array $settings params
     * @return \Cake\Datasource\ResultSetInterface|\App\Controller\ResultSet
     */
    public function paginate($object = null, array $settings = [])
    {
        $results = parent::paginate($object, $settings);

        $paging = $this->request->getAttribute('paging');

        if (empty($paging)) {
            $params = [
                'page' => 1,
                'count' => 0,
                'perPage' => $this->request->getAttribute('limit') ?? PAGINATION_LIMIT,
            ];
        } else {
            $paging = Hash::extract($paging, '{s}');

            $params = [
                'page' => $paging[0]['page'],
                'count' => $paging[0]['count'],
                'perPage' => $paging[0]['perPage'],
            ];
        }
        $this->set('pagination', $params);

        return $results;
    }

    /**
     * Try to dispatch a fatal error to the right controller.
     *
     * @param \Psr\Http\Message\RequestInterface $request The request leading to the fatal error
     * @param string $message The fatal error message
     * @return void
     */
    public static function handleFatalErrorMessage(RequestInterface $request, string $message): void
    {
        if ($request->getParam('controller') === 'Generates') {
            GeneratesController::persistFatalErrorMessage($request, $message);
        } elseif ($request->getParam('controller') === 'Summons') {
            SummonsController::persistFatalErrorMessage($request, $message);
        } else {
            static::persistFatalErrorMessage($request, $message);
        }
    }

    /**
     * Try to persist a fatal error. Should be overridden in subclasses.
     *
     * @param \Psr\Http\Message\RequestInterface $request The request leading to the fatal error
     * @param string $message The fatal error message
     * @return void
     */
    public static function persistFatalErrorMessage(RequestInterface $request, string $message): void
    {
        Log::critical(
            sprintf(
                'Error while persisting fatal error exception for %sController::%s with parameters %s: %s',
                $request->getParam('controller'),
                $request->getParam('action'),
                json_encode($request->getParam('pass')),
                $message
            )
        );
    }
}
