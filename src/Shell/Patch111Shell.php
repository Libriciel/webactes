<?php
declare(strict_types=1);

namespace App\Shell;

use App\Model\Table\ContainersTable;
use App\Model\Table\HistoriesTable;
use Cake\Console\ConsoleOptionParser;
use Cake\Log\Log;
// @codingStandardsIgnoreFile

class Patch111Shell extends AppShell
{
    /**
     * Passe les projets signés dont le type d'acte et non transmissible à l'état acquitté.
     * Uniquement utile pour la mise à jour de 1.1.0 vers 1.1.1.
     * Inutile pour les versions suivantes !
     *
     * @return void
     */
    public function updateContainerState()
    {
        //récupérer les actes qu'il faut changer
        /*
         * ``̀  sql
         * SELECT container_id from containers_stateacts WHERE container_id IN
         * (Select id
            * FROM containers
            * where typesact_id IN (select id from typesacts where istdt = false))
            * AND stateact_id IN (SELECT id from stateacts where code IN ('declare-signe','signe-i-parapheur'))
            GROUP BY container_id;
         *
         * */

        /**
         * @var $Containers ContainersTable
         */
        $Containers = $this->fetchTable('Containers');

        $containers = $this->getContainers($Containers);

        Log::info("Nombre d'actes à mettre à jour : " . var_export(count($containers), true));

        foreach ($containers as $container) {
            $Containers->addStateact($container['id'], ACQUITTED);

            Log::info("Mise à jour de l'acte  : " . var_export($container['id'], true));

            $history = $Containers->Histories->newEntity(
                [
                'structure_id' => $container['structure_id'],
                'container_id' => $container['id'],
                'user_id' => null,
                'comment' => HistoriesTable::FORCE_CHANGE_STATUS_TO_TDT_ACQUITTED,
                ]
            );
            $Containers->Histories->save($history);
            Log::info("Mise à jour de l'historique de l'acte  : " . var_export($container['id'], true));
        }
    }

    /**
     * @param \App\Model\Table\ContainersTable $Containers container
     * @return array
     */
    private function getContainers(ContainersTable $Containers): array
    {
        $ContainersStateacts = $this->fetchTable('ContainersStateacts');
        $TypeActs = $this->fetchTable('Typesacts');

        return $Containers->find()
            ->select(['id', 'structure_id'])
            ->innerJoinWith('Stateacts')
            ->innerJoinWith('Typesacts')
            ->where(
                [
                'Containers.typesact_id IN' => $TypeActs->find('all')->select(['id'])->where(['istdt' => false]),
                'Stateacts.id in' => [DECLARE_SIGNED, PARAPHEUR_SIGNED],
                //Permet de mettre une condition sur le id du dernier état du container.
                'ContainersStateacts.id' => $ContainersStateacts->getLastByContainersIdSubquery(),
                ]
            )->toArray();
    }

    /**
     * @return void
     */
    public function turnAllNonTransmissibleTypeActToTransmissible()
    {
        /**
         * @var $Containers ContainersTable
         */
        $Containers = $this->fetchTable('Containers');

        $containers = $this->getContainers($Containers);

        if (count($containers) > 0) {
            $this->err(sprintf('Il existe %s projet(s) avec un type d\'acte non transmissilbe(s). Il faut donc mettre à jour l\'état des projets en passant la commande ** update_container_state **', var_export(count($containers), true)));

            $this->_stop(self::CODE_ERROR);
        }

        $TypeActs = $this->fetchTable('Typesacts');
        $countTypeActs = $TypeActs->updateAll(['istdt' => true], ['istdt' => false]);

        Log::info("Les types d'actes ont été mis à jour : " . $countTypeActs);
    }

    /**
     * Complete the option parser for this shell.
     *
     * @param \Cake\Console\ConsoleOptionParser $parser foo
     * @return \Cake\Console\ConsoleOptionParser
     */
    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(__('Change le statut d\'un container à Acquitté'))
            ->addArgument(
                'UpdateContainerState',
                [
                    'help' => __('Passe les containers à l\'état Acquitté.'),
                    'short' => 'u',
                ]
            )
            ->addArgument(
                'turnAllNonTransmissibleTypeActToTransmissible',
                [
                    'help' => __('Passe les types d\'actes non transmissilbe a transmissilbe.'),
                    'short' => 't',
                ]
            );

        return $parser;
    }
}
