<?php
declare(strict_types=1);

namespace App\Shell;

use App\Model\Table\GenerationerrorsTable;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\Locator\LocatorAwareTrait;

class GenerationerrorsShell extends AppShell
{
    use LocatorAwareTrait;

    /**
     * @var \App\Model\Table\GenerationerrorsTable
     */
    public $Generationerrors;

    /**
     * @var string[]
     */
    public $helpers = ['Table'];

    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->Generationerrors = $this->fetchTable('Generationerrors');
    }

    /**
     * Lists the entries in the generationerrors table.
     *
     * @return void
     */
    public function list(): void
    {
        $results = $this->Generationerrors
            ->find()
            ->orderAsc('created')
            ->enableHydration(false)
            ->toArray();

        if (empty($results)) {
            $this->_io->info('Aucun enregistrement trouvé dans la table generationerrors');
        } else {
            $headers = array_keys($results[0]);
            $this->_io->helper('table')->output(array_merge([$headers], $results));
        }
        $this->_stop(self::CODE_SUCCESS);
    }

    /**
     * Delete one or more entries from the generationerrors table.
     *
     * @return void
     */
    public function delete(): void
    {
        $count = count($this->args);
        if ($this->args === ['all']) {
            $conditions = ['1 = 1'];
        } elseif (
            $count === 2
            && in_array($this->args[0], GenerationerrorsTable::$types)
            && ctype_digit($this->args[1])
        ) {
            $conditions = ['type' => $this->args[0], 'foreign_key' => $this->args[1]];
        } elseif ($count >= 1 && array_search(false, array_map('ctype_digit', $this->args), true) === false) {
            $conditions = ['id IN' => $this->args];
        } else {
            $this->_io->error('Mauvais format d\'appel pour la commande delete');
            $this->_io->out($this->getOptionParser()->help('delete'));
            $this->_stop(self::CODE_ERROR);
        }

        $deleted = $this->Generationerrors->deleteAll($conditions);
        $format = __n('%d enregistrement a été supprimé', '%d enregistrements ont été supprimés', $deleted);
        $this->_io->out(sprintf($format, $deleted));
        $this->_stop(self::CODE_SUCCESS);
    }

    /**
     * Complete the option parser for this shell.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();
        $allowedTypes = implode("', '", GenerationerrorsTable::$types);

        return $parser
            ->setDescription('Permet de lister ou de supprimer les entrées de la table generationerrors.')
            ->addSubcommand(
                'list',
                [
                    'help' => __('Liste les entrées de la table generationerrors'),
                    'parser' => [
                        'description' => 'Permet de lister les entrées de la table generationerrors',
                    ],
                ]
            )
            ->addSubcommand(
                'delete',
                [
                    'help' => __('Suppression d\'entrées de la table generationerrors'),
                    'parser' => [
                        'description' => [
                            'Permet de supprimer une ou plusieurs entrées de la table generationerrors'
                            . " afin de forcer la regénération, notamment pour le type 'act'."
                            . ' 3 formes de paramètres sont acceptées:',
                            "'all'\t\t\t- ex.: bin/cake generationerrors delete all",
                            "<type> <foreign_key>\t- ex.: bin/cake generationerrors delete act 1",
                            "<id>...\t\t\t- ex.: bin/cake generationerrors delete 1 2 3",
                            "Les valeurs acceptées pour 'type' sont: '{$allowedTypes}'",
                        ],
                    ],
                ]
            );
    }
}
