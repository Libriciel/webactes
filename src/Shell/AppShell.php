<?php
declare(strict_types=1);

namespace App\Shell;

use Cake\Console\Shell;

abstract class AppShell extends Shell
{
    /**
     * Désactivation de la re-création des loggers, ce qui cassait toute la configuration précédente.
     *
     * @return void
     */
    public function startup(): void
    {
        parent::startup();

        $this->_io->setLoggers(false);
    }
}
