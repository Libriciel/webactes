<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 06/09/19
 * Time: 08:41
 */

namespace App\Shell;

use Beanstalk\Utility\Beanstalk;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\Locator\LocatorAwareTrait;
use Psr\Log\LogLevel;

class UpdateMailSecInformationsShell extends AppShell
{
    use LocatorAwareTrait;

    /**
     * @var \App\Model\Table\ConvocationsTable
     */
    protected $Convocations = null;

    /**
     * sync webact state with pastell
     *
     * @return int
     */
    public function sync()
    {
        $this->Convocations = $this->fetchTable('Convocations');

        $convocations = $this->Convocations
            ->find('all')
            ->select(['structure_id', 'pastell_id'])
            ->where([
                'pastell_id IS NOT' => null,
                'date_send IS NOT' => null,
                'date_open IS' => null,
            ])
            ->group(['structure_id', 'pastell_id']);

        $message = sprintf('Mailsec folders to consider: %d', $convocations->count());
        $this->log($message, LogLevel::DEBUG, ['scope' => 'mail-sec-pastell']);

        if (empty($convocations) === false) {
            $beanstalk = new Beanstalk('mail-sec-pastell');

            foreach ($convocations as $convocation) {
                try {
                    $message = [
                        'flux' => 'mailsec',
                        'flux_name' => 'mailse',
                        'controller' => 'shell',
                        'action' => 'checkStatusMailSec',
                        'structure_id' => $convocation['structure_id'],
                        'pastell_id' => $convocation['pastell_id'],
                    ];

                    $responseId = $beanstalk->emit($message);

                    // @todo: les autre $this->out des cron pour avoir plus de contexte
                    $message = sprintf(
                        'Successfully reserved job %d for structure_id %s and pastell_id %s',
                        $responseId,
                        $convocation['structure_id'],
                        $convocation['pastell_id'],
                    );
                    $this->log($message, LogLevel::DEBUG, ['scope' => 'mail-sec-pastell']);
                } catch (\Throwable $exc) {
                    $message = sprintf(
                        'Could not reserve job for structure_id %s and pastell_id %s',
                        $responseId,
                        $convocation['structure_id'],
                        $convocation['pastell_id'],
                    );
                    $this->log($message, LogLevel::DEBUG, ['scope' => 'mail-sec-pastell']);
                }
            }
        }

        return 0;
    }

    /**
     * Complete the option parser for this shell.
     *
     * @param \Cake\Console\ConsoleOptionParser $parser foo
     * @return \Cake\Console\ConsoleOptionParser
     */
    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(__('sync mail sec with pastell'))
            ->addArgument(
                'sync',
                [
                    'help' => __('(Rei)nitialiaze the default permissions'),
                ]
            );

        return $parser;
    }
}
