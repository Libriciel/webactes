<?php
declare(strict_types=1);

namespace App\Shell;

use App\Utility\CsvFileReader;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ConnectionInterface;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use InvalidArgumentException;
use Throwable;

/**
 * Class ImportStructuresUsersShell
 *
 * ATTENTION
 *  - headers structures CSV -> 'id', 'id_pastell', 'name'
 *  - headers users CSV -> 'id', 'active', 'firstname', 'lastname', 'username', 'email', 'PASSWORD', 'structure_id', 'profil'
 *  - 'separator' => ',',
 *  - 'delimiter' => '",',
 *
 * `docker cp ~/webACTES/structures.csv apache-api:/tmp && docker cp ~/webACTES/users.csv apache-api:/tmp && docker-compose -f docker-compose.yml exec apache-api bash`
 * `bin/cake import_csv_ternum_structures_users -o 1 structures /tmp/structures.csv`
 * `bin/cake import_csv_ternum_structures_users -o 1 users /tmp/users.csv`
 *
 * @package App\Shell
 */
/*
SELECT
        organizations.name,
        structures.id,
        structures.business_name,
        structures.id_external,
        services.name,
        roles.name
    FROM organizations
        INNER JOIN structures ON (structures.organization_id = organizations.id)
        INNER JOIN services ON (services.structure_id = structures.id)
        INNER JOIN roles ON (roles.structure_id = structures.id)
    ORDER BY organizations.name, structures.business_name, structures.id_external, services.name, roles.name;

SELECT
        organizations.name,
        structures.business_name,
        services.name,
        users.keycloak_id,
        users.username,
        roles.name
    FROM users
        INNER JOIN organizations ON (users.organization_id = organizations.id)
        INNER JOIN structures ON (structures.organization_id = organizations.id)
        INNER JOIN users_services ON (users_services.user_id = users.id)
        INNER JOIN services ON (services.structure_id = structures.id AND users_services.service_id = services.id)
        INNER JOIN roles_users ON (roles_users.user_id = users.id)
        INNER JOIN roles ON (roles_users.role_id = roles.id)
    ORDER BY organizations.name, structures.business_name, services.name, users.username, roles.name;
*/

class ImportCsvTernumStructuresUsersShell extends AppShell
{
    /**
     * Tasks used by this shell.
     *
     * @var array
     */
    public $tasks = [
        'ImportCsvTernumStructures',
        'ImportCsvTernumUsers',
    ];

    /**
     * Returns a connection to the database as defined by the --connection parameter.
     *
     * @return \Cake\Datasource\ConnectionInterface
     */
    public function getConnection(): ConnectionInterface
    {
        return ConnectionManager::get($this->params['connection']);
    }

    /**
     * Startup the shell, checks arguments and organization.
     *
     * @return void
     */
    public function startup(): void
    {
        parent::startup();

        if (file_exists($this->args[1]) === false) {
            $msgid = 'File "%s" cannot be read';
            throw new InvalidArgumentException(sprintf($msgid, $this->args[1]));
        }

        $organization_id = $this->param('organization_id');
        if (empty($organization_id) === true) {
            $msgid = 'The organization_id option is required';
            throw new InvalidArgumentException($msgid);
        }

        $this->Organizations = $this->fetchTable('Organizations');

        $organization = $this->Organizations
            ->find()
            ->select(['id', 'name'])
            ->enableHydration(false)
            ->first();

        $msgid = 'Beginning work for organization "%s" (%d) with file "%s"';
        $this->out(sprintf($msgid, $organization['name'], $organization['id'], $this->args[1]));
    }

    /**
     * Returns the currently used task based on the first parameter passed to the shell
     *
     * @return \Cake\Console\Shell
     */
    public function getCsvTask()
    {
        $taskName = Inflector::camelize(sprintf('import_csv_ternum_%s', $this->args[0]));

        return $this->{$taskName};
    }

    /**
     * Main function for looping on all the rows of the CSV file and try to process them with the corresponding task.
     *
     * @return bool|int|void|null
     */
    public function main()
    {
        $task = $this->getCsvTask();
        $headers = $task->getHeaders();
        $csv = new CsvFileReader($this->args[1], compact('headers'));

        $inserted = 0;
        $updated = 0;
        $rejects = [];

        $conn = $this->getConnection();
        $conn->begin();

        $success = true;
        try {
            foreach ($csv as $idx => $row) {
                $idx += ($this->param('headers') === 'true' ? 2 : 1);
                if (empty($row) === false) {
                    $name = $task->getDisplayFieldValue($row);
                    try {
                        [$mode, $entity] = $task->processRow($idx, $row);
                        if ($entity->hasErrors() === true) {
                            $rejects[] = array_merge($row, ["Line {$idx}: " . json_encode($entity->getErrors())]);
                            $msgid = 'Rejecting line %d ("%s"): %s';
                            $this->err(sprintf($msgid, $idx, $name, json_encode($entity->getErrors())));
                        } else {
                            if ($mode === 'insert') {
                                $msgid = 'Inserting line %d ("%s"): %s';
                            } else {
                                $msgid = 'Updating line %d ("%s"): %s';
                            }

                            $this->out(sprintf($msgid, $idx, $name, 'success'));
                            if ($mode === 'insert') {
                                $inserted++;
                            } else {
                                $updated++;
                            }
                        }
                    } catch (Throwable $exception) {
                        $success = false;
                        $rejects[] = array_merge($row, [var_export($exception->getMessage(), true)]);
                        $msgid = 'Rejecting line %d ("%s"): %s';
                        $this->err(sprintf($msgid, $idx, $name, var_export($exception->getMessage(), true)));
                    }
                }
            }
        } catch (Throwable $exception) {
            $success = false;
            $this->err($exception->getMessage());
        }

        if ($success === true) {
            $conn->commit();
            $msgid = 'Success: %d insertion(s), %d update(s), %d reject(s)';
            $this->out(sprintf($msgid, $inserted, $updated, count($rejects)));
        } else {
            $conn->rollback();
            $task->onRollback();
            $msgid = 'Error(s): %d insertion(s), %d update(s), %d reject(s)';
            $this->out(sprintf($msgid, $inserted, $updated, count($rejects)));
        }

        if (count($rejects) > 0) {
            $this->exportRejects($rejects);
        }

        $this->_stop($success ? static::CODE_SUCCESS : static::CODE_ERROR);
    }

    /**
     * Exports the rejected CSV rows to a CSV file.
     *
     * @param  array $rows The rejected rows
     * @return void
     */
    public function exportRejects(array $rows)
    {
        $path = TMP . Inflector::underscore(
            str_replace('\\', '', self::class)
        )
            . date('_YmdHis_')
            . $this->args[0] . '_' . 'errors_' . basename($this->args[1]);
        $msgid = 'Rejected lines will be available in file %s';
        $this->err(sprintf($msgid, $path));

        $task = $this->getCsvTask();
        $csv = fopen('php://temp/maxmemory:' . (5 * 1024 * 1024), 'r+');
        foreach (array_merge([$task->getHeaders()], $rows) as $row) {
            fputcsv($csv, $row);
        }
        rewind($csv);
        $lines = stream_get_contents($csv);

        if ($this->createFile($path, (string)$lines) === false) {
            $this->err(__('Coult not write to file "{0}"', $path));
        }
    }

    /**
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        // Get an empty parser from the framework.
        $parser = parent::getOptionParser();

        // Define your options and arguments.
        $parser->setDescription(__('This shell can import structures or users from a CSV file to begin populating the database'));// @codingStandardsIgnoreLine
        $parser->addArguments(
            [
                'type' => [
                    'help' => 'The type of CSV file',
                    'required' => true,
                    'choices' => ['structures', 'users'],
                ],
                'file' => [
                    'help' => 'The path to the CSV file',
                    'required' => true,
                ],
            ]
        );

        $this->Organizations = $this->fetchTable('Organizations');

        $organizations = $this->Organizations
            ->find()
            ->select(['id'])
            ->enableHydration(false)
            ->orderAsc('id')
            ->toArray();
        $organizations = Hash::extract($organizations, '{n}.id');
        $organizations = array_map(fn(&$value): string => (string)$value, $organizations);
        $parser->addOptions(
            [
                'connection' => [
                    'help' => 'The connection name for the production database',
                    'short' => 'c',
                    'default' => 'default',
                ],
                'headers' => [
                    'help' => 'Should be true if the first line in the input CSV file is a header line',
                    'short' => 't',
                    'default' => 'true',
                    'choices' => ['true', 'false'],
                ],
                'organization_id' => [
                    'help' => 'The id of the organization to insert the structures or the users into',
                    'short' => 'o',
                    'default' => null,
                    'choices' => $organizations,
                ],
                'service' => [
                    'help' => 'The name of the default service to create while creating or updating a structure',
                    'short' => 's',
                    'default' => 'Service par défaut',
                ],
            ]
        );

        // Return the completed parser
        return $parser;
    }
}
