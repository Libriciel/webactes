<?php
declare(strict_types=1);

namespace App\Shell;

use App\Model\Table\RolesTable;
use App\Service\Security\ControllerActionsService;
use App\Service\Security\PermissionsService;
use Cake\Console\ConsoleOptionParser;

class PermissionsSummaryShell extends AppShell
{
    /**
     * @var string[]
     */
    public array $helpers = ['Table'];

    /**
     * @var string[]
     */
    public array $uis = [
        'Projets > Brouillons' => 'controllers/Projects/indexDrafts',
        'Projets > En cours dans un circuit' => 'controllers/Projects/indexValidating',
        'Projets > À traiter' => 'controllers/Projects/indexToValidate',
        'Projets > Validés' => 'controllers/Projects/indexValidated',
        'Projets > Projets sans séances' => 'controllers/Projects/indexUnassociated',
        'Séances > Séances en cours' => 'controllers/Sittings/index', // ?statesitting=seance_close&isNot
        'Séances > Séances closes' => 'controllers/Sittings/index', // ?statesitting=seance_close
        'Télétransmission > À déposer sur le Tdt' => 'controllers/Projects/indexToTransmit',
        'Télétransmission > À envoyer en préfecture' => 'controllers/Projects/indexReadyToTransmit',
        'Actes > Liste des actes ' => 'controllers/Projects/indexAct',
    ];

    protected function getRoles(): array
    {
        return [
            RolesTable::SUPER_ADMINISTRATOR,
            RolesTable::ADMINISTRATOR,
            RolesTable::FUNCTIONNAL_ADMINISTRATOR,
            RolesTable::VALIDATOR_REDACTOR_NAME,
            RolesTable::GENERAL_SECRETARIAT,
        ];
    }

    protected function isAuthorized(string $controllerAction, array $permissions): bool
    {
        $parts = explode('/', $controllerAction);

        return in_array($parts[0], $permissions, true)
            || in_array("{$parts[0]}/{$parts[1]}", $permissions, true)
            || in_array("{$parts[0]}/{$parts[1]}/{$parts[2]}", $permissions, true);
    }

    protected function getSummary(): array
    {
        $controllerActions = ControllerActionsService::getControllerActions();

        $result = [];
        foreach ($this->getRoles() as $role) {
            $permissions = PermissionsService::getByRole($role);
            foreach ($controllerActions as $controllerAction) {
                $isAuthorized = $this->isAuthorized($controllerAction, $permissions);
                if (!isset($result[$controllerAction])) {
                    $result[$controllerAction] = [$controllerAction];
                }
                $result[$controllerAction][$role] = $isAuthorized ? 'Oui' : '-';
            }
        }

        return $result;
    }

    public function main(): void
    {
        $titles = array_merge([''], $this->getRoles());
        $summaries = $this->getSummary();

        // Accès aux entrées d'API
        $this->_io->nl(2);
        $this->_io->out('# Accès entrées d\'API');
        $this->_io->nl(2);

        $this->_io->helper('table')->output(array_merge([$titles], $summaries));

        // Accès aux menus
        $this->_io->nl(2);
        $this->_io->out('# Accès via l\'interface utilisateur');
        $this->_io->nl(2);

        $uis = [];
        foreach ($this->uis as $label => $controllerAction) {
            $line = $summaries[$controllerAction];
            $line[0] = sprintf('%s (%s)', $label, $controllerAction);
            $uis[] = $line;
        }
        $this->_io->helper('table')->output(array_merge([$titles], $uis));
    }

    public function getOptionParser(): ConsoleOptionParser
    {
        return parent::getOptionParser()
            ->setDescription(__('Tableaux de résumé des droits d\'accès pour les différents profils (API, UI)'));
    }
}
