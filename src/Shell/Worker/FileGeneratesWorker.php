<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use App\Exception\GenerationException;
use App\Model\Entity\Container;
use App\Model\Table\GenerationerrorsTable;
use App\Model\Table\WorkergenerationsTable;
use App\Utilities\Common\ConfigureIniSettingsTrait;
use Beanstalk\Utility\Beanstalk;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;
use Cake\Log\Log;

/**
 * Class FileGeneratesWorker
 *
 * @property \App\Service\GeneratesService $Generates
 * @property \App\Model\Table\MaindocumentsTable $Maindocuments
 * @property \App\Model\Table\FilesTable $Files
 * @package App\Shell\Worker
 */
class FileGeneratesWorker extends AbstractGenerationWorker
{
    use ConfigureIniSettingsTrait;
    use ServiceAwareTrait;

    public const TYPE_NOT_IMPLEMENTED_YET = 'Il n\'existe pas encore de generation pour ce type : %s';
    public const ACTION_NOT_EXISTS = 'l\'action %s n\'existe pas ou n\'est pas encore implémentée';
    public const ACTION_KEY = 'action';
    public const GENERATE_PROJECT = 'generateProject';
    public const GENERATE_ACT = 'generateAct';

    /**
     * Le nom du scope de ce worker
     *
     * @var string
     */
    protected $_scope = 'file-generates';

    /**
     * @inheritDoc
     */
    public function work($data)
    {
        if (in_array($data['action'], ['generateAct', 'generateProject'])) {
            return $this->dispatchAction($data);
        }
        throw new BadRequestException(sprintf(
            self::TYPE_NOT_IMPLEMENTED_YET,
            $data['action']
        ));
    }

    /**
     * Dispatch the job to the correct function by taking into account the action to execute
     *
     * @param mixed $data The job data
     * @return mixed
     * @throws \Exception
     */
    private function dispatchAction($data)
    {
        $this->log(self::class . '*******' . __FUNCTION__ . '*******');

        switch ($data[self::ACTION_KEY]) {
            case self::GENERATE_PROJECT:
                $this->generateProject($data);
                break;
            case self::GENERATE_ACT:
                $this->generateAct($data);
                break;
            default:
                throw new BadRequestException(self::ACTION_NOT_EXISTS);
        }

        return true;
    }

    /**
     * Generate project.
     *
     * @param array $data Données nessaire à l'action
     * @return void
     */
    protected function generateProject(array $data)
    {
        Log::emergency(str_repeat("'", 120), ['scope' => $this->_scope]);
        Log::emergency(__FILE__ . ':' . __LINE__, ['scope' => $this->_scope]);
        Log::emergency(print_r($data, true), ['scope' => $this->_scope]);
        Log::emergency(str_repeat("'", 120), ['scope' => $this->_scope]);
    }

    /**
     * Checks that every annexe got converted before starting the generation.
     * If any annexe isn't converted, re-launches the same beanstalk message with a delay.
     * Returns true if a retry was launched, false otherwise.
     *
     * @param \App\Model\Entity\Container $container The container for which to generate
     * @param array $data The data to transmit through beanstalk for the potential retry
     * @param int $delay The delay before retrying
     * @return bool
     */
    protected function handleRetryWithDelay(Container $container, array $data, int $delay = 30): bool
    {
        $retry = false;
        foreach ($container->annexes as $annexe) {
            if ($annexe->is_generate && $annexe->file->mimetype === 'application/pdf') {
                $pdfPath = $annexe->file->path;
                $odtPath = dirname($pdfPath) . DS . 'odt' . DS . basename($pdfPath);
                if (!file_exists($odtPath)) {
                    $retry = true;
                }
            }
        }

        if ($retry === true) {
            $messageBase = sprintf(
                'Toutes les annexes du projet "%d" n\'ont pas été converties.',
                $container->project_id
            );
            $message = sprintf("{$messageBase} Attente de %d secondes avant de réessayer.", $delay);
            Log::debug($message, ['scope' => $this->_scope]);

            $beanstalk = new Beanstalk('file-generates');
            $beanstalk->emit($data, Configure::read('Beanstalk.Priority.HIGHT'), $delay);

            return true;
        }

        return false;
    }

    /**
     * Generate project.
     *
     * @param array $data Données nécessaires à l'action
     * @return void
     * @throws \Exception
     */
    protected function generateAct(array $data)
    {
        $this->log(sprintf('Début de %s pour le projet de container_id %d', __METHOD__, $data['container_id']));
        $this->applyConfigureIniSettings(__METHOD__);

        $container = $this->fetchTable('Containers')
            ->find()
            ->contain([
                'Annexes',
                'Annexes.Files',
                'Projects' => [
                    'fields' => ['code_act','name'],
                ],
                'Maindocuments',
                'Maindocuments.Files',
            ])
            ->where(['Containers.id' => $data['container_id']])
            ->firstOrFail();

        if (
            !$this->handlePreviousFatalError(__METHOD__, 'act', $container->project_id, $data)
            && !$this->handleRetryWithDelay($container, $data)
        ) {
            GenerationerrorsTable::cleanup('act', $container->project_id);

            WorkergenerationsTable::persist(getmypid(), 'act', $container->project_id);

            $generations = $this->loadService('Generations', [], false);
            $mimeType = 'application/pdf';
            try {
                $content = $generations->generateAct($container->project_id, $mimeType);
                GenerationerrorsTable::cleanup('act', $container->project_id);

                $fileName = $this->generateActFileName($container->project_id, $container->project->code_act);

                $data = [
                    'name' => $fileName,
                    'contents' => $content,
                    'size' => strlen((string)$content) * 8,
                    'mimetype' => 'application/pdf',
                ];

                if (empty($container->maindocument->files)) {
                    $this->fetchTable('Files')->saveInFsNewStream(
                        $data,
                        ['maindocument_id' => $container->maindocument->id],
                        $container->id,
                        $container->structure_id
                    );
                } else {
                    $this->fetchTable('Files')->saveInFsStream(
                        $container->maindocument->files[0]->id,
                        $data,
                        $container->structure_id
                    );
                }
            } catch (\InvalidArgumentException $exc) {
                $message = sprintf('GENERATE EXCEPTION (code %d): %s', $exc->getCode(), $exc->getMessage());
                $this->log($message);

                GenerationerrorsTable::persist('act', $container->project_id, $exc->getMessage());
                throw new GenerationException('act', $container->project_id, $exc->getMessage(), $exc->getCode());
            } finally {
                // This block will be executed everytime, except for \Cake\Error\FatalErrorException
                $generations = null;
                WorkergenerationsTable::cleanup(getmypid(), 'act', $container->project_id);
            }

            $this->log(sprintf('Fin de %s pour le projet de container_id %d', __METHOD__, $container->id));
        }
    }

    /**
     * @param int $id The name of the project
     * @param string|null $codeAct The code_act of the project
     * @return string
     */
    public function generateActFileName(int $id, ?string $codeAct = null): string
    {
        $fileName = $codeAct ?: sprintf('acte-%d', $id);

        return sprintf('%s.pdf', $fileName);
    }
}
