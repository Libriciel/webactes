<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;
use Cake\Mailer\Mailer;
use Cake\ORM\Locator\LocatorAwareTrait;

class EmailNotificationsWorker extends AbstractWorker
{
    use LocatorAwareTrait;

    public const TYPE = 'type';
    public const MAIL_TYPE_NOT_IMPLEMENTED_YET = 'Il n\'existe pas encore de mail pour ce type : %s';
    public const ACTION_NOT_EXISTS = 'l\'action %s n\'existe pas ou n\'est pas encore implémentée';
    public const ACTION_KEY = 'action';
    public const SEND_NOTIFICATION_ACTION_VALIDATION_TO_DO = 'validationToDo';
    public const PROJECT_AT_STATE = 'projectAtState';
    public const MY_PROJECT_AT_STATE = 'myProjectAtState';

    /**
     * Le nom du scope de ce worker
     *
     * @var string
     */
    protected $_scope = 'email-notifications';

    /**
     * @inheritDoc
     */
    public function work($data)
    {
        if (in_array($data[self::TYPE], Configure::read('Email.User.Type'))) {
            return $this->dispatchAction($data);
        }
        throw new BadRequestException(sprintf(
            self::MAIL_TYPE_NOT_IMPLEMENTED_YET,
            $data[self::TYPE]
        ));
    }

    /**
     * Dispatch the job to the correct function by taking into account the action to execute
     *
     * @param mixed $data The job data
     * @return mixed
     */
    private function dispatchAction($data)
    {
        switch ($data[self::ACTION_KEY]) {
            case self::SEND_NOTIFICATION_ACTION_VALIDATION_TO_DO:
                $this->validationToDo($data);
                break;
            case self::MY_PROJECT_AT_STATE:
                $this->myProjectAtState($data);
                break;
            default:
                throw new BadRequestException(self::ACTION_NOT_EXISTS);
        }

        return true;
    }

    /**
     * Envoie un email indiquant qu'il y a un projet à examiner.
     *
     * @param array $data Données nessaire à l'action
     * @return void
     */
    protected function validationToDo(array $data)
    {
        $this->log(self::class . '*******' . __FUNCTION__ . '*******');

        $CurrentUser = $this->fetchTable('Users')->get($data['user_id']);

        $LastUser = $this->fetchTable('Users')->get($data['last_user_id']);

        $email = new Mailer('default');
        $email->viewBuilder()->setTemplate('validationToDo');
        $test = $email
            ->setFrom([MAIL_FROM => 'WebActes'])
            ->setTo($CurrentUser->email)
            ->setEmailFormat('html')
            ->setSubject(h('Validation à faire'))
            ->setViewVars(
                [
                    'userName' => isset($data['user_id']) ? $CurrentUser->nom_complet_court : '',
                    'libelle' => $data['libelle'] ?? '',
                    'commentaire' => $data['comment'] ?? '',
                    'backUserName' => $LastUser->nom_complet_court,
                    'projectUrl' => PROJECT_VIEW . $data['object_id'],
                ]
            )
            ->deliver('');
        $this->log(var_export($data['notification_name'], true));
        $this->log(var_export($test, true));
        $this->log('Notification envoyée pour : ' . $CurrentUser->nom_complet_court);
    }

    /**
     * Envoie un email indiquant qu'il y a un projet à examiner.
     *
     * @param array $data Données nécessaires à l'action
     * @return void
     */
    protected function myProjectAtState(array $data)
    {
        $this->log(self::class . '*******' . __FUNCTION__ . '*******');

        $CurrentUser = $this->fetchTable('Users')->get($data['user_id']);

        $LastUser = isset($data['last_user_id'])
            ? $this->fetchTable('Users')->get($data['last_user_id'])
            : null;

        $this->log(
            sprintf(
                'Notification [%s] envoyée pour : %s',
                $data['object_state_code'],
                $CurrentUser->nom_complet_court
            )
        );
        $this->log($CurrentUser->email);

        $email = new Mailer('default');

        $email->viewBuilder()->setTemplate($data['object_state_code'] . ($data['isTdt'] === true ? '_tdt' : ''));

        $mail = $email
            ->setFrom([MAIL_FROM => 'WebActes'])
            ->setTo($CurrentUser->email)
            // Si télétransmissible alors suffix pour le template
            ->setSubject($data['notification_name'])
            ->setEmailFormat('html')
            ->setViewVars(
                [
                    'userName' => isset($data['user_id']) ? $CurrentUser->nom_complet_court : '',
                    'libelle' => $data['libelle'] ?? '',
                    'projectState' => $data['object_state_code'],
                    'commentaire' => $data['comment'] ?? '',
                    'backUserName' => isset($data['last_user_id']) ? $LastUser->nom_complet_court : '',
                    'projectUrl' => PROJECT_VIEW . $data['object_id'],
                    'isMine' => $data['isMine'],
                ]
            )
            ->deliver('');
        $this->log(var_export($mail, true));
    }
}
