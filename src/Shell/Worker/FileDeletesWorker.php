<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Http\Exception\BadRequestException;

/**
 * Class FileGeneratesWorker
 *
 * @property \App\Model\Table\MaindocumentsTable $Maindocuments
 * @property \App\Model\Table\FilesTable $Files
 * @package App\Shell\Worker
 */
class FileDeletesWorker extends AbstractWorker
{
    use ServiceAwareTrait;

    public const TYPE_NOT_IMPLEMENTED_YET = 'Il n\'existe pas encore de generation pour ce type : %s';
    public const ACTION_NOT_EXISTS = 'l\'action %s n\'existe pas ou n\'est pas encore implémentée';

    /**
     * Le nom du scope de ce worker
     *
     * @var string
     */
    protected $_scope = 'file-deletes';

    /**
     * @inheritDoc
     */
    public function work($data)
    {
        if (in_array($data['action'], ['deleteFileAnnex'])) {
            return $this->dispatchAction($data);
        }
        throw new BadRequestException(sprintf(
            self::TYPE_NOT_IMPLEMENTED_YET,
            $data['action']
        ));
    }

    /**
     * Dispatch the job to the correct function by taking into account the action to execute
     *
     * @param  mixed $data The job data
     * @return mixed
     */
    private function dispatchAction($data)
    {
        $this->log(self::class . '*******' . __FUNCTION__ . '*******');

        switch ($data['action']) {
            case 'deleteFileAnnex':
                $this->deleteFileAnnex($data);
                break;
            default:
                throw new BadRequestException(self::ACTION_NOT_EXISTS);
        }

        return true;
    }

    /**
     * Generate project.
     *
     * @param  array $data Données nécessaire à l'action
     * @return void
     */
    protected function deleteFileAnnex(array $data)
    {
    }
}
