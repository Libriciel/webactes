<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;

abstract class AbstractGenerationWorker extends AbstractWorker
{
    use LocatorAwareTrait;

    /**
     * Essaie de gérér une erreur fatale précédemment émise par un autre worker lors d'une génération, c'est à dire si
     * une entrée lié se trouve dans la table generationerrors car dans ce cas on est probablement face à une erreur
     * fatale et on va crasher en boucle.
     *
     * Dans ce cas, on abandonne cette génération et on ajoute un message avec le niveau critical dans les logs.
     *
     * Retourne true si une erreur a dû être gérée, false sinon.
     *
     * @param string $method La méthode utilisée pour la génération dans le worker
     * @param string $type Le type de génération (voir la règle de validation inList GenerationerrorsTable de pour les
     *  valeurs possibles)
     * @param int $foreignKey La clé étrangère pour la génération (projects.id ou sittings.id)
     * @param array $data Les données envoyées au worker
     * @return bool
     */
    protected function handlePreviousFatalError(string $method, string $type, int $foreignKey, array $data): bool
    {
        $generationerror = $this->fetchTable('Generationerrors')
            ->find()
            ->where(['type' => $type, 'foreign_key' => $foreignKey])
            ->enableHydration(false)
            ->first();

        if ($generationerror) {
            $format = 'Abandon de %s pour la clé étrangère %d (data %s): %s';
            $message = sprintf(
                $format,
                $method,
                $foreignKey,
                json_encode($data),
                $generationerror['message']
            );
            Log::critical($message, ['scope' => $this->_scope]);

            return true;
        }

        return false;
    }
}
