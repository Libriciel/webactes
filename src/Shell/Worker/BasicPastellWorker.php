<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use App\Utilities\Api\Pastell\Pastell;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\ORM\Locator\LocatorAwareTrait;

class BasicPastellWorker extends AbstractPastellBaseWorker
{
    use LocatorAwareTrait;

    public const STATE_PENDING = 'PENDING';
    public const STATE_DONE = 'DONE';
    public const STATE_FAILED = 'FAILED';
    protected $http;
    protected $base_url;
    protected $base_api;

    public const DEFAULT_BASE_API = 'api/v1';

    /**
     * Le nom du scope de ce worker
     *
     * @var string
     */
    protected $_scope = 'basic-pastell';

    /**
     * @var \App\Model\Table\StructuresTable
     */
    protected $Structures = null;

    /**
     * @inheritDoc
     */
    public function __construct(Beanstalk $Beanstalk, $keepAlive = true, array $params = [])
    {
        parent::__construct($Beanstalk, $keepAlive, $params);

        $this->Structures = $this->fetchTable('Structures');
    }

    //FIXME : copy in lots of class

    /**
     * get pastell client
     *
     * @param int $structureId structureId
     * @return \App\Utilities\Api\Pastell\Pastell
     */
    protected function getPastellClient($structureId)
    {
        $connecteur = $this->Structures->Connecteurs
            ->find()
            ->where([
                'connector_type_id' => 1,
                'structure_id' => $structureId,
            ])
            ->firstOrFail();

        $client = new Client(
            [
                'auth' => [
                    'username' => $connecteur->get('username'),
                    'password' => $connecteur->get('password'),
                ],
                'ssl_verify_peer' => false,
                'redirect' => 2,
            ]
        );

        return new Pastell($client, $connecteur->get('url'));
    }

    /**
     * @inheritDoc
     */
    protected function prepareDocumentOnPastell($data)
    {
        $message = sprintf(
            '%s structure_id %s, flux_name %s, sousTypeParapheur %s, object_id %s, user_id %s',
            __METHOD__,
            (string)$data['structure_id'],
            (string)$data['flux_name'],
            (string)($data['sousTypeParapheur'] ?? 'NULL'),
            (string)$data['object_id'],
            (string)($data['user_id'] ?? 'NULL'),
        );
        $this->log($message);

        $pastellFluxType_id = $this->Structures->Containers->Pastellfluxs->Pastellfluxtypes
            ->find()
            ->select('id')
            ->where(
                [
                    'structure_id' => $data['structure_id'],
                    'name' => $data['flux_name'],
                ]
            )->firstOrFail()->get('id');

        if (
            $this->Structures->Containers->buildPastellDocumentActesGeneriques(
                $data['object_id'],
                $pastellFluxType_id,
                $data['structure_id']
            )
            && $this->Structures->Containers->getLastStateActEntry($data['object_id'])[0]->id == PARAPHEUR_PENDING
            //            && isset($data['user_id'])
        ) {
            $this->Structures->Containers->addStateact($data['object_id'], PARAPHEUR_SIGNING);

            $beanstalkSignature = new Beanstalk('signature-pastell');

            $dataSignature = [
                'flux' => Configure::read('Flux.actes-generique.name'),
                'flux_name' => Configure::read('Flux.actes-generique.name'),
                'controller' => 'projects',
                'structure_id' => $data['structure_id'],
                'object_id' => $data['object_id'],
                'action' => 'sendParapheur',
                'user_id' => $data['user_id'],
                'sous-type' => $data['sousTypeParapheur'],
            ];

            $responseId = $beanstalkSignature->emit($dataSignature, Configure::read('Beanstalk.Priority.VERY_HIGHT'));

            $this->set(compact('responseId'));
        }

        return true;
    }

    /**
     * Add an entry to histories for the given structure and container
     *
     * @param array $data contains structure_id and container_id
     * @param string $historyType creation, edition, etc
     * @return void
     */
    protected function addHistory($data, string $historyType)
    {
        $history = $this->Structures->Containers->Histories->newEntity(
            [
                'structure_id' => $data['structure_id'],
                'container_id' => $data['container_id'],
                'user_id' => null,
                'comment' => $historyType,
            ]
        );
        $this->Structures->Containers->save($history);
    }

    /**
     * @inheritDoc
     */
    protected function checkStatus($data)
    {
    }

    /**
     * @inheritDoc
     */
    protected function sendParapheur($data)
    {
    }

    /**
     * @inheritDoc
     */
    protected function sendTdt($data)
    {
    }

    /**
     * @inheritDoc
     */
    protected function checkStateTdt($data)
    {
    }

    /**
     * @inheritDoc
     */
    protected function cancelDocumentTdt($data)
    {
    }

    /**
     * @inheritDoc
     */
    protected function sendMailSec(array $data)
    {
    }
}
