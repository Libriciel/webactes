<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use App\Model\Table\HistoriesTable;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Log\Log;

class TdtPastellWorker extends BasicPastellWorker
{
    /**
     * Le nom du scope de ce worker
     *
     * @var string
     */
    protected $_scope = 'tdt-pastell';

    /**
     * @inheritDoc
     */
    protected function sendTdt($data)
    {
        $message = sprintf(
            '%s structure_id %s, pastellFlux_id %s, object_id %s',
            __METHOD__,
            (string)$data['structure_id'],
            (string)$data['pastellFlux_id'],
            (string)$data['object_id'],
        );
        $this->log($message);

        try {
            $EntityPastellId = $this->Structures
                ->find()
                ->select('id_orchestration')
                ->where(['id' => $data['structure_id']])
                ->firstOrFail()
                ->get('id_orchestration');

            $pastellResponse = $this->Structures->Containers->triggerAnActionOnDocument(
                $EntityPastellId,
                $data['object_id'],
                Configure::read('Flux.actes-generique.tdt.action'),
                $data['structure_id']
            );

            //On sauvegarde l'action PASTELL effectuée => son état dépand du retour de PASTELL.
            $this->Structures->Containers->savePastellFluxAction(
                Configure::read('Flux.actes-generique.tdt.action'),
                $data['pastellFlux_id'],
                $this->Structures->get($data['structure_id'])->organization_id,
                $data['structure_id'],
                isset($pastellResponse['error-message']) ? self::STATE_FAILED : self::STATE_DONE,
                $pastellResponse['error-message'] ?? $pastellResponse['message']
            );
        } catch (RecordNotFoundException $e) {
            $message = sprintf(
                '[PASTELL-WEBACTES]' . $e->getCode() . '-' . 'Entité non renseignée dans PASTELL %s.',
                $data['structure_id']
            );
            Log::error($message, ['scope' => $this->_scope]);
        }

        return true;
    }

    /**
     * @param array $data data structure_id, container_id
     * @return mixed|void
     */
    protected function checkStateTdt($data)
    {
        $message = sprintf(
            '%s structure_id %s, container_id %s, object_id %s',
            __METHOD__,
            (string)$data['structure_id'],
            (string)$data['container_id'],
            isset($data['object_id']) ? (string)$data['object_id'] : null,
        );
        $this->log($message);

        try {
            //faire une methode pour cela !
            $entityPastellId = $this->Structures->find()
                ->select('id_orchestration')
                ->where(
                    [
                        'id' => $data['structure_id'],
                    ]
                )->firstOrFail()->get('id_orchestration');

            //get Id_d pastell
            $flux = $this->Structures->Containers->Pastellfluxs
                ->find()
                ->where(['container_id' => $data['container_id']])
                ->orderDesc('id')
                ->firstOrFail();
            $pastellDocumentId = $flux->get('id_d');

            $this->Structures->Containers->triggerAnActionOnDocument(
                $entityPastellId,
                $pastellDocumentId,
                'verif-tdt',
                $data['structure_id']
            );

            // don't give a shit about the answer

            $pastellClient = $this->getPastellClient($data['structure_id']);
            $pastellResponse = $pastellClient->getDocument(
                $entityPastellId,
                $pastellDocumentId
            );

            $response = $pastellResponse->getJson();

            if (!isset($response['error-message']) && !isset($response['message'])) {
                $response['message'] = $response['last_action']['action'] ?? 'pas de message';
            }

            //On sauvegarde l'action PASTELL effectuée => son état dépand du retour de PASTELL.
            $this->Structures->Containers->savePastellFluxAction(
                'getDocument',
                $flux->get('id'),
                $this->Structures->get($data['structure_id'])->organization_id,
                $data['structure_id'],
                isset($response['error-message']) ? self::STATE_FAILED : self::STATE_DONE,
                $response['error-message'] ?? $response['message']
            );

            if (!isset($response['last_action']['action'])) {
                return;
            }

            switch ($response['last_action']['action']) {
                case 'send-tdt':
                case 'document-transmis-tdt':
                    //set status to a ordonner
                    $this->saveState(
                        $data['container_id'],
                        TO_ORDER,
                        $response['data']['tedetis_transaction_id']
                    );
                    break;
                case 'teletransmission-tdt': //état qui n'est pas pris en compte par pastell visiblement
                    //Télétransmis en attente de reponse de la pref
                    $this->saveState(
                        $data['container_id'],
                        PENDING_TDT,
                        $response['data']['tedetis_transaction_id']
                    );
                    break;
                case 'send-tdt-erreur':
                    $this->saveState(
                        $data['container_id'],
                        TDT_ERROR,
                        null,
                        HistoriesTable::TDT_ERROR
                    );
                    //erruer de l'envoie au tdt (pas la peine de repasser dessus c'est mort le laisser en manuel)
                    break;
                case 'tdt-error':
                    //erreur tdt pas la peine de rententer ta chance au auto (le laisser en manuel)
                    $this->saveState(
                        $data['container_id'],
                        TDT_ERROR,
                        null,
                        HistoriesTable::TDT_ERROR
                    );
                    break;
                case 'acquiter-tdt':
                    // Document acquité par la pref
                    $this->saveState(
                        $data['container_id'],
                        ACQUITTED,
                        $response['data']['tedetis_transaction_id'],
                        HistoriesTable::TDT_ACQUITTED
                    );
                    break;
                case 'annuler-tdt':
                    $this->saveState(
                        $data['container_id'],
                        TDT_CANCEL,
                        HistoriesTable::TDT_CANCEL
                    );
                    break;
                case 'modification'://retour à l'état précédant car des infos. sont manquantes au niveau de Pastell
                    // @info: object_id (id_d) dans PastellTdtListener, container_id dans UpdateTdtStateShell
                    if (isset($data['object_id']) === true) {
                        $containerId = $this->Structures->Containers->Pastellfluxs
                            ->find('all')
                            ->select(['container_id'])
                            ->where(['id_d' => $data['object_id']])
                            ->firstOrFail()
                            ->get('container_id');
                    } else {
                        $containerId = $data['container_id'];
                    }

                    // @info: parfois 1 seule entrée retournée
                    $stateacts = $this->Structures->Containers->getLastStateActEntry($data['container_id'], 2);

                    $message = sprintf(
                        '%s:%d (%s) container_id %s, %s stateacts, new state %s - %s',
                        __METHOD__,
                        __LINE__,
                        $response['last_action']['action'],
                        (string)$data['container_id'],
                        (string)count($stateacts),
                        (string)$stateacts[count($stateacts) - 1]->id,
                        (string)$stateacts[count($stateacts) - 1]->name,
                    );
                    $this->log($message);

                    // @todo: seulement si l'état n'est pas le même que précédemment, ce qui éviterait de feinter dans
                    // la méthode Containers::getLastStateActEntry
                    $this->saveState(
                        $containerId,
                        $stateacts[count($stateacts) - 1]->id,
                        ''
                    );
                    break;
            }
        } catch (RecordNotFoundException $e) {
            $message = sprintf(
                '[PASTELL-WEBACTES]' . $e->getCode() . '-' . 'Projet non renseigné dans pastell : %s.',
                $data['container_id']
            );
            Log::error($message, ['scope' => $this->_scope]);
        }

        return true;
    }

    /**
     * @param array $data data structure_id, container_id
     * @return mixed|void
     */
    protected function cancelDocumentTdt($data)
    {
        $message = sprintf(
            '%s structure_id %s, container_id %s',
            __METHOD__,
            (string)$data['structure_id'],
            (string)$data['container_id'],
        );
        $this->log($message);

        try {
            //faire une methode pour cela !
            $entityPastellId = $this->Structures->find()
                ->select('id_orchestration')
                ->where(
                    [
                        'id' => $data['structure_id'],
                    ]
                )->firstOrFail()->get('id_orchestration');

            $flux = $this->Structures->Containers->Pastellfluxs
                ->find()
                ->where(['container_id' => $data['container_id']])
                ->orderDesc('id')
                ->firstOrFail();
            $pastellDocumentId = $flux->get('id_d');

            $pastellResponse = $this->Structures->Containers->triggerAnActionOnDocument(
                $entityPastellId,
                $pastellDocumentId,
                'annulation-tdt',
                $data['structure_id']
            );

            if ($pastellResponse->getJson()['result']) {
                $this->saveState($data['container_id'], TDT_CANCEL);
            }
        } catch (RecordNotFoundException $e) {
            $message = sprintf(
                '[PASTELL-WEBACTES]' . $e->getCode() . '-' . 'Entité non renseignée dans PASTELL : %s.',
                $data['structure_id']
            );
            Log::error($message, ['scope' => $this->_scope]);
        }

        return true;
    }

    /**
     * save last stateacts
     *
     * @param int $containerId containerId
     * @param int $status status
     * @param null|string $tdt_id tdtId
     * @param string $historyState state for history
     * @return void
     */
    protected function saveState($containerId, $status, $tdt_id = null, $historyState = '')
    {
        $stateAct = $this->Structures->Containers->getLastStateActEntry($containerId);

        if (!empty($tdt_id)) {
            $container = $this->Structures->Containers->get($containerId);
            $containerArray = $container->toArray();
            $containerArray['tdt_id'] = $tdt_id;
            $container = $this->Structures->Containers->patchEntity($container, $containerArray);
            $this->Structures->Containers->save($container);
        }

        /**
         * Si le document a déjà été ordonné, il ne doit repasse pas en à ordonner
         * dans les deux cas pastell renvoie document-transmis-tdt
         */
        if ($stateAct[0]['id'] == PENDING_TDT && $status == TO_ORDER) {
            return;
        }

        //set state to 'a-ordonner'
        $this->Structures->Containers->addStateact($containerId, $status);

        if ($historyState != '') {
            $history = $this->Structures->Containers->Histories->newEntity(
                [
                    'structure_id' => $container->structure_id,
                    'container_id' => $containerId,
                    'user_id' => null,
                    'comment' => $historyState,
                ]
            );

            $this->Structures->Containers->Histories->save($history);
        }
    }
}
