<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use Cake\Log\Log;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LogLevel;

/**
 * Par défaut, on journalise au niveau "info".
 */
abstract class AbstractWorker extends \Beanstalk\Command\AbstractWorker
{
    use LoggerAwareTrait;

    /**
     * Le nom du scope de ce worker
     *
     * @var string
     */
    protected $_scope = 'abstract-worker';

    /**
     * @inheritDoc
     */
    public function log($string): bool
    {
        return Log::write(LogLevel::INFO, $string, ['scope' => $this->_scope]);
    }
}
