<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;

abstract class AbstractPastellBaseWorker extends AbstractWorker
{
    public const ACTION_KEY = 'action';

    //Exception messages
    public const ACTION_NOT_EXISTS = 'L\'action n\'existe pas.';
    public const FLUX_NOT_IMPLEMENTED_YET = "Ce flux n'est pas implémenté dans l'application.";

    public const FLUX_KEY = 'flux';
    public const FLUX_ACTION_KEY = 'flux_action';
    public const FLUX_CONTROLLER_KEY = 'controller';
    public const FLUX_OBJECT_ID_KEY = 'object_id';
    public const ACTION_SEND_TDT = 'sendTdt';
    public const ACTION_PREPARE_DOCUMENT_TDT = 'prepareDocumentOnPastell';

    public const ACTION_SEND_PARAPHEUR = 'sendParapheur';
    public const ACTION_CHECK_PARAPHEUR = 'checkStatusParapheur';
    public const ACTION_CHECK_MAIL_SEC = 'checkStatusMailSec';
    public const ACTION_SEND_MAIL_SEC = 'sendMailSec';

    /**
     * Le nom du scope de ce worker
     *
     * @var string
     */
    protected $_scope = 'abstract-pastell-base';

    /**
     * @inheritDoc
     */
    public function work($data)
    {
        switch ($data[self::FLUX_KEY]) {
            case Configure::read('Flux.actes-generique.name') || 'mailSec':
                return $this->dispatchAction($data);
            default:
                throw new BadRequestException(self::FLUX_NOT_IMPLEMENTED_YET);
        }
    }

    /**
     * Dispatch the job to the correct function by taking into account the action to execute
     *
     * @param mixed $data The job data
     * @return mixed
     */
    private function dispatchAction(array $data)
    {
        // La conf de min 1 seconde pour un program lancé par supervisord
        // D'ou le fait de forcer le worker a vivre au moins une seconde.
        sleep(1);
        switch ($data[self::ACTION_KEY]) {
            case self::ACTION_SEND_TDT:
                return $this->sendTdt($data);
            case self::ACTION_PREPARE_DOCUMENT_TDT:
                return $this->prepareDocumentOnPastell($data);
            case 'checkStateTdt':
                return $this->checkStateTdt($data);
            case 'cancelDocumentTdt':
                return $this->cancelDocumentTdt($data);
            case self::ACTION_SEND_PARAPHEUR:
                return $this->sendParapheur($data);
            case self::ACTION_CHECK_PARAPHEUR:
            case self::ACTION_CHECK_MAIL_SEC:
                return $this->checkStatus($data);
            case self::ACTION_SEND_MAIL_SEC:
                return $this->sendMailSec($data);
            default:
                throw new BadRequestException(self::ACTION_NOT_EXISTS);
        }
    }

    /**
     * Interroge l'api dans le but d'envoyer au TDT via PATELL.
     *
     * @param array $data Données permettant d'interroger l'api.
     * @return mixed
     */
    abstract protected function sendTdt($data);

    /**
     * Interroge l'api afin de préparer l'acte sur PASTELL
     *
     * @param array $data Données permettant d'interroger l'api.
     * @return mixed
     */
    abstract protected function prepareDocumentOnPastell($data);

    /**
     * @param array $data data
     * @return mixed
     */
    abstract protected function checkStateTdt($data);

    /**
     * @param array $data data
     * @return mixed
     */
    abstract protected function checkStatus($data);

    /**
     * @param array $data data
     * @return mixed
     */
    abstract protected function cancelDocumentTdt($data);

    /**
     * @param array $data data
     * @return mixed
     */
    abstract protected function sendParapheur($data);

    /**
     * @param array $data data
     * @return mixed
     */
    abstract protected function sendMailSec(array $data);
}
