<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use App\Model\Table\HistoriesTable;
use App\Service\NotificationService;
use Beanstalk\Utility\Beanstalk;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Log\Log;
use Exception;
use RuntimeException;

class SignaturePastellWorker extends BasicPastellWorker
{
    use ServiceAwareTrait;

    /**
     * @inheritDoc
     */
    public function __construct(Beanstalk $Beanstalk, $keepAlive = true, array $params = [])
    {
        parent::__construct($Beanstalk, $keepAlive, $params);
    }

    /**
     * Le nom du scope de ce worker
     *
     * @var string
     */
    protected $_scope = 'signature-pastell';

    /**
     * Gère l'envoi parapheur
     * * Patch sous type parapheur
     * * Patch document Principal
     * * Ètat du container dans [WA]
     *
     * @param array $data data
     * @return mixed|void
     */
    protected function sendParapheur($data)
    {
        $message = sprintf(
            '%s structure_id %s, object_id %s',
            __METHOD__,
            (string)$data['structure_id'],
            (string)$data['object_id'],
        );
        $this->log($message);

        try {
            $EntityPastellId = $this->Structures
                ->find()
                ->select('id_orchestration')
                ->where(
                    [
                        'id' => $data['structure_id'],
                    ]
                )->firstOrFail()->get('id_orchestration');

            $this->patchDocumentsPastell($data);

            /**
             * Patch sous-type parapheur
             */

            $this->patchSousTypeParapheur((string)$EntityPastellId, $data);

            $this->sendActionParapheur((string)$EntityPastellId, $data);
        } catch (RecordNotFoundException $e) {
            $message = sprintf(
                '[PASTELL-WEBACTES-SIGNATURE]' . $e->getCode() . '-' . 'Entité non renseignée dans PASTELL %s.',
                $data['structure_id']
            );
            Log::error($message, ['scope' => $this->_scope]);
            throw new RecordNotFoundException($message);
        } catch (Exception $exception) {
            Log::warning(
                sprintf(
                    "Impossible d'envoyer le projet (id container %s) en signature. \n Il est de nouveau en état VALIDATED",// @codingStandardsIgnoreLine
                    $data['object_id']
                ),
                ['scope' => $this->_scope]
            );
            $this->Structures->Containers->addStateact($data['object_id'], VALIDATED);
        }

        /**
         * @var \App\Service\NotificationService $notificationService
         */
        $notificationService = $this->loadService('Notification', [], false);

        $project = $this->Structures->Containers->Projects
            ->find()
            ->select(['id', 'structure_id', 'name'])
            ->contain(
                ['Containers' =>
                    [
                        'fields' => [
                            'Containers.id',
                            'Containers.project_id',
                        ],
                        'Typesacts',
                    ],
                ]
            )
            ->where([
                'id' =>
                    $this->Structures->Containers
                        ->find()
                        ->where(['id' => $data['object_id']])
                        ->disableHydration()
                        ->firstOrFail()['project_id'],
            ])
            ->firstOrFail();

        $notificationService->fireEvent(NotificationService::MY_PROJECT_AT_STATE, $project);

        return true;
    }

    /**
     * Modifie le document principal sur le document/dossier PASTELL.
     *
     * @param array $data Données nénessaire pour effectuer la modification.
     * @return bool|mixed
     * @throws \Exception
     */
    public function patchDocumentsPastell(array $data)
    {
        $message = sprintf(
            '%s structure_id %s, flux_name %s, object_id %s',
            __METHOD__,
            (string)$data['structure_id'],
            (string)$data['flux_name'],
            (string)$data['object_id'],
        );
        $this->log($message);

        $pastellFluxType_id = $this->Structures->Containers->Pastellfluxs->Pastellfluxtypes
            ->find()
            ->select('id')
            ->where(
                [
                    'structure_id' => $data['structure_id'],
                    'name' => $data['flux_name'],
                ]
            )->firstOrFail()->get('id');

        return $this->Structures->Containers
            ->buildPastellDocumentActesGeneriques(
                $data['object_id'],
                $pastellFluxType_id,
                $data['structure_id']
            );
    }

    /**
     * Renseigne le sous-type parapheur pour le document sur PASTELL
     *
     * @param string $EntityPastellId L'idientifiant unique du document sur PASTELL
     * @param array $data Données nécessaires pour la modification
     *                                          du document.
     * @return \Cake\Http\Client\Response
     */
    private function patchSousTypeParapheur(string $EntityPastellId, array $data)
    {
        sleep(1);

        $message = sprintf(
            '%s structure_id %s, sous-type %s, object_id %s',
            __METHOD__,
            (string)$data['structure_id'],
            (string)$data['sous-type'],
            (string)$data['object_id'],
        );
        $this->log($message);
        /**
         * Patch du document pour préciser le sous-type parapheur.
         */
        $data['externalField'] = [
            'name' => 'iparapheur_sous_type',
            'value' => ['iparapheur_sous_type' => $data['sous-type']],
        ];

        $data['id_d'] = $this->Structures->Containers->Pastellfluxs->find()
            ->select('id_d')
            ->where(
                [
                    'container_id' => $data['object_id'],
                ]
            )->orderDesc('id')->first()->get('id_d');

        $response = $this->Structures->Containers->patchPastellExternalData(
            $data['structure_id'],
            (int)$EntityPastellId,
            $data
        );

        return $response;
    }

    /**
     * Gère l'envoi de l'action send-parapheur sur pastell et les enregistrements de suivi sur [WA].
     *
     * @param array $EntityPastellId Identifiant unique du document sous PASTELL
     * @param array $data Données nécessaires pour
     *                                          effectuer l'action.
     * @return mixed|void
     */
    private function sendActionParapheur(string $EntityPastellId, array $data)
    {
        $message = sprintf(
            '%s structure_id %s, flux_name %s, object_id %s, user_id %s',
            __METHOD__,
            (string)$data['structure_id'],
            (string)$data['flux_name'],
            (string)$data['object_id'],
            (string)$data['user_id'],
        );
        $this->log($message);

        $documentSousPastell = $this->Structures->Containers->Pastellfluxs
            ->find()
            ->select('id_d')
            ->where(
                [
                    'container_id' => $data['object_id'],
                ]
            )
            ->orderDesc('id')
            ->first();

        $this->log('[PARAPHEUR SEND DOCUMENT PASTELL ID]' . ' - ' . $documentSousPastell['id_d']);

        $pastellFluxType_id = $this->Structures->Containers->Pastellfluxs->Pastellfluxtypes
            ->find()
            ->select('id')
            ->where(
                [
                    'structure_id' => $data['structure_id'],
                    'name' => $data['flux_name'],
                ]
            )->firstOrFail()->get('id');
        $this->log('[PARAPHEUR SEND DOCUMENT FLUXTYPES ID]' . ' - ' . $pastellFluxType_id);

        /**
         * ****** SEND PARAPHEUR ******
         */
        $response = $this->Structures->Containers->triggerAnActionOnDocument(
            $EntityPastellId,
            $documentSousPastell['id_d'],
            Configure::read('Flux.actes-generique.signature-electronique.action'),
            $data['structure_id']
        );

        if (
            isset($response['result'])
            && ($response['result'])
        ) {
            $this->Structures->Containers->addStateact($data['object_id'], PARAPHEUR_SIGNING);
            $this->log('Container id : ' . $data['object_id'] . 'PARAPHEUR_SIGNING');

            $this->Structures->Containers->savePastellFlux(
                $pastellFluxType_id,
                $documentSousPastell['id_d'],
                $data['object_id'],
                $this->Structures->Containers->Structures->get($data['structure_id'])->organization_id,
                $data['structure_id'],
                'DONE',
                $response['message']
            );

            $history = $this->Structures->Containers->Histories->newEntity(
                [
                    'structure_id' => $data['structure_id'],
                    'container_id' => $data['object_id'],
                    'user_id' => $data['user_id'],
                    'comment' => HistoriesTable::PARAPHEUR_SIGNING,
                ]
            );

            $this->Structures->Containers->Histories->save($history);
            $this->log('***********' . '[FIN SEND ACTION PARAPHEUR]' . '***********');

            return true;
        }

        return false;
    }

    /**
     * @param array $data data structure_id, container_id
     * @return mixed|void
     */
    protected function checkStatus($data)
    {
        $message = sprintf(
            '%s structure_id %s, container_id %s',
            __METHOD__,
            (string)$data['structure_id'],
            (string)$data['container_id'],
        );
        $this->log($message);

        try {
            //faire une methode pour cela !
            $entityPastellId = $this->Structures
                ->find()
                ->select('id_orchestration')
                ->where(['id' => $data['structure_id']])
                ->firstOrFail()
                ->get('id_orchestration');

            $flux = $this->Structures->Containers->Pastellfluxs
                ->find()
                ->where(['container_id' => $data['container_id']])
                ->orderDesc('id')
                ->firstOrFail();
            $pastellDocumentId = $flux->get('id_d');

            $this->log(sprintf(
                'container %s, pastell entity %s, pastell document %s',
                (string)$data['container_id'],
                (string)$entityPastellId,
                (string)$pastellDocumentId,
            ));

            $pastellClient = $this->getPastellClient($data['structure_id']);

            $status = $pastellClient->getIParapheurStatus($entityPastellId, $pastellDocumentId);

            if (!empty($status)) {
                $this->log('[I-PARAPHEUR STATUS DOCUMENT]' . '-' . $status['status']);
                $notificationService = $this->loadService('Notification', [], false);

                switch ($status['status']) {
                    case 'Lu':
                    case 'NonLu':
                        $this->Structures->Containers->savePastellFluxAction(
                            'iparapheur_historique',
                            $flux->get('id'),
                            $this->Structures->get($data['structure_id'])->organization_id,
                            $data['structure_id'],
                            isset($status['error-message']) ? self::STATE_FAILED : self::STATE_PENDING,
                        );
                        break;
                    case 'Signe':
                        $project = $this->Structures->Containers->Projects
                            ->find()
                            ->contain([
                                'Containers' => ['Typesacts', 'fields' => ['Containers.project_id', 'Containers.id']],
                            ])
                            ->innerJoinWith('Containers')
                            ->where(['Containers.id' => $data['container_id']])
                            ->firstOrFail();
                        $project = $this->Structures->Containers->Projects->patchEntity(
                            $project,
                            ['signature_date' => $status['signature_date']]
                        );
                        if ($this->Structures->Containers->Projects->save($project) === false) {
                            $message = sprintf(
                                'Impossible d\'enregistrer la date de signature pour le projet %d: %s',
                                $project->id,
                                print_r($project->getErrors(), true)
                            );
                            throw new RuntimeException($message);
                        }
                        $this->Structures->Containers->addStateact($data['container_id'], PARAPHEUR_SIGNED);

                        $this->Structures->Containers->savePastellFluxAction(
                            'recu-iparapheur',
                            $flux->get('id'),
                            $this->Structures->get($data['structure_id'])->organization_id,
                            $data['structure_id'],
                            self::STATE_DONE,
                            $status['status'] . ' : ' . $status['annotation']
                        );
                        $history = $this->Structures->Containers->Histories->newEntity(
                            [
                                'structure_id' => $data['structure_id'],
                                'container_id' => $data['container_id'],
                                'user_id' => null,
                                'comment' => HistoriesTable::PARAPHEUR_SIGNED,
                            ]
                        );

                        if ($this->Structures->Containers->Histories->save($history, ['checkRules' => false])) {
                            $notificationService->fireEvent(
                                NotificationService::MY_PROJECT_AT_STATE,
                                $project,
                                [
                                    'last_user_id' => null,
                                ]
                            );
                        }

                        break;
                    case 'RejetSignataire':
                        $this->Structures->Containers->addStateact($data['container_id'], PARAPHEUR_REFUSED);

                        $this->Structures->Containers->savePastellFluxAction(
                            'rejet-iparapheur',
                            $flux->get('id'),
                            $this->Structures->get($data['structure_id'])->organization_id,
                            $data['structure_id'],
                            self::STATE_DONE,
                            $status['status'] . ' : ' . $status['annotation']
                        );

                        $history = $this->Structures->Containers->Histories->newEntity(
                            [
                                'structure_id' => $data['structure_id'],
                                'container_id' => $data['container_id'],
                                'user_id' => null,
                                'comment' => HistoriesTable::PARAPHEUR_REFUSED,
                            ]
                        );

                        $this->Structures->Containers->Histories->save($history, ['checkRules' => false]);
                        break;
                    default:
                        return;
                }

                return;
            }
        } catch (RecordNotFoundException $e) {
            $message = sprintf(
                '[PASTELL-WEBACTES]' . $e->getCode() . '-' . 'Entité non renseignée dans PASTELL : %s.',
                $data['structure_id']
            );
            Log::error($message, ['scope' => $this->_scope]);
            throw new RecordNotFoundException($message);
        }
    }
}
