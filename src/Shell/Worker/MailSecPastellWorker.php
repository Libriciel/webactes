<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use App\Utilities\Common\RealTimezone;
use Cake\Log\Log;
use Psr\Log\LogLevel;

class MailSecPastellWorker extends BasicPastellWorker
{
    /**
     * Le nom du scope de ce worker
     *
     * @var string
     */
    protected $_scope = 'mail-sec-pastell';

    /**
     * Récupère les destinataires d'un dossier de mail sécurisé dont la date de lecture pourrait être mise à jour.
     *
     * @param string $structureId L'id de l'entité Pastell
     * @param string $pastellId L'id du dossier Pastell
     * @return array|mixed
     */
    protected function getEmailInfos(string $structureId, string $pastellId)
    {
        $entityPastellId = $this->Structures
            ->find()
            ->select('id_orchestration')
            ->where(['id' => $structureId])
            ->firstOrFail()
            ->get('id_orchestration');

        $response = $this
            ->getPastellClient($structureId)
            ->getDocument($entityPastellId, $pastellId)
            ->getJson();

        // @info: on ne sait pas quelle sera la position de l'adresse "factice"
        $dummyEmail = env('PASTELL_MAILSEC_TO', 'webactes@webactes.dev.libriciel.net');

        return array_filter(
            $response['email_info'],
            function ($value) use ($dummyEmail) {
                return $value['lu'] != '0' && $value['email'] !== $dummyEmail;
            },
            ARRAY_FILTER_USE_BOTH
        );
    }

    /**
     * @param array $data data
     * @return mixed|void
     */
    public function checkStatus($data)
    {
        $message = sprintf(
            '%s structure_id %s, pastell_id %s',
            __METHOD__,
            (string)$data['structure_id'],
            (string)$data['pastell_id'],
        );
        $this->log($message);

        try {
            $emailInfos = $this->getEmailInfos((string)$data['structure_id'], (string)$data['pastell_id']);

            foreach ($emailInfos as $emailInfo) {
                $convocation = $this->Structures->Convocations
                    ->find()
                    ->innerJoinWith('Actors')
                    ->where([
                        'Convocations.pastell_id' => (string)$emailInfo['id_d'],
                        'Actors.email' => $emailInfo['email'],
                    ])
                    ->firstOrFail();

                $dateOpen = RealTimezone::fromPastellToSql($emailInfo['date_lecture']);
                $convocation = $this->Structures->Convocations->patchEntity($convocation, ['date_open' => $dateOpen]);

                if ($convocation->isDirty('date_open') === true) {
                    $save = $this->Structures->Convocations->save($convocation);
                    if ($save === false) {
                        $msgid = 'Impossible de mettre à jour la date de lecture de la convocation %d: %s';
                        $message = sprintf($msgid, $convocation->id, var_export($convocation->getErrors(), true));
                        Log::write(LogLevel::ERROR, $message, ['scope' => $this->_scope]);
                    } else {
                        $msgid = 'Mise à jour de la date de lecture de la convocation %d: %s (UTC)';
                        $message = sprintf($msgid, $convocation->id, $dateOpen);
                        Log::write(LogLevel::DEBUG, $message, ['scope' => $this->_scope]);
                    }
                }
            }
        } catch (\Throwable $exc) {
            $message = sprintf(
                'Impossible de mettre à jour les informations de convocations : %s',
                $exc->getMessage()
            );
            Log::error($message, ['scope' => $this->_scope]);
        }
    }

    /**
     * Send mail secure
     *
     * @param array $data summonId, pastellFluxId, currentUserId
     * @return void
     */
    public function sendMailSec(array $data)
    {
        /** @var \App\Model\Table\SummonsTable $summon */
        $summon = $this->fetchTable('Summons');
        $summon->buildPastellDocumentMailSec(
            $data['summonId'],
            $data['pastellFluxId'],
            $data['currentUserId']
        );
    }
}
