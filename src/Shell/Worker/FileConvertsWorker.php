<?php
declare(strict_types=1);

namespace App\Shell\Worker;

use App\Exception\GenerationException;
use App\Model\Table\GenerationerrorsTable;
use App\Model\Table\WorkergenerationsTable;
use App\Utilities\Common\ConfigureIniSettingsTrait;
use Beanstalk\Utility\Beanstalk;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;

/**
 * Class FileConvertsWorker
 *
 * @property \App\Model\Table\MaindocumentsTable $Maindocuments
 * @property \App\Model\Table\FilesTable $Files
 * @package App\Shell\Worker
 */
class FileConvertsWorker extends AbstractGenerationWorker
{
    use ConfigureIniSettingsTrait;
    use ServiceAwareTrait;

    public const TYPE_NOT_IMPLEMENTED_YET = 'Il n\'existe pas encore de conversion pour ce type : %s';
    public const ACTION_NOT_EXISTS = 'l\'action %s n\'existe pas ou n\'est pas encore implémentée';

    /**
     * Le nom du scope de ce worker
     *
     * @var string
     */
    protected $_scope = 'file-converts';

    /**
     * @inheritDoc
     */
    public function work($data)
    {
        if (in_array($data['action'], ['convertFileAnnex'])) {
            return $this->dispatchAction($data);
        }
        throw new BadRequestException(sprintf(
            self::TYPE_NOT_IMPLEMENTED_YET,
            $data['action']
        ));
    }

    /**
     * Dispatch the job to the correct function by taking into account the action to execute
     *
     * @param mixed $data The job data
     * @return mixed
     */
    private function dispatchAction($data)
    {
        $this->log(self::class . '*******' . __FUNCTION__ . '*******');

        switch ($data['action']) {
            case 'convertFileAnnex':
                $this->convertFileAnnex($data);
                break;
            default:
                throw new BadRequestException(self::ACTION_NOT_EXISTS);
        }

        return true;
    }

    /**
     * Checks that the project got generated or re-launches the same beanstalk message with a delay.
     * Returns true if a retry was launched, false otherwise.
     *
     * @param int $projectId The projet id (projects.id)
     * @param array $data The data to transmit through beanstalk for the potential retry
     * @param int $delay The delay before retrying
     * @return bool
     */
    protected function handleRetryWithDelay(int $projectId, array $data, int $delay = 30): bool
    {
        $workergeneration = $this->fetchTable('Workergenerations')
            ->find()
            ->select(['pid'])
            ->where(['type' => 'act', 'foreign_key' => $projectId])
            ->enableHydration(false)
            ->first();

        if (!empty($workergeneration)) {
            $messageBase = sprintf(
                'Le projet "%d" est toujours en cours de génération (pid %d)',
                $projectId,
                $workergeneration['pid']
            );
            $message = sprintf("{$messageBase} Attente de %d secondes avant de réessayer.", $delay);
            $this->log($message);

            $beanstalk = new Beanstalk('file-converts');
            $beanstalk->emit(
                $data,
                Configure::read('Beanstalk.Priority.VERY_HIGHT'),
                $delay,
                (int)env('PDF2ODT_BEANSTALK_TTR', 60 * 60 * 4) // 4 heures par défaut
            );

            return true;
        }

        return false;
    }

    /**
     * Generate project.
     *
     * @param array $data Données nécessaire à l'action
     * @return void
     * @throws \App\Shell\Worker\App\Exception\GenerationException
     */
    protected function convertFileAnnex(array $data)
    {
        $this->log(sprintf('Début de %s pour les annexes d\'id %s', __METHOD__, implode(', ', $data['ids'])));
        $this->applyConfigureIniSettings(__METHOD__);

        $this->Annexes = $this->fetchTable('Annexes');

        // @info: normalement, toutes ces annexes ne concernent qu'un seul projet
        $projects = [];
        $tmps = $this->Annexes->Containers
            ->find()
            ->select(['Containers.project_id', 'Annexes.id'])
            ->innerJoinWith('Annexes')
            ->where(['Annexes.id IN' => $data['ids']])
            ->enableHydration(false)
            ->toArray();
        foreach ($tmps as $tmp) {
            if (!isset($projects[$tmp['project_id']])) {
                $projects[$tmp['project_id']] = [];
            }
            $projects[$tmp['project_id']][] = $tmp['_matchingData']['Annexes']['id'];
        }

        foreach ($projects as $projectId => $annexesIds) {
            if (
                !$this->handlePreviousFatalError(__METHOD__, 'act', $projectId, $data)
                && !$this->handleRetryWithDelay($projectId, ['ids' => $annexesIds, 'action' => 'convertFileAnnex'])
            ) {
                try {
                    WorkergenerationsTable::persist(getmypid(), 'act', $projectId);
                    $this->loadService('Converts', ['templateTypeCode' => 'annex_id', 'ids' => $annexesIds]);
                    $this->Converts->convertFiles();
                } catch (\InvalidArgumentException $exc) {
                    $message = sprintf('CONVERT EXCEPTION (code %d): %s', $exc->getCode(), $exc->getMessage());
                    $this->log($message);

                    GenerationerrorsTable::persist('act', $projectId, $exc->getMessage());
                    throw new GenerationException('act', $projectId, $exc->getMessage(), $exc->getCode());
                } finally {
                    // This block will be executed everytime, except for \Cake\Error\FatalErrorException
                    $this->Converts = null;
                    WorkergenerationsTable::cleanup(getmypid(), 'act', $projectId);
                }
            }
        }

        $this->log(sprintf('Fin de %s pour les annexes d\'id %s', __METHOD__, implode(', ', $data['ids'])));
    }
}
