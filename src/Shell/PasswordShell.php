<?php
declare(strict_types=1);

namespace App\Shell;
// @codingStandardsIgnoreFile
use App\Model\Entity\Auth;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Exception;

class PasswordShell extends AppShell
{
    /**
     * @param \Cake\Console\Arguments $args "foo"
     * @param \Cake\Console\ConsoleIo $io "foo"
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->out('Password Command');
    }

    protected $keycloakAuth = null;

    /**
     * get the keycloakAuth client
     *
     * @return \App\Model\Entity\Auth
     */
    protected function getKeycloakAuth(): Auth
    {
        if ($this->keycloakAuth === null) {
            $auth = new Auth();
            $token = $auth->getToken(KEYCLOAK_TEST_USER, KEYCLOAK_TEST_PASSWORD);
            $auth->setToken($token);
            $this->keycloakAuth = $auth;
        }

        return $this->keycloakAuth;
    }

    /**
     * upadteKc password
     *
     * @param  array  $keycloakId userInfo
     * @param  string $password   password
     * @return void
     * @throws \Exception
     */
    private function updatePasswordKeycloakUser($keycloakId, $password)
    {
        $userKc = [
            'credentials' => [[
                'type' => 'password',
                'value' => $password,
                'temporary' => false,
            ]],
        ];

        $auth = $this->getKeycloakAuth();

        $res = $auth->updateUser($keycloakId, $userKc);
        if (!$res['success']) {
            throw new Exception('Exception updateUser');
        }
    }

    /**
     * get the keycloak id
     *
     * @param  string $username username
     * @return mixed
     */
    public function getKeycloakId($username)
    {
        $auth = $this->getKeycloakAuth();
        $res = $auth->getUsersQuery('username=' . urlencode($username));

        return $res[0]['id'];
    }

    /**
     * set password
     *
     * @return int
     */
    public function setPassword()
    {
        $password = $this->param('password');
        $lastname = $this->param('lastname');

        if (empty($password) || empty($lastname)) {
            throw new Exception('password and lastname must be set');
        }

        $Users = $this->fetchTable('Users');
        $users = $Users->find()->select('username')->where(['lastname' => $lastname])->toArray();

        foreach ($users as $user) {
            $kcId = $this->getKeycloakId($user['username']);
            $this->updatePasswordKeycloakUser($kcId, $password);
        }

        return 0;
    }

    /**
     * Complete the option parser for this shell.
     *
     * @param \Cake\Console\ConsoleOptionParser $parser foo
     * @return \Cake\Console\ConsoleOptionParser
     */
    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addSubcommand(
            'setPassword',
            [
            'help' => __('set password in keycloak'),
            'parser' => [
                'description' => [
                    __('Use this command to grant ACL permissions. Once executed, the '),
                    __('ARO specified (and its children, if any) will have ALLOW access '),
                    __("to the specified ACO action (and the ACO's children, if any)."),
                ],
                'arguments' => [
                    'aro' => ['help' => __('ARO to check.'), 'required' => true],
                    'aco' => ['help' => __('ACO to check.'), 'required' => true],
                    'action' => ['help' => __('Action to check')],
                ],
            ],
            ]
        );

        //        $parser->setDescription(__('set password in keycloak'))
        //            ->addArgument(
        //                'setPassword',
        //                [
        //                    'help' => __('set password in keycloak'),
        //                ]
        //            )
        //            ->addArguments([
        //                'lastname' => [
        //                    'help' => 'Lastname of user to update',
        //                    'short' => 'l',
        //                ],
        //                'password' => [
        //                    'help' => 'password',
        //                    'short' => 'p',
        //                ],
        //            ]);

        return $parser;
    }
}
