<?php
declare(strict_types=1);

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\Locator\LocatorAwareTrait;
use DateTimeImmutable;
use Laminas\Diactoros\UploadedFile;

class DockerStatsShell extends AppShell
{
    use LocatorAwareTrait;

    /**
     * @var \App\Model\Table\ProjectsTable
     */
    protected $Projects;

    /**
     * @var \App\Model\Table\SittingsTable
     */
    protected $Sittings;

    /**
     * @var \App\Model\Table\StructuresTable
     */
    protected $Structures;

    /**
     * @var \App\Model\Table\UsersTable
     */
    protected $Users;

    protected int $structureId;
    protected int $userId;
    protected string $annexePath;
    protected int $sittingId;
    protected int $sleep;

    /**
     * Copies the file to upload and returns an UploadedFile object.
     *
     * @param string $path The path to the file to upload
     * @return \Laminas\Diactoros\UploadedFile
     */
    protected function createUploadedFile(string $path): UploadedFile
    {
        $tempnam = tempnam('/tmp', self::class);
        if (!$tempnam) {
            $message = sprintf('Could not create a tempnam in "%s" with "%s"', '/tmp', self::class);
            throw new RuntimeException($message);
        }
        if (!copy($path, $tempnam)) {
            $message = sprintf('Could not copy from "%s" to "%s"', $path, $tempnam);
            throw new RuntimeException($message);
        }

        return new UploadedFile(
            $tempnam,
            filesize($tempnam),
            UPLOAD_ERR_OK,
            basename($path),
            'application/pdf'
        );
    }

    /**
     * Returns the number of seconds of a DateInterval.
     *
     * @param \DateInterval $dateInterval The interval
     * @return int seconds
     * @see https://stackoverflow.com/a/25149337
     */
    protected function dateIntervalToSeconds($dateInterval)
    {
        $reference = new DateTimeImmutable();
        $endTime = $reference->add($dateInterval);

        return $endTime->getTimestamp() - $reference->getTimestamp();
    }

    /**
     * Shell startup, parameters initialisation.
     *
     * @return void
     */
    public function startup(): void
    {
        parent::startup();

        $this->Projects = $this->fetchTable('Projects');
        $this->Sittings = $this->fetchTable('Sittings');
        $this->Structures = $this->fetchTable('Structures');
        $this->Users = $this->fetchTable('Users');

        $this->annexePath = (string)$this->param('annexe');
        if (!file_exists($this->annexePath)) {
            $this->err(sprintf('Le fichier "%s" n\'existe pas', $this->annexePath));
            $this->_stop(self::CODE_ERROR);
        }
        $this->structureId = $this->getStructureId(str_replace('_', ' ', $this->param('structure')));
        $this->userId = $this->getUserId($this->param('user'));
        $this->sittingId = (int)$this->param('sitting');
        $this->sleep = (int)$this->param('sleep');
    }

    /**
     * Returns the structure primary key given its name.
     *
     * @param string $businessName The structure's name
     * @return int
     */
    protected function getStructureId(string $businessName): int
    {
        try {
            return $this->Structures
                ->find()
                ->select(['id'])
                ->where(['business_name' => $businessName])
                ->enableHydration(false)
                ->firstOrFail()['id'];
        } catch (RecordNotFoundException $exc) {
            $message = sprintf(
                'Record not found in table "%s" for business_name "%s"',
                $this->Structures->getTable(),
                $businessName
            );
            throw new RecordNotFoundException($message);
        }
    }

    /**
     * Returns the user primary key given its username.
     *
     * @param string $username The username
     * @return int
     */
    protected function getUserId(string $username): int
    {
        try {
            return $this->Users
                ->find()
                ->select(['id'])
                ->where(['username' => $username])
                ->enableHydration(false)
                ->firstOrFail()['id'];
        } catch (RecordNotFoundException $exc) {
            $message = sprintf(
                'Record not found in table "%s" for username "%s"',
                $this->Users->getTable(),
                $username
            );
            throw new RecordNotFoundException($message);
        }
    }

    /**
     * Main function, try to generate a project with the given annexe and wait for it to succeed of fail.
     *
     * @return void
     */
    public function main()
    {
        $last = $this->Projects->find()->select(['id'])->orderDesc('created')->enableHydration(false)->first();

        $name = sprintf('Projet %05d', $last === null ? 1 : $last['id'] + 1);

        $data = [
            'project' => [
                'typesact_id' => 15,
                'stateact_id' => 1,
                'theme_id' => null,
                'matiere_id' => null,
                'sittings' => [$this->sittingId],
                'name' => $name,
                'ismultichannel' => false,
                'code_act' => null,
                'maindocument' => [
                    'codetype' => '99_DE',
                ],
                'annexes' => [
                    [
                        'is_generate' => true,
                        'istransmissible' => false,
                        'name' => 'annexe.pdf',
                        'codetype' => null,
                    ],
                ],
            ],
            'annexes' => [
                $this->createUploadedFile($this->annexePath),
            ],
        ];

        $project = $this->Projects->generateProject($this->structureId, $this->userId, $data);

        if ($project->hasErrors()) {
            dump($project->hasErrors());
            $this->_stop(static::CODE_ERROR);
        }

        do {
            $record = $this->Projects
                ->find('all')
                ->select(['id', 'created'])
                ->where(['id' => $project->id])
                ->contain([
                    'Containers' => [
                        'fields' => ['id', 'project_id'],
                    ],
                    'Containers.Maindocuments' => [
                        'fields' => ['id'],
                    ],
                    'Containers.Maindocuments.Files' => [
                        'fields' => ['maindocument_id', 'name', 'path', 'modified'],
                    ],
                    'Generationerrors' => [
                        'fields' => ['foreign_key', 'message'],
                    ],
                ])
                ->enableHydration(false)
                ->first();

            $error = !empty($record['generationerrors'][0]['message']);
            if ($error) {
                dump($record['generationerrors']);
                $this->_stop(static::CODE_ERROR);
            }

            $try = empty($record['containers'][0]['maindocument']['files']);
            if ($try) {
                sleep($this->sleep);
            }
        } while ($try);

        $interval = $record['created']->diff($record['containers'][0]['maindocument']['files'][0]['modified']);
        $message = sprintf(
            'Fin de la génération du projet "%s" (%d) au bout de %s secondes',
            $name,
            $record['id'],
            $this->dateIntervalToSeconds($interval)
        );
        $this->out($message);
    }

    /**
     * @inheritDoc
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();

        $parser->addOptions(
            [
                'annexe' => [
                    'help' => 'Le chemin relatif vers l\'annexe',
                    'required' => true,
                    'short' => 'a',
                ],
                'sitting' => [
                    'help' => 'L\'id de la séance',
                    'required' => true,
                    'short' => 'i',
                ],
                'sleep' => [
                    'default' => 5,
                    'help' => 'Le nombre de secondes à attendre avant de vérifier la génération du document d\'acte',
                    'required' => true,
                    'short' => 'i',
                ],
                'structure' => [
                    'help' => 'Le nom de la structure (remplacer les espaces par des underscore)',
                    'required' => true,
                    'short' => 's',
                ],
                'user' => [
                    'help' => 'L\'identifiant de l\'utilisateur',
                    'required' => true,
                    'short' => 'u',
                ],
            ]
        );

        return $parser;
    }
}
