<?php
declare(strict_types=1);

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\ORM\Locator\TableLocator;
use Cake\Utility\Inflector;

/**
 * Usage: bin/cake trees_recovery
 */
class TreesRecoveryShell extends AppShell
{
    /**
     * @return void
     */
    public function main()
    {
        $tableLocator = new TableLocator();
        $usersTable = $tableLocator->get('Users');
        $tables = $usersTable->getConnection()->getSchemaCollection()->listTables();
        foreach ($tables as $name) {
            $table = $tableLocator->get(Inflector::camelize($name));
            if ($table->hasBehavior('Tree') === true) {
                $this->out(sprintf('Récupération des arbres pour la table %s', $name));
                $table->recover();
            }
        }
    }

    /**
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();

        $parser->setDescription('Récupération des arbres pour les tables utilisant cette fonctionnalité');

        return $parser;
    }
}
