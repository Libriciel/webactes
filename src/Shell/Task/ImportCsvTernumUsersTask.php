<?php
declare(strict_types=1);

namespace App\Shell\Task;

use App\Model\Entity\Auth;
use App\Model\Table\RolesTable;
use Cake\Console\Shell;
use Exception;
use RuntimeException;
use Throwable;
// @codingStandardsIgnoreFile

class ImportCsvTernumUsersTask extends Shell
{
    protected $_keycloakIds = [];

    protected $_keycloakAuth = null;

    /**
     * get the keycloakAuth client
     *
     * @return \App\Model\Entity\Auth|null
     */
    protected function _getKeycloakAuth()
    {
        if ($this->_keycloakAuth === null) {
            $auth = new Auth();
            $token = $auth->getToken(KEYCLOAK_USER, KEYCLOAK_PASSWORD);
            $auth->setToken($token);
            $this->_keycloakAuth = $auth;
        }

        return $this->_keycloakAuth;
    }

    /**
     * Returns the headers row for the CSV file.
     *
     * @return array
     */
    public function getHeaders()
    {
        return ['id', 'active', 'firstname', 'lastname', 'username', 'email', 'PASSWORD', 'structure_id', 'profil'];
    }

    /**
     * Returns the value of the displayField for the row.
     *
     * @param  array $row The candidate row to process
     * @return mixed
     */
    public function getDisplayFieldValue(array $row)
    {
        $headers = array_flip($this->getHeaders());

        return $row[$headers['username']];
    }

    /**
     * Try to insert a candidate row in the database
     *
     * @param  int   $lineNumber The line number in the CSV file
     * @param  array $row        The candidate row to process
     * @return \Cake\ORM\Entity
     */
    public function processRow(int $lineNumber, array $row)
    {
        $this->Organizations = $this->fetchTable('Organizations');

        $headers = array_flip($this->getHeaders());

        $roleName = null;

        if ($row[$headers['profil']] === 'super_admin') {
            $roleName = RolesTable::SUPER_ADMINISTRATOR;
        } elseif ($row[$headers['profil']] === 'admin') {
            $roleName = RolesTable::ADMINISTRATOR;
        } elseif ($row[$headers['profil']] === 'user') {
            $roleName = RolesTable::VALIDATOR_REDACTOR_NAME;
        } elseif ($row[$headers['profil']] === 'admin_fonctionnel') {
            $roleName = RolesTable::FUNCTIONNAL_ADMINISTRATOR;
        } elseif ($row[$headers['profil']] === 'secretariat_general') {
            $roleName = RolesTable::GENERAL_SECRETARIAT;
        }

        $structure = $this->Organizations->Structures
            ->find()
            ->select()
            ->where(
                [
                'id_external' => (int)$row[$headers['structure_id']],
                ]
            )
            ->enableHydration(false)
            ->first();

        if (empty($structure) === true) {
            $msgstr = sprintf('Structure with external id %d not found', $row[$headers['structure_id']]);
            throw new RuntimeException($msgstr);
        }


        $service = $this->Organizations->Structures->Services
            ->find()
            ->select()
            ->where(
                [
                'name' => $this->param('service'),
                'structure_id' => $structure['id'],
                ]
            )
            ->enableHydration(false)
            ->first();
        if (empty($service) === true) {
            $msgstr = sprintf(
                'Service with name "%s" belonging to structure with id_external %d not found',
                $this->param('service'),
                $row[$headers['structure_id']]
            );
            throw new RuntimeException($msgstr);
        }

        $role = $this->Organizations->Structures->Roles
            ->find()
            ->select()
            ->where(
                [
                'name' => $roleName,
                'structure_id' => $structure['id'],
                ]
            )
            ->enableHydration(false)
            ->first();
        if (empty($role) === true) {
            $msgstr = sprintf(
                'Role with name %s belonging to structure with id_external %d not found',
                $roleName,
                $row[$headers['structure_id']]
            );
            throw new RuntimeException($msgstr);
        }

        $user = $this->Organizations->Structures->Users
            ->find()
            ->where(['id_external' => (int) $row[$headers['id']]])
            ->first();


        if (empty($user) === true) {
            $mode = 'insert';
            $user = $this->Organizations->Structures->Users->newEmptyEntity();
        } else {
            $mode = 'update';
        }

        $data = [
            'organization_id' => $this->param('organization_id'),
            'active' => empty($row[$headers['active']]) === false,
            'lastname' => strtolower($row[$headers['lastname']]),
            'firstname' => strtolower($row[$headers['firstname']]),
            'username' => strtolower($row[$headers['username']]),
            'email' => strtolower($row[$headers['email']]),
            'structure_id' => $structure['id'],
            'data_security_policy' => false,
            'id_external' => (int)$row[$headers['id']],
            'roles' => [
                [
                    'id' => $role['id'],
                ],
            ],
            'services' => [
                [
                    'id' => $service['id'],
                ],
            ],
            'structures' => [
                [
                    'id' => $structure['id'],
                ],
            ],
        ];

        /**
         * @var $user User
         */
        $user = $this->Organizations->Structures->Users->patchEntity(
            $user,
            $data,
            [
                'associated' => [
                    'Roles',
                    'Services',
                    'Structures',
                ],
            ]
        );

        if ($user->hasErrors() === false) {
            try {
                if (empty($user['keycloak_id']) || $mode === 'insert') {
                    $kcId = $this->__createKeycloakUser($data);
                    $data['keycloak_id'] = $kcId;
                    $user = $this->Organizations->Structures->Users->patchEntity($user, $data);
                }
            } catch (Throwable $exception) {
                $user->setError('keycloak_id', $exception->getMessage());
            }
            $this->Organizations->Structures->Users->save($user, ['atomic' => false]);
        }

        return [$mode, $user];
    }

    /**
     * Rollback
     *
     * @return void
     */
    public function onRollback()
    {
        $auth = $this->_getKeycloakAuth();
        foreach ($this->_keycloakIds as $id) {
            $auth->deleteUser($id);
        }
    }

    /**
     * create the user in keycloak
     *
     * @param  array $user userInfo
     * @return int
     */
    private function __createKeycloakUser($user)
    {
        $userKc = [
            'firstName' => $user['firstname'],
            'lastName' => $user['lastname'],
            'username' => $user['username'],
            'email' => $user['email'],
            'enabled' => $user['active'],
        ];

        $auth = $this->_getKeycloakAuth();

        $res = $auth->addUser($userKc);
        if (!$res['success']) {
            throw new Exception('impossible de creer l\'utilisateur dans keycloak : ' . str_replace("\n", ' ', var_export($res, true)));
        }

        $this->_keycloakIds[] = $res['id'];

        return $res['id'];
    }
}
