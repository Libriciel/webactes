<?php
declare(strict_types=1);

namespace App\Shell\Task;

use App\Model\Table\RolesTable;
use Cake\Console\Shell;
use Cake\Utility\Hash;

class ImportCsvTernumStructuresTask extends Shell
{
    /**
     * Returns the headers row for the CSV file.
     *
     * @return array
     */
    public function getHeaders()
    {
        return ['id', 'id_pastell', 'name'];
    }

    /**
     * Rollback
     *
     * @return void
     */
    public function onRollback()
    {
    }

    /**
     * Returns the value of the displayField for the row.
     *
     * @param  array $row The candidate row to process
     * @return mixed
     */
    public function getDisplayFieldValue(array $row)
    {
        $headers = array_flip($this->getHeaders());

        return $row[$headers['name']];
    }

    /**
     * Try to insert a candidate row in the database
     *
     * @param  int   $lineNumber The line number in the CSV file
     * @param  array $row        The candidate row to process
     * @return \Cake\ORM\Entity
     */
    public function processRow(int $lineNumber, array $row)
    {
        $this->Organizations = $this->fetchTable('Organizations');

        $headers = array_flip($this->getHeaders());

        $structure = $this->Organizations->Structures
            ->find()
            ->contain(['Roles' => ['sort' => ['name' => 'asc']]])
            ->where(['id_external' => $row[$headers['id']]])
            ->first();

        if (empty($structure) === true) {
            $mode = 'insert';
            $structure = $this->Organizations->Structures->newEmptyEntity();
        } else {
            $mode = 'update';
        }

        $data = [
            'organization_id' => $this->param('organization_id'),
            'active' => true,
            'business_name' => $row[$headers['name']],
            'id_orchestration' => $row[$headers['id_pastell']],
            'id_external' => $row[$headers['id']],
            'roles' => [
                [
                    'name' => RolesTable::ADMINISTRATOR,
                ],
                [
                    'name' => RolesTable::VALIDATOR_REDACTOR_NAME,
                ],
                [
                    'name' => RolesTable::GENERAL_SECRETARIAT,
                ],
                [
                    'name' => RolesTable::FUNCTIONNAL_ADMINISTRATOR,
                ],
                [
                    'name' => RolesTable::SUPER_ADMINISTRATOR,
                ],
            ],
        ];

        $paths = ['roles.0.id', 'roles.1.id'];
        foreach ($paths as $path) {
            $value = Hash::get($structure, $path);
            if (empty($value) === false) {
                $data = Hash::insert($data, $path, $value);
            }
        }

        $structure = $this->Organizations->Structures->patchEntity($structure, $data, ['associated' => ['Roles']]);

        if ($structure->hasErrors() === false) {
            $tmp = $this->Organizations->Structures->save($structure, ['atomic' => false]);

            if ($tmp !== false) {
                $service = $this->Organizations->Structures->Services
                    ->find()
                    ->where(
                        [
                        'structure_id' => $structure->id,
                        'name' => $this->param('service'),
                        ]
                    )
                    ->first();
                if (empty($service) === true) {
                    $service = $this->Organizations->Structures->Services->newEmptyEntity();
                }

                $service = $this->Organizations->Structures->Services->patchEntity(
                    $service,
                    [
                        'structure_id' => $structure->id,
                        'active' => true,
                        'name' => $this->param('service'),
                    ]
                );
                $tmp = $this->Organizations->Structures->Services->save($service, ['atomic' => false]);

                $this->addPastellFluxTypes($structure->id);

                if ($tmp === false) {
                    $structure->setErrors($tmp);
                }
            } else {
                $structure->setErrors($tmp);
            }
        }

        return [$mode, $structure];
    }

    /**
     * add pastell flux
     *
     * @param  int $structureId structureId
     * @return void
     */
    private function addPastellFluxTypes($structureId)
    {
        $data = [
            'organization_id' => $this->param('organization_id'),
            'structure_id' => $structureId,
            'active' => true,
            'name' => 'actes-generique',
            'description' => 'Actes générique de test',
        ];

        $Pastellfluxtypes = $this->fetchTable('Pastellfluxtypes');
        $fluxType = $Pastellfluxtypes->newEntity($data);
        $Pastellfluxtypes->save($fluxType);

        $data = [
            'organization_id' => $this->param('organization_id'),
            'structure_id' => $structureId,
            'active' => true,
            'name' => 'mailsec',
            'description' => 'Mail securisé',
        ];

        $Pastellfluxtypes = $this->fetchTable('Pastellfluxtypes');
        $fluxType = $Pastellfluxtypes->newEntity($data);
        $Pastellfluxtypes->save($fluxType);
    }
}
