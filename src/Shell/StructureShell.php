<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 06/09/19
 * Time: 08:41
 */

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Exception;
use Psr\Log\LogLevel;

class StructureShell extends AppShell
{
    /**
     * Add missing flux
     *
     * @return int
     */
    public function addMissingFluxType()
    {
        $Structures = $this->fetchTable('Structures');
        $Pastellfluxtypes = $this->fetchTable('Pastellfluxtypes');
        $structures = $Structures->find()->select(['id'])->contain(['Pastellfluxtypes'])->toArray();

        $emptyStructures = [];
        foreach ($structures as $structure) {
            if (empty($structure['pastellfluxtypes'])) {
                array_push($emptyStructures, $structure['id']);
            }
        }

        if (empty($emptyStructures)) {
            $this->log('Nothing to do', LogLevel::DEBUG);

            return 0;
        }

        foreach ($emptyStructures as $emptyStructure) {
            $this->out($emptyStructure);
            $data = [
                'organization_id' => 1,
                'structure_id' => $emptyStructure,
                'active' => true,
                'name' => 'actes-generique',
                'description' => 'Actes générique de test',
            ];

            $fluxType = $Pastellfluxtypes->newEntity($data);
            $Pastellfluxtypes->save($fluxType);
            if ($fluxType->hasErrors()) {
                $this->log(sprintf('Save failed: %s', json_encode($fluxType->getErrors())), LogLevel::ERROR);
            }
        }

        return 0;
    }

    /**
     * add missing typeActs
     *
     * @return int
     * @throws \Exception
     */
    public function addMissingTypes()
    {
        $Structures = $this->fetchTable('Structures');
        $structures = $Structures->find()->select(['id'])->contain(['TypesActs'])->toArray();

        $emptyStructures = [];
        foreach ($structures as $structure) {
            if (empty($structure['typesacts'])) {
                array_push($emptyStructures, $structure['id']);
            }
        }

        if (empty($emptyStructures)) {
            $this->log('Nothing to do', LogLevel::DEBUG);

            return 0;
        }

        foreach ($emptyStructures as $emptyStructure) {
            $this->out($emptyStructure);
            $this->addTypeSittings($emptyStructure);
            $this->addTypesActs($emptyStructure);
        }

        $this->log('Done', LogLevel::DEBUG);

        return 0;
    }

    /**
     * add type
     *
     * @return int
     * @throws \Exception
     */
    public function addType()
    {
        $structureId = $this->param('structure');

        if (empty($structureId)) {
            throw new Exception('structure must be set');
        }

        $this->addTypeSittings($structureId);
        $this->addTypesActs($structureId);

        return 0;
    }

    /**
     * @param  int $structureId structureId
     * @return bool
     * @throws \Exception
     */
    private function addTypeSittings($structureId)
    {
        $typeSittings = [
            [
                'structure_id' => $structureId,
                'name' => 'Délibération',
            ],
            [
                'structure_id' => $structureId,
                'name' => 'Conseil Municipal',
            ],
            [
                'structure_id' => $structureId,
                'name' => 'Conseil Communautaire',
            ],
        ];

        $TypeSittings = $this->fetchTable('Typesittings');
        $types = $TypeSittings->newEntities($typeSittings);

        return $TypeSittings->saveMany($types);
    }

    /**
     * @param  int $structureId structureId
     * @return bool
     * @throws \Exception
     */
    private function addTypesActs($structureId)
    {
        $typesActs = [
            [
                'structure_id' => $structureId,
                'nature_id' => 5,
                'name' => 'Arrêté individuel',
                'istdt' => true,
                'active' => true,
            ],
            [
                'structure_id' => $structureId,
                'nature_id' => 5,
                'name' => 'Autre',
                'istdt' => false,
                'active' => true,
            ],
            [
                'structure_id' => $structureId,
                'nature_id' => 7,
                'name' => 'Arrêté réglementaire',
                'istdt' => true,
                'active' => true,
            ],
            [
                'structure_id' => $structureId,
                'nature_id' => 6,
                'name' => 'Contrat et convention',
                'istdt' => true,
                'active' => true,
            ],
            [
                'structure_id' => $structureId,
                'nature_id' => 8,
                'name' => 'Délibération',
                'istdt' => true,
                'active' => true,
            ],
        ];

        $TypesActs = $this->fetchTable('Typesacts');
        $types = $TypesActs->newEntities($typesActs);

        return $TypesActs->saveMany($types);
    }

    /**
     * Complete the option parser for this shell.
     *
     * @param \Cake\Console\ConsoleOptionParser $parser foo
     * @return \Cake\Console\ConsoleOptionParser
     */
    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(__('ajout des typesittings et typeacts minimum à une structure'))
            ->addArgument(
                'addType',
                [
                    'help' => __('ajout des typesittings et typeacts minimum à une structure'),
                ]
            )
            ->addArgument(
                'addMissingTypes',
                [
                    'help' => __('ajout des typesittings et typeacts minimum à toutes les structures qui en manquent'),
                ]
            )
            ->addSubcommand(
                'addMissingFluxType',
                [
                    'help' => __('ajout des pastellfluxtype à toutes les structures qui en manquent'),
                ]
            )
            ->addOptions(
                [
                'structure' => [
                    'help' => 'structureId',
                    'short' => 's',
                ],
                ]
            );

        return $parser;
    }
}
