<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 06/09/19
 * Time: 08:41
 */

namespace App\Shell;

use Beanstalk\Utility\Beanstalk;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;

class UpdateTdtStateShell extends AppShell
{
    /**
     * sync webact state with pastell
     *
     * @return int
     */
    public function sync()
    {
        $ContainersStateacts = $this->fetchTable('ContainersStateacts');
        $Containers = $this->fetchTable('Containers');

        $containers = $Containers->find()
            ->innerJoinWith('Stateacts')
            ->where(
                [
                    'Stateacts.id in' => [TO_ORDER, PENDING_TDT, PENDING_PASTELL, TDT_CANCEL_PENDING, TDT_ERROR],
                    //Permet de mettre une condition sur le id du dernier état du container.
                    'ContainersStateacts.id' => $ContainersStateacts->getLastByContainersIdSubquery(),
                ]
            )->toArray();

        $beanstalk = new Beanstalk('tdt-pastell');

        foreach ($containers as $container) {
            $message = [
                'flux' => 'actes-generique',
                'flux_name' => 'actes-generique',
                'controller' => 'shell',
                'action' => 'checkStateTdt',
                'structure_id' => $container['structure_id'],
                'container_id' => $container['id'],
            ];

            $responseId = $beanstalk->emit($message, Configure::read('Beanstalk.Priority.NORMAL'));
            $this->out($responseId);
        }

        return 0;
    }

    /**
     * Complete the option parser for this shell.
     *
     * @param \Cake\Console\ConsoleOptionParser $parser foo
     * @return \Cake\Console\ConsoleOptionParser
     */
    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(__('sync stateacts with pastell'))
            ->addArgument(
                'sync',
                [
                    'help' => __('(Rei)nitialiaze the default permissions'),
                ]
            );

        return $parser;
    }
}
