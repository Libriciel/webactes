<?php
declare(strict_types=1);

namespace App\Shell;

use Beanstalk\Utility\Beanstalk;
use Cake\Console\ConsoleOptionParser;
use Cake\Core\Configure;
use Cake\Log\Log;

class SignatureShell extends AppShell
{
    /**
     * checkStatus of PARAPHEUR_SIGNING
     *
     * @return int
     */
    public function checkStatus(): int
    {
        $ContainersStateacts = $this->fetchTable('ContainersStateacts');
        $Containers = $this->fetchTable('Containers');

        $containers = $Containers->find()
            ->innerJoinWith('Stateacts')
            ->where(
                [
                'Stateacts.id in' => [PARAPHEUR_SIGNING],
                //Permet de mettre une condition sur le id du dernier état du container.
                'ContainersStateacts.id' => $ContainersStateacts->getLastByContainersIdSubquery(),
                ]
            )->toArray();

        $beanstalk = new Beanstalk('signature-pastell');

        foreach ($containers as $container) {
                Log::info(
                    __FUNCTION__
                    . ': [StructureId]=' . $container['structure_id'] . '-' . '[ContainerId]=' . $container['id']
                );
                $message = [
                    'flux' => 'actes-generique',
                    'flux_name' => 'actes-generique',
                    'controller' => 'shell',
                    'action' => 'checkStatusParapheur',
                    'structure_id' => $container['structure_id'],
                    'container_id' => $container['id'],
                ];

                $responseId = $beanstalk->emit($message, Configure::read('Beanstalk.Priority.NORMAL'));
                $this->out($responseId);
        }

        return 0;
    }

    /**
     * Complete the option parser for this shell.
     *
     * @param \Cake\Console\ConsoleOptionParser $parser foo
     * @return \Cake\Console\ConsoleOptionParser
     */
    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->setDescription(__('sync stateacts with pastell for projects in PARAPHEUR_SIGNING state.'))
            ->addArgument(
                'checkStatus',
                [
                    'help' => __('(Rei)nitialiaze the default permissions'),
                ]
            );

        return $parser;
    }
}
