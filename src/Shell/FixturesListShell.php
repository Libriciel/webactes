<?php
declare(strict_types=1);

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Inflector;

class FixturesListShell extends AppShell
{
    use LocatorAwareTrait;

    /**
     * @var \Cake\Database\Connection
     */
    public $connection;

    /**
     * @var string[]
     */
    public $helpers = ['Table'];

    /**
     * @inheritDoc
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->connection = $this->fetchTable('Users')->getConnection();
        $this->schemaCollection = $this->connection->getSchemaCollection();
    }

    /**
     * Retourne la liste des tables dont une table est dépendante (au niveau des clés étrangères).
     *
     * @param string $tableName Le nom de la table pour laquelle on veut obtenir les dépendances
     * @return array
     */
    protected function getDependencies(string $tableName): array
    {
        $schema = $this->schemaCollection->describe($tableName);
        $constraintNames = $schema->constraints();

        $dependencies = [];
        foreach ($constraintNames as $constraintName) {
            if ($constraintName !== 'primary') {
                $constraint = $schema->getConstraint($constraintName);
                if ($constraint['type'] === 'foreign' && $constraint['references'][0] !== $tableName) {
                    $dependencies[] = $constraint['references'][0];
                }
            }
        }
        $dependencies = array_unique($dependencies);
        sort($dependencies);

        return $dependencies;
    }

    /**
     * Retourne la liste des tables dépendantes triée par en fonction des dépendances (clés étrangères).
     *
     * @param array $list La liste des tables de dépendances
     * @return array
     */
    protected function getOrdered(array $list): array
    {
        $result = [];
        while ($list !== []) {
            foreach ($list as $tableName => $dependencies) {
                if (empty($dependencies) || array_diff($dependencies, $result) === []) {
                    $result[] = $tableName;
                    unset($list[$tableName]);
                }
            }
        }

        return $result;
    }

    /**
     * Imprime l'attribut $fixtures à copier/coller dans la classe de tests unitaires.
     *
     * @param array $tableNames La liste des fixtures
     * @return void
     */
    protected function output(array $tableNames): void
    {
        $this->out('public $fixtures = [');
        foreach ($tableNames as $idx => $tableName) {
            $this->out("\t'app." . Inflector::camelize($tableName) . "',");
        }
        $this->out('];');
    }

    /**
     * Récupère la liste de l'ensemble des tables de la base de données.
     *
     * @return array
     */
    protected function getAllList(): array
    {
        $list = [];
        $tableNames = $this->schemaCollection->listTables();
        foreach ($tableNames as $tableName) {
            $list[$tableName] = $this->getDependencies($tableName);
        }

        return $list;
    }

    /**
     * Récupère la liste des tables initiales ainsi que l'ensemble des tables qui en dépendent (clés étrangères).
     *
     * @param array $filters La liste des tables initiales
     * @return array
     */
    protected function getFilteredList(array $filters)
    {
        $todos = [];
        $count = count($filters);

        for ($idx = 0; $idx < $count; $idx++) {
            $tableName = Inflector::underscore($filters[$idx]);
            if (!in_array($tableName, $this->schemaCollection->listTables())) {
                $this->err(sprintf('La table %s n\'existe pas dans la base de données', $tableName));
                $this->_stop(static::CODE_ERROR);
            }
            $todos[] = $tableName;
        }

        $list = [];

        while ($todos !== []) {
            foreach ($todos as $idx => $tableName) {
                $dependencies = $this->getDependencies($tableName);
                if (!array_key_exists($tableName, $list)) {
                    $list[$tableName] = $dependencies;
                }
                foreach ($dependencies as $dependency) {
                    if (!array_key_exists($dependency, $list)) {
                        $todos[] = $dependency;
                    }
                }
                unset($todos[$idx]);
            }
        }

        return $list;
    }

    /**
     * Méthode principale, récupération de la liste des tables demandées et impression de l'attribut $fixtures.
     *
     * @return void
     */
    public function main(): void
    {
        if (count($this->args) === 0) {
            $list = $this->getAllList();
        } else {
            $list = $this->getFilteredList($this->args);
        }

        $ordered = $this->getOrdered($list);
        $this->output($ordered);
        $this->_stop(static::CODE_SUCCESS);
    }

    /**
     * Complete the option parser for this shell.
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser(): ConsoleOptionParser
    {
        $parser = parent::getOptionParser();

        return $parser
            ->setDescription(
                [
                    'Affiche l\'attribut $fixtures à utiliser, à partir des tables demandées'
                    . ', en fonction des dépendances de clés étrangères entre tables.',
                    'Si aucun paramètre n\'est fourni, retourne l\'ensemble des tables de l\'application',
                    'Sinon, on peut fournir un ensemble de noms de tables pour limiter.',
                    'Exemple: bin/cake fixtures_list Projects Users',
                ]
            );
    }
}
