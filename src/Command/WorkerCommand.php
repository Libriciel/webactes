<?php
declare(strict_types=1);

/**
 * Shell de lancement des workers
 *
 * Permet de lancer un worker en fonction de la configuration et en assurant
 * le suivi sur base de donnée.
 *
 * @category    Beanstalk
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (\c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;

/**
 * Surcharge pour garder la configuration des loggers.
 */
class WorkerCommand extends \Beanstalk\Command\WorkerCommand
{
    /**
     * @inheritDoc
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->setLoggers(false);
        parent::execute($args, $io);
    }
}
