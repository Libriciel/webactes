<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActorsVote Entity
 *
 * @property int $id
 * @property int|null $actor_id
 * @property int $vote_id
 * @property string|null $result
 *
 * @property \App\Model\Entity\Actor $actor
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\Vote $vote
 */
class ActorsVote extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'actor_id' => true,
        'vote_id' => true,
        'result' => true,
        'actor' => true,
        'project' => true,
        'vote' => true,
    ];
}
