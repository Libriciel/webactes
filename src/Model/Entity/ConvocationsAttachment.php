<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConvocationsAttachment Entity
 *
 * @property int $id
 * @property int $convocation_id
 * @property int $attachment_id
 *
 * @property \App\Model\Entity\Convocation $convocation
 * @property \App\Model\Entity\Attachment $attachment
 */
class ConvocationsAttachment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'convocation_id' => true,
        'attachment_id' => true,
        'convocation' => true,
        'attachment' => true,
    ];
}
