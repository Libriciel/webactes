<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\ORM\AbstractEntityWithAvailableActions;

/**
 * Sequence Entity
 *
 * @property int $id
 * @property string $name
 * @property string $comment
 * @property int $sequence_num
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenDate|null $starting_date
 * @property \Cake\I18n\FrozenDate|null $expiry_date
 *
 * @property \App\Model\Entity\Counter[] $counters
 * @property \App\Model\Entity\Structure $structure
 */
class Sequence extends AbstractEntityWithAvailableActions
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'name' => true,
        'comment' => true,
        'sequence_num' => true,
        'created' => true,
        'modified' => true,
        'starting_date' => true,
        'expiry_date' => true,
        'counters' => true,
        'structure' => true,
    ];

    protected $_virtual = [
        'availableActions',
    ];

    /**
     * @inheritDoc
     */
    protected $_availableActions = [
        'can_be_edited' => 'canBeEdited',
        'can_be_deleted' => 'canBeDeleted',
    ];

    /**
     * Sequence can be edited without any condition
     *
     * @return array
     * @created 17/12/2020
     * @version V2
     */
    public function canBeEdited(): array
    {
        return [
            'value' => true,
            'data' => true
                ? null
                : 'La séquence ne peut pas être modifiée.',
        ];
    }

    /**
     * Checks if sequence can be deleted
     *
     * @return array
     * @created 17/12/2020
     * @version V2
     */
    public function canBeDeleted(): array
    {
        $value = !$this->isUsedByCounter();

        return [
            'value' => $value,
            'data' => false
                ? null
                : 'La séquence est déjà utilisée par un compteur',
        ];
    }

    /**
     * Checks if sequence is used by counter
     *
     * @return bool
     * @created 17/12/2020
     * @version V2
     */
    private function isUsedByCounter(): bool
    {
        $counters = $this->fetchTable('Counters');

        return $counters
                ->find('all')
                ->from('counters')
                ->where(['sequence_id' => $this->id])
                ->count() > 0;
    }
}
