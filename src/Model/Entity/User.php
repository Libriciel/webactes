<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\ORM\AbstractEntityWithAvailableActions;

/**
 * User Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property int $structure_id
 * @property string|null $keycloak_id
 * @property bool $active
 * @property string $civility
 * @property string $lastname
 * @property string $firstname
 * @property string $username
 * @property string $email
 * @property string|null $phone
 * @property bool $data_security_policy
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Organization $organization
 * @property \App\Model\Entity\History[] $histories
 * @property \App\Model\Entity\Structure[] $structures
 * @property \App\Model\Entity\Service[] $services
 * @property \App\Model\Entity\Container[] $containers
 * @property \App\Model\Entity\Workflow[] $workflows
 * @property \App\Model\Entity\UsersInstance[] $usersInstances
 * @property \App\Model\Entity\RolesUser[] $rolesUsers
 */
class User extends AbstractEntityWithAvailableActions
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'keycloak_id' => true,
        'active' => true,
        'civility' => true,
        'lastname' => true,
        'firstname' => true,
        'username' => true,
        'email' => true,
        'phone' => true,
        'data_security_policy' => true,
        'created' => true,
        'modified' => true,
        'id_external' => true,
        'organization' => true,
        'role' => true,
        'roles' => true,
        'histories' => true,
        'structures' => true,
        'services' => true,
        'notifications_users' => true,
        'containers' => true,
        'roles_users' => true,
        'structures_users' => true,
        'structure_id' => true,
        'deleted' => true,
        'availableActions' => true,
    ];

    /**
     * @var string[]
     */
    protected $_virtual = ['name', 'full_name', 'availableActions'];

    /**
     * @inheritDoc
     */
    protected $_availableActions = [
        'can_be_edited' => 'canBeEdited',
        'can_be_deleted' => 'canBeDeleted',
    ];

    /**
     * @return string
     */
    protected function _getName(): string
    {
        return $this->_fields['firstname'] . ' ' .
            $this->_fields['lastname'];
    }

    /**
     * @return string
     */
    protected function _getFullName(): string
    {
        return $this->_fields['civility'] . ' ' . $this->_fields['firstname'] . ' ' .
            $this->_fields['lastname'];
    }

    /**
     * Champ virtuel pour l'affichage de prénom et nom de famille
     *
     * @return string
     * @access  protected
     * @created 15/04/2019
     * @version V0.0.9
     */
    protected function _getNomCompletCourt()
    {
        $nomCompletCourt = null;
        if (isset($this->_fields['firstname']) && isset($this->_fields['lastname'])) {
            $nomCompletCourt = ucfirst($this->_fields['firstname']) . ' ' . strtoupper($this->_fields['lastname']);
        }

        return $nomCompletCourt;
    }

    /**
     * User can be edited without any condition for super admin.
     * But admin cannot edit super admin
     *
     * @return array
     * @created 14/09/2021
     * @version 2.0.0
     */
    public function canBeEdited(): array
    {
        if (HAS_PROVISIONING || (isset($this->editActionPermission) && $this->editActionPermission === false)) {
            $retour = [
                'value' => false,
                'data' => 'L\'utilisateur ne peut pas être modifié.',
            ];
        } else {
            $retour = [
             'value' => true,
             'data' => null,
            ];
        }

        return $retour;
    }

    /**
     * Checks if user can be deleted.
     * Admin cannot delete super admin
     *
     * @return array
     * @created 14/09/2021
     * @version 2.0.0
     */
    public function canBeDeleted(): array
    {
        $isConnectedUserSuperAdmin = isset($this->_virtual['current_user_is_super_admin']);

        $value = $this->isUsedInWorkflow();

        if (
            !$isConnectedUserSuperAdmin
            || $value
            || HAS_PROVISIONING
            || (isset($this->deleteActionPermission) && $this->deleteActionPermission === false)
        ) {
            $retour = [
                'value' => false,
                'data' => 'L\'utilisateur ne peut pas être supprimé.',
            ];
        } else {
            $retour = [
                'value' => true,
                'data' => null,
            ];
        }

        return $retour;
    }

    /**
     * Returns user's structure id
     *
     * @return \Cake\ORM\Query
     */
    protected function getStructureId()
    {
        return $this->fetchTable('structures_users')
            ->find()
            ->from('structures_users')
            ->where(['structures_users.user_id' => $this->id]);
    }

    /**
     * Checks if user is in already used workflow
     *
     * @return bool
     */
    private function isUsedInWorkflow()
    {
        return $this->isNew() ||
            $this->fetchTable('UsersInstances')
                    ->find()
                    ->from('users_instances')
                    ->innerJoin('Projects', ['users_instances.id_flowable_instance = projects.id_flowable_instance'])
                    ->where(['users_instances.user_id' => $this->id])
                    ->count() > 0;
    }
}
