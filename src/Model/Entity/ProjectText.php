<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProjectText Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $project_id
 * @property int $draft_template_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\DraftTemplate $draft_template
 * @property \App\Model\Entity\File[] $files
 */
class ProjectText extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'project_id' => true,
        'draft_template_id' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'project' => true,
        'draft_template' => true,
        'files' => true,
    ];
}
