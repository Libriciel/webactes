<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Metadata Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $template_id
 * @property int $tab_id
 * @property int $field_id
 * @property int $number_line
 * @property int $number_column
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Template $template
 * @property \App\Model\Entity\Tab $tab
 * @property \App\Model\Entity\Field $field
 */
class Metadata extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'template_id' => true,
        'tab_id' => true,
        'field_id' => true,
        'number_line' => true,
        'number_column' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'template' => true,
        'tab' => true,
        'field' => true,
    ];
}
