<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActorsAct Entity
 *
 * @property int $id
 * @property int $act_id
 * @property int $actor_id
 *
 * @property \App\Model\Entity\Act $act
 * @property \App\Model\Entity\Actor $actor
 */
class ActorsAct extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'act_id' => true,
        'actor_id' => true,
        'act' => true,
        'actor' => true,
    ];
}
