<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Stateact Entity
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Container[] $containers
 */
class Stateact extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'code' => true,
        'created' => true,
        'modified' => true,
        'containers' => true,
    ];

    public const DRAFT = 'brouillon';
    public const VALIDATION_PENDING = 'en-cours-de-validation';
    public const VALIDATED = 'valide';
    public const REFUSED = 'refuse';
    public const DECLARE_SIGNED = 'declare-signe';
    public const PARAPHEUR_SIGNED = 'signe-i-parapheur';
    public const PARAPHEUR_REFUSED = 'refuse-i-parapheur';
    public const PARAPHEUR_SIGNING = 'en-cours-signature-i-parapheur';
    public const TO_TDT = 'a-teletransmettre';
    public const PENDING_TDT = 'en-cours-teletransmission';
    public const ACQUITTED = 'acquitte';
    public const TDT_CANCEL = 'annule';
    public const TDT_ERROR = 'erreur-tdt';
    public const PENDING_PASTELL = 'en-cours-depot-tdt';
    public const TO_ORDER = 'a-ordonner';
    public const TDT_CANCEL_PENDING = 'annulation-en-cours';
    public const PARAPHEUR_PENDING = 'en-cours-envoi-i-parapheur';
}
