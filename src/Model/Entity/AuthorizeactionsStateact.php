<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AuthorizeactionsStateact Entity
 *
 * @property int $id
 * @property int $authorizeaction_id
 * @property int $stateact_id
 *
 * @property \App\Model\Entity\Authorizeaction $authorizeaction
 * @property \App\Model\Entity\Stateact $stateact
 */
class AuthorizeactionsStateact extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'authorizeaction_id' => true,
        'stateact_id' => true,
        'authorizeaction' => true,
        'stateact' => true,
    ];
}
