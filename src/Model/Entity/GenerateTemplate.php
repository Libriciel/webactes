<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GenerateTemplate Entity
 *
 * @property int $id
 * @property int $generate_template_type_id
 * @property string $name
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\GenerateTemplateType $generate_template_type
 * @property \App\Model\Entity\File[] $files
 */
class GenerateTemplate extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'generate_template_type_id' => true,
        'name' => true,
        'created' => true,
        'modified' => true,
        'generate_template_types' => true,
        'files' => true,
    ];
}
