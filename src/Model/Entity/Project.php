<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Table\HistoriesTable;
use App\ORM\AbstractEntityWithAvailableActions;
use Cake\Utility\Hash;
use WrapperFlowable\Api\WrapperFactory;
use WrapperFlowable\Model\Entity\InstanciableInterface;

/**
 * Project Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int|null $theme_id
 * @property int|null $template_id
 * @property int|null $workflow_id
 * @property string $name
 * @property string|null id_flowable_instance
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string|null $code_act
 * @property bool $ismultichannel
 * @property string|null $codematiere
 * @property int $state
 * @property \Cake\I18n\FrozenTime|null $receiptDate
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Theme $theme
 * @property \App\Model\Entity\Template $template
 * @property \App\Model\Entity\Container[] $containers
 * @property \App\Model\Entity\Workflow $workflow
 * @property \App\Model\Entity\Actor[] $actors
 */
class Project extends AbstractEntityWithAvailableActions implements InstanciableInterface
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'theme_id' => true,
        'template_id' => true,
        'name' => true,
        'id_flowable_instance' => true,
        'workflow_id' => true,
        'workflow' => true,
        'created' => true,
        'modified' => true,
        'code_act' => true,
        'ismultichannel' => true,
        'codematiere' => true,
        'structure' => true,
        'theme' => true,
        'template' => true,
        'containers' => true,
        'actors' => true,
        'container_data' => true,
        'matiere' => true,
        'signature_date' => true,
    ];

    protected $_virtual = [
        'matiere',
        'availableActions',
        'isVoted',
    ];

    protected $_hidden = ['codematiere'];

    /**
     * Cache pour les champs calculés utilisant de requêtes SQL.
     *
     * @todo: appliquer aux autres, ou ne pas utiliser ?
     * @var array
     */
    protected $_cache = [];

    /**
     * @var int|null _getState cache @fixme: réhabiliter / utiliser l'autre
     */
    protected $_state = null;

    /**
     * @inheritDoc
     */
    protected $_availableActions = [
        'can_be_send_to_i_parapheur' => 'canBeSendToIParapheur',
        'can_be_send_to_manual_signature' => 'canBeSendToManualSignature',
        'can_be_send_to_workflow' => 'canBeSendToWorkflow',
        'can_be_voted' => 'canBeVoted',
        'can_generate_act_number' => 'canGenerateActNumber',
        'check_signing' => 'canCheckSigning',
        'check_refused' => 'canCheckRefused',
    ];

    /**
     * Récupère les informations necessaires pour la matière.
     *
     * @return mixed
     */
    protected function _getMatiere()
    {
        if (empty($this->codematiere)) {
            return [];
        }

        return $this->fetchTable('Matieres')->find()
            ->select(
                [
                    'id',
                    'libelle',
                    'codematiere',
                ]
            )
            ->where(['codematiere' => $this->codematiere])
            ->first() ?? [];
    }

    /**
     * Date d'acquitement du projet
     *
     * @return array|null
     */
    protected function _getReceiptDate()
    {
        if ($this->id !== null) {
            $history = $this->fetchTable('Histories')
                ->find()
                ->select(['created'])
                ->innerJoinWith('Containers.Projects')
                ->where([
                    'Histories.comment' => HistoriesTable::TDT_ACQUITTED,
                    'Projects.id' => $this->id,
                ])
                ->orderDesc('Histories.created')
                ->enableHydration(false)
                ->first();

            return $history['created'] ?? null;
        }

        return null;
    }

    /**
     * Get State id with caching
     *
     * StateAct id
     *
     * @return int
     */
    protected function _getState(): ?int
    {
        if ($this->id !== null) {
            $Container = $this->fetchTable('Containers');
            $containerId = $Container
                ->find()
                ->select(['id'])
                ->where(['project_id' => $this->id])
                ->firstOrFail()
                ->get('id');

            /** @var \App\Model\Table\ContainersTable $Container */
            $state = $Container->getLastStateActEntry($containerId);

            return $this->_state = $state[0]['id'];
        }

        return null;
    }

    /**
     * True if the project was signed by i-parapheur
     *
     * @return bool
     */
    protected function _getIsSignedIParapheur(): bool
    {
        $containerId = Hash::get($this, 'containers.0.id');
        if ($containerId !== null) {
            return $this->fetchTable('ContainersStateacts')
                    ->find()
                    ->select(['id'])
                    ->where([
                        'container_id' => $containerId,
                        'stateact_id' => PARAPHEUR_SIGNED,
                    ])
                    ->enableHydration(false)
                    ->first() !== null;
        }

        return false;
    }

    /**
     * Calcule le droit de faire l'action 'envoyer au i-parapheur'
     *
     * @return array
     */
    public function canBeSendToIParapheur(): array
    {
        $value = $this->hasCodeActe()
            && !$this->isDeliberating()
                ? in_array($this->state, [VALIDATED, PARAPHEUR_REFUSED, VOTED_APPROVED, VOTED_REJECTED])
                : in_array($this->state, [PARAPHEUR_REFUSED, VOTED_APPROVED, VOTED_REJECTED, TAKENOTE]);
        $reason = $value
            ? null
            : 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte';// @codingStandardsIgnoreLine

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * Can be send to a workflow
     *
     * @return array
     */
    public function canBeSendToWorkflow(): array
    {
        $value = in_array($this->_getState(), [DRAFT, REFUSED]);
        $reason = $value ? null : 'Le projet n\'est pas dans l\'état "Brouillon" ou "Refusé"';

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * Calcule le droit de faire l'action 'envoyer en signature manuelle'
     *
     * @return array
     */
    public function canBeSendToManualSignature(): array
    {
        return $this->canBeSendToIParapheur();
    }

    /**
     * Calcule le droit de faire l'action 'vérifier la signature i-parapheur'
     *
     * @return array
     */
    public function canCheckSigning(): array
    {
        $value = $this->state === PARAPHEUR_SIGNING;
        $reason = $value ? null : 'Le projet n\'est pas dans l\'état "En cours de signature l\'i-parapheur"';

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * Calcule le droit de faire l'action 'voir le motif de refus i-parapheur'
     *
     * @return array
     */
    public function canCheckRefused(): array
    {
        $value = $this->state === PARAPHEUR_REFUSED;
        $reason = $value ? null : 'Le projet n\'est pas dans l\'état "Refusé par l\'i-parapheur"';

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * @fixme : Si le projet a été dans un circuit, normalement id_flowable_instance est à null,
     * mais pour le besoin d'affichage de l'historique circuit,
     * on va dans ce cas retourner le id_flowable_instance du dernier circuit dans lequel il a été
     * Ceci en attendant une vraie gestion de l'historique des différents circuits d'un même projet
     * @return string|null
     */
    public function getIdFlowableInstance()
    {
        if ($this->id_flowable_instance !== null) {
            return $this->id_flowable_instance;
        }
        $data = $this->fetchTable('Histories')
            ->find()
            ->contain(['Containers.Projects'])
            ->where(
                [
                    'Histories.comment' => HistoriesTable::SEND_TO_CIRCUIT,
                    'Projects.id' => $this->id,
                ]
            )
            ->order(
                [
                    'Histories.id' => 'DESC',
                ]
            )
            ->first();

        // n'a jamais été dans un circuit
        if ($data === null) {
            return null;
        }

        // a été dans un circuit, on complete toutes les données
        $data = $data->toArray()['data'];

        $this->id_flowable_instance = $data['id_flowable_instance'];
        $this->workflow_id = $data['workflow_id'];

        $this->workflow = $this->fetchTable('Workflows')
            ->get($this->workflow_id, ['getDeleted' => true]);

        return $this->id_flowable_instance;
    }

    /**
     * @return \App\Model\Entity\Workflow|null
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }

    /**
     * @return int|null
     */
    public function getWorkflowId()
    {
        return $this->workflow_id;
    }

    /**
     * @param int $userId if we need to check if the current user can validate the current step
     * @return array|null
     * @throws \Exception
     */
    public function getInstanceDetails(int $userId = -1)
    {
        if ($this->getIdFlowableInstance() === null) {
            return null;
        }

        /** @var \App\Model\Entity\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();
        $instance = $wrapper->getInstanceDetails($this, $userId);
        $instance['workflow_id'] = $this->workflow_id;
        $instance['name'] = $this->workflow->name;
        $instance['description'] = $this->workflow->description;

        Workflow::completeUserData($instance['steps']);

        $instance['currentStep'] =
            $instance['currentStepIndex'] !== null
                ? $instance['steps'][$instance['currentStepIndex']]
                : null;

        return $instance;
    }

    /**
     * True if has a code acte false otherwise.
     *
     * @return bool
     */
    public function hasCodeActe(): bool
    {
        return $this->code_act != null;
    }

    /**
     * @return array
     */
    public function canBeVoted(): array // @fixme: public OK ? + les autres
    {
        $value = $this->isDeliberating()
            && in_array(
                $this->_getState(),
                [DRAFT, VALIDATION_PENDING, VALIDATED, VOTED_REJECTED, VOTED_APPROVED, TAKENOTE]
            );
        $reason = $value ? null : 'Le projet a un état qui ne permet pas de le voter.';

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * @return array
     */
    protected function isDeliberating(): bool
    {
        if ($this->id !== null) {
            $typesact = $this->fetchTable('Typesacts')
                ->find()
                ->select(['isdeliberating'])
                ->innerJoinWith('Containers')
                ->where(['Containers.project_id' => $this->id])
                ->enableHydration(false)
                ->firstOrFail();

            return $typesact['isdeliberating'];
        }

        return false;
    }

    /**
     * @deprecated
     * @return array
     */
    protected function _getStates(): array
    {
        if ($this->id !== null) {
            $Container = $this->fetchTable('Containers');
            $containerId = $Container
                ->find()
                ->select(['id'])
                ->where(['project_id' => $this->id]);

            /** @var \App\Model\Table\ContainersTable $Container */
            $stateActs = $Container->getAllStateActsEntries($containerId);

            return Hash::extract($stateActs, '{n}.id');
        }

        return [];
    }

    /**
     * @return array
     */
    protected function _getIsVoted(): array
    {
        if ($this->id !== null) {
            $isVoted = $this->fetchTable('ContainersStateacts')
                ->find()
                ->select(['id'])
                ->innerJoinWith('Containers')
                ->where([
                    'Containers.project_id' => $this->id,
                    'ContainersStateacts.stateact_id IN' => [VOTED_REJECTED, VOTED_APPROVED, TAKENOTE],
                ])
                ->enableHydration(false)
                ->first();
        } else {
            $isVoted = null;
        }

        $value = $isVoted !== null;
        $reason = $value ? 'Le projet a déjà été voté' : null;

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * Return true if the act number can be Generate, false + reason otherwise
     *
     * @return array
     */
    public function canGenerateActNumber()
    {
        $isUsingGenerateActNumber = $this->fetchTable('StructureSettings')
            ->getValueByModelAndStructureId(
                'act',
                'generate_number',
                $this->structure_id,
            );

        if ($this->id !== null) {
            $isValidated = $this->fetchTable('ContainersStateacts')
                ->find()
                ->select(['id'])
                ->innerJoinWith('Containers')
                ->where([
                    'Containers.project_id' => $this->id,
                    'ContainersStateacts.stateact_id IN' => [VALIDATED],
                ])
                ->enableHydration(false)
                ->first();
        } else {
            $isValidated = null;
        }

        $value = $isUsingGenerateActNumber
            && (!$this->hasCodeActe() && (!$this->isDeliberating() && $isValidated !== null));
        $reason = !$value ? 'Le numéro d\'acte ne peu pas être généré' : null;

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }
}
