<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\ORM\AbstractEntityWithAvailableActions;

/**
 * Typesact Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $nature_id
 * @property string $name
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property bool $istdt
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Nature $nature
 * @property \App\Model\Entity\TypesactsTypesitting[] $typesacts_typesittings
 * @property \App\Model\Entity\Typesitting[] $typesittings
 * @property \App\Model\Entity\DraftTemplate[] $draft_templates
 * @property \App\Model\Entity\Counter[] $counters
 */
class Typesact extends AbstractEntityWithAvailableActions
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'nature_id' => true,
        'name' => true,
        'istdt' => true,
        'structure' => true,
        'nature' => true,
        'typesacts_typesittings' => true,
        'typesittings' => true,
        'active' => true,
        'created' => false,
        'modified' => false,
        'id' => false,
        'isdeliberating' => true,
        'counter_id' => true,
        'draft_templates' => true,
        'generate_template_project_id' => true,
        'generate_template_act_id' => true,
    ];

    protected $_virtual = [
        'availableActions',
    ];

    /**
     * @inheritDoc
     */
    protected $_availableActions = [
        'can_be_edited' => 'canBeEdited',
        'can_be_deleted' => 'canBeDeleted',
        'can_be_deactivate' => 'canBeDeactivate',
    ];

    /**
     * Return an array with true if can be edited or false and reason
     *
     * @return array
     */
    public function canBeEdited(): array
    {
        return [
            'value' => true,
            'data' => true
                ? null
                : 'Le type d\'acte ne peut pas être édité.',
        ];
    }

    /**
     * Return an array with true if can be deleted or false and reason
     *
     * @return array
     */
    public function canBeDeleted()
    {
        return [
            'value' => !$this->isUsedInContainer(),
            'data' => !$this->isUsedInContainer()
                ? null
                : 'Le type d\'acte ne peut pas être supprimé.',
        ];
    }

    /**
     * Return an array with true if can be deactivate or false and reason
     *
     * @return array
     */
    public function canBeDeactivate()
    {
        return [
            'value' => true,
            'data' => true
                ? null
                : 'Le type d\'acte ne peut pas être désactivé.',
        ];
    }

    /**
     *  For given typesact_id return true if it used by a container
     */
    public function isUsedInContainer()
    {
        $container = $this->fetchTable('Containers')->find()
            ->where(['typesact_id' => $this->_fields['id']])
            ->first();

        return $container ? true : false;
    }
}
