<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Table\HistoriesTable;
use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Exception;
use WrapperFlowable\Api\WrapperFactory;
use WrapperFlowable\Model\Entity\WorkflowInterface;
use WrapperFlowable\Utilities\XmlWorkflowConverter;

/**
 * Workflow Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $organization_id
 * @property string $process_definition_key
 * @property int $structure_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property bool $is_editable
 * @property bool $is_deletable
 *
 * @property \App\Model\Entity\Organization $organization
 * @property \App\Model\Entity\Structure $structure
 */
class Workflow extends Entity implements WorkflowInterface
{
    use LocatorAwareTrait;

    public const NO_USER = 'NO_USER';
    public const NO_GROUP = 'NO_GROUP';

    /**
     * In case a deleted users still exists in flowable
     */
    public const UNEXISTANT_USER = [
        'firstname' => '__deleted__',
        'lastname' => '__deleted__',
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'file' => true,
        'process_definition_key' => true,
        'organization_id' => true,
        'structure_id' => true,
        'created' => true,
        'modified' => true,
        'active' => true,
        'deleted' => true,
        'organization' => true,
        'structure' => true,
        'projects' => true,
    ];

    protected $_virtual = [
        'is_editable',
        'is_deletable',
    ];

    protected $_hidden = [
        'deleted',
    ];

    /**
     * Récupère les informations necessaires pour l'édition.
     *
     * @return mixed
     */
    protected function _getIsEditable()
    {
        return $this->isEditable();
    }

    /**
     * Récupère les informations necessaires pour la suppression
     *
     * @return mixed
     */
    protected function _getIsDeletable()
    {
        return $this->isDeletable();
    }

    /**
     * @param  array $data data
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException|\Exception
     */
    public function createDeployment(array $data): array
    {
        $organizationId = $data['organization_id'];
        $structureId = $data['structure_id'];

        $folder = TMP . $organizationId . DS . $structureId;

        if (!file_exists($folder)) {
            umask(0);
            mkdir($folder, 0777, true);
        }

        try {
            copy($data['file']['tmp_name'], $folder . DS . $data['file']['name']);
            $data['file']['path'] = $folder . DS . $data['file']['name'];
        } catch (Exception $exception) {
            Log::write('error', $exception->getMessage(), 'flowableErrors');
            throw $exception;
        }

        //create the deployment using flowable
        /** @var \App\Model\Entity\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();
        $response = $wrapper->createDeployment($data);

        //Generate the deployment to save it into the webActes database.
        return $this->generateDeployment($response, $data);
    }

    /**
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getSteps(): array
    {
        /** @var \App\Model\Entity\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();

        return $wrapper->getSteps($this->getProcessDefinitionKey());
    }

    /**
     * @param  array $deploymentRequested deploymentRequested
     * @param  array $data                original data to extract organization_id and structure_id
     * @return array
     */
    private function generateDeployment($deploymentRequested, $data): array
    {
        return [
            'process_definition_key' => $deploymentRequested['process_definition_key'],
            'organization_id' => $data['organization_id'],
            'structure_id' => $data['structure_id'],
            'file' => $data['file']['name'],
        ];
    }

    /**
     * A workflow is editable if no project is instanced in it
     *
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->fetchTable('Histories')
            ->find()
            ->select(['id'])
            ->where(
                [
                    'comment' => HistoriesTable::SEND_TO_CIRCUIT,
                    "CAST(json::json->>'workflow_id' AS INT) =" => $this->id,
                    ]
            )
            ->count() === 0;
    }

    /**
     * Check if an instance exists for the specified process definition key
     *
     * @return bool
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function isDeletable(): bool
    {
        /** @var \App\Model\Entity\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();
        $instances = $wrapper->getInstancesByProcessDefinitionId($this->getProcessDefinitionKey());

        return count($instances) === 0;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getProcessDefinitionKey()
    {
        return $this->process_definition_key;
    }

    /**
     * @param  string $instanceId id
     * @return array
     * @throws \WrapperFlowable\Api\FlowableException
     */
    public function getInstanceHistoryByProjectInstanceId(string $instanceId): array
    {
        /** @var \App\Model\Entity\FlowableWrapper $wrapper */
        $wrapper = WrapperFactory::createWrapper();

        return $wrapper->getInstanceHistoryByBusinessKey($instanceId);
    }

    //    /**
    //     * @param string $instanceId workflow id
    //     * @return Client\Response
    //     */
    //    public function getTaskHistoryByProjectInstanceId(string $instanceId)
    //    {
    //        $queryParam = [
    //            'businessKey' => $instanceId,
    //            'includeProcessVariables' => true,
    //        ];
    //
    //        return $this->http->getTaskHistoryByProjectInstanceId($queryParam);
    //    }

    /**
     * Conversion du json en fichier bpmn20.xml
     * Enregistrement du fichier sur le disque
     *
     * @param  array $circuit Nom du circuit et nom des étapes du Workflows
     * @return array|bool Retourne un tableau avec le nom et le fichier xml pour l'envoie à flowable ou false en cas
     * d'erreur
     * @access  public
     * @created 26/03/2019
     * @version V0.0.9
     */
    public function convertJsonToXml($circuit)
    {
        $organizationId = $circuit['organization_id'];
        $structureId = $circuit['structure_id'];

        foreach ($circuit['steps'] as &$step) {
            $step['users'] = implode(',', $step['users']);
            if (empty($step['users'])) {
                $step['users'] = self::NO_USER;
            }
            if (empty($step['groups'])) {
                $step['groups'] = self::NO_GROUP;
            }
        }

        $xml_data = XmlWorkflowConverter::convertJsonToXml($circuit);

        // Nécessaire pour le dépot manuel du fichier .bpmn.xml sur l'interface de flowable
        //        $this->addBpmndiDiagram($circuit["name"], $xml_data);

        $folder = WORKSPACE . DS . $organizationId . DS . $structureId . DS . 'Workflows';
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        $microtime = time() . '_' . substr(explode(' ', microtime())[0], 2);
        $nameFile = $microtime . '.bpmn20.xml';
        $pathFile = $folder . DS . $nameFile;

        file_put_contents($pathFile, $xml_data->saveXml());

        if (file_exists($pathFile)) {
            return [
                'deploymentKey' => $circuit['name'],
                'deploymentName' => $circuit['name'],
                'file' => [
                    'name' => $nameFile,
                    'type' => mime_content_type($pathFile),
                    'tmp_name' => $pathFile,
                ],
                'organization_id' => $organizationId,
                'structure_id' => $structureId,
            ];
        }

        return false;
    }

    /**
     * Remplace les id d'users dans les steps (validators et actedUponBy) par leurs données en base
     *
     * @param  array $steps steps
     * @return void
     */
    public static function completeUserData(array &$steps)
    {
        $ids = [];

        foreach ($steps as $step) {
            foreach ($step['validators'] ?? [] as $id) {
                $ids[$id] = $id;
            }
            if (isset($step['actedUponBy'])) {
                $ids[$step['actedUponBy']] = $step['actedUponBy'];
            }
        }

        if (empty($ids)) { // nothing to do, shouldn't be possible but will be with validation groups
            return;
        }

        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $users = $usersTable
            ->find()
            ->select(
                [
                'id',
                'firstname',
                'lastname',
                ]
            )
            ->where(
                [
                'Users.id IN' => array_unique($ids),
                // AFAIRE: 'structure_id' => $this->Multico->getCurrentStructureId()
                ]
            )
            ->enableHydration(false)
            ->toArray();

        foreach ($steps as $idx => &$step) {
            foreach ($step['validators'] ?? [] as $idxV => $validator) {
                $hashUsers = Hash::extract($users, '{n}[id=/^' . $validator . '$/]');
                if (count($hashUsers)) {
                    $step['validators'][$idxV] = $hashUsers[0];
                } else {
                    $step['validators'][$idxV] = self::UNEXISTANT_USER;
                    $step['validators'][$idxV]['id'] = $validator;
                }
            }
            if (isset($step['actedUponBy'])) {
                $hashUsers = Hash::extract($users, '{n}[id=/^' . $step['actedUponBy'] . '$/]');

                if (count($hashUsers)) {
                    $step['actedUponBy'] = $hashUsers[0];
                } else {
                    $step['actedUponBy'] = self::UNEXISTANT_USER;
                    $step['actedUponBy']['id'] = $step['actedUponBy'];
                }
            }
        }
    }
}
