<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pastellflux Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property int $structure_id
 * @property int $pastellfluxtype_id
 * @property int|null $container_id
 * @property int|null $sitting_id
 * @property string $id_d
 * @property string $state
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Organization $organization
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Pastellfluxtype $pastellfluxtype
 * @property \App\Model\Entity\Container $container
 * @property \App\Model\Entity\Sitting $sitting
 * @property \App\Model\Entity\Pastellfluxaction[] $pastellfluxactions
 */
class Pastellflux extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'structure_id' => true,
        'pastellfluxtype_id' => true,
        'container_id' => true,
        'sitting_id' => true,
        'id_d' => true,
        'state' => true,
        'created' => true,
        'modified' => true,
        'organization' => true,
        'structure' => true,
        'pastellfluxtype' => true,
        'container' => true,
        'sitting' => true,
        'comments' => true,
        'pastellfluxactions' => true,
    ];
}
