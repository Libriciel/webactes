<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vote Entity
 *
 * @property int $id
 * @property int|null $actor_id
 * @property int $president_id
 * @property int $project_id
 * @property int $structure_id
 * @property int|null $yes_counter
 * @property int|null $no_counter
 * @property int|null $abstentions_counter
 * @property int|null $no_words_counter
 * @property bool|null $take_act
 * @property bool|null $result
 * @property string|null $comment
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Actor $actor
 * @property \App\Model\Entity\Project $project
 */
class Vote extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'actor_id' => true,
        'president_id' => true,
        'project_id' => true,
        'structure_id' => true,
        'yes' => true,
        'no' => true,
        'yes_counter' => true,
        'no_counter' => true,
        'abstentions_counter' => true,
        'no_words_counter' => true,
        'take_act' => true,
        'result' => true,
        'method' => true,
        'comment' => true,
        'created' => true,
        'modified' => true,
        'actor' => true,
        'project' => true,
        'actors_votes' => true,
    ];
}
