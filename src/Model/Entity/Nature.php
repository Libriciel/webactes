<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Nature Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $codenatureacte
 * @property string $libelle
 * @property string $typeabrege
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Typesact[] $typesacts
 * @property \App\Model\Entity\Typespiecesjointe[] $typespiecesjointes
 */
class Nature extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'created' => true,
        'modified' => true,
        'codenatureacte' => true,
        'libelle' => true,
        'typeabrege' => true,
        'structure' => true,
        'typesacts' => true,
        'typespiecesjointes' => true,
    ];
}
