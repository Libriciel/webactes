<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;

/**
 * Structure Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property bool $active
 * @property string|null $logo
 * @property string $business_name
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int|null $id_orchestration
 * @property string|null $customname
 * @property array|null $pastellConnector
 *
 * @property \App\Model\Entity\Organization $organization
 * @property \App\Model\Entity\Actor[] $actors
 * @property \App\Model\Entity\Act[] $acts
 * @property \App\Model\Entity\Annex[] $annexes
 * @property \App\Model\Entity\Attachment[] $attachments
 * @property \App\Model\Entity\Classification[] $classifications
 * @property \App\Model\Entity\Container[] $containers
 * @property \App\Model\Entity\Convocation[] $convocations
 * @property \App\Model\Entity\Dpo[] $dpos
 * @property \App\Model\Entity\Field[] $fields
 * @property \App\Model\Entity\File[] $files
 * @property \App\Model\Entity\History[] $histories
 * @property \App\Model\Entity\Maindocument[] $maindocuments
 * @property \App\Model\Entity\Metadata[] $metadatas
 * @property \App\Model\Entity\Nature[] $natures
 * @property \App\Model\Entity\Official[] $officials
 * @property \App\Model\Entity\Project[] $projects
 * @property \App\Model\Entity\Role[] $roles
 * @property \App\Model\Entity\Service[] $services
 * @property \App\Model\Entity\Sitting[] $sittings
 * @property \App\Model\Entity\Tab[] $tabs
 * @property \App\Model\Entity\TdtMessage[] $tdt_messages
 * @property \App\Model\Entity\Template[] $templates
 * @property \App\Model\Entity\Theme[] $themes
 * @property \App\Model\Entity\Typesact[] $typesacts
 * @property \App\Model\Entity\Typesitting[] $typesittings
 * @property \App\Model\Entity\Value[] $values
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Sequence[] $sequences
 */
class Structure extends Entity
{
    use LocatorAwareTrait;

    /**
     * @var array|null _getPastellConnector cache
     */
    protected $_pastellConnector = null;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'active' => true,
        'logo' => true,
        'business_name' => true,
        'created' => true,
        'modified' => true,
        'id_orchestration' => true,
        'customname' => true,
        'spinner' => true,
        'id_external' => true,
        'organization' => true,
        'actors' => true,
        'acts' => true,
        'annexes' => true,
        'attachments' => true,
        'classifications' => true,
        'containers' => true,
        'convocations' => true,
        'dpos' => true,
        'fields' => true,
        'files' => true,
        'histories' => true,
        'maindocuments' => true,
        'metadatas' => true,
        'natures' => true,
        'officials' => true,
        'projects' => true,
        'roles' => true,
        'services' => true,
        'sittings' => true,
        'tabs' => true,
        'tdt_messages' => true,
        'templates' => true,
        'themes' => true,
        'typesacts' => true,
        'typesittings' => true,
        'values' => true,
        'users' => true,
        'sequences' => true,
        'name' => true,
    ];

    /**
     * @return array vide si pas de connecteur trouvé
     */
    protected function _getPastellConnector(): array
    {
        //if cached
        if ($this->_pastellConnector !== null) {
            return $this->_pastellConnector;
        }

        // if loaded
        $connector = $this->get('connecteur') ?? $this['connecteur'];

        if ($connector !== null) {
            $this->_pastellConnector = [
                'pastell_user' => $connector['username'],
                'pastell_password' => $connector['password'],
                'pastell_url' => $connector['url'],
            ];

            return $this->_pastellConnector;
        }
        //    modifier un peu tout ça pour prévoir plusieurs connecteur ? (hasOne à changer)
        //
        //        puis partout où on load PastellComponent, utiliser en lieu et place de data $structure->pastellConnector

        // récupération en base
        $connector = (int)$this->fetchTable('Projects')
            ->find()
            ->where(['id' => $this->id])
            ->contain(
                [
                'Containers' => [
                    'Stateacts' => [
                        'queryBuilder' => function (Query $query) {
                            return $query
                                ->where(
                                    [
                                    'ContainersStateacts.id IN' => $this->fetchTable('ContainersStateacts')
                                        ->getLastByContainersIdSubquery('ContainersStateacts.container_id'),
                                    ]
                                )
                                ->order(['ContainersStateacts.modified' => 'DESC']);
                        },
                    ],
                ],
                ]
            )
            ->firstOrFail()
            ->toArray()['containers'][0]['stateacts'][0]['id'];

        if ($connector !== null) {
            $this->_pastellConnector = [
                'pastell_user' => $connector['username'],
                'pastell_password' => $connector['password'],
                'pastell_url' => $connector['url'],
            ];

            return $this->_pastellConnector;
        }

        return [];
    }
}
