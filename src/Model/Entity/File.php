<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * File Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int|null $maindocument_id
 * @property int|null $annex_id
 * @property string $name
 * @property string $path
 * @property string $mimetype
 * @property int|null $size
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Maindocument $maindocument
 * @property \App\Model\Entity\Annex $annex
 */
class File extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'maindocument_id' => true,
        'generate_template_id' => true,
        'draft_template_id' => true,
        'project_text_id' => true,
        'attachment_summon_id' => true,
        'annex_id' => true,
        'name' => true,
        'path' => true,
        'mimetype' => true,
        'size' => true,
        'structure' => true,
        'maindocument' => true,
        'annex' => true,
        'generate_template' => true,
        'draft_template' => true,
        'project_text' => true,
        'attachment_summon' => true,
        'created' => true,
        'modified' => true,
    ];
}
