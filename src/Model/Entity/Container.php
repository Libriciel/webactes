<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Container Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int|null $act_id
 * @property int|null $project_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Act $act
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\Sitting $sitting
 * @property \App\Model\Entity\History[] $histories
 * @property \App\Model\Entity\Stateact[] $stateacts
 */
class Container extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'act_id' => true,
        'project_id' => true,
        'typesact_id' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'act' => true,
        'project' => true,
        'sitting' => true,
        'histories' => true,
        'stateacts' => true,
        'sittings' => true,
        'maindocument' => true,
        'annexes' => true,
        'tdt_id' => true,
        'containers_sittings' => true,
        'generation_job_id' => true,
    ];
}
