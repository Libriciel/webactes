<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Convocation Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $actor_id
 * @property int $sitting_id
 * @property \Cake\I18n\FrozenTime $date_send
 * @property \Cake\I18n\FrozenTime|null $date_open
 * @property int|null $jeton
 * @property int $pastell_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Actor $actor
 * @property \App\Model\Entity\Sitting $sitting
 * @property \App\Model\Entity\Pastell $pastell
 * @property \App\Model\Entity\Attachment[] $attachments
 */
class Convocation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'actor_id' => true,
        'summon_id' => true,
        'date_send' => true,
        'date_open' => true,
        'jeton' => true,
        'pastell_id' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'actor' => true,
        'summon' => true,
        'pastell' => true,
        'attachments' => true,
    ];
}
