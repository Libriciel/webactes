<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\ORM\AbstractEntityWithAvailableActions;

/**
 * Sitting Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $typesitting_id
 * @property \Cake\I18n\FrozenTime $date
 * @property \Cake\I18n\FrozenTime|null $date_convocation
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Typesitting $typesitting
 * @property \App\Model\Entity\Convocation[] $convocations
 * @property \App\Model\Entity\Actor[] $actors
 * @property \App\Model\Entity\Container[] $containers
 */
class Sitting extends AbstractEntityWithAvailableActions
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'typesitting_id' => true,
        'date' => true,
        'date_convocation' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'typesitting' => true,
        'summons' => true,
        'convocations' => true,
        'actors' => true,
        'containers' => true,
        'generate_template_convocation_id' => true,
        'generate_template_executive_summary_id' => true,
        'generate_template_deliberations_list_id' => true,
        'generate_template_verbal_trial_id' => true,
        'president_id' => true,
        'secretary_id' => true,
        'place' => true,
    ];

    protected $_virtual = [
        'availableActions',
    ];

    /**
     * @inheritDoc
     */
    protected $_availableActions = [
        'can_be_edited' => 'canBeEdited',
        'can_be_deleted' => 'canBeDeleted',
        'can_propose_to_vote' => 'canProposeToVote',
        'can_be_closed' => 'canBeClosed',
    ];

    /**
     * Sitting can be edited without any condition
     *
     * @return array
     * @created 12/05/21
     * @version V2
     */
    public function canBeEdited(): array
    {
        return [
            'value' => true,
            'data' => true
                ? null
                : 'La séance ne peut pas être modifiée.',
        ];
    }

    /**
     * Checks if a sitting project has already been voted
     *
     * @return bool
     * @created 12/05/21
     * @version V2
     */
    protected function sittingProjectIsVoted(): bool
    {
        if ($this->id !== null) {
            $actorsProjectsSittingTable = $this->fetchTable('ActorsProjectsSittings');

            $votedProject = $actorsProjectsSittingTable->find()
                ->select(['yes', 'no', 'abstention', 'no words'])
                ->where(['sitting_id' => $this->id])
                ->count();

            return $votedProject > 0;
        }

        return false;
    }

    /**
     * Checks if sitting can be deleted
     *
     * @return array
     * @created 12/05/21
     * @version V2
     */
    public function canBeDeleted(): array
    {
        $value = !$this->sittingProjectIsVoted();
        $reason = $value ? null : 'La séance contient un projet voté.';

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * @return array
     */
    public function canProposeToVote(): array
    {
        if ($this->typesitting_id !== null) {
            $TypeSittingTable = $this->fetchTable('Typesittings');
            $isDeliberating = $TypeSittingTable->find()
                ->where(['id' => $this->typesitting_id])
                ->first()
                ->get('isdeliberating');
        } else {
            $isDeliberating = false;
        }

        $value = $isDeliberating;
        $reason = $value ? '' : 'La séance n\'est pas délibérante';

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * @return array
     */
    public function canBeClosed(): array
    {
        if ($this->id !== null) {
            /** @var \App\Model\Table\SittingsTable $sittingsTable */
            $sittingsTable = $this->fetchTable('Sittings');
            $votedProjects = $sittingsTable->isSittingCanBeClosed($this->id);
        } else {
            $votedProjects = false;
        }

        $value = $votedProjects;
        $reason = $value ? '' : 'Tous les projets de la séance n\'ont pas été votés.';

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }
}
