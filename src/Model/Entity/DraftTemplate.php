<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DraftTemplate Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $draft_template_type_id
 * @property string $name
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\DraftTemplateType $draft_template_type
 */
class DraftTemplate extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'draft_template_type_id' => true,
        'name' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'draft_template_type' => true,
        'files' => true,
        'type_acts' => true,
    ];

    /**
     * @var string[]
     */
    protected $_hidden = [
        '_joinData',
    ];
}
