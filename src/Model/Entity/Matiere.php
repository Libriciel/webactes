<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Matiere Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property string|null $parent_id
 * @property string $codematiere
 * @property string $libelle
 * @property int $lft
 * @property int $rght
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 */
class Matiere extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'parent_id' => true,
        'codematiere' => true,
        'libelle' => true,
        'lft' => true,
        'rght' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
    ];
}
