<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\ORM\AbstractEntityWithAvailableActions;

/**
 * Counter Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property string $name
 * @property string $comment
 * @property string $counter_def
 * @property int $sequence_id
 * @property string $reinit_def
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Sequence $sequence
 * @property \App\Model\Entity\Typesact $typeact
 */
class Counter extends AbstractEntityWithAvailableActions
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'name' => true,
        'comment' => true,
        'counter_def' => true,
        'sequence_id' => true,
        'reinit_def' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'sequence' => true,
    ];

    protected $_virtual = [
        'availableActions',
    ];

    /**
     * @inheritDoc
     */
    protected $_availableActions = [
        'can_be_edited' => 'canBeEdited',
        'can_be_deleted' => 'canBeDeleted',
    ];

    /**
     * Checks if counter is used by type act
     *
     * @return array
     * @created 17/12/2020
     * @version V2
     */
    public function canBeDeleted(): array
    {
        $value = !$this->isUsedByTypeAct()['value'];
        $reason = $value ? null : 'Le compteur est utilisé par un type d\'acte. Suppression impossible.';

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * No condition to edit a counter, always return true
     *
     * @return array
     * @created 17/12/2020
     * @version V2
     */
    public function canBeEdited(): array
    {
        return [
            'value' => true,
            'data' => true
                ? null
                : 'Le compteur ne peut pas être modifié.',
        ];
    }

    /**
     * Checks if sequence is already used by counter
     *
     * @return array [value => bool, data => reason as string ]
     * @created 17/12/2020
     * @version V2
     */
    private function isUsedByTypeAct(): array
    {
        $typesActsTable = $this->fetchTable('Typesacts');

        $typeAct = $typesActsTable->find('all')
            ->from('typesacts')
            ->where(['counter_id' => $this->id])
            ->count();

        $value = $typeAct > 0;
        $reason = $value ? 'Le compteur est utilisé par un type d\'acte. Suppression impossible.' : null;

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }
}
