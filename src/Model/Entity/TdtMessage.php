<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TdtMessage Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $tdt_id
 * @property int $tdt_type
 * @property int|null $tdt_etat
 * @property \Cake\I18n\FrozenDate|null $date_message
 * @property string|resource|null $tdt_data
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\² $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Tdt $tdt
 */
class TdtMessage extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'tdt_id' => true,
        'tdt_type' => true,
        'tdt_etat' => true,
        'date_message' => true,
        'tdt_data' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'tdt' => true,
    ];
}
