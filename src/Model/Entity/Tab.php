<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tab Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $template_id
 * @property int $position
 * @property string $tab
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Template $template
 * @property \App\Model\Entity\Metadata[] $metadatas
 */
class Tab extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'template_id' => true,
        'position' => true,
        'tab' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'template' => true,
        'metadatas' => true,
    ];
}
