<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SittingsStatesitting Entity
 *
 * @property int $id
 * @property int $sitting_id
 * @property int $statesitting_id
 *
 * @property \App\Model\Entity\Sitting $sitting
 * @property \App\Model\Entity\Statesitting $statesitting
 */
class SittingsStatesitting extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sitting_id' => true,
        'statesitting_id' => true,
        'sitting' => true,
        'statesitting' => true,
    ];
}
