<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Template Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property string $name
 * @property string $version
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Act[] $acts
 * @property \App\Model\Entity\Metadata[] $metadatas
 * @property \App\Model\Entity\Project[] $projects
 * @property \App\Model\Entity\Tab[] $tabs
 * @property \App\Model\Entity\Typesact[] $typesacts
 * @property \App\Model\Entity\Value[] $values
 */
class Template extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'name' => true,
        'version' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'acts' => true,
        'metadatas' => true,
        'projects' => true,
        'tabs' => true,
        'typesacts' => true,
        'values' => true,
    ];
}
