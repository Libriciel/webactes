<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\Model\Table\HistoriesTable;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * History Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $container_id
 * @property int $user_id
 * @property string $comment
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Container $container
 * @property \App\Model\Entity\User $user
 */
class History extends Entity
{
    use LocatorAwareTrait;

    protected $_virtual = [
        'data',
    ];

    protected $_hidden = [
        'json',
    ];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'container_id' => true,
        'user_id' => true,
        'comment' => true,
        'json' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'container' => true,
        'user' => true,
    ];

    /**
     * Affichage propre de l'entrée
     *
     * @return array|null
     */
    protected function _getData()
    {
        if ($this->comment === HistoriesTable::SEND_TO_CIRCUIT) {
            $workflow = $this->fetchTable('Workflows')
                ->find('all', ['getDeleted' => true])
                ->where(
                    [
                    'id' => $this->json['workflow_id'],
                    ]
                )
                ->first()
                ->toArray()['name'];
            $this->json['workflow_name'] = $workflow;
        }

        return $this->json;
    }
}
