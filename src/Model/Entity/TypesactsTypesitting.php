<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TypesactsTypesitting Entity
 *
 * @property int $id
 * @property int $typesact_id
 * @property int $typesitting_id
 *
 * @property \App\Model\Entity\Typesact $typesact
 * @property \App\Model\Entity\Typesitting $typesitting
 */
class TypesactsTypesitting extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'typesact_id' => true,
        'typesitting_id' => true,
        'typesact' => true,
        'typesitting' => true,
    ];
}
