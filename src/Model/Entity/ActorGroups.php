<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Actorsgroups Entity
 *
 * @property int $id
 */
class ActorGroups extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    // protected $_virtual = ['availableActions'];
    /**
     * @var string[]
     */
    protected $_hidden = [
        '_joinData',
    ];

    /**
     * Retourne true en value si on peut supprimer le groupe d'acteurs ou false dans le cas contraire
     *
     * @return  array [value => bool, data => reason as string ]
     * @version v2.0
     */
    public function canBeDeleted()
    {
        return [
            'value' => true,
            'data' => null,
        ];
    }

    /**
     * Retourne true en value si on peut désactiver le groupe d'acteurs ou false dans le cas contraire
     *
     * @return  array [value => bool, data => reason as string ]
     * @version v2.0
     */
    private function canBeDeactivate()
    {
        return [
            'value' => true,
            'data' => null,
        ];
    }
}
