<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActorGroupsTypesitting Entity
 *
 * @property int $id
 * @property int $actor_group_id
 * @property int $typesitting_id
 *
 * @property \App\Model\Entity\ActorGroups $actor_group
 * @property \App\Model\Entity\Typesitting $typesitting
 */
class ActorGroupsTypesitting extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'actor_group_id' => true,
        'typesitting_id' => true,
        'actor_group' => true,
        'typesitting' => true,
    ];
}
