<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pastellfluxtype Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property int $structure_id
 * @property bool $active
 * @property string $name
 * @property string|null $description
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Organization $organization
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Pastellflux[] $pastellfluxs
 */
class Pastellfluxtype extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'structure_id' => true,
        'active' => true,
        'name' => true,
        'description' => true,
        'created' => true,
        'modified' => true,
        'organization' => true,
        'structure' => true,
        'pastellfluxs' => true,
    ];
}
