<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Annex Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $container_id
 * @property int $rank
 * @property bool $istransmissible
 * @property string|null $codetype
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Container $container
 * @property \App\Model\Entity\File[] $files
 */
class Annex extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'container_id' => true,
        'rank' => true,
        'is_generate' => true,
        'istransmissible' => true,
        'codetype' => true,
        'structure' => true,
        'container' => true,
        'file' => true,
        'current' => true,
    ];
}
