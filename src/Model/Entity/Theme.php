<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Theme Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property bool $active
 * @property string $name
 * @property int|null $parent_id
 * @property int|null $lft
 * @property int|null $rght
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\ParentTheme $parent_theme
 * @property \App\Model\Entity\Project[] $projects
 * @property \App\Model\Entity\ChildTheme[] $child_themes
 */
class Theme extends Entity
{
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'active' => true,
        'name' => true,
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'parent_theme' => true,
        'projects' => true,
        'child_themes' => true,
        'position' => true,
        'deleted' => true,
    ];

    protected $_virtual = [
        'is_editable',
        'is_deletable',
        'is_deactivable',
    ];

    /**
     * Récupère les informations necessaires pour l'édition.
     *
     * @return  mixed
     * @created 25/11/2020
     * @version V2
     */
    protected function _getIsEditable()
    {
        return $this->isEditable();
    }

    /**
     * Récupère les informations necessaires pour la suppression
     *
     * @return  mixed
     * @created 25/11/2020
     * @version V2
     */
    protected function _getIsDeletable()
    {
        return $this->isDeletable();
    }

    /**
     * Récupère les informations necessaires pour la désactivation
     *
     * @return  mixed
     * @created 25/11/2020
     * @version V2
     */
    protected function _getIsDeactivable()
    {
        return $this->isDeactivable();
    }

    /**
     * Pas de règle de modification, donc retourne vrai dans tous les cas
     *
     * @return  bool
     * @created 25/11/2020
     * @version V2
     */
    public function isEditable(): bool
    {
        return true;
    }

    /**
     * Vérifie si un thème est utilisé dans un projet
     *
     * @return  bool
     * @created 25/11/2020
     * @version V2
     */
    public function isDeletable(): bool
    {
        if (!$this->isThemeUsedByProject($this->id) && !$this->isParent($this->id)) {
            return true;
        }

        return false;
    }

    /**
     * Vérifie si un thème peut être désactivé
     *
     * @return  bool
     * @created 25/11/2020
     * @version V2
     */
    public function isDeactivable(): bool
    {
        if (!$this->isThemeUsedByProjectSentToTdt($this->id) && !$this->isParent($this->id)) {
            return true;
        }

        return false;
    }

    /**
     * Vérifie si un thème est déjà utilisé par un projet
     *
     * @param   int $id theme
     * @return  bool
     * @created 25/11/2020
     * @version V2
     */
    public function isThemeUsedByProject(int $id)
    {
        $projects = $this->fetchTable('Projects');

        return $projects->find()
            ->select()
            ->from('projects')
            ->where(['projects.theme_id' => $id])
            ->count() > 0;
    }

    /**
     * Vérifie si un thème est déjà utilisé par un projet lié au tdt
     *
     * @param   int $id theme
     * @return  bool
     * @created 25/11/2020
     * @version V2
     */
    public function isThemeUsedByProjectSentToTdt(int $id)
    {
        $themes = $this->fetchTable('Themes');

        return $themes->find()
            ->select('stateacts.id')
            ->from('themes')
            ->innerJoin('projects', ["$id = projects.theme_id"])
            ->innerJoin('containers', ['containers.project_id = projects.id'])
            ->innerJoin('containers_stateacts', ['containers.id = containers_stateacts.container_id'])
            ->innerJoin('stateacts', ['containers_stateacts.stateact_id = stateacts.id'])
            ->whereInList('stateacts.id', [10, 11, 13, 14])
            ->count() > 0;
    }

    /**
     * Vérifie si un thème a des enfants
     *
     * @param   int $id theme
     * @return  bool
     * @created 25/11/2020
     * @version V2
     */
    public function isParent(int $id)
    {
        $themes = $this->fetchTable('Themes');

        return $themes ->find()
            ->where(['parent_id' => $id])
            ->count() > 0;
    }

    /**
     * Récupère les services avec les enfants
     *
     * @param   int $id id
     * @return \Cake\ORM\Query
     * @created 25/11/2020
     * @version V2
     */
    public function getWithChildren(int $id)
    {
        $parentThemes = $this->get($id);

        return $this->find('threaded')
            ->where(
                ['lft >=' => $parentThemes['lft'], 'rght <=' => $parentThemes['rght']]
            );
    }
}
