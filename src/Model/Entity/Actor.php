<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\ORM\AbstractEntityWithAvailableActions;

/**
 * Actor Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property bool $active
 * @property string $civility
 * @property string $lastname
 * @property string $firstname
 * @property \Cake\I18n\FrozenDate|null $birthday
 * @property string $email
 * @property string|null $title
 * @property string $address
 * @property string|null $address_supplement
 * @property string $post_code
 * @property string $city
 * @property string|null $phone
 * @property string|null $cellphone
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Convocation[] $convocations
 * @property \App\Model\Entity\Act[] $acts
 * @property \App\Model\Entity\Project[] $projects
 * @property \App\Model\Entity\Sitting[] $sittings
 * @property \App\Model\Entity\ActorGroups[] $actorsgroups
 */
class Actor extends AbstractEntityWithAvailableActions
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'active' => true,
        'rank' => true,
        'civility' => true,
        'lastname' => true,
        'firstname' => true,
        'birthday' => true,
        'email' => true,
        'title' => true,
        'address' => true,
        'address_supplement' => true,
        'post_code' => true,
        'city' => true,
        'phone' => true,
        'cellphone' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'convocations' => true,
        'acts' => true,
        'projects' => true,
        'sittings' => true,
        'actor_groups' => true,
    ];

    /**
     * @var string[]
     */
    protected $_virtual = [
        'name',
        'full_name',
        'availableActions',
    ];

    /**
     * @inheritDoc
     */
    protected $_availableActions = [
        'can_read' => 'canRead',
        'can_modify' => 'canEdit',
        'can_remove' => 'canRemove',
    ];

    /**
     * @return string
     */
    protected function _getName(): string
    {
        return $this->_fields['firstname'] . ' ' .
            $this->_fields['lastname'];
    }

    /**
     * @return string
     */
    protected function _getFullName(): string
    {
        return $this->_fields['civility'] . ' ' . $this->_fields['firstname'] . ' ' .
            $this->_fields['lastname'];
    }

    /**
     * No condition to create, always returns true
     *
     * @return array
     * @created 04/05/21
     * @version 2.0
     */
    public function canRead(): array
    {
        return [
            'value' => true,
            'data' => true ? null : 'L\'acteur ne pas être créé',
        ];
    }

    /**
     * No condition to edit an actor, always returns true
     *
     * @return array
     * @created 04/05/21
     * @version 2.0
     */
    public function canEdit(): array
    {
        return [
            'value' => true,
            'data' => true
                ? null
                : 'L\'acteur ne peut pas être modifié.',
        ];
    }

    /**
     * Checks if actor has already voted at least one project
     *
     * @return bool
     * @created 04/05/21
     * @version 2.0
     */
    protected function actorHasVotedProject(): bool
    {
        $actorsVotesTable = $this->fetchTable('ActorsVotes');
        $actorsProjects = $actorsVotesTable
                            ->find()
                            ->select(['yes', 'no','abstention', 'no words'])
                            ->where(['actor_id' => $this->id])
                            ->count();

        return $actorsProjects > 0;
    }

    /**
     * Checks if actor can be deleted
     *
     * @return array
     * @created 04/05/21
     * @version 2.0
     */
    public function canRemove(): array
    {
        $value = !$this->actorHasVotedProject();
        $reason = !$value ? 'L\'acteur a voté un projet. Suppression impossible.' : null;

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }
}
