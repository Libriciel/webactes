<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContainersSitting Entity
 *
 * @property int $id
 * @property int $container_id
 * @property int $sitting_id
 *
 * @property \App\Model\Entity\Container $container
 * @property \App\Model\Entity\Sitting $sitting
 */
class ContainersSitting extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'container_id' => true,
        'sitting_id' => true,
        'container' => true,
        'sitting' => true,
        'rank' => true,
    ];
}
