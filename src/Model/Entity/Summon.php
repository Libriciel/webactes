<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Summon Entity
 *
 * @property int $id
 * @property int $summon_type_id
 * @property int $structure_id
 * @property int $sitting_id
 * @property \Cake\I18n\FrozenDate|null $date_send
 * @property bool $is_sent_individually
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Sitting $sitting
 * @property \App\Model\Entity\Attachment $attachment
 */
class Summon extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'summon_type_id' => true,
        'structure_id' => true,
        'sitting_id' => true,
        'date_send' => true,
        'is_sent_individually' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'sitting' => true,
        'attachments_summons' => true,
    ];
}
