<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dpo Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property string $civility
 * @property string $name
 * @property string $firstname
 * @property string $email
 * @property string|null $address
 * @property string|null $address_supplement
 * @property string|null $post_code
 * @property string|null $city
 * @property string|null $phone
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 */
class Dpo extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'civility' => true,
        'lastname' => true,
        'firstname' => true,
        'email' => true,
        'address' => true,
        'address_supplement' => true,
        'post_code' => true,
        'city' => true,
        'phone' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
    ];
}
