<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Act Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property int $template_id
 * @property string $name
 * @property string $code_act
 * @property \Cake\I18n\FrozenTime|null $date_send
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Template $template
 * @property \App\Model\Entity\Container[] $containers
 * @property \App\Model\Entity\Actor[] $actors
 */
class Act extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'template_id' => true,
        'name' => true,
        'code_act' => true,
        'date_send' => true,
        'created' => true,
        'modified' => true,
        'structure' => true,
        'template' => true,
        'containers' => true,
        'actors' => true,
    ];
}
