<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActorsProjectsSitting Entity
 *
 * @property int $id
 * @property int|null $actor_id
 * @property int $mandataire_id
 * @property int $project_id
 * @property int $sitting_id
 * @property int $structure_id
 * @property bool|null $is_present
 *
 * @property \App\Model\Entity\Actor $actor
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\Sitting $sitting
 * @property \App\Model\Entity\Structure $structure
 */
class ActorsProjectsSitting extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'actor_id' => true,
        'mandataire_id' => true,
        'project_id' => true,
        'sitting_id' => true,
        'structure_id' => true,
        'is_present' => true,
        'actor' => true,
        'project' => true,
        'sitting' => true,
        'structure' => true,
    ];
}
