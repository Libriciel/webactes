<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 28/05/19
 * Time: 09:23
 */

namespace App\Model\Entity;

use App\Utilities\Api\Keycloak\KeycloakGuzzle;

class Auth
{
    private $kcW;
    private $kcA;

    /**
     * Auth constructor.
     *
     * @param string $token token
     */
    public function __construct($token = null)
    {
        $configWeb = file_get_contents(KEYCLOAK_WEB_CLIENT_PATH);
        $configApi = file_get_contents(KEYCLOAK_API_CLIENT_PATH);
        $this->kcW = new KeycloakGuzzle(json_decode($configWeb, true), true);
        $this->kcA = new KeycloakGuzzle(json_decode($configApi, true), true);
        if (!empty($token)) {
            $this->kcA->token = $token;
        }
    }

    /**
     * set token to client
     *
     * @param  string $token token
     * @return void
     */
    public function setToken($token)
    {
        $this->kcA->token = $token;
    }

    /**
     * get token
     *
     * @param  string $user     user
     * @param  string $password password
     * @return mixed string
     */
    public function getToken($user, $password)
    {
        $res = $this->kcW->getToken($user, $password);

        return $res['data']['access_token'];
    }

    /**
     * check if token is valid
     *
     * @param  string $token token
     * @return array
     */
    public function verifyToken($token)
    {
        return $this->kcA->verifyToken($token);
    }

    /**
     * get all users
     *
     * @return mixed
     */
    public function getUsers()
    {
        $res = $this->kcA->getUsers();

        return $res['data'];
    }

    /**
     * get an user
     *
     * @param  int $userId userId
     * @return mixed
     */
    public function getUser($userId)
    {
        $res = $this->kcA->getUser($userId);

        return $res['data'];
    }

    /**
     * get users matching a query
     *
     * @param  string $query query
     * @return mixed
     */
    public function getUsersQuery($query)
    {
        $res = $this->kcA->getUsersQuery($query);

        return $res['data'];
    }

    /**
     * adda an user
     *
     * @param  array $user user info
     * @return array
     */
    public function addUser($user)
    {
        return $this->kcA->createNewUser($user);
    }

    /**
     * delete an user
     *
     * @param  int $userId userId
     * @return array
     */
    public function deleteUser($userId)
    {
        return $this->kcA->deleteUser($userId);
    }

    /**
     * update an user
     *
     * @param  int   $userId userId
     * @param  array $data   user info
     * @return array
     */
    public function updateUser($userId, $data)
    {
        return $this->kcA->updateUser($userId, $data);
    }
}
