<?php
declare(strict_types=1);

namespace App\Model\Entity;

use App\ORM\AbstractEntityWithAvailableActions;

/**
 * Typesitting Entity
 *
 * @property int $id
 * @property int $structure_id
 * @property string $name
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property bool $active
 * @property bool $isdeliberating
 *
 * @property \App\Model\Entity\Structure $structure
 * @property \App\Model\Entity\Sitting[] $sittings
 * @property \App\Model\Entity\Sitting[] $opened_sittings
 * @property \App\Model\Entity\TypesactsTypesitting[] $typesacts_typesittings
 * @property \App\Model\Entity\Typesact[] $typeActs
 * @property \App\Model\Entity\ActorGroups[] $actor_groups
 */
class Typesitting extends AbstractEntityWithAvailableActions
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'structure_id' => true,
        'name' => true,
        'created' => true,
        'modified' => true,
        'active' => true,
        'isdeliberating' => true,
        'structure' => true,
        'sittings' => true,
        'opened_sittings' => true,
        'typesacts_typesittings' => true,
        'typeActs' => true,
        'actorGroups' => true,
        'generate_template_convocation_id' => true,
        'generate_template_executive_summary_id' => true,
        'generate_template_deliberations_list_id' => true,
        'generate_template_verbal_trial_id' => true,
    ];

    protected $_virtual = [
        'availableActions',
    ];

    /**
     * @inheritDoc
     */
    protected $_availableActions = [
        'can_be_edited' => 'canBeEdited',
        'can_be_deleted' => 'canBeDeleted',
        'can_be_deactivate' => 'canBeDeactivate',
    ];

    /**
     * Retourne true en value
     *
     * @return  array [value => bool, data => reason as string ]
     * @version V2
     */
    public function canBeEdited()
    {
        return [
            'value' => true,
            'data' => true
                ? null
                : 'Le type d\'acte ne peut pas être modifié.',
        ];
    }

    /**
     * Retourne true en value s'il appartient à une séance ou false dans le cas contraire
     *
     * @return  array [value => bool, data => reason as string ]
     * @version V2
     */
    public function canBeDeleted()
    {
        $value = !$this->belongsToSittings()['value'];
        $reason = $value ? null : 'Le type de séance est lié à au moins une séance';

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * Retourne true en value s'il appartient à une séance ouverte ou false dans le cas contraire
     *
     * @return  array [value => bool, data => reason as string ]
     * @version V2
     */
    public function canBeDeactivate()
    {
        $value = !$this->belongsToOpenedSitting();

        return [
            'value' => $value,
            'data' => $value
                ? null
                : 'Le type d\'acte ne peut pas être désactivé.',
        ];
    }

    /**
     * Retourne true s'il appartient à une séance ou false dans le cas contraire
     *
     * @return  array [value => bool, data => reason as string ]
     * @version V2
     */
    private function belongsToSittings()
    {
        $sittingTable = $this->fetchTable('Sittings');

        $numberOfSitting = $sittingTable->find('all')
            ->where(
                [
                    'typesitting_id' => $this->id,
                ]
            )
            ->count();
        $value = $numberOfSitting > 0;
        $reason = $value ? null : 'Le type de séance est lié à au moins une séance';

        return [
            'value' => $value,
            'data' => $reason,
        ];
    }

    /**
     * @return  bool Retourne true s'il appartient à une séance ouverte ou false dans le cas contraire
     * @version V2
     */
    private function belongsToOpenedSitting()
    {
        /**
         * @var \App\Model\Table\SittingsTable $sittingTable
         */
        $sittingTable = $this->fetchTable('Sittings');

        $query = $sittingTable->find('all');
        $Statesittings = $this->fetchTable('Statesittings');

        $idStateSitting = $Statesittings
            ->find('all')
            ->where(
                [
                    'code' => 'ouverture_seance',
                ]
            )
            ->first()
            ->get('id');

        return $sittingTable
                ->findSittingsByStatesittings($query, $idStateSitting, false)
                ->find('byTypesittingId', ['typesitting_id' => $this->id])
                ->count() > 0;
    }
}
