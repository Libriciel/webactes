<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AttachmentsSummon Entity
 *
 * @property int $id
 * @property int $attachment_id
 * @property int $summon_id
 * @property int $rank
 *
 * @property \App\Model\Entity\Attachment $attachment
 * @property \App\Model\Entity\Summon $summon
 */
class AttachmentsSummon extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'attachment_id' => true,
        'summon_id' => true,
        'rank' => true,
        'attachment' => true,
        'summon' => true,
    ];
}
