<?php
declare(strict_types=1);

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Event\EventInterface;
use Cake\ORM\Behavior;
use Cake\ORM\Query;

class SoftDeletionBehavior extends Behavior
{
    /**
     * Get only entities where deleted is not null, except if getDeleted is true
     *
     * @param \Cake\Event\Event $event event
     * @param \Cake\ORM\Query $query entity
     * @param \ArrayObject $options option
     * @param  bool        $primary Indicates whether or not this is the root query, or an associated query
     * @return void
     */
    public function beforeFind(EventInterface $event, Query $query, ArrayObject $options, bool $primary)
    {
        if (!$primary) { // not used in case of jointure
            return;
        }
        if (isset($options['getDeleted']) && $options['getDeleted']) {
            return;
        }
        $query->where([$this->table()->getAlias() . '.deleted IS NULL']);
    }
}
