<?php
declare(strict_types=1);

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Cache\Cache;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Utility\Hash;

class DateFormattableBehavior extends Behavior
{
    /**
     * Live cache.
     *
     * @var array
     */
    protected $_cache = [];

    /**
     * Returns the cache key for a given method and the current config.
     *
     * @param string $method The namespaced method name
     * @return string
     */
    protected function cacheKey($method)
    {
        $hash = hash('sha256', serialize(Hash::filter($this->getConfig())));

        return str_replace(['\\', '::'], ['__', '__'], $method) . '__' . $this->_table->getTable() . '__' . $hash;
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     * @access  public
     * @created 05/09/2019
     * @version V0.0.9
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $cacheKey = $this->cacheKey(__METHOD__);
        $fields = Cache::read($cacheKey);
        if (empty($fields)) {
            $fields = [];

            foreach ($this->_table->getSchema()->columns() as $column) {
                if ($this->_table->getSchema()->getColumnType($column) === 'date') {
                    $fields[] = $column;
                }
            }

            Cache::write($cacheKey, $fields);
        }
        $this->_cache[$this->_table->getTable()] = $fields;
    }

    /**
     * @param \Cake\Event\Event $event event
     * @param \ArrayObject $data data
     * @param \ArrayObject $options options
     * @return void
     * @access  public
     * @created 05/09/2019
     * @version V0.0.9
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        foreach ($this->_cache[$this->_table->getTable()] as $key) {
            $value = Hash::get($data, $key);
            if ($value instanceof DateTimeInterface) {
                $data[$key] = $value->format('Y-m-d');
            } elseif (is_string($value) === true) {
                $matches = [];
                if (
                    preg_match('/^([0-9]{4}\-[0-9]{2}\-[0-9]{2})/', trim($value), $matches) !== false
                    && empty($matches) === false
                ) {
                    $data[$key] = $matches[0];
                }
            }
        }
    }
}
