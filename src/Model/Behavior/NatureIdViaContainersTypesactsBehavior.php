<?php
declare(strict_types=1);

namespace App\Model\Behavior;

use Cake\ORM\Behavior;

/**
 * The class NatureIdViaContainersTypesactsBehavior provides the getNatureIdSubquery method that returns a subquery
 * giving the value of nature_id from the typesact associated to the container which the annexes and maindocuments belong
 * to.
 *
 * @package App\Model\Behavior
 */
class NatureIdViaContainersTypesactsBehavior extends Behavior
{
    /**
     * Returns the subquery that returns the nature_id from the typesacts from the container.
     *
     * @return \Cake\ORM\Query
     */
    public function getNatureIdSubquery()
    {
        if ($this->_table->Containers->behaviors()->has('AssociatedTables') === false) {
            $this->_table->Containers->addBehavior('LibricielCakephp3Database.AssociatedTables');
        }

        return $this->_table->Containers
            ->find()
            ->select(['Typesacts.nature_id'])
            ->innerJoin('Typesacts', $this->_table->Containers->getAssociationFullConditions('Typesacts'))
            ->where(["Containers.id = {$this->_table->getAlias()}.container_id"]);
    }
}
