<?php
declare(strict_types=1);

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Utility\Hash;

class LettercaseFormattableBehavior extends Behavior
{
    public $settings = [];

    /**
     * @param  array $config config
     * @return void
     * @access  public
     * @created 26/03/2019
     * @version V0.0.9
     */
    public function initialize(array $config): void
    {
        $defaults = [
            'lower' => [],
            'lower_noaccents' => [],
            'title' => [],
            'title_noaccents' => [],
            'upper' => [],
            'upper_noaccents' => [],
        ];

        $this->settings = $config + $defaults;
    }

    /**
     * Remplace les lettres contenant des signes diacritiques (accents, ...) par des
     * lettres sans signes et les ligatures et remplace les ligatures (ex. œ) par les
     * caractères la composant dans une chaine de caractères multibyte.
     *
     * @see {@link https://gist.github.com/devlucas/5703396}
     * @see {@link http://www.web1.pro/unicode.htm}
     * @param  string $str str
     * @return string
     * @access  public
     * @created 26/03/2019
     * @version V0.0.9
     */
    public static function noAccents($str)
    {
        $str = htmlentities($str, ENT_COMPAT, 'UTF-8');
        $ligs = [
            'from' => ['&aelig;', '&AElig;', '&oelig;', '&OElig;', '&szlig;'],
            'to' => ['ae', 'AE', 'oe', 'OE', 's'],
        ];

        $str = str_replace($ligs['from'], $ligs['to'], $str);
        $symbols = 'uml|acute|grave|circ|tilde|cedil|ring|th|slash|horn';
        $str = preg_replace('/&([a-zA-Z])(' . $symbols . ');/', '\1', $str);

        return html_entity_decode($str, ENT_COMPAT, 'UTF-8');
    }

    /**
     * @param \Cake\Event\Event $event event
     * @param \ArrayObject $data data
     * @param \ArrayObject $options options
     * @return void
     * @access  public
     * @created 15/04/2019
     * @version V0.0.9
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        foreach ($this->settings as $convert => $fields) {
            $fields = (array)$fields;
            foreach ($fields as $fieldName) {
                $value = Hash::get($data, $fieldName);
                if (empty($value) === false) {
                    $caseMode = null;

                    switch ($convert) {
                        // Minuscule
                        case 'lower':
                        case 'lower_noaccents':
                            $caseMode = MB_CASE_LOWER;
                            break;
                        // Première lettre en majuscule
                        case 'title':
                        case 'title_noaccents':
                            $caseMode = MB_CASE_TITLE;
                            break;
                        // Majuscule
                        case 'upper':
                        case 'upper_noaccents':
                            $caseMode = MB_CASE_UPPER;
                            break;
                    }

                    if ($caseMode !== null) {
                        if (strpos($convert, '_noaccents') !== false) {
                            $value = $this->noAccents($value);
                        }
                        $data[$fieldName] = mb_convert_case($value, $caseMode);
                    }
                }
            }
        }
    }
}
