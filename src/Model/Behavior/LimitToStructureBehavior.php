<?php
/**
 * @codingStandardsIgnoreFile
 * @SuppressWarnings(PHPMD)
 */


/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 11/02/19
 * Time: 15:18
 */

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Event\Event;
use Cake\Event\EventInterface;
use Cake\Http\Exception\BadRequestException;
use Cake\Log\Log;
use Cake\ORM\Behavior;
use Cake\ORM\Query;

class LimitToStructureBehavior extends Behavior
{
    /**
     *
     * @param object $event event
     * @param object $query entity
     * @param object $options option
     * @return void
     * RQ : On devrait faire autrement que d'acceder à la session d'ici Il vaut mieux la passer en option cela evitera bcp de pb
     *
     */
    public function beforeFind(EventInterface $event, Query $query, ArrayObject $options, $primary)
    {
        if (!isset($options['limitToStructure'])) {
            return;
        }
        $structureId = $options['limitToStructure'];
        $query->where([$this->table()->getAlias() . '.structure_id' => $structureId]);
    }

    /**
     * Check the conformity of structure_id
     *
     * @param Event $event
     * @param $entity
     * @param ArrayObject $options
     * @param $operation
     */
    public function beforeRules(EventInterface $event, $entity, ArrayObject $options, $operation)
    {
        if (isset($options['limitToStructure']) === true && $entity['structure_id'] !== $options['limitToStructure']) {
            $message = sprintf(
                'wrong_structure_id saving attempt: %s#%d, ',
                get_class($entity),
                $entity['id']
            );
            $entity->setErrors(['structure_id' => ['wrong_structure_id']]);

            $event->stopPropagation();
            return false;
        }
    }
}
