<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 11/02/19
 * Time: 15:18
 */

namespace App\Model\Behavior;

use Cake\Http\Session;
use Cake\ORM\Behavior;

class AddOrganizationBehavior extends Behavior
{
    /**
     * Add organizationId to entity
     *
     * @param  object $event   event
     * @param  object $entity  entity
     * @param  object $options options
     * @return void
     */
    public function beforeMarshal($event, $entity, $options)
    {
        $session = new Session();
        $session->write('organization_id', 1);

        $organizationId = $session->read('organization_id');
        $entity['organization_id'] = $organizationId;
    }
}
