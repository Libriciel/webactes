<?php
declare(strict_types=1);

namespace App\Model\Behavior;

use App\Model\Table\HistoriesTable;
use ArrayObject;
use Cake\Database\Expression\QueryExpression;
use Cake\Event\EventInterface;
use Cake\ORM\Behavior;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;

class PermissionBehavior extends Behavior
{
    use LocatorAwareTrait;

    /**
     * Add filter before find projects
     *
     * @param  object $event   event
     * @param  object $query   entity
     * @param  object $options option
     * @return void
     */
    public function beforeFind(EventInterface $event, Query $query, ArrayObject $options)
    {
        if (isset($options['businessFilters']['limitOnProjectsOwnByCurrentUser'])) {
            $userId = $options['businessFilters']['limitOnProjectsOwnByCurrentUser'];
            $query
                ->where(function (QueryExpression $exp, Query $q) use ($userId) {
                    return $exp->exists(
                        $this->fetchTable('Histories')->getByCommentAndUserIdAndContainerIdSubquery(
                            HistoriesTable::CREATE_PROJECT,
                            $userId
                        )
                    );
                });
        }
    }
}
