<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Templates Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\ActsTable|\App\Model\Table\HasMany $Acts
 * @property \App\Model\Table\MetadatasTable|\App\Model\Table\HasMany $Metadatas
 * @property \App\Model\Table\ProjectsTable|\App\Model\Table\HasMany $Projects
 * @property \App\Model\Table\TabsTable|\App\Model\Table\HasMany $Tabs
 * @property \App\Model\Table\TypesactsTable|\App\Model\Table\HasMany $Typesacts
 * @property \App\Model\Table\ValuesTable|\App\Model\Table\HasMany $Values
 * @method \App\Model\Table\Template get($primaryKey, $options = [])
 * @method \App\Model\Entity\Template newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Template[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Template|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Template|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Template patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Template[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Template findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TemplatesTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('templates');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Acts',
            [
                'foreignKey' => 'template_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Metadatas',
            [
                'foreignKey' => 'template_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Projects',
            [
                'foreignKey' => 'template_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Tabs',
            [
                'foreignKey' => 'template_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Values',
            [
                'foreignKey' => 'template_id',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        $validator
            ->scalar('version')
            ->maxLength('version', 100)
            ->requirePresence('version', 'create')
            ->allowEmptyString('version', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['structure_id', 'name']));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        return $rules;
    }
}
