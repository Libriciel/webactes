<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Project;
use App\Utilities\Common\PdfUtils;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Http\Exception\BadRequestException;
use Cake\Log\Log;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use InvalidArgumentException;
use Throwable;
use WrapperFlowable\Api\WrapperFactory;

/**
 * Projects Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\ThemesTable|\App\Model\Table\BelongsTo $Themes
 * @property \App\Model\Table\TemplatesTable|\App\Model\Table\BelongsTo $Templates
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\HasMany $Containers
 * @property \App\Model\Table\ActorsTable|\App\Model\Table\BelongsToMany $Actors
 * @property \App\Model\Table\GenerationerrorsTable|\App\Model\Table\HasMany $Generationerrors
 * @method \App\Model\Entity\Project get($primaryKey, $options = [])
 * @method \App\Model\Entity\Project newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Project[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Project|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Project saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Project patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Project[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Project findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProjectsTable extends Table
{
    use LocatorAwareTrait;

    /*
     * Constantes de nom d'index
     */
    public const DRAFT = 'DRAFT';
    public const TO_BE_VALIDATED = 'TO_BE_VALIDATED';
    public const VALIDATING = 'VALIDATING';
    public const VALIDATED = 'VALIDATED';
    public const TO_BE_TRANSMITTED = 'TO_BE_TRANSMITTED';
    public const TRANSMISSION_READY = 'TRANSMISSION_READY';
    public const ACT = 'ACT';
    public const UNASSOCIATED = 'UNASSOCIATED';
    public const VOTED = 'VOTED';

    /*
     * Tableau d'association constante index -> route index
     */
    public const INDEXES = [
        self::DRAFT => 'index_drafts',
        self::TO_BE_VALIDATED => 'index_to_validate',
        self::VALIDATING => 'index_validating',
        self::VALIDATED => 'index_validated',
        self::TO_BE_TRANSMITTED => 'index_to_transmit',
        self::TRANSMISSION_READY => 'index_ready_to_transmit',
        self::ACT => 'index_act',
        self::UNASSOCIATED => 'index_unassociated',
        self::VOTED => 'index_validated',
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('projects');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');
        $this->addBehavior('DateFormattable');
        $this->addBehavior('Permission');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Themes',
            [
                'foreignKey' => 'theme_id',
            ]
        );
        $this->belongsTo(
            'Templates',
            [
                'foreignKey' => 'template_id',
            ]
        );
        $this->belongsTo(
            'Matieres',
            [
                'foreignKey' => false,
                'conditions' => [
                    "{$this->getAlias()}.codematiere = Matieres.codematiere",
                    "{$this->getAlias()}.structure_id = Matieres.structure_id",
                ],
            ]
        );
        $this->hasMany(
            'Containers',
            [
                'foreignKey' => 'project_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'ProjectTexts',
            [
                'foreignKey' => 'project_id',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Actors',
            [
                'foreignKey' => 'project_id',
                'targetForeignKey' => 'actor_id',
                'joinTable' => 'actors_projects',
                'dependent' => true,
            ]
        );

        $this->hasOne('Votes');
        $this->belongsTo(
            'Workflows',
            [
                'foreignKey' => 'workflow_id',
            ]
        );

        $this->hasMany('UsersInstances')
            ->setForeignKey('id_flowable_instance')
            ->setBindingKey('id_flowable_instance')
            ->setJoinType('INNER')
            ->setDependent(false);

        $this->hasMany(
            'Generationerrors',
            [
                'foreignKey' => 'foreign_key',
                'conditions' => [
                    'type IN' => ['act', 'project'],
                ],
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('structure_id')
            ->requirePresence('structure_id', 'create');

        $validator
            ->integer('typesact_id')
            ->requirePresence('typesact_id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 500)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        $validator
            ->scalar('code_act')
            ->maxLength('code_act', 15)
            ->allowEmptyString('code_act');

        $validator
            ->boolean('ismultichannel')
            ->allowEmptyString('ismultichannel', null, false);

        $validator
            ->scalar('codematiere')
            ->maxLength('codematiere', 5)
            ->allowEmptyString('codematiere');

        $validator
            ->scalar('workflow_id')
            ->requirePresence('workflow_id', false);

        return $validator;
    }

    /**
     * Rules for the manualSignature method.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationManualSignature(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->date('signature_date')
            ->requirePresence('signature_date')
            ->allowEmptyDate('signature_date', null, false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['template_id'], 'Templates'));
        $rules->add($rules->existsIn(['theme_id'], 'Themes'));
        $rules->add($rules->existsIn(['workflow_id'], 'Workflows'));
        $rules->add($rules->isUnique(['id_flowable_instance']));
        $rules->add($rules->isUnique(['code_act', 'structure_id'], ['allowMultipleNulls' => true]));

        // Ajoute une règle pour la suppression.
        $rules->addDelete(
            function ($entity) {
                return !isset($entity->containers[0]) || $this->Containers->isDeletable($entity->containers[0]['id']);
            },
            'projectCantBeDelete',
            [
                'errorField' => 'id',
                'message' => 'Cannot delete a locked project',
            ]
        );

        return $rules;
    }

    /**
     * Create a project and its associated records in a single transaction.
     * Use ['atomic' => false] in the options to disable the transaction.
     *
     * @param int $structure_id Id of the structure
     * @param int $user_id Id of the user
     * @param array $rawData Data to be saved
     * @param array $options Options passed to the save method
     * @return \App\Model\Entity\Project
     */
    public function generateProject(int $structure_id, int $user_id, array $rawData, array $options = [])
    {
        $data = [
            'project' => Hash::get($rawData, 'project'),
            'annexes' => (array)Hash::get($rawData, 'annexes'),
        ];

        $is_writing = $this->fetchTable('StructureSettings')
            ->getValueByModelAndStructureId(
                'project',
                'project_writing',
                $structure_id,
            );

        if (Hash::check($rawData, 'projectTexts')) {
            $data['projectTexts'] = Hash::get($rawData, 'projectTexts');
        }

        if (Hash::check($rawData, 'mainDocument')) {
            $data['mainDocument'] = Hash::get($rawData, 'mainDocument');
        }

        if (empty($data['project']['ismultichannel'])) {
            $data['project']['ismultichannel'] = 0;
        }

        $data['project']['structure_id'] = $structure_id;

        // Get codeMatiere
        if (isset($data['project']['matiere_id'])) {
            $data['project']['codematiere'] = $this->fetchTable('Matieres')
                ->getMatiereCodeFromId($data['project']['matiere_id']);
        }

        //check codetype
        $this->checkCodetype($data['project']);

        $data['project']['containers'] = [
            [
                'typesact_id' => Hash::get($data, 'project.typesact_id'),
                'structure_id' => $structure_id,
                'histories' => [
                    [
                        'structure_id' => $structure_id,
                        'user_id' => $user_id,
                        'comment' => HistoriesTable::CREATE_PROJECT,
                    ],
                ],
                'maindocument' => [
                    'structure_id' => $structure_id,
                    'name' => Hash::get($data, 'project.name'),
                    'codetype' => $data['project']['maindocument']['codetype'] ?? null,
                    'files' => !$is_writing ? [
                        [
                            'structure_id' => $structure_id,
                            'name' => $data['mainDocument']->getClientFilename(),
                            'path' => $data['mainDocument']->getStream()->getMetadata('uri'),
                            'mimetype' => $data['mainDocument']->getClientMediaType(),
                            'size' => $data['mainDocument']->getSize(),
                        ],
                    ] : null,
                ],
                'stateacts' => [
                    '_ids' => (array)Hash::get($data, 'project.stateact_id'),
                ],
            ],
        ];

        foreach ($data['annexes'] as $key => $annexe) {
            $data['project']['containers'][0]['annexes'][] = [
                'structure_id' => $structure_id,
                'rank' => $key + 1,
                'is_generate' => !empty($data['project']['annexes'][$key]['is_generate']),
                'istransmissible' => !empty($data['project']['annexes'][$key]['istransmissible']),
                'codetype' => $data['project']['annexes'][$key]['codetype'] ??
                    null,
                'file' => [
                    'structure_id' => $structure_id,
                    'name' => $annexe->getClientFilename(),
                    'path' => $annexe->getStream()->getMetadata('uri'),
                    'mimetype' => $annexe->getClientMediaType(),
                    'size' => $annexe->getSize(),
                ],
            ];
        }

        // Association de la séance
        $data['project']['containers'][0]['sitting_id'] = Hash::get($data, 'project.sittings', []);

        $project = $this->newEntity(
            $data['project'],
            [
                'associated' => [
                    'Containers' => [
                        'associated' => [
                            'Annexes' => [
                                'associated' => [
                                    'Files',
                                ],
                            ],
                            'Histories',
                            'Maindocuments' => [
                                'associated' => [
                                    'Files',
                                ],
                            ],
                            'Sittings',
                            'Stateacts' => ['onlyIds' => true],
                        ],
                    ],
                ],
            ]
        );

        try {
            $this->getConnection()->begin();
            $this->saveOrFail($project, ['atomic' => false, 'associated' => true]);

            if (!empty($project->getErrors())) {
                throw new PersistenceFailedException($project, ['save']);
            }

            if ($is_writing) {
                $projectTexts = $this->fetchTable('ProjectTexts');
                $projectTexts->createProjectTextsByProjectIdAndTypesact(
                    $project->id,
                    Hash::get($data, 'project.typesact_id'),
                    $structure_id
                );
            }

            foreach ($project->get('containers') as $container) {
                foreach (Hash::get($data, 'project.sittings', []) as $sitting_id) {
                    $conSit = $this->Containers->ContainersSittings->newEntity(
                        [
                            'container_id' => $container->get('id'),
                            'sitting_id' => $sitting_id,
                            'rank' => $this->Containers->getNextPositionIntoSitting($sitting_id),
                        ]
                    );
                    if (!$this->Containers->ContainersSittings->save($conSit, ['atomic' => false])) {
                        $container->setError('sitting_id', $conSit->getError('sitting_id'));

                        break;
                    }
                }
            }

            $annexesErrors = $this->getProjectAnnexesErrors($project);

            if (empty($annexesErrors) === false) {
                $project->setError('id', $annexesErrors);
            }

            if ($project->hasErrors()) {
                throw new PersistenceFailedException($project, 'Votre projet n\'a pas été enregistré', 400);
            }
        } catch (PersistenceFailedException $e) {
            $this->getConnection()->rollback();
            Log::error(var_export($e->getEntity()->getErrors(), true), 'projects');

            return $project;
        } finally {
            $this->getConnection()->commit();
        }

        if (!empty($project->containers[0]->annexes)) {
            $annexesIds = Hash::extract($project->containers[0]->annexes, '{n}[is_generate=true].id');
            if (!empty($annexesIds)) {
                $this->getEventManager()->dispatch(new Event('Model.Annexe.afterSave.convertFile', $this, [
                    'ids' => $annexesIds,
                ]));
            }
        }

        // generate documents acte
        $this->generateProjectFiles((int)$project->id);

        return $project;
    }

    /**
     * check if codetype are corrects
     *
     * @param array $project project from server
     * @return bool
     */
    private function checkCodetype($project)
    {
        $Typesacts = $this->fetchTable('Typesacts');
        if (!isset($project['typesact_id'])) {
            throw new BadRequestException(sprintf("Le type de l'acte est non spécifié."));
        }
        $authorizedType = $Typesacts->getAssociatedTypesPiecesJointes($project['typesact_id']);

        //checkAnnexes
        if (isset($project['annexes']) === true) {
            foreach ($project['annexes'] as $annex) {
                if (isset($annex['codetype'])) {
                    if (!in_array($annex['codetype'], $authorizedType)) {
                        throw new BadRequestException('invalid codetype : ' . $annex['codetype']);
                    }
                }
            }
        }

        //checkproject
        if (isset($project['maindocument']['codetype'])) {
            $associated99Codes = $Typesacts->getAssociatedNature($project['typesact_id']);
            if (!in_array($project['maindocument']['codetype'], $associated99Codes)) {
                throw new BadRequestException('invalid codetype : ' . $project['maindocument']['codetype']);
            }
        }

        return true;
    }

    /**
     * @param int $sittingId sittingId
     * @param int $containerId containerId
     * @return void
     */
    public function createRankIfNotExists(int $sittingId, int $containerId)
    {
        $containersSittingsTable = $this->fetchTable('ContainersSittings');
        $containerSitting = $containersSittingsTable
            ->find()
            ->select(['id'])
            ->where([
                'sitting_id' => $sittingId,
                'container_Id' => $containerId,
            ])
            ->enableHydration(false)
            ->first();
        if ($containerSitting === null) {
            $containerSitting = $containersSittingsTable->newEntity(
                [
                    'container_id' => $containerId,
                    'sitting_id' => $sittingId,
                    'rank' => $this->Containers->getNextPositionIntoSitting($sittingId),
                ]
            );
            $containersSittingsTable->save($containerSitting);
        }
    }

    /**
     * Vérifie les annexes jointes à la fusion d'un projet.
     * Retourne un array contenant les erreurs concernant ces annexes: soit car le PDF est protégé par mot de passe,
     * soit que le nombre total de pages d'annexes est > PROJECT_ANNEXES_GENERATE_MAX_PAGE_COUNT.
     *
     * @param \App\Model\Entity\Project $project Le projet à sauvegarder dont on veut vérifier les annexes jointes à la fusion
     * @return array
     */
    protected function getProjectAnnexesErrors(Project $project): array
    {
        $pageCountThreshold = Configure::read('Project.Annexes.Generate.maxPagesCount', 1000);
        $pageCount = 0;
        $annexesErrors = [];

        foreach ($project->get('containers') as $container) {
            if (isset($container->annexes)) {
                foreach ($container->annexes as $annexe) {
                    if ($annexe->is_generate && $annexe->file->mimetype === 'application/pdf') {
                        try {
                            $pageCount += PdfUtils::getPageCount($annexe->file->path);
                        } catch (\Throwable $exc) {
                            $annexesErrors[] = sprintf('%s: %s', $annexe->file->name, $exc->getMessage());
                        }
                    }
                }
            }
        }

        if ($pageCount > $pageCountThreshold) {
            $message = sprintf(
                'Le nombre total de pages d\'annexes jointes à la fusion est de %d (%d maximum)',
                $pageCount,
                $pageCountThreshold
            );
            array_unshift($annexesErrors, $message);
        }

        return $annexesErrors;
    }

    /**
     * @param int $id project id
     * @param array $rawProject rawProject
     * @param \Laminas\Diactoros\UploadedFile $mainDocument mainDocument
     * @param array $annexes annexes
     * @param array $options o
     * @return \App\Model\Entity\Project|bool
     */
    public function modifyProject($id, $rawProject, $mainDocument, $annexes, $options = [])
    {
        $is_writing = $this->fetchTable('StructureSettings')
            ->getValueByModelAndStructureId(
                'project',
                'project_writing',
                $options['Auth']['Role']['structure_id'],
            );

        //get current project
        $projectServer = $this->find()
            ->where(['Projects.id' => $id])
            ->contain(
                [
                    'Containers.Annexes' => [
                        'sort' => ['rank' => 'ASC'],
                    ],
                    'Containers.Maindocuments' => [
                        'conditions' => ['current' => true],
                    ],
                ]
            )
            ->first();

        try {
            $isEditable = $this->Containers->isEditable($projectServer['containers'][0]['id']);
        } catch (RecordNotFoundException $e) {
            throw new InvalidArgumentException($e->getMessage());
        }

        $exception = null;
        $this->getConnection()->begin();

        try {
            // remove annexes and text
            if ($isEditable) {
                $this->removeAnnexes($projectServer->toArray(), $rawProject);
            }

            // maj transmissible status annex
            $this->Containers->Annexes->updateTransmissibleStatus(
                (array)Hash::get($rawProject, 'annexes')
            );

            $this->updateGenerateAnnexes($rawProject, $options['Auth']['Role']['structure_id']);

            // maj codetype annex
            $this->Containers->Annexes->updateCodetype((array)Hash::get($rawProject, 'annexes'));

            //maj codeType MainDocument except if we will change it
            $this->updateMainDocumentCodeType(
                !empty($rawProject['maindocument']['codetype']) ? $rawProject['maindocument']['codetype'] : '',
                $projectServer['containers'][0]['maindocument']['id']
            );

            if (isset($rawProject['sittings'])) {
                foreach ($rawProject['sittings'] as $sittingId) {
                    $this->createRankIfNotExists($sittingId, $projectServer->containers[0]->id);
                }
            }

            $projectServer = $this->find()
                ->where(['Projects.id' => $id])
                ->contain(
                    [
                        'Containers.Annexes' => ['sort' => ['rank' => 'ASC']],
                        'Containers.Maindocuments' => ['conditions' => ['current' => true]],
                    ]
                )
                ->first();

            if ($isEditable) {
                if (!empty($annexes)) {
                    $this->addAnnexes(
                        $projectServer,
                        $annexes,
                        (array)Hash::get($rawProject, 'annexes')
                    );
                }

                if ($mainDocument !== null && !$is_writing) {
                    $this->mainDocumentValidation($isEditable, $mainDocument);

                    $this->replaceMainDoc(
                        $projectServer,
                        $mainDocument,
                        $rawProject['maindocument']['codetype'] ?? null,
                        $options
                    );
                }

                $rawProject['containers'][0] = $projectServer->containers[0]->toArray();
                $rawProject['containers'][0]['typesact_id'] = Hash::get($rawProject, 'typesact_id');
            }

            // Associate Sittings
            if (isset($rawProject['sittings'])) {
                $this->Containers->associateSittings($projectServer['containers'][0]['id'], $rawProject['sittings']);
            }

            if (empty($rawProject['ismultichannel'])) {
                $rawProject['ismultichannel'] = 0;
            }
            // maj codematiere
            if ($this->canUpdateCodeMatiere($projectServer['containers'][0]['id'])) {
                $this->updateCodeMatiere($rawProject);
            }
        } catch (Throwable $exception) {
        }

        $project = $this->patchEntity($projectServer, $rawProject);

        $options['atomic'] = false;
        if ($exception !== null || $this->save($project, $options) !== false) {
            try {
                $savedProject = $this->find()
                    ->where(['Projects.id' => $id])
                    ->contain([
                        'Containers.Annexes' => [
                            'queryBuilder' => function (Query $query) {
                                return $query
                                    ->where(['Annexes.current' => true]);
                            },
                            'sort' => ['rank' => 'ASC'],
                        ],
                        'Containers.Annexes.Files',
                    ])
                    ->first();

                $annexesErrors = $this->getProjectAnnexesErrors($savedProject);

                if (empty($annexesErrors) === false) {
                    $project->setError('id', $annexesErrors);
                }

                if ($project->hasErrors()) {
                    throw new PersistenceFailedException($project, 'Votre projet n\'a pas été enregistré', 400);
                }
            } catch (\Throwable $exception) {
            }
        }

        if ($exception !== null || $project->hasErrors()) {
            $this->getConnection()->rollback();
            if ($exception !== null) {
                if (is_a($exception, '\Cake\ORM\Exception\PersistenceFailedException')) {
                    $project->setErrors($exception->getEntity()->getErrors());
                } else {
                    $project->setError('id', $exception->getMessage());
                }
            }
        } else {
            // Associate StateAct
            // You can associate state act only if currentstate act != VALIDATION_PENDING !
            if ($this->isStateEditable($projectServer['containers'][0]['id'])) {
                $this->Containers->addStateact(
                    $projectServer['containers'][0]['id'],
                    Hash::get($rawProject, 'stateact_id')
                );
            }
            $this->editHistory($options['Auth'], $project->containers[0]->id, HistoriesTable::EDIT_PROJECT);

            $this->getConnection()->commit();

            // generate documents acte
            $this->generateProjectFiles((int)$project->id);
        }

        return $project;
    }

    /**
     * Déclare que le document à été signé manuscritement
     *
     * @param int $id Identifiant du projet
     * @param array $rawProject information sur le projet
     * @param array $mainDocument Document principale
     * @param array $options o
     * @return \App\Model\Entity\Project|bool
     */
    public function manualSignature($id, $rawProject, $mainDocument, $options = [])
    {
        //get current project
        $projectServer = $this->find()
            ->where(['Projects.id' => $id])
            ->contain(
                ['Containers.Annexes',
                    'Containers.Maindocuments' => [
                        'conditions' => ['current' => true],
                    ],
                    'Containers.Typesacts',
                ]
            )
            ->first();

        $exception = null;
        $this->getConnection()->begin();

        try {
            if (!empty($mainDocument)) {
                $accepted = (array)Configure::read('mainDocumentAcceptedTypesForSigned');

                if (!in_array($mainDocument->getClientMediaType(), $accepted)) {
                    $err = sprintf(
                        '"%s" MIME type is not amongst the accepted types: "%s"',
                        $mainDocument->getClientMediaType(),
                        implode('"", "', $accepted)
                    );
                    throw new Exception($err);
                }
                $this->replaceMainDoc($projectServer, $mainDocument, null, $options);
            }

            // Associate TypesAct
            $rawProject['containers'][0] = $projectServer->containers[0]->toArray();
        } catch (Throwable $exception) {
        }

        $project = $this->patchEntity($projectServer, $rawProject, ['validate' => 'ManualSignature']);
        $options['atomic'] = false;
        if ($exception !== null || $this->save($project, $options) === false) {
            $this->getConnection()->rollback();
            if ($exception !== null) {
                $project->setError('id', $exception->getMessage());
            }
        } else {
            // Associate StateAct
            $this->Containers->addStateact($rawProject['containers'][0]['id'], $rawProject['stateact_id']);

            $this->generateProjectFiles((int)$projectServer->id);

            $this->editHistory($options['Auth'], $project->containers[0]->id, HistoriesTable::MANUAL_SIGNATURE);

            $this->getConnection()->commit();
        }

        return $project;
    }

    /**
     * Déclare que le document à été signé manuscritement
     *
     * @param int $id Identifiant du projet
     * @param array $rawProject information sur le projet
     * @param array $mainDocument Document principale
     * @param array $options o
     * @return \App\Model\Entity\Project|bool
     */
    public function replaceTheMainDocument($id, $rawProject, $mainDocument, $options = [])
    {
        //get current project
        $projectServer = $this->find()
            ->where(['Projects.id' => $id])
            ->contain(
                ['Containers.Annexes',
                    'Containers.Maindocuments' => [
                        'conditions' => ['current' => true],
                    ],
                ]
            )
            ->first();

        $exception = null;

        try {
            if (!empty($mainDocument)) {
                $this->replaceMainDoc($projectServer, $mainDocument, null, $options);
            }
        } catch (Throwable $exception) {
        }

        $project = $this->patchEntity($projectServer, $rawProject);

        $options['atomic'] = false;
        if ($exception !== null || $this->save($project, $options) === false) {
            $this->getConnection()->rollback();
            if ($exception !== null) {
                $project->setError('id', $exception->getMessage());
            }
        } else {
            $this->editHistory($options['Auth'], $project->containers[0]->id, HistoriesTable::EDIT_PROJECT);
        }

        return $project;
    }

    /**
     * Replace main document
     *
     * @param array $projectServer projectServer
     * @param array $mainDocumentFile mainDocumentFile
     * @param string|null $codeType null => no change
     * @param array $options mainDocumentFile
     * @return void
     */
    public function replaceMainDoc($projectServer, $mainDocumentFile, $codeType = null, array $options = [])
    {
        $options += ['atomic' => false];

        if (empty($mainDocumentFile)) {
            return;
        }

        $Maindocuments = $this->fetchTable('Maindocuments');
        //unset current document
        $mainDocumentId = Hash::get($projectServer, 'containers.0.maindocument.id');
        $current = $Maindocuments->get($mainDocumentId);
        $current->current = 0;

        $res = $Maindocuments->save($current, $options);
        if (!$res) {
            throw new InvalidArgumentException(var_export($current->getErrors(), true));
        }

        $structureId = Hash::get($projectServer, 'structure_id');
        $containerId = Hash::get($projectServer, 'containers.0.id');

        $data = [
            'container_id' => $containerId,
            'structure_id' => $structureId,
            'codetype' => $codeType ?? Hash::get($projectServer, 'containers.0.maindocument.codetype'),
        ];

        $mainDocumentFileData = [
            'name' => $mainDocumentFile->getClientFilename(),
            'tmp_name' => $mainDocumentFile->getStream()->getMetadata('uri'),
            'path' => $mainDocumentFile->getStream()->getMetadata('uri'),
            'type' => $mainDocumentFile->getClientMediaType(),
            'size' => $mainDocumentFile->getSize(),
        ];

        $Maindocuments->saveDocument($mainDocumentFileData, $data);

        $this->editHistory($options['Auth'], $containerId, HistoriesTable::EDIT_MAIN_DOCUMENT);
    }

    /**
     * Add annexes
     *
     * @param array $projectServer projectServer
     * @param array $annexeFiles annexeFiles
     * @param array $rawAnnexes rawAnnexes from client
     * @return void
     */
    private function addAnnexes($projectServer, $annexeFiles, $rawAnnexes)
    {
        $Annexes = $this->fetchTable('Annexes');
        $structureId = Hash::get($projectServer, 'structure_id');
        $containerId = Hash::get($projectServer, 'containers.0.id');

        $query = $this->Containers->Annexes
            ->find()
            ->where(['container_id' => $projectServer->containers[0]->id]);
        $count = max(
            $query->count(),
            $query->select(['max' => $query->func()->max('rank')])->first()->get('max')
        );

        // @info: nouvelles annexes
        foreach ($annexeFiles as $key => $annexeFile) {
            $index = array_search($annexeFile->getClientFilename(), array_column($rawAnnexes, 'name'));
            $Annexes->saveAnnexe(
                $annexeFile,
                $containerId,
                $structureId,
                $count + $key + 1,
                $rawAnnexes[$index]['is_generate'],
                $rawAnnexes[$index]['istransmissible'],
                $rawAnnexes[$index]['codetype'] ?? null
            );
        }
    }

    /**
     * list of annexeIds to remove
     *
     * @param array $projectServer array on server
     * @param array $rawProject rawproject
     * @return array
     */
    private function annexeIdsToRemove($projectServer, $rawProject)
    {
        if (empty($rawProject)) {
            return [];
        }

        $clientAnnexeIds = Hash::extract($rawProject, 'annexes.{n}.id');
        $serverAnnexeIds = Hash::extract($projectServer, 'containers.0.annexes.{n}.id');

        $toRemoveAnnexes = [];
        foreach ($serverAnnexeIds as $serverAnnexeId) {
            if (!in_array($serverAnnexeId, $clientAnnexeIds)) {
                array_push($toRemoveAnnexes, $serverAnnexeId);
            }
        }

        return $toRemoveAnnexes;
    }

    /**
     * @param bool $isEditable isEditable
     * @param \Laminas\Diactoros\UploadedFile $mainDocument mainDocument
     * @return void
     * @throws \Exception
     */
    public function mainDocumentValidation(bool $isEditable, \Laminas\Diactoros\UploadedFile $mainDocument): void
    {
        $accepted = (array)Configure::read('mainDocumentAcceptedTypesForSigned');
        if (
            $isEditable
            && $mainDocument !== null
            && $mainDocument->getError() === 0
            && !in_array($mainDocument->getClientMediaType(), $accepted)
        ) {
            $err = sprintf(
                '"%s" MIME type is not amongst the accepted types: "%s"',
                $mainDocument->getClientMediaType(),
                implode('"", "', $accepted)
            );

            throw new Exception($err);
        }
    }

    /**
     * @param array $projectServer projectServer
     * @param array $rawProject rawProject
     * @return void
     */
    public function removeAnnexes(array $projectServer, array $rawProject): void
    {
        $toRemoveAnnexes = $this->annexeIdsToRemove($projectServer, $rawProject);

        if (!empty($toRemoveAnnexes)) {
            $this->Containers->Annexes->deleteAll(['id IN' => $toRemoveAnnexes]);
            $this->fetchTable('Files')->deleteAll(['annex_id IN' => $toRemoveAnnexes]);
        }
    }

    /**
     * @param string $codetype codeType
     * @param int $id1 id
     * @return void
     */
    public function updateMainDocumentCodeType(string $codetype, int $id1): void
    {
        $this->fetchTable('Maindocuments')
            ->setCodetype(
                $codetype,
                $id1
            );
    }

    /**
     * @param array $rawProject rawProject
     * @return void
     */
    public function updateCodeMatiere(array &$rawProject): void
    {
        if (isset($rawProject['matiere_id'])) {
            $Matieres = $this->fetchTable('Matieres');
            $rawProject['codematiere'] = $Matieres->getMatiereCodeFromId($rawProject['matiere_id']);
        } elseif (empty($rawProject['matiere_id'])) {
            $rawProject['codematiere'] = '';
        }
    }

    /**
     * @param int $containerId containerId
     * @return bool
     */
    public function canUpdateCodeMatiere(int $containerId): bool
    {
        return $this->Containers->getLastStateActEntry($containerId) !== ACQUITTED;
    }

    /**
     * @param array $auth information sur la structure et l'utilisateur
     * @param int $containerId Le container Id
     * @param string $comment Le commentaire à ajouter dans l'historique
     * @return void
     */
    public function editHistory(array $auth, int $containerId, string $comment): void
    {
        $history = $this->Containers->Histories->newEntity(
            [
                'structure_id' => $auth['Role']['structure_id'],
                'container_id' => $containerId,
                'user_id' => $auth['User']['user_id'],
                'comment' => $comment,
            ]
        );

        $this->Containers->Histories->save($history);
    }

    /**
     * list of annexeIds to remove
     *
     * @param array $projectServer array on server
     * @param array $rawProject rawproject
     * @return array
     */
    private function projectTextsIdsToRemove($projectServer, $rawProject)
    {
        if (empty($rawProject)) {
            return [];
        }

        $clientAnnexeIds = Hash::extract($rawProject, 'project_texts.{n}.id');
        $serverAnnexeIds = Hash::extract($projectServer, 'containers.0.project_texts.{n}.id');

        $toRemoveAnnexes = [];
        foreach ($serverAnnexeIds as $serverAnnexeId) {
            if (!in_array($serverAnnexeId, $clientAnnexeIds)) {
                array_push($toRemoveAnnexes, $serverAnnexeId);
            }
        }

        return $toRemoveAnnexes;
    }

    /**
     * get the container from $projectId
     *
     * @param int $projectId projectId
     * @param int $structureId structureId
     * @return array|\Cake\Datasource\EntityInterface
     */
    public function getContainerByProject($projectId, $structureId)
    {
        $Containers = $this->fetchTable('Containers');
        $container = $Containers->find()
            ->select(['id'])
            ->where(['project_id' => $projectId, 'structure_id' => $structureId])
            ->firstOrFail();

        return $container;
    }

    /**
     * check if projecs are cancelable
     *
     * @param array $projectIds list of projectId
     * @return bool
     */
    public function areCancelable($projectIds)
    {
        $containers = $this->Containers->find()->select(['Containers.id', 'Containers.project_id'])
            ->where(['project_id IN' => $projectIds])->toArray();
        $notAcquitted = [];
        foreach ($containers as $container) {
            if ($this->Containers->getLastStateActEntry($container['id']) != ACQUITTED) {
                array_push($notAcquitted, $container['project_id']);
            }
        }

        if (!empty($notAcquitted)) {
            $projectsIdsError = json_encode($notAcquitted);
            throw new BadRequestException("les projets suivant ne sont pas acquittés : $projectsIdsError");
        }

        return true;
    }

    /**
     * remove nature from project
     *
     * @param array $projectIds list of project ids
     * @return int
     */
    public function removeNature($projectIds)
    {
        return $this->updateAll(['codematiere' => null], ['id IN' => $projectIds]);
    }

    /**
     * return if a state is editable
     *
     * @param int $containerId containerId
     * @return bool
     */
    public function isStateEditable(int $containerId): bool
    {
        $currentState = $this->Containers->getLastStateActEntry($containerId);

        return $currentState[0]['id'] !== VALIDATION_PENDING
            && in_array(
                $currentState[0]['id'],
                [DRAFT, VALIDATED, REFUSED, PARAPHEUR_REFUSED]
            );
    }

    /**
     * Return if the current is equals to
     *
     * @param int $containerId The container ID
     * @param int $state the state to compare to current
     * @return bool
     */
    public function isCurrentStateEqualsTo($containerId, int $state): bool
    {
        /**
         * @var \App\Model\Entity\Stateact $currentState
         */
        $currentState = $this->Containers->getLastStateActEntry($containerId);

        return $currentState[0]['id'] == $state;
    }

    /**
     * Change l'état d'un projet suite à la validation de la étape du circuite ou validation en urgence :
     *     - StateAct "validé"
     *     - workflow_id et id_flowable_instance à null
     *
     * @param \App\Model\Entity\Project $project entity
     * @param int $userId active user
     * @param bool $emergency false for regular validation (all steps)
     * @return bool success
     */
    public function approveProject(Project $project, int $userId, $emergency = false): bool
    {
        $conn = $this->getConnection();
        $conn->begin();

        /** @var \App\Model\Table\ContainersTable $Containers */
        $Containers = $this->Containers;
        $containerId = $project->containers[0]['id'];
        $Containers->addStateact($containerId, VALIDATED, true);

        /**
         * @var \App\Model\Table\ContainersTable $Containers
         */
        $history = $Containers->Histories->newEntity(
            [
                'structure_id' => $project->structure_id,
                'container_id' => $containerId,
                'user_id' => $userId,
                'comment' => $emergency ? HistoriesTable::EMERGENCY_VALIDATION : HistoriesTable::VALIDATION,
            ]
        );

        $project->workflow_id = null;
        $project->id_flowable_instance = null;

        if ($Containers->Histories->save($history) !== false && $this->save($project) !== false) {
            return $conn->commit();
        }

        $conn->rollback();

        return false;
    }

    /**
     * Change l'état d'un projet suite à au rejet d'une étape du circuit :
     *     - StateAct "refusé"
     *     - workflow_id et id_flowable_instance à null
     *
     * @param \App\Model\Entity\Project $project entity
     * @param int $userId active user
     * @return bool success
     */
    public function rejectProject(Project $project, int $userId): bool
    {
        $conn = $this->getConnection();
        $conn->begin();

        /** @var \App\Model\Table\ContainersTable $Containers */
        $Containers = $this->Containers;
        $containerId = $project->containers[0]['id'];
        $Containers->addStateact($containerId, REFUSED, true);

        /**
         * @var \App\Model\Table\ContainersTable $Containers
         */
        $history = $Containers->Histories->newEntity(
            [
                'structure_id' => $project->structure_id,
                'container_id' => $containerId,
                'user_id' => $userId,
                'comment' => HistoriesTable::REJECT,
            ]
        );

        $project->workflow_id = null;
        $project->id_flowable_instance = null;

        $success = $Containers->Histories->save($history) !== false && $this->save($project) !== false;

        if ($success) {
            return $conn->commit();
        }

        $conn->rollback();

        return false;
    }

    /**
     * Retourne le menu principal où je projet peut être consulté, ou null si pas visible pour l'utilisateur
     *
     * @param int $projectId project
     * @param array $sessionData [
     *                           'keycloak_id' => (string),
     *                           'user_id' => (int),
     *                           'Auth' => [
     *                           'User' => [
     *                           'id' => (int),
     *                           'structure_id' => (int),
     *                           'user_id' => (int)
     *                           ],
     *                           'Role' => [
     *                           'id' => (int),
     *                           'name' => (string)
     *                           ]
     *                           ],
     *                           'structure_id' => (int),
     *                           'organization_id' => (int)
     *                           ]
     * @return string|null
     */
    public function getMainMenu(int $projectId, array $sessionData)
    {
        // @fixme avec gestion des droits : if $project['rights']['is_visible'] à différents endroits

        $project = $this->find()
            ->innerJoinWith('Containers')
            ->innerJoinWith('Containers.Histories')
            ->innerJoinWith('Containers.Stateacts')
            ->innerJoinWith('Containers.Typesacts')
            ->where(
                [
                    'Projects.id' => $projectId,
                    'Projects.structure_id' => $sessionData['structure_id'],
                    'ContainersStateacts.id' => $this->fetchTable('ContainersStateacts')
                        ->getLastByContainersIdSubquery(),
                    'Histories.id' => $this->Containers->Histories
                        ->find()
                        ->select(['Histories.id'])
                        ->where(['Histories.container_id = Containers.id'])
                        ->order(['Histories.created' => 'asc'])
                        ->limit(1),
                ]
            )
            ->contain(
                [
                    'Containers' => [
                        'Sittings' => [
                            'Typesittings',
                        ],
                        'Stateacts' => [
                            'queryBuilder' => function (Query $query) {
                                return $query
                                    ->where(
                                        [
                                            'ContainersStateacts.id IN' => $this->fetchTable('ContainersStateacts')
                                                ->getLastByContainersIdSubquery('ContainersStateacts.container_id'),
                                        ]
                                    )
                                    ->order(['ContainersStateacts.modified' => 'DESC']);
                            },
                        ],
                        'Typesacts',
                        'Histories',
                    ],
                    'Workflows',
                ]
            )
            ->firstOrFail();

        $canViewProjects = in_array(
            $sessionData['Auth']['Role']['name'],
            [RolesTable::ADMINISTRATOR, RolesTable::SUPER_ADMINISTRATOR, RolesTable::GENERAL_SECRETARIAT],
            true
        );

        $projectCreatorId = $project['containers'][0]['histories'][0]['user_id'];

        switch ($project['containers'][0]['stateacts'][0]['id']) {
            // brouillon ou refusé, visible uniquement si l'user est le créateur ou admin
            case DRAFT:
            case REFUSED:
                return $projectCreatorId === $sessionData['user_id'] || $canViewProjects
                    ? self::DRAFT : null;

            case VALIDATION_PENDING:
                // On peut toujours Voir les projets dans un circuit dans lequel on est concerné
                // Sinon, si on est pas dans le circuit, il faut être créateur ou avoir un rôle admin
                $isUserInvolvedInCircuit = $this->fetchTable('UsersInstances')
                        ->find()
                        ->select('id')
                        ->where(
                            [
                                'user_id' => $sessionData['user_id'],
                                'id_flowable_instance' => $project['id_flowable_instance'],
                            ]
                        )
                        ->count() !== 0;
                if (!$isUserInvolvedInCircuit) {
                    return $canViewProjects || $projectCreatorId === $sessionData['user_id']
                        ? self::VALIDATING
                        : null;
                }

                $wrapper = WrapperFactory::createWrapper();

                return $wrapper->isUserCurrentForInstance($sessionData['user_id'], $project['id_flowable_instance'])
                    ? self::TO_BE_VALIDATED
                    : self::VALIDATING;

            case VALIDATED:
            case PARAPHEUR_SIGNING:
            case PARAPHEUR_REFUSED:
                return self::VALIDATED;
            case VOTED_REJECTED:
            case VOTED_APPROVED:
            case TAKENOTE:
                return self::VOTED;
            case TO_TDT:
            case PENDING_PASTELL:
            case TDT_ERROR:
                return self::TO_BE_TRANSMITTED;

            case TO_ORDER:
                return self::TRANSMISSION_READY;

            case DECLARE_SIGNED:
            case PARAPHEUR_SIGNED:
                return $project['containers'][0]['typesact']['istdt'] ? self::TO_BE_TRANSMITTED : self::ACT;

            case PENDING_TDT:
            case ACQUITTED:
            case TDT_CANCEL:
            case TDT_CANCEL_PENDING:
                return self::ACT;

            default:
                return null;
        }
    }

    /**
     * If the act is on Pastell return TRUE , false otherwise.
     *
     * @param \App\Model\Entity\Project $project A project entity
     * @return bool
     */
    private function isOnPastell(Project $project): bool
    {
        $PastellFlux = $this->fetchTable('Pastellfluxs');

        $containerid = $this->fetchTable('Containers')
            ->find()
            ->select('id')
            ->where(['project_id' => $project->id])
            ->first()
            ->id;
        $documentSousPastell = $PastellFlux->find()
            ->select('id_d')
            ->where(
                [
                    'container_id' => $containerid,
                ]
            )
            ->orderDesc('id')
            ->first();

        return $documentSousPastell !== null;
    }

    /**
     * @param \Cake\ORM\Query $query query
     * @param array $options list of options
     * @return \Cake\ORM\Query query
     */
    public function findAllProjects(Query $query, array $options)
    {
        return $query
            ->innerJoinWith('Containers')
            ->innerJoinWith('Containers.Histories')
            ->innerJoinWith('Containers.Stateacts')
            ->innerJoinWith('Containers.Typesacts')
            ->where(
                [
                    'ContainersStateacts.id' => $this->fetchTable('ContainersStateacts')
                        ->getLastByContainersIdSubquery(),
                    'Histories.id' => $this->Containers->Histories
                        ->find()
                        ->select(['Histories.id'])
                        ->where(['Histories.container_id = Containers.id'])
                        ->order(['Histories.created' => 'asc'])
                        ->limit(1),
                ]
            )
            ->contain(
                [
                    'Containers' => [
                        'Annexes' => [
                            'conditions' => ['current' => true],
                            'Files',
                            'Typespiecesjointes' => [
                                'fields' => ['id', 'codetype', 'libelle'],
                            ],
                        ],
                        'Maindocuments' => [
                            'conditions' => ['current' => true],
                            'Files',
                            'Typespiecesjointes' => [
                                'fields' => ['id', 'codetype', 'libelle'],
                            ],
                        ],
                        'Sittings' => [
                            'Typesittings',
                            'sort' => ['Sittings.id' => 'ASC'],
                        ],
                        'Stateacts' => [
                            'queryBuilder' => function (Query $query) {
                                return $query
                                    ->where(
                                        [
                                            'ContainersStateacts.id IN' => $this->fetchTable('ContainersStateacts')
                                                ->getLastByContainersIdSubquery('ContainersStateacts.container_id'),
                                        ]
                                    )
                                    ->order(['ContainersStateacts.id' => 'DESC']);
                            },
                        ],
                        'Typesacts',
                    ],
                    'Workflows',
                ]
            );
    }

    /**
     * @param \Cake\ORM\Query $query query
     * @param array $options list of options
     * @return \Cake\Datasource\QueryInterface|\Cake\ORM\Query
     */
    public function findAllProjectsWithWorkflows(Query $query, array $options)
    {
        return $query->find('allProjects')
            ->innerJoinWith('Workflows');
    }

    /**
     * @param \Cake\ORM\Query $query query
     * @param array $options list of options
     * @return \Cake\ORM\Query query with clause
     */
    public function findAddClauseProjectValidatingWhereUserIsInvolved(Query $query, array $options): Query
    {
        return $query->where(
            ['OR' => [
                [
                    'Projects.id_flowable_instance IN' => $this->fetchTable('UsersInstances')
                        ->find()
                        ->select('id_flowable_instance')
                        ->where(['user_id = ' . $options['userId']]),
                ],
            ]]
        );
    }

    /**
     * Add clause to find project created by the user specified in parametter
     *
     * @param \Cake\ORM\Query $query query
     * @param array $options list of options
     * @return \Cake\ORM\Query
     */
    public function findAddProjectCreatedByUser(Query $query, array $options): Query
    {
        return $query->orWhere(
            [
                'Histories.id' => $this->fetchTable('Histories')
                    ->find()
                    ->select(['Histories.id'])
                    ->where(
                        [
                            'Histories.container_id = Containers.id',
                            'Histories.comment' => HistoriesTable::CREATE_PROJECT,
                            'Histories.user_id' => $options['userId'],
                        ]
                    ),
            ]
        );
    }

    /**
     * @param \Cake\ORM\Query $query query
     * @param array $options list of options
     * @return \Cake\ORM\Query
     */
    public function findAddProjectUserActOn(Query $query, array $options): Query
    {
        $actionsList = HistoriesTable::getAllActions();

        $options['conditions']['OR'][] =
            [
                'Histories.id' => $this->fetchTable('Histories')
                    ->find()
                    ->select(['Histories.id'])
                    ->where(
                        [
                            'Histories.container_id = Containers.id',
                            'Histories.comment' => !isset($options['historyAction'])
                                ? $actionsList
                                : $options['historyAction'],
                            'Histories.user_id' => $options['userId'],
                        ]
                    ),
            ];

        return $query->applyOptions($options);
    }

    /**
     * Find all project Sittingsless
     *
     * @param \Cake\ORM\Query $query query
     * @param array $options options
     * @return \Cake\ORM\Query query
     */
    public function findSittingsLessProjects(Query $query, array $options): Query
    {
        $subquery = $this->Containers->Typesacts->Natures
            ->find()
            ->select(['id'])
            ->where(
                [
                    'typeabrege' => 'DE',
                    'structure_id' => $options['structureId'],
                ]
            )
            ->limit(1)
            ->orderAsc('id');

        $conditions = [
            'Stateacts.code IN' => ['brouillon', 'valide', 'refuse', 'en-cours-de-validation', 'annule'],
            'Typesacts.nature_id IN' => $subquery,
            'Projects.signature_date IS NULL',
            'Containers.id NOT IN' => $this->fetchTable('ContainersSittings')
                ->find()
                ->select(['ContainersSittings.container_id'])
                ->where(['ContainersSittings.container_id = Containers.id']),
        ];

        return $query->where($conditions);
    }

    /**
     * @param int $project_id id of the project to check.
     * @return bool
     */
    public function hadSitting(int $project_id)
    {
        return $this->fetchTable('ContainersSittings')
                ->find()
                ->select(['ContainersSittings.container_id'])
                ->where([
                    'ContainersSittings.container_id' => $this
                        ->find()
                        ->where(['id' => $project_id])
                        ->first()
                        ->get('container_id')])
                ->count() > 1;
    }

    /**
     * @param int $id ProjectId
     * @return string $code_act
     */
    public function generateActNumber(int $id): string
    {
        $project = $this->find()
            ->contain([
                'Containers',
                'Containers.Typesacts',
                'Containers.Typesacts.Counters',
            ])
            ->where(['Projects.id' => $id])
            ->firstOrFail();

        $is_generate = $this->fetchTable('StructureSettings')
            ->getValueByModelAndStructureId(
                'act',
                'generate_number',
                $project->structure_id,
            );

        if (!$is_generate) {
            return '';
        }

        if (!isset($project->containers[0]->typesact->counter_id)) {
            throw new RecordNotFoundException(__('Record not found in table Counters'));
        }

        $ContainersStateacts = $this->fetchTable('ContainersStateacts');
        if (
            empty($project->code_act) &&
            ((
                    $ContainersStateacts->getStateId($id) > VALIDATION_PENDING &&
                    !$project->containers[0]->typesact->isdeliberating
                ) ||
                in_array($ContainersStateacts->getStateId($id), [VOTED_APPROVED, VOTED_REJECTED, TAKENOTE], true)) &&
            $project->containers[0]->typesact->counter_id
        ) {
            /** @var \App\Model\Table\CountersTable $Counters */
            $codeAct = $this->fetchTable('Counters')
                ->generatesCounter($project->containers[0]->typesact->counter_id);

            if ($project->containers[0]->typesact->isdeliberating) {
                $sittingEntity = $this->Containers->getDeliberatingSittingByContainerId($project->containers[0]->id);
                $rank = $this->Containers->getRankInDeliberatingSitting(
                    $sittingEntity->get('id'),
                    $project->containers[0]->id
                );
                $codeAct = str_replace('#p#', (string)$rank, $codeAct);
            }

            try {
                $this->patchEntity($project, ['code_act' => $codeAct]);
                if ($this->save($project)) {
                    $isGenerate = $this->fetchTable('StructureSettings')
                        ->getValueByModelAndStructureId(
                            'act',
                            'act_generate',
                            $project->structure_id,
                        );

                    if ($isGenerate) {
                        $this->generateProjectFiles($id);
                    }

                    return $project->code_act;
                }
            } catch (Throwable $exception) {
                $project->setError('id', $exception->getMessage());

                return '';
            }
        }

        return $project->code_act ?? '';
    }

    /**
     * @param int $id ProjectId
     * @return void
     */
    public function generateProjectFiles(int $id)
    {
        $project = $this->find()
            ->contain([
                'Containers',
                'Containers.Maindocuments.Files',
            ])
            ->where(['Projects.id' => $id])
            ->firstOrFail();

        $is_generate = $this->fetchTable('StructureSettings')
            ->getValueByModelAndStructureId(
                'act',
                'act_generate',
                $project->structure_id,
            );

        $containerId = $project->containers[0]->id;
        if ($is_generate && !$this->Containers->isSignedParapheur($containerId)) {
            GenerationerrorsTable::cleanup('act', $project->id);
            if (isset($project['containers'][0]['maindocument']['files'][0]['id'])) {
                $Files = $this->fetchTable('Files');
                $Files->delete($Files->get($project['containers'][0]['maindocument']['files'][0]['id']));
            }

            $this->getEventManager()->dispatch(new Event('Model.Project.afterSave.generateAct', $this, [
                'container_id' => $containerId,
            ]));
        }
    }

    /**
     * @param array $rawProject Contain the data of the project
     * @param int $structureId id of the structure
     * @return void
     */
    private function updateGenerateAnnexes($rawProject, int $structureId)
    {
        // maj generate annex
        $isGenerate = $this->fetchTable('StructureSettings')
            ->getValueByModelAndStructureId(
                'act',
                'act_generate',
                $structureId,
            );
        if ($isGenerate) {
            $this->Containers->Annexes->updateGenerate(
                (array)Hash::get($rawProject, 'annexes')
            );
        }
    }

    /**
     * @param array|\Cake\Datasource\EntityInterface $project
     * @return void
     */
    public function syncToPastell(array|\Cake\Datasource\EntityInterface $project): void
    {
        $containerId = !isset($project->containers[0])
            ? $this->getContainerByProject($project->id, $project->structure_id)->id
            : $project->containers[0]->id;

        $event = new Event(
            'Model.Project.afterSaveState',
            $this,
            [
                'id_d' => $containerId,
                'flux' => Configure::read('Flux.actes-generique.name'),
                'flux_name' => Configure::read('Flux.actes-generique.name'),
                'structureId' => $project->structure_id,
                'organizationId' => $this->Structures->getOrganizationId($project->structure_id),
            ]
        );
        $this->getEventManager()->dispatch($event);
    }

    public function afterSave(Event $event, Project $entity)
    {
        if (!$this->isOnPastell($entity) && !$entity->hasCodeActe()) {
            return;
        }

        $this->syncToPastell($entity);
    }
}
