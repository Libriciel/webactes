<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * ProjectTexts Model
 *
 * @property \App\Model\Table\StructuresTable&\Cake\ORM\Association\BelongsTo $Structures
 * @property \App\Model\Table\ProjectsTable&\Cake\ORM\Association\BelongsTo $Projects
 * @property \App\Model\Table\DraftTemplatesTable&\Cake\ORM\Association\BelongsTo $DraftTemplates
 * @property \App\Model\Table\FilesTable&\Cake\ORM\Association\HasMany $Files
 * @method \App\Model\Entity\ProjectText newEmptyEntity()
 * @method \App\Model\Entity\ProjectText newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ProjectText[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProjectText get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProjectText findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ProjectText patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProjectText[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProjectText|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProjectText saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProjectText[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ProjectText[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ProjectText[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ProjectText[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProjectTextsTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('project_texts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Structures', [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('DraftTemplates', [
            'foreignKey' => 'draft_template_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Files', [
            'foreignKey' => 'project_text_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'), ['errorField' => 'structure_id']);
        $rules->add($rules->existsIn(['project_id'], 'Projects'), ['errorField' => 'project_id']);
        $rules->add($rules->existsIn(['draft_template_id'], 'DraftTemplates'), ['errorField' => 'draft_template_id']);

        return $rules;
    }

    /**
     * @param int $projectId Foo
     * @param int $typesactId Foo
     * @param int $structureId Foo
     * @return void
     */
    public function createProjectTextsByProjectIdAndTypesact($projectId, $typesactId, $structureId)
    {
        Log::write('info', self::class . '*******' . __FUNCTION__ . '*******', 'DraftTemplates');

        $typesact = $this->fetchTable('Typesacts')->find()
            ->select(['id'])
            ->contain(['DraftTemplates'])
            ->where([
                'id' => $typesactId,
                'structure_id' => $structureId,
            ])->firstOrFail();

        $draftTemplateIds = Hash::extract($typesact['draft_templates'], '{n}.id');

        foreach ($draftTemplateIds as $draftTemplateId) {
            $projectText = $this->newEntity(
                [
                    'structure_id' => $structureId,
                    'project_id' => $projectId,
                    'draft_template_id' => $draftTemplateId,
                ]
            );
            if ($this->save($projectText, ['atomic' => false])) {
                $file = $this->Files->saveInFsDuplicateFile(
                    $projectId,
                    'draft_template_id',
                    $draftTemplateId,
                    ['project_text_id' => $projectText->id],
                    $structureId
                );
            }
        }
    }
}
