<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pastellfluxs Model
 *
 * @property \App\Model\Table\OrganizationsTable|\App\Model\Table\BelongsTo $Organizations
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\PastellfluxtypesTable|\App\Model\Table\BelongsTo $Pastellfluxtypes
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\BelongsTo $Containers
 * @property \App\Model\Table\SittingsTable|\App\Model\Table\BelongsTo $Sittings
 * @property \App\Model\Table\PastellfluxactionsTable|\App\Model\Table\HasMany $Pastellfluxactions
 * @method \App\Model\Entity\Pastellflux get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pastellflux newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pastellflux[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pastellflux|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pastellflux saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pastellflux patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pastellflux[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pastellflux findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PastellfluxsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('pastellfluxs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Organizations',
            [
                'foreignKey' => 'organization_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Pastellfluxtypes',
            [
                'foreignKey' => 'pastellfluxtype_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Containers',
            [
                'foreignKey' => 'container_id',
            ]
        );
        $this->belongsTo(
            'Sittings',
            [
                'foreignKey' => 'sitting_id',
            ]
        );
        $this->hasMany(
            'Pastellfluxactions',
            [
                'foreignKey' => 'pastellflux_id',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('id_d')
            ->maxLength('id_d', 50)
            ->requirePresence('id_d', 'create')
            ->allowEmptyString('id_d', null, false);

        $validator
            ->scalar('state')
            ->maxLength('state', 10)
            ->requirePresence('state', 'create')
            ->allowEmptyString('state', null, false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['container_id'], 'Containers'));
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->add($rules->existsIn(['pastellfluxtype_id'], 'Pastellfluxtypes'));
        $rules->add($rules->existsIn(['sitting_id'], 'Sittings'));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        return $rules;
    }
}
