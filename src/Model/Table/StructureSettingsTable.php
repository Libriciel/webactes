<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StructureSettings Model
 *
 * @property \App\Model\Table\StructuresTable&\Cake\ORM\Association\BelongsTo $Structures
 * @method \App\Model\Entity\StructureSetting newEmptyEntity()
 * @method \App\Model\Entity\StructureSetting newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\StructureSetting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StructureSetting get($primaryKey, $options = [])
 * @method \App\Model\Entity\StructureSetting findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\StructureSetting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StructureSetting[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\StructureSetting|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StructureSetting saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StructureSetting[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\StructureSetting[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\StructureSetting[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\StructureSetting[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StructureSettingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('structure_settings');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Structures', [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->allowEmptyString('value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'), ['errorField' => 'structure_id']);

        return $rules;
    }

    /**
     * @param string $name Foo
     * @param string $key Foo
     * @param int $structureId Foo
     * @return mixed
     */
    public function getValueByModelAndStructureId($name, $key, $structureId)
    {
        if ($name !== null && $structureId !== null) {
            $structureSetting = $this->find()
                ->select(['value'])
                ->where(['name' => $name, 'structure_id' => $structureId])
                ->first();

            return $structureSetting['value'][$key] ?? false;
        }

        return false;
    }

    /**
     * @param int $structureId Foo
     * @return mixed
     */
    public function getSettingsByStructureId($structureId)
    {
        $structureSetting = [];

        $structureSetting['groupSettings'][] = [
            'code' => 'act',
            'name' => 'Acte',
            'settings' => [
                [
                    'code' => 'generate_number',
                    'type' => 'bool',
                    'name' => 'Numérotation automatique des actes',
                    'active' => $this->getValueByModelAndStructureId(
                        'act',
                        'generate_number',
                        $structureId,
                    ),
                ],
                [
                    'code' => 'act_generate',
                    'type' => 'bool',
                    'name' => 'Génération des actes',
                    'active' => $this->getValueByModelAndStructureId(
                        'act',
                        'act_generate',
                        $structureId,
                    ),
                ],
            ],
        ];
        $structureSetting['groupSettings'][] = [
            'code' => 'convocation',
            'name' => 'Convocation',
            'settings' => [
                [
                    'code' => 'convocation_idelibre',
                    'type' => 'bool',
                    'name' => 'Envoi des convocations via I-délibRE',
                    'active' => $this->getValueByModelAndStructureId(
                        'convocation',
                        'convocation_idelibre',
                        $structureId,
                    ),
                ],
                [
                    'code' => 'convocation_mailsec',
                    'type' => 'bool',
                    'name' => 'Envoi des convocations via mail sécurisé',
                    'active' => $this->getValueByModelAndStructureId(
                        'convocation',
                        'convocation_mailsec',
                        $structureId,
                    ),
                ],
            ],
        ];
        $structureSetting['groupSettings'][] = [
            'code' => 'sitting',
            'name' => 'Séance',
            'settings' => [
                [
                    'code' => 'sitting_enable',
                    'type' => 'bool',
                    'name' => 'Mise en séance des projets',
                    'active' => $this->getValueByModelAndStructureId(
                        'sitting',
                        'sitting_enable',
                        $structureId,
                    ),
                ],
                [
                    'code' => 'sitting_generate_deliberations_list',
                    'type' => 'bool',
                    'name' => 'Génération des listes des délibérations',
                    'active' => $this->getValueByModelAndStructureId(
                        'sitting',
                        'sitting_generate_deliberations_list',
                        $structureId,
                    ),
                ],
                [
                    'code' => 'sitting_generate_verbal_trial',
                    'type' => 'bool',
                    'name' => 'Génération des procès-verbaux',
                    'active' => $this->getValueByModelAndStructureId(
                        'sitting',
                        'sitting_generate_verbal_trial',
                        $structureId,
                    ),
                ],
            ],
        ];
        $structureSetting['groupSettings'][] = [
            'code' => 'project',
            'name' => 'Projet',
            'settings' => [
                [
                    'code' => 'project_generate',
                    'type' => 'bool',
                    'name' => 'Génération des projets',
                    'active' => $this->getValueByModelAndStructureId(
                        'project',
                        'project_generate',
                        $structureId,
                    ),
                ],
                [
                    'code' => 'project_writing',
                    'type' => 'bool',
                    'name' => 'Rédaction des textes avec Collabora online',
                    'active' => $this->getValueByModelAndStructureId(
                        'project',
                        'project_writing',
                        $structureId,
                    ),
                ],
                [
                    'code' => 'project_workflow',
                    'type' => 'bool',
                    'name' => 'Circuit de validation',
                    'active' => $this->getValueByModelAndStructureId(
                        'project',
                        'project_workflow',
                        $structureId,
                    ),
                ],
            ],
        ];

        return $structureSetting;
    }
}
