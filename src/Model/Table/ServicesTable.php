<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Services Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\ServicesTable|\App\Model\Table\BelongsTo $ParentServices
 * @property \App\Model\Table\ServicesTable|\App\Model\Table\HasMany $ChildServices
 * @property \App\Model\Table\UsersTable|\App\Model\Table\BelongsToMany $Users
 * @method \App\Model\Table\Service get($primaryKey, $options = [])
 * @method \App\Model\Entity\Service newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Service[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Service|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Service|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Service patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Service[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Service findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class ServicesTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('services');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'ParentServices',
            [
                'className' => 'Services',
                'foreignKey' => 'parent_id',
            ]
        );
        $this->hasMany(
            'ChildServices',
            [
                'className' => 'Services',
                'foreignKey' => 'parent_id',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Structures',
            [
                'foreignKey' => 'service_id',
                'targetForeignKey' => 'structure_id',
                'joinTable' => 'structures_services',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Users',
            [
                'foreignKey' => 'service_id',
                'targetForeignKey' => 'user_id',
                'joinTable' => 'users_services',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->allowEmptyString('active', null, false);

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['structure_id', 'name']));
        $rules->add($rules->existsIn(['parent_id'], 'ParentServices'));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        return $rules;
    }

    /**
     * Get the service tree
     *
     * @param  array $conditions conditions
     * @return \Cake\ORM\Query
     */
    public function generateTreeListByOrder($conditions)
    {
        $services = $this->find('threaded')
            ->where($conditions)
            ->order('id');

        return $services;
    }

    /**
     * get services with children
     *
     * @param  int $id id
     * @return \Cake\ORM\Query
     */
    public function getWithChildren($id)
    {
        $parentService = $this->get($id);
        $services = $this->find('threaded')
            ->where(
                [
                    'lft >=' => $parentService['lft'],
                    'rght <=' => $parentService['rght'],
                ]
            );

        return $services;
    }
}
