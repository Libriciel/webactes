<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Statesittings Model
 *
 * @property \App\Model\Table\SittingsTable|\App\Model\Table\BelongsToMany $Sittings
 * @method \App\Model\Table\Statesitting get($primaryKey, $options = [])
 * @method \App\Model\Entity\Statesitting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Statesitting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Statesitting|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Statesitting saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Statesitting patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Statesitting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Statesitting findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StatesittingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('statesittings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany(
            'Sittings',
            [
                'foreignKey' => 'statesitting_id',
                'targetForeignKey' => 'sitting_id',
                'joinTable' => 'sittings_statesittings',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('state')
            ->maxLength('state', 100)
            ->requirePresence('state', 'create')
            ->allowEmptyString('state', false)
            ->add('state', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('code')
            ->maxLength('code', 100)
            ->requirePresence('code', 'create')
            ->allowEmptyString('code', false)
            ->add('code', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['state']));
        $rules->add($rules->isUnique(['code']));
        $rules->add($rules->isUnique(['state', 'code']));

        return $rules;
    }
}
