<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Typespiecesjointes Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\NaturesTable|\App\Model\Table\BelongsTo $Natures
 * @method \App\Model\Table\Typespiecesjointe get($primaryKey, $options = [])
 * @method \App\Model\Entity\Typespiecesjointe newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Typespiecesjointe[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Typespiecesjointe|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Typespiecesjointe saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Typespiecesjointe patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Typespiecesjointe[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Typespiecesjointe findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TypespiecesjointesTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('typespiecesjointes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Natures',
            [
                'foreignKey' => 'nature_id',
                'joinType' => 'INNER',
            ]
        );

        $this->hasMany(
            'Maindocuments',
            [
            'foreignKey' => false,
            'conditions' => [
                "{$this->getAlias()}.structure_id = Maindocuments.structure_id",
                "{$this->getAlias()}.codetype = Maindocuments.codetype",
            ],
            'dependent' => false,
            ]
        );

        $this->hasMany(
            'Annexes',
            [
            'foreignKey' => false,
            'conditions' => [
                "{$this->getAlias()}.structure_id = Annexes.structure_id",
                "{$this->getAlias()}.codetype = Annexes.codetype",
            ],
            'dependent' => false,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('codetype')
            ->maxLength('codetype', 5)
            ->requirePresence('codetype', 'create')
            ->allowEmptyString('codetype', null, false);

        $validator
            ->scalar('libelle')
            ->maxLength('libelle', 100)
            ->requirePresence('libelle', 'create')
            ->allowEmptyString('libelle', null, false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['nature_id'], 'Natures'));
        $rules->add($rules->isUnique(['structure_id', 'nature_id', 'codetype', 'libelle']));

        return $rules;
    }

    /**
     * return codetype from id
     *
     * @param  int $id typeId
     * @return string
     */
    public function getCodeTypeFromId($id)
    {
        $type = $this->get($id);

        return $type->codetype;
    }

    /**
     * return detail from codetype
     *
     * @param  string $codetype codetype
     * @param  int $structure_id codetype
     * @return string
     */
    /*public function getDetailFromCodetype($codetype, $structure_id)
    {
        $type = $this->find()
            ->select(['id', 'codetype', 'libelle'])
            ->where(['codetype' => $codetype, 'structure_id' => $structure_id])
            ->firstOrFail();

        return $type;
    }*/
}
