<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Typesittings Model
 *
 * @property \App\Model\Table\StructuresTable&\Cake\ORM\Association\BelongsTo $Structures
 * @property \App\Model\Table\SittingsTable&\Cake\ORM\Association\HasMany $Sittings
 * @property \App\Model\Table\TypesactsTable&\Cake\ORM\Association\BelongsToMany $Typesacts
 * @method \App\Model\Entity\Typesitting newEmptyEntity()
 * @method \App\Model\Entity\Typesitting newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Typesitting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Typesitting get($primaryKey, $options = [])
 * @method \App\Model\Entity\Typesitting findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Typesitting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Typesitting[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Typesitting|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Typesitting saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Typesitting[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Typesitting[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Typesitting[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Typesitting[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TypesittingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('typesittings');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');
        $this->addBehavior('Permission');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Sittings',
            [
                'foreignKey' => 'typesitting_id',
                'dependent' => true,
            ]
        );

        $this->hasMany(
            'OpenedSittings',
            [
                'className' => 'Sittings',
                'foreignKey' => 'typesitting_id',
                'dependent' => true,
                //                'finder' => , finder sittings opened
            ]
        );

        $this->hasMany(
            'TypesactsTypesittings',
            [
                'foreignKey' => 'typesitting_id',
                'dependent' => true,
            ]
        );

        $this->belongsToMany(
            'Typesacts',
            [
                'foreignKey' => 'typesitting_id',
                'targetForeignKey' => 'typesact_id',
                'joinTable' => 'typesacts_typesittings',
                'dependent' => true,
                'propertyName' => 'typeActs',
            ]
        );

        $this->belongsToMany(
            'ActorGroups',
            [
                'foreignKey' => 'typesitting_id',
                'targetForeignKey' => 'actor_group_id',
                'joinTable' => 'actor_groups_typesittings',
                'dependent' => true,
                'propertyName' => 'actorGroups',
            ]
        );

        $this->belongsTo('ConvocationGenerateTemplates', [
            'className' => 'GenerateTemplates',
        ])
        ->setForeignKey('generate_template_convocation_id')
        ->setJoinType('INNER')
        ->setDependent(true);

        $this->belongsTo('ExecutiveSummaryGenerateTemplates', [
            'className' => 'GenerateTemplates',
        ])
        ->setForeignKey('generate_template_executive_summary_id')
        ->setJoinType('INNER')
        ->setDependent(true);

        $this->belongsTo('DeliberationsListGenerateTemplates', [
            'className' => 'GenerateTemplates',
        ])
        ->setForeignKey('generate_template_deliberations_list_id')
        ->setJoinType('INNER')
        ->setDependent(true);

        $this->belongsTo('VerbalTrialGenerateTemplates', [
            'className' => 'GenerateTemplates',
        ])
        ->setForeignKey('generate_template_verbal_trial_id')
        ->setJoinType('INNER')
        ->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->integer('structure_id')
            ->requirePresence('structure_id', 'create');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        $validator
            ->boolean('isdeliberating')
            ->notEmptyString('isdeliberating');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['structure_id', 'name']));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        // deactivation forbidden if not deactivable
        $rules->addUpdate(
            function ($entity) {
                $dirty = $entity->getDirty();

                // ne pas attribuer la règle
                // Si (on ne modifie pas l'attribut "active" et que l'attribut active est === false)
                // OU que l'attribut === true
                // Alors je retroune true => je n'applique pas la restriction.
                // Sinon je veux modifier active SI l'attribut active est === false et valeur availableActions['can_be_deactivate']['value']
                return !Hash::contains($dirty, ['active']) || $entity->active
                ? true
                : !$entity->active && $entity->availableActions['can_be_deactivate']['value'];
            },
            'typeSittingCantBeDeactivate',
            [
            'errorField' => 'id',
            'message' => 'Cannot deactivate a type sitting with open sitting',
            ]
        );

        // modification forbidden if not editable
        $rules->addUpdate(
            function ($entity) {

                return $entity->availableActions['can_be_edited']['value'];
            },
            'typeSittingCantBeEdited',
            [
            'errorField' => 'id',
            'message' => 'Cannot edit a type sitting without the admin role',
            ]
        );

        // deletion forbidden if not deletable
        $rules->addDelete(
            function ($entity) {

                return $entity->availableActions['can_be_deleted']['value'];
            },
            'typeSittingCantBeDeleted',
            [
            'errorField' => 'id',
            'message' => 'Le type de séance est lié à au moins une séance',
            ]
        );

        return $rules;
    }

    /**
     * actorsSummonByTypeSittingId
     *
     * @param int $id typeSittingId
     * @return array
     */
    public function actorsSummonByTypeSittingId(int $id): array
    {
        $typeSitting = $this->find('all', [
            'contain' => [
                'ActorGroups',
                'ActorGroups.Actors',
                'Sittings' => ['conditions' => [ 'Sittings.id' => $id]],
            ],
        ])->firstOrFail();

        return Hash::extract($typeSitting, 'actorGroups.{n}.actors.{n}.id');
    }
}
