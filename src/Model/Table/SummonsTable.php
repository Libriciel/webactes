<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Utilities\Api\Pastell\Pastell;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Http\Client;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use Cake\Mailer\Message;
use Cake\Mailer\Renderer;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Summons Model
 *
 * @property \App\Model\Table\StructuresTable&\Cake\ORM\Association\BelongsTo $Structures
 * @property \App\Model\Table\SittingsTable&\Cake\ORM\Association\BelongsTo $Sittings
 * @property \App\Model\Table\AttachmentsSummonsTable&\Cake\ORM\Association\BelongsTo $AttachmentsSummons
 * @property \App\Model\Table\FilesTable&\Cake\ORM\Association\BelongsTo $Files
 * @property \App\Model\Table\GenerationerrorsTable&\Cake\ORM\Association\HasMany $Generationerrors
 * @method \App\Model\Entity\Summon newEmptyEntity()
 * @method \App\Model\Entity\Summon newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Summon[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Summon get($primaryKey, $options = [])
 * @method \App\Model\Entity\Summon findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Summon patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Summon[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Summon|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Summon saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Summon[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Summon[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Summon[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Summon[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SummonsTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('summons');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SummonTypes', [
            'foreignKey' => 'summon_type_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Structures', [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Sittings', [
            'foreignKey' => 'sitting_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('AttachmentsSummons', [
            'foreignKey' => 'summon_id',
        ]);
        $this->hasMany('Convocations', [
            'foreignKey' => 'summon_id',
        ]);

        $this->hasMany(
            'Generationerrors',
            [
                'bindingKey' => 'sitting_id',
                'foreignKey' => 'foreign_key',
                'conditions' => [
                    'type IN' => ['convocation', 'executive_summary'],
                ],
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('summon_type_id')
            ->allowEmptyString('summon_type_id', null, 'create');

        $validator
            ->integer('sitting_id')
            ->allowEmptyString('sitting_id', null, 'create');

        $validator
            ->date('date_send')
            ->allowEmptyDate('date_send');

        $validator
            ->boolean('is_sent_individually')
            ->notEmptyString('is_sent_individually');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['summon_type_id'], 'SummonTypes'), ['errorField' => 'summon_type_id']);
        $rules->add($rules->existsIn(['structure_id'], 'Structures'), ['errorField' => 'structure_id']);
        $rules->add($rules->existsIn(['sitting_id'], 'Sittings'), ['errorField' => 'sitting_id']);

        return $rules;
    }

    /**
     * @param \Cake\Event\Event $event Foo
     * @param \Cake\Datasource\EntityInterface $entity Foo
     * @param \ArrayObject $options Foo
     * @return void Foo
     */
    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        // create convocation is a new summons
        if ($entity->isNew()) {
            $typesittings = $this->fetchTable('Typesittings');
            $typesitting = $typesittings->find('all', [
                'contain' => [
                    'ActorGroups',
                    'ActorGroups.Actors',
                ],
            ])->where(['id' =>
                $this->fetchTable('Sittings')
                    ->find()
                    ->select(['Sittings.typesitting_id'])
                    ->where(['Sittings.id' => $entity->sitting_id])])
                ->firstOrFail();
            $actors = Hash::extract($typesitting, 'actorGroups.{n}.actors.{n}.id');
            foreach ($actors as $actorId) {
                $convocation = $this->Convocations->newEntity(
                    [
                        'structure_id' => $entity->structure_id,
                        'actor_id' => $actorId,
                        'summon_id' => $entity->id,
                    ]
                );
                $this->Convocations->save($convocation);
            }
        }
    }

    /**
     * @param int $summonId Foo
     * @param int $pastellFluxType_id ID du type de flux
     * @param int $structureId structureId
     * @return array|bool
     * @access  public
     * @created 02/07/2019
     * @version V0.0.9
     */
    public function buildPastellDocumentMailSec(int $summonId, int $pastellFluxType_id, int $structureId)
    {
        $structure = $this->Structures->get($structureId);

        $summon = $this->find()
            ->where(
                [
                    'Summons.id' => $summonId,
                    'Summons.structure_id' => $structure->id,
                ]
            )
            ->contain(
                [
                    'Structures',
                    'Convocations',
                    'Convocations.Actors' => function ($q) {
                        return $q
                            ->where(['Actors.active' => true]);
                    },
                    'AttachmentsSummons.Files',
                ]
            )
            ->firstOrFail();

        $attachments = [];
        $i = 0;

        foreach ($summon->attachments_summons as $attachmentSummon) {
            // Si la note de synthèse n'a pas été renseignée lors de l'upload des fichiers,
            // alors on n'essaye pas de la récupérer.
            if (isset($attachmentSummon->file)) {
                $attachments[] = [
                    'rank' => $i++,
                    'file_name' => $attachmentSummon->file->name,
                    'file_content' => file_get_contents($attachmentSummon->file->path),
                ];
            }
        }

        $documentPastell = $this->createDocumentOnPastell(
            $structure->id_orchestration,
            $structure->id
        );
        if (empty($documentPastell['id_d'])) {
            return $documentPastell['error-message'];
        }

        $documentPastellId = $documentPastell['id_d'];

        $success = $this->savePastellFlux(
            $pastellFluxType_id,
            $documentPastellId,
            $summon->id,
            $structure->id_orchestration,
            $structure->id,
            'PENDING'
        );

        $render = new Renderer();
        $render->viewBuilder()->setTemplate('convocation');
        $data = [
            'to' => env('PASTELL_MAILSEC_TO', 'webactes@webactes.dev.libriciel.net'),
            'bcc' => implode(',', Hash::extract($summon->convocations, '{n}.actor.email')),
            'titre' => __('Convocation à la séance id (%s)', $summon->sitting_id),
            'objet' => 'Convocation à une séance',
            'message' => $render->render('', [Message::MESSAGE_TEXT]),
        ];

        $this->patchDocumentOnPastell($structure->id_orchestration, $documentPastellId, $data, $structure->id);

        foreach ($attachments as $attachment) {
            $rank = $attachment['rank'];
            unset($attachment['rank']);
            $this->attachmentFile($structure->id, $structure->id_orchestration, $documentPastellId, $attachment, $rank);
        }

        //ACTION send
        //sauvegarder le pastell id dans le champ pastell_id de la table convocation
        $convocations = $this->fetchTable('Convocations');
        $convocations->updateAll(
            [
                'pastell_id' => $documentPastellId,
                'date_send' => FrozenTime::now(),
            ],
            [
                'summon_id' => $summon->id,
            ]
        );

        $summon = $this->patchEntity($summon, ['date_send' => FrozenTime::now()]);
        $this->save($summon);

        $pastellResponse = $this->triggerAnActionOnDocument(
            $structure->id_orchestration,
            $documentPastellId,
            'envoi',
            $structure->id
        );

        //Si pas de return ayant pour cause un erreur sur Pastell.
        $success = $this->savePastellFlux(
            $pastellFluxType_id,
            $documentPastellId,
            $summon->id,
            $structure->organization_id,
            $structure->id,
            isset($pastellResponse['result']) && $pastellResponse['result'] ? 'DONE' : 'FAILED',
        );

        if ($success == false) {
            Log::error("erreur lors de l'enregistrement DONE de l'etat du flux pastell", 'Pastellfluxs');
        }

        return true;
    }

    /**
     * @param int $id_orchestration The pastell id of the strucutre
     * @param int $structureId structureId
     * @return mixed
     */
    public function createDocumentOnPastell(int $id_orchestration, int $structureId)
    {
        $client = $this->getPastellClient($structureId);
        $response = $client->createDocument($id_orchestration, 'mailsec');

        return $response->getJson();
    }

    /**
     * return pastellClient
     *
     * @param int $structureId structureId
     * @return \App\Utilities\Api\Pastell\Pastell
     */
    private function getPastellClient($structureId)
    {
        /**
         * @var \App\Model\Table\ConnecteursTable $Connecteurs
         */
        $Connecteurs = $this->fetchTable('Connecteurs');
        $connecteur = $Connecteurs->find()->where([
            'connector_type_id' => 1,
            'structure_id' => $structureId,
        ])->firstOrFail();

        $client = new Client(
            [
                'auth' => [
                    'username' => $connecteur->get('username'),
                    'password' => $connecteur->get('password'),
                ],
                'ssl_verify_peer' => false,
                'redirect' => 2,
                'timeout' => 120,
            ]
        );

        return new Pastell($client, $connecteur->get('url'));
    }

    /**
     * @param int $pastellFluxType_id ID du flux
     * @param int $id_document ID du document sous pastell
     * @param int $summon_id ID du container
     * @param int $organization_id ID de l'organization
     * @param int $structure_id ID de la structure
     * @param string $state Etat du flux
     * @param string $message Message retourné par PASTELL lors de l'éxécution de
     *                                    l'ction
     * @return bool
     * @access  public
     * @created 09/07/2019
     * @version V0.0.9
     */
    public function savePastellFlux($pastellFluxType_id, $id_document, $summon_id, $organization_id, $structure_id, $state, $message = null)// @codingStandardsIgnoreLine
    {
        $pastellFluxs = $this->fetchTable('Pastellfluxs');
        $pastellflux = $pastellFluxs->newEntity(
            [
                'organization_id' => $organization_id,
                'structure_id' => $structure_id,
                'pastellfluxtype_id' => $pastellFluxType_id,
                'id_d' => $id_document,
                'summon_id' => $summon_id,
                'state' => $state,
                'comments' => $message,
            ]
        );

        $success = $pastellFluxs->save($pastellflux) !== false;

        return $success;
    }

    /**
     * Fonction permettant l'envoie des modifications à apporter sur le document
     *
     * @param int $id_orchestration : id_e sous pastell de notre structure
     * @param int $id_document : id_d sous pastell de notre document
     * @param array $data valeur à modifier sous
     *                                 pastell
     * @param int $structureId structureId
     * @return mixed
     * @access  public
     * @created 09/07/2019
     * @version V0.0.9
     */
    public function patchDocumentOnPastell($id_orchestration, $id_document, $data, $structureId)
    {
        $client = $this->getPastellClient($structureId);
        $response = $client->editDocument($id_orchestration, $id_document, $data);

        return $response->getJson();
    }

    /**
     * Fonction permettant l'envoie du document principal sous pastell
     *
     * @param int $structureId structureId
     * @param int $orchestrationId id_e sous pastell de notre structure
     * @param string $documentId id_d sous pastell de notre document
     * @param array $data Tableau avec le nom et les datas du document principale à envoyer sous
     *                                 pastell
     * @param int $rank Foo
     * @return mixed
     * @access  public
     * @created 09/07/2019
     * @version V0.0.9
     */
    public function attachmentFile(int $structureId, int $orchestrationId, string $documentId, array $data, int $rank)
    {
        $client = $this->getPastellClient($structureId);
        $response = $client->sendAttachmentFileToDocument($orchestrationId, $documentId, $data, $rank);

        return $response->getJson();
    }

    /**
     * REMI PQ passer par le container pour ça ?
     * Exécute une action sur un document PASTELL
     *
     * @param int $entityId The PASTELL id => id_orchestration
     * @param string $documentId The PASTELL docuemnt id => id_d
     * @param string $actionName Nom de l'action à effectuer sur un
     *                             document
     * @param int $structureId structureId
     * @return mixed
     */
    public function triggerAnActionOnDocument($entityId, $documentId, $actionName, $structureId)
    {
        $client = $this->getPastellClient($structureId);
        $document = $client->triggerActionOnDocument($entityId, $documentId, $actionName);

        return $document->getJson();
    }

    /**
     * @param int $id SittingId
     * @return \Cake\I18n\FrozenTime|null
     */
    public function getConvocationDateBySittingId(int $id): ?FrozenDate
    {
        $summon = $this->find()->select('date_send')->where(['sitting_id' => $id])->orderAsc('date_send')->first();

        return $summon['date_send'] ?? null;
    }
}
