<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContainersStateacts Model
 *
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\BelongsTo $Containers
 * @property \App\Model\Table\StateactsTable|\App\Model\Table\BelongsTo $Stateacts
 * @method \App\Model\Table\ContainersStateact get($primaryKey, $options = [])
 * @method \App\Model\Entity\ContainersStateact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ContainersStateact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ContainersStateact|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContainersStateact|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContainersStateact patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ContainersStateact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ContainersStateact findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContainersStateactsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('containers_stateacts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Containers',
            [
            'foreignKey' => 'container_id',
            'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Stateacts',
            [
            'foreignKey' => 'stateact_id',
            'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['container_id'], 'Containers'));
        $rules->add($rules->existsIn(['stateact_id'], 'Stateacts'));

        return $rules;
    }

    /** getStateId
     *
     * @param int $id ContainerId
     * @return int
     */
    public function getStateId(int $id): int
    {
        $containersState = $this->find()
            ->where(['container_id' => $id])
            ->all()
            ->max('stateact_id');

        return (int)($containersState->stateact_id ?? 0);
    }

    /**
     * Retourne une (sous) query contenant le champ id de la table containers_stateacts pour le dernier enregistrement
     * d'un état d'un conteneur donné (tri par id de containers_stateacts descendant).
     *
     * @param string $alias L'alias de la clé primaire de la table Containers de la requête principale
     * @return \Cake\ORM\Query
     */
    public function getLastByContainersIdSubquery(string $alias = 'Containers.id')
    {
        return $this
            ->find()
            ->select(['containers_stateacts.id'])
            ->from(['containers_stateacts'])
            ->where(function (QueryExpression $exp, Query $q) use ($alias) {
                return $exp->equalFields('containers_stateacts.container_id', $alias);
            })
            ->orderDesc('containers_stateacts.modified')
            ->limit(1);
    }
}
