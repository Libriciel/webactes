<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Actorsgroups Model
 *
 * @property \App\Model\Table\|BelongsTo $Projects
 * @property \App\Model\Table\ActorsTable|\App\Model\Table\BelongsTo $Actors
 * @method \App\Model\Table\ActorsProject get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActorsProject newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ActorsProject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActorsProject|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsProject|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsProject patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsProject[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsProject findOrCreate($search, callable $callback = null, $options = [])
 */
class ActorGroupsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->setEntityClass('App\Model\Entity\ActorGroups');
        $this->setTable('actor_groups');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('LimitToStructure');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Permission');

        $this->belongsToMany('Actors', [
            'foreignKey' => 'actor_group_id',
            'targetForeignKey' => 'actor_id',
            'joinTable' => 'actors_actor_groups',
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');
        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        // $rules->add($rules->existsIn(['actor_id'], 'Actors'));
        // $rules->add($rules->existsIn(['project_id'], 'Projects'));
        //$rules->add($rules->existsIn(['id'], 'Actorsgroups'));

        // deletion forbidden if not deletable
        // $rules->addDelete(function ($entity, $options) {
        //     return $entity->canBeDeleted();
        // }, 'actorsgroupsCantBeDeleted', [
        //     'errorField' => 'id',
        //     'message' => "Le groupe d'acteur est lié à au moins une séance",
        // ]);

        // $rules->addDelete(function ($entity, $options) {
        //     return $entity->canBeDeleted();
        // }, 'actorsgroupsCantBeDeleted', [
        //     'errorField' => 'id',
        //     'message' => "Le groupe d'acteur est lié à au moins une séance",
        // ]);
        // $rules->add(new Rule\IsUnique(['name']), [
        //     'errorField' => 'name',
        //     'message' => 'Le nom du groupe doit être unique'
        // ]);

        return $rules;
    }
}
