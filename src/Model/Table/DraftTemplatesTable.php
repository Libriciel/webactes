<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DraftTemplates Model
 *
 * @property \App\Model\Table\StructuresTable&\Cake\ORM\Association\BelongsTo $Structures
 * @property \App\Model\Table\DraftTemplateTypesTable&\Cake\ORM\Association\BelongsTo $DraftTemplateTypes
 * @method \App\Model\Entity\DraftTemplate newEmptyEntity()
 * @method \App\Model\Entity\DraftTemplate newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\DraftTemplate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DraftTemplate get($primaryKey, $options = [])
 * @method \App\Model\Entity\DraftTemplate findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\DraftTemplate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DraftTemplate[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\DraftTemplate|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DraftTemplate saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DraftTemplate[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DraftTemplate[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\DraftTemplate[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DraftTemplate[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DraftTemplatesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('draft_templates');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('LimitToStructure');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Permission');
        $this->belongsTo('Structures', [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('DraftTemplateTypes', [
            'foreignKey' => 'draft_template_type_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Files', [
            'foreignKey' => 'draft_template_id',
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'), ['errorField' => 'structure_id']);
        $rules->add(
            $rules->existsIn(['draft_template_type_id'], 'DraftTemplateTypes'),
            ['errorField' => 'draft_template_type_id']
        );

        return $rules;
    }
}
