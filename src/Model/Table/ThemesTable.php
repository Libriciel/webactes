<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Themes Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\ThemesTable|\App\Model\Table\BelongsTo $ParentThemes
 * @property \App\Model\Table\ProjectsTable|\App\Model\Table\HasMany $Projects
 * @property \App\Model\Table\ThemesTable|\App\Model\Table\HasMany $ChildThemes
 * @method \App\Model\Table\Theme get($primaryKey, $options = [])
 * @method \App\Model\Entity\Theme newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Theme[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Theme|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Theme|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Theme patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Theme[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Theme findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class ThemesTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('themes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');
        $this->addBehavior('LimitToStructure');
        $this->addBehavior('SoftDeletion');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'ParentThemes',
            [
                'className' => 'Themes',
                'foreignKey' => 'parent_id',
            ]
        );
        $this->hasMany(
            'Projects',
            [
                'foreignKey' => 'theme_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'ChildThemes',
            [
                'className' => 'Themes',
                'foreignKey' => 'parent_id',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('structure_id')
            ->requirePresence('structure_id', 'create');

        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->allowEmptyString('active', null, false);

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        $validator
            ->scalar('position')
            ->maxLength('position', 100)
            ->requirePresence('position', 'create')
            ->allowEmptyString('position', null, false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['structure_id', 'name', 'active','position']));
        $rules->add($rules->existsIn(['parent_id'], 'ParentThemes'));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        $rules->addDelete(
            function ($entity, $options) {

                return $entity->is_deletable;
            },
            'themeCantBeDeleted',
            [
            'errorField' => 'id',
            'message' => 'Cannot delete a theme in use',
            ]
        );

        $rules->addUpdate(
            function ($entity, $options) {
                $dirty = $entity->getDirty();

                return !Hash::contains($dirty, ['active']) || $entity->active
                ? true
                : !$entity->active && $entity->is_deactivable;
            },
            'themeCantBeDeactivate',
            [
            'errorField' => 'id',
            'message' => 'Cannot deactivate a theme in use',
            ]
        );

        return $rules;
    }

    /**
     * Get the service tree
     *
     * @param  array $conditions conditions
     * @return \Cake\ORM\Query
     */
    public function generateTreeListByOrder($conditions = null)
    {
        $themes = $this->find('threaded');
        if ($conditions) {
            $themes = $themes->where($conditions);
        }

        return $themes->order(['name']);
    }
}
