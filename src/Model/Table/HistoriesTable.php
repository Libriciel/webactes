<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Histories Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\BelongsTo $Containers
 * @property \App\Model\Table\UsersTable|\App\Model\Table\BelongsTo $Users
 * @method \App\Model\Table\History get($primaryKey, $options = [])
 * @method \App\Model\Entity\History newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\History[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\History|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\History|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\History patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\History[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\History findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class HistoriesTable extends Table
{
    public const CREATE_PROJECT = 'Création de projet';
    public const EDIT_PROJECT = 'Édition de projet';
    public const SEND_TO_CIRCUIT = 'Envoi dans un circuit';
    public const VALIDATION = 'Validé';
    public const REJECT = 'Refusé';
    public const EMERGENCY_VALIDATION = 'Validation en urgence';
    public const MANUAL_SIGNATURE = 'Signature manuelle';
    public const SEND_TDT = 'Dépôt TDT';
    public const TDT_TRANSMISSION = 'Transmission TDT';
    public const SEND_TDT_ERROR = 'Erreur envoi TDT';
    public const TDT_ERROR = 'Erreur TDT';
    public const TDT_ACQUITTED = 'Acquitté TDT';
    public const TDT_CANCEL = 'Annulé TDT';
    public const PARAPHEUR_SIGNING = 'Envoyé au i-parapheur';
    public const PARAPHEUR_SIGNED = 'Signé via i-parapheur';
    public const PARAPHEUR_REFUSED = 'Refusé via i-parapheur';
    public const EDIT_MAIN_DOCUMENT = 'Remplacement du document principal';
    public const FORCE_CHANGE_STATUS = 'Statut forcé';
    public const FORCE_CHANGE_STATUS_TO_TDT_ACQUITTED = self::FORCE_CHANGE_STATUS . ' : ' . self::TDT_ACQUITTED;
    public const VOTED_APPROVED = 'Voté et approuvé';
    public const VOTED_REJECTED = 'Voté et rejeté';
    public const TAKE_NOTE = 'Prend acte';

    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('histories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Containers',
            [
                'foreignKey' => 'container_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Users',
            [
                'foreignKey' => 'user_id',
                'joinType' => 'INNER',
                'strategy' => 'select',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('comment')
            ->requirePresence('comment', 'create')
            ->allowEmptyString('comment', null, false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['container_id'], 'Containers'));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * Array of actions
     *
     * @return array all actions
     */
    public static function getAllActions(): array
    {
        return [
            HistoriesTable::TDT_TRANSMISSION,
            HistoriesTable::CREATE_PROJECT,
            HistoriesTable::EDIT_PROJECT,
            HistoriesTable::SEND_TO_CIRCUIT,
            HistoriesTable::VALIDATION,
            HistoriesTable::REJECT,
            HistoriesTable::EMERGENCY_VALIDATION,
            HistoriesTable::MANUAL_SIGNATURE,
            HistoriesTable::SEND_TDT,
            HistoriesTable::TDT_TRANSMISSION,
            HistoriesTable::SEND_TDT_ERROR,
            HistoriesTable::TDT_ERROR,
            HistoriesTable::TDT_ACQUITTED,
            HistoriesTable::TDT_CANCEL,
            HistoriesTable::PARAPHEUR_SIGNING,
            HistoriesTable::PARAPHEUR_SIGNED,
            HistoriesTable::PARAPHEUR_REFUSED,
            HistoriesTable::EDIT_MAIN_DOCUMENT,
        ];
    }

    /**
     * Retourne une (sous) query contenant le champ id de la table histories pour le dernier enregistrement
     * d'un historique d'un conteneur donné (tri par id de histories descendant).
     *
     * @param string $alias L'alias de la clé primaire de la table Containers de la requête principale
     * @return \Cake\ORM\Query
     */
    public function getLastByContainersIdSubquery(string $alias = 'Containers.id')
    {
        return $this
            ->find()
            ->select(['histories.id'])
            ->from(['histories'])
            ->where(function (QueryExpression $exp, Query $q) use ($alias) {
                return $exp->equalFields('histories.container_id', $alias);
            })
            ->orderDesc('histories.created')
            ->limit(1);
    }

    /**
     * Retourne une (sous) query contenant le champ id de la table histories pour le premier enregistrement
     * d'un historique ayant un commentaire donné, pour un utilisateur donné et un conteneur donné (pas de tri).
     *
     * @param string $comment Le commentaire à rechercher
     * @param int $userId L'id de l'utilisateur recherché
     * @param string $containerId L'alias de la clé primaire de la table Containers de la requête principale
     * @return \Cake\ORM\Query
     */
    public function getByCommentAndUserIdAndContainerIdSubquery(
        string $comment,
        int $userId,
        string $containerId = 'Containers.id'
    ) {
        return $this
            ->find()
            ->select(['histories.id'])
            ->from(['histories'])
            ->where([
                'histories.comment' => $comment,
                'histories.user_id' => $userId,
            ])
            ->where(function (QueryExpression $exp, Query $q) use ($containerId) {
                return $exp->equalFields('histories.container_id', $containerId);
            })
            ->limit(1);
    }
}
