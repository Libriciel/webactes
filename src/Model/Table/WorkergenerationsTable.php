<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Log\Log;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Workergenerations Model
 *
 * @method \App\Model\Entity\Workergeneration newEmptyEntity()
 * @method \App\Model\Entity\Workergeneration newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Workergeneration[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Workergeneration get($primaryKey, $options = [])
 * @method \App\Model\Entity\Workergeneration findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Workergeneration patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Workergeneration[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Workergeneration|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Workergeneration saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Workergeneration[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Workergeneration[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Workergeneration[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Workergeneration[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WorkergenerationsTable extends Table
{
    /**
     * @var \App\Model\Table\WorkergenerationsTable|null
     */
    protected static $instance;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('workergenerations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('pid')
            ->requirePresence('pid', 'create')
            ->notEmptyString('pid');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->integer('foreign_key')
            ->requirePresence('foreign_key', 'create')
            ->notEmptyString('foreign_key');

        return $validator;
    }

    /**
     * Static utility function to persist a record in the table before doing the generation.
     *
     * Logs a critical error if the record cannot be saved.
     *
     * @param int $pid The worker's PID
     * @param string $type The type of generation
     * @param array|int $foreignKeys The foreign keys for the generation
     * @return bool
     * @throws \Exception
     */
    public static function persist(int $pid, string $type, $foreignKeys): bool
    {
        if (static::$instance === null) {
            static::$instance = TableRegistry::getTableLocator()->get('Workergenerations');
        }
        $foreignKeys = (array)$foreignKeys;

        $entities = [];
        foreach ($foreignKeys as $foreignKey) {
            $entities[] = static::$instance->newEntity(['pid' => $pid, 'type' => $type, 'foreign_key' => $foreignKey]);
        }

        $result = static::$instance->saveMany($entities, ['atomic' => false]);
        if ($result === false) {
            $errors = [];
            foreach ($entities as $entity) {
                $errors[] = $entity->getErrors();
            }
            Log::critical(sprintf('%s error(s): %s', __METHOD__, print_r($errors, true)));
        }

        return $result !== false;
    }

    /**
     * Static utility function to delete records in the table after doing the generation or during the transformation
     * to a generationerrors.
     *
     * @param ?int $pid The worker's PID
     * @param string $type The type of generation
     * @param array|int $foreignKeys The foreign key(s) for the generation
     * @return bool
     */
    public static function cleanup(?int $pid, string $type, $foreignKeys): bool
    {
        if (static::$instance === null) {
            static::$instance = TableRegistry::getTableLocator()->get('Workergenerations');
        }
        $foreignKeys = (array)$foreignKeys;
        $conditions = ['type' => $type, 'foreign_key IN' => $foreignKeys];
        if ($pid !== null) {
            $conditions['pid'] = $pid;
        }

        return static::$instance->deleteAll($conditions) > 0;
    }

    /**
     * Static utility function to transform a workergenerations record into a generationerrors record.
     *
     * @param ?int $pid The worker's PID
     * @param string $message The error message
     * @return bool
     */
    public static function toGenerationerror(int $pid, string $message): bool
    {
        if (static::$instance === null) {
            static::$instance = TableRegistry::getTableLocator()->get('Workergenerations');
        }

        $workergeneration = static::$instance
            ->find()
            ->select(['id', 'type', 'foreign_key'])
            ->where(['pid' => $pid])
            ->enableHydration(false)
            ->first();

        if (empty($workergeneration) === false) {
            GenerationerrorsTable::persist($workergeneration['type'], $workergeneration['foreign_key'], $message);
            static::cleanup($pid, $workergeneration['type'], $workergeneration['foreign_key']);

            return true;
        }

        return false;
    }
}
