<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tabs Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\TemplatesTable|\App\Model\Table\BelongsTo $Templates
 * @property \App\Model\Table\MetadatasTable|\App\Model\Table\HasMany $Metadatas
 * @method \App\Model\Table\Tab get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tab newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tab[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tab|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tab|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tab patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tab[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tab findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TabsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('tabs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Templates',
            [
                'foreignKey' => 'template_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Metadatas',
            [
                'foreignKey' => 'tab_id',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('position')
            ->requirePresence('position', 'create')
            ->allowEmptyString('position', false);

        $validator
            ->scalar('tab')
            ->maxLength('tab', 10)
            ->requirePresence('tab', 'create')
            ->allowEmptyString('tab', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['template_id'], 'Templates'));

        return $rules;
    }
}
