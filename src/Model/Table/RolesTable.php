<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Roles Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @method \App\Model\Table\Role get($primaryKey, $options = [])
 * @method \App\Model\Entity\Role newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Role[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Role|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Role|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Role patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Role[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Role findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RolesTable extends Table
{
    public const SUPER_ADMINISTRATOR = 'Super Administrateur';
    public const VALIDATOR_REDACTOR_NAME = 'Rédacteur / Valideur';
    public const GENERAL_SECRETARIAT = 'Secrétariat général';
    public const ADMINISTRATOR = 'Administrateur';
    public const FUNCTIONNAL_ADMINISTRATOR = 'Administrateur Fonctionnel';

    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('roles');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('LibricielCakephp3Database.AssociatedTables');

        $this->addBehavior(
            'LibricielCakephp3Database.GuardedAssociatedTables',
            [
                'delete' => [],
                'update' => [
                    'unguardedFields' => ['name', 'active'],
                ],
            ]
        );
        $this->addBehavior('LimitToStructure');

        $this->addBehavior('Timestamp');

        $this->addBehavior('LibricielCakephp3Database.VirtualFields');

        $this->behaviors()->get('VirtualFields')->disableAuto(['is_updatable', 'is_linked']);

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );

        $this->belongsToMany(
            'Users',
            [
                'foreignKey' => 'role_id',
                'targetForeignKey' => 'user_id',
                'joinTable' => 'roles_users',
                'dependent' => true,
                'through' => RolesUsersTable::class,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['name', 'structure_id']));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        return $rules;
    }

    /**
     * return a role from its name
     *
     * @param  string $name        roleName
     * @param  int    $structureId structureId
     * @throws \Cake\Datasource\Exception\RecordNotFoundException
     * @return mixed
     */
    public function findByName($name, $structureId)
    {
        return $this->find()->where(['name' => $name, 'structure_id' => $structureId])->firstOrFail();
    }

    /**
     * Retourne tous les rôles sans distinction de structure
     *
     * @return array
     */
    public function getRolesNames()
    {
        $query = $this->find()
            ->where(function ($exp) {
                return $exp
                    ->notEq('name', static::SUPER_ADMINISTRATOR);
            });

        return $query->distinct('name')->toArray();
    }
}
