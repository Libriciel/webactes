<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Http\Exception\NotFoundException;
use Cake\I18n\FrozenDate;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Counters Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\SequencesTable|\App\Model\Table\BelongsTo $Sequences
 * @property \App\Model\Table\TypesactsTable|\App\Model\Table\BelongsTo $Typeacts
 * @method \App\Model\Entity\Counter get($primaryKey, $options = [])
 * @method \App\Model\Entity\Counter newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Counter[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Counter|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Counter saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Counter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Counter[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Counter findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CountersTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     * @created 17/12/2020
     * @version V2
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('counters');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');
        $this->addBehavior('Permission');

        $this->belongsTo('Structures', [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Sequences', [
            'foreignKey' => 'sequence_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Typesacts', [
            'foreignKey' => 'counter_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @created 17/12/2020
     * @version V2
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        $validator
            ->scalar('comment')
            ->maxLength('comment', 1500)
            ->allowEmptyString('comment');

        $validator
            ->scalar('counter_def')
            ->maxLength('counter_def', 255)
            ->requirePresence('counter_def', 'create')
            ->allowEmptyString('counter_def', null, false)
            ->add('counter_def', 'acte_counter_def', [
                'rule' => function ($value) {
                    $pattern = "/^(?!_)(\#(A{2,4}|M{1,2}|J{1,2}|s|S{1,10}|p|0{1,10})\#|[A-Z0-9_])+(?<!_){2,15}$/";

                    return !empty(preg_match($pattern, $value)) && empty(preg_match('/^#S{2,}/', $value));
                },
                'message' => 'La définition du compteur ne respecte pas le protocole @cte.',
            ]);

        $validator
            ->scalar('reinit_def')
            ->maxLength('reinit_def', 255)
            ->requirePresence('reinit_def', 'create')
            ->allowEmptyString('reinit_def', null, false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     * @created 17/12/2020
     * @version V2
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['sequence_id'], 'Sequences'));
        $rules->add($rules->isUnique(['structure_id', 'name']));

        $rules->addUpdate(function ($entity) {
            return $entity->availableActions['can_be_edited']['value'];
        }, 'counterCantBeEdited', [
            'errorField' => 'id',
            'message' => 'Cannot edit a counter without the admin role',
        ]);

        $rules->addDelete(function ($entity) {
            return $entity->availableActions['can_be_deleted']['value'];
        }, 'counterCantBeDeleted', [
            'errorField' => 'id',
            'message' => 'Cannot delete a counter in use',
        ]);

        return $rules;
    }

    /**
     * Finds counter with it's id
     *
     * @param int $id counter
     * @return \App\Model\Entity\Counter
     * @throws \Cake\Http\Exception\NotFoundException
     * @version V2
     */
    public function findById(int $id)
    {
        $counter = $this->get($id);

        if (empty($counter)) {
            throw new NotFoundException(__('ID:"%s" de compteur invalide', $id));
        }

        return $counter;
    }

    /**
     * Returns next counter value,
     * save new sequence value and reinit_def in database
     *
     * @param int $id counter
     * @param \Cake\I18n\FrozenDate $date Choosen date, today's date if not specified
     * @return string next counter value
     * @version V2
     */
    public function generatesCounter($id = null, $date = null)
    {
        $counter = $this->findById($id);
        $sequence = $this->fetchTable('Sequences');
        $date = $date === null ? FrozenDate::now() : new FrozenDate($date);
        $sequenceNum = $sequence->getNextValue($date, $counter->reinit_def, $counter->sequence_id);

        return $this->writeCounter($counter->counter_def, $date, $sequenceNum);
    }

    /**
     * Writes an act number from a template, the date and sequence num
     *
     * @param string $counterDef counter def
     * @param \Cake\I18n\FrozenDate $date date
     * @param int $sequence_num sequence number
     * @return string
     * @version V2
     */
    public function writeCounter(string $counterDef, FrozenDate $date, int $sequence_num)
    {
        // Search array init and replacement for the sequence
        $strnseqS = sprintf("%'_10d", $sequence_num);
        $strnseqZ = sprintf('%010d', $sequence_num);

        // Counter definition replacement with date
        $setDate = ['#AAAA#' => $date->format('Y'),
            '#AA#' => $date->format('y'),
            '#M#' => $date->format('n'),
            '#MM#' => $date->format('m'),
            '#J#' => $date->format('j'),
            '#JJ#' => $date->format('d'),
            '#s#' => $sequence_num,
            '#S#' => substr($strnseqS, -1, 1),
            '#SS#' => substr($strnseqS, -2, 2),
            '#SSS#' => substr($strnseqS, -3, 3),
            '#SSSS#' => substr($strnseqS, -4, 4),
            '#SSSSS#' => substr($strnseqS, -5, 5),
            '#SSSSSS#' => substr($strnseqS, -6, 6),
            '#SSSSSSS#' => substr($strnseqS, -7, 7),
            '#SSSSSSSS#' => substr($strnseqS, -8, 8),
            '#SSSSSSSSS#' => substr($strnseqS, -9, 9),
            '#SSSSSSSSSS#' => $strnseqS,
            '#0#' => substr($strnseqZ, -1, 1),
            '#00#' => substr($strnseqZ, -2, 2),
            '#000#' => substr($strnseqZ, -3, 3),
            '#0000#' => substr($strnseqZ, -4, 4),
            '#00000#' => substr($strnseqZ, -5, 5),
            '#000000#' => substr($strnseqZ, -6, 6),
            '#0000000#' => substr($strnseqZ, -7, 7),
            '#00000000#' => substr($strnseqZ, -8, 8),
            '#000000000#' => substr($strnseqZ, -9, 9),
            '#0000000000#' => $strnseqZ,
        ];
        // Generates counter value
        return str_replace(array_keys($setDate), array_values($setDate), $counterDef);
    }
}
