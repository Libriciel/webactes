<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Authorizeactions Model
 *
 * @property \App\Model\Table\StateactsTable|\App\Model\Table\BelongsToMany $Stateacts
 * @method \App\Model\Table\Authorizeaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\Authorizeaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Authorizeaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Authorizeaction|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Authorizeaction saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Authorizeaction patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Authorizeaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Authorizeaction findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AuthorizeactionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('authorizeactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany(
            'Stateacts',
            [
                'foreignKey' => 'authorizeaction_id',
                'targetForeignKey' => 'stateact_id',
                'joinTable' => 'authorizeactions_stateacts',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('action')
            ->maxLength('action', 9)
            ->requirePresence('action', 'create')
            ->allowEmptyString('action', false)
            ->add('action', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['action']));

        return $rules;
    }
}
