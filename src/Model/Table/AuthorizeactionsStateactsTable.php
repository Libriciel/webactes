<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AuthorizeactionsStateacts Model
 *
 * @property \App\Model\Table\AuthorizeactionsTable|\App\Model\Table\BelongsTo $Authorizeactions
 * @property \App\Model\Table\StateactsTable|\App\Model\Table\BelongsTo $Stateacts
 * @method \App\Model\Table\AuthorizeactionsStateact get($primaryKey, $options = [])
 * @method \App\Model\Entity\AuthorizeactionsStateact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AuthorizeactionsStateact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AuthorizeactionsStateact|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AuthorizeactionsStateact saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AuthorizeactionsStateact patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AuthorizeactionsStateact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AuthorizeactionsStateact findOrCreate($search, callable $callback = null, $options = [])
 */
class AuthorizeactionsStateactsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('authorizeactions_stateacts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo(
            'Authorizeactions',
            [
            'foreignKey' => 'authorizeaction_id',
            'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Stateacts',
            [
            'foreignKey' => 'stateact_id',
            'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['authorizeaction_id'], 'Authorizeactions'));
        $rules->add($rules->existsIn(['stateact_id'], 'Stateacts'));

        return $rules;
    }
}
