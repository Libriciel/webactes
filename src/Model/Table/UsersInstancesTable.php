<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UsersInstances Model
 *
 * @property \App\Model\Table\UsersTable|\App\Model\Table\BelongsTo $Users
 * @method \App\Model\Entity\UsersInstances get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsersInstances newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsersInstances[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsersInstances|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersInstances|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersInstances patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsersInstances[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsersInstances findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersInstancesTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users_instances');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo(
            'Projects',
            [
                'foreignKey' => 'id_flowable_instance',
                'bindingKey' => 'id_flowable_instance',
                'joinType' => 'INNER',
                'dependent' => true,
            ]
        );

        $this->belongsTo(
            'Users',
            [
                'foreignKey' => 'user_id',
                'joinType' => 'INNER',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('user_id')
            ->requirePresence('user_id');

        $validator
            ->scalar('id_flowable_instance')
            ->requirePresence('id_flowable_instance');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        //        $rules->add($rules->existsIn(['id_flowable_instance'], 'Projects'));
        $rules->add($rules->isUnique(['user_id', 'id_flowable_instance']));

        return $rules;
    }
}
