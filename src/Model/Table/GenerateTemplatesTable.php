<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GenerateTemplates Model
 *
 * @property \App\Model\Table\GenerateTemplateTypesTable&\Cake\ORM\Association\BelongsTo $GenerateTemplateTypes
 * @property \App\Model\Table\FilesTable&\Cake\ORM\Association\HasMany $Files
 * @method \App\Model\Entity\GenerateTemplate newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\GenerateTemplate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GenerateTemplate get($primaryKey, $options = [])
 * @method \App\Model\Entity\GenerateTemplate findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\GenerateTemplate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GenerateTemplate[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\GenerateTemplate|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GenerateTemplate saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GenerateTemplate[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GenerateTemplate[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\GenerateTemplate[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GenerateTemplate[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class GenerateTemplatesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('generate_templates');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('LimitToStructure');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Permission');
        $this->belongsTo('Structures', [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('GenerateTemplateTypes', [
            'foreignKey' => 'generate_template_type_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Files', [
            'foreignKey' => 'generate_template_id',
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['generate_template_type_id'], 'GenerateTemplateTypes'), [
            'errorField' => 'generate_template_type_id']);

        return $rules;
    }
}
