<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Acts Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\TemplatesTable|\App\Model\Table\BelongsTo $Templates
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\HasMany $Containers
 * @property \App\Model\Table\ActorsTable|\App\Model\Table\BelongsToMany $Actors
 * @method \App\Model\Table\Act get($primaryKey, $options = [])
 * @method \App\Model\Entity\Act newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Act[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Act|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Act|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Act patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Act[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Act findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ActsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('acts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Templates',
            [
                'foreignKey' => 'template_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Containers',
            [
                'foreignKey' => 'act_id',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Actors',
            [
                'foreignKey' => 'act_id',
                'targetForeignKey' => 'actor_id',
                'joinTable' => 'actors_acts',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        $validator
            ->scalar('code_act')
            ->maxLength('code_act', 15)
            ->requirePresence('code_act', 'create')
            ->allowEmptyString('code_act', false)
            ->add('code_act', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->dateTime('date_send')
            ->allowEmptyDateTime('date_send');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['structure_id', 'name']));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['template_id'], 'Templates'));

        return $rules;
    }
}
