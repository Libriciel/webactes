<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UsersServices Model
 *
 * @property \App\Model\Table\UsersTable|\App\Model\Table\BelongsTo $Users
 * @property \App\Model\Table\ServicesTable|\App\Model\Table\BelongsTo $Services
 * @method \App\Model\Table\UsersService get($primaryKey, $options = [])
 * @method \App\Model\Entity\UsersService newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UsersService[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UsersService|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersService|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UsersService patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UsersService[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UsersService findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersServicesTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users_services');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Users',
            [
                'foreignKey' => 'user_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Services',
            [
                'foreignKey' => 'service_id',
                'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['user_id', 'service_id']));
        $rules->add($rules->existsIn(['service_id'], 'Services'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
