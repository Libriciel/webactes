<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Matieres Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property |\Cake\ORM\Association\BelongsTo $ParentMatieres
 * @property |\Cake\ORM\Association\HasMany $ChildMatieres
 * @method \App\Model\Table\Matiere get($primaryKey, $options = [])
 * @method \App\Model\Entity\Matiere newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Matiere[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Matiere|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Matiere saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Matiere patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Matiere[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Matiere findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class MatieresTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('matieres');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'ParentMatieres',
            [
                'className' => 'Matieres',
                'foreignKey' => 'parent_id',
            ]
        );
        $this->hasMany(
            'ChildMatieres',
            [
                'className' => 'Matieres',
                'foreignKey' => 'parent_id',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('codematiere')
            ->maxLength('codematiere', 5)
            ->requirePresence('codematiere', 'create')
            ->allowEmptyString('codematiere', null, false);

        $validator
            ->scalar('libelle')
            ->maxLength('libelle', 100)
            ->requirePresence('libelle', 'create')
            ->allowEmptyString('libelle', null, false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['parent_id'], 'ParentMatieres'));
        $rules->add($rules->isUnique(['structure_id', 'codematiere', 'libelle']));

        return $rules;
    }

    /**
     * return codeMatiere from matiereId
     *
     * @param  int $id matiereId
     * @return string
     */
    public function getMatiereCodeFromId($id)
    {
        $nature = $this->get($id);

        return $nature->codematiere;
    }
}
