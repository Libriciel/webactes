<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GenerateTemplateTypes Model
 *
 * @property \App\Model\Table\GenerateTemplatesTable&\Cake\ORM\Association\HasMany $GenerateTemplates
 * @method \App\Model\Entity\GenerateTemplateType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\GenerateTemplateType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GenerateTemplateType get($primaryKey, $options = [])
 * @method \App\Model\Entity\GenerateTemplateType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\GenerateTemplateType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GenerateTemplateType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\GenerateTemplateType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GenerateTemplateType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GenerateTemplateType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GenerateTemplateType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\GenerateTemplateType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GenerateTemplateType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class GenerateTemplateTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('generate_template_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('GenerateTemplates', [
            'foreignKey' => 'generate_template_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        return $validator;
    }
}
