<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Stateacts Model
 *
 * @property |\Cake\ORM\Association\BelongsToMany $Authorizeactions
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\BelongsToMany $Containers
 * @property \App\Model\Table\ContainersStateactsTable|\App\Model\Table\HasMany $ContainersStateacts
 * @method \App\Model\Table\Stateact get($primaryKey, $options = [])
 * @method \App\Model\Entity\Stateact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Stateact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Stateact|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Stateact saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Stateact patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Stateact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Stateact findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StateactsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('stateacts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany(
            'Authorizeactions',
            [
            'foreignKey' => 'stateact_id',
            'targetForeignKey' => 'authorizeaction_id',
            'joinTable' => 'authorizeactions_stateacts',
            'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Containers',
            [
            'foreignKey' => 'stateact_id',
            'targetForeignKey' => 'container_id',
            'joinTable' => 'containers_stateacts',
            'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false)
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('code')
            ->maxLength('code', 100)
            ->requirePresence('code', 'create')
            ->allowEmptyString('code', false)
            ->add('code', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->isUnique(['code']));
        $rules->add($rules->isUnique(['name', 'code']));

        return $rules;
    }
}
