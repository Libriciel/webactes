<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use InvalidArgumentException;

/**
 * Annexes Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\BelongsTo $Containers
 * @property \App\Model\Table\FilesTable|\App\Model\Table\HasMany $Files
 * @method \App\Model\Table\Annex get($primaryKey, $options = [])
 * @method \App\Model\Entity\Annex newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Annex[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Annex|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Annex|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Annex patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Annex[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Annex findOrCreate($search, callable $callback = null, $options = [])
 */
class AnnexesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('annexes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('LimitToStructure');
        $this->addBehavior('NatureIdViaContainersTypesacts');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Containers',
            [
                'foreignKey' => 'container_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasOne(
            'Files',
            [
                'foreignKey' => 'annex_id',
                'dependent' => true,
            ]
        );

        $this->belongsTo(
            'Typespiecesjointes',
            [
                'foreignKey' => false,
                'conditions' => [
                    "{$this->getAlias()}.structure_id = Typespiecesjointes.structure_id",
                    "{$this->getAlias()}.codetype = Typespiecesjointes.codetype",
                    sprintf('Typespiecesjointes.nature_id IN (%s)', $this->getNatureIdSubquery()),
                ],
                'joinType' => 'LEFT OUTER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('rank')
            ->requirePresence('rank', 'create')
            ->allowEmptyString('rank', null, false);

        $validator
            ->boolean('is_generate')
            ->requirePresence('is_generate', 'create')
            ->allowEmptyString('is_generate', null, false);

        $validator
            ->boolean('istransmissible')
            ->requirePresence('istransmissible', 'create')
            ->allowEmptyString('istransmissible', null, false);

        $validator
            ->scalar('codetype')
            ->maxLength('codetype', 5)
            ->allowEmptyString('codetype');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['container_id'], 'Containers'));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        return $rules;
    }

    /**
     * remove annexes and delete files !
     *
     * @param array $annexeIds array of annexe ids
     * @return bool
     */
    public function unActive(array $annexeIds)
    {
        if (empty($annexeIds)) {
            return true;
        }

        return $this->updateAll(
            ['current' => 0],
            ['id in ' => $annexeIds]
        );
    }

    /**
     * @param array $infoAnnexes infoAnnexes from client
     * @return void
     */
    public function updateTransmissibleStatus(array $infoAnnexes)
    {
        // to prevent from modify annexes from others
        // check if sended annexeIds are Authorized
        //$annexes = $this->find()->where(['container_id' => $containerId])->select(['id'])->toArray();
        //$annexeServerIds = Hash::extract($annexes, '{n}.id');
        $areTransmissible = [];
        $areNotTransmissible = [];
        foreach ($infoAnnexes as $infoAnnex) {
            if (!isset($infoAnnex['id'])) {
                continue;
            }
            if (isset($infoAnnex['istransmissible']) && $infoAnnex['istransmissible']) {
                $areTransmissible[] = $infoAnnex['id'];
            } else {
                $areNotTransmissible[] = $infoAnnex['id'];
            }
        }

        if (!empty($areTransmissible)) {
            $this->updateAll(['istransmissible' => 1], ['id in' => $areTransmissible, 'current' => true]);
        }
        if (!empty($areNotTransmissible)) {
            $this->updateAll(
                [
                    'istransmissible' => 0,
                    'codetype' => null],
                ['id in' => $areNotTransmissible,
                    'current' => true,
                ]
            );
        }
    }

    /**
     * @param array $infoAnnexes Foo
     * @return void
     */
    public function updateGenerate(array $infoAnnexes): void
    {
        $areGenerate = [];
        $areNotGenerate = [];
        foreach ($infoAnnexes as $infoAnnex) {
            if (!isset($infoAnnex['id'])) {
                continue;
            }
            if (isset($infoAnnex['is_generate']) && $infoAnnex['is_generate']) {
                $areGenerate[] = $infoAnnex['id'];
            } else {
                $areNotGenerate[] = $infoAnnex['id'];
            }
        }

        if (!empty($areGenerate)) {
            $this->updateAll(['is_generate' => 1], ['id in' => $areGenerate, 'current' => true]);
            $this->getEventManager()->dispatch(new Event('Model.Annexe.afterSave.convertFile', $this, [
                'ids' => $areGenerate,
            ]));
        }
        if (!empty($areNotGenerate)) {
            $this->updateAll(['is_generate' => 0], ['id in' => $areNotGenerate, 'current' => true]);
            $this->getEventManager()->dispatch(new Event('Model.Annexe.afterSave.deleteFile', $this, [
                'ids' => $areNotGenerate,
            ]));
        }
    }

    /**
     * @param array $infoAnnexes infoAnnexes from client
     * @return void
     */
    public function updateCodetype(array $infoAnnexes)
    {
        foreach ($infoAnnexes as $infoAnnex) {
            if (!isset($infoAnnex['id'])) {
                continue;
            }
            if (!isset($infoAnnex['codetype'])) {
                $infoAnnex['codetype'] = null;
            }
            $this->updateAll(['codetype' => $infoAnnex['codetype']], ['id' => $infoAnnex['id']]);
        }
    }

    /**
     * Save annexe
     *
     * @param array $uploadedFile uploadedFile
     * @param int $containerId containerId
     * @param int $structureId structureId
     * @param int $rank Annex rank
     * @param bool $isGenerate isGenerate
     * @param bool $isTransmissible isTransmissible
     * @return bool
     */
    public function saveAnnexe(
        $uploadedFile,
        $containerId,
        $structureId,
        $rank,
        $isGenerate = false,
        $isTransmissible = false,
        $codeType = null
    ) {
        $data = [
            'structure_id' => $structureId,
            'container_id' => $containerId,
            'rank' => $rank,
            'is_generate' => $isGenerate ? 1 : 0,
            'istransmissible' => $isTransmissible ? 1 : 0,
            'codetype' => $codeType,
        ];

        $annex = $this->newEntity($data);
        $res = $this->save($annex, ['atomic' => false]);
        if (!$res) {
            throw new InvalidArgumentException(var_export($annex->getErrors(), true));
        }

        $annexId = $annex->id;
        $uploadedFileData = [
            'name' => $uploadedFile->getClientFilename(),
            'tmp_name' => $uploadedFile->getStream()->getMetadata('uri'),
            'path' => $uploadedFile->getStream()->getMetadata('uri'),
            'type' => $uploadedFile->getClientMediaType(),
            'size' => $uploadedFile->getSize(),
        ];
        $res = $this->Files->saveInFs($uploadedFileData, ['annex_id' => $annexId], $containerId, $structureId);
        if (!$res) {
            return false;
        }

        if ($isGenerate) {
            $this->getEventManager()->dispatch(new Event('Model.Annexe.afterSave.convertFile', $this, [
                'ids' => [$annexId],
            ]));
        }

        return true;
    }

    /**
     * remove codeType
     *
     * @param array $containerIds list of containers ids
     * @return int
     */
    public function removeCodetype($containerIds)
    {
        return $this->updateAll(['codetype' => null], ['container_id IN' => $containerIds]);
    }
}
