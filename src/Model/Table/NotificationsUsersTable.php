<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NotificationsUsers Model
 *
 * @property \App\Model\Table\UsersTable|\App\Model\Table\BelongsTo $Users
 * @property \App\Model\Table\NotificationsTable|\App\Model\Table\BelongsTo $Notifications
 * @method \App\Model\Table\NotificationsUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\NotificationsUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NotificationsUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NotificationsUser|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NotificationsUser saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NotificationsUser patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NotificationsUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NotificationsUser findOrCreate($search, callable $callback = null, $options = [])
 */
class NotificationsUsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('notifications_users');

        $this->belongsTo(
            'Users',
            [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Notifications',
            [
            'foreignKey' => 'notification_id',
            'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        return $validator;
    }

    /**
     * Add clause to find personal notifications
     *
     * @param \Cake\ORM\Query $query query
     * @param  array $options list of options
     * @return \Cake\ORM\Query
     */
    public function findPersonal(Query $query, array $options): Query
    {
        return $this
            ->query()
            ->select([
                'id' => 'NotificationsUsers.id',
                'name' => 'Notifications.name',
                'active' => 'NotificationsUsers.active' ])
            ->contain(['Notifications'])
            ->where(
                [
                'Notifications.is_global' => false,
                ]
            );
    }

    /**
     * Add clause to find global notifications
     *
     * @param \Cake\ORM\Query $query query
     * @param  array $options list of options
     * @return \Cake\ORM\Query
     */
    public function findGlobal(Query $query, array $options = []): Query
    {
        return $this
            ->query()
            ->select([
                'id' => 'NotificationsUsers.id',
                'name' => 'Notifications.name',
                'active' => 'NotificationsUsers.active' ])
            ->contain(['Notifications'])
            ->where(
                [
                'Notifications.is_global' => true,
                ]
            );
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['notification_id'], 'Notifications'));

        return $rules;
    }
}
