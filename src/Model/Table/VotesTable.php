<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Votes Model
 *
 * @property \App\Model\Table\ActorsTable|\Cake\ORM\Association\BelongsTo $Actors
 * @property \App\Model\Table\ProjectsTable|\Cake\ORM\Association\BelongsTo $Projects
 * @property \App\Model\Table\ActorsVotesTable|\Cake\ORM\Association\BelongsTo $ActorsVotes
 * @method \App\Model\Entity\Vote get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vote newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vote[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vote|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vote saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vote patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vote[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vote findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class VotesTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Correspondances for the fusion of the field method, so we get the same value as webdelib.
     *
     * @var string[]
     */
    public $correspondances = [
        'details' => 'DETAIL',
        'result' => 'RESULTAT',
        'total' => 'TOTAL',
        'take_act' => 'PRENDRE_ACTE',
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('votes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        // ***** BEHAVIORS *****
        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo('Actors', [
            'foreignKey' => 'president_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Structures', [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany(
            'ActorsVotes',
            [
                'foreignKey' => 'vote_id',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

//        $validator
//            ->integer('structure_id')
//            ->requirePresence('structure_id', 'create');
//
//        $validator
//            ->integer('user_id')
//            ->requirePresence('user_id', 'create');

        $validator
            ->integer('yes')
            ->allowEmptyString('yes');

        $validator
            ->integer('no')
            ->allowEmptyString('no');

        $validator
            ->integer('yes_counter')
            ->allowEmptyString('yes_counter');

        $validator
            ->integer('no_counter')
            ->allowEmptyString('no_counter');

        $validator
            ->boolean('take_act')
            ->allowEmptyString('take_act');

        $validator
            ->boolean('result')
            ->allowEmptyString('result');

        $validator
            ->scalar('comment')
            ->allowEmptyString('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['president_id'], 'Actors'));
        $rules->add($rules->existsIn(['project_id'], 'Projects'));

        return $rules;
    }

    /**
     * @param array $vote tableau de votes
     * @param array $options all options usefull for save vote
     * @return \App\Model\Entity\Vote|array
     */
    public function vote(array $vote, array $options)
    {
        $votesResult = $this->createVote($vote, $options);

        if (isset($votesResult['state']['state']['value']) && $votesResult['state']['state']['value'] !== false) {
            // Modification de l'état du projet
            $this->updateContainer($votesResult, $options);
            /** @var \App\Model\Table\ProjectsTable $Projects */
            $Projects = $this->fetchTable('Projects');
            // generate num acte
            $Projects->generateActNumber((int)$votesResult['id']);
            // generate documents acte
            $Projects->generateProjectFiles((int)$votesResult['id']);

            return $votesResult;
        }

        return $votesResult;
    }

    /**
     * @param array $data the entity vote to save
     * @param array $options array of options
     * @return array
     */
    private function createVote(array $data, array $options): array
    {
        switch ($data['method']) {
            case 'details':
                $stateMessage = $this->prepareVoteDetails($data, $options);
                break;
            default:
                $stateMessage = $this->prepareVote($data, $options);
                break;
        }

        return [
            'state' => $stateMessage,
            'id' => $data['project_id'],
        ];
    }

    /**
     * Prepare vote for a project only if it can be voted
     *
     * @param array $data Raw data
     * @param array $options array of options
     * @return array | false
     */
    private function prepareVote(array $data, array $options)
    {
        if (isset($data['id'])) {
            $vote = $this->get($data['id']);
            $this->patchEntity($vote, $data);
        } else {
            $vote = $this->newEmptyEntity();
        }

        $vote->president_id = $data['president_id'] ?? null;
        $vote->structure_id = $options['structureId'];
        $vote->project_id = $data['project_id'];
        $vote->comment = $data['comment'] ?? null;
        $vote->method = $data['method'];

        $state = [];
        // Can be Voted
        if ($this->canVoted($data['project_id'])) {
            switch ($data['method']) {
                case 'total': // Vote par total des voix
                    $vote->yes_counter = $data['yes_counter'];
                    $vote->no_counter = $data['no_counter'];
                    $vote->abstentions_counter = $data['abstentions_counter'];
                    $vote->no_words_counter = $data['no_words_counter'];

                    $state['historyState'] = $vote->yes_counter > $vote->no_counter
                        ? HistoriesTable::VOTED_APPROVED
                        : HistoriesTable::VOTED_REJECTED;
                    $state['state']['data'] = $vote->yes_counter > $vote->no_counter
                        ? VOTED_APPROVED
                        : VOTED_REJECTED;
                    $state['state']['value'] = $vote->yes_counter > $vote->no_counter
                        ? VOTED_APPROVED
                        : VOTED_REJECTED;
                    break;

                case 'result': // Vote par résultat
                    $vote->result = $data['result'];
                    $state['historyState'] = $vote->result
                        ? HistoriesTable::VOTED_APPROVED
                        : HistoriesTable::VOTED_REJECTED;
                    $state['state']['data'] = $vote->result
                        ? VOTED_APPROVED
                        : VOTED_REJECTED;
                    $state['state']['value'] = true;
                    break;

                case 'take_act': // Prendre Acte
                    $vote->take_act = $data['take_act'];
                    $state['historyState'] = HistoriesTable::TAKE_NOTE;
                    $state['state']['data'] = TAKENOTE;
                    $state['state']['value'] = true;
                    break;

                default:
                    return false;
            }
        }

        if ($this->save($vote)) {
            return $state;
        }

        return false;
    }

    /**
     * Prepare vote for a project only if it can be voted
     *
     * @param array $data Raw data
     * @param array $options array of options
     * @return array | false
     */
    private function prepareVoteDetails(array $data, array $options)
    {
        $vote = $this->newEntity($data, ['associated' => ['ActorsVotes']]);
        $vote->president_id = $data['president_id'] ?? null;
        $vote->structure_id = $options['structureId'];
        $vote->project_id = $data['project_id'];
        $vote->comment = $data['comment'] ?? null;

        $state = [];

        if ($this->save($vote, ['associated' => ['ActorsVotes']])) {
            // Can be Voted
            if ($this->canVoted($data['project_id'])) {
                $state['state']['data'] = $this->ActorsVotes->calculateResult((int)$vote->id);

                $state['historyState'] = $state['state']['data'] === VOTED_APPROVED
                    ? HistoriesTable::VOTED_APPROVED
                    : HistoriesTable::VOTED_REJECTED;

                $state['state']['value'] = true;
            }

            return $state;
        }

        return false;
    }

    /**
     * @param int $project_id Foo
     * @return mixed
     */
    private function canVoted($project_id)
    {
        $project = $this->fetchTable('Projects')
            ->get(
                $project_id,
                [
                    'contain' => [
                        'Containers',
                        'Containers.Stateacts',
                    ],
                ]
            );

        return $project->get('availableActions')['can_be_voted']['value'];
    }

    /**
     * @param array $vote datas
     * @param array $options options
     * @return void
     */
    private function updateContainer(array $vote, array $options)
    {
        /** @var \App\Model\Table\ContainersTable $Container */
        $Container = $this->fetchTable('Containers');

        $containerId = $Container->find()->where(['project_id' => $vote['id']])->first()->get('id');

        $Container->addStateact($containerId, $vote['state']['state']['data']);

        $history = $Container->Histories->newEntity([
            'structure_id' => $options['structureId'],
            'container_id' => $containerId,
            'user_id' => $options['userId'],
            'comment' => $vote['state']['historyState'],
        ]);

        $Container->Histories->save($history);
    }

    /**
     * @param array $vote The id Of the project
     * @return array State result
     */
    private function calculateResultFromDetails(Entity $vote)
    {
        $totalOfOpinionFor = $this->ActorsVotes->find()
            ->where(['vote_id' => $vote->get('id'), 'result' => 'yes'])
            ->count();

        $totalOfOpinionAgainst = $this->ActorsVotes->find()
            ->where(['vote_id' => $vote->get('id'), 'result' => 'no'])
            ->count();

        $totalOfAbstention = $this->ActorsVotes->find()
            ->where(['vote_id' => $vote->get('id'), 'result' => 'abstention'])
            ->count();

        $totalOfNoWord = $this->ActorsVotes->find()
            ->where(['vote_id' => $vote->get('id'), 'result' => 'no_words'])
            ->count();

        return [
            'approved' => $totalOfOpinionFor > $totalOfOpinionAgainst,
            'yes_counter' => $totalOfOpinionFor,
            'abstentions_counter' => $totalOfAbstention,
            'no_counter' => $totalOfOpinionAgainst,
            'no_words_counter' => $totalOfNoWord,
            'vote_counter' => $totalOfOpinionFor + $totalOfOpinionAgainst + $totalOfAbstention,
        ];
    }

    /**
     * calculResult
     *
     * @param int $projetId Identifiant project
     * @return array
     */
    public function result(int $projetId): array
    {
        // @info: il y a potentiellement plusieurs entrées dans la table votes car on enregistre l'historique
        $vote = $this->find()->where(['project_id' => $projetId])->orderDesc('id')->first();
        if (empty($vote)) {
            return [];
        }
        $result = [
            'method' => $vote->get('method'),
            'comment' => $vote->get('comment'),
        ];

        if ($vote->get('method') === 'details') {
            $result = array_merge($result, $this->calculateResultFromDetails($vote));
        } else {
            $result = array_merge($result, $this->calculateResult($vote));
        }

        return $result;
    }

    /**
     * @param \Cake\ORM\Entity $vote Foo
     * @return array
     */
    private function calculateResult(Entity $vote): array
    {
        switch ($vote->get('method')) {
            case 'total':
                return [
                    'approved' => $vote->get('yes_counter') > $vote->get('no_counter'),
                    'yes_counter' => $vote->get('yes_counter'),
                    'abstentions_counter' => $vote->get('abstentions_counter'),
                    'no_counter' => $vote->get('no_counter'),
                    'no_words_counter' => $vote->get('no_words_counter'),
                    'vote_counter' => $vote->get('yes_counter')
                        + $vote->get('no_counter')
                        + $vote->get('abstentions_counter'),
                ];
            case 'result':
                return [
                    'approved' => $vote->get('result'),
                ];
            case 'take_act':
                return [
                    'take_act' => $vote->get('take_act'),
                ];
            default:
                return [];
        }
    }
}
