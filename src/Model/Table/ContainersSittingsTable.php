<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContainersSittings Model
 *
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\BelongsTo $Containers
 * @property \App\Model\Table\SittingsTable|\App\Model\Table\BelongsTo $Sittings
 * @method \App\Model\Table\ContainersSitting get($primaryKey, $options = [])
 * @method \App\Model\Entity\ContainersSitting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ContainersSitting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ContainersSitting|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContainersSitting|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContainersSitting patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ContainersSitting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ContainersSitting findOrCreate($search, callable $callback = null, $options = [])
 */
class ContainersSittingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('containers_sittings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo(
            'Containers',
            [
                'foreignKey' => 'container_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Sittings',
            [
                'foreignKey' => 'sitting_id',
                'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['container_id'], 'Containers'));
        $rules->add($rules->existsIn(['sitting_id'], 'Sittings'));

        return $rules;
    }

    /**
     * Retourne une (sous) query contenant le champ id de la table containers_sittings pour une séance donnée.
     *
     * @param string $alias L'alias de la clé primaire de la table Sittings de la requête principale
     * @return \Cake\ORM\Query
     */
    public function getBySittingsIdSubquery(string $alias = 'Sittings.id')
    {
        return $this
            ->find()
            ->select(['containers_sittings.id'])
            ->from(['containers_sittings'])
            ->where(function (QueryExpression $exp, Query $q) use ($alias) {
                return $exp->equalFields('containers_sittings.sitting_id', $alias);
            });
    }

    /**
     * Retourne une sous-requête contenant le rang d'un projet donné pour une séance donnée.
     *
     * @param int $sittingId L'id de la séance (sittings.id)
     * @param int $projectId L'id du projet (projects.id)
     * @return \Cake\ORM\Query
     */
    public function getContainersSittingsRankSubquery(int $sittingId, int $projectId)
    {
        return $this
            ->find()
            ->select(['containers_sittings.rank'])
            ->from(['containers_sittings'])
            ->join([
                'containers' => [
                    'table' => 'containers',
                    'type' => 'INNER',
                    'conditions' => [
                        'containers_sittings.container_id = containers.id',
                    ],
                ],
            ])
            ->where([
                'containers.project_id' => $projectId,
                'containers_sittings.sitting_id' => $sittingId,
            ]);
    }

    /**
     * Calculates projects rank if one project is removed from sitting
     *
     * @param int $sittingId sittingId
     * @return array
     */
    public function calculatesNewRank(int $sittingId)
    {
        $containerSitting = $this
            ->find()
            ->where(['sitting_id' => $sittingId])
            ->order('rank')
            ->enableHydration(false)
            ->toArray();

        array_unshift($containerSitting, '');
        unset($containerSitting[0]);

        foreach (array_keys($containerSitting) as $key) {
            $containerSitting[$key]['rank'] = $key;
        }

        return $containerSitting;
    }
}
