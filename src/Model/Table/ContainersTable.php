<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Utilities\Api\Pastell\Pastell;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Http\Client;
use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Locator\TableLocator;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Containers Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\ActsTable|\App\Model\Table\BelongsTo $Acts
 * @property \App\Model\Table\ContainersSittingsTable|\App\Model\Table\HasMany $ContainersSittings
 * @property \App\Model\Table\ContainersStateactsTable|\App\Model\Table\HasMany $ContainersStateacts
 * @property \App\Model\Table\ProjectsTable|\App\Model\Table\BelongsTo $Projects
 * @property \App\Model\Table\HistoriesTable|\App\Model\Table\HasMany $Histories
 * @property \App\Model\Table\SittingsTable|\App\Model\Table\BelongsToMany $Sittings
 * @property \App\Model\Table\StateactsTable|\App\Model\Table\BelongsToMany $Stateacts
 * @method \App\Model\Table\Container get($primaryKey, $options = [])
 * @method \App\Model\Entity\Container newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Container[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Container|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Container|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Container patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Container[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Container findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContainersTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('containers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Acts',
            [
                'foreignKey' => 'act_id',
            ]
        );
        $this->belongsTo(
            'Projects',
            [
                'foreignKey' => 'project_id',
            ]
        );
        $this->belongsTo(
            'Typesacts',
            [
                'foreignKey' => 'typesact_id',
            ]
        );
        $this->hasMany(
            'Histories',
            [
                'foreignKey' => 'container_id',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Sittings',
            [
                'foreignKey' => 'container_id',
                'targetForeignKey' => 'sitting_id',
                'joinTable' => 'containers_sittings',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'ContainersSittings',
            [
                'foreignKey' => 'container_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'ContainersStateacts',
            [
                'foreignKey' => 'container_id',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Stateacts',
            [
                'foreignKey' => 'container_id',
                'targetForeignKey' => 'stateact_id',
                'joinTable' => 'containers_stateacts',
                'dependent' => true,
                'through' => 'ContainersStateacts',
            ]
        );
        $this->hasOne(
            'Maindocuments',
            [
                'foreignKey' => 'container_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Annexes',
            [
                'foreignKey' => 'container_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Pastellfluxs',
            [
                'foreignKey' => 'container_id',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     * @access  public
     * @created 25/06/2019
     * @version V0.0.9
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('structure_id')
            ->requirePresence('structure_id', 'create');

        $validator
            ->integer('typesact_id')
            ->requirePresence('typesact_id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['act_id'], 'Acts'));
        $rules->add($rules->existsIn(['project_id'], 'Projects'));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['typesact_id'], 'Typesacts'));

        return $rules;
    }

    /**
     * return pastellClient
     *
     * @param int $structureId structureId
     * @return \App\Utilities\Api\Pastell\Pastell
     */
    private function getPastellClient($structureId)
    {
        /**
         * @var \App\Model\Table\ConnecteursTable $Connecteurs
         */
        $Connecteurs = $this->fetchTable('Connecteurs');
        $connecteur = $Connecteurs->find()->where([
            'connector_type_id' => 1,
            'structure_id' => $structureId,
        ])->firstOrFail();

        $client = new Client(
            [
                'auth' => [
                    'username' => $connecteur->get('username'),
                    'password' => $connecteur->get('password'),
                ],
                'ssl_verify_peer' => false,
                'redirect' => 2,
            ]
        );

        return new Pastell($client, $connecteur->get('url'));
    }

    /**
     * return if documents are already signed
     *
     * @param int $containerId containerID
     * @return bool
     */
    public function isSigned(int $containerId): bool
    {
        return $this->fetchTable('ContainersStateacts')->find()
                ->where([
                    'container_id' => $containerId,
                    'stateact_id IN' => [DECLARE_SIGNED, PARAPHEUR_SIGNED],
                ])
                ->count() > 0;
    }

    /**
     * return if documents are already electronically signed (parapheur)
     *
     * @param int $containerId containerID
     * @return bool
     */
    public function isSignedParapheur(int $containerId): bool
    {
        return $this->fetchTable('ContainersStateacts')->find()
                ->where([
                    'container_id' => $containerId,
                    'stateact_id IN' => [PARAPHEUR_SIGNED],
                ])
                ->count() > 0;
    }

    /**
     * return if project can be deleted or not
     *
     * @param int $containerId containerID
     * @return bool
     */
    public function isLocked(int $containerId): bool
    {
        return $this->isSigned($containerId)
            || $this->getLastStateActEntry($containerId)[0]->id === PARAPHEUR_SIGNING;
    }

    /**
     * return if a container is deletable
     *
     * @param int $containerId contanerId
     * @return bool
     */
    public function isDeletable(int $containerId): bool
    {
        if ($this->isLocked($containerId)) {
            return false;
        }
        $count = $this->ContainersSittings->find()->where(['container_id' => $containerId])->count();

        return $count === 0;
    }

    /**
     * @param int $containerId containerId
     * @param int $statActId stateactId
     * @param bool $isTransactionOpened Si la une transaction est déjà ouverte
     * @return void
     */
    public function addStateact($containerId, $statActId, $isTransactionOpened = true)
    {
        $ContainersStateacts = $this->fetchTable('ContainersStateacts');

        $lastContainerStateact = $ContainersStateacts
            ->find()
            ->where(['container_id' => $containerId])
            ->orderDesc('modified')
            ->first();

        if (empty($lastContainerStateact) || $lastContainerStateact->stateact_id !== (int)$statActId) {
            $containerStateact = $ContainersStateacts->newEntity([
                'container_id' => $containerId,
                'stateact_id' => $statActId,
            ]);
            $ContainersStateacts->save($containerStateact, ['atomic' => false]);
        } else {
            $ContainersStateacts->touch($lastContainerStateact);
            $ContainersStateacts->save($lastContainerStateact, ['atomic' => false]);
        }

        if ($isTransactionOpened) {
            $this->getConnection()->commit();
        }
    }

    /**
     * @param int $containerId containerId
     * @param array $sittingIds sittingIds
     * @return void
     */
    public function associateSittings($containerId, $sittingIds)
    {
        $container = $this->get(
            $containerId,
            [
                'contain' => ['Sittings'],
            ]
        );

        $containerArray = $container->toArray();
        $containerArray['sittings'] = ['_ids' => $sittingIds];
        $this->patchEntity($container, $containerArray);

        $this->saveOrFail($container);
    }

    /**
     * Return the last relation between container  / stateact
     *
     * @param int $containerId containerId
     * @param int $limit limit default 1.
     * @return array
     */
    public function getLastStateActEntry($containerId, $limit = 1)
    {
        $Stateacts = $this->fetchTable('Stateacts');
        $query = $Stateacts
            ->find()
            ->innerJoinWith('Containers')
            ->where(['ContainersStateacts.container_id' => $containerId])
            ->order(['ContainersStateacts.modified' => 'DESC'])
            ->limit($limit);

        return $query->toArray();
    }

    /**
     * Return all relations between container  / stateact
     *
     * @param int $containerId containerId
     * @return array
     */
    public function getAllStateActsEntries($containerId)
    {
        $ContainersStateacts = $this->fetchTable('ContainersStateacts');
        $Stateacts = $this->fetchTable('Stateacts');

        $cs = $ContainersStateacts->find()
            ->select(['stateact_id'])
            ->where(['container_id' => $containerId])
            ->order(['modified' => 'DESC'])
            ->toArray();

        return $Stateacts->find()->where(['id IN' => Hash::extract($cs, '{n}.stateact_id')])->toArray();
    }

    /**
     * @param int $container_id ID du container
     * @param int $pastellFluxTypeId ID du type de flux
     * @param int $structureId structureId
     * @return array|bool
     * @access  public
     * @created 02/07/2019
     * @throws \Exception
     * @version V0.0.9
     */
    public function buildPastellDocumentActesGeneriques($container_id, $pastellFluxTypeId, $structureId)
    {
        $container = $this->find()
            ->where(
                [
                    'Containers.id' => $container_id,
                ]
            )->contain(
                [
                    'Structures',
                    'Projects',
                    'Typesacts',
                    'Typesacts.Natures',
                    'Maindocuments' => [
                        'conditions' => ['current' => true],
                    ],
                    'Maindocuments.Files',
                    'Annexes',
                    'Annexes.Files',
                ]
            )
            ->firstOrFail()
            ->toArray();

        $idOrchestration = $container['structure']['id_orchestration'];
        $projectId = $container['project']['id'];

        if ($this->needCreateDocumentOnPastell($container_id, $idOrchestration, $structureId)) {
            $documentSousPastell = $this->createDocumentSousPastell($idOrchestration, $structureId);
            if (empty($documentSousPastell['id_d'])) {
                return $documentSousPastell['message'];
            }
        } else {
            $PastellFlux = $this->fetchTable('Pastellfluxs');
            $documentSousPastell = $PastellFlux->find()
                ->select('id_d')
                ->where(
                    [
                        'container_id' => $container_id,
                    ]
                )
                ->orderDesc('id')
                ->first();
        }

        $organizationId = $this->Structures->get($container['structure_id'])->organization_id;

        $idDocument = $documentSousPastell['id_d'];
        $success = $this->savePastellFlux(
            $pastellFluxTypeId,
            $idDocument,
            $container_id,
            $organizationId,
            $container['structure_id'],
            'PENDING',
            !empty($documentSousPastell['last_action']['message']) ?? $documentSousPastell['last_action']['message']
        );

        $CodeTypeDefaut = $this->getCodeTypeDefault($container['typesact']['nature']['id']);

        if ($success == false) {
            Log::error(
                "erreur lors de l'enregistrement de l'etat du flux pastell, id_d perdu : " . $idDocument,
                'Pastellfluxs'
            );

            return false;
        }

        $arrayTypesPJAnnexes = [];
        if (!empty($container['annexes'])) {
            $arrayTypesPJAnnexes = [];
            foreach ($container['annexes'] as $annexe) {
                if ($annexe['istransmissible'] && $annexe['current']) {
                    $arrayTypesPJAnnexes[] = $annexe['codetype'] ?? $CodeTypeDefaut;
                }
            }
        }

        $dateActe = $this->getDateDecision($container);

        $data = [
            'id_e' => $idOrchestration, //ID de la structure sous pastell
            'id_d' => $idDocument, //ID du document sous pastell
            'acte_nature' => $container['typesact']['nature']['codenatureacte'] ?? 'DE', // table natures codenatureacte // Nature de l'acte
            'classification' => $container['project']['matiere']['codematiere'] ?? 1.1, // Classification de l'acte
            'document_papier' => $container['project']['ismultichannel'],
            'objet' => $container['project']['name'],
            'numero_de_lacte' => $container['project']['code_act']
                ?? chr(rand(65, 90)) . '_' . random_int(0, 1000000000000),
            'date_de_lacte' => $dateActe !== null ? $dateActe->format('y-m-d') : date('y-m-d'),
            'envoi_tdt' => $container['typesact']['istdt'] ? true : false,
            'envoi_signature' => $this->isSignedManualy($container_id) ? false : true,
        ];

        $this->patchDocumentSousPastell($idOrchestration, $idDocument, $data, $structureId);

        $success = $this->updateAnnexes($idOrchestration, $idDocument, $container, $pastellFluxTypeId, $organizationId);

        $typePieces = [];
        $typePieces['type_pj'] = [$container['maindocument']['codetype'] ?? $CodeTypeDefaut];

        // @info: on veut relancer le worker, donc on attend une exception
        $dataSendDocument = [
            'file_name' => $container['maindocument']['files'][0]['name'],
            'file_content' => file_get_contents($container['maindocument']['files'][0]['path']),
        ];
        $internalData = [
            'orchestrationId' => $idOrchestration,
            'containerId' => $container_id,
            'structureId' => $structureId,
        ];

        // if (mimetype($container['maindocument']['files'][0]['path']) == 'application/pdf') {
        if ($this->needSendMainDocumentSousPastell($container_id, $idOrchestration, $structureId)) {
            $sendMainDocument = $this->sendMainDocumentSousPastell(
                $idOrchestration,
                $idDocument,
                $dataSendDocument,
                $structureId
            );

            if (!isset($sendMainDocument['formulaire_ok']) || $sendMainDocument['formulaire_ok'] == false) {
                $success = $this->savePastellFlux(
                    $pastellFluxTypeId,
                    $idDocument,
                    $container_id,
                    $organizationId,
                    $container['structure_id'],
                    'FAILED',
                    $sendMainDocument['message']
                );

                if ($success == false) {
                    Log::error("erreur lors de l'enregistrement FAILED de l'etat du flux pastell", 'Pastellfluxs');
                }
            }
        }

        $types = [];
        $types['type_pj'] = array_merge($typePieces['type_pj'], $arrayTypesPJAnnexes);
        if (!empty($types['type_pj'])) {
            $this->patchTypologiePieces($internalData, $types);
        }

        //Si pas de return ayant pour cause un erreur sur Pastell.
        $success = $this->savePastellFlux(
            $pastellFluxTypeId,
            $idDocument,
            $container_id,
            $organizationId,
            $container['structure_id'],
            'DONE'
        );

        if ($success == false) {
            Log::error("erreur lors de l'enregistrement DONE de l'etat du flux pastell", 'Pastellfluxs');
        }

        return $container;
    }

    /**
     * @param int $id_orchestration The pastell id of the strucutre
     * @param int $structureId structureId
     * @return mixed
     */
    public function createDocumentSousPastell($id_orchestration, $structureId)
    {
        $client = $this->getPastellClient($structureId);
        $response = $client->createDocument($id_orchestration);

        return $response->getJson();
    }

    /**
     * Fonction permettant l'envoie des modifications à apporter sur le document
     *
     * @param int $id_orchestration : id_e sous pastell de notre structure
     * @param int $id_document : id_d sous pastell de notre document
     * @param array $data valeur à modifier sous
     *                                 pastell
     * @param int $structureId structureId
     * @return mixed
     * @access  public
     * @created 09/07/2019
     * @version V0.0.9
     */
    public function patchDocumentSousPastell($id_orchestration, $id_document, $data, $structureId)
    {
        $client = $this->getPastellClient($structureId);
        $response = $client->editDocument($id_orchestration, $id_document, $data);

        return $response->getJson();
    }

    /**
     * Fonction permettant l'envoie du document principal sous pastell
     *
     * @param int $id_orchestration : id_e sous pastell de notre structure
     * @param int $id_document : id_d sous pastell de notre document
     * @param array $data Tableau avec le nom et les datas du document principale à envoyer sous
     *                                 pastell
     * @param int $structureId structureId
     * @return mixed
     * @access  public
     * @created 09/07/2019
     * @version V0.0.9
     */
    public function sendMainDocumentSousPastell($id_orchestration, $id_document, $data, $structureId)
    {
        $client = $this->getPastellClient($structureId);

        $response = $client->sendMainFileToDocument($id_orchestration, $id_document, $data);

        return $response->getJson();
    }

    /**
     * Fonction permettant l'envoie d'une annexe à pastell
     *
     * @param int $id_orchestration : id_e sous pastell de notre structure
     * @param int $id_document : id_d sous pastell de notre document
     * @param array $data Tableau avec le nom et les datas du document annexe à envoyer sous
     *                                 pastell
     * @param int $key numero de l'annexe
     * @param int $structureId structureId
     * @return mixed
     * @access  public
     * @created 09/07/2019
     * @version V0.0.9
     */
    public function sendAnnexeDocumentSousPastell($id_orchestration, $id_document, $data, $key, $structureId)
    {
        $client = $this->getPastellClient($structureId);
        $structures = $client->sendAnnexeFileToDocument($id_orchestration, $id_document, $data, $key);

        return $structures->getJson();
    }

    /**
     * Patch la typologie d'une annexe spécifiée par son numéro
     *
     * @param array $internalData internal data
     * @param array $data données pour le
     *                             patch
     * @return \Cake\Http\Client\Response
     */
    public function patchTypologiePieces(array $internalData, array $data)
    {
        $data['externalField'] = [
            'name' => 'type_piece',
            'value' => $data,
        ];
        $PastellFlux = $this->fetchTable('Pastellfluxs');

        $data['id_d'] = $PastellFlux->find()
            ->select('id_d')
            ->where(
                [
                    'container_id' => $internalData['containerId'],
                ]
            )->orderDesc('id')->first()->get('id_d');

        $returnPastell = $this->patchPastellExternalData(
            $internalData['structureId'],
            $internalData['orchestrationId'],
            $data
        );

        return $returnPastell;
    }

    /**
     * @param int $pastellFluxType_id ID du flux
     * @param int $id_document ID du document sous pastell
     * @param int $container_id ID du container
     * @param int $organization_id ID de l'organization
     * @param int $structure_id ID de la structure
     * @param string $state Etat du flux
     * @param string $message Message retourné par PASTELL lors de l'éxécution de
     *                                    l'ction
     * @return bool
     * @access  public
     * @created 09/07/2019
     * @version V0.0.9
     */
    public function savePastellFlux($pastellFluxType_id, $id_document, $container_id, $organization_id, $structure_id, $state, $message = null)// @codingStandardsIgnoreLine
    {
        $pastellFluxs = $this->fetchTable('Pastellfluxs');
        $pastellflux = $pastellFluxs->newEntity(
            [
                'organization_id' => $organization_id,
                'structure_id' => $structure_id,
                'pastellfluxtype_id' => $pastellFluxType_id,
                'id_d' => $id_document,
                'container_id' => $container_id,
                'state' => $state,
                'comments' => $message,
            ]
        );

        $success = $pastellFluxs->save($pastellflux) !== false;
        // Spécifier ici que c'est vraiment pour neovyé au tDT
        if ($success && $state == 'DONE') {
            switch ($this->getLastStateActEntry($container_id)[0]->id) {// Fire an event when the document is ready to be send
                case PENDING_PASTELL:
                    $event = new Event(
                        'Model.Container.afterSavePastellFlux',
                        $this,
                        [
                            'id_d' => $id_document,
                            'structureId' => $structure_id,
                            'organizationId' => $organization_id,
                            'pastellFluxId' => $pastellflux->id,
                        ]
                    );
                    $this->getEventManager()->dispatch($event);
                    break;
            }
        }

        Log::write(
            'info',
            '[DocWaId] ' . $container_id . ' - ' . '[DocPASTELLId] ' . $id_document . ' [mess] : ' . $message,
            'pastell'
        );

        return $success;
    }

    /**
     * REMI PQ passer par le container pour ça ?
     * Exécute une action sur un document PASTELL
     *
     * @param int $entityId The PASTELL id => id_orchestration
     * @param string $documentId The PASTELL docuemnt id => id_d
     * @param string $actionName Nom de l'action à effectuer sur un
     *                             document
     * @param int $structureId structureId
     * @return mixed
     */
    public function triggerAnActionOnDocument($entityId, $documentId, $actionName, $structureId)
    {
        $client = $this->getPastellClient($structureId);
        $document = $client->triggerActionOnDocument($entityId, $documentId, $actionName);
        $json = $document->getJson();

        $message = sprintf(
            '[TRIGGER ACTION RESPONSE] for action %s on entity %s, structure %s, document %s - %s',
            $actionName,
            (string)$entityId,
            (string)$structureId,
            (string)$documentId,
            var_export($json, true)
        );
        Log::write('info', $message, 'pastell');

        return $json;
    }

    /**
     * Sauvegarde l'action exécuté pour un flux PASTELL
     *
     * @param string $action Nom de l'action
     * @param int $pastellFluxId Identifiant du flux PASTELL
     * @param int $organization_id Identifiant de l'organisation
     * @param int $structure_id Identifiant de la stucture
     * @param string $state Etat de l'ation => PENDING, DOE , FAILED
     * @param string $message Message retourné pas PASTELL lors de l'éxécution de
     *                                 l'action.
     * @return bool
     */
    public function savePastellFluxAction($action, $pastellFluxId, $organization_id, $structure_id, $state, $message = '')// @codingStandardsIgnoreLine
    {
        $PastellfluxsAction = $this->fetchTable('pastellfluxactions');
        $pastellFluxAction = $PastellfluxsAction->newEmptyEntity();
        $pastellFluxAction = $PastellfluxsAction->patchEntity(
            $pastellFluxAction,
            [
                'organization_id' => $organization_id,
                'structure_id' => $structure_id,
                'pastellflux_id' => $pastellFluxId,
                'state' => $state,
                'action' => $action,
                'comments' => $message,
            ]
        );

        Log::write(
            'info',
            '[StructureId]: ' . $structure_id . '[FLuxPastellId]: ' . $pastellFluxId . ' [action]: ' . $action . ' [message]: ' . $message,// @codingStandardsIgnoreLine
            'pastell'
        );

        return $PastellfluxsAction->save($pastellFluxAction) !== false;
    }

    /**
     * If the act is on Pastell return TRUE , false otherwise.
     *
     * @param int $containerId The container id to check
     * @return bool
     */
    public function isOnPastell(int $containerId): bool
    {
        $PastellFlux = $this->fetchTable('Pastellfluxs');
        $documentSousPastell = $PastellFlux->find()
            ->select('id_d')
            ->where(
                [
                    'container_id' => $containerId,
                ]
            )
            ->orderDesc('id') // si le document à été créé à nouveau sur PASTELL (ex : Refus parapheur puis renvoi parapheur)
            ->first();

        return $documentSousPastell !== null;
    }

    /**
     * @param int $containerId The id container
     * @return void
     */
    public function sendEnventPacthPastell(int $containerId)
    {
        $strucutreId = $this->find()
            ->select('structure_id')
            ->where(
                [
                    'id' => $containerId,
                ]
            )->first();

        $event = new Event(
            'Model.Project.afterSaveEdit.PatchPastell',
            $this,
            [
                'id_d' => $containerId,
                'flux' => Configure::read('Flux.actes-generique.name'),
                'flux_name' => Configure::read('Flux.actes-generique.name'),
                'structureId' => $strucutreId['structure_id'],
                'organizationId' => $this->Structures->getOrganizationId($strucutreId['structure_id']),
            ]
        );
        $this->getEventManager()->dispatch($event);
    }

    /**
     * @param int|null $containerId The container Id
     * @param int|null $structureId The structure Id
     * @return array
     */
    public function getIparapheurSousTypes(?int $containerId = null, ?int $structureId = null): array
    {
        $pastellClient = $this->getPastellClient($structureId ?? $this->Structures->get('id'));

        $PastellFlux = $this->fetchTable('Pastellfluxs');

        $documentSousPastell = $PastellFlux->find()
            ->select('id_d')
            ->where(
                [
                    'container_id' => $containerId ?? $this->get('id'),
                ]
            )
            ->orderDesc('id')
            ->first();

        $id_orchestration = $this->Structures->find()
            ->select('id_orchestration')
            ->where(
                [
                    'id' => $structureId ?? $this->Structures->get('id'),
                ]
            )
            ->first();

        $sousTypesParapheur = $pastellClient
            ->getIparapheurSousTypesByDocument(
                $id_orchestration['id_orchestration'],
                $documentSousPastell['id_d']
            );

        return $sousTypesParapheur->getJson() ?: [];
    }

    /**
     * @param int $structureId L'identifiant de la strucuture
     * @param int $entityId L'identifiant de la
     *                            collectivé PASTELL
     * @param array $data Les données nécessaires pour effectuer le
     *                            patch
     * @return \Cake\Http\Client\Response
     */
    public function patchPastellExternalData(int $structureId, int $entityId, array $data)
    {
        $client = $this->getPastellClient($structureId);

        return $client->patchExternalData(
            $entityId,
            $data['id_d'],
            $data['externalField']['name'],
            $data['externalField']['value']
        );
    }

    /**
     * Return the current parapheur status but do not save it into the webact database
     *
     * @param int $structureId Id of the structure
     * @param int $containerId Id of the container
     * @return array
     */
    public function getIParapheurStatus(int $structureId, int $containerId): array
    {
        Log::write('info', self::class . '*******' . __FUNCTION__ . '*******', 'pastell');

        $client = $this->getPastellClient($structureId);

        /**
         * @var \App\Model\Table\StructuresTable $Structure
         */
        $Structure = $this->fetchTable('Structures');
        $EntityPastellId = $Structure->find()
            ->select('id_orchestration')
            ->where(
                [
                    'id' => $structureId,
                ]
            )->firstOrFail()->get('id_orchestration');

        $PastellFlux = $this->fetchTable('Pastellfluxs');

        $documentSousPastell = $PastellFlux->find()
            ->select('id_d')
            ->where(
                [
                    'container_id' => $containerId,
                ]
            )
            ->orderDesc('id')
            ->first();

        return $client->getIParapheurStatus($EntityPastellId, $documentSousPastell['id_d']);
    }

    /**
     * True if the project was signed manualy
     *
     * @param int $container_id The container Id
     * @return bool
     */
    public function isSignedManualy(int $container_id)
    {
        return $this->fetchTable('ContainersStateacts')
            ->find()
            ->where(
                [
                    'container_id' => $container_id,
                    'stateact_id' => DECLARE_SIGNED,
                ]
            )
            ->count();
    }

    /**
     * @param int $sittingId id de la séance
     * @return int
     */
    public function getNextPositionIntoSitting(int $sittingId): int
    {
        $maxValue = $this->fetchTable('ContainersSittings')
            ->find()
            ->select('rank')
            ->where(
                [
                    'sitting_id' => $sittingId,
                ]
            )
            ->all()
            ->max('rank');

        return !empty($maxValue['rank']) ? $maxValue['rank'] + 1 : 1;
    }

    /**
     * getSittingIsDeliberating
     *
     * @param int $id ContainerId
     * @return int SittingId
     */
    public function getDeliberatingSittingIdByContainerId(int $id)
    {
        $sitting = $this->getDeliberatingSittingByContainerId($id);

        return $sitting ? $sitting->id : null;
    }

    /**
     * Récupère la séance délibérante pour un container
     *
     * @param int $id container_id
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function getDeliberatingSittingByContainerId(int $id)
    {
        return $this->fetchTable('Sittings')->find()
            ->innerJoinWith('Typesittings', function ($q) {
                return $q->where(['Typesittings.isdeliberating' => true]);
            })
            ->innerJoinWith('ContainersSittings', function ($q) use ($id) {
                return $q->where(['ContainersSittings.container_id' => $id]);
            })
            ->first();
    }

    /**
     * Retourne la position du container dans la séance
     *
     * @param int $sittingId identifiant de la séance
     * @param int $containerId identifiant du container
     * @return int
     */
    public function getRankInDeliberatingSitting($sittingId, $containerId): int
    {
        return $this->fetchTable('ContainersSittings')
            ->find()
            ->select(['ContainersSittings.rank'])
            ->where(
                [
                    'ContainersSittings.container_id' => $containerId,
                    'ContainersSittings.sitting_id' => $sittingId,
                ]
            )
            ->first()
            ->get('rank');
    }

    /**
     * @param int $containerTypeActNatureId id
     * @return mixed
     */
    private function getCodeTypeDefault(int $containerTypeActNatureId)
    {
        $tableLocatorTypespiecesjointes = new TableLocator();
        $Typespiecesjointes = $tableLocatorTypespiecesjointes->get('Typespiecesjointes');

        return $Typespiecesjointes->find()
            ->where(['nature_id' => $containerTypeActNatureId])
            ->order(['codetype' => 'DESC'])
            ->first()
            ->get('codetype');
    }

    /**
     * @param int $containerId container_id
     * @param int $idOrchestration idOrchestration
     * @param int $structureId structureId
     * @return bool
     */
    private function needCreateDocumentOnPastell(int $containerId, int $idOrchestration, int $structureId)
    {
        $document = $this->getDocumentOnPastell($containerId, $idOrchestration, $structureId);

        return !$this->isOnPastell($containerId)
            || $this->isDeletableOnlyOnPastell($containerId, $idOrchestration, $structureId);
    }

    /**
     * @param int $container_id The container id
     * @param int $idOrchestration The pastell id of the strucutre
     * @param int $structureId The structure id
     * @return bool
     */
    private function needSendMainDocumentSousPastell(int $container_id, int $idOrchestration, int $structureId)
    {
        return (
                !$this->isSigned($container_id) && $this->getLastStateActEntry($container_id)[0]->id != PENDING_PASTELL)
            || $this->isEditableOnPastell($container_id, $idOrchestration, $structureId)
            || $this->getLastStateActEntry($container_id)[0]->id == TDT_ERROR;
    }

    /**
     * @param int $containerId The container id
     * @return bool
     */
    public function isEditable(int $containerId)
    {
        return in_array($this->getLastStateActEntry($containerId)[0]->id, [
            DRAFT,
            VALIDATION_PENDING,
            VALIDATED,
            REFUSED,
            PARAPHEUR_REFUSED,
            VOTED_REJECTED,
            VOTED_APPROVED,
        ], true);
    }

    /**
     * @param int $containerId containerId
     * @param int $idOrchestration idOrchestration
     * @param int $structureId structureId
     * @return bool
     */
    private function isEditableOnPastell(int $containerId, int $idOrchestration, int $structureId)
    {
        return $this->checkStateOnPastell($containerId, $idOrchestration, $structureId, 'modification');
    }

    /**
     * @param int $containerId containerId
     * @param int $idOrchestration idOrchestration
     * @param int $structureId structureId
     * @param string $state action_possible pastell
     * @return bool
     */
    public function checkStateOnPastell(int $containerId, int $idOrchestration, int $structureId, string $state)
    {
        $PastellFlux = $this->fetchTable('Pastellfluxs');
        $documentId_d = $this->getDocumentIdD($PastellFlux, $containerId);

        if ($documentId_d !== null) {
            $document = $this->getPastellClient($structureId)
                ->getDocument($idOrchestration, $documentId_d['id_d'])
                ->getJson();

            return in_array($state, $document['action_possible'], true);
        }

        return false;
    }

    /**
     * @param int $containerId containerId
     * @param int $idOrchestration idOrchestration
     * @param int $structureId structureId
     * @return bool
     */
    private function isDeletableOnlyOnPastell(int $containerId, int $idOrchestration, int $structureId)
    {
        $PastellFlux = $this->fetchTable('Pastellfluxs');
        $documentId_d = $this->getDocumentIdD($PastellFlux, $containerId);

        if ($documentId_d !== null) {
            $document = $this->getPastellClient($structureId)
                ->getDocument($idOrchestration, $documentId_d['id_d'])
                ->getJson();

            return count($document['action_possible']) == 1
                && in_array('supression', $document['action_possible'], true);
        }

        return false;
    }

    /**
     * @param \Cake\ORM\Table $PastellFlux PastellfluxsTable
     * @param int $containerId Identifiant du container
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function getDocumentIdD(Table $PastellFlux, int $containerId)
    {
        $documentId_d = $PastellFlux->find()
            ->select('id_d')
            ->where(['container_id' => $containerId])
            ->orderDesc('id')
            ->first();

        return $documentId_d;
    }

    /**
     * Récupère le document sur Pastell
     *
     * @param int $containerId Identifiant du container
     * @param int $idOrchestration Identifiant de la structure sur PASTELL
     * @param int $structureId Identifiant de la structure
     * @return array|false|mixed
     */
    private function getDocumentOnPastell(int $containerId, int $idOrchestration, int $structureId)
    {
        $PastellFlux = $this->fetchTable('Pastellfluxs');
        $documentId_d = $this->getDocumentIdD($PastellFlux, $containerId);

        if ($documentId_d !== null) {
            $document = $this->getPastellClient($structureId)
                ->getDocument($idOrchestration, $documentId_d['id_d'])
                ->getJson();

            return $document;
        }

        return false;
    }

    /**
     * @param int $containerId Identifiant du container
     * @param int $idOrchestration Identifiant de la structure sur PASTELL
     * @param int $structureId Identifiant de la structure
     * @return int|null
     */
    private function getNumberOfAnnexesOnPastell(int $containerId, int $idOrchestration, int $structureId)
    {
        $document = $this->getDocumentOnPastell($containerId, $idOrchestration, $structureId);

        return isset($document['data']['autre_document_attache'])
            ? count($document['data']['autre_document_attache'])
            : 0;
    }

    /**
     * Supprime une annexe (autre_document_attache) sur PASTELL
     *
     * @param int $idOrchestration Identifiant de la structure sur PASTELL
     * @param int $structureId Identifiant de la structure
     * @param string $idDocument Identifiant du document sur PASTELL
     * @param int $key Numéro sur PASTELL du document attache
     * @return array|mixed
     */
    private function deleteAnnexeOnPastell(int $idOrchestration, int $structureId, string $idDocument, int $key)
    {
        return $this->getPastellClient($structureId)->deleteAnnexe($idOrchestration, $idDocument, $key)->getJson();
    }

    /**
     * Get Date Decision
     *
     * @param \App\Model\Entity\Container $container Container
     * @return \Cake\I18n\FrozenTime|null
     */
    public function getDateDecision($container): ?FrozenTime
    {
        $sitting = $this->getDeliberatingSittingByContainerId($container['id']);
        if ($sitting !== null) {
            return new FrozenTime($sitting->get('date'));
        }

        return $container['project']['signature_date'] ? new FrozenTime($container['project']['signature_date']) : null;
    }

    /**
     * @param int $container_id
     * @param mixed $idOrchestration
     * @param string $idDocument
     * @param array $container
     * @param int $pastellFluxTypeId
     * @param int $organizationId
     * @return bool
     */
    // @codingStandardsIgnoreLine
    public function updateAnnexes(mixed $idOrchestration, string $idDocument, array $container, int $pastellFluxTypeId, int $organizationId): bool
    {
        $success = true;

        $annexesCount = $this->getNumberOfAnnexesOnPastell(
            $container['id'],
            $idOrchestration,
            $container['structure_id']
        );

        for ($key = 0; $key < $annexesCount; $key++) {
            $response = $this->deleteAnnexeOnPastell($idOrchestration, $container['structure_id'], $idDocument, $key);
        }
        foreach ($container['annexes'] as $annexNum => $annexe) {
            if ($annexe['istransmissible']) { //envoi a pastell uniquement si l'annexe est télétransmissible
                $dataSendAnnexeDocument = [
                    'file_name' => $annexe['file']['name'],
                    'file_content' => file_get_contents($annexe['file']['path']),
                ];

                $sendAnnexeDocument = $this->sendAnnexeDocumentSousPastell(
                    $idOrchestration,
                    $idDocument,
                    $dataSendAnnexeDocument,
                    $annexNum,
                    $container['structure_id']
                );
            }
        }

        return $success;
    }
}
