<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Organizations Model
 *
 * @property \App\Model\Table\ActsTable|\App\Model\Table\HasMany $Acts
 * @property \App\Model\Table\AttachmentsTable|\App\Model\Table\HasMany $Attachments
 * @property \App\Model\Table\ClassificationsTable|\App\Model\Table\HasMany $Classifications
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\HasMany $Containers
 * @property \App\Model\Table\ConvocationsTable|\App\Model\Table\HasMany $Convocations
 * @property \App\Model\Table\DposTable|\App\Model\Table\HasMany $Dpos
 * @property \App\Model\Table\FieldsTable|\App\Model\Table\HasMany $Fields
 * @property \App\Model\Table\HistoriesTable|\App\Model\Table\HasMany $Histories
 * @property \App\Model\Table\MetadatasTable|\App\Model\Table\HasMany $Metadatas
 * @property \App\Model\Table\NaturesTable|\App\Model\Table\HasMany $Natures
 * @property \App\Model\Table\OfficialsTable|\App\Model\Table\HasMany $Officials
 * @property \App\Model\Table\ProjectsTable|\App\Model\Table\HasMany $Projects
 * @property \App\Model\Table\RolesTable|\App\Model\Table\HasMany $Roles
 * @property \App\Model\Table\ServicesTable|\App\Model\Table\HasMany $Services
 * @property \App\Model\Table\SittingsTable|\App\Model\Table\HasMany $Sittings
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\HasMany $Structures
 * @property \App\Model\Table\TabsTable|\App\Model\Table\HasMany $Tabs
 * @property \App\Model\Table\TdtMessagesTable|\App\Model\Table\HasMany $TdtMessages
 * @property \App\Model\Table\TemplatesTable|\App\Model\Table\HasMany $Templates
 * @property \App\Model\Table\ThemesTable|\App\Model\Table\HasMany $Themes
 * @property \App\Model\Table\TypesactsTable|\App\Model\Table\HasMany $Typesacts
 * @property \App\Model\Table\TypesittingsTable|\App\Model\Table\HasMany $Typesittings
 * @property \App\Model\Table\UsersTable|\App\Model\Table\HasMany $Users
 * @property \App\Model\Table\ValuesTable|\App\Model\Table\HasMany $Values
 * @method \App\Model\Entity\Organization get($primaryKey, $options = [])
 * @method \App\Model\Entity\Organization newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Organization[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Organization|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Organization saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Organization patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Organization[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Organization findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrganizationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('organizations');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $allHasManyAssociations = [
            'Pastellfluxactions',
            'Pastellfluxs',
            'Pastellfluxtypes',
            'Structures',
            'Users',
        ];
        // @info: choose WRT @master 20190716-20h55
        $hasManyAssociations = $allHasManyAssociations;
        foreach ($hasManyAssociations as $hasManyAssociation) {
            $this->hasMany($hasManyAssociation, ['foreignKey' => 'organization_id', 'dependent' => true]);
        }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->boolean('active')
            ->allowEmptyString('active', null, false);

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }
}
