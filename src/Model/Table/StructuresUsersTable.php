<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StructuresUsers Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\UsersTable|\App\Model\Table\BelongsTo $Users
 * @method \App\Model\Table\StructuresUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\StructuresUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\StructuresUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StructuresUser|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StructuresUser|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StructuresUser patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StructuresUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\StructuresUser findOrCreate($search, callable $callback = null, $options = [])
 */
class StructuresUsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('structures_users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo(
            'Structures',
            [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Users',
            [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->isUnique(['structure_id', 'user_id']));

        return $rules;
    }
}
