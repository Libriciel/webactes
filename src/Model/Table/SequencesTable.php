<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Http\Exception\NotFoundException;
use Cake\I18n\FrozenDate;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use RuntimeException;

/**
 * Sequences Model
 *
 * @property \App\Model\Table\CountersTable|\App\Model\Table\HasMany $Counters
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @method \App\Model\Entity\Sequence get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sequence newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sequence[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sequence|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sequence saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sequence patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sequence[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sequence findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SequencesTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sequences');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->hasMany('Counters', [
            'foreignKey' => 'sequence_id',
        ]);

        $this->belongsTo('Structures', [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('structure_id')
            ->requirePresence('structure_id', 'create');

        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        $validator
            ->scalar('comment')
            ->maxLength('comment', 1500)
            ->allowEmptyString('comment');

        $validator
            ->integer('sequence_num')
            ->requirePresence('sequence_num', 'create')
            ->allowEmptyString('sequence_num', null, false);

        $validator
            ->date('starting_date')
            ->allowEmptyDate('starting_date');

        $validator
            ->date('expiry_date')
            ->allowEmptyDate('expiry_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['structure_id', 'name']));
        $rules->addUpdate(function ($entity) {
            return $entity->availableActions['can_be_edited']['value'];
        }, 'sequenceCantBeEdited', [
            'errorField' => 'id',
            'message' => 'Cannot edit a sequence without the admin role',
        ]);

        $rules->addDelete(function ($entity) {
            return $entity->availableActions['can_be_deleted']['value'];
        }, 'sequenceCantBeDeleted', [
            'errorField' => 'id',
            'message' => 'Cannot delete a sequence in use',
        ]);

        return $rules;
    }

    /**
     * Returns a sequence by it's id
     *
     * @param int $id sequence
     * @return \App\Model\Entity\Sequence
     * @throws \Cake\Http\Exception\NotFoundException
     * @version V2
     */
    public function findById(int $id)
    {
        $sequence = $this->get($id);

        if (empty($sequence)) {
            throw new NotFoundException(__('ID:"%s" de sequence invalide', $id));
        }

        return $sequence;
    }

    /**
     * Checks all sequences used by counters
     *
     * @return \Cake\ORM\Query
     * @created 17/12/2020
     * @version V2
     */
    public function findAvailableSequences()
    {
        $countersTable = $this->fetchTable('Counters');
        $counters = $countersTable->find()->select('sequence_id');

        return $this->find()
            ->where(['id NOT IN' => $counters]);
    }

    /**
     * Increase sequence number
     *
     * @param \Cake\I18n\FrozenDate $date choosen date
     * @param string $reinit_def reinit_def
     * @param int $sequenceId sequence
     * @return int
     * @version V2
     */
    public function getNextValue(FrozenDate $date, string $reinit_def, int $sequenceId)
    {
        $sequence = $this->findById($sequenceId);
        $reinit = $this->checkIfReinit(
            new FrozenDate($sequence->starting_date),
            new FrozenDate($sequence->expiry_date),
            $reinit_def,
            $date
        );

        if ($reinit) {
            $this->reInit($sequence->id, $reinit_def, $date);

            return 1;
        } else {
            $this->increment($sequence->id, $sequence->sequence_num);

            return $sequence->sequence_num + 1;
        }
    }

    /**
     * Reinitialize counter with info provided
     *
     * @param int $id counter
     * @param string $reinitDef reinit def
     * @param \Cake\I18n\FrozenDate $date choosen date
     * @throws \RuntimeException
     * @return void
     * @version V2
     */
    public function reInit(int $id, string $reinitDef, FrozenDate $date)
    {
        $sequence = $this->findById($id);
        $data = $this->calculatesNewDates($reinitDef, $date) + ['id' => $id, 'sequence_num' => 1];

        $sequence = $this->patchEntity($sequence, $data);

        if ($this->save($sequence) === false) {
            throw new RuntimeException(__(
                'Impossible d\'enregistrer la séquence d\'id %d (%s)',
                $id,
                var_export($this->Sequence->validationErrors, true)
            ));
        }
    }

    /**
     * Increments sequence_num
     *
     * @param int $id sequence
     * @param int $currentValue sequence value
     * @throws \RuntimeException
     * @return void
     * @version V2
     */
    public function increment(int $id, int $currentValue)
    {
        $sequence = $this->findById($id);
        $this->patchEntity($sequence, ['sequence_num' => $currentValue + 1 ]);
        if (!$this->save($sequence)) {
            throw new RuntimeException(__(
                'Impossible d\'enregistrer la séquence d\'id',
                $id,
                var_export($sequence->getErrors(), true)
            ));
        }
    }

    /**
     * Checks if sequence must be reinit
     *
     * @param \Cake\I18n\FrozenDate $startingDate starting date
     * @param \Cake\I18n\FrozenDate $expiryDate expiry date
     * @param string $reinitDef reinit_def
     * @param \Cake\I18n\FrozenDate $date date
     * @throws \Cake\Http\Exception\NotFoundException
     * @return bool
     * @version V2
     */
    public function checkIfReinit(FrozenDate $startingDate, FrozenDate $expiryDate, string $reinitDef, FrozenDate $date)
    {
        if (empty($reinitDef)) {
            return false;
        }

        $date->i18nFormat('Y-m-d');

        if ($date < $startingDate) {
            throw new NotFoundException('Dernière réinitialisation : ' . $startingDate . ', date demandée ' . $date);
        }
        if (($date >= $startingDate) && ($date <= $expiryDate)) {
            return false;
        }
        if ($date > $expiryDate) {
            return true;
        }
        throw new NotFoundException('cas de reinitialisation non gere');
    }

    /**
     * Calculates new starting and ending dates
     *
     * @param string $reinitDef reinit_def
     * @param \Cake\I18n\FrozenDate $date date
     * @return array [["starting_date", "expiry_date"]]
     */
    public function calculatesNewDates(string $reinitDef, FrozenDate $date)
    {
        if ($reinitDef === '#AAAA#' || $reinitDef === '#AA#') {
            $startingDate = $date->format('Y-01-01');
            $expiryDate = $date->format('Y-12-31');
        }
        if ($reinitDef === '#M#' || $reinitDef === '#MM#') {
            $startingDate = $date->format('Y-m-01');
            $expiryDate = $date->format('Y-m-t');
        }
        if ($reinitDef === '#J#' || $reinitDef === '#JJ#') {
            $startingDate = $date->format('Y-m-d');
            $expiryDate = $date->format('Y-m-d');
        }
        $startingDate = $startingDate ?? [];
        $expiryDate = $expiryDate ?? [];

        return ['starting_date' => $startingDate, 'expiry_date' => $expiryDate];
    }
}
