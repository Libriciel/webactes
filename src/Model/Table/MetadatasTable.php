<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Metadatas Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\TemplatesTable|\App\Model\Table\BelongsTo $Templates
 * @property \App\Model\Table\TabsTable|\App\Model\Table\BelongsTo $Tabs
 * @property \App\Model\Table\FieldsTable|\App\Model\Table\BelongsTo $Fields
 * @method \App\Model\Table\Metadata get($primaryKey, $options = [])
 * @method \App\Model\Entity\Metadata newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Metadata[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Metadata|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Metadata|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Metadata patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Metadata[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Metadata findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MetadatasTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('metadatas');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Templates',
            [
                'foreignKey' => 'template_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Tabs',
            [
                'foreignKey' => 'tab_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Fields',
            [
                'foreignKey' => 'field_id',
                'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('number_line')
            ->requirePresence('number_line', 'create')
            ->allowEmptyString('number_line', false);

        $validator
            ->integer('number_column')
            ->requirePresence('number_column', 'create')
            ->allowEmptyString('number_column', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['template_id'], 'Templates'));
        $rules->add($rules->existsIn(['tab_id'], 'Tabs'));
        $rules->add($rules->existsIn(['field_id'], 'Fields'));

        return $rules;
    }
}
