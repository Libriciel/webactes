<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ActorsProjects Model
 *
 * @property \App\Model\Table\ProjectsTable|\App\Model\Table\BelongsTo $Projects
 * @property \App\Model\Table\ActorsTable|\App\Model\Table\BelongsTo $Actors
 * @method \App\Model\Table\ActorsProject get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActorsProject newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ActorsProject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActorsProject|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsProject|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsProject patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsProject[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsProject findOrCreate($search, callable $callback = null, $options = [])
 */
class ActorsProjectsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('actors_projects');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo(
            'Projects',
            [
                'foreignKey' => 'project_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Actors',
            [
                'foreignKey' => 'actor_id',
                'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['actor_id'], 'Actors'));
        $rules->add($rules->existsIn(['project_id'], 'Projects'));

        return $rules;
    }
}
