<?php
declare(strict_types=1);

namespace App\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\OrganizationsTable|\App\Model\Table\BelongsTo $Organizations
 * @property \App\Model\Table\HistoriesTable|\App\Model\Table\HasMany $Histories
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsToMany $Structures
 * @property \App\Model\Table\ServicesTable|\App\Model\Table\BelongsToMany $Services
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('LimitToStructure');
        $this->addBehavior('Timestamp');
        $this->addBehavior('SoftDeletion');
        $this->addBehavior(
            'LettercaseFormattable',
            [
                'upper_noaccents' => ['lastname'],
            ]
        );

        $this->belongsTo(
            'Organizations',
            [
                'foreignKey' => 'organization_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Histories',
            [
                'foreignKey' => 'user_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Systemlogs',
            [
                'bindingKey' => 'keycloak_id',
                'foreignKey' => 'keycloak_id',
                'dependant' => false,
            ]
        );
        $this->belongsToMany(
            'Roles',
            [
                'foreignKey' => 'user_id',
                'targetForeignKey' => 'role_id',
                'joinTable' => 'roles_users',
                'dependent' => true,
                'through' => RolesUsersTable::class,
            ]
        );
        $this->belongsToMany(
            'Structures',
            [
                'foreignKey' => 'user_id',
                'targetForeignKey' => 'structure_id',
                'joinTable' => 'structures_users',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Services',
            [
                'foreignKey' => 'user_id',
                'targetForeignKey' => 'service_id',
                'joinTable' => 'users_services',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'UsersInstances',
            [
                'foreignKey' => 'user_id',
                'dependent' => true,
            ]
        );

        $this->hasMany(
            'NotificationsUsers',
            [
                'joinTable' => 'notifications_users',
                'foreignKey' => 'user_id',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('structure_id')
            ->maxLength('structure_id', 50)
            ->requirePresence('structure_id', 'create')
            ->allowEmptyString('structure_id', null, false);

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->allowEmptyString('active', null, false);

        $validator
            ->scalar('civility')
            ->maxLength('civility', 4)
            ->allowEmptyString('civility', null, true);

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 50)
            ->requirePresence('lastname', 'create')
            ->allowEmptyString('lastname', null, false);

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 50)
            ->requirePresence('firstname', 'create')
            ->allowEmptyString('firstname', null, false);

        $validator
            ->scalar('username')
            ->maxLength('username', 50)
            ->requirePresence('username', 'create')
            ->allowEmptyString('username', null, false);

        $validator
            ->email('email')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'E-mail must be valid',
            ])
            ->requirePresence('email', 'create')
            ->allowEmptyString('email', null, false);

        $validator
            ->scalar('phone')
            ->maxLength('phone', 15)
            ->allowEmptyString('phone')
            ->setProvider('fr', 'Cake\Localized\Validation\FrValidation')
            ->add(
                'phone',
                'checkPhone',
                [
                    'rule' => 'phone',
                    'provider' => 'fr',
                    'message' => "Le numéro de téléphone n'est pas valide",
                ]
            );

        $validator
            ->boolean('data_security_policy')
            ->requirePresence('data_security_policy', 'create')
            ->allowEmptyString('data_security_policy', null, false);

        $validator
            ->integer('id_external')
            ->allowEmptyString('id_external', null, true);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['keycloak_id']));
        $rules->add($rules->isUnique(['id_external'], ['allowMultipleNulls' => true]));
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));
        $rules->addUpdate(function ($entity) {
            return $this->canByPassEditWhenHasProvisioning($entity);
        }, 'userCantBeEdited', [
            'errorField' => 'id',
            'message' => 'Cannot edit a user without at least the admin role',
        ]);

        $rules->addDelete(function ($entity) {
            return $this->canByPassDeleteWhenHasProvisioning($entity);
        }, 'userCantBeDeleted', [
            'errorField' => 'id',
            'message' => 'Cannot delete a user in use',
        ]);

        return $rules;
    }

    /**
     * @param \Cake\Event\Event $event Foo
     * @param \Cake\Datasource\EntityInterface $entity Foo
     * @param \ArrayObject $options Foo
     * @return void Foo
     */
    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity->isNew()) {
            $this->addNotifications($entity);
        }
    }

    /**
     * Add Notifications to user
     *
     * @param \Cake\Datasource\EntityInterface $entity user entity
     * @param bool $all true
     * @return void
     */
    private function addNotifications(EntityInterface $entity, $all = true)
    {
        if ($all) {
            $NotificationsUsers = $this->fetchTable('NotificationsUsers');
            $Notifications = $this->fetchTable('Notifications');
            $notificationsIds = Hash::extract(
                $Notifications
                    ->find('all')
                    ->all()
                    ->extract('id')
                    ->toArray(),
                '{n}'
            );
            foreach ($notificationsIds as $notificationsId) {
                $notificationUser = $NotificationsUsers->newEntity(
                    ['user_id' => $entity->id,
                        'notification_id' => $notificationsId]
                );
                $NotificationsUsers->save($notificationUser);
            }
        }
    }

    /**
     * Finds super admin through all users to set a new virtual field,
     * To specify connected user role context
     *
     * @param \Cake\ORM\Query $query query
     * @param array $options options
     * @return \Cake\ORM\Query
     */
    public function findAllUsersSuperAdminDisabledOrNot(Query $query, array $options): Query
    {
        return $query->find('all')->select()->contain(['Roles'])->formatResults(function ($results) {

            return $results->map(function (EntityInterface $user) {
                    $user->setVirtual(['current_user_is_super_admin' => 'true'], true);

                return $user;
            });
        });
    }

    /**
     * @param \Cake\ORM\Entity $entity entity
     * @return bool|mixed|string
     */
    private function canByPassEditWhenHasProvisioning(Entity $entity)
    {
        return HAS_PROVISIONING ?: $entity->availableActions['can_be_edited']['value'];
    }

    /**
     * @param \Cake\ORM\Entity $entity entity
     * @return bool|mixed|string
     */
    private function canByPassDeleteWhenHasProvisioning(Entity $entity)
    {
        return HAS_PROVISIONING ?: $entity->availableActions['can_be_deleted']['value'];
    }
}
