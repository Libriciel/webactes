<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Workflows Model
 *
 * @property \App\Model\Table\WorkflowsTable|\App\Model\Table\BelongsTo $Workflows
 * @property \App\Model\Table\OrganizationsTable|\App\Model\Table\BelongsTo $Organizations
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @method \App\Model\Table\Workflow get($primaryKey, $options = [])
 * @method \App\Model\Entity\Workflow newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Workflow[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Workflow|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Workflow|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Workflow patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Workflow[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Workflow findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WorkflowsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('workflows');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');
        $this->addBehavior('SoftDeletion');

        $this->belongsTo(
            'Organizations',
            [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Structures',
            [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Projects',
            [
            'foreignKey' => 'workflow_id',
            'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        $validator
            ->scalar('description')
            ->maxLength('description', 500)
            ->requirePresence('description', false)
            ->allowEmptyString('description');

        $validator
            ->scalar('file')
            ->maxLength('file', 50)
            ->requirePresence('file', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating application integrity.
     * Name must be unique for a given structure and for not soft deleted entries
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        // name unicity for a given structure and not deleted : creation
        $rules->addCreate(
            function ($entity, $options) {

                return $this->find()->where(
                    [
                        'name' => $entity->name,
                        'structure_id' => $entity->structure_id,
                        'deleted IS NULL',
                    ]
                )->count() === 0;
            },
            'duplicate',
            [
                'errorField' => 'name',
                'message' => 'duplicate name for this structure',
            ]
        );

        // name unicity for a given structure and not deleted : edition
        $rules->addUpdate(
            function ($entity, $options) {

                return $this->find()->where(
                    [
                        'name' => $entity->name,
                        'id !=' => $entity->id, // except for itself
                        'structure_id' => $entity->structure_id,
                        'deleted IS NULL',
                    ]
                )->count() === 0;
            },
            'duplicate',
            [
                'errorField' => 'name',
                'message' => 'duplicate name for this structure',
            ]
        );

        // deletion forbidden if not deletable
        $rules->addUpdate(
            function ($entity, $options) {

                return $entity->deleted === null || $entity->is_deletable;
            },
            'workflowCantBeDelete',
            [
            'errorField' => 'id',
            'message' => 'Cannot delete a workflow with instanced projects',
            ]
        );

        return $rules;
    }
}
