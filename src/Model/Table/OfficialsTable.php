<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Officials Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @method \App\Model\Table\Official get($primaryKey, $options = [])
 * @method \App\Model\Entity\Official newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Official[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Official|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Official|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Official patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Official[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Official findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OfficialsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('officials');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('structure_id')
            ->requirePresence('structure_id', 'create');

        $validator
            ->scalar('civility')
            ->maxLength('civility', 4)
            ->requirePresence('civility', 'create')
            ->allowEmptyString('civility', null, false);

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 50)
            ->requirePresence('lastname', 'create')
            ->allowEmptyString('lastname', null, false);

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 50)
            ->requirePresence('firstname', 'create')
            ->allowEmptyString('firstname', null, false);

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->allowEmptyString('email', null, false);

        $validator
            ->scalar('adresse')
            ->maxLength('adresse', 100)
            ->allowEmptyString('adresse');

        $validator
            ->scalar('address_supplement')
            ->maxLength('address_supplement', 100)
            ->allowEmptyString('address_supplement');

        $validator
            ->scalar('post_code')
            ->maxLength('post_code', 5)
            ->allowEmptyString('post_code');

        $validator
            ->scalar('city')
            ->maxLength('city', 100)
            ->allowEmptyString('city');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 15)
            ->allowEmptyString('phone');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['structure_id', 'email']));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        return $rules;
    }
}
