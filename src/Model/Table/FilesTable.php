<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Structure;
use ArrayObject;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use Exception;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid;
use function App\mimetype as mimetype;
use function App\uuid4 as uuid4;

/**
 * Files Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\MaindocumentsTable|\App\Model\Table\BelongsTo $Maindocuments
 * @property \App\Model\Table\AnnexesTable|\App\Model\Table\BelongsTo $Annexes
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\BelongsTo $Containers
 * @method \App\Model\Table\File get($primaryKey, $options = [])
 * @method \App\Model\Entity\File newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\File[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\File|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\File patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\File[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\File findOrCreate($search, callable $callback = null, $options = [])
 */
class FilesTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
        parent::initialize($config);

        $this->setTable('files');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('LimitToStructure');
        $this->addBehavior('Timestamp');
        //$this->addBehavior('Permission');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Maindocuments',
            [
                'foreignKey' => 'maindocument_id',
            ]
        );
        $this->belongsTo(
            'Annexes',
            [
                'foreignKey' => 'annex_id',
            ]
        );
        $this->belongsTo(
            'GenerateTemplates',
            [
                'foreignKey' => 'generate_template_id',
            ]
        );
        $this->belongsTo(
            'DraftTemplates',
            [
                'foreignKey' => 'draft_template_id',
            ]
        );
        $this->belongsTo(
            'ProjectTexts',
            [
                'foreignKey' => 'project_text_id',
            ]
        );
        $this->belongsTo(
            'AttachmentsSummons',
            [
                'foreignKey' => 'attachment_summon_id',
            ]
        );
    }

    /**
     * Get real MIME type, override the "mimetype" before creating the entity.
     *
     * @param \Cake\Event\Event $event The calling event
     * @param \ArrayObject $data The available data
     * @param \ArrayObject $options Marshalling options
     * @return void
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (isset($data['path'])) {
            $data['mimetype'] = mimetype($data['path']);
        }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 512)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        $validator
            ->scalar('path')
            ->maxLength('path', 512)
            ->requirePresence('path', 'create')
            ->allowEmptyString('path', null, false);

        $validator
            ->scalar('mimetype')
            ->maxLength('mimetype', 256)
            ->requirePresence('mimetype', 'create')
            ->allowEmptyString('mimetype', null, false);

        $validator
            ->allowEmptyString('size');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating application integrity.
     * Adds a rule for validating the MIME type for records linked to Maindocuments or Annexes.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['annex_id'], 'Annexes'));
        $rules->add($rules->existsIn(['maindocument_id'], 'Maindocuments'));
        $rules->add($rules->existsIn(['generate_template_id'], 'GenerateTemplates'));
        $rules->add($rules->existsIn(['draft_template_id'], 'DraftTemplates'));
        $rules->add($rules->existsIn(['project_text_id'], 'ProjectTexts'));
        $rules->add($rules->existsIn(['attachment_summon_id'], 'AttachmentsSummons'));

        $rules->add(
            function ($entity) {
                if (empty($entity->maindocument_id) === false) {
                    $accepted = (array)Configure::read('mainDocumentAcceptedTypes');
                } elseif (empty($entity->annex_id) === false) {
                    $accepted = (array)Configure::read('annexeAcceptedTypes');
                } elseif (empty($entity->generate_template_id) === false) {
                    $accepted = (array)Configure::read('generateTemplateAcceptedTypes');
                } elseif (empty($entity->draft_template_id) === false) {
                    $accepted = (array)Configure::read('draftTemplateAcceptedTypes');
                } elseif (empty($entity->project_text_id) === false) {
                    $accepted = (array)Configure::read('projetTextAcceptedTypes');
                } elseif (empty($entity->attachment_summon_id) === false) {
                    $accepted = (array)Configure::read('attachmentSummonAcceptedTypes');
                } else {
                    $accepted = [];
                }

                return in_array($entity->mimetype, $accepted, true)
                    ? true
                    : sprintf(
                        '"%s" MIME type is not amongst the accepted types: "%s"',
                        $entity->mimetype,
                        implode('"", "', $accepted)
                    );
            },
            '_mimetype',
            ['errorField' => 'mimetype']
        );

        return $rules;
    }

    /**
     * Override the save method to save the file on the filesystem and put its path in the "path" field.
     *
     * @param \Cake\Datasource\EntityInterface $entity The entity to save
     * @param array $options Save options
     * @return \Cake\Datasource\EntityInterface|false
     * @throws \Cake\ORM\Exception\RolledbackTransactionException If the transaction is aborted in the afterSave event.
     */
    public function save(EntityInterface $entity, $options = [])
    {
        if (isset($options['inFS']) && $options['inFS']) {
            return parent::save($entity, $options);
        }

        $work = [
            'annex_id' => Hash::get($entity, 'annex_id'),
            'maindocument_id' => Hash::get($entity, 'maindocument_id'),
            'create_dir' => null,
            'path' => null,
        ];

        if (empty($work['maindocument_id']) === false) {
            $container_id = $this->Maindocuments->get($entity->maindocument_id)->container_id;
        } elseif (empty($work['annex_id']) === false) {
            $container_id = $this->Annexes->get($entity->annex_id)->container_id;
        } else {
            $msgstr = 'File entity has both maindocument_id and annex_id empty attributes';
            $entity->setError('maindocument_id', $msgstr);
            $entity->setError('annex_id', $msgstr);

            return false;
        }

        $organizationId = $this->Structures->get($entity->structure_id)->organization_id;

        $folder = sprintf(
            WORKSPACE . DS . '%d' . DS . '%d' . DS . '%d' . DS . '%d',
            $organizationId,
            $entity->structure_id,
            date('Y'),
            $container_id
        );

        if (file_exists($folder) === false) {
            $work['create_dir'] = $folder;
            $res = mkdir($folder, 0777, true);
            if (!$res) {
                $entity->setError('id', sprintf('impossible de creer le dossier %s', $folder));

                return false;
            }
        }

        $work['path'] = $folder . DS . uuid4();
        $res = rename($entity->path, $work['path']);
        if (!$res) {
            $entity->setError('id', sprintf('impossible de copier dans le dossier %s', $work['path']));

            return false;
        }

        $entity->path = $work['path'];
        $entity->path = $work['path'];

        $result = parent::save($entity, $options);

        if ($result === false || $entity->hasErrors()) {
            if (empty($work['path']) === false) {
                unlink($work['path']);
            }

            return false;
        }

        return true;
    }

    /**
     * Save file in fs
     *
     * @param array $uploadedFile uploaded file
     * @param array $linkedTo ex : [maindocument_id => 3]
     * @param int $containerId container_id
     * @param int $structureId structure_id
     * @return bool
     * @access  public
     * @created 11/03/2019
     * @version V0.0.9
     */
    public function saveInFs(array $uploadedFile, array $linkedTo, $containerId, $structureId)
    {
        // put the real collectivity and structure id
        $structure_id = $structureId;

        $folder = $this->folderPath($this->Structures->get($structureId), $linkedTo);

        if (!file_exists($folder) && !mkdir($folder, 0755, true)) {
            throw new InvalidArgumentException('impossible de créer le repertoire');
        }

        //get uuid
        try {
            $uuid = Uuid::uuid4()->toString();
        } catch (Exception $e) {
            throw new InvalidArgumentException('Erreur de création de l\'uuid');
        }

        // save file in database
        $data = [
            'structure_id' => $structure_id,
            'path' => $folder . DS . $uuid,
            'mimetype' => $uploadedFile['type'],
            'name' => $uploadedFile['name'],
            'size' => $uploadedFile['size'],
        ];
        $data += $linkedTo;

        $res = '';
        if ($uploadedFile['tmp_name']) {
            $res = rename($uploadedFile['tmp_name'], $folder . DS . $uuid);
        } else {
            unset($uploadedFile['tmp_name']);
        }

        if (!$res) {
            throw new InvalidArgumentException('Impossible de déplacer le fichier');
        }

        $file = $this->newEntity($data);

        if (!$this->save($file, ['atomic' => false, 'inFS' => true])) {
            Log::info(var_export($file->getErrors(), true));

            throw new InvalidArgumentException('Impossible de sauvegarder le fichier');
        }

        $this->fileCallback($file);

        return $file;
    }

    /**
     * Save file in fs
     *
     * @param int $id Foo
     * @param array $uploadedFilesStream Foo
     * @param int $structureId Foo
     * @return \App\Model\Entity\File|bool
     * @access  public
     * @created 11/03/2019
     * @version V0.0.9
     */
    public function saveInFsStream(int $id, array $uploadedFilesStream, int $structureId)
    {
        $file = $this->get($id, ['limitToStructure' => $structureId]);
        $linkedTo = $this->fileLinkedTo($file);
        $folder = $this->folderPath($this->Structures->get($structureId), $linkedTo);
        if (!file_exists($folder) && !mkdir($folder, 0777, true)) {
            throw new InvalidArgumentException('impossible de créer le repertoire');
        }
        file_put_contents($file->path, $uploadedFilesStream['contents']);

        // save file in database
        $file = $this->patchEntity($file, ['size' => $uploadedFilesStream['size']]);
        if (!$this->save($file, ['atomic' => false, 'inFS' => true])) {
            throw new InvalidArgumentException('Impossible de sauvegarder le fichier');
        }

        $this->fileCallback($file);

        return $file;
    }

    /**
     * Save file in fs
     *
     * @param int $id Foo
     * @param string $sourceKey Foo
     * @param int $sourceKeyId Foo
     * @param array $linkedTo Foo
     * @param int $structureId Foo
     * @return \App\Model\Entity\File|bool
     */
    public function saveInFsDuplicateFile(int $id, $sourceKey, $sourceKeyId, array $linkedTo, int $structureId)
    {
        $file = $this->find()
            ->select(['path', 'mimetype', 'size', 'mimetype', 'name'])
            ->where([
                "$sourceKey" => $sourceKeyId,
            ])
            ->firstOrFail();

        //get uuid
        try {
            $uuid = Uuid::uuid4()->toString();
        } catch (Exception $e) {
            throw new InvalidArgumentException('Erreur de création de l\'uuid');
        }

        copy($file->path, TMP . $uuid);

        $copyFileData = [
            'name' => $file->name,
            'tmp_name' => TMP . $uuid,
            'path' => TMP . $uuid,
            'type' => $file->mimetype,
            'size' => $file->size,
        ];

        return $this->saveInFs(
            $copyFileData,
            $linkedTo,
            DS . array_keys($linkedTo)[0] . DS . $id,
            $structureId
        );
    }

    /**
     * Save file in fs
     *
     * @param array $uploadedFilesStream Foo
     * @param array $linkedTo Foo
     * @param int|string $containerId Foo
     * @param int $structureId Foo
     * @return \App\Model\Entity\File|bool
     */
    public function saveInFsNewStream(array $uploadedFilesStream, array $linkedTo, $containerId, int $structureId)
    {
        //get uuid
        try {
            $uuid = Uuid::uuid4()->toString();
        } catch (Exception $e) {
            throw new InvalidArgumentException('Erreur de création de l\'uuid');
        }

        file_put_contents(TMP . $uuid, $uploadedFilesStream['contents']);

        $uploadedFile = [
            'name' => $uploadedFilesStream['name'],
            'tmp_name' => TMP . $uuid,
            'path' => TMP . $uuid,
            'type' => $uploadedFilesStream['mimetype'],
            'size' => $uploadedFilesStream['size'],
        ];

        return $this->saveInFs(
            $uploadedFile,
            $linkedTo,
            $containerId,
            $structureId
        );
    }

    /**
     * @param \App\Model\Entity\Structure $entity Foo
     * @param array $linkedTo Foo
     * @return string
     */
    private function folderPath(Structure $entity, array $linkedTo): string
    {
        $linkedToKey = array_keys($linkedTo)[0];
        $linkedToValue = array_values($linkedTo)[0];

        switch ($linkedToKey) {
            case 'maindocument_id':
            case 'annex_id':
            case 'attachment_summon_id':
            case 'project_text_id':
                return WORKSPACE . DS . $entity->organization_id . DS . $entity->id
                    . DS . date('Y') . DS . $linkedToKey . DS . $linkedToValue;
            case 'generate_template_id':
            case 'draft_template_id':
                return WORKSPACE . DS . $entity->organization_id . DS . $entity->id
                    . DS . $linkedToKey . DS . $linkedToValue;
            default:
                throw new InvalidArgumentException('Erreur de la récupération de la key');
        }
    }

    /**
     * @param \Cake\Datasource\EntityInterface $entity Foo
     * @return array
     */
    private function fileLinkedTo(EntityInterface $entity): array
    {
        $fileKeys = [
            'maindocument_id',
            'annex_id',
            'generate_template_id',
            'draft_template_id',
            'project_text_id',
            'attachment_summon_id',
        ];
        foreach ($fileKeys as $fileKey) {
            if (!empty(Hash::get($entity, $fileKey))) {
                return ["$fileKey" => $entity->{$fileKey}];
            }
        }

        throw new InvalidArgumentException('Erreur de la récupération de la key');
    }

    /**
     * @param \Cake\Datasource\EntityInterface $entity Foo
     * @return void
     */
    private function fileCallback(EntityInterface $entity): void
    {
        $fileKey = array_keys($this->fileLinkedTo($entity))[0];
        switch ($fileKey) {
            case 'maindocument_id':
                $maindocument = $this->Maindocuments->get(Hash::get($entity, 'maindocument_id'));
                $containers = $this->fetchTable('Containers');
                $container = $containers->get($maindocument->container_id);
                if ($containers->isOnPastell($container->id)) {
                    $containers->sendEnventPacthPastell($container->id);
                }
                break;
            case 'annex_id': // ????
                $annex = $this->Annexes->get(Hash::get($entity, 'annex_id'));
                $containers = $this->fetchTable('Containers');
                $container = $containers->get($annex->container_id);
                if ($containers->isOnPastell($container->id)) {
                    $containers->sendEnventPacthPastell($container->id);
                }
                break;
            case 'attachment_summon_id':
            case 'project_text_id':
                break;
        }
    }

    /**
     * @param \Cake\Event\Event $event The afterSave event that was fired
     * @param \Cake\ORM\Entity $entity The entity being saved
     * @return void
     */
    public function afterDelete(Event $event, Entity $entity)
    {
        if (file_exists($entity->path)) {
            unlink($entity->path);
        }
    }
}
