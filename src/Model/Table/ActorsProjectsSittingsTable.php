<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * ActorsProjectsSittings Model
 *
 * @property \App\Model\Table\ActorsTable&\Cake\ORM\Association\BelongsTo $Actors
 * @property \App\Model\Table\ProjectsTable&\Cake\ORM\Association\BelongsTo $Projects
 * @property \App\Model\Table\SittingsTable&\Cake\ORM\Association\BelongsTo $Sittings
 * @property \App\Model\Table\StructuresTable&\Cake\ORM\Association\BelongsTo $Structures
 * @method \App\Model\Entity\ActorsProjectsSitting newEmptyEntity()
 * @method \App\Model\Entity\ActorsProjectsSitting newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ActorsProjectsSitting[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ActorsProjectsSittingsTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('actors_projects_sittings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Actors', [
            'className' => 'Actors',
            'foreignKey' => 'actor_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Representatives', [
            'className' => 'Actors',
            'foreignKey' => 'mandataire_id',
        ]);
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Sittings', [
            'foreignKey' => 'sitting_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('is_present')
            ->requirePresence('is_present', 'create')
            ->notEmptyString('is_present');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['actor_id'], 'Actors'), ['errorField' => 'actor_id']);
        $rules->add($rules->existsIn(['mandataire_id'], 'Actors'), ['errorField' => 'mandataire_id']);
        $rules->add($rules->existsIn(['project_id'], 'Projects'), ['errorField' => 'project_id']);
        $rules->add($rules->existsIn(['sitting_id'], 'Sittings'), ['errorField' => 'sitting_id']);

        return $rules;
    }

    /**
     * @param int $projectId project_id
     * @param int $sittingId sitting_id
     * @return bool
     * @throws \Exception
     */
    public function updateFromPrecedentRank(int $projectId, int $sittingId)
    {
        $ContainerSittings = $this->fetchTable('ContainersSittings');
        $Containers = $this->fetchTable('Containers');

        $containerSittingToUpdate = $ContainerSittings->find()
            ->select(['rank', 'container_id'])
            ->where([
                'sitting_id' => $sittingId,
                'container_id' => $Containers->find()
                    ->where(['project_id' => $projectId])
                    ->first()
                    ->get('id'),
            ])
            ->first()
            ->toArray();

        // On a le rank du projet a update
        // On va prendre la liste des votants du rank précédent.
        $projectIdFrom = $Containers->find()
            ->select('project_id')
            ->where(['id' => $ContainerSittings
                ->find()
                ->select('container_id')
                ->where([
                    'sitting_id' => $sittingId,
                    'rank' => $containerSittingToUpdate['rank'] - 1,
                ])
                ->first()
                ->get('container_id')])
            ->first()
            ->get('project_id');

        $actorProjectSittingAsBase = $this
            ->find()
            ->select(['actor_id', 'project_id', 'is_present', 'mandataire_id'])
            ->where([
                'sitting_id' => $sittingId,
                'project_id' => $projectIdFrom,
            ])
            ->toArray();

        $voters = [];
        foreach ($actorProjectSittingAsBase as $voter) {
            array_push($voters, [
                'actor_id' => $voter['actor_id'],
                'sitting_id' => $sittingId,
                'is_present' => $voter['is_present'],
                'project_id' => $projectId,
                'mandataire_id' => $voter['mandataire_id'],
            ]);
        }

        $entitiesToUpdate = $this
            ->find()
            ->where([
                'sitting_id' => $sittingId,
                'project_id' => $projectId,
            ]);
        $entities = [];
        foreach ($entitiesToUpdate as $entityToUpdate) {
            $dataUpdated = Hash::extract($voters, '{n}[actor_id=' . $entityToUpdate['actor_id'] . ']');

            $entityToUpdate = $this->patchEntity($entityToUpdate, $dataUpdated[0]);

            array_push($entities, $entityToUpdate);
        }
        if (!empty($entities) && $this->saveMany($entities)) {
            return true;
        }

        return false;
    }

    /**
     * Retourne un query contenant la liste de présence du projet de rang précédent d'un projet donné pour une séance
     * donnée.
     *
     * @param int $sittingId L'id de la séance (sittings.id)
     * @param int $projectId L'id du projet (projects.id)
     * @return \Cake\ORM\Query
     */
    public function getPrevious(int $sittingId, int $projectId)
    {
        return $this
            ->find()
            ->innerJoinWith('Projects')
            ->innerJoinWith('Projects.Containers')
            ->innerJoinWith('Projects.Containers.ContainersSittings')
            ->where([
                'ActorsProjectsSittings.sitting_id' => $sittingId,
                'ContainersSittings.rank + 1 =' => $this->Projects->Containers->ContainersSittings
                    ->getContainersSittingsRankSubquery($sittingId, $projectId),
            ]);
    }

    /**
     * @param int $currentStructureId currentStructureId
     * @param int $sittingId sittingId
     * @param int $projectId projectId
     * @param array $votersList votersList
     * @return \Cake\ORM\Query Redirects on successful add, renders view otherwise.
     * @throws \Exception
     */
    public function initializeAttendanceList(int $currentStructureId, int $sittingId, int $projectId, array $votersList)
    {
        $actorsIdList = Hash::extract($votersList, '{n}.actor_id');
        $actorsList = $this->fetchTable('Actors')
            ->find('list', [
                'keyField' => 'id',
                'valueField' => function ($actor) {
                    return $actor->get('id');
                },
            ])
            ->where(
                [
                    'id IN ' => $actorsIdList,
                    'active' => true,
                    'structure_id' => $currentStructureId,
                ]
            )
            ->all()
            ->toList();

        foreach ($votersList as $index => $voter) {
            if (!in_array($votersList[$index]['actor_id'], $actorsList)) {
                unset($votersList[$index]);
            }
        }

        $actorsListProjectsSitting = $this->newEntities($votersList);

        $this->saveMany($actorsListProjectsSitting);

        return $this
            ->find('all', ['limitToStructure' => $currentStructureId])
            ->contain(['Actors'])
            ->where(['project_id' => $projectId, 'sitting_id' => $sittingId]);
    }

    /**
     * @param int $currentStructureId currentStructureId
     * @param int $projectId projectId
     * @return array
     */
    public function getMissingVoters(int $currentStructureId, int $projectId)
    {
        return $this->find('all', ['limitToStructure' => $currentStructureId])
            ->where(['is_present' => false, 'project_id' => $projectId])
            ->enableHydration(false)
            ->toArray();
    }
}
