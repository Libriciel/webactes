<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Log\Log;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Generationerrors Model
 *
 * @method \App\Model\Entity\Generationerror newEmptyEntity()
 * @method \App\Model\Entity\Generationerror newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Generationerror[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Generationerror get($primaryKey, $options = [])
 * @method \App\Model\Entity\Generationerror findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Generationerror patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Generationerror[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Generationerror|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Generationerror saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Generationerror[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Generationerror[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Generationerror[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Generationerror[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class GenerationerrorsTable extends Table
{
    /**
     * @var \App\Model\Table\GenerationerrorsTable|null
     */
    protected static $instance;

    /**
     * The possible values for the type column.
     *
     * @var string[]
     */
    public static $types = ['act', 'convocation', 'deliberations_list', 'verbal_trial', 'project', 'executive_summary'];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('generationerrors');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->inList('type', static::$types)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->integer('foreign_key')
            ->requirePresence('foreign_key', 'create')
            ->notEmptyString('foreign_key');

        $validator
            ->scalar('message')
            ->requirePresence('message', 'create')
            ->notEmptyString('message');

        return $validator;
    }

    /**
     * Static utility function to persist records in the table.
     *
     * @param string $type The type of generation
     * @param int $foreignKey The foreign key for the generation
     * @param string $message The error message
     * @return bool
     */
    public static function persist(string $type, int $foreignKey, string $message): bool
    {
        if (static::$instance === null) {
            static::$instance = TableRegistry::getTableLocator()->get('Generationerrors');
        }

        $data = ['type' => $type, 'foreign_key' => $foreignKey];
        $generationerror = static::$instance
            ->find()
            ->where($data)
            ->first();

        $generationerror = !empty($generationerror)
            ? $generationerror
            : static::$instance->newEntity($data);
        $generationerror->set(compact('message'));

        if (static::$instance->save($generationerror, ['atomic' => false]) === false) {
            $format = 'Impossible d\'enregistrer l\'entrée de generationerrors pour type=%s,'
                . ' foreign_key=%d et message=%s (%s)';
            $message = sprintf($format, $type, $foreignKey, $message, print_r($generationerror->getErrors(), true));
            Log::critical($message);

            return false;
        }

        return true;
    }

    /**
     * Static utility function to cleanup records in the table.
     *
     * @param string $type The type of generation
     * @param int $foreignKey The foreign key for the generation
     * @return bool
     */
    public static function cleanup(string $type, int $foreignKey): bool
    {
        if (static::$instance === null) {
            static::$instance = TableRegistry::getTableLocator()->get('Generationerrors');
        }

        return static::$instance->deleteAll(['type' => $type, 'foreign_key' => $foreignKey]) > 0;
    }
}
