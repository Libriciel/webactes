<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\I18n\FrozenTime;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Structures Model
 *
 * @property \App\Model\Table\OrganizationsTable|\App\Model\Table\BelongsTo $Organizations
 * @property \App\Model\Table\ActorsTable|\App\Model\Table\HasMany $Actors
 * @property \App\Model\Table\ActsTable|\App\Model\Table\HasMany $Acts
 * @property \App\Model\Table\AnnexesTable|\App\Model\Table\HasMany $Annexes
 * @property \App\Model\Table\AttachmentsTable|\App\Model\Table\HasMany $Attachments
 * @property \App\Model\Table\ClassificationsTable|\App\Model\Table\HasMany $Classifications
 * @property \App\Model\Table\Connecteurs|\App\Model\Table\HasOne $Connecteurs
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\HasMany $Containers
 * @property \App\Model\Table\ConvocationsTable|\App\Model\Table\HasMany $Convocations
 * @property \App\Model\Table\DposTable|\App\Model\Table\HasMany $Dpos
 * @property \App\Model\Table\FieldsTable|\App\Model\Table\HasMany $Fields
 * @property \App\Model\Table\FilesTable|\App\Model\Table\HasMany $Files
 * @property \App\Model\Table\HistoriesTable|\App\Model\Table\HasMany $Histories
 * @property \App\Model\Table\MaindocumentsTable|\App\Model\Table\HasMany $Maindocuments
 * @property |\Cake\ORM\Association\HasMany $Matieres
 * @property \App\Model\Table\MetadatasTable|\App\Model\Table\HasMany $Metadatas
 * @property \App\Model\Table\NaturesTable|\App\Model\Table\HasMany $Natures
 * @property \App\Model\Table\OfficialsTable|\App\Model\Table\HasMany $Officials
 * @property |\Cake\ORM\Association\HasMany $Pastellfluxactions
 * @property |\Cake\ORM\Association\HasMany $Pastellfluxs
 * @property |\Cake\ORM\Association\HasMany $Pastellfluxtypes
 * @property \App\Model\Table\ProjectsTable|\App\Model\Table\HasMany $Projects
 * @property \App\Model\Table\RolesTable|\App\Model\Table\HasMany $Roles
 * @property \App\Model\Table\ServicesTable|\App\Model\Table\HasMany $Services
 * @property \App\Model\Table\SittingsTable|\App\Model\Table\HasMany $Sittings
 * @property \App\Model\Table\TabsTable|\App\Model\Table\HasMany $Tabs
 * @property \App\Model\Table\TdtMessagesTable|\App\Model\Table\HasMany $TdtMessages
 * @property \App\Model\Table\TemplatesTable|\App\Model\Table\HasMany $Templates
 * @property \App\Model\Table\ThemesTable|\App\Model\Table\HasMany $Themes
 * @property \App\Model\Table\TypesactsTable|\App\Model\Table\HasMany $Typesacts
 * @property \App\Model\Table\TypesittingsTable|\App\Model\Table\HasMany $Typesittings
 * @property \App\Model\Table\ValuesTable|\App\Model\Table\HasMany $Values
 * @property \App\Model\Table\UsersTable|\App\Model\Table\BelongsToMany $Users
 * @property \App\Model\Table\SequencesTable|\App\Model\Table\HasMany $Sequences
 * @method \App\Model\Entity\Structure get($primaryKey, $options = [])
 * @method \App\Model\Entity\Structure newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Structure[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Structure|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Structure saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Structure patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Structure[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Structure findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StructuresTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('structures');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Organizations',
            [
                'foreignKey' => 'organization_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Actors',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Acts',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Annexes',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Attachments',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Classifications',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Containers',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Convocations',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Dpos',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Fields',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Files',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Histories',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Maindocuments',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Matieres',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Metadatas',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Natures',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Officials',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Pastellfluxactions',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Pastellfluxs',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Pastellfluxtypes',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Projects',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Roles',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Services',
            [
                'foreignKey' => 'structure_id',
                'targetForeignKey' => 'service_id',
                'joinTable' => 'structures_services',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Sittings',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Systemlogs',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Tabs',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'TdtMessages',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Templates',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Themes',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Typesacts',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Typespiecesjointes',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Typesittings',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Typespiecesjointes',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Values',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );

        $this->hasOne(
            'Connecteurs',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );

        $this->belongsToMany(
            'Services',
            [
                'foreignKey' => 'structure_id',
                'targetForeignKey' => 'service_id',
                'joinTable' => 'structures_services',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Users',
            [
                'foreignKey' => 'structure_id',
                'targetForeignKey' => 'user_id',
                'joinTable' => 'structures_users',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Sequences',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'Counters',
            [
                'foreignKey' => 'structure_id',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->boolean('active')
            ->allowEmptyString('active', null, false);

        $validator
            ->scalar('logo')
            ->allowEmptyString('logo');

        $validator
            ->scalar('business_name')
            ->requirePresence('business_name', 'create')
            ->allowEmptyString('business_name', null, false)
            ->add(
                'business_name',
                [
                    'maxLength' => [
                        'rule' => ['maxLength', 100],
                        'last' => true,
                    ],
                    'unique' => [
                        'rule' => 'validateUnique',
                        'provider' => 'table',
                        'last' => true,
                    ],
                ]
            );

        $validator
            ->integer('id_orchestration')
            ->allowEmptyString('id_orchestration');

        $validator
            ->scalar('customname')
            ->maxLength('customname', 255)
            ->allowEmptyString('customname');

        $validator
            ->integer('id_external')
            ->allowEmptyString('id_external', null, true);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['business_name']));
        $rules->add($rules->isUnique(['id_external'], ['allowMultipleNulls' => true]));
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'));

        return $rules;
    }

    /**
     * création des types de séances par default
     *
     * @param int $structureId structureId
     * @return bool
     * @throws \Exception
     */
    public function addMinimumTypeSittings($structureId)
    {
        $typeSittings = [
            [
                'structure_id' => $structureId,
                'name' => 'Délibération',
            ],
            [
                'structure_id' => $structureId,
                'name' => 'Conseil Municipal',
            ],
            [
                'structure_id' => $structureId,
                'name' => 'Conseil Communautaire',
            ],
        ];

        $types = $this->Typesittings->newEntities($typeSittings);

        return $this->Typesittings->saveMany($types);
    }

    /**
     * Création des type d'actes par default
     * Attention La creation des type d'actes nécéssite d'avoir la matiere
     *
     * @param int $structureId structureId
     * @return bool
     * @throws \Exception
     */
    public function addMinimumTypesActs($structureId)
    {
        $Nature = $this->fetchTable('Natures');
        debug($Nature->find('all')->where(['structure_id' => $structureId])->toArray());
        $typesActs = [
            [
                'structure_id' => $structureId,
                'nature_id' => $Nature->find('all')
                    ->where(['codenatureacte' => '3', 'structure_id' => $structureId])
                    ->first()
                    ->id,
                'name' => 'Arrêté individuel',
                'istdt' => true,
                'active' => true,
                'isdeliberating' => true,
            ],
            [
                'structure_id' => $structureId,
                'nature_id' => $Nature->find('all')
                    ->where(['codenatureacte' => '6', 'structure_id' => $structureId])
                    ->first()
                    ->id,
                'name' => 'Autre',
                'istdt' => false,
                'active' => true,
                'isdeliberating' => false,
            ],
            [
                'structure_id' => $structureId,
                'nature_id' => $Nature->find('all')
                    ->where(['codenatureacte' => '2' , 'structure_id' => $structureId])
                    ->first()
                    ->id,
                'name' => 'Arrêté réglementaire',
                'istdt' => true,
                'active' => true,
                'isdeliberating' => false,
            ],
            [
                'structure_id' => $structureId,
                'nature_id' => $Nature->find('all')
                    ->where(['codenatureacte' => '4', 'structure_id' => $structureId])
                    ->first()
                    ->id,
                'name' => 'Contrat et convention',
                'istdt' => true,
                'active' => true,
                'isdeliberating' => false,
            ],
            [
                'structure_id' => $structureId,
                'nature_id' => $Nature->find('all')
                    ->where(['codenatureacte' => '1', 'structure_id' => $structureId])
                    ->first()
                    ->id,
                'name' => 'Délibération',
                'istdt' => true,
                'active' => true,
                'isdeliberating' => true,
            ],
        ];

        $types = $this->Typesacts->newEntities($typesActs);

        return $this->Typesacts->saveMany($types);
    }

    /**
     * Add default pastell flux
     *
     * @param int $structureId structureId
     * @return \App\Model\Table\EntityInterface|false
     */
    public function addMinimumPastellFluxTypes($structureId)
    {
        $data = [
            [
                'organization_id' => 1,
                'structure_id' => $structureId,
                'active' => true,
                'name' => 'actes-generique',
                'description' => 'Actes générique de test',
            ],
            [
                'organization_id' => 1,
                'structure_id' => $structureId,
                'active' => true,
                'name' => 'mailsec',
                'description' => 'Mail securisé',
            ],
        ];

        $fluxType = $this->Pastellfluxtypes->newEntities($data);

        return $this->Pastellfluxtypes->saveMany($fluxType);
    }

    /**
     * Add default roles flux
     *
     * @return array
     */
    public function addMinimumRoles()
    {
        return [
            [
                'name' => RolesTable::SUPER_ADMINISTRATOR,
                'created' => new FrozenTime(),
                'modified' => new FrozenTime(),
            ],
            [
                'name' => RolesTable::ADMINISTRATOR,
                'created' => new FrozenTime(),
                'modified' => new FrozenTime(),
            ],
            [
                'name' => RolesTable::VALIDATOR_REDACTOR_NAME,
                'created' => new FrozenTime(),
                'modified' => new FrozenTime(),
            ],
            [
                'name' => RolesTable::GENERAL_SECRETARIAT,
                'created' => new FrozenTime(),
                'modified' => new FrozenTime(),
            ],
            [
                'name' => RolesTable::FUNCTIONNAL_ADMINISTRATOR,
                'created' => new FrozenTime(),
                'modified' => new FrozenTime(),
            ],
        ];
    }

    /**
     * Retourne les structures d'une organization
     *
     * @param int $id organizationId
     * @return array
     */
    public function findStructuresOrganization(int $id)
    {
        return $this->find()->select()->where(['organization_id' => $id])->toArray();
    }

    /**
     * Return a structure's organization_id or NULL if not found.
     *
     * @param int $id The structure id
     * @return int|null
     */
    public function getOrganizationId($id)
    {
        $record = $this->find()
            ->select(['organization_id'])
            ->where(['id' => $id])
            ->firstOrFail();

        return Hash::get($record, 'organization_id');
    }
}
