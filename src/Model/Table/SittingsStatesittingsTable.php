<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SittingsStatesittings Model
 *
 * @property \App\Model\Table\SittingsTable|\App\Model\Table\BelongsTo $Sittings
 * @property \App\Model\Table\StatesittingsTable|\App\Model\Table\BelongsTo $Statesittings
 * @method \App\Model\Table\SittingsStatesitting get($primaryKey, $options = [])
 * @method \App\Model\Entity\SittingsStatesitting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SittingsStatesitting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SittingsStatesitting|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SittingsStatesitting saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SittingsStatesitting patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SittingsStatesitting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SittingsStatesitting findOrCreate($search, callable $callback = null, $options = [])
 */
class SittingsStatesittingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sittings_statesittings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo(
            'Sittings',
            [
                'foreignKey' => 'sitting_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Statesittings',
            [
                'foreignKey' => 'statesitting_id',
                'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['sitting_id'], 'Sittings'));
        $rules->add($rules->existsIn(['statesitting_id'], 'Statesittings'));

        return $rules;
    }

    /**
     * Retourne une (sous) query contenant le champ id de la table sittings_statesittings pour le dernier enregistrement
     * d'une séance donnée (tri par id de sittings_statesittings descendant).
     *
     * @param string $alias L'alias de la clé primaire de la table SittingsStatesittings de la requête principale
     * @return \Cake\ORM\Query
     */
    public function getLastBySittingsIdSubquery(string $alias = 'SittingsStatesittings.sitting_id')
    {
        return $this
            ->find()
            ->select(['sittings_statesittings.id'])
            ->from(['sittings_statesittings'])
            ->where(function (QueryExpression $exp, Query $q) use ($alias) {
                return $exp->equalFields('sittings_statesittings.sitting_id', $alias);
            })
            ->orderDesc('sittings_statesittings.id')
            ->limit(1);
    }
}
