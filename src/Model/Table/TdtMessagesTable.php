<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TdtMessages Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\TdtsTable|\App\Model\Table\BelongsTo $Tdts
 * @method \App\Model\Table\TdtMessage get($primaryKey, $options = [])
 * @method \App\Model\Entity\TdtMessage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TdtMessage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TdtMessage|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TdtMessage|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TdtMessage patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TdtMessage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TdtMessage findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TdtMessagesTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('tdt_messages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Tdts',
            [
                'foreignKey' => 'tdt_id',
                'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('tdt_type')
            ->requirePresence('tdt_type', 'create')
            ->allowEmptyString('tdt_type', false);

        $validator
            ->integer('tdt_etat')
            ->allowEmptyString('tdt_etat');

        $validator
            ->date('date_message')
            ->allowEmptyDate('date_message');

        $validator
            ->allowEmptyString('tdt_data');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        return $rules;
    }
}
