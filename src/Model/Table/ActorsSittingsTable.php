<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ActorsSittings Model
 *
 * @property \App\Model\Table\ActorsTable|\App\Model\Table\BelongsTo $Actors
 * @property \App\Model\Table\SittingsTable|\App\Model\Table\BelongsTo $Sittings
 * @method \App\Model\Entity\ActorsSitting get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActorsSitting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ActorsSitting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActorsSitting|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsSitting|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsSitting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsSitting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsSitting findOrCreate($search, callable $callback = null, $options = [])
 */
class ActorsSittingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('actors_sittings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo(
            'Actors',
            [
                'foreignKey' => 'actor_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Sittings',
            [
                'foreignKey' => 'sitting_id',
                'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['actor_id'], 'Actors'));
        $rules->add($rules->existsIn(['sitting_id'], 'Sittings'));

        return $rules;
    }
}
