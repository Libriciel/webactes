<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Convocations Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\ActorsTable|\App\Model\Table\BelongsTo $Actors
 * @property \App\Model\Table\SittingsTable|\App\Model\Table\BelongsTo $Sittings
 * @property \App\Model\Table\AttachmentsTable|\App\Model\Table\BelongsToMany $Attachments
 * @method \App\Model\Table\Convocation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Convocation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Convocation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Convocation|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Convocation|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Convocation patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Convocation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Convocation findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ConvocationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('convocations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Actors',
            [
                'foreignKey' => 'actor_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Sittings',
            [
                'foreignKey' => 'sitting_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsToMany(
            'Attachments',
            [
                'foreignKey' => 'convocation_id',
                'targetForeignKey' => 'attachment_id',
                'joinTable' => 'convocations_attachments',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('summon_id')
            ->allowEmptyString('summon_id', 'create');

        $validator
            ->dateTime('date_send')
            ->allowEmptyDateTime('date_send', null, false);

        $validator
            ->dateTime('date_open')
            ->allowEmptyDateTime('date_open');

        $validator
            ->integer('jeton')
            ->allowEmptyString('jeton');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['actor_id'], 'Actors'));
        $rules->add($rules->existsIn(['sitting_id'], 'Sittings'));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        return $rules;
    }
}
