<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Log\Log;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use InvalidArgumentException;

/**
 * Maindocuments Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\BelongsTo $Containers
 * @property \App\Model\Table\FilesTable|\App\Model\Table\HasMany $Files
 * @method \App\Model\Table\Maindocument get($primaryKey, $options = [])
 * @method \App\Model\Entity\Maindocument newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Maindocument[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Maindocument|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Maindocument|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Maindocument patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Maindocument[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Maindocument findOrCreate($search, callable $callback = null, $options = [])
 */
class MaindocumentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('maindocuments');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('LimitToStructure');
        $this->addBehavior('NatureIdViaContainersTypesacts');
        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Structures',
            [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
            ]
        );

        $this->belongsTo(
            'Containers',
            [
            'foreignKey' => 'container_id',
            'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Files',
            [
            'foreignKey' => 'maindocument_id',
            'dependent' => true,
            ]
        );

        $this->belongsTo(
            'Typespiecesjointes',
            [
            'foreignKey' => false,
            'conditions' => [
                "{$this->getAlias()}.structure_id = Typespiecesjointes.structure_id",
                "{$this->getAlias()}.codetype = Typespiecesjointes.codetype",
                sprintf('Typespiecesjointes.nature_id IN (%s)', $this->getNatureIdSubquery()),
            ],
            'joinType' => 'LEFT OUTER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 512)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        $validator
            ->scalar('codetype')
            ->maxLength('codetype', 5)
            ->allowEmptyString('codetype');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['container_id'], 'Containers'));
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        return $rules;
    }

    /**
     * Sauvegarde du fichier principal
     *
     * @param  array $uploadedFile uploadedFile
     * @param  array $data         [containerId, structureId, codeType]
     * @return bool
     */
    public function saveDocument($uploadedFile, $data)
    {
        $data['name'] = $uploadedFile['name'];

        $mainDoc = $this->newEntity($data);
        $res = $this->save($mainDoc, ['atomic' => false]);
        if (!$res) {
            Log::write('error', $mainDoc->getErrors(), 'projectErrors');
            throw new InvalidArgumentException($mainDoc->getErrors());
        }

        $res = $this->Files->saveInFs(
            $uploadedFile,
            ['maindocument_id' => $mainDoc->id],
            $data['container_id'],
            $data['structure_id']
        );

        if (!$res) {
            return false;
        }

        return true;
    }

    /**
     * set codeType
     *
     * @param  string $codetype       codetype
     * @param  int    $mainDocumentId maindocumentId
     * @return void
     */
    public function setCodetype($codetype, $mainDocumentId)
    {
        $this->updateAll(['codetype' => $codetype], ['id' => $mainDocumentId]);
    }

    /**
     * remove codeType
     *
     * @param  array $containerIds list of containers ids
     * @return int
     */
    public function removeCodetype($containerIds)
    {
        return $this->updateAll(['codetype' => null], ['container_id IN' => $containerIds]);
    }
}
