<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Natures Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\TypesactsTable|\App\Model\Table\HasMany $Typesacts
 * @property \App\Model\Table\TypespiecesjointesTable|\App\Model\Table\HasMany $Typespiecesjointes
 * @method \App\Model\Table\Nature get($primaryKey, $options = [])
 * @method \App\Model\Entity\Nature newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Nature[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Nature|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Nature saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Nature patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Nature[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Nature findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NaturesTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('natures');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
            'foreignKey' => 'structure_id',
            'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Typesacts',
            [
            'foreignKey' => 'nature_id',
            'dependent' => true,
            ]
        );
        $this->hasMany(
            'Typespiecesjointes',
            [
            'foreignKey' => 'nature_id',
            'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('codenatureacte')
            ->maxLength('codenatureacte', 100)
            ->requirePresence('codenatureacte', 'create')
            ->allowEmptyString('codenatureacte', null, false);
        //->add('codenatureacte', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('libelle')
            ->maxLength('libelle', 100)
            ->requirePresence('libelle', 'create')
            ->allowEmptyString('libelle', null, false);
        //->add('libelle', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('typeabrege')
            ->maxLength('typeabrege', 100)
            ->requirePresence('typeabrege', 'create')
            ->allowEmptyString('typeabrege', null, false);
        //->add('typeabrege', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->isUnique(['structure_id', 'typeabrege']));
        $rules->add($rules->isUnique(['structure_id', 'libelle']));
        $rules->add($rules->isUnique(['structure_id', 'codenatureacte']));

        return $rules;
    }
}
