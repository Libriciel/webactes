<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AttachmentsSummons Model
 *
 * @property \App\Model\Table\AttachmentsTable&\Cake\ORM\Association\BelongsTo $Attachments
 * @property \App\Model\Table\SummonsTable&\Cake\ORM\Association\BelongsTo $Summons
 * @method \App\Model\Entity\AttachmentsSummon newEmptyEntity()
 * @method \App\Model\Entity\AttachmentsSummon newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentsSummon[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentsSummon get($primaryKey, $options = [])
 * @method \App\Model\Entity\AttachmentsSummon findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\AttachmentsSummon patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentsSummon[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\AttachmentsSummon|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttachmentsSummon saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AttachmentsSummon[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\AttachmentsSummon[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\AttachmentsSummon[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\AttachmentsSummon[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class AttachmentsSummonsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('attachments_summons');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Attachments', [
            'foreignKey' => 'attachment_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Summons', [
            'foreignKey' => 'summon_id',
            'joinType' => 'INNER',
        ]);
        $this->hasOne(
            'Files',
            [
                'foreignKey' => 'attachment_summon_id',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('rank')
            ->requirePresence('rank', 'create')
            ->notEmptyString('rank');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['attachment_id'], 'Attachments'), ['errorField' => 'attachment_id']);
        $rules->add($rules->existsIn(['summon_id'], 'Summons'), ['errorField' => 'summon_id']);

        return $rules;
    }
}
