<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DraftTemplateTypes Model
 *
 * @property \App\Model\Table\DraftTemplatesTable&\Cake\ORM\Association\HasMany $DraftTemplates
 * @method \App\Model\Entity\DraftTemplateType newEmptyEntity()
 * @method \App\Model\Entity\DraftTemplateType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\DraftTemplateType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DraftTemplateType get($primaryKey, $options = [])
 * @method \App\Model\Entity\DraftTemplateType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\DraftTemplateType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DraftTemplateType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\DraftTemplateType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DraftTemplateType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DraftTemplateType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DraftTemplateType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\DraftTemplateType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DraftTemplateType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class DraftTemplateTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('draft_template_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('DraftTemplates', [
            'foreignKey' => 'draft_template_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('code')
            ->maxLength('code', 10)
            ->requirePresence('code', 'create')
            ->notEmptyString('code');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        return $validator;
    }
}
