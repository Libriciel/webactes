<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Actors Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\ConvocationsTable|\App\Model\Table\HasMany $Convocations
 * @property \App\Model\Table\ActsTable|\App\Model\Table\BelongsToMany $Acts
 * @property \App\Model\Table\ProjectsTable|\App\Model\Table\BelongsToMany $Projects
 * @property \App\Model\Table\SittingsTable|\App\Model\Table\BelongsToMany $Sittings
 * @property \App\Model\Table\ActorGroupsTable|\App\Model\Table\BelongsToMany $Actorsgroups
 * @method \App\Model\Table\Actor get($primaryKey, $options = [])
 * @method \App\Model\Entity\Actor newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Actor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Actor|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Actor|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Actor patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Actor[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Actor findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ActorsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('actors');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Convocations',
            [
                'foreignKey' => 'actor_id',
                'dependent' => true,
            ]
        );
        $this->hasMany(
            'ActorsProjectsSittings',
            [
                'foreignKey' => 'actor_id',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Acts',
            [
                'foreignKey' => 'actor_id',
                'targetForeignKey' => 'act_id',
                'joinTable' => 'actors_acts',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Projects',
            [
                'foreignKey' => 'actor_id',
                'targetForeignKey' => 'project_id',
                'joinTable' => 'actors_projects',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'ActorGroups',
            [
                'foreignKey' => 'actor_id',
                'targetForeignKey' => 'actor_group_id',
                'joinTable' => 'actors_actor_groups',
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('structure_id')
            ->requirePresence('structure_id', 'create');

        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->boolean('active')
            ->allowEmptyString('active');

        $validator
            ->scalar('civility')
            ->maxLength('civility', 50)
            ->requirePresence('civility', 'create')
            ->notEmptyString('civility');

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 50)
            ->requirePresence('lastname', 'create')
            ->notEmptyString('lastname');

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 50)
            ->minLength('firstname', 3)
            ->requirePresence('firstname', 'create')
            ->notEmptyString('firstname');

        $validator
            ->date('birthday')
            ->allowEmptyDate('birthday');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email')
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'E-mail must be valid',
            ]);

        $validator
            ->scalar('title')
            ->maxLength('title', 250)
            ->allowEmptyString('title');

        $validator
            ->scalar('address')
            ->maxLength('address', 100)
            ->allowEmptyString('address');

        $validator
            ->scalar('address_supplement')
            ->maxLength('address_supplement', 100)
            ->allowEmptyString('address_supplement');

        $validator
            ->scalar('post_code')
            ->maxLength('post_code', 5)
            ->allowEmptyString('post_code');

        $validator
            ->scalar('city')
            ->maxLength('city', 100)
            ->allowEmptyString('city');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 15)
            ->allowEmptyString('phone');

        $validator
            ->scalar('cellphone')
            ->maxLength('cellphone', 15)
            ->allowEmptyString('cellphone');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));

        return $rules;
    }
}
