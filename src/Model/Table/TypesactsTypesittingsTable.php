<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypesactsTypesittings Model
 *
 * @property \App\Model\Table\TypesactsTable|\App\Model\Table\BelongsTo $Typesacts
 * @property \App\Model\Table\TypesittingsTable|\App\Model\Table\BelongsTo $Typesittings
 * @method \App\Model\Table\TypesactsTypesitting get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypesactsTypesitting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TypesactsTypesitting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypesactsTypesitting|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypesactsTypesitting|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypesactsTypesitting patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypesactsTypesitting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypesactsTypesitting findOrCreate($search, callable $callback = null, $options = [])
 */
class TypesactsTypesittingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('typesacts_typesittings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo(
            'Typesacts',
            [
                'foreignKey' => 'typesact_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Typesittings',
            [
                'foreignKey' => 'typesitting_id',
                'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('typesact_id')
            ->requirePresence('typesact_id', 'create');

        $validator
            ->integer('typesitting_id')
            ->requirePresence('typesitting_id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['typesact_id'], 'Typesacts'));
        $rules->add($rules->existsIn(['typesitting_id'], 'Typesittings'));

        return $rules;
    }
}
