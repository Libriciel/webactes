<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Typesacts Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\NaturesTable|\App\Model\Table\BelongsTo $Natures
 * @property |\Cake\ORM\Association\HasMany $Containers
 * @property \App\Model\Table\TypesittingsTable|\App\Model\Table\BelongsToMany $Typesittings
 * @property \App\Model\Table\CountersTable|\Cake\Model\Table\BelongsTo $Counters
 * @method \App\Model\Table\Typesact get($primaryKey, $options = [])
 * @method \App\Model\Entity\Typesact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Typesact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Typesact|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Typesact saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Typesact patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Typesact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Typesact findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TypesactsTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('typesacts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        $this->addBehavior('LimitToStructure');
        $this->addBehavior('Timestamp');

        $this->addBehavior('LibricielCakephp3Database.AssociatedTables');

//        $this->addBehavior(
//            'LibricielCakephp3Database.GuardedAssociatedTables',
//            [
//                'update' => [
//                    'unguardedFields' => ['name', 'active', 'istdt', 'nature_id'],
//                    'unguardedAliases' => ['DraftTemplatesTypesacts'],
//                ],
//            ]
//        );

        $this->addBehavior('LibricielCakephp3Database.VirtualFields');

        $this->behaviors()->get('VirtualFields')->disableAuto(['is_deletable', 'is_updatable', 'is_linked']);

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Natures',
            [
                'foreignKey' => 'nature_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'Containers',
            [
                'foreignKey' => 'typesact_id',
                'dependent' => true,
            ]
        );

        $this->belongsToMany(
            'Typesittings',
            [
                'foreignKey' => 'typesact_id',
                'targetForeignKey' => 'typesitting_id',
                'joinTable' => 'typesacts_typesittings',
                'dependent' => true,
            ]
        );
        $this->belongsTo(
            'Counters',
            [
                'foreignKey' => 'counter_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsToMany(
            'DraftTemplates',
            [
                'foreignKey' => 'typesact_id',
                'targetForeignKey' => 'draft_template_id',
                'joinTable' => 'draft_templates_typesacts',
                'dependent' => true,
            ]
        );

        $this->belongsTo('ProjectGenerateTemplates', [
            'className' => 'GenerateTemplates',
        ])
            ->setForeignKey('generate_template_project_id')
            ->setJoinType('INNER')
            ->setDependent(true);

        $this->belongsTo('ActGenerateTemplates', [
            'className' => 'GenerateTemplates',
        ])
            ->setForeignKey('generate_template_act_id')
            ->setJoinType('INNER')
            ->setDependent(true);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('structure_id')
            ->requirePresence('structure_id', 'create');

        $validator
            ->integer('nature_id')
            ->requirePresence('nature_id', ['create', 'update']);

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', null, false);

        $validator
            ->boolean('istdt')
            ->requirePresence('istdt', 'create')
            ->allowEmptyString('istdt', null, false);

        $validator
            ->boolean('isdeliberating')
            ->requirePresence('isdeliberating', 'create')
            ->allowEmptyString('isdeliberating', null, false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['nature_id'], 'Natures'));
        $rules->add($rules->isUnique(['structure_id', 'name']));

        $rules->addDelete(function ($entity, $options) {

            return $entity->availableActions['can_be_deleted']['value'];
        }, 'CanNotBeDeleted', [
            'errorField' => 'id',
            'message' => 'Cannot delete a Typesacts in use',
        ]);

        return $rules;
    }

    /**
     * for a given typesact_id get authorized TypesPiecesJointes codetype
     *
     * @param int $id typeact_id
     * @return array
     */
    public function getAssociatedTypesPiecesJointes($id)
    {
        $typesActes = $this->get($id);
        $Typespiecesjointes = $this->fetchTable('Typespiecesjointes');
        $typesPJ = $Typespiecesjointes->find()
            ->select(['codetype'])
            ->where(['nature_id' => $typesActes->nature_id])
            ->toArray();

        return Hash::extract($typesPJ, '{n}.codetype');
    }

    /**
     * for a given typesact_id get associated Nature typeabrege
     *
     * @param int $id typeact_id
     * @return string
     */
    public function getAssociatedNature($id)
    {
        $type = $this->getAssociatedTypesPiecesJointes($id);

        $authorizedCode = array_filter(
            $type,
            function ($code) {
                if (substr($code, 0, 2)) {
                    return $code;
                }
            }
        );

        return $authorizedCode;
    }
}
