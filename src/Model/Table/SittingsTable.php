<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Sittings Model
 *
 * @property \App\Model\Table\StructuresTable|\App\Model\Table\BelongsTo $Structures
 * @property \App\Model\Table\TypesittingsTable|\App\Model\Table\BelongsTo $Typesittings
 * @property \App\Model\Table\ConvocationsTable|\App\Model\Table\HasMany $Convocations
 * @property \App\Model\Table\ContainersSittingsTable|\App\Model\Table\HasMany $ContainersSittings
 * @property \App\Model\Table\ActorsTable|\App\Model\Table\BelongsToMany $Actors
 * @property \App\Model\Table\ContainersTable|\App\Model\Table\BelongsToMany $Containers
 * @property \App\Model\Table\SittingsStatesittingsTable|\App\Model\Table\HasMany $SittingsStatesittings
 * @property \App\Model\Table\GenerationerrorsTable|\App\Model\Table\HasMany $Generationerrors
 * @property |\Cake\ORM\Association\BelongsToMany $Statesittings
 * @method \App\Model\Table\Sitting get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sitting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sitting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sitting|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sitting saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sitting patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sitting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sitting findOrCreate($search, callable $callback = null, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SittingsTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->addBehavior('DateFormattable');

        $this->setTable('sittings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('LimitToStructure');

        $this->belongsTo(
            'Structures',
            [
                'foreignKey' => 'structure_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Typesittings',
            [
                'foreignKey' => 'typesitting_id',
                'joinType' => 'INNER',
            ]
        );
        $this->hasMany(
            'ContainersSittings',
            [
                'foreignKey' => 'sitting_id',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Containers',
            [
                'foreignKey' => 'sitting_id',
                'targetForeignKey' => 'container_id',
                'joinTable' => 'containers_sittings',
                'dependent' => true,
            ]
        );
        $this->belongsToMany(
            'Statesittings',
            [
                'foreignKey' => 'sitting_id',
                'targetForeignKey' => 'statesitting_id',
                'joinTable' => 'sittings_statesittings',
                'dependent' => true,
                'through' => 'SittingsStatesittings',
            ]
        );
        $this->hasMany(
            'SittingsStatesittings',
            [
                'foreignKey' => 'statesitting_id',
                'dependent' => false,
            ]
        );
        $this->hasMany(
            'ActorsProjectsSittings',
            [
                'foreignKey' => 'sitting_id',
                'dependent' => true,
            ]
        );

        $this->hasMany(
            'Summons',
            [
                'foreignKey' => 'sitting_id',
                'dependent' => true,
            ]
        );

        $this->hasMany(
            'Generationerrors',
            [
                'foreignKey' => 'foreign_key',
                'conditions' => [
                    'type IN' => ['convocation', 'executive_summary', 'deliberations_list', 'verbal_trial'],
                ],
                'dependent' => true,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('typesitting_id')
            ->requirePresence('typesitting_id', true);

        $validator
            ->integer('structure_id')
            ->requirePresence('structure_id', 'create');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->allowEmptyDateTime('date', null, false);

        $validator
            ->dateTime('date_convocation')
            ->allowEmptyDateTime('date_convocation');

        $validator
            ->allowEmptyString('place');

        return $validator;
    }

    /**
     * @param \Cake\ORM\RulesChecker $rules rules
     * @return \Cake\ORM\RulesChecker
     * @access  public
     * @created 02/04/2019
     * @version V0.0.9
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['structure_id'], 'Structures'));
        $rules->add($rules->existsIn(['typesitting_id'], 'Typesittings'));
        $rules->add($rules->isUnique(['structure_id', 'typesitting_id', 'date']));
        $rules->addDelete(function ($entity) {
            return $entity->availableActions['can_be_deleted']['value'];
        }, 'sittingCantBeDeleted', [
            'errorField' => 'id',
            'message' => 'Cannot delete a sitting with a voted project',
        ]);

        return $rules;
    }

    /**
     * Finder
     * Permets de récupérer les séances en filtrant par l'état passé en paramètre.
     *
     * Allows you to recover the sessions by filtering by the state passed in parameter. (PS:Pour MIKA)
     *
     * @param \Cake\ORM\Query $query query
     * @param int $idSearchStateSitting id de l'état dans la table
     *                                     Statesittings
     * @param bool $isNot reverse condition
     * @return \Cake\ORM\Query
     * @access  public
     * @created 16/05/2019
     * @version V0.0.9
     */
    public function findSittingsByStatesittings(Query $query, $idSearchStateSitting, $isNot = false)
    {
        return $query
            ->matching('Statesittings')
            ->where(
                ['SittingsStatesittings.id' => $this->SittingsStatesittings->getLastBySittingsIdSubquery()]
                + (
                $isNot
                    ? ['SittingsStatesittings.statesitting_id <>' => $idSearchStateSitting]
                    : ['SittingsStatesittings.statesitting_id' => $idSearchStateSitting]
                )
            );
    }

    /**
     * @param \Cake\ORM\Query $query query
     * @param array $options options
     * @return \Cake\ORM\Query
     * @version V2
     */
    public function findByTypesittingId(Query $query, array $options)
    {
        return $query->where(['typesitting_id' => $options['typesitting_id']]);
    }

    /**
     * @param int $sittingId sittingId
     * @return array
     */
    public function findProjectsSitting(int $sittingId): array
    {
        return $this->find('all')
            ->select('Containers.project_id')
            ->innerJoinWith('ContainersSittings')
            ->innerJoinWith('ContainersSittings.Containers')
            ->where(['Sittings.id IS' => $sittingId])
            ->enableHydration(false)
            ->toArray();
    }

    /**
     * @param int $sittingId sittingId
     * @return void
     */
    public function updateSittingInGeneratedActDocument(int $sittingId)
    {
        $projects = $this->findProjectsSitting($sittingId);

        /** @var \App\Model\Table\ProjectsTable $Projects */
        $Projects = $this->fetchTable('Projects');

        foreach ($projects as $project) {
            // generate documents acte
            $Projects->generateProjectFiles((int)$project['_matchingData']['Containers']['project_id']);
        }
    }

    /**
     * @param int $sittingId sittingId
     * @return bool
     */
    public function isSittingCanBeClosed(int $sittingId): bool
    {
        $sitting = $this
            ->find()
            ->select(['Typesittings.isdeliberating'])
            ->innerJoinWith('Typesittings')
            ->where(['Sittings.id' => $sittingId])
            ->enableHydration(false)
            ->firstOrFail();

        if ($sitting['_matchingData']['Typesittings']['isdeliberating'] === false) {
            return true;
        }

        $Containers = $this->fetchTable('Containers');
        $containersStateacts = $Containers
            ->find()
            ->select(['Containers.id', 'ContainersStateacts.stateact_id'])
            ->innerJoinWith('ContainersSittings')
            ->leftJoinWith('ContainersStateacts', function ($query) {
                return $query->where([
                    'ContainersStateacts.stateact_id IN' => [VOTED_REJECTED, VOTED_APPROVED, TAKENOTE],
                ]);
            })
            ->where([
                'ContainersSittings.sitting_id' => $sittingId,
            ])
            ->enableHydration(false)
            ->toArray();

        $containersStateacts = Hash::combine(
            $containersStateacts,
            '{n}.id',
            '{n}._matchingData.ContainersStateacts.stateact_id'
        );

        return count($containersStateacts) === count(array_filter($containersStateacts));
    }
}
