<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ActorGroupsTypesittings Model
 *
 * @property \App\Model\Table\ActorGroupsTable&\Cake\ORM\Association\BelongsTo $ActorGroups
 * @property \App\Model\Table\TypesittingsTable&\Cake\ORM\Association\BelongsTo $Typesittings
 * @method \App\Model\Entity\ActorsgroupsTypesitting newEmptyEntity()
 * @method \App\Model\Entity\ActorsgroupsTypesitting newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ActorsgroupsTypesitting[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ActorGroupsTypesittingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('actor_groups_typesittings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ActorGroups', [
            'foreignKey' => 'actor_group_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Typesittings', [
            'foreignKey' => 'typesitting_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['actor_group_id'], 'ActorGroups'), ['errorField' => 'actor_group_id']);
        $rules->add($rules->existsIn(['typesitting_id'], 'Typesittings'), ['errorField' => 'typesitting_id']);

        return $rules;
    }
}
