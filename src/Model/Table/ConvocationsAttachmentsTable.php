<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConvocationsAttachments Model
 *
 * @property \App\Model\Table\ConvocationsTable|\App\Model\Table\BelongsTo $Convocations
 * @property \App\Model\Table\AttachmentsTable|\App\Model\Table\BelongsTo $Attachments
 * @method \App\Model\Table\ConvocationsAttachment get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConvocationsAttachment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConvocationsAttachment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConvocationsAttachment|bool save(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConvocationsAttachment|bool saveOrFail(\App\Model\Table\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConvocationsAttachment patchEntity(\App\Model\Table\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConvocationsAttachment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConvocationsAttachment findOrCreate($search, callable $callback = null, $options = [])
 */
class ConvocationsAttachmentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('convocations_attachments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo(
            'Convocations',
            [
                'foreignKey' => 'convocation_id',
                'joinType' => 'INNER',
            ]
        );
        $this->belongsTo(
            'Attachments',
            [
                'foreignKey' => 'attachment_id',
                'joinType' => 'INNER',
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['convocation_id', 'attachment_id']));
        $rules->add($rules->existsIn(['attachment_id'], 'Attachments'));
        $rules->add($rules->existsIn(['convocation_id'], 'Convocations'));

        return $rules;
    }
}
