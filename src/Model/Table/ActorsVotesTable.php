<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Http\Exception\BadRequestException;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ActorsVotes Model
 *
 * @property \App\Model\Table\ActorsTable&\Cake\ORM\Association\BelongsTo $Actors
 * @property \App\Model\Table\VotesTable&\Cake\ORM\Association\BelongsTo $Votes
 * @method \App\Model\Entity\ActorsVote newEmptyEntity()
 * @method \App\Model\Entity\ActorsVote newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ActorsVote[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActorsVote get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActorsVote findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ActorsVote patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsVote[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActorsVote|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsVote saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActorsVote[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ActorsVote[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ActorsVote[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ActorsVote[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ActorsVotesTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('actors_votes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Actors', [
            'foreignKey' => 'actor_id',
        ]);
        $this->belongsTo('Votes', [
            'foreignKey' => 'vote_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ActorsProjectsSittings', [
            'foreignKey' => 'actor_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('result')
            ->maxLength('result', 255)
            ->allowEmptyString('result');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['actor_id'], 'Actors'), ['errorField' => 'actor_id']);
        $rules->add($rules->existsIn(['vote_id'], 'Votes'), ['errorField' => 'vote_id']);

        return $rules;
    }

    /**
     * @param array $voteId The id Of the vote
     * @return array State result
     */
    public function calculateResult(int $voteId): int
    {
        $totalOfOpinionFor = $this->find()->where(['vote_id' => $voteId, 'result' => 'yes'])->count();
        $totalOfOpinionAgainst = $this->find()->where(['vote_id' => $voteId, 'result' => 'no'])->count();

        return $totalOfOpinionFor > $totalOfOpinionAgainst ? VOTED_APPROVED : VOTED_REJECTED;
    }

    /**
     * @param int $actor actor
     * @param int $projectId projectId
     * @return true
     */
    public function checkVoteValidity(int $actor, int $projectId)
    {
        /**
         * @var \App\Model\Table\VotesTable $VotesTable
         */
        $VotesTable = $this->fetchTable('Votes');

        $actorVote = $VotesTable->find()
            ->select([
                'ActorsVotes.id', 'ActorsVotes.result', 'Actors.firstname', 'Actors.lastname',
                'ActorsProjectsSittings.id', 'ActorsProjectsSittings.mandataire_id',
                'ActorsProjectsSittings.is_present',
            ])
            ->innerJoinWith('ActorsVotes')
            ->innerJoinWith('ActorsVotes.Actors.ActorsProjectsSittings')
            ->where(['ActorsVotes.actor_id IS' => $actor, 'ActorsProjectsSittings.project_id' => $projectId])
            ->orderDesc('Votes.modified')
            ->enableHydration(false)
            ->first();

        if (
            !empty($actorVote) &&
            !$actorVote['_matchingData']['ActorsProjectsSittings']['mandataire_id'] &&
            $actorVote['_matchingData']['ActorsProjectsSittings']['is_present'] === false &&
            !empty($actorVote['_matchingData']['ActorsVotes']['result'])
        ) {
            throw new BadRequestException('Impossible de modifier le vote de l\'acteur ' .
                $actorVote['_matchingData']['Actors']['firstname'] . ' ' .
                $actorVote['_matchingData']['Actors']['lastname']);
        }

        return true;
    }
}
