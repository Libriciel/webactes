<?php
declare(strict_types=1);

namespace App\Model\Enum;

enum RolesName
{
    public const SUPER_ADMINISTRATOR = 'Super Administrateur';
    public const ADMINISTRATOR = 'Administrateur';
    public const FUNCTIONAL_ADMINISTRATOR = 'Administrateur Fonctionnel';
    public const GENERAL_SECRETARIAT = 'Secrétariat général';
    public const VALIDATOR_WRITER = 'Rédacteur / Valideur';
}
