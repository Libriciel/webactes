<?php
declare(strict_types=1);

namespace App\Model\Enum;

enum DocumentType
{
    public const ACT = 'act';
    public const PROJECT = 'project';
    public const CONVOCATION = 'convocation';
    public const DELIBERATIONS_LIST = 'deliberations_list';
    public const VERBAL_TRIAL = 'verbal_trial';
    public const EXECUTIVE_SUMMARY = 'executive_summary';
}
