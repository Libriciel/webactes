<?php
declare(strict_types=1);

namespace App\Utility;

use Cake\Core\Configure;
use Cake\Filesystem\File;
use Countable;
use Iterator;

class CsvFileReader implements Iterator, Countable
{
    /**
     * Le fichier CSV.
     *
     * @var \Cake\Filesystem\File
     */
    protected $_file = null;

    /**
     * @var string
     */
    protected $_content = null;

    /**
     * @var array
     */
    protected $_lines = [];

    /**
     * @var array
     */
    protected $_records = [];

    /**
     * @var string
     */
    protected $_separator = null;

    /**
     * @var string
     */
    protected $_delimiter = null;

    /**
     * @var string
     */
    protected $_headers = null;

    /**
     * L'array de paramètres par défaut du constructeur.
     *
     * @var array
     */
    protected $_defaults = [
        'separator' => ',',
        'delimiter' => '",',
        'encoding' => null,
        'headers' => false,
    ];

    /**
     * Loads the file content
     *
     * @param  string $path The path to the CSV file
     * @throws \App\Utility\RuntimeException
     * @return void
     */
    protected function _loadFile($path)
    {
        $this->_file = new File($path);

        if (!$this->_file->exists()) {
            $msgstr = 'Le fichier "%s" n\'existe pas';
            throw new RuntimeException(sprintf($msgstr, $this->_file->pwd()));
        } elseif (!$this->_file->readable()) {
            $msgstr = 'Le fichier "%s" n\'est pas lisible';
            throw new RuntimeException(sprintf($msgstr, $this->_file->pwd()));
        }

        $this->_content = $this->_file->read();
    }

    /**
     * Parse every CSV row content.
     *
     * @return void
     */
    protected function _parseContent()
    {
        $this->_lines = explode("\n", $this->_toUtf8($this->_content));

        foreach ($this->_lines as $key => $line) {
            $line = trim($line);
            $row = str_getcsv($line, $this->_separator, $this->_delimiter);

            if ($line === '' || empty($row)) {
                $row = [];
            }

            $this->_records[$key] = $row;
        }
    }

    /**
     * @param  string $path   The path to the CSV file
     * @param  array  $params Parameters, keys are "separator", "delimiter", "headers"
     * @return void
     */
    public function __construct($path, array $params = [])
    {
        $params += $this->_defaults;

        $this->_separator = $params['separator'];
        $this->_delimiter = $params['delimiter'];
        $this->_headers = $params['headers'];

        $this->_loadFile($path);
        $this->_parseContent();

        $this->rewind();
    }

    /**
     * Convert a string to UTF-8 if needed.
     *
     * @param  string $string The string to convert
     * @return string
     */
    protected function _toUtf8($string)
    {
        mb_detect_order(['UTF-8', 'ISO-8859-1', 'ASCII']);
        $encoding = mb_detect_encoding($string);

        $appEncoding = Configure::read('App.encoding');
        if ($encoding !== $appEncoding) {
            $string = mb_convert_encoding($string, $appEncoding, $encoding);
        }

        return $string;
    }

    /**
     * Rewind the CSV file.
     *
     * @return void
     */
    public function rewind()
    {
        $this->_position = 0;
    }

    /**
     * Returns the real key from the CSV file (with respect to the headers).
     *
     * @return int
     */
    protected function _realKey()
    {
        return $this->_position + ($this->_headers ? 1 : 0);
    }

    /**
     * Returns the current row read from the CSV file.
     *
     * @return array
     */
    public function current()
    {
        return $this->_records[$this->_realKey()];
    }

    /**
     * Returns the current position.
     *
     * @return int
     */
    public function key()
    {
        return $this->_position;
    }

    /**
     * Updates the current reference to the next row.
     *
     * @return void
     */
    public function next()
    {
        ++$this->_position;
    }

    /**
     * Checks that the current position is valid.
     *
     * @return bool
     */
    public function valid()
    {
        return isset($this->_records[$this->_realKey()]);
    }

    /**
     * Returns the number of rows read from the CSV file.
     *
     * @return int
     */
    public function count()
    {
        return max(count($this->_records) - ($this->_headers ? 1 : 0), 0);
    }

    /**
     * Returns the headers read from the CSV file.
     *
     * @return array
     */
    public function headers()
    {
        if ($this->_headers && count($this->_records) > 0) {
            return $this->_records[0];
        }

        return [];
    }
}
