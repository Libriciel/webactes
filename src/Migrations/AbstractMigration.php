<?php
declare(strict_types=1);

namespace App\Migrations;

abstract class AbstractMigration extends \Migrations\AbstractMigration
{
    /**
     * Supprime (drop) la contrainte (constraint) d'une table.
     *
     * @param string $table Le nom de la table
     * @param string $constraint Le nom de la contrainte
     * @return void
     */
    protected function dropConstraint(string $table, string $constraint): void
    {
        $this->execute(sprintf('ALTER TABLE %s DROP CONSTRAINT %s;', $table, $constraint));
    }
}
