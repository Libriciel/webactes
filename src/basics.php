<?php
declare(strict_types=1);

namespace App;

use Cake\Utility\Hash;
use Ramsey\Uuid\Uuid;
use RuntimeException;

/**
 * Generate a version 4 (random) UUID.
 *
 * @return string
 * @throws \App\Exception
 */
function uuid4()
{
    return Uuid::uuid4()->toString();
}

/**
 * Returns the MIME type for a given file, using the siegfried binary or falling back to mime_content_type PHP function
 * it siegfried fails to recognize the MIME type.
 *
 * @param  string $path The path to the file.
 * @return false|string
 */
function mimetype($path)
{
    $result = false;

    if (file_exists($path) === true) {
        $output = $returnVar = null;
        $command = sprintf('sf -json %s 2>/dev/null', escapeshellarg($path));
        $output = exec($command, $output, $returnVar);
        if ($returnVar !== 0) {
            $msgstr = 'Error code %d returned by the shell command: %s';
            throw new RuntimeException(sprintf($msgstr, $returnVar, $command));
        } else {
            $result = Hash::get(json_decode($output, true), 'files.0.matches.0.mime');
            // Fallback, for example PHP files aren't recognized
            if (empty($result) === true) {
                $result = mime_content_type($path);
            }
        }
    }

    return $result;
}

/**
 * Returns the result of a Hash::get call with the ability to use functions in the path.
 *
 * Examples, giving "bar"
 *  - hash_get_call_user_func_array(['foo' => ['bar' => 'baz']], 'foo.bar')
 *  - hash_get_call_user_func_array(['foo' => json_encode(['bar' => 'baz'])], 'foo|json_decode.bar')
 *  - hash_get_call_user_func_array(['foo' => json_encode(['bar' => 'baz'])], 'foo|json_decode,true.bar')
 *
 * @param array|\ArrayAccess $data Array of data or object implementing
 * \ArrayAccess interface to operate on.
 * @param  string             $path The path being searched for, as a dot separated string.
 * @return mixed
 */
function hash_get_call_user_func_array(array $data, $path)
{
    $current = $data;
    while (($pos = strpos($path, '|')) !== false) {
        $partialPath = substr($path, 0, $pos);
        $path = substr($path, $pos + 1);
        $current = Hash::get($current, $partialPath);
        $endFunctionPos = strpos($path, '.');
        $endFunctionPos = $endFunctionPos === false ? strlen($path) : $endFunctionPos;
        $function = explode(',', substr($path, 0, $endFunctionPos));
        $path = substr($path, $endFunctionPos + 1);
        $params = array_merge([$current], array_slice($function, 1));
        if ($function[0] === 'json_decode' && $params[0] === null) {
            $params[0] = 'null';
        }
        $current = call_user_func_array($function[0], $params);
        if (is_scalar($current) === false) {
            $current = (array)$current;
        }
        if ($path === false) {
            return $current;
        }
    }

    return Hash::get($current, $path);
}

/**
 * Create a directory (recursively if needed) with the given permissions if the path not exist yet.
 * If the path already exists, checks that it is a writable directory for the current user or throws an exception.
 *
 * It is meant to replace the usage of \Cake\Filesystem\Folder when its sole purpose is the create a directory.
 *
 * @param string $path The path to the directory
 * @param int $permissions The permissions with which to create the directory if it does not exist
 * @return void
 * @throws \RuntimeException
 */
function safe_mkdir(string $path, int $permissions = 0777): void
{
    if (is_dir($path)) {
        if (!is_writable($path)) {
            throw new RuntimeException(sprintf('Directory %s exists but is not writable', $path));
        }
    } elseif (is_file($path)) {
        throw new RuntimeException(sprintf('Cannot create directory %s because its path exists as a file', $path));
    } else {
        if (mkdir($path, $permissions, true) !== true) {
            throw new RuntimeException(sprintf('Cannot create directory %s', $path));
        }
    }
}

/**
 * Equivalent to the shell_exec command but that throws a \RuntimeException if the command fails.
 *
 * @param string $command The command that will be executed.
 * @return string
 * @throws \RuntimeException
 */
function safe_shell_exec(string $command): string
{
    $output = [];
    $code = null;
    if (exec($command, $output, $code) === false || $code !== 0) {
        $message = sprintf(
            "The command returned an error (%d): %s\n\t%s",
            $code,
            $command,
            implode("\n\t", $output)
        );
        throw new RuntimeException($message);
    }

    return implode("\n", $output);
}
