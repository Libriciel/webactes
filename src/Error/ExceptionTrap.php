<?php
declare(strict_types=1);

namespace App\Error;

use App\Controller\AppController;
use App\Error\Renderer\WebExceptionRenderer;
use App\Model\Table\WorkergenerationsTable;
use Cake\Error\ExceptionTrap as CakeExceptionTrap;
use Cake\Error\Renderer\ConsoleExceptionRenderer;
use Cake\Routing\Router;

class ExceptionTrap extends CakeExceptionTrap
{
    /**
     * Choose an exception renderer based on the fact that an HTTP request exists
     *
     * @return class-string<\Cake\Error\ExceptionRendererInterface>
     */
    protected function chooseRenderer(): string
    {
        /** @var class-string<\Cake\Error\ExceptionRendererInterface> */
        return Router::getRequest() === null ? ConsoleExceptionRenderer::class : WebExceptionRenderer::class;
    }

    /**
     * Override the fatal error handling to get a chance to properly log generation errors.
     *
     * @param int $code Code of error
     * @param string $description Error description
     * @param string $file File on which error occurred
     * @param int $line Line that triggered the error
     * @return void
     */
    public function handleFatalError(int $code, string $description, string $file, int $line): void
    {
        $kilobytes = $this->_config['extraFatalErrorMemory'] ?? 4;
        if ($kilobytes > 0) {
            $this->increaseMemoryLimit($kilobytes * 1024);
        }

        $request = Router::getRequest();

        if ($request) {
            AppController::handleFatalErrorMessage($request, $description);
        } else {
            WorkergenerationsTable::toGenerationerror(getmypid(), $description);
        }

        parent::handleFatalError($code, $description, $file, $line);
    }
}
