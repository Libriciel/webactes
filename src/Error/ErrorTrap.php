<?php
declare(strict_types=1);

namespace App\Error;

use Cake\Error\ErrorTrap as CakeErrorTrap;
use Cake\Error\Renderer\ConsoleErrorRenderer;
use Cake\Error\Renderer\HtmlErrorRenderer;
use Cake\Routing\Router;

class ErrorTrap extends CakeErrorTrap
{
    /**
     * Choose an exception renderer based on the fact that an HTTP request exists
     *
     * @return class-string<\Cake\Error\ErrorRendererInterface>
     */
    protected function chooseErrorRenderer(): string
    {
        $config = $this->getConfig('errorRenderer');
        if ($config !== null) {
            return $config;
        }

        /** @var class-string<\Cake\Error\ErrorRendererInterface> */
        return Router::getRequest() === null ? ConsoleErrorRenderer::class : HtmlErrorRenderer::class;
    }
}
