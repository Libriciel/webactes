<?php
declare(strict_types=1);

namespace App\Error\Renderer;

use Cake\Error\FatalErrorException;
use Cake\Error\Renderer\WebExceptionRenderer as CakeWebExceptionRenderer;
use Psr\Http\Message\ResponseInterface;

class WebExceptionRenderer extends CakeWebExceptionRenderer
{
    /**
     * Override exception rendering in case of a fatal error exception
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function render(): ResponseInterface
    {
        $exception = $this->error;
        if ($exception instanceof FatalErrorException) {
            $this->clearOutput();

            if (!in_array(PHP_SAPI, ['cli', 'phpdbg'])) {
                $request = $this->controller->getRequest();
                if (!empty($request)) {
                    return $this->controller
                        ->getResponse()
                        ->withType('application/json')
                        ->withStringBody(json_encode(['message' => $exception->getMessage()]))
                        ->withStatus($this->getHttpCode($exception), $exception->getMessage());
                }
            }
        }

        return parent::render();
    }
}
