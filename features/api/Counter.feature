# language: fr
Fonctionnalité: Gestion des compmteurs

   Contexte: Etant donné cet utilisateur connecté:
   | username | password     |
   | p.hofstadter  | p.hofstadter |


   Scenario: Générer un numéro d'acte
   Lorsque je suis connecté en tant que "p.hofstadter" "p.hofstadter"
   Et que j'ajoute l'entête "Content-Type" égale à "application/json"
   Et que j'ajoute l'entête "Accept" égale à "application/json"
   Quand j'envoie une requête "PUT" sur "/act/generate/:id"
   Et la réponse est au format JSON
   """
   {
      "id": 5,
      "name": "Tout nouveau circuit",
      "description": "Youhou !!!! Super description",
      "active": true,
      "steps":[
            {
               "name": "Etape_1",
               "validators":[
                    {
                        "id": 5,
                        "firstname": "Romain",
                        "lastname": "Cyan"
                    },
                    {
                        "id": 3,
                        "firstname": "Julien",
                        "lastname": "ORANGE"
                    }

                ]
            },
            {
               "name": "Etape_2",
               "validators":[
                    {
                        "id": 1,
                        "firstname": "Sébastien",
                        "lastname": "BLANC"
                    }
                ]
            }
        ],
      "is_editable": true,
      "is_deletable": true
   }
   """
   Alors le code de status de la réponse devrait être 201

