# language: fr
@actors
Fonctionnalité: Gérer des séances

Contexte: Etant donné cet utilisateur connecté:
   | username   | password     |
   | e.sweeney  | e.sweeney    |



Scenario: Afficher la liste des séances pour la création ou la modification d'un projet
   Lorsque je suis connecté avec "e.sweeney" "e.sweeney"
   Et que j'ajoute l'entête "Content-Type" égale à "application/json"
   Et que j'ajoute l'entête "Accept" égale à "application/json"
   Quand j'envoie une requête "GET" sur "/sittings/"
   Alors le code de status de la réponse devrait être 200
   Alors le JSON devrait être égal à :
   """
   [
      {
         "id":1,
         "date": "2020-01-31T15:00:00+00:00",
         "statesittings": [{
            "id": 1, 
            "state": "Ouverture de la séance",
            "code": "ouverture_seance"
         }],
         "typesitting": {
            "id": 1,
            "name": "Délibération"
         }
      },
      {
         "id":2,
         "date": "2020-02-07T13:00:00+00:00",
         "statesittings": [{
            "id": 1, 
            "state": "Ouverture de la séance",
            "code": "ouverture_seance"
         }],
         "typesitting": {
            "id": 2,
            "name": "Conseil Municipal"
         }
      },
      {  
         "id":3,
         "date": "2020-02-13T15:00:00+00:00",
         "statesittings": [{
            "id": 1, 
            "state": "Ouverture de la séance",
            "code": "ouverture_seance"
         }],
         "typesitting": {
            "id": 3,
            "name": "Conseil Communautaire"
         }
      }
   ]
   """


   Scénario: Récupérer une séance par son id
   Lorsque je suis connecté avec "j.orange" "Aqwxsz123!er"
   Et que j'ajoute l'entête "Content-Type" égale à "application/json"
   Et que j'ajoute l'entête "Accept" égale à "application/json"
   Quand j'envoie une requête "GET" sur "/sittings/:id"
   Alors le code de status de la réponse devrait être 200
   Alors le JSON devrait être égal à :
   """
   """

   Scenario: Créer une séance
   Lorsque je suis connecté avec "s.blanc" "Aqwxsz123!er"
   Et que j'ajoute l'entête "Content-Type" égale à "application/json"
   Et que j'ajoute l'entête "Accept" égale à "application/json"
   Quand j'envoie une requête "POST" sur "/sittings/"
   Alors le code de status de la réponse devrait être 201
   Alors le JSON devrait être égal à :
   """
   {  
      "id":4,
      "date": "2020-09-10T18:00:00+00:00",
      "statesittings": [{
         "id": 1, 
         "state": "Ouverture de la séance",
         "code": "ouverture_seance"
      }],
      "typesitting": {
         "id": 3,
         "name": "Conseil Communautaire"
      }
   }

   """

   Scenario: Modifier une séance
   Lorsque je suis connecté avec "s.blanc" "Aqwxsz123!er"
   Et que j'ajoute l'entête "Content-Type" égale à "application/json"
   Et que j'ajoute l'entête "Accept" égale à "application/json"
   Quand j'envoie une requête "PUT" sur "/sittings/:id"
   Alors le code de status de la réponse devrait être 200
   Et la requête devrait être au format JSON
   """
   {  
      "id":4,
      "date": "2020-09-10T18:00:00+00:00",
      "statesittings": [{
         "id": 1, 
         "state": "Ouverture de la séance",
         "code": "ouverture_seance"
      }],
      "typesitting": {
         "id": 1,
         "name": "Délibération"
      }
   }
   """


   Scenario: Clore une séance
   Lorsque je suis connecté avec "s.blanc" "Aqwxsz123!er"
   Et que j'ajoute l'entête "Content-Type" égale à "application/json"
   Et que j'ajoute l'entête "Accept" égale à "application/json"
   Quand j'envoie une requête "PUT" sur "/close"
   Alors le code de status de la réponse devrait être 200
   Alors le JSON devrait être égal à :
   """
   {
      "success": true
   }
   """


   Scenario: Supprimer une séance
   Lorsque je suis connecté avec "s.blanc" "Aqwxsz123!er"
   Et que j'ajoute l'entête "Content-Type" égale à "application/json"
   Et que j'ajoute l'entête "Accept" égale à "application/json"
   Quand j'envoie une requête "DELETE" sur "/sittings/:id" ?
   Alors le code de status de la réponse devrait être 204


   Scenario: Afficher la liste des séances closes
   Lorsque je suis connecté avec "s.blanc" "Aqwxsz123!er"
   Et que j'ajoute l'entête "Content-Type" égale à "application/json"
   Et que j'ajoute l'entête "Accept" égale à "application/json"
   Quand j'envoie une requête "GET" sur "/sittings"?
   Alors le code de status de la réponse devrait être 200
   Alors le JSON devrait être égal à :
   """
   [
      {
         "id":5,
         "date": "2020-01-31T17:00:00+00:00",
         "statesittings": [{
            "id": 2, 
            "state": "Séance close",
            "code": "seance_close"
         }],
         "typesitting": {
            "id": 1,
            "name": "Délibération"
         }
      },
      {
         "id":6,
         "date": "2020-02-07T13:00:00+00:00",
         "statesittings": [{
            "id": 2, 
            "state": "Séance close",
            "code": "seance_close"
         }],
         "typesitting": {
            "id": 2,
            "name": "Conseil Municipal"
         }
      }
   ]
   """

