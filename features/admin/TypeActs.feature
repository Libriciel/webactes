# language: fr
@actors
Fonctionnalité: Gérer des séances

    Contexte:
        Étant donné que je suis connecté avec l'utilisateur "Emily"
        Et que je vais dans le menu "Administration > Types d'acte"
        Alors je devrais voir un titre contenant "Liste des types d'acte"

    Scénario: Récupérer un type d'acte par son id
        Lorsque je suis connecté avec "j.orange" "Aqwxsz123!er"
        Et que j'ajoute l'entête "Content-Type" égale à "application/json"
        Et que j'ajoute l'entête "Accept" égale à "application/json"
        Quand j'envoie une requête "GET" sur "/typesacts/"
        Alors le code de status de la réponse devrait être 200

    Scénario: Créer un type d'acte
        Lorsque je suis connecté avec "s.blanc" "Aqwxsz123!er"
        Et que j'ajoute l'entête "Content-Type" égale à "application/json"
        Et que l'entête "Accept" égale à "application/json"
        Quand j'envoie une requête "POST" sur "/typesacts/"
        Alors le code de status de la réponse devrait être 201

    Scénario: Modifier un type d'acte
        Lorsque je suis connecté avec "s.blanc" "Aqwxsz123!er"
        Et que j'ajoute l'entête "Content-Type" égale à "application/json"
        Et que j'ajoute l'entête "Accept" égale à "application/json"
        Quand j'envoie une requête "PUT" sur "/typesacts/:id"
        Alors le code de status de la réponse devrait être 200

    Scénario: Activer un type d'acte
        Lorsque je suis connecté avec "s.blanc" "Aqwxsz123!er"
        Et que j'ajoute l'entête "Content-Type" égale à "application/json"
        Et que j'ajoute l'entête "Accept" égale à "application/json"
        Quand j'envoie une requête "PUT" sur "/typesacts/:id/activate"
        Alors le code de status de la réponse devrait être 200
        Alors le JSON devrait être égal à :
   """
   {
      "success": true
   }
   """

    Scénario: Désactiver un type d'acte
        Lorsque je suis connecté avec "s.blanc" "Aqwxsz123!er"
        Et que j'ajoute l'entête "Content-Type" égale à "application/json"
        Et que j'ajoute l'entête "Accept" égale à "application/json"
        Quand j'envoie une requête "PUT" sur "/typesacts/:id/deactivate"
        Alors le code de status de la réponse devrait être 200
        Alors le JSON devrait être égal à :
   """
   {
      "success": true
   }
   """

    Scénario: Supprimer un type d'acte
        Lorsque je suis connecté avec "s.blanc" "Aqwxsz123!er"
        Et que j'ajoute l'entête "Content-Type" égale à "application/json"
        Et que j'ajoute l'entête "Accept" égale à "application/json"
        Quand j'envoie une requête "DELETE" sur "/sittings/:id" ?
        Alors le code de status de la réponse devrait être 204


