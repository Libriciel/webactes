# language: fr

@themes

Fonctionnalité: Gestions des thèmes dans l'administration

Contexte:
    En tant que Emily
    Je veux créer une nouvelle thème
    Afin de classer les délibérations par thématique

    Plan du Scénario: Créer des thèmes
        Lorsque je suis "Créer un thème"
            Alors je devrais voir le titre "Créer un thème"
        Lorsque je remplis "Nom" avec "<Nom>"
            Et que je sélectionne le parent avec "<Thème principal>"
            Et que je presse "Enregistrer"
        Alors je devrais voir "Le thème \"<Nom>\" a été ajouté"
            Et je vois la ligne "<Nom> | <Thème principal>"
    Exemples:
        | Nom                    | Thème principal|
        | Municipalité           |                |
        | Affaires juridiques    |                |
        | Ressources humaines    | Municipalité   |
