# language: fr
@actors
Fonctionnalité: Gestions des acteurs

Contexte:
    Etant donné que je suis connecté avec l'utilisateur "Emily"
    Et que je vais dans le menu "Administration > Acteurs"
    Alors je devrais voir un titre contenant "Liste des acteurs"

Plan du Scénario: Ajouter un acteur
    Lorsque je suis "Ajouter un acteur"
        Alors je devrais voir le titre "Ajouter un acteur"
    Lorsque je remplis "Civilité" avec "<Civilité>"
        Et que je remplis "Nom" avec "<Nom>"
        Et que je remplis "Prénom" avec "<Prénom>"
        Et que je remplis "Titre" avec "<Titre>"
        Et que je remplis "Adresse 1" avec "<Adresse 1>"
        Et que je remplis "Adresse 2" avec "<Adresse 2>"
        Et que je remplis "Code postal" avec "<Code postal>"
        Et que je remplis "Ville" avec "<Ville>"
        Et que je remplis "Téléphone fixe" avec "<Téléphone fixe>"
        Et que je remplis "Téléphone mobile" avec "<Téléphone mobile>"
        Et que je remplis "Email" avec "<Email>"
        Et que je sélectionne "<types_actesur>" depuis "ActeurTypeacteurId"
        Et que je remplis "Ordre dans le conseil" avec "<Ordre>"
        Et que je presse "Enregistrer"
    Alors je devrais voir "L'acteur \"<Prénom> <Nom>\" a été ajouté"
        Et je vois la ligne "<Ordre> | <Civilité> | <Nom> | <Prénom> | <Titre> | <types_actesur> | élu"
Exemples:
    | Civilité | Nom      | Prénom    | Titre       | Adresse 1                    | Adresse 2 | Code postal | Ville       | Téléphone fixe | Téléphone mobile | Email            | types_actesur          | Ordre |
    | Mme      | BLANC    | Louise    | Maire       | 1 avenue Gambetta            |           | 34000       | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 1     |
    | M.       | BLEU     | Etienne   | 1er adjoint | 145 impasse du Levant        |           | 34000       | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 2     |
    | Mme      | ROSE     | Charline  | 2e adjointe | 1290 avenue du Pont Trinquat |           | 34070       | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 3     |
    | M.       | VERT     | Elliot    | 3e adjoint  | 4 boulevard du Jeu de Paume  |           | 34000       | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 4     |
    | Mme      | VIOLET   | Emma      |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 5     |
    | M.       | NOIR     | Charles   |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 6     |
    | Mme      | JAUNE    | Caroline  |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 7     |
    | M.       | ORANGE   | François  |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 8     |
    | Mme      | AZUR     | Jennifer  |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 9     |
    | M.       | PRUNE    | Jean      |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 10    |
    | Mme      | FUSCHIA  | Léa       |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 11    |
    | M.       | MARRON   | Edouard   |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 12    |
    | Mme      | GRIS     | Agathe    |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 13    |
    | M.       | ROUGE    | Jérémy    |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus titulaires | 14    |
    | M.       | BRUN     | David     |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus suppléants | 16    |
    | Mme      | BLOND    | Pascaline |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus suppléants | 17    |
    | M.       | ROUX     | François  |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus suppléants | 18    |
    | Mme      | VENITIEN | Anne      |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus suppléants | 19    |
    | M.       | CHATAIN  | Loïc      |             |                              |           |             | Montpellier |                |                  | admin@recette.fr | Elus suppléants | 20    |

Plan du Scénario: Ajouter un acteur suppléant
    Lorsque je clique sur "Modifier" dans la ligne "<Nom> | <Prénom>"
        Alors je devrais voir le titre "Modifier l' acteur"
    Lorsque je sélectionne "<Suppléant>" depuis "ActeurSuppleantId"
        Et que je presse "Enregistrer"
    Alors je devrais voir "L'acteur \"<Prénom> <Nom>\" a été modifié"
        Et je vois la ligne "<Nom> | <Prénom> | <Suppléant>"
Exemples:
    | Nom    | Prénom   | Suppléant         |
    | BLANC  | Louise   | David - BRUN      |
    | BLEU   | Etienne  | Pascaline - BLOND |
    | ROSE   | Charline | François - ROUX   |
    | VERT   | Elliot   | Anne - VENITIEN   |
    | VIOLET | Emma     | Loïc - CHATAIN    |
