# language: fr
@actors_group

Fonctionnalité: Gestions des types d'acteurs dans l'administration

Contexte:
    En tant que Emily
    Je veux créer un groupe d'acteurs
    Afin de rassembler les acteurs dans ce groupe

    Plan du Scénario: Ajouter des types d'acteurs
        Lorsque je suis "Ajouter un type d'acteur"
            Alors je devrais voir le titre "Ajouter un type d'acteur"
        Lorsque je remplis "Nom" avec "<Nom>"
            Et que je sélectionne le bouton radio "<Type>"
            Et que je presse "Enregistrer"
        Alors je devrais voir "Le types_actesur \"<Nom>\" a été ajouté"
            Et je vois la ligne "<Nom> | <Type>"
    Exemples:
        | Nom                    | Type    |
        | AUTO - Elus titulaires | élu     |
        | AUTO - Elus suppléants | élu     |
        | AUTO - Invités         | non élu |
