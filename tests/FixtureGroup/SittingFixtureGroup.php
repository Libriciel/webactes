<?php
declare(strict_types=1);

namespace App\Test\FixtureGroup;

use App\TestSuite\Fixture\TestFixtureGroup;

class SittingFixtureGroup extends TestFixtureGroup
{
    public $fixtures = [
        'app.Statesittings',
        'app.Organizations',
        'app.Structures',
        'app.GenerateTemplates',
        'app.Actors',
        'app.Typesittings',
        'app.Sittings',
        'app.SittingsStatesittings',
    ];
}
