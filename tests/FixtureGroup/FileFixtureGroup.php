<?php
declare(strict_types=1);

namespace App\Test\FixtureGroup;

use App\TestSuite\Fixture\TestFixtureGroup;

class FileFixtureGroup extends TestFixtureGroup
{
    public $fixtures = [
        'app.Organizations',
        'app.Workflows',
        'app.Structures',
        'app.Templates',
        'app.Themes',
        'app.Natures',
        'app.Actors',
        'app.DraftTemplates',
        'app.GenerateTemplates',
        'app.Attachments',
        'app.Projects',
        'app.Acts',
        'app.Typesacts',
        'app.Typesittings',
        'app.ProjectTexts',
        'app.Containers',
        'app.Sittings',
        'app.Annexes',
        'app.Maindocuments',
        'app.Summons',
        'app.AttachmentsSummons',
        'app.Files',
    ];
}
