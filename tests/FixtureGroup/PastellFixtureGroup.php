<?php
declare(strict_types=1);

namespace App\Test\FixtureGroup;

use App\TestSuite\Fixture\TestFixtureGroup;

class PastellFixtureGroup extends TestFixtureGroup
{
    public $fixtures = [
        'app.Organizations',
        'app.Structures',
        'app.Actors',
        'app.Templates',
        'app.Themes',
        'app.Workflows',
        'app.GenerateTemplates',
        'app.Natures',
        'app.Pastellfluxtypes',
        'app.Acts',
        'app.Projects',
        'app.Typesacts',
        'app.Typesittings',
        'app.Containers',
        'app.Sittings',
        'app.Summons',
        'app.Pastellfluxs',
        'app.Pastellfluxactions',
    ];
}
