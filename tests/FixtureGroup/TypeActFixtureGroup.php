<?php
declare(strict_types=1);

namespace App\Test\FixtureGroup;

use App\TestSuite\Fixture\TestFixtureGroup;

class TypeActFixtureGroup extends TestFixtureGroup
{
    public $fixtures = [
        'app.Organizations',
        'app.Structures',
        'app.Natures',
        'app.GenerateTemplates',
        'app.Typesacts',
    ];
}
