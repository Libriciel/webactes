<?php
declare(strict_types=1);

namespace App\Test\FixtureGroup;

use App\TestSuite\Fixture\TestFixtureGroup;

class ProjectFixtureGroup extends TestFixtureGroup
{
    public $fixtures = [
        'app.Workflows',
        'app.Organizations',
        'app.Structures',
        'app.Actors',
        'app.GenerateTemplates',
        'app.Templates',
        'app.Themes',
        'app.Typesittings',
        'app.Projects',
        'app.Sittings',
    ];
}
