<?php
declare(strict_types=1);

namespace App\Test\FixtureGroup;

use App\TestSuite\Fixture\TestFixtureGroup;

class ConvocationFixtureGroup extends TestFixtureGroup
{
    public $fixtures = [
        'app.ActorGroups',
        'app.Organizations',
        'app.Workflows',
        'app.Structures',
        'app.Templates',
        'app.Themes',
        'app.Actors',
        'app.ActorsActorGroups',
        'app.GenerateTemplates',
        'app.Projects',
        'app.ActorsProjects',
        'app.Typesittings',
        'app.Sittings',
        'app.Summons',
        'app.Convocations',
    ];
}
