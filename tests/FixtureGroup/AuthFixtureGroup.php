<?php
declare(strict_types=1);

namespace App\Test\FixtureGroup;

use App\TestSuite\Fixture\TestFixtureGroup;

/**
 * Les fixtures nécessaires à la connexion d'un utilisateur.
 */
class AuthFixtureGroup extends TestFixtureGroup
{
    public $fixtures = [
        'app.Organizations',
        'app.Structures',
        'app.StructureSettings',
        'app.Services',
        'app.Users',
        'app.UsersInstances',
        'app.Roles',
        'app.RolesUsers',
        'app.StructuresServices',
        'app.StructuresUsers',
    ];
}
