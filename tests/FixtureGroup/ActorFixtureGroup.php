<?php
declare(strict_types=1);

namespace App\Test\FixtureGroup;

use App\TestSuite\Fixture\TestFixtureGroup;

class ActorFixtureGroup extends TestFixtureGroup
{
    public $fixtures = [
        'app.Organizations',
        'app.Workflows',
        'app.Structures',
        'app.Templates',
        'app.Themes',
        'app.Actors',
        'app.Groups',
        'app.ActorGroups',
        'app.ActorsActorGroups',
        'app.Projects',
        'app.ActorsProjects',
        'app.Votes',
        'app.ActorsVotes',
    ];
}
