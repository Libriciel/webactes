<?php
declare(strict_types=1);

namespace App\Test\FixtureGroup;

use App\TestSuite\Fixture\TestFixtureGroup;

class ContainerFixtureGroup extends TestFixtureGroup
{
    public $fixtures = [
        'app.group.Project',
        'app.group.TypeAct',
        'app.Containers',
        'app.group.Act',
    ];
}
