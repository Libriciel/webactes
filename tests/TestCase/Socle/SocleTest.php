<?php
declare(strict_types=1);

namespace App\Test\TestCase\Socle;

use App\Test\TestCase\AbstractTestCase;
use App\Utilities\Socle\Socle;
use App\Utilities\Socle\SocleAlreadyExitsRessourceException;
use App\Utilities\Socle\SocleMissingRessourceException;
use App\Utilities\Socle\SocleRelationMissingException;
use App\Utilities\Socle\SocleSyntaxException;
use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestTrait;

/**
 * get the client secret
 * requests.get("https://mylink.com/auth/admin/realms/{myrealm}/clients/{myclientid}/client-secret", data=data, headers= {"Authorization": "Bearer " + token.get('access_token'), "Content-Type": "application/json"})
 */
class SocleTest extends AbstractTestCase
{
    use IntegrationTestTrait;

    public const XML_DIR = TESTS . 'TestCase' . DS . 'Socle' . DS . 'xml' . DS;

    public $fixtures = [
        'app.group.All',
        'app.GenerateTemplates',
        'app.Typesittings',
    ];

    public function setUp(): void
    {
        $this->skipIfNoSocleConfig();

        parent::setUp();
        $this->locales = Configure::read('App.paths.locales');
        $locales = TESTS . DS . 'test_app' . DS . 'Locale' . DS;
        Configure::write('App.paths.locales', $locales);
        $this->defaultLocale = Configure::read('App.defaultLocale');
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    public function testCreateStructure()
    {
        $xml = file_get_contents(self::XML_DIR . 'organism_new.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var StructuresTable $Structures
         */
        $Structures = $this->fetchTable('Structures');
        $socle = new Socle(null, $Structures);
        $res = $socle->addStructureXml($xml);
        $this->assertNotEmpty($res);

        $newStructure = $Structures->find()->where(['id' => $res['id']])->contain(['Typesacts','Typesittings', 'Pastellfluxtypes', 'Roles'])->first();
        $this->assertNotEmpty($newStructure['typesittings']);
        $this->assertNotEmpty($newStructure['roles']);
        $this->assertNotEmpty($newStructure['pastellfluxtypes']);
    }

    public function testCreateStructureAlreadyExists()
    {
        $xml = file_get_contents(self::XML_DIR . 'organism.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var StructuresTable $Structures
         */
        $Structures = $this->fetchTable('Structures');
        $socle = new Socle(null, $Structures);
        try {
            $socle->addStructureXml($xml);
        } catch (SocleAlreadyExitsRessourceException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>ORGANISM</classType><errorCode>005</errorCode><errorLabel>RESOURCE_ALREADY_EXISTS</errorLabel><errorMessage>La resource existe déjà</errorMessage><methodType>POST</methodType><resourceId>100000005</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(409, $e->getCode());
        }
        $this->assertEquals(2, $Structures->find()->count());
    }

    public function testUpdateStructure()
    {
        $xml = file_get_contents(self::XML_DIR . 'organism.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var StructuresTable $Structures
         */
        $Structures = $this->fetchTable('Structures');
        $socle = new Socle(null, $Structures);
        $res = $socle->updateStructureXml($xml);
        $this->assertNotEmpty($res);
        $this->assertEquals(2, $Structures->find()->count());
    }

    public function testUpdateStructure_notExists()
    {
        $Structures = $this->fetchTable('Structures');
        $xml = file_get_contents(self::XML_DIR . 'organism_new.xml');
        $initialCountUser = $Structures->find()->count();

        $this->assertNotEmpty($xml);

        $socle = new Socle(null, $Structures);
        $res = $socle->updateStructureXml($xml);
        $this->assertEquals($initialCountUser + 1, $Structures->find()->count());
    }

    public function testDeleteStructure()
    {
        /**
         * @var StructuresTable $Structures
         */
        $Structures = $this->fetchTable('Structures');
        $socle = new Socle(null, $Structures);
        $res = $socle->deleteStructure('100000005');
        $this->assertNotEmpty($res);
        $structure = $Structures->find()->where(['id_external' => '100000005'])->first();
        $this->assertEquals(false, $structure->get('active'));
    }

    public function testDeleteStructure_badId()
    {
        /**
         * @var StructuresTable $Structures
         */
        $Structures = $this->fetchTable('Structures');
        $socle = new Socle(null, $Structures);
        try {
            $res = $socle->deleteStructure('99999');
        } catch (SocleMissingRessourceException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>ORGANISM</classType><errorCode>003</errorCode><errorLabel>RESOURCE_MISSING</errorLabel><errorMessage>La ressource spécifiée est introuvable</errorMessage><methodType>DELETE</methodType><resourceId>99999</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(404, $e->getCode());
        }
    }

    public function testCreateAgent()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var UsersTable $Users
         */
        $Users = $this->fetchTable('Users');
        $socle = new Socle($Users, null);
        $res = $socle->addUserXml($xml);
        $this->assertNotEmpty($res);
        $users = $Users->find()->where(['id_external' => $res->get('id_external')])->contain(['Structures', 'Roles'])->toArray();
        $this->assertEquals(1, count($users));
        $this->assertNotEmpty($users[0]['structures']);
        $this->assertNotEmpty($users[0]['roles']);
    }

    public function testCreateAgent_alreadyExists()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_existing.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var UsersTable $Users
         */
        $Users = $this->fetchTable('Users');
        $socle = new Socle($Users, null);
        try {
            $res = $socle->addUserXml($xml);
        } catch (SocleAlreadyExitsRessourceException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>005</errorCode><errorLabel>RESOURCE_ALREADY_EXISTS</errorLabel><errorMessage>La resource existe déjà</errorMessage><methodType>POST</methodType><resourceId>888</resourceId></error>
';
            $this->assertEquals($expected, preg_replace('/<resourceId>[0-9]+<\/resourceId>/', '<resourceId>888</resourceId>', $e->getMessage()));
            $this->assertEquals(409, $e->getCode());
        }
    }

    public function testCreateAgent_MissingData()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_missingData.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var UsersTable $Users
         */
        $Users = $this->fetchTable('Users');
        $socle = new Socle($Users, null);
        try {
            $res = $socle->addUserXml($xml);
        } catch (SocleSyntaxException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>unknown</classType><errorCode>001</errorCode><errorLabel>SYNTAX_ERROR</errorLabel><errorMessage>Syntaxe du message en entrée incorrecte</errorMessage><methodType>unknown</methodType><resourceId>unknown</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(400, $e->getCode());
        }
        $this->assertEquals(11, $Users->find()->count());
    }

    public function testCreateAgent_NotExistsRole()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_notExistsRole.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var UsersTable $Users
         */
        $Users = $this->fetchTable('Users');
        $socle = new Socle($Users, null);
        try {
            $socle->addUserXml($xml);
        } catch (SocleRelationMissingException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>004</errorCode><errorLabel>RELATION_MISSING</errorLabel><errorMessage>Une relation avec l\'entité est manquante</errorMessage><methodType>POST</methodType><resourceId>999</resourceId></error>
';
            $this->assertEquals($expected, preg_replace('/<resourceId>[0-9]+<\/resourceId>/', '<resourceId>999</resourceId>', $e->getMessage()));
            $this->assertEquals(400, $e->getCode());
        }
    }

    public function testCreateAgent_NotExistsStructure()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_notExistsStructure.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var UsersTable $Users
         */
        $Users = $this->fetchTable('Users');
        $socle = new Socle($Users, null);
        try {
            $socle->addUserXml($xml);
        } catch (SocleRelationMissingException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>004</errorCode><errorLabel>RELATION_MISSING</errorLabel><errorMessage>Une relation avec l\'entité est manquante</errorMessage><methodType>POST</methodType><resourceId>999</resourceId></error>
';
            $this->assertEquals($expected, preg_replace('/<resourceId>[0-9]+<\/resourceId>/', '<resourceId>999</resourceId>', $e->getMessage()));
            $this->assertEquals(400, $e->getCode());
        }
    }

    public function testUpdateAgent()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_existing.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var UsersTable $Users
         */
        $Users = $this->fetchTable('Users');
        $socle = new Socle($Users, null);
        $res = $socle->updateUserXml($xml);
        $this->assertNotEmpty($res);
        $users = $Users->find()
            ->where(['id_external' => $res->get('id_external')])
            ->contain(['Structures', 'Roles'])
            ->orderAsc('id')
            ->enableHydration(false)
            ->toArray();
        $this->assertEquals(1, count($users));
        $this->assertNotEmpty($users[0]['structures']);
        $this->assertNotEmpty($users[0]['roles']);
        $this->assertEquals('HEISSLER', $users[0]['lastname']);
        $this->assertEquals(11, $Users->find()->count());
    }

    public function testUpdateAgentNotExists()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var UsersTable $Users
         */
        $Users = $this->fetchTable('Users');
        $initialCountUser = $Users->find()->count();
        $socle = new Socle($Users, null);
        try {
            $res = $socle->updateUserXml($xml);
        } catch (SocleMissingRessourceException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>003</errorCode><errorLabel>RESOURCE_MISSING</errorLabel><errorMessage>La ressource spécifiée est introuvable</errorMessage><methodType>PUT</methodType><resourceId>999</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(404, $e->getCode());
        }

        $this->assertEquals($initialCountUser + 1, $Users->find()->count());
    }

    public function testUpdateAgentFalseRole()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_existingFalseRole.xml');
        $this->assertNotEmpty($xml);
        /**
         * @var UsersTable $Users
         */
        $Users = $this->fetchTable('Users');
        $socle = new Socle($Users, null);
        try {
            $res = $socle->updateUserXml($xml);
        } catch (SocleRelationMissingException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>004</errorCode><errorLabel>RELATION_MISSING</errorLabel><errorMessage>Une relation avec l\'entité est manquante</errorMessage><methodType>PUT</methodType><resourceId>888</resourceId></error>
';
            $this->assertEquals($expected, preg_replace('/<resourceId>[0-9]+<\/resourceId>/', '<resourceId>888</resourceId>', $e->getMessage()));
            $this->assertEquals(400, $e->getCode());
        }
    }

    public function testDeleteUser()
    {
        $this->markTestSkipped('@fixme: La ressource spécifiée est introuvable');
        $Users = $this->fetchTable('Users');
        $socle = new Socle($Users, null);
        $res = $socle->deleteUser('888');
        $this->assertNotEmpty($res);
        $user = $Users->find()->where(['id_external' => '888'])->first();
        $this->assertEquals(false, $user->get('active'));
    }

    public function testDeleteUser_badId()
    {
        $Users = $this->fetchTable('Users');
        $socle = new Socle($Users, null);
        try {
            $res = $socle->deleteUser('9999999');
        } catch (SocleMissingRessourceException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>003</errorCode><errorLabel>RESOURCE_MISSING</errorLabel><errorMessage>La ressource spécifiée est introuvable</errorMessage><methodType>DELETE</methodType><resourceId>9999999</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(404, $e->getCode());
        }
    }
}
