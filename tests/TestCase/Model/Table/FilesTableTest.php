<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Test\TestCase\AbstractTestCase;

/**
 * App\Model\Table\FilesTable Test Case
 */
class FilesTableTest extends AbstractTestCase
{
    /**
     * Test subject
     *
     * @var FilesTable
     */
    public $Files;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Organizations',
        'app.Structures',
        'app.Templates',
        'app.Acts',
        'app.Themes',
        'app.Workflows',
        'app.Projects',
        'app.GenerateTemplates',
        'app.Natures',
        'app.Typesacts',
        'app.Containers',
        'app.Maindocuments',
        'app.Annexes',
        'app.DraftTemplates',
        'app.ProjectTexts',
        'app.Attachments',
        'app.Actors',
        'app.Typesittings',
        'app.Sittings',
        'app.Summons',
        'app.AttachmentsSummons',
        'app.Files',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->Files = $this->fetchTable('Files');
    }

    public function testSaveInFsDuplicateFile()
    {
        $saved = $this->Files->saveInFsDuplicateFile(1, 'draft_template_id', 1, ['project_text_id' => 1], 1);
        $this->assertInstanceOf('\App\Model\Entity\File', $saved);
        $this->assertMatchesRegularExpression(
            '/^\/data\/workspace\/1\/1\/' . date('Y') . '\/project_text_id\/1\/' . static::REGEXP_UUID . '$/',
            $saved->path
        );
        unlink($saved->path);
    }
}
