<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Test\TestCase\AbstractTestCase;
use Cake\I18n\FrozenTime;

/**
 * App\Model\Table\ContainersTable Test Case
 */
class ContainersTableTest extends AbstractTestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ContainersTable
     */
    public $Containers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Project',
        'app.group.Container',
        'app.group.Convocation',
        'app.group.Pastell',
        'app.Workflows',
        'app.Organizations',
        'app.Structures',
        'app.Actors',
        'app.Templates',
        'app.Themes',
        'app.GenerateTemplates',
        'app.Natures',
        'app.Acts',
        'app.Projects',
        'app.Typesacts',
        'app.Typesittings',
        'app.Containers',
        'app.Sittings',
        'app.ContainersSittings',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->Containers = $this->fetchTable('Containers');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Containers);

        parent::tearDown();
    }

    public function testFindById()
    {
        $query = $this->Containers->get(1);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'structure_id' => 1,
            'act_id' => null,
            'project_id' => 1,
            'typesact_id' => 4,
            'tdt_id' => null,
            'created' => '2019-02-11T10:28:20+00:00',
            'modified' => '2019-02-11T10:28:20+00:00',
            'generation_job_id' => null,
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testFindByIdNotSetting()
    {
        $query = $this->Containers->get(27);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 27,
            'structure_id' => 1,
            'act_id' => null,
            'project_id' => 27,
            'typesact_id' => 1,
            'tdt_id' => null,
            'created' => '2019-02-11T10:28:20+00:00',
            'modified' => '2019-02-11T10:28:20+00:00',
            'generation_job_id' => null,
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return void
     */
    public function testgetDateDecisionSitting()
    {
        $date = new FrozenTime(date('Y-m-d 00:00:00'));
        $this->signContainer(1, $date);
        $dateDecision = $this->Containers->getDateDecision($this->getContainer(1));
        $this->assertEquals(new FrozenTime('2019-02-10 15:01:46'), $dateDecision);
    }

    /**
     * @return void
     */
    public function testgetDateDecisionNotSitting()
    {
        $date = new FrozenTime(date('Y-m-d 00:00:00'));
        $this->signContainer(27, $date);
        $dateDecision = $this->Containers->getDateDecision($this->getContainer(27));
        $this->assertEquals($date, $dateDecision);
    }

    /**
     * @param $id
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    private function getContainer($id)
    {
        return $this->Containers->find()
            ->contain(['Projects'])
            ->where(['Containers.id' => $id])
            ->first();
    }

    /**
     * @param int $id
     * @param FrozenTime $date
     * @return void
     */
    private function signContainer(int $id, FrozenTime $date)
    {
        $container = $this->getContainer($id);
        $projectsTable = $this->fetchTable('Projects');
        $project = $projectsTable->findById($container['project']->id)->first();

        $projectsTable->patchEntity($project, [
            'signature_date' => $date,
        ]);
        $projectsTable->save($project);
    }
}
