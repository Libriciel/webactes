<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Test\TestCase\AbstractTestCase;
use Cake\I18n\FrozenDate;

/**
 * App\Model\Table\SequencesTable Test Case
 */
class SequencesTableTest extends AbstractTestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SequencesTable
     */
    public $Sequences;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Sequences',
        'app.Counters',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->Sequences = $this->fetchTable('Sequences');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Sequences);

        parent::tearDown();
    }

    /**
     * @dataProvider dataProviderGetNextValue
     */
    public function testGetNextValue(FrozenDate $date, string $reinit_def, int $sequenceId, int $expected)
    {
        $this->assertEquals($expected, $this->Sequences->getNextValue($date, $reinit_def, $sequenceId));
    }

    public function dataProviderGetNextValue()
    {
        return [
            [FrozenDate::parse('2020-07-31'), '#AAAA#', 1, 2],
            [FrozenDate::parse('2023-07-31'), '#AAAA#', 1, 1],
        ];
    }

    /**
     * @dataProvider dataProviderReInit
     */
    public function testReInit(int $id, string $reinitDef, FrozenDate $date, array $expected)
    {
        $this->Sequences->reInit($id, $reinitDef, $date);
        $actual = $this->Sequences
            ->find()
            ->select(['sequence_num', 'starting_date', 'expiry_date'])
            ->where(['id' => $id])
            ->enableHydration(false)
            ->first();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderReInit()
    {
        return [
            [
                1,
                '#AAAA#',
                FrozenDate::parse('2020-05-15'),
                [
                    'sequence_num' => 1,
                    'starting_date' => FrozenDate::parse('2020-01-01'),
                    'expiry_date' => FrozenDate::parse('2020-12-31'),
                ],
            ],
            [
                1,
                '#AAAA#',
                FrozenDate::parse('2021-05-15'),
                [
                    'sequence_num' => 1,
                    'starting_date' => FrozenDate::parse('2021-01-01'),
                    'expiry_date' => FrozenDate::parse('2021-12-31'),
                ],
            ],
        ];
    }

    /**
     * @dataProvider dataProviderCheckIfReinit
     */
    public function testCheckIfReinit(FrozenDate $startingDate, FrozenDate $expiryDate, string $reinitDef, FrozenDate $date, bool $expected)
    {
        $this->assertEquals($expected, $this->Sequences->checkIfReinit($startingDate, $expiryDate, $reinitDef, $date));
    }

    public function dataProviderCheckIfReinit()
    {
        return [
            [FrozenDate::parse('2023-07-31'), FrozenDate::parse('2023-07-31'), '#AAAA#', FrozenDate::parse('2023-07-31'), false],
            [FrozenDate::parse('2023-07-31'), FrozenDate::parse('2023-07-31'), '#AAAA#', FrozenDate::parse('2023-08-01'), true],
            [FrozenDate::parse('2023-07-31'), FrozenDate::parse('2023-07-31'), '', FrozenDate::parse('2023-08-01'), false],
        ];
    }

    /**
     * @dataProvider dataProviderCalculatesNewDates
     */
    public function testCalculatesNewDates(string $reinitDef, FrozenDate $date, array $expected)
    {
        $this->assertEquals($expected, $this->Sequences->calculatesNewDates($reinitDef, $date));
    }

    public function dataProviderCalculatesNewDates()
    {
        return [
            ['#AAAA#', FrozenDate::parse('2023-07-31 10:04:55'), ['starting_date' => '2023-01-01', 'expiry_date' => '2023-12-31']],
            ['#AA#', FrozenDate::parse('2023-07-31 10:04:55'), ['starting_date' => '2023-01-01', 'expiry_date' => '2023-12-31']],
            ['#MM#', FrozenDate::parse('2023-07-31 10:04:55'), ['starting_date' => '2023-07-01', 'expiry_date' => '2023-07-31']],
            ['#M#', FrozenDate::parse('2023-07-31 10:04:55'), ['starting_date' => '2023-07-01', 'expiry_date' => '2023-07-31']],
            ['#JJ#', FrozenDate::parse('2023-07-31 10:04:55'), ['starting_date' => '2023-07-31', 'expiry_date' => '2023-07-31']],
            ['#J#', FrozenDate::parse('2023-07-31 10:04:55'), ['starting_date' => '2023-07-31', 'expiry_date' => '2023-07-31']],
        ];
    }
}
