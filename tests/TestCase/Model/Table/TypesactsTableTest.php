<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Test\TestCase\AbstractTestCase;

/**
 * App\Model\Table\TypesactsTable Test Case
 */
class TypesactsTableTest extends AbstractTestCase
{
    /**
     * Test subject
     *
     * @var TypesactsTable
     */
    public $Typesacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.TypeAct',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->Typesacts = $this->fetchTable('Typesacts');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Typesacts);

        parent::tearDown();
    }

    /**
     * ...
     *
     * @return void
     */
    public function testGetIsDeliberatingFalse()
    {
        $query = $this->Typesacts->get(1);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'structure_id' => 1,
            'nature_id' => 4,
            'name' => 'Autre',
            'created' => '2019-02-20T10:34:58+00:00',
            'modified' => '2019-02-20T10:34:58+00:00',
            'istdt' => false,
            'active' => true,
            'isdefault' => false,
            'isdeliberating' => false,
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deactivate' => [
                    'value' => true,
                    'data' => null,
                ],
            ],
            'counter_id' => 1,
            'generate_template_project_id' => 1,
            'generate_template_act_id' => 2,
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * ...
     *
     * @return void
     */
    public function testGetIsDeliberatingFalseWithSpecificFields()
    {
        $query = $this->Typesacts->get(1, ['fields' => ['id']]);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deactivate' => [
                    'value' => true,
                    'data' => null,
                ],
            ],
        ];

        $this->assertEquals($expected, $actual);
    }

    /**
     * ...
     *
     * @return void
     */
    public function testGetIsDeliberatingFalseWithSpecificFieldsAndIsdeliberating()
    {
        $query = $this->Typesacts->get(1, ['fields' => ['id', 'isdeliberating']]);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'isdeliberating' => false,
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deactivate' => [
                    'value' => true,
                    'data' => null,
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * ...
     *
     * @return void
     */
    public function testGetIsDeliberatingFalseWithSpecificFieldsAndContain()
    {
        $query = $this->Typesacts->get(1, ['contain' => ['Natures']]);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'structure_id' => 1,
            'nature_id' => 4,
            'name' => 'Autre',
            'created' => '2019-02-20T10:34:58+00:00',
            'modified' => '2019-02-20T10:34:58+00:00',
            'istdt' => false,
            'active' => true,
            'isdefault' => false,
            'isdeliberating' => false,
            'nature' => [
                'id' => 4,
                'structure_id' => 1,
                'created' => '2019-05-28T14:38:01+00:00',
                'modified' => '2019-05-28T14:38:01+00:00',
                'codenatureacte' => '6',
                'libelle' => 'Autres',
                'typeabrege' => 'AU',
            ],
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deactivate' => [
                    'value' => true,
                    'data' => null,
                ],
            ],
            'counter_id' => 1,
            'generate_template_project_id' => 1,
            'generate_template_act_id' => 2,
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * ...
     *
     * @return void
     */
    public function testGetIsDeliberatingFalseWithSpecificFieldsAndIsdeliberatingAndContain()
    {
        $query = $this->Typesacts->get(1, ['fields' => ['id', 'isdeliberating']]);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'isdeliberating' => false,
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deactivate' => [
                    'value' => true,
                    'data' => null,
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test get method
     *
     * @return void
     */
    public function testGetIsDeliberatingTrue()
    {
        $query = $this->Typesacts->get(4);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 4,
            'structure_id' => 1,
            'nature_id' => 1,
            'name' => 'Délibération',
            'created' => '2019-02-20T10:34:58+00:00',
            'modified' => '2019-02-20T10:34:58+00:00',
            'istdt' => true,
            'active' => true,
            'isdefault' => true,
            'isdeliberating' => true,
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deactivate' => [
                    'value' => true,
                    'data' => null,
                ],
            ],
            'counter_id' => 1,
            'generate_template_project_id' => 1,
            'generate_template_act_id' => 2,
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testGetNatureWithTypesactIsDeliberatingTrue()
    {
        $options = ['contain' => ['Typesacts' => ['sort' => ['id']]]];
        $query = $this->Typesacts->Natures->get(1, $options);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'structure_id' => 1,
            'created' => '2019-05-28T14:38:01+00:00',
            'modified' => '2019-05-28T14:38:01+00:00',
            'codenatureacte' => '1',
            'libelle' => 'Délibérations',
            'typeabrege' => 'DE',
            'typesacts' => [
                [
                    'id' => 4,
                    'structure_id' => 1,
                    'nature_id' => 1,
                    'name' => 'Délibération',
                    'created' => '2019-02-20T10:34:58+00:00',
                    'modified' => '2019-02-20T10:34:58+00:00',
                    'istdt' => true,
                    'active' => true,
                    'isdefault' => true,
                    'isdeliberating' => true,
                    'availableActions' => [
                        'can_be_edited' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deleted' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deactivate' => [
                            'value' => true,
                            'data' => null,
                        ],
                    ],
                    'counter_id' => 1,
                    'generate_template_project_id' => 1,
                    'generate_template_act_id' => 2,
                ],
                [
                    'id' => 8,
                    'structure_id' => 2,
                    'nature_id' => 1,
                    'name' => 'Délibération',
                    'created' => '2019-02-20T10:34:58+00:00',
                    'modified' => '2019-02-20T10:34:58+00:00',
                    'istdt' => true,
                    'active' => true,
                    'isdefault' => true,
                    'isdeliberating' => true,
                    'availableActions' => [
                        'can_be_edited' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deleted' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deactivate' => [
                            'value' => true,
                            'data' => null,
                        ],
                    ],
                    'counter_id' => 1,
                    'generate_template_project_id' => null,
                    'generate_template_act_id' => null,
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testGetNatureWithTypesactIsDeliberatingTrueSpecificFields()
    {
        $options = ['contain' => ['Typesacts' => ['fields' => ['id', 'nature_id'], 'sort' => ['id']]]];
        $query = $this->Typesacts->Natures->get(1, $options);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'structure_id' => 1,
            'created' => '2019-05-28T14:38:01+00:00',
            'modified' => '2019-05-28T14:38:01+00:00',
            'codenatureacte' => '1',
            'libelle' => 'Délibérations',
            'typeabrege' => 'DE',
            'typesacts' => [
                [
                    'id' => 4,
                    'nature_id' => 1,
                    'availableActions' => [
                        'can_be_edited' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deleted' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deactivate' => [
                            'value' => true,
                            'data' => null,
                        ],
                    ],
                ],
                [
                    'id' => 8,
                    'nature_id' => 1,
                    'availableActions' => [
                        'can_be_edited' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deleted' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deactivate' => [
                            'value' => true,
                            'data' => null,
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testGetNatureWithTypesactIsDeliberatingTrueSpecificFieldsAndIsDeliberating()
    {
        $options = ['contain' => ['Typesacts' => ['fields' => ['id', 'nature_id', 'isdeliberating'], 'sort' => ['id']]]];
        $query = $this->Typesacts->Natures->get(1, $options);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'structure_id' => 1,
            'created' => '2019-05-28T14:38:01+00:00',
            'modified' => '2019-05-28T14:38:01+00:00',
            'codenatureacte' => '1',
            'libelle' => 'Délibérations',
            'typeabrege' => 'DE',
            'typesacts' => [
                [
                    'id' => 4,
                    'nature_id' => 1,
                    'isdeliberating' => true,
                    'availableActions' => [
                        'can_be_edited' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deleted' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deactivate' => [
                            'value' => true,
                            'data' => null,
                        ],
                    ],
                ],
                [
                    'id' => 8,
                    'nature_id' => 1,
                    'isdeliberating' => true,
                    'availableActions' => [
                        'can_be_edited' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deleted' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deactivate' => [
                            'value' => true,
                            'data' => null,
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testQueryFindAllWithNoField()
    {
        $query = $this->Typesacts->find()->all();
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            [
                'id' => 1,
                'structure_id' => 1,
                'nature_id' => 4,
                'name' => 'Autre',
                'created' => '2019-02-20T10:34:58+00:00',
                'modified' => '2019-02-20T10:34:58+00:00',
                'istdt' => false,
                'active' => true,
                'isdefault' => false,
                'isdeliberating' => false,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 1,
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
            ],
            [
                'id' => 2,
                'structure_id' => 1,
                'nature_id' => 2,
                'name' => 'Arrêté réglementaire',
                'created' => '2019-02-20T10:34:58+00:00',
                'modified' => '2019-02-20T10:34:58+00:00',
                'istdt' => true,
                'active' => true,
                'isdefault' => false,
                'isdeliberating' => false,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 2,
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
            ],
            [
                'id' => 3,
                'structure_id' => 1,
                'nature_id' => 3,
                'name' => 'Contrat et convention',
                'created' => '2019-02-20T10:34:58+00:00',
                'modified' => '2019-02-20T10:34:58+00:00',
                'istdt' => true,
                'active' => true,
                'isdefault' => false,
                'isdeliberating' => false,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 3,
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
            ],
            [
                'id' => 4,
                'structure_id' => 1,
                'nature_id' => 1,
                'name' => 'Délibération',
                'created' => '2019-02-20T10:34:58+00:00',
                'modified' => '2019-02-20T10:34:58+00:00',
                'istdt' => true,
                'active' => true,
                'isdefault' => true,
                'isdeliberating' => true,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 1,
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
            ],
            [
                'id' => 5,
                'structure_id' => 2,
                'nature_id' => 4,
                'name' => 'Autre',
                'created' => '2019-02-20T10:34:58+00:00',
                'modified' => '2019-02-20T10:34:58+00:00',
                'istdt' => false,
                'active' => true,
                'isdefault' => false,
                'isdeliberating' => false,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 2,
                'generate_template_project_id' => null,
                'generate_template_act_id' => null,
            ],
            [
                'id' => 6,
                'structure_id' => 2,
                'nature_id' => 2,
                'name' => 'Arrêté réglementaire',
                'created' => '2019-02-20T10:34:58+00:00',
                'modified' => '2019-02-20T10:34:58+00:00',
                'istdt' => true,
                'active' => true,
                'isdefault' => false,
                'isdeliberating' => false,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 3,
                'generate_template_project_id' => null,
                'generate_template_act_id' => null,
            ],
            [
                'id' => 7,
                'structure_id' => 2,
                'nature_id' => 3,
                'name' => 'Contrat et convention',
                'created' => '2019-02-20T10:34:58+00:00',
                'modified' => '2019-02-20T10:34:58+00:00',
                'istdt' => true,
                'active' => true,
                'isdefault' => false,
                'isdeliberating' => false,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 2,
                'generate_template_project_id' => null,
                'generate_template_act_id' => null,
            ],
            [
                'id' => 8,
                'structure_id' => 2,
                'nature_id' => 1,
                'name' => 'Délibération',
                'created' => '2019-02-20T10:34:58+00:00',
                'modified' => '2019-02-20T10:34:58+00:00',
                'istdt' => true,
                'active' => true,
                'isdefault' => true,
                'isdeliberating' => true,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 1,
                'generate_template_project_id' => null,
                'generate_template_act_id' => null,
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testQueryFindAllWithSpecificFields()
    {
        $query = $this->Typesacts->find()->select(['id', 'isdeliberating'])->first();
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'isdeliberating' => false,
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deactivate' => [
                    'value' => true,
                    'data' => null,
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testQueryFindAllWithTableAsFields()
    {
        $query = $this->Typesacts->find()->select($this->Typesacts)->first();
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'structure_id' => 1,
            'nature_id' => 4,
            'name' => 'Autre',
            'created' => '2019-02-20T10:34:58+00:00',
            'modified' => '2019-02-20T10:34:58+00:00',
            'istdt' => false,
            'active' => true,
            'isdefault' => false,
            'isdeliberating' => false,
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deactivate' => [
                    'value' => true,
                    'data' => null,
                ],
            ],
            'counter_id' => 1,
            'generate_template_project_id' => 1,
            'generate_template_act_id' => 2,
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testQueryFindWithJoinsAndSpecificFieldsWithAlias()
    {
        $query = $this->Typesacts
            ->find()
            ->select(
                [
                    'Typesacts.id',
                    'Typesacts.isdeliberating',
                    'Natures.id',
                ]
            )
            ->leftJoin('Natures')
            ->first();
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'isdeliberating' => false,
            'Natures' => [
                'id' => 1,
            ],
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deactivate' => [
                    'value' => true,
                    'data' => null,
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getAssociationFullConditions method for the Natures belongsTo association
     *
     * @return void
     */
    public function testGetAssociationFullConditionsBelongsToNatures()
    {
        $actual = $this->Typesacts->getAssociationFullConditions('Natures');
        $expected = ['Typesacts.nature_id = Natures.id'];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getAssociationFullConditions method for the Containers hasMany association
     *
     * @return void
     */
    public function testGetAssociationFullConditionsHasManyContainers()
    {
        $actual = $this->Typesacts->getAssociationFullConditions('Containers');
        $expected = ['Typesacts.id = Containers.typesact_id'];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test getAssociationFullConditions method for the Typesittings belongsToMany association
     *
     * @return void
     */
    public function testGetAssociationFullConditionsBelongsToManyTypesittings()
    {
        $actual = $this->Typesacts->getAssociationFullConditions('TypesactsTypesittings');
        $expected = ['Typesacts.id = TypesactsTypesittings.typesact_id'];
        $this->assertEquals($expected, $actual);
    }
}
