<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Test\TestCase\AbstractTestCase;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\FrozenDate;

/**
 * App\Model\Table\CountersTable Test Case
 */
class CountersTableTest extends AbstractTestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CountersTable
     */
    public $Counters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Counters',
        'app.Sequences',
        'app.group.Auth',
        'app.group.TypeAct',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->Counters = $this->fetchTable('Counters');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Counters);

        parent::tearDown();
    }

    public function testFindById()
    {
        $query = $this->Counters->get(1);
        $actual = json_decode(json_encode($query->toArray()), true);
        $expected = [
            'id' => 1,
            'structure_id' => 1,
            'name' => 'Compteurs délib',
            'comment' => 'Un système de compteur pour les délibérations',
            'counter_def' => '#AAAA#_#0000#',
            'sequence_id' => 4,
            'reinit_def' => '#AAAA#',
            'created' => '2020-11-27T21:45:39+00:00',
            'modified' => '2020-11-27T21:45:39+00:00',
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => false,
                    'data' => 'Le compteur est utilisé par un type d\'acte. Suppression impossible.',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Counter with reinitialization year, not to be reinitialize
     *
     * @version V2
     */
    public function testGeneratesCounterYearWithoutReinitialize()
    {
        $actual = $this->Counters->generatesCounter(1, date('Y-m-d', strtotime('2020-02-15')));
        $expected = '2020_0002';
        $this->assertEquals($expected, $actual);

        $actual = $this->Counters->generatesCounter(1, date('Y-m-d', strtotime('2020-02-15')));
        $expected = '2020_0003';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Counter with reinitialization month, not to be reinitialize
     *
     * @version V2
     */
    public function testGeneratesCounterMonthWithoutReinitialize()
    {
        $actual = $this->Counters->generatesCounter(4, date('Y-m-d', strtotime('2020-02-02')));
        $expected = 'DELIB20200202_2';
        $this->assertEquals($expected, $actual);

        $actual = $this->Counters->generatesCounter(4, date('Y-m-d', strtotime('2020-02-02')));
        $expected = 'DELIB20200202_3';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Counter with reinitialization day, not to be reinitialize
     *
     * @version V2
     */
    public function testGeneratesCounterDayWithoutReinitialize()
    {
        $actual = $this->Counters->generatesCounter(2, date('Y-m-d', strtotime('2020-02-01')));
        $expected = 'COM_0002';
        $this->assertEquals($expected, $actual);

        $actual = $this->Counters->generatesCounter(2, date('Y-m-d', strtotime('2020-02-01')));
        $expected = 'COM_0003';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Counter with reinitialization year, to be reinitialized
     *
     * @version V2
     */
    public function testGeneratesCounterYearToBeReinitialized()
    {
        $actual = $this->Counters->generatesCounter(1, date('Y-m-d', strtotime('2020-12-31')));
        $expected = '2020_0002';
        $this->assertEquals($expected, $actual);

        $actual = $this->Counters->generatesCounter(1, date('Y-m-d', strtotime('2021-01-01')));
        $expected = '2021_0001';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Counter with reinitialization month, to be reinitialized
     *
     * @version V2
     */
    public function testGeneratesCounterMonthToBeReinitialized()
    {
        $actual = $this->Counters->generatesCounter(4, date('Y-m-d', strtotime('2020-12-31')));
        $expected = 'DELIB20201231_2';
        $this->assertEquals($expected, $actual);

        $actual = $this->Counters->generatesCounter(4, date('Y-m-d', strtotime('2021-01-01')));
        $expected = 'DELIB20210101_1';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Counter with day reinitialization to be reinitialized
     *
     * @version V2
     */
    public function testGeneratesCounterDayToBeReinitialized()
    {
        $actual = $this->Counters->generatesCounter(2, date('Y-m-d', strtotime('2020-12-31')));
        $expected = 'COM_0002';
        $this->assertEquals($expected, $actual);

        $actual = $this->Counters->generatesCounter(2, date('Y-m-d', strtotime('2021-01-01')));
        $expected = 'COM_0001';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Counter with year reinitialization already been reinitialized by another counter
     *
     * @version V2
     */
    public function testGeneratesCounterYearAlreadyReinitializedByAnotherCounter()
    {
        $actual = $this->Counters->generatesCounter(6, date('Y-m-d', strtotime('2021-01-01')));
        $expected = 'D20210101_1';
        $this->assertEquals($expected, $actual);

        $actual = $this->Counters->generatesCounter(7, date('Y-m-d', strtotime('2021-01-01')));
        $expected = 'D20210101_2';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Counter with month reinitialization already been reinitialized by another counter
     *
     * @version V2
     */
    public function testGeneratesCounterMonthAlreadyReinitializedByAnotherCounter()
    {
        $actual = $this->Counters->generatesCounter(8, date('Y-m-d', strtotime('2021-01-01')));
        $expected = '2021_0001';
        $this->assertEquals($expected, $actual);

        $actual = $this->Counters->generatesCounter(9, date('Y-m-d', strtotime('2021-01-01')));
        $expected = 'D20210101_2';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Counter with day reinitialization already been reinitialized by another counter
     *
     * @version V2
     */
    public function testGeneratesCounterDayAlreadyReinitializedByAnotherCounter()
    {
        $actual = $this->Counters->generatesCounter(10, date('Y-m-d', strtotime('2021-02-01')));
        $expected = '2021_0001';
        $this->assertEquals($expected, $actual);

        $actual = $this->Counters->generatesCounter(11, date('Y-m-d', strtotime('2021-02-01')));
        $expected = 'D20210201_2';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Generate counter with previous year
     *
     * @version V2
     */
    public function testGeneratesCounterPreviousYearWithoutReinitializeNotFoundException()
    {
        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage('Dernière réinitialisation : 01/01/2020, date demandée 01/01/2019');
        $this->Counters->generatesCounter(1, '2019-01-01');
        $this->assertTextEquals('Dernière réinitialisation : 2020-01-01, date demandée 2019-01-01');
    }

    /**
     * Generates counter with unexisting id
     *
     * @version V2
     */
    public function testGeneratesUnexistingCounterId()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "counters"');
        $this->Counters->generatesCounter(1500000, '2020-12-10');
    }

    /**
     * @dataProvider dataProviderWriteCounter
     */
    public function testWriteCounter(string $counterDef, FrozenDate $date, int $sequence_num, string $expected)
    {
        $this->assertEquals($expected, $this->Counters->writeCounter($counterDef, $date, $sequence_num));
    }

    public function dataProviderWriteCounter()
    {
        return [
            ['1ATDT#000#', new FrozenDate('2023-07-31 09:27:10'), 34, '1ATDT034'],
            ['1DEL#AA##MM##JJ##SSS#', new FrozenDate('2023-07-31 09:27:10'), 34, '1DEL230731_34'],
        ];
    }

    /**
     * @dataProvider dataTestValidationCounterDef
     * @param string $counterDef counterDef
     * @param bool $valid valid
     * @return void
     */
    public function testValidationCounterDef(string $counterDef, bool $valid)
    {
        $data = ['counter_def' => $counterDef, 'name' => 'foo', 'reinit_def' => '#AAAA#'];
        $counter = $this->Counters->newEntity($data);
        $errors = $counter->getErrors();
        if ($valid) {
            $this->assertEmpty($errors['counter_def'] ?? null, sprintf('%s devrait être acceptée', $counterDef));
        } else {
            $this->assertNotEmpty(
                $errors['counter_def'] ?? null,
                sprintf('%s ne devrait pas être acceptée', $counterDef)
            );
        }
    }

    /**
     * @return array[]
     */
    public function dataTestValidationCounterDef()
    {
        return [
            ['_delib_#0000#_*', false],
            ['DEL_#AAAA#_#000#', true],
            ['delib_#0000#_*', false],
            ['DELIB_#S#', true],
            ['DELIB_#SS#', true],
            ['#S#_DELIB', true],
            ['#SS#_DELIB', false],
            ['#SSS#_DELIB', false],
            ['%%PD¨µZPµ¨ZPµ¨F', false],
            ['TYUTUYT#SSS#_', false],
            ['ATDT#SSSS##MM##JJ##000#', true],
            ['YUZ_R24#SS##00##M##J##JJ##SSSS#', true],
            ['1DEL_#SSS#_#p#', true],
            ['RC5_#SSSS#', true],
        ];
    }
}
