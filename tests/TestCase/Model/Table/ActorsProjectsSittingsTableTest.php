<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Test\Mock\FlowableWrapperMock;
use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Utility\Hash;

class ActorsProjectsSittingsTableTest extends AbstractTestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ActorsProjectsSittingsTable
     */
    public $actorsProjectsSittings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Sitting',
        'app.group.Actor',
        'app.group.TypeAct',
        'app.group.Container',
        'app.group.Pastell',
        'app.Stateacts',
        'app.Statesittings',
        'app.ActorGroups',
        'app.Workflows',
        'app.Organizations',
        'app.Structures',
        'app.Templates',
        'app.Themes',
        'app.Natures',
        'app.Acts',
        'app.Actors',
        'app.Projects',
        'app.GenerateTemplates',
        'app.Typesittings',
        'app.Votes',
        'app.ActorsVotes',
        'app.Sittings',
        'app.Typesacts',
        'app.ActorGroupsTypesittings',
        'app.ActorsProjectsSittings',
        'app.TypesactsTypesittings',
        'app.Containers',
        'app.ContainersStateacts',
        'app.ContainersSittings',
        'app.Annexes',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        $this->ActorsProjectsSittings = $this->fetchTable('ActorsProjectsSittings');

        Configure::write('KEYCLOAK_DEV_MODE', true);
        Configure::write('Flowable.wrapper_mock', '\App\Test\Mock\FlowableWrapperMock');

        FlowableWrapperMock::reset();
    }

    public function testGetPrevious()
    {
        $actorsProjectsSittings = $this->ActorsProjectsSittings->getPrevious(2, 7)
            ->orderAsc('ActorsProjectsSittings.id')
            ->toArray();
        $this->assertEquals([4, 5, 6], Hash::extract($actorsProjectsSittings, '{n}.id'));
    }

    public function testGetPreviousWithNoPreviousProject()
    {
        $actorsProjectsSittings = $this->ActorsProjectsSittings->getPrevious(2, 6)
            ->orderAsc('ActorsProjectsSittings.id')
            ->toArray();
        $this->assertEquals([], Hash::extract($actorsProjectsSittings, '{n}.id'));
    }
}
