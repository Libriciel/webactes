<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectsTable;
use App\Test\TestCase\AbstractTestCase;

/**
 * App\Model\Table\TypesactsTable Test Case
 */
class ProjectsTableTest extends AbstractTestCase
{
    public $fixtures = [
        'app.group.Auth',
        'app.group.Pastell',
        'app.group.Container',
        'app.group.Project',
        'app.group.File',
        'app.group.TypeAct',
        'app.group.Sitting',
        'app.group.Actor',
        'app.Organizations',
        'app.Structures',
        'app.Users',
        'app.Stateacts',
        'app.UsersInstances',
        'app.Notifications',
        'app.DraftTemplateTypes',
        'app.Counters',
        'app.Sequences',
        'app.Generationerrors',
        'app.ConnectorTypes',
        'app.Workflows',
        'app.Natures',
        'app.DraftTemplates',
        'app.Actors',
        'app.GenerateTemplates',
        'app.Templates',
        'app.Themes',
        'app.Typespiecesjointes',
        'app.Matieres',
        'app.Systemlogs',
        'app.NotificationsUsers',
        'app.Connecteurs',
        'app.Typesacts',
        'app.Projects',
        'app.Acts',
        'app.Typesittings',
        'app.DraftTemplatesTypesacts',
        'app.Containers',
        'app.Sittings',
        'app.ContainersStateacts',
        'app.ContainersSittings',
        'app.Histories',
        'app.ActorsProjectsSittings',
        'app.Maindocuments',
        'app.StructuresUsers',
        'app.Roles',
        'app.RolesUsers',
    ];

    /**
     * Data provider for getMainMenu.
     */
    public function mainMenuProvider()
    {
        return [
            'Draft or Refused as Creator' => [
                1, // project ID
                4, // Redactor with id 4
                ProjectsTable::DRAFT, // expected result
            ],
            'Draft or Refused as Admin' => [
                1,
                1, // Admin with id 1
                ProjectsTable::DRAFT,
            ],
            'Draft or Refused as General Secretariat' => [
                1,
                3, // General Secretariat with id 3
                ProjectsTable::DRAFT,
            ],
            'Validated as Admin' => [
                11,
                1, // Admin with id 1
                ProjectsTable::VALIDATED,
            ],
            'Validated as General Secretariat' => [
                11,
                3, // General Secretariat with id 3
                ProjectsTable::VALIDATED,
            ],
            'Validating as General Secretariat' => [
                3,
                3, // General Secretariat with id 3
                ProjectsTable::VALIDATING,
            ],
            'DECLARE_SIGNED SIGNED as General Secretariat' => [
                27, // Act non TDT with id 27
                3, // General Secretariat with id 3
                ProjectsTable::ACT,
            ],
            'PARAPHEUR_SIGNED as General Secretariat' => [
                28,
                3, // General Secretariat with id 3
                ProjectsTable::TO_BE_TRANSMITTED,
            ],
            'TO_ORDER as General Secretariat' => [
                30,
                3, // General Secretariat with id 3
                ProjectsTable::TRANSMISSION_READY,
            ],
            'PENDING_TDT as General Secretariat' => [
                31,
                3, // General Secretariat with id 3
                ProjectsTable::ACT,
            ],
            'ACQUITTED as General Secretariat' => [
                32,
                3, // General Secretariat with id 3
                ProjectsTable::ACT,
            ],
        ];
    }

    /**
     * @dataProvider mainMenuProvider
     */
    public function testGetMainMenu(int $projectId, int $userId, string $expected): void
    {
        $result = $this->fetchTable('Projects')->getMainMenu($projectId, $this->switchUser($userId));
        $this->assertEquals($expected, $result);
    }
}
