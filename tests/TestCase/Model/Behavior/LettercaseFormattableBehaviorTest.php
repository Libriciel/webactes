<?php
declare(strict_types=1);

namespace Database\Test\TestCase\Model\Behavior;

use App\Test\TestCase\AbstractTestCase;

class LettercaseFormattableBehaviorTest extends AbstractTestCase
{
    public $fixtures = [
        'app.Organizations',
        'app.Users',
    ];

    /**
     * @var \App\Model\Table\UsersTable|null
     */
    public $Users = null;

    public function setUp(): void
    {
        parent::setUp();

        $this->Users = $this->fetchTable('Users');
        $this->Users->removeBehavior('LettercaseFormattable');
    }

    public function tearDown(): void
    {
        $this->Users->removeBehavior('LettercaseFormattable');
        unset($this->Users);
        parent::tearDown();
    }

    public function dataProviderBeforeMarshal(): array
    {
        return [
            ['lower', 'ÉMILIE-FRANÇOISE', 'émilie-françoise'],
            ['lower_noaccents', 'ÉMILIE-FRANÇOISE', 'emilie-francoise'],
            ['title', 'émilie-françoise', 'Émilie-Françoise'],
            ['title_noaccents', 'émilie-françoise', 'Emilie-Francoise'],
            ['upper', 'émilie-françoise', 'ÉMILIE-FRANÇOISE'],
            ['upper_noaccents', 'émilie-françoise', 'EMILIE-FRANCOISE'],
        ];
    }

    /**
     * @dataProvider dataProviderBeforeMarshal
     * @param $key The key for the firstname configuration (lower, lower_noaccents, ...)
     * @param $value The initial value for firstname
     * @param $expected The expected value for firstname after marshalling
     */
    public function testBeforeMarshal($key, $value, $expected)
    {
        $this->Users->addBehavior('LettercaseFormattable', [$key => ['firstname']]);

        $user = $this->Users->newEntity(['firstname' => $value]);
        $this->assertEquals($expected, $user->firstname);
    }
}
