<?php
declare(strict_types=1);

namespace Database\Test\TestCase\Model\Behavior;

use App\Test\TestCase\AbstractTestCase;
use Cake\I18n\FrozenTime;
use DateTimeInterface;

class DateFormattableBehaviorTest extends AbstractTestCase
{
    public $fixtures = [
        'app.group.Auth',
        'app.group.Project',
        'app.group.Actor',
        'app.GenerateTemplates',
    ];

    /**
     * @var Table|null
     */
    public $Projects = null;

    public function setUp(): void
    {
        parent::setUp();

        $this->Projects = $this->fetchTable('Projects');

        $this->Projects->addBehavior('DateFormattable');//@fixme: s'il n'est pas déjà chargé
    }

    public function tearDown(): void
    {
        unset($this->Projects);
        parent::tearDown();
    }

    /**
     * Data provider for the testBeforeMarshal method
     *
     * @return array
     */
    public function providerTestBeforeMarshal(): array
    {
        return [
            ['2019-06-07', '2019-06-07'],
            ['2019-05-28T22:00:00.000Z', '2019-05-28'],
            ['2019-05-03T22:00:00.000Z', '2019-05-03'],
            ['2019-05-28T', '2019-05-28'],
            ['coucou', null],
            [new FrozenTime('2023-04-03'), '2023-04-03'],
        ];
    }

    /**
     * @dataProvider providerTestBeforeMarshal
     * @param        $date
     * @param        $expected
     */
    public function testBeforeMarshal($date, $expected)
    {
        $data = [
            'signature_date' => $date,
        ];

        $project = $this->Projects->newEntity($data);

        if ($project->signature_date instanceof DateTimeInterface) {
            $actual = $project->signature_date->format('Y-m-d');
        } else {
            $actual = $project->signature_date;
        }

        $this->assertEquals($expected, $actual);
    }
}
