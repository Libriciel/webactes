<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Entity;

use App\Test\TestCase\AbstractTestCase;
use App\TestSuite\LoadOncePerClassFixturesTrait;

class ProjectTest extends AbstractTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * Test subject
     *
     * @var ProjectsTable
     */
    public $Projects;

    /**
     * @var string[]
     */
    public $fixtures = [
        'app.Organizations',
        'app.Stateacts',
        'app.Structures',
        'app.Templates',
        'app.Themes',
        'app.Workflows',
        'app.Acts',
        'app.GenerateTemplates',
        'app.Natures',
        'app.Projects',
        'app.StructureSettings',
        'app.Typesacts',
        'app.Containers',
        'app.ContainersStateacts',
    ];

    /**
     * @var array[]
     */
    protected $availableActionsNone = [
        'can_be_send_to_i_parapheur' => [
            'value' => false,
            'data' => 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
        ],
        'can_be_send_to_manual_signature' => [
            'value' => false,
            'data' => 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
        ],
        'can_be_send_to_workflow' => [
            'value' => false,
            'data' => 'Le projet n\'est pas dans l\'état "Brouillon" ou "Refusé"',
        ],
        'can_be_voted' => [
            'value' => false,
            'data' => 'Le projet a un état qui ne permet pas de le voter.',
        ],
        'can_generate_act_number' => [
            'value' => false,
            'data' => 'Le numéro d\'acte ne peu pas être généré',
        ],
        'check_signing' => [
            'value' => false,
            'data' => 'Le projet n\'est pas dans l\'état "En cours de signature l\'i-parapheur"',
        ],
        'check_refused' => [
            'value' => false,
            'data' => 'Le projet n\'est pas dans l\'état "Refusé par l\'i-parapheur"',
        ],
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->Projects = $this->fetchTable('Projects');
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactBrouillon
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactBrouillon(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $expectedAvailableActions = [
            'can_be_send_to_i_parapheur' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
            ],
            'can_be_send_to_manual_signature' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
            ],
            'can_be_send_to_workflow' => [
                'value' => true,
                'data' => null,
            ],
            'can_be_voted' => [
                'value' => $isdeliberating,
                'data' => $isdeliberating ? '' : 'Le projet a un état qui ne permet pas de le voter.',
            ],
            'can_generate_act_number' => [
                'value' => false,
                'data' => 'Le numéro d\'acte ne peu pas être généré',
            ],
            'check_signing' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "En cours de signature l\'i-parapheur"',
            ],
            'check_refused' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Refusé par l\'i-parapheur"',
            ],
        ];

        $this->assertEquals($expectedAvailableActions, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactBrouillon()
    {
        return [
            // Mêmes résultats pour les projets, d'id 1 et 2 (type d'acte délibérant)
            [1, true],
            // Mêmes résultats pour les projets, d'id 21 et 22 (type d'acte non délibérant)
            [21, false],
        ];
    }

    public function testVirtualFieldsAvailableActionsIsVotedForStateactEnCoursDeValidation()
    {
        // Mêmes résultats pour tous les projets, soit les id 3, 4, 5, 6, 7, 8, 9, 10
        $project = $this->Projects->get(4);

        $expectedAvailableActions = [
            'can_be_send_to_i_parapheur' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
            ],
            'can_be_send_to_manual_signature' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
            ],
            'can_be_send_to_workflow' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Brouillon" ou "Refusé"',
            ],
            'can_be_voted' => [
                'value' => true,
                'data' => null,
            ],
            'can_generate_act_number' => [
                'value' => false,
                'data' => 'Le numéro d\'acte ne peu pas être généré',
            ],
            'check_signing' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "En cours de signature l\'i-parapheur"',
            ],
            'check_refused' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Refusé par l\'i-parapheur"',
            ],
        ];
        $this->assertEquals($expectedAvailableActions, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactValide
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactValide(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $expectedAvailableActions = [
            'can_be_send_to_i_parapheur' => [
                'value' => !$isdeliberating,
                'data' => !$isdeliberating
                    ? null
                    : 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
            ],
            'can_be_send_to_manual_signature' => [
                'value' => !$isdeliberating,
                'data' => !$isdeliberating
                    ? null
                    : 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
            ],
            'can_be_send_to_workflow' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Brouillon" ou "Refusé"',
            ],
            'can_be_voted' => [
                'value' => $isdeliberating,
                'data' => $isdeliberating
                    ? null
                    : 'Le projet a un état qui ne permet pas de le voter.',
            ],
            'can_generate_act_number' => [
                'value' => false,
                'data' => 'Le numéro d\'acte ne peu pas être généré',
            ],
            'check_signing' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "En cours de signature l\'i-parapheur"',
            ],
            'check_refused' => [
                'value' => !$isdeliberating,
                'data' => !$isdeliberating
                    ? null
                    : 'Le projet n\'est pas dans l\'état "Refusé par l\'i-parapheur"',
            ],
        ];
        $this->assertEquals($expectedAvailableActions, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactValide()
    {
        return [
            // Mêmes résultats pour les projets, d'id 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 (type d'acte délibérant)
            [11, true],
            // type d'acte non délibérant
            [35, false],
        ];
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactRefuse
     * @see testVirtualFieldsAvailableActionsIsVotedForStateactBrouillon car refusé signifie implicitement retour en
     * "Brouillon"
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactRefuse(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $expectedAvailableActions = [
            'can_be_send_to_i_parapheur' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
            ],
            'can_be_send_to_manual_signature' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
            ],
            'can_be_send_to_workflow' => [
                'value' => true,
                'data' => null,
            ],
            'can_be_voted' => [
                'value' => $isdeliberating,
                'data' => $isdeliberating ? '' : 'Le projet a un état qui ne permet pas de le voter.',
            ],
            'can_generate_act_number' => [
                'value' => false,
                'data' => 'Le numéro d\'acte ne peu pas être généré',
            ],
            'check_signing' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "En cours de signature l\'i-parapheur"',
            ],
            'check_refused' => [
                'value' => false,
                'data' => 'Le projet n\'est pas dans l\'état "Refusé par l\'i-parapheur"',
            ],
        ];

        $this->assertEquals($expectedAvailableActions, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactRefuse()
    {
        return [
            // Mêmes résultats pour les projets, d'id 21 et 22 (type d'acte non délibérant)
            [21, false],
        ];
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactDeclareSigne
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactDeclareSigne(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $this->assertEquals($this->availableActionsNone, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactDeclareSigne()
    {
        return [
            // type d'acte non délibérant
            [27, false],
        ];
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactSigneIParapheur
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactSigneIParapheur(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $this->assertEquals($this->availableActionsNone, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactSigneIParapheur()
    {
        return [
            // type d'acte délibérant
            [28, true],
            // type d'acte non délibérant
            [29, false],
        ];
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactRefuseIParapheur
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactRefuseIParapheur(int $projectId, bool $isdeliberating)
    {
            $project = $this->Projects->get($projectId);

            $expectedAvailableActions = [
                'can_be_send_to_i_parapheur' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_send_to_manual_signature' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_send_to_workflow' => [
                    'value' => false,
                    'data' => 'Le projet n\'est pas dans l\'état "Brouillon" ou "Refusé"',
                ],
                'can_be_voted' => [
                    'value' => false,
                    'data' => 'Le projet a un état qui ne permet pas de le voter.',
                ],
                'can_generate_act_number' => [
                    'value' => false,
                    'data' => 'Le numéro d\'acte ne peu pas être généré',
                ],
                'check_signing' => [
                    'value' => false,
                    'data' => 'Le projet n\'est pas dans l\'état "En cours de signature l\'i-parapheur"',
                ],
                'check_refused' => [
                    'value' => true,
                    'data' => null,
                ],
            ];

            $this->assertEquals($expectedAvailableActions, $project->availableActions);
            $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactRefuseIParapheur()
    {
        return [
            // type d'acte non délibérant
            [35, false],
        ];
    }

    // @info: à l'heure actuelle, on n'a aucun projet dans cet état-là
    //public function testVirtualFieldsAvailableActionsIsVotedForStateactEnCoursSignatureIParapheur() {}

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactATeletransmettre
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactATeletransmettre(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $this->assertEquals($this->availableActionsNone, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactATeletransmettre()
    {
        return [
            [23, true],
        ];
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactEnCoursTeletransmission
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactEnCoursTeletransmission(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $this->assertEquals($this->availableActionsNone, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactEnCoursTeletransmission()
    {
        return [
            [31, false],
        ];
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactAcquitte
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactAcquitte(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $this->assertEquals($this->availableActionsNone, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactAcquitte()
    {
        return [
            [32, false],
        ];
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactAnnule
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactAnnule(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $this->assertEquals($this->availableActionsNone, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactAnnule()
    {
        return [
            [33, true],
            [34, false],
        ];
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactErreurTdt
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactErreurTdt(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $this->assertEquals($this->availableActionsNone, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactErreurTdt()
    {
        return [
            // Mêmes résultats pour les projets, d'id 25 et 26 (type d'acte délibérant)
            [25, false],
        ];
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactEnCoursDepotTdt
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactEnCoursDepotTdt(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $this->assertEquals($this->availableActionsNone, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactEnCoursDepotTdt()
    {
        return [
            [24, false],
        ];
    }

    /**
     * @dataProvider dataVirtualFieldsAvailableActionsIsVotedForStateactAOrdonner
     */
    public function testVirtualFieldsAvailableActionsIsVotedForStateactAOrdonner(int $projectId, bool $isdeliberating)
    {
        $project = $this->Projects->get($projectId);

        $this->assertEquals($this->availableActionsNone, $project->availableActions);
        $this->assertEquals(['value' => false, 'data' => null], $project->isVoted);
    }

    public function dataVirtualFieldsAvailableActionsIsVotedForStateactAOrdonner()
    {
        return [
            [30, false],
        ];
    }

    // @info: à l'heure actuelle, on n'a aucun projet dans cet état-là
    // public function testVirtualFieldsAvailableActionsIsVotedForStateactAnnulationEnCours() {}

    // @info: à l'heure actuelle, on n'a aucun projet dans cet état-là
    //public function testVirtualFieldsAvailableActionsIsVotedForStateactEnCoursEnvoiIParapheur() {}

    // @info: à l'heure actuelle, on n'a aucun projet dans cet état-là
    //public function testVirtualFieldsAvailableActionsIsVotedForStateactVoteEtApprouve() {}

    // @info: à l'heure actuelle, on n'a aucun projet dans cet état-là
    //public function testVirtualFieldsAvailableActionsIsVotedForStateactVoteEtRejete() {}

    // @info: à l'heure actuelle, on n'a aucun projet dans cet état-là
    //public function testVirtualFieldsAvailableActionsIsVotedForStateactPrendreActe() {}
}
