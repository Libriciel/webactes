<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\Mock\FlowableWrapperMock;
use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Utility\Hash;

/**
 * App\Controller\ContainersSittingsController Test Case
 *
 * @uses \App\Controller\ContainersSittingsController
 */
class ContainersSittingsControllerTest extends AbstractTestCase
{
    public $fixtures = [
        'app.ActorGroups',
        'app.Authorizeactions',
//        'app.BeanstalkPhinxlog',
//        'app.BeanstalkWorkers',
//        'app.ConnectorTypes',
        'app.Counters',
        'app.DraftTemplateTypes',
        'app.GenerateTemplateTypes',
        'app.Generationerrors',
        'app.Notifications',
        'app.Organizations',
//        'app.Phinxlog',
        'app.Sequences',
        'app.Stateacts',
        'app.Statesittings',
        'app.Structures',
        'app.SummonTypes',
        'app.Systemlogs',
        'app.TdtMessages',
        'app.Templates',
        'app.Themes',
        'app.Users',
        'app.UsersInstances',
        'app.Values',
        'app.Workergenerations',
        'app.Workflows',
//        'app.WrapperFlowablePhinxlog',
        'app.Actors',
        'app.ActorsActorGroups',
        'app.Acts',
        'app.Attachments',
        'app.AuthorizeactionsStateacts',
//        'app.BeanstalkJobs',
        'app.Classifications',
//        'app.Connecteurs',
        'app.Dpos',
        'app.DraftTemplates',
        'app.Fields',
        'app.GenerateTemplates',
        'app.Matieres',
        'app.Natures',
        'app.NotificationsUsers',
        'app.Officials',
        'app.Pastellfluxtypes',
        'app.Projects',
        'app.Roles',
        'app.RolesUsers',
        'app.Services',
        'app.StructureSettings',
        'app.StructuresServices',
        'app.StructuresUsers',
        'app.Tabs',
        'app.Typesacts',
        'app.Typesittings',
        'app.Typespiecesjointes',
        'app.UsersServices',
        'app.Votes',
        'app.ActorGroupsTypesittings',
        'app.ActorsActs',
        'app.ActorsProjects',
        'app.ActorsVotes',
        'app.Containers',
        'app.ContainersStateacts',
        'app.DraftTemplatesTypesacts',
        'app.Groups',
//        'app.GroupsUsers',
        'app.Histories',
        'app.Maindocuments',
        'app.Metadatas',
        'app.ProjectTexts',
        'app.Sittings',
        'app.SittingsStatesittings',
        'app.Summons',
        'app.TypesactsTypesittings',
        'app.ActorsProjectsSittings',
        'app.Annexes',
        'app.AttachmentsSummons',
        'app.ContainersSittings',
        'app.Convocations',
        'app.ConvocationsAttachments',
        'app.Files',
        'app.Pastellfluxs',
        'app.Pastellfluxactions',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        Configure::write('KEYCLOAK_DEV_MODE', true);
        Configure::write('Flowable.wrapper_mock', '\App\Test\Mock\FlowableWrapperMock');

        FlowableWrapperMock::reset();
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/containers-sittings?sitting_id=1');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 200], // Secrétariat général
            ['r.violet', 200], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataProviderTestIndex
     */
    public function testIndex(string $params, int $status, array $expected): void
    {
        $this->switchUser($this->getUserId('s.blanc'));
        $this->setJsonRequest();

        $this->get('/api/v1/containers-sittings' . $params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    public function dataProviderTestIndex(): array
    {
        return [
            ['?sitting_id=1', 200, ['containersSittings' => [['id' => 1], ['id' => 2], ['id' => 3]], 'pagintation' => null]],
            [
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "sitting_id"',
                        ],
                    ],
                ],
            ],
        ];
    }

    public function testCalculatesRank()
    {
        $this->setJsonRequest();
        $this->get('api/v1/containers-sittings?sitting_id=1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(
            [1 => 1, 2 => 2, 3 => 3],
            Hash::combine($body, 'containersSittings.{n}.id', 'containersSittings.{n}.rank')
        );
        $table = $this->fetchTable('ContainersSittings');
        $table->deleteAll(['id' => 1]);

        $this->setJsonRequest();
        $this->get('api/v1/containers-sittings?sitting_id=1');
        $body = $this->getResponse();
        //dump($body);
        $this->assertResponseCode(200);
        $this->assertEquals(
            [2 => 1, 3 => 2],
            Hash::combine($body, 'containersSittings.{n}.id', 'containersSittings.{n}.rank')
        );
        $this->assertEquals(1, $body['containersSittings'][0]['rank']);
        $this->assertEquals(2, $body['containersSittings'][1]['rank']);
    }
}
