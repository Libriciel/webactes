<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Model\Table\RolesTable;
use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Hash;

/**
 * App\Controller\RolesController Test Case
 */
class RolesControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
    ];

    /**
     * Original locale
     *
     * @var string
     */
    protected $defaultLocale = null;

    /**
     * Original locales path
     *
     * @var array
     */
    protected $locales = null;

    /**
     * Setup.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        $this->locales = Configure::read('App.paths.locales');
        $locales = TESTS . DS . 'test_app' . DS . 'Locale' . DS;
        Configure::write('App.paths.locales', $locales);

        $this->defaultLocale = Configure::read('App.defaultLocale');
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();

        Configure::write('App.paths.locales', $this->locales);
        Configure::write('App.defaultLocale', $this->defaultLocale);
    }

    /**
     * Test the "index" method if admin.
     *
     * @throws Exception
     */
    public function testIndexRoleAdmin()
    {
        $this->switchUser(1);
        $this->setJsonRequest();
        $this->get('/api/v1/roles');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(
            [
                RolesTable::ADMINISTRATOR,
                RolesTable::FUNCTIONNAL_ADMINISTRATOR,
                RolesTable::VALIDATOR_REDACTOR_NAME,
                RolesTable::GENERAL_SECRETARIAT,
            ],
            Hash::extract($body['roles'], '{n}.name')
        );
    }

    /**
     * Test the "index" method if super admin.
     *
     * @throws Exception
     */
    public function testIndexRoleSuperAdmin()
    {
        $this->switchUser(5);
        $this->setJsonRequest();
        $this->get('/api/v1/roles');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(
            [
                RolesTable::ADMINISTRATOR,
                RolesTable::ADMINISTRATOR,
                RolesTable::FUNCTIONNAL_ADMINISTRATOR,
                RolesTable::FUNCTIONNAL_ADMINISTRATOR,
                RolesTable::VALIDATOR_REDACTOR_NAME,
                RolesTable::VALIDATOR_REDACTOR_NAME,
                RolesTable::GENERAL_SECRETARIAT,
                RolesTable::GENERAL_SECRETARIAT,
                RolesTable::SUPER_ADMINISTRATOR,
                RolesTable::SUPER_ADMINISTRATOR,
            ],
            Hash::extract($body['roles'], '{n}.name')
        );
    }

    /**
     * Test an unsuccessful call to the "view" method for a missing record.
     *
     * @throws Exception
     */
    public function testViewErrorRoleNotFound()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "roles"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/roles/666');
        $this->assertResponseCode(404);
    }

    /**
     * Test an unsuccessful call to the "add" method for an empty name.
     *
     * @throws Exception
     */
    public function testAddErrorEmptyName()
    {
        $data = [
            'structure_id' => 1,
            'name' => '',
            'active' => false,
        ];

        $this->setJsonRequest();
        $this->post('/api/v1/roles', $data);
        $this->assertResponseCode(400);

        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'name' => [
                    '_empty' => 'This field cannot be left empty',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test an unsuccessful call to the "add" method for a duplicate name for the same structure_id.
     *
     * @throws Exception
     */
    public function testAddErrorDuplicateNameSameStructure()
    {
        $data = [
            'structure_id' => 1,
            'name' => RolesTable::ADMINISTRATOR,
            'active' => false,
            'permissions' => [
                '_ids' => [1],
            ],
        ];

        $this->setJsonRequest();
        $this->post('/api/v1/roles', $data);
        $this->assertResponseCode(400);

        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'name' => [
                    '_isUnique' => 'This value is already in use',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test an unsuccessful call to the "edit" method for a missing record.
     *
     * @throws Exception
     */
    public function testEditErrorRoleNotFound()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "roles"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->put('/api/v1/roles/666');
        $this->assertResponseCode(404);
    }

    /**
     * Test an unsuccessful call to the "edit" method for a duplicate name for the same structure_id.
     *
     * @throws Exception
     */
    public function testEditErrorDuplicateNameSameStructure()
    {
        $data = [
            'structure_id' => 1,
            'name' => RolesTable::VALIDATOR_REDACTOR_NAME,
            'active' => false,
            'permissions' => [
                '_ids' => [1],
            ],
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/roles/1', $data);
        $this->assertResponseCode(400);

        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'name' => [
                    '_isUnique' => 'This value is already in use',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }
}
