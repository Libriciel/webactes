<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\NotFoundException;
use Cake\Utility\Hash;

/**
 * App\Controller\SittingsController Test Case
 */
class SittingsControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Sitting',
        'app.group.Actor',
        'app.group.Container',
        'app.group.TypeAct',
        'app.group.File',//->'app.Histories'
        'app.group.Pastell',//->'app.Histories'
        'app.Stateacts',
        'app.Statesittings',
        'app.Generationerrors',
        'app.ActorGroups',
        'app.Workflows',
        'app.Templates',
        'app.Attachments',
        'app.Natures',
        'app.Themes',
        'app.Acts',
        'app.Typespiecesjointes',
        'app.Actors',
        'app.Projects',
        'app.GenerateTemplates',
        'app.Typesittings',
        'app.Votes',
        'app.ActorsVotes',
        'app.Sittings',
        'app.Typesacts',
        'app.ActorGroupsTypesittings',
        'app.ActorsProjectsSittings',
        'app.TypesactsTypesittings',
        'app.Containers',
        'app.ContainersStateacts',
        'app.Summons',
        'app.AttachmentsSummons',
        'app.ContainersSittings',
        'app.Annexes',
        'app.Convocations',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
        Configure::write('Flowable.wrapper_mock', '\App\Test\Mock\FlowableWrapperMock');
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/sittings?statesitting=ouverture_seance');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 200], // Secrétariat général
            ['r.violet', 200], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * Récupération de la liste des séances.
     *
     * @dataProvider dataProviderIndex
     * @param string|null $params
     * @param int $status
     * @param array $expected
     * @param array $expectedBody The expected body content in case of a $status different from 2200
     * @throws \Exception
     */
    public function testIndex(?string $params, int $status, array $expected)
    {
        $this->switchUser($this->getUserId('s.blanc'));
        $this->setJsonRequest();

        $this->get('/api/v1/sittings' . (string)$params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    /**
     * Source de données pour la méthode testIndex (/api/v1/sittings ou /api/v1/sittings?format=full).
     *
     * @return array[]
     */
    public function dataProviderIndex()
    {
        return [
            ['?statesitting=ouverture_seance', 200, ['sittings' => [['id' => 1], ['id' => 2], ['id' => 3]], 'pagination' => ['count' => 3]]],
            ['?format=full&statesitting=ouverture_seance', 200, ['sittings' => [['id' => 1], ['id' => 2], ['id' => 3]], 'pagination' => ['count' => 3]]],
            ['?format=list&statesitting=ouverture_seance', 200, ['sittings' => [['id' => 1], ['id' => 2], ['id' => 3]], 'pagination' => null]],
            ['?statesitting=seance_close', 200, ['sittings' => [['id' => 4]], 'pagination' => ['count' => 1]]],
            ['?format=full&statesitting=seance_close', 200, ['sittings' => [['id' => 4]], 'pagination' => ['count' => 1]]],
            ['?format=list&statesitting=seance_close', 200, ['sittings' => [['id' => 4]], 'pagination' => null]],
            ['?direction=desc&format=full&limit=2&page=2&sort=id&statesitting=ouverture_seance', 200, ['sittings' => [['id' => 1]], 'pagination' => ['count' => 3]]],
            [
                '?format=list&limit=2&statesitting=ouverture_seance',
                400,
                [
                    'errors' => [
                        'limit' => [
                            '_unexpected' => 'Unexpected parameter "limit", accepted parameters: "format", "statesitting", "typesitting_name"',
                        ],
                    ],
                ],
            ],
            [
                '?isNot=true&statesitting=ouverture_seance',
                400,
                [
                    'errors' => [
                        'isNot' => [
                            '_unexpected' => 'Unexpected parameter "isNot", accepted parameters: "direction", "format", "limit", "offset", "page", "sort", "statesitting", "typesitting_name"',
                        ],
                    ],
                ],
            ],
            [
                null,
                400,
                [
                    'errors' => [
                        'statesitting' => [
                            '_empty' => 'This field cannot be left empty',
                        ],
                    ],
                ],
            ],
            [
                '?statesitting=choucroute',
                400,
                [
                    'errors' => [
                        'statesitting' => [
                            'inList' => 'The provided value is invalid, use one of "ouverture_seance", "seance_close"',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @throws Exception
     */
    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/sittings/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $this->assertEquals(1, count($body));
        $this->assertNotEmpty($body['sitting']['typesitting_id']);
    }

    /**
     * @throws Exception
     */
    public function testViewFormatShort()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/sittings/1?format=short');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $expected = [
            'sitting' => [
                'id' => 1,
                'structure_id' => 1,
                'typesitting_id' => 1,
                'date' => '2019-02-21T14:34:00+00:00',
                'date_convocation' => '2019-02-21T14:34:00+00:00',
                'created' => '2019-02-21T14:34:00+00:00',
                'modified' => '2019-02-21T14:34:00+00:00',
                'president_id' => 1,
                'secretary_id' => 1,
                'typesitting' => [
                    'id' => 1,
                    'structure_id' => 1,
                    'name' => 'Bureau',
                    'created' => '2019-02-11T10:28:30+00:00',
                    'modified' => '2019-02-11T10:28:30+00:00',
                    'active' => true,
                    'isdeliberating' => false,
                    'generate_template_convocation_id' => 4,
                    'generate_template_executive_summary_id' => 5,
                    'generate_template_deliberations_list_id' => 6,
                    'generate_template_verbal_trial_id' => 7,
                    'availableActions' => [
                        'can_be_edited' => [
                            'value' => true,
                            'data' => null,
                        ],
                        'can_be_deleted' => [
                            'value' => false,
                            'data' => 'Le type de séance est lié à au moins une séance',
                        ],
                        'can_be_deactivate' => [
                            'value' => false,
                            'data' => 'Le type d\'acte ne peut pas être désactivé.',
                        ],
                    ],
                ],
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => false,
                        'data' => 'La séance contient un projet voté.',
                    ],
                    'can_propose_to_vote' => [
                        'value' => false,
                        'data' => 'La séance n\'est pas délibérante',
                    ],
                    'can_be_closed' => [
                        'value' => true,
                        'data' => '',
                    ],
                ],
                'generationerrors' => [],
                'place' => null,
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    /**
     * @throws Exception
     */
    public function testViewFormatError()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/sittings/1?format=choucroute');
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'format' => [
                    'inList' => 'The provided value is invalid, use one of "full", "short"',
                ],
            ],

        ];
        $this->assertEquals($expected, $body);
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testViewFailedBadId()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "sittings"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/777');
        $this->assertResponseCode(404);
    }

    public function testViewOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "sittings"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/8');
        $this->assertResponseCode(404);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->setJsonRequest();
        $data = [
            'date' => date('Y-m-d H:i:s'),
            'typesitting_id' => 1,
        ];
        $this->post('/api/v1/sittings', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['sitting']['id']);
        $seanceIdAdd = $body['sitting']['id'];

        // Check état de la séance avant cloture
        $this->setJsonRequest();
        $this->get('/api/v1/sittings/' . $body['sitting']['id']);
        $this->assertResponseCode(200);

        $body = $this->getResponse();

        $this->assertEquals($seanceIdAdd, $body['sitting']['id']);
        $this->assertEquals(1, $body['sitting']['structure_id']);
        $this->assertEquals(1, $body['sitting']['typesitting_id']);

        $this->assertEquals(1, count($body['sitting']['statesittings']));
        $this->assertEquals(1, $body['sitting']['statesittings'][0]['id']);
        //        $this->assertEquals('Ouverture de la séance', $body['sitting']['statesittings'][0]['state']);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=ouverture_seance');
        $this->assertResponseCode(200);
        $this->assertEquals([10, 1, 2, 3], Hash::extract($this->getResponse(), 'sittings.{n}.id'));
    }

    public function testAddWithDateISO8601()
    {
        $this->setJsonRequest();
        $data = [
            'date' => '2019-04-08T14:13:34+00:00',
            'typesitting_id' => 1,
        ];
        $this->post('/api/v1/sittings', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['sitting']['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=ouverture_seance');
        $this->assertResponseCode(200);
        $this->assertEquals([10, 1, 2, 3], Hash::extract($this->getResponse(), 'sittings.{n}.id'));
    }

    public function testAddWithDateTwoISO8601()
    {
        $this->setJsonRequest();
        $data = [
            'date' => '2019-04-08T14:13:34+02:00',
            'typesitting_id' => 1,
        ];
        $this->post('/api/v1/sittings', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['sitting']['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=ouverture_seance');
        $this->assertResponseCode(200);
        $this->assertEquals([10, 1, 2, 3], Hash::extract($this->getResponse(), 'sittings.{n}.id'));
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddTwoSittingsDate()
    {
        $date = date('Y-m-d H:i:s');

        // One
        $this->setJsonRequest();
        $data = [
            'date' => $date,
            'typesitting_id' => 1,
        ];
        $this->post('/api/v1/sittings', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['sitting']['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=ouverture_seance');
        $this->assertResponseCode(200);
        $this->assertEquals([10, 1, 2, 3], Hash::extract($this->getResponse(), 'sittings.{n}.id'));

        // Two
        $this->setJsonRequest();
        $data = [
            'date' => $date,
            'typesitting_id' => 1,
        ];
        $this->post('/api/v1/sittings', $data);
        $this->assertResponseCode(400);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=ouverture_seance');
        $this->assertResponseCode(200);
        $this->assertEquals([10, 1, 2, 3], Hash::extract($this->getResponse(), 'sittings.{n}.id'));
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddFailedMissingData()
    {
        $this->setJsonRequest();
        $data = [
            'date' => date('Y-m-d H:i:s'),
        ];
        $this->post('/api/v1/sittings', $data);
        $this->assertResponseCode(400);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=ouverture_seance');
        $this->assertResponseCode(200);
        $this->assertEquals([1, 2, 3], Hash::extract($this->getResponse(), 'sittings.{n}.id'));
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddFailedBadFormatData()
    {
        $this->setJsonRequest();
        $data = [
            'date' => date('Y-m-d H:i:s'),
            'typesitting_id' => 'BONJOUR',
        ];
        $this->post('/api/v1/sittings', $data);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'typesitting_id' => [
                    'integer' => 'The provided value is invalid',
                ],
            ],
        ];
        $this->assertEquals($expected, $body);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=ouverture_seance');
        $this->assertResponseCode(200);
        $this->assertEquals([1, 2, 3], Hash::extract($this->getResponse(), 'sittings.{n}.id'));
    }

    /**
     * Test add method missing data
     *
     * @return void
     */
    public function testAddFailedNoData()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/sittings');
        $this->assertResponseCode(400);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=ouverture_seance');
        $this->assertResponseCode(200);
        $this->assertEquals([1, 2, 3], Hash::extract($this->getResponse(), 'sittings.{n}.id'));
    }

    /**
     * Test de la funtion de modification d'une séance (sessions/edit) avec la méthode PATCH
     *
     * @return void
     */
    public function testPatchEdit()
    {
        $this->setJsonRequest();
        $data = [
            'date' => date('Y-m-d H:i:s'),
            'typesitting_id' => 3,
        ];
        $this->patch('/api/v1/sittings/1', $data);
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals($body['sitting']['id'], 1);
        $this->assertEquals($body['sitting']['typesitting_id'], 3);
    }

    /**
     * Test de la funtion de modification d'une séance (sessions/edit) avec la méthode PUT
     *
     * @return void
     */
    public function testPutEdit()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/sittings/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $this->assertEquals($body['sitting']['id'], 1);
        $this->assertEquals($body['sitting']['typesitting_id'], 1);

        $this->setJsonRequest();
        $data = [
            'date' => date('Y-m-d H:i:s'),
            'typesitting_id' => 3,
        ];
        $this->put('/api/v1/sittings/1', $data);
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['sitting']['id']);
        $this->assertNotEmpty($body['sitting']['structure_id']);
        $this->assertNotEmpty($body['sitting']['typesitting_id']);
        $this->assertNotEmpty($body['sitting']['date']);
        $this->assertNotEmpty($body['sitting']['created']);
        $this->assertNotEmpty($body['sitting']['modified']);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals($body['sitting']['id'], 1);
        $this->assertEquals($body['sitting']['typesitting_id'], 3);
    }

    public function testPatchEditFailedBadId()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "sittings"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $data = [
            'date' => date('Y-m-d H:i:s'),
            'typesitting_id' => 3,
        ];
        $this->patch('/api/v1/sittings/1551', $data);
        $this->assertResponseCode(404);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/1551');
        $this->assertResponseCode(404);
    }

    public function testPutEditFailedBadId()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "sittings"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $data = [
            'date' => date('Y-m-d H:i:s'),
            'typesitting_id' => 3,
        ];
        $this->put('/api/v1/sittings/45', $data);
        $this->assertResponseCode(404);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/45');
        $this->assertResponseCode(404);
    }

    public function testPatchEditFailedBadData()
    {
        $this->setJsonRequest();
        $data = [
            'typesitting_id' => 'sdf',
        ];
        $this->patch('/api/v1/sittings/1', $data);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'typesitting_id' => [
                    'integer' => 'The provided value is invalid',
                ],
            ],
        ];
        $this->assertEquals($expected, $body);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals($body['sitting']['id'], 1);
        $this->assertEquals($body['sitting']['typesitting_id'], 1);
    }

    public function testPutEditFailedBadData()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "sittings"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $data = [
            'typesitting_id' => 1,
        ];
        $this->put('/api/v1/sittings/45', $data);
        $this->assertResponseCode(404);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals($body['sitting']['id'], 1);
        $this->assertEquals($body['sitting']['typesitting_id'], 1);
    }

    /**
     * @return void
     */
    public function testEditUnexistantTypeSitting()
    {
        $this->setJsonRequest();
        $data = [
            'date' => date('Y-m-d H:i:s'),
            'typesitting_id' => 9999999,
        ];
        $this->patch('/api/v1/sittings/1', $data);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'typesitting_id' => [
                    '_existsIn' => 'This value does not exist',
                ],
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $projects = $this->fetchTable('Projects');
        $countProjects = $projects->find('all')->count();

        $this->setJsonRequest();
        $this->delete('/api/v1/sittings/3');
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=ouverture_seance');
        $this->assertResponseCode(200);
        $this->assertEquals([1, 2], Hash::extract($this->getResponse(), 'sittings.{n}.id'));

        $projects = $this->fetchTable('Projects');
        $countProjectsAfterDelete = $projects->find('all')->count();

        $this->assertEquals($countProjects, $countProjectsAfterDelete);
    }

    /**
     * Test delete method fails
     *
     * @return void
     */
    public function testDeleteVotedProjectFails()
    {
        $projects = $this->fetchTable('Projects');
        $countProjects = $projects->find('all')->count();

        $this->setJsonRequest();
        $this->delete('/api/v1/sittings/1');
        $this->assertResponseCode(400);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=ouverture_seance');
        $this->assertResponseCode(200);
        $this->assertEquals([1, 2, 3], Hash::extract($this->getResponse(), 'sittings.{n}.id'));

        $projects = $this->fetchTable('Projects');
        $countProjectsAfterDelete = $projects->find('all')->count();

        $this->assertEquals($countProjects, $countProjectsAfterDelete);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDeleteFailedBadId()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "sittings"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->delete('/api/v1/sittings/999');
        $this->assertResponseCode(404);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/999');
        $this->assertResponseCode(404);
    }

    /**
     * @return void
     */
    public function testDeleteBash()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/sittings?ids=4,5');
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings?format=list&statesitting=seance_close');
        $this->assertResponseCode(200);
        $this->assertEquals([], Hash::extract($this->getResponse(), 'sittings.{n}.id'));
    }

    /**
     * @return void
     */
    public function testDeleteBashBadIds()
    {
        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage('Not Found');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->delete('/api/v1/sittings?ids=10,11');
        $this->assertResponseCode(404);
    }

    /**
     * @return void
     */
    public function testDeleteOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "sittings"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->delete('/api/v1/sittings/8');
        $this->assertResponseCode(404);
    }

    /**
     * @return void
     */
    public function testDeleteBashOtherStructureFail()
    {
        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage('Not Found');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->delete('/api/v1/sittings?ids=8,9');
        $this->assertResponseCode(404);
    }

    /**
     * @return void
     */
    public function testDeleteBashOneOtherStructureFail()
    {
        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage('Not Found');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->delete('/api/v1/sittings?ids=7,8');
        $this->assertResponseCode(404);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/7');
        $this->assertResponseCode(200);

        $this->session(['structure_id' => 2]);
        $this->setJsonRequest();
        $this->get('/api/v1/sittings/8');
        $this->assertResponseCode(200);
        $this->assertEquals(8, $this->getResponse()['sitting']['id']);
    }

    /**
     * @return void
     */
    public function testSittingClose()
    {
        $datas = [
            1,
        ];

        foreach ($datas as $data) {
            // Check état de la séance avant cloture
            $this->setJsonRequest();
            $this->get('/api/v1/sittings/' . $data);
            $this->assertResponseCode(200);

            $body = $this->getResponse();

            $this->assertEquals($data, $body['sitting']['id']);
            $this->assertEquals(1, $body['sitting']['structure_id']);
            $this->assertEquals(1, $body['sitting']['typesitting_id']);

            $this->assertEquals(1, count($body['sitting']['statesittings']));
            $this->assertEquals(ETAT_SITTING_OPEN, $body['sitting']['statesittings'][0]['id']);
        }

        $this->setJsonRequest();
        $this->put('/api/v1/sittings/close', $datas);
        $this->assertResponseCode(200);

        foreach ($datas as $data) {
            // Check état de la séance après cloture
            $this->setJsonRequest();
            $this->get('/api/v1/sittings/' . $data);
            $this->assertResponseCode(200);

            $body = $this->getResponse();

            $this->assertEquals($data, $body['sitting']['id']);
            $this->assertEquals(1, $body['sitting']['structure_id']);
            $this->assertEquals(1, $body['sitting']['typesitting_id']);

            $this->assertEquals(2, count($body['sitting']['statesittings']));
            $this->assertEquals(ETAT_SITTING_OPEN, $body['sitting']['statesittings'][0]['id']);
            $this->assertEquals(ETAT_SITTING_CLOSE, $body['sitting']['statesittings'][1]['id']);
        }
    }

    /**
     * @return void
     */
    public function testSittingCannotBeClosed()
    {
        $datas = [
            3,
        ];

        foreach ($datas as $data) {
            // Check état de la séance avant cloture
            $this->setJsonRequest();
            $this->get('/api/v1/sittings/3');
            $this->assertResponseCode(200);

            $body = $this->getResponse();

            $this->assertEquals($data, $body['sitting']['id']);
            $this->assertEquals(1, $body['sitting']['structure_id']);
            $this->assertEquals(2, $body['sitting']['typesitting_id']);

            $this->assertEquals(1, count($body['sitting']['statesittings']));
            $this->assertEquals(ETAT_SITTING_OPEN, $body['sitting']['statesittings'][0]['id']);

            $this->assertEquals(
                "Tous les projets de la séance n'ont pas été votés.",
                $body['sitting']['availableActions']['can_be_closed']['data']
            );
        }

        $this->setJsonRequest();
        $this->put('/api/v1/sittings/close', $datas);
        $body = $this->getResponse();
        $this->assertResponseCode(400);
        $this->assertEquals(['message' => 'Abandon: 1 séance ne peut pas être clôturée'], $body);

        foreach ($datas as $data) {
            // Check état de la séance sans cloture
            $this->setJsonRequest();
            $this->get('/api/v1/sittings/' . $data);
            $this->assertResponseCode(200);

            $body = $this->getResponse();

            $this->assertEquals($data, $body['sitting']['id']);
            $this->assertEquals(1, $body['sitting']['structure_id']);
            $this->assertEquals(2, $body['sitting']['typesitting_id']);
            $this->assertEquals(ETAT_SITTING_OPEN, $body['sitting']['statesittings'][0]['id']);

            $this->assertEquals(
                "Tous les projets de la séance n'ont pas été votés.",
                $body['sitting']['availableActions']['can_be_closed']['data']
            );
        }
    }

    /**
     * @return void
     */
    public function testSittingsClose()
    {
        $datas = [
            1,
            2,
        ];

        foreach ($datas as $data) {
            // Check état de la séance avant cloture
            $this->setJsonRequest();
            $this->get('/api/v1/sittings/' . $data);
            $this->assertResponseCode(200);

            $body = $this->getResponse();

            $this->assertEquals($data, $body['sitting']['id']);
            $this->assertEquals(1, $body['sitting']['structure_id']);
            $this->assertEquals(1, $body['sitting']['typesitting_id']);

            $this->assertEquals(1, count($body['sitting']['statesittings']));
            $this->assertEquals(1, $body['sitting']['statesittings'][0]['id']);
            //            $this->assertEquals('Ouverture de la séance', $body["sitting"]["statesittings"][0]['state']);
        }

        $this->setJsonRequest();
        $this->put('/api/v1/sittings/close', $datas);
        $this->assertResponseCode(200);

        foreach ($datas as $data) {
            // Check état de la séance après cloture
            $this->setJsonRequest();
            $this->get('/api/v1/sittings/' . $data);
            $this->assertResponseCode(200);

            $body = $this->getResponse();

            $this->assertEquals($data, $body['sitting']['id']);
            $this->assertEquals(1, $body['sitting']['structure_id']);
            $this->assertEquals(1, $body['sitting']['typesitting_id']);

            $this->assertEquals(2, count($body['sitting']['statesittings']));
            //            $this->assertEquals('Ouverture de la séance', $body["sitting"]["statesittings"][0]['state']);
            //            $this->assertEquals('Séance close', $body["sitting"]["statesittings"][1]['state']);
        }
    }

    /**
     * @return void
     */
    public function testSittingCloseFailedBadId()
    {
        $this->setJsonRequest();
        $data = [
            999,
        ];
        $this->put('/api/v1/sittings/close', $data);
        $this->assertResponseCode(404);
    }

    /**
     * @return void
     */
    public function testSittingsCloseFailedAllBadId()
    {
        $this->setJsonRequest();
        $data = [
            999,
            666,
            3,
        ];
        $this->put('/api/v1/sittings/close', $data);
        $this->assertResponseCode(404);
    }

    /**
     * @return void
     */
    public function testSittingsCloseFailedOneBadId()
    {
        $this->setJsonRequest();
        $data = [
            1,
            2,
            999,
        ];
        $this->put('/api/v1/sittings/close', $data);
        $this->assertResponseCode(404);
    }

    public function testSittingCloseOtherStructure()
    {
        $this->setJsonRequest();
        $data = [
            8,
            9,
        ];
        $this->put('/api/v1/sittings/close', $data);
        $this->assertResponseCode(404);
    }

    public function testSittingCloseOneOtherStructure()
    {
        $this->setJsonRequest();
        $data = [
            1,
            8,
        ];
        $this->put('/api/v1/sittings/close', $data);
        $this->assertResponseCode(404);

        $this->setJsonRequest();
        $this->get('/api/v1/sittings/1');
        $this->assertResponseCode(200);
        $this->assertEquals(1, count($this->getResponse()['sitting']['statesittings']));

        $this->session(['structure_id' => 2]);
        $this->setJsonRequest();
        $this->get('/api/v1/sittings/8');
        $this->assertResponseCode(200);
        $this->assertEquals(0, count($this->getResponse()['sitting']['statesittings']));
    }
}
