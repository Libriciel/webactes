<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;

/**
 * App\Controller\TypespiecesjointesController Test Case
 */
class TypespiecesjointesControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Natures',
        'app.Typespiecesjointes',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    public function testGetAssociatedType()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/Typespiecesjointes/getAssociatedType/1');
        $this->assertResponseCode(200);

        $response = $this->getResponse();
        $expected = [
            'typespiecesjointes' => [
                [
                    'nature_id' => 1,
                    'codetype' => '99_AU',
                    'libelle' => 'Autre document',
                ],
            ],
        ];

        $this->assertEquals($expected['typespiecesjointes'][0]['nature_id'], 1);
        $this->assertEquals($expected['typespiecesjointes'][0]['codetype'], '99_AU');
        $this->assertEquals($expected['typespiecesjointes'][0]['libelle'], 'Autre document');
    }

    public function testGetAssociatedTypeOtherStructureEmpty()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/Typespiecesjointes/getAssociatedType/5');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEmpty($body['typespiecesjointes']);
    }
}
