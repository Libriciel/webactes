<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * App\Controller\NaturesController Test Case
 */
class NaturesControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Natures',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/natures');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 200], // Secrétariat général
            ['r.violet', 200], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataProviderTestIndex
     */
    public function testIndex(string $params, int $status, array $expected): void
    {
        $this->switchUser($this->getUserId('s.blanc'));
        $this->setJsonRequest();

        $this->get('/api/v1/natures' . $params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    public function dataProviderTestIndex(): array
    {
        $natures = [
            ['id' => 2],
            ['id' => 6],
            ['id' => 4],
            ['id' => 3],
            ['id' => 1],
        ];

        return [
            ['', 200, ['natures' => $natures, 'pagination' => ['count' => 5]]],
            ['?paginate=false', 200, ['natures' => $natures, 'pagination' => null]],
            [
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "direction", "limit", "offset", "page", "paginate", "sort"',
                        ],
                    ],
                ],
            ],
        ];
    }

    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/natures/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(1, count($body));
        $this->assertNotEmpty($body['nature']['id']);
    }

    public function testViewNotExistsFailed()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "natures"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/natures/100');
        $this->assertResponseCode(404);
    }

    public function testViewOtherStructureFailed()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "natures"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/natures/5');
        $this->assertResponseCode(404);
    }
}
