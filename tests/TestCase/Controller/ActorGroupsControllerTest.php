<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * [ActorsgroupsControllerTest description]
 */
class ActorGroupsControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Actors',
        'app.Groups',
        'app.ActorGroups',
        'app.ActorsActorGroups',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/actor-groups');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 403], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataProviderTestIndex
     */
    public function testIndex(string $params, int $status, array $expected): void
    {
        $this->switchUser($this->getUserId('s.blanc'));
        $this->setJsonRequest();

        $this->get('/api/v1/actor-groups' . $params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    public function dataProviderTestIndex(): array
    {
        $actorGroups = [
            ['id' => 1],
        ];

        return [
            ['', 200, ['actorGroups' => $actorGroups, 'pagination' => ['count' => 1, 'perPage' => 10]]],
            ['?paginate=false', 200, ['actorGroups' => $actorGroups, 'pagination' => null]],
            ['?name=Groupe%201', 200, ['pagination' => ['count' => 1]]],
            ['?active=false', 200, ['pagination' => ['count' => 0]]],
            [
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "active", "direction", "limit", "name", "offset", "page", "paginate", "sort"',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/actor-groups/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(1, count($body));
        $this->assertNotEmpty($body['actorGroup']['id']);
    }

    /**
     * [testViewOtherStructureFail description]
     */
    public function testViewOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "actor_groups"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->get('/api/v1/actor-groups/2');
        $this->assertResponseCode(404);
    }

    /**
     * [testViewNotExistentFailed description]
     */
    public function testViewNotExistentFailed()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "actor_groups"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->get('/api/v1/actor-groups/1000');

        $this->assertResponseCode(404);
    }

    /**
     * [testAdd description]
     */
    public function testAdd()
    {
        $data = [
            'name' => 'Structure 1 - Groupe 2',
        ];
        $this->setJsonRequest();
        $this->post('/api/v1/actor-groups', json_encode($data));
        //$this->assertResponseCode(201);

        $body = $this->getResponse();
        $this->assertNotEmpty($body['id']);
        $this->assertEquals(3, $body['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/actor-groups');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(2, count($body['actorGroups']));
    }

    /**
     * [testAddFailed description]
     */
    public function testAddFailed()
    {
        $data = [];

        $this->setJsonRequest();
        $this->post('/api/v1/actor-groups', json_encode($data));
        $body = $this->getResponse();
        $this->assertResponseCode(400);

        $expected = [
            'errors' => [
                'name' => [
                    '_required' => 'This field is required',
                ],
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    /**
     * [testAddOtherStructureFail description]
     */
    public function testAddOtherStructureFail()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
            'name' => 'Structure 2 - Groupe 1',
        ];
        $this->post('/api/v1/actor-groups', json_encode($data));
        $this->assertResponseCode(400);

        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'structure_id' => [
                    'wrong_structure_id',
                ],
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    /**
     * [testEdit description]
     */
    public function testEdit()
    {
        $data = [
            'structure_id' => 1,
            'active' => 0,
            'name' => 'Structure 1 - Groupe 1',
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/actor-groups/1', json_encode($data));
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['actorGroup']['id']);
        $this->assertFalse($body['actorGroup']['active']);
    }

    /**
     * [testEditOtherStructureFail description]
     */
    public function testEditOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "actor_groups"');
        $this->disableErrorHandlerMiddleware();

        $data = [
            'structure_id' => 1,
            'active' => 0,
            'name' => 'Structure 1 - Groupe 2',
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/actor-groups/2', json_encode($data));
        $this->assertResponseCode(404);
    }

    /**
     * [testEditChangeStructureFail description]
     */
    public function testEditChangeStructureFail()
    {
        $data = [
            'structure_id' => 2,
            'active' => 0,
            'name' => 'Structure 2 - Groupe 1',
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/actor-groups/1', json_encode($data));
        $this->assertResponseCode(400);

        $body = $this->getResponse();

        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $this->assertEquals($expected, $body['errors']);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->switchUser(1);

        $this->setJsonRequest();
        $this->delete('/api/v1/actor-groups/1');
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/actor-groups');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(0, count($body['actorGroups']));
    }

    /**
     * [testDeleteOtherStructureFail description]
     *
     * @return [type] [description]
     */
    public function testDeleteOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "actor_groups"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->delete('/api/v1/actor-groups/4');
        $this->assertResponseCode(404);
    }
}
