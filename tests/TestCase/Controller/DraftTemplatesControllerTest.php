<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * App\Controller\DraftTemplatesController Test Case
 *
 * @uses \App\Controller\DraftTemplatesController
 */
class DraftTemplatesControllerTest extends AbstractTestCase
{
    public $fixtures = [
        'app.group.Auth',
        'app.DraftTemplateTypes',
        'app.DraftTemplates',
    ];

    private string $draftTemplateOdt;
    private string $draftTemplateOdtTMP;

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        Configure::write('KEYCLOAK_DEV_MODE', true);

        $this->draftTemplateOdt = ROOT . '/tests/Fixture/files/templates/template.odt';
        $this->draftTemplateOdtTMP = TMP . 'tests' . DS . 'template.odt';

        $this->assertTrue(copy($this->draftTemplateOdt, $this->draftTemplateOdtTMP));
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/draft-templates');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 403], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataProviderTestIndex
     */
    public function testIndex(string $params, int $status, array $expected): void
    {
        $this->switchUser($this->getUserId('s.blanc'));
        $this->setJsonRequest();

        $this->get('/api/v1/draft-templates' . $params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    public function dataProviderTestIndex(): array
    {
        $draftTemplates = [
            ['id' => 2, 'draft_template_type' => ['id' => 2]],
            ['id' => 1, 'draft_template_type' => ['id' => 1]],
        ];

        return [
            ['', 200, ['draftTemplates' => $draftTemplates, 'pagination' => ['count' => 2]]],
            ['?paginate=false', 200, ['draftTemplates' => $draftTemplates, 'pagination' => null]],
            ['?name=foo', 200, ['pagination' => ['count' => 0]]],
            ['?name=projet', 200, ['pagination' => ['count' => 1]]],
            ['?name=acte', 200, ['pagination' => ['count' => 1]]],
            [
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "direction", "limit", "name", "offset", "page", "paginate", "sort"',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView(): void
    {
        $this->setJsonRequest();
        $this->get('/api/v1/draft-templates/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(1, count($body));
        $this->assertNotEmpty($body['draftTemplate']['id']);
    }

    /**
     * [testViewOtherStructureFail description]
     *
     * @return [type] [description]
     */
    public function testViewOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "draft_templates"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->get('/api/v1/draft-templates/8');
        $this->assertResponseCode(404);
    }

    /**
     * [testViewNotExistentFailed description]
     *
     * @return [type] [description]
     */
    public function testViewNotExistentFailed()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "draft_templates"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->get('/api/v1/draft-templates/1000');
        $this->assertResponseCode(404);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd(): void
    {
        $this->setJsonRequest();
        $this->assertTrue(copy($this->draftTemplateOdt, $this->draftTemplateOdtTMP));

        $data = ['draftTemplate' => [
            'structure_id' => 1,
            'name' => 'Gabarit d\'acte de arrêté',
            'draft_template_type_id' => 1,
            'created' => 1549880898,
            'modified' => 1549880898,
        ],
        ];

        $this->post('/api/v1/draft-templates', json_encode($data));
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['draftTemplate']);
        $this->assertEquals(4, $body['draftTemplate']['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/draft-templates');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(3, count($body['draftTemplates']));
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit(): void
    {
        $data = ['draftTemplate' => [
            'structure_id' => 1,
            'name' => 'Gabarit d\'acte de décision',
            'draft_template_type_id' => 1,
            'created' => 1549880898,
            'modified' => 1549880898,
        ],
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/draft-templates/1', json_encode($data));
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['draftTemplate']['id']);
        $this->assertStringContainsString('Gabarit d\'acte de décision', $body['draftTemplate']['name']);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete(): void
    {
        $this->switchUser(1);

        $this->setJsonRequest();
        $this->delete('/api/v1/draft-templates/2');
        $body = $this->getResponse();

        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/draft-templates');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(1, count($body['draftTemplates']));
    }
}
