<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Shell\Worker\BasicPastellWorker;
use App\Test\TestCase\AbstractTestCase;
use App\TestSuite\InvokePrivateTrait;
use Beanstalk\Utility\Beanstalk;
use Cake\Core\Configure;
use Cake\Event\EventList;
use Laminas\Diactoros\UploadedFile;
use Pheanstalk\Job;
use SplFileInfo;

/**
 * App\Controller\SignaturesController Test Case
 */
class SignaturesControllerTest extends AbstractTestCase
{
    use InvokePrivateTrait;

    /**
     * @var array $fixtures
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Container',
        'app.group.Pastell',
        'app.Stateacts',
        'app.DraftTemplateTypes',
        'app.Notifications',
        'app.Counters',
        'app.Sequences',
        'app.ConnectorTypes',
        'app.Workflows',
        'app.Natures',
        'app.GenerateTemplates',
        'app.Typesacts',
        'app.Templates',
        'app.Themes',
        'app.Typesittings',
        'app.Connecteurs',
        'app.Matieres',
        'app.Typespiecesjointes',
        'app.DraftTemplates',
        'app.DraftTemplatesTypesacts',
        'app.Systemlogs',
        'app.NotificationsUsers',
        'app.Attachments',
        'app.Actors',
        'app.Projects',
        'app.Sittings',
        'app.Summons',
        'app.Acts',
        'app.ProjectTexts',
        'app.ActorsProjectsSittings',
        'app.AttachmentsSummons',
        'app.ActorsProjects',
        'app.Containers',
        'app.Histories',
        'app.Annexes',
        'app.Maindocuments',
        'app.ContainersStateacts',
        'app.ContainersSittings',
        'app.Files',
    ];

    // Routes testées
    //$routes->connect('/projects/signature/soustypes/:id', ['controller' => 'Signatures', 'action' => 'getIParapheurSousTypes', '_method' => 'GET']);
    //$routes->connect('/projects/:id/manual-signature', ['controller' => 'Signatures', 'action' => 'manualSignature', '_method' => 'PUT'], ['pass' => ['id']]);
    //$routes->connect('/projects/:id/electronic-signature', ['controller' => 'Signatures', 'action' => 'electronicSignature', '_method' => 'PUT'], ['pass' => ['id']]);

    /**
     * @var ProjectsTable $Projects
     */
    public $Projects;

    public const URL_GET_SOUSTYPE_PARAPHEUR = '/api/v1/projects/signature/soustypes/';

    /**
     * Saved configuration values, to be saved in ::setUp and restored in ::tearDown.
     *
     * @var array
     */
    protected $savedConfiguration = [];
    protected $cheminFilePdf = '';
    protected $mainDocumentTemplate = '';
    protected $mainDocument = '';

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);

        $this->Projects = $this->fetchTable('Projects');

        $this->Projects->getEventManager()->setEventList(new EventList());
        $this->Projects->Containers->getEventManager()->setEventList(new EventList());

        $this->cheminFilePdf = ROOT . '/tests/Fixture/files/pdf/';
        $this->mainDocumentTemplate = $this->cheminFilePdf . 'mainDocumentTemplate.pdf';
        $this->mainDocument = $this->cheminFilePdf . 'mainDocument.pdf';
        $this->mainDocumentOdt = TMP . 'tests' . DS . 'mainDocument.odt';
        exec('cp ' . $this->mainDocumentTemplate . ' ' . $this->mainDocument);
    }

    public function testGetSoustypesParapheurWhenDocumentNotExists()
    {
        $this->setJsonRequest();

        $this->get(self::URL_GET_SOUSTYPE_PARAPHEUR . '/' . 1);

        $this->assertResponseCode(404);
    }

    private function addProject()
    {
        $files = [
            $this->mainDocument,
            $this->cheminFilePdf . 'annexe_001.pdf',
            $this->cheminFilePdf . 'annexe_002.pdf',
        ];

        foreach ($files as $file) {
            $this->assertTrue(copy($this->mainDocumentTemplate, $file));
        }
        $annexe1 = new SplFileInfo($this->cheminFilePdf . 'annexe_001.pdf');
        $annexe2 = new SplFileInfo($this->cheminFilePdf . 'annexe_002.pdf');

        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Réabilitation de la rue du Pirée',
                'stateact_id' => 1,
                'typesact_id' => 1,
                'sittings' => [6, 7],
                'matiere_id' => 1,
                'maindocument' => [
                    'codetype' => '99_AR',
                ],
                'annexes' => [
                    [
                        'istransmissible' => true,
                        'codetype' => '99_SE',
                    ],
                    [
                        'istransmissible' => false,
                        'codetype' => '99_AR',
                    ],
                ],
            ]
        );

        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $this->mainDocument,
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
            'annexes' => [
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_001.pdf',
                    $annexe1->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe1->getFilename(),
                    'application/pdf'
                )
                ,
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_002.pdf',
                    $annexe2->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe2->getFilename(),
                    'application/pdf'
                ),
            ],
        ];
        $this->post('/api/v1/projects', $data);

        return $this->getResponse();
    }

    public function testPassProjectToValidatedAndCheckIfEventCreateOnPastellWasFired()
    {
        //TODO : réparer
        $this->markTestSkipped('Fail');

        $project = $this->addProject();

        //Change State of the project to VALIDATED
        $project['stateact_id'] = VALIDATED;
        $project['typesact_id'] = 1;

        $project['sittings'] = '';

        $data = [
            'project' => json_encode($project),
            'mainDocument' => [
                'name' => 'mainDocument.pdf',
                'type' => 'application/pdf',
                'tmp_name' => $this->mainDocument,
                'error' => 0,
                'size' => 7551,
            ],
            'annexes' => [],
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/projects/' . $project['id'], $data);

        // Le projet est passé en état validé => on vérifie le lancement de l'èvènement.
        // Si l'évènement est lancé c'est que le projet est sauvegardé
        $this->assertEventFired('Model.Project.afterSaveState', $this->Projects->getEventManager());
        $this->setJsonRequest();
        $this->get('/api/v1/projects/' . $project['id']);
        /**
         * @var Beanstalk|MockObject $Beanstalk
         */
        $Beanstalk = $this->createMock(Beanstalk::class);
        $job = $this->createMock(Job::class);
        $job->method('getId')->willReturn(1);
        $Beanstalk->method('getJob')->willReturn($job);
        $worker = new BasicPastellWorker($Beanstalk);
        $table = $this->fetchTable('Pastellfluxs');

        $query = $table
            ->find('all')
            ->where(['container_id' => $project['id']])
            ->firstOrFail();

        $message = [
            'flux' => Configure::read('Flux.actes-generique.name'),
            'object_id' => (int)$query['id_d'],
            'organization_id' => 1,
            'structure_id' => 1,
            'pastellFlux_id' => 1,
            'flux_name' => Configure::read('Flux.actes-generique.name'),
            'action' => 'prepareDocumentOnPastell',
        ];

        $this->invokeMethod($worker, 'work', [$message]);
    }

    public function testManualSigningWithNotPdfFails()
    {
        $cheminFileOdt = ROOT . '/tests/Fixture/files/odt/';
        $this->assertTrue(copy($cheminFileOdt . 'file.odt', $this->mainDocumentOdt));

        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Réabilitation de la rue du Pirée',
                'stateact_id' => VALIDATED,
                'typesact_id' => 1,
                'sittings' => [6, 7],
                'matiere_id' => 1,
                'maindocument' => [
                    'codetype' => '99_AR',
                ],
            ]
        );
        $fileMainDocument = new SplFileInfo($this->mainDocumentOdt);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/vnd.oasis.opendocument.text'
            ),
        ];

        $this->post('/api/v1/projects', $data);

        $id = $this->getResponse()['id'];
        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'signature_date' => '2019-05-28',
            ]
        );
        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/vnd.oasis.opendocument.text'
            ),
        ];
        $this->put('/api/v1/projects/' . $id . '/manual-signature', $data);
        $this->assertResponseCode(400);
        $expected = [
            'errors' => [
                'id' => [
                    '"application/vnd.oasis.opendocument.text" MIME type is not amongst the accepted types: "application/pdf"',
                ],
            ],
        ];
        $actual = $this->getResponse();
        $this->assertEquals($expected, $actual);
    }
}
