<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;

/**
 * App\Controller\ActorsProjectsSittingsController Test Case
 */
class ActorsProjectsSittingsControllerTest extends AbstractTestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Actor',
        'app.GenerateTemplates',
        'app.Typesittings',
        'app.Sittings',
        'app.ActorsProjectsSittings',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    public function testEditAttendanceList()
    {
        $this->setJsonRequest();
        $data = [
            [
                'id' => 1,
                'actor_id' => 1,
                'project_id' => 19,
                'sitting_id' => 1,
                'is_present' => false,
            ],
            [
                'id' => 2,
                'actor_id' => 2,
                'project_id' => 19,
                'sitting_id' => 1,
                'is_present' => false,
            ],
        ];
        $this->put('/api/v1/sittings/1/projects/19/liste-presence', $data);
        $this->assertResponseCode(200);

        $body = $this->getResponse();

        $actual = Hash::combine($body, 'actorsProjectsSittings.{n}.id', 'actorsProjectsSittings.{n}.is_present');
        $expected = [1 => false, 2 => false, 3 => true];
        $this->assertEquals($expected, $actual);
    }

    public function testMandatorCannotBeMissing()
    {
        $this->setJsonRequest();

        $data = [
            [
                'id' => 13,
                'actor_id' => 1,
                'mandataire_id' => null,
                'project_id' => 4,
                'sitting_id' => 1,
                'is_present' => false,
            ],
            [
                'id' => 14,
                'actor_id' => 2,
                'mandataire_id' => 1,
                'project_id' => 4,
                'sitting_id' => 1,
                'is_present' => false,
            ],
            [
                'id' => 15,
                'actor_id' => 3,
                'mandataire_id' => null,
                'project_id' => 4,
                'sitting_id' => 1,
                'is_present' => false,
            ],
        ];

        $this->put('/api/v1/sittings/1/projects/1/liste-presence', $data);
        $body = $this->getResponse();

        $this->assertEquals("Un acteur déclaré absent ne peut être mandataire d'un autre.", $body['message']);
    }
}
