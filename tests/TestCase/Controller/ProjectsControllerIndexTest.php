<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\TestSuite\LoadOncePerClassFixturesTrait;
use Cake\Utility\Hash;

/**
 * App\Controller\ProjectsController Test Case
 *
 * @see ProjectsIndexComponentGetQueryTest
 * @group ProjectsIndex
 */
class ProjectsControllerIndexTest extends AbstractProjectsControllerTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderIndexByUrl
     * @dataProvider dataProviderIndexByUrlAndGetParameters
     */
    public function testIndexAllProviders($user, string $params, int $expectedCode, ?array $expectedIdsOrBody = null, ?array $expectedPagination = null)
    {
        $userId = is_int($user) ? $user : $this->getUserId($user);
        $this->switchUser($userId);
        $this->setJsonRequest();

        $this->get('/api/v1/projects' . $params);
        $this->assertResponseCode($expectedCode);

        $body = $this->getResponse();
        if ($expectedCode === 200) {
            if ($expectedIdsOrBody !== null) {
                $this->assertEquals($expectedIdsOrBody, Hash::extract($body, 'projects.{n}.id'));
            }
            if ($expectedPagination !== null) {
                $expectedPagination += ['page' => 1, 'perPage' => 10];
                $this->assertEquals($expectedPagination, $body['pagination']);
            }
        } else {
            $this->assertEquals($expectedIdsOrBody, $body);
        }
    }

    /**
     * Récupération des projets par s.blanc, organization 1, structure 1
     *
     * @see ProjectsIndexComponentGetQueryTest for all users
     *
     * return array[]
     */
    public function dataProviderIndexByUrl()
    {
        return [
            // Projets à l'état "Brouillon"
            ['s.blanc', '/index_drafts', 200, [22, 21, 2, 1], ['count' => 4]],
            // Projets à l'état "En cours dans un circuit"
            ['s.blanc', '/index_validating', 200, [10, 9, 8, 7, 6, 5, 4, 3], ['count' => 8, 'perPage' => 10]],
            // Projets à l'état "À traiter"
            ['s.blanc', '/index_to_validate', 200, [], ['count' => 0]],
            // Projets à l'état "Validés"
            ['s.blanc', '/index_validated', 200, [38, 37, 35, 20, 19, 18, 17, 16, 15, 14], ['count' => 13]],
            // Projets à l'état "Projets sans séances"
            ['s.blanc', '/index_unassociated', 200, [33, 22, 21, 20, 18, 17, 16, 15, 14, 13], ['count' => 17]],
            // Projets à l'état "À déposer sur le Tdt"
            ['s.blanc', '/index_to_transmit', 200, [28, 26, 25, 24, 23], ['count' => 5]],
            // Projets à l'état "À envoyer en préfecture"
            ['s.blanc', '/index_ready_to_transmit', 200, [30], ['count' => 1]],
            // Projets à l'état "Liste des actes"
            ['s.blanc', '/index_act', 200, [34, 33, 32, 31, 29, 27], ['count' => 6]],
        ];
    }

    /**
     * Récupération des projets par s.blanc, organization 1, structure 1 avec des paramètres GET
     *
     * return array[]
     */
    public function dataProviderIndexByUrlAndGetParameters()
    {
        return [
            ['s.blanc', '/index_drafts?name=PROJET+REFUSÉ+PAR+5,+CRÉÉ+PAR+5', 200, [22], ['count' => 1]],
            ['s.blanc', '/index_drafts?name=REFUSÉ+PAR+5', 200, [22], ['count' => 1]],
            ['s.blanc', '/index_drafts?name=foo', 200, [], ['count' => 0]],
            ['s.blanc', '/index_drafts?code_act=foo', 200, [], ['count' => 0]],
            ['s.blanc', '/index_validated?typeact_name=AUTRE', 200, [38, 35], ['count' => 2]],
            ['s.blanc', '/index_validated?typeact_name=utr', 200, [38, 35], ['count' => 2]],
            ['s.blanc', '/index_validated?typeact_name=foo', 200, [], ['count' => 0]],
            ['s.blanc', '/index_validated?name=refusé&typeact_name=délibération', 200, [37, 35, 20, 19, 18, 17, 16, 15, 14, 13], ['count' => 12]],
            [
                's.blanc',
                '/index_validated?name=refusé&typeact_name=délibération&foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "code_act", "direction", "limit", "name", "offset", "page", "sort", "typeact_name"',
                        ],
                    ],
                ],
            ],
        ];
    }

    public function testGetIndexToValidate()
    {
        $this->switchUser(4);
        $this->setJsonRequest();
        $this->get('/api/v1/projects/index_to_validate');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();

        $this->assertContains($this->records[8]['id'], $actual['projects'][0]);
        $this->assertContains($this->records[8]['instance']['canPerformAnAction'], $actual['projects'][0]['instance']);
        $this->assertSame(true, $actual['projects'][0]['instance']['canPerformAnAction']);

        $this->assertContains($this->records[7]['id'], $actual['projects'][1]);
        $this->assertContains($this->records[8]['instance']['canPerformAnAction'], $actual['projects'][1]['instance']);
        $this->assertSame(true, $actual['projects'][1]['instance']['canPerformAnAction']);

        $this->assertContains($this->records[4]['id'], $actual['projects'][2]);
        $this->assertContains($this->records[8]['instance']['canPerformAnAction'], $actual['projects'][2]['instance']);
        $this->assertSame(true, $actual['projects'][2]['instance']['canPerformAnAction']);

        $this->assertContains($this->records[3]['id'], $actual['projects'][3]);
        $this->assertContains($this->records[8]['instance']['canPerformAnAction'], $actual['projects'][3]['instance']);
        $this->assertSame(true, $actual['projects'][3]['instance']['canPerformAnAction']);
    }

    public function testGetReceiptDate()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/projects/index_act');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();
        $this->assertNotNull($actual['projects'][2]['receiptDate']);
    }
}
