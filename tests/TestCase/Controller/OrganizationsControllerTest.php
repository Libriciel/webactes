<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Hash;

/**
 * App\Controller\OrganizationsController Test Case
 */
class OrganizationsControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.ConnectorTypes',
        'app.Connecteurs',
        'app.Actors',
    ];

    public function setUp(): void
    {
        parent::setUp();

        $this->switchUser(5);
        Configure::write('KEYCLOAK_DEV_MODE', true);

        $this->Organizations = $this->fetchTable('Organizations');
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/organizations');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 401], // Administrateur
            ['m.vert', 403], // Administrateur Fonctionnel
            ['j.orange', 403], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataProviderTestIndex
     */
    public function testIndex(string $params, int $status, array $expected): void
    {
        $this->switchUser($this->getUserId('r.cyan'));
        $this->setJsonRequest();

        $this->get('/api/v1/organizations' . $params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    public function dataProviderTestIndex(): array
    {
        $organizations = [
            [
                'id' => 1,
                'structures' => [
                    ['id' => 1],
                    ['id' => 2],
                ],
            ],
        ];

        return [
            ['', 200, ['organizations' => $organizations, 'pagination' => ['count' => 1],]],
            ['?paginate=false', 200, ['organizations' => $organizations, 'pagination' => null]],
            [
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "direction", "limit", "offset", "page", "paginate", "sort"',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Test view method
     *
     * @return void
     * @access  public
     * @created 11/02/2019
     * @version V0.0.9
     */
    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/organizations/1');
        $body = $this->getResponse();
        $this->assertResponseCode(200);
        $this->assertEquals('Libriciel SCOP', $body['organization']['name']);
    }

    /**
     * Test view method with an existant record.
     *
     * @return [type] [description]
     */
    public function testViewFailedRecordNotFound()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "organizations"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/organizations/15');
        $this->assertResponseCode(404);
    }

    /**
     * Test add method
     *
     * @return void
     * @access  public
     * @created 11/02/2019
     * @version V0.0.9
     */
    public function testAdd()
    {
        $this->setJsonRequest();
        $data = [
            'active' => true,
            'name' => 'Marseille',
        ];
        $this->post('/api/v1/organizations', $data);
        $this->assertResponseCode(201);

        $this->setJsonRequest();
        $this->get('/api/v1/organizations');
        $body = $this->getResponse();
        $this->assertResponseCode(200);
        $this->assertEquals(2, count($body['organizations']));
        $this->assertEquals('Marseille', $body['organizations'][1]['name']);
    }

    /**
     * Test add method
     *
     * @return void
     * @access  public
     * @created 11/02/2019
     * @version V0.0.9
     */
    public function testAddFailedEmptyRequiredField()
    {
        $this->setJsonRequest();
        $data = [
            'name' => '',
        ];
        $this->post('/api/v1/organizations', $data);
        $this->assertResponseCode(400);

        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'name' => [
                    '_empty' => 'This field cannot be left empty',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test de la funtion de modification d'une organization (organizations/edit) avec la méthode PATCH
     *
     * @return void
     */
    public function testPatchEdit()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'MTP',
        ];
        $this->patch('/api/v1/organizations/1', $data);
        $this->assertResponseCode(204);
        $this->assertNull($this->getResponse());

        $this->setJsonRequest();
        $this->get('/api/v1/organizations/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals('MTP', $body['organization']['name']);
    }

    /**
     * Test de la funtion de modification d'une organization (organizations/edit) avec la méthode PATCH et une erreur
     * due à un champ obligatoire vide.
     *
     * @return void
     */
    public function testPatchEditEmptyRequiredField()
    {
        $this->setJsonRequest();
        $data = [
            'name' => '',
        ];
        $this->patch('/api/v1/organizations/1', $data);
        $this->assertResponseCode(400);

        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'name' => [
                    '_empty' => 'This field cannot be left empty',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test de la funtion de modification d'une organization (organizations/edit) avec la méthode PUT et une erreur due
     * à un champ obligatoire vide.
     *
     * @return void
     */
    public function testPutEditEmptyRequiredField()
    {
        $this->setJsonRequest();
        $data = [
            'name' => '',
        ];
        $this->put('/api/v1/organizations/1', $data);
        $this->assertResponseCode(400);

        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'name' => [
                    '_empty' => 'This field cannot be left empty',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test delete method
     *
     * @return void
     * @access  public
     * @created 11/02/2019
     * @version V0.0.9
     */
    public function testDelete()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/organizations/1');
        $this->assertResponseCode(204);

        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "organizations"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/organizations/1');
        $this->assertResponseCode(200);
    }

    /**
     * Test delete method pour un enregistrement inexistant.
     *
     * @return [type] [description]
     */
    public function testDeleteFailedRecordNotFound()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "organizations"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->delete('/api/v1/organizations/666');
        $this->assertResponseCode(404);
    }

    /**
     * Test active method
     *
     * @return void
     * @access  public
     * @created 11/02/2019
     * @version V0.0.9
     */
    public function testActiveFalse()
    {
        $this->setJsonRequest();
        $this->put('/api/v1/organizations/1/active', ['active' => false]);
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/organizations/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertFalse($body['organization']['active']);
        $structuresActive = Hash::extract($body['organization']['structures'], '{n}[active=false]');
        $this->assertCount(2, $structuresActive);
    }

    /**
     * Test active method
     *
     * @return void
     */
    public function testActiveTrue()
    {
        $this->setJsonRequest();
        $this->put('/api/v1/organizations/1/active', ['active' => true]);
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/organizations/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertTrue($body['organization']['active']);
        $structuresActive = Hash::extract($body['organization']['structures'], '{n}[active=true]');
        $this->assertCount(2, $structuresActive);
    }

    public function testGetOrchestrationOrganizations()
    {
        $this->switchUser(5);
        $this->setJsonRequest();
        $this->get('/api/v1/organizations/getOrchestrationOrganizations');

        $this->assertResponseCode(200);

        $actual = $this->getResponse();

        $expected = [
            'structures' => [
                [
                    'id_e' => '9',
                    'denomination' => 'poles-actes',
                    'siren' => '000000000',
                    'type' => 'collectivite',
                    'centre_de_gestion' => '0',
                    'entite_mere' => '1',
                    'is_active' => true,
                ],
                [
                    'id_e' => '118',
                    'denomination' => 'AGENCE REGIONALE DU NUMERIQUE ET DE L\'INTELLIGENCE ARTIFICIELLE (ARNia) (21)',
                    'siren' => '000000000',
                    'type' => 'collectivite',
                    'centre_de_gestion' => '0',
                    'entite_mere' => '9',
                    'is_active' => true,
                ],
            ],
        ];

        $this->assertEquals($expected['structures'][0], $actual['structures'][0]);
        $this->assertEquals($expected['structures'][1], $actual['structures'][1]);
    }
}
