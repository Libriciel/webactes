<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Model\Table\HistoriesTable;
use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;

/**
 * App\Controller\VotesController Test Case
 */
class VotesControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Container',
        'app.group.Project',
        'app.group.Sitting',
        'app.group.Actor',
        'app.group.File',
        'app.Stateacts',
        'app.Sequences',
        'app.Counters',
        'app.DraftTemplateTypes',
        'app.Generationerrors',
        'app.Workflows',
        'app.Natures',
        'app.Actors',
        'app.Templates',
        'app.Themes',
        'app.GenerateTemplates',
        'app.Typespiecesjointes',
        'app.Projects',
        'app.Acts',
        'app.Typesacts',
        'app.Typesittings',
        'app.Votes',
        'app.ActorsVotes',
        'app.Containers',
        'app.Sittings',
        'app.ContainersSittings',
        'app.ContainersStateacts',
        'app.Histories',
        'app.Summons',
        'app.ActorsProjectsSittings',
    ];

    /**
     * Set up the configuration.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test add method
     * Should return [success => false] Http 400
     *
     * @return void
     */
    public function testVote_Result_REJECTED()
    {
        $this->setJsonRequest();
        $data = [
            'method' => 'result',
            'structure_id' => 1,
            'project_id' => 19,
            'president_id' => 1,
            'result' => false,
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        //On vote bien le projet avec l'id : 19
        $this->assertEquals(19, $body['vote']['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/19');
        $body = $this->getResponse();

        $lastHistory = array_key_last($body['project']['containers'][0]['histories']);

        // Vérification de l'historique.
        $this->assertTextEquals(HistoriesTable::VOTED_REJECTED, $body['project']['containers'][0]['histories'][$lastHistory]['comment']);

        //État du container doit être correcte
        $this->assertTextEquals($body['project']['containers'][0]['stateacts'][0]['code'], 'vote-et-rejete');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testVote_Result_APPROVED()
    {
        $this->setJsonRequest();
        $data = [
            'method' => 'result',
            'structure_id' => 1,
            'project_id' => 19,
            'president_id' => 1,
            'result' => true,
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        //On vote bien le projet avec l'id : 19
        $this->assertEquals(19, $body['vote']['id']);
        $this->assertTextEquals(HistoriesTable::VOTED_APPROVED, $body['vote']['state']['historyState']);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/19');
        $body = $this->getResponse();

        // Vérification du dernier élément de l'historique
        $lastHistory = array_key_last($body['project']['containers'][0]['histories']);
        $this->assertTextEquals(HistoriesTable::VOTED_APPROVED, $body['project']['containers'][0]['histories'][$lastHistory]['comment']);

        //État du container doit être correcte
        $this->assertTextEquals($body['project']['containers'][0]['stateacts'][0]['code'], 'vote-et-approuve');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testVote_TAKENOTE_APPROVED()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'project_id' => 19,
            'president_id' => 1,
            'method' => 'take_act',
            'take_act' => true,
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        //On vote bien le projet avec l'id : 19
        $this->assertEquals(19, $body['vote']['id']);
        $this->assertTextEquals(HistoriesTable::TAKE_NOTE, $body['vote']['state']['historyState']);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/19');
        $body = $this->getResponse();

        // Vérification du dernier élément de l'historique
        $lastHistory = array_key_last($body['project']['containers'][0]['histories']);
        $this->assertTextEquals(HistoriesTable::TAKE_NOTE, $body['project']['containers'][0]['histories'][$lastHistory]['comment']);

        //État du container doit être correcte
        $this->assertTextEquals('prendre-acte', $body['project']['containers'][0]['stateacts'][0]['code']);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testVote_TOTAL_APPROVED()
    {
        $this->setJsonRequest();
        $data = [
            'method' => 'total',
            'structure_id' => 1,
            'project_id' => 19,
            'president_id' => 1,
            'yes_counter' => 30,
            'no_counter' => 0,
            'abstentions' => 0,
            'abstentions_counter' => 0,
            'no_words_counter' => 0,
            'noWords' => 0,
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        //On vote bien le projet avec l'id : 19
        $this->assertEquals(19, $body['vote']['id']);
        $this->assertTextEquals(HistoriesTable::VOTED_APPROVED, $body['vote']['state']['historyState']);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/19');
        $body = $this->getResponse();

        // Vérification du dernier élément de l'historique
        $lastHistory = array_key_last($body['project']['containers'][0]['histories']);
        $this->assertTextEquals(HistoriesTable::VOTED_APPROVED, $body['project']['containers'][0]['histories'][$lastHistory]['comment']);

        //État du container doit être correcte
        $this->assertTextEquals('vote-et-approuve', $body['project']['containers'][0]['stateacts'][0]['code']);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testVote_TOTAL_REJECED()
    {
        $this->setJsonRequest();
        $data = [
            'method' => 'total',
            'structure_id' => 1,
            'project_id' => 19,
            'president_id' => 1,
            'yes_counter' => 0,
            'no_counter' => 30,
            'abstentions' => 0,
            'abstentions_counter' => 0,
            'no_words_counter' => 0,
            'noWords' => 0,
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        //On vote bien le projet avec l'id : 19
        $this->assertEquals(19, $body['vote']['id']);
        $this->assertTextEquals(HistoriesTable::VOTED_REJECTED, $body['vote']['state']['historyState']);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/19');
        $body = $this->getResponse();

        // Vérification du dernier élément de l'historique
        $lastHistory = array_key_last($body['project']['containers'][0]['histories']);
        $this->assertTextEquals(HistoriesTable::VOTED_REJECTED, $body['project']['containers'][0]['histories'][$lastHistory]['comment']);

        //État du container doit être correcte
        $this->assertTextEquals('vote-et-rejete', $body['project']['containers'][0]['stateacts'][0]['code']);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testVote_DETAILS_REJECED()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'user_id' => 1,
            'method' => 'details',
            'project_id' => 19,
            'president_id' => 1,
            'actors_votes' => [
                [
                    'actor_id' => 1,
                    'vote_id' => 1,
                    'result' => 'no',
                ],
                [
                    'actor_id' => 2,
                    'vote_id' => 1,
                    'result' => 'yes',
                ],
                [
                    'actor_id' => 3,
                    'vote_id' => 1,
                    'result' => 'no',
                ],
            ],
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        //On vote bien le projet avec l'id : 19
        $this->assertEquals(19, $body['vote']['id']);
        $this->assertTextEquals(HistoriesTable::VOTED_REJECTED, $body['vote']['state']['historyState']);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/19');
        $body = $this->getResponse();

        // Vérification du dernier élément de l'historique
        $lastHistory = array_key_last($body['project']['containers'][0]['histories']);
        $this->assertTextEquals(HistoriesTable::VOTED_REJECTED, $body['project']['containers'][0]['histories'][$lastHistory]['comment']);

        //État du container doit être correcte
        $this->assertTextEquals('vote-et-rejete', $body['project']['containers'][0]['stateacts'][0]['code']);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testVote_DETAILS_APPROVED()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'user_id' => 1,
            'method' => 'details',
            'project_id' => 19,
            'president_id' => 1,
            'actors_votes' => [
                [
                    'actor_id' => 1,
                    'vote_id' => 1,
                    'result' => 'yes',
                ],
                [
                    'actor_id' => 2,
                    'vote_id' => 1,
                    'result' => 'yes',
                ],
                [
                    'actor_id' => 3,
                    'vote_id' => 1,
                    'result' => 'no',
                ],
            ],
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        //On vote bien le projet avec l'id : 19
        $this->assertEquals(19, $body['vote']['id']);
        $this->assertTextEquals(HistoriesTable::VOTED_APPROVED, $body['vote']['state']['historyState']);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/19');
        $body = $this->getResponse();

        // Vérification du dernier élément de l'historique
        $lastHistory = array_key_last($body['project']['containers'][0]['histories']);
        $this->assertTextEquals(HistoriesTable::VOTED_APPROVED, $body['project']['containers'][0]['histories'][$lastHistory]['comment']);

        //État du container doit être correcte
        $this->assertTextEquals('vote-et-approuve', $body['project']['containers'][0]['stateacts'][0]['code']);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testVote_DETAILS_APPROVED_CONTAIN_A_REJECT_VOTE()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'user_id' => 1,
            'method' => 'details',
            'project_id' => 19,
            'president_id' => 1,
            'actors_votes' => [
                [
                    'actor_id' => 1,
                    'vote_id' => 1,
                    'result' => 'yes',
                ],
                [
                    'actor_id' => 2,
                    'vote_id' => 1,
                    'result' => 'yes',
                ],
                [
                    'actor_id' => 3,
                    'vote_id' => 1,
                    'result' => 'no',
                ],
            ],
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        //On vote bien le projet avec l'id : 19
        $this->assertEquals(19, $body['vote']['id']);
        $this->assertTextEquals(HistoriesTable::VOTED_APPROVED, $body['vote']['state']['historyState']);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/19');
        $body = $this->getResponse();

        // Vérification du dernier élément de l'historique
        $lastHistory = array_key_last($body['project']['containers'][0]['histories']);
        $this->assertTextEquals(HistoriesTable::VOTED_APPROVED, $body['project']['containers'][0]['histories'][$lastHistory]['comment']);

        //État du container doit être correcte
        $this->assertTextEquals('vote-et-approuve', $body['project']['containers'][0]['stateacts'][0]['code']);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testVote_DETAILS_REJECTED_CONTAIN_A_APPROVE_VOTE()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'user_id' => 1,
            'method' => 'details',
            'project_id' => 19,
            'president_id' => 1,
            'actors_votes' => [
                [
                    'actor_id' => 1,
                    'vote_id' => 1,
                    'result' => 'yes',
                ],
                [
                    'actor_id' => 2,
                    'vote_id' => 1,
                    'result' => 'no',
                ],
                [
                    'actor_id' => 3,
                    'vote_id' => 1,
                    'result' => 'no',
                ],
            ],
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        //On vote bien le projet avec l'id : 19
        $this->assertEquals(19, $body['vote']['id']);
        $this->assertTextEquals(HistoriesTable::VOTED_REJECTED, $body['vote']['state']['historyState']);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/19');
        $body = $this->getResponse();

        // Vérification du dernier élément de l'historique
        $lastHistory = array_key_last($body['project']['containers'][0]['histories']);
        $this->assertTextEquals(HistoriesTable::VOTED_REJECTED, $body['project']['containers'][0]['histories'][$lastHistory]['comment']);

        //État du container doit être correcte
        $this->assertTextEquals('vote-et-rejete', $body['project']['containers'][0]['stateacts'][0]['code']);
    }

    public function testCheckDataValidity()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'user_id' => 1,
            'method' => 'details',
            'project_id' => 19,
            'president_id' => 1,
            'actors_votes' => [
                [
                    'actor_id' => 1,
                    'vote_id' => 1,
                    'result' => 'yes',
                ],
                [
                    'actor_id' => 2,
                    'vote_id' => 1,
                    'result' => 'yes',
                ],
                [
                    'actor_id' => 3,
                    'vote_id' => 1,
                    'result' => 'no',
                ],
            ],
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(201);

        $ActorsProjectsSittings = $this->fetchTable('ActorsProjectsSittings');
        $actor1 = $ActorsProjectsSittings->find()->where(['actor_id' => 1, 'project_id' => 19])->first();
        $actor1->set(['is_present' => false, 'mandataire_id' => null]);
        $ActorsProjectsSittings->save($actor1);

        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'user_id' => 1,
            'method' => 'details',
            'project_id' => 19,
            'president_id' => 2,
            'actors_votes' => [
                [
                    'actor_id' => 1,
                    'vote_id' => 2,
                    'result' => 'yes',
                ],
                [
                    'actor_id' => 2,
                    'vote_id' => 2,
                    'result' => 'no',
                ],
                [
                    'actor_id' => 3,
                    'vote_id' => 2,
                    'result' => 'yes',
                ],
            ],
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(400);

        $body = $this->getResponse();
        $this->assertEquals("Impossible de modifier le vote de l'acteur Théo ROUGE", $body['message']);
    }

    public function testPresidentCannotBeMissingFromSitting()
    {
        $this->setJsonRequest();
        $data = [
            'method' => 'details',
            'project_id' => 7,
            'president_id' => 1,
            'actors_votes' => [
                [
                    'actor_id' => 1,
                    'vote_id' => 1,
                    'result' => 'yes',
                ],
            ],
        ];

        $this->post('/api/v1/votes', $data);
        $this->assertResponseCode(400);

        $body = $this->getResponse();

        $this->assertEquals('Le président de vote ne peut pas être un acteur déclaré absent.', $body['message']);
    }
}
