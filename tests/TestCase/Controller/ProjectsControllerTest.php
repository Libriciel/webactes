<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Model\Table\HistoriesTable;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\Utility\Hash;
use Laminas\Diactoros\UploadedFile;
use SplFileInfo;

/**
 * App\Controller\ProjectsController Test Case
 */
class ProjectsControllerTest extends AbstractProjectsControllerTestCase
{
    /**
     * Utility method to get a project and its associated records for the integration tests.
     *
     * @param int $id The id of the project
     * @return array
     */
    protected function getProject($id)
    {
        $table = $this->fetchTable('Projects');
        $query = $table
            ->find('all')
            ->contain(
                [
                    'Themes',
                    'Containers',
                    'Containers.Annexes' => [
                        'sort' => ['Annexes.id' => 'asc'],
                    ],
                    'Containers.Annexes.Files',
                    'Containers.Maindocuments',
                    'Containers.Maindocuments.Files',
                    'Containers.Stateacts',
                    'Containers.Sittings' => ['sort' => ['Sittings.id' => 'ASC']],
                    'Containers.Typesacts',
                ]
            )
            ->where(['Projects.id' => $id])
            ->enableHydration(false);

        $project = json_decode(json_encode($query->toArray()[0]), true);

        unset(
            $project['_matchingData'],
            $project['codematiere'],
            $project['containers'][0]['stateacts'][0]['_joinData']
        );
        foreach ($project['containers'][0]['sittings'] as $key => $value) {
            unset($project['containers'][0]['sittings'][$key]['_joinData']);
        }

        return $project;
    }

    public function testView()
    {
        $expected = array_merge(
            $this->records[1],
            [
                'isSignedIParapheur' => false,
                'isVoted' => [
                    'value' => false,
                    'data' => null,
                ],
                'availableActions' =>
                    array_merge(
                        $this->records[1]['availableActions'],
                        [
                            'can_be_send_to_workflow' => [
                                'value' => true,
                                'data' => null,
                            ],
                            'can_approve_project' => [
                                'value' => false,
                                'data' => 'Vous ne pouvez pas valider le projet',
                            ],
                            'can_be_edited' => [
                                'value' => true,
                                'data' => null,
                            ],
                            'can_be_deleted' => [
                                'value' => false,
                                'data' => 'le projet ne peut pas être supprimé.',
                            ],
                            'can_generate_act_number' => [
                                'value' => false,
                                'data' => 'Le numéro d\'acte ne peu pas être généré',
                            ],
                        ]
                    ),
                'containers' => [
                    array_merge(
                        $this->records[1]['containers'][0],
                        [
                            'histories' => [
                                [
                                    'id' => 1,
                                    'structure_id' => 1,
                                    'container_id' => 1,
                                    'user_id' => 4,
                                    'comment' => HistoriesTable::CREATE_PROJECT,
                                    'data' => null,
                                    'created' => '2019-02-11T10:28:19+00:00',
                                    'modified' => '2019-02-11T10:28:19+00:00',
                                    'user' => [
                                        'id' => 4,
                                        'civility' => 'M.',
                                        'firstname' => 'Rémi',
                                        'lastname' => 'VIOLET',
                                        'name' => 'Rémi VIOLET',
                                        'full_name' => 'M. Rémi VIOLET',
                                        'availableActions' => [
                                            'can_be_deleted' => [
                                                'value' => false,
                                                'data' => "L'utilisateur ne peut pas être supprimé.",
                                            ],
                                            'can_be_edited' => [
                                                'value' => true,
                                                'data' => null,
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    'id' => 37,
                                    'structure_id' => 1,
                                    'container_id' => 1,
                                    'user_id' => 4,
                                    'comment' => HistoriesTable::EDIT_PROJECT,
                                    'data' => null,
                                    'created' => '2019-02-11T10:28:55+00:00',
                                    'modified' => '2019-02-11T10:28:55+00:00',
                                    'user' => [
                                        'id' => 4,
                                        'civility' => 'M.',
                                        'firstname' => 'Rémi',
                                        'lastname' => 'VIOLET',
                                        'name' => 'Rémi VIOLET',
                                        'full_name' => 'M. Rémi VIOLET',
                                        'availableActions' => [
                                            'can_be_deleted' => [
                                                'value' => false,
                                                'data' => "L'utilisateur ne peut pas être supprimé.",
                                            ],
                                            'can_be_edited' => [
                                                'value' => true,
                                                'data' => null,
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    'id' => 38,
                                    'structure_id' => 1,
                                    'container_id' => 1,
                                    'user_id' => 4,
                                    'comment' => HistoriesTable::EDIT_PROJECT,
                                    'data' => null,
                                    'created' => '2019-02-11T10:28:56+00:00',
                                    'modified' => '2019-02-11T10:28:56+00:00',
                                    'user' => [
                                        'id' => 4,
                                        'civility' => 'M.',
                                        'firstname' => 'Rémi',
                                        'lastname' => 'VIOLET',
                                        'name' => 'Rémi VIOLET',
                                        'full_name' => 'M. Rémi VIOLET',
                                        'availableActions' => [
                                            'can_be_deleted' => [
                                                'value' => false,
                                                'data' => "L'utilisateur ne peut pas être supprimé.",
                                            ],
                                            'can_be_edited' => [
                                                'value' => true,
                                                'data' => null,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ]
                    ),
                ],
                'theme' => [
                    'id' => 1,
                    'active' => true,
                    'name' => 'Administration Générale',
                    'is_editable' => true,
                    'is_deletable' => false,
                    'is_deactivable' => false,
                ],
                'isStateEditable' => true,
                'generationerrors' => [],
            ]
        );

        $this->setJsonRequest();
        $this->get('/api/v1/projects/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $expected['containers'][0]['sittings'][0]['typesitting']['availableActions'] = [
            'can_be_edited' => [
                'value' => true,
                'data' => null,
            ],
            'can_be_deleted' => [
                'value' => false,
                'data' => 'Le type de séance est lié à au moins une séance',
            ],
            'can_be_deactivate' => [
                'value' => false,
                'data' => 'Le type d\'acte ne peut pas être désactivé.',
            ],
        ];
        $expected['containers'][0]['typesact']['availableActions'] = [
            'can_be_edited' => [
                'value' => true,
                'data' => null,
            ],
            'can_be_deleted' => [
                'value' => false,
                'data' => 'Le type d\'acte ne peut pas être supprimé.',
            ],
            'can_be_deactivate' => [
                'value' => true,
                'data' => null,
            ],
        ];
        unset($body['project']['project_texts']);
        unset($body['project']['containers'][0]['sittings'][0]['_joinData']);
        unset($body['project']['containers'][0]['sittings'][1]['_joinData']);

        $this->assertEquals($expected, $body['project']);
    }

    public function testSignedIParapheur()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/projects/27');
        $body = $this->getResponse();
        $this->assertEquals(0, $body['project']['isSignedIParapheur']);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/28');
        $body = $this->getResponse();
        $this->assertEquals(1, $body['project']['isSignedIParapheur']);
    }

    public function testAddSuccess()
    {
        $files = [
            $this->mainDocument,
            $this->cheminFilePdf . 'annexe_001.pdf',
            $this->cheminFilePdf . 'annexe_002.pdf',
        ];

        foreach ($files as $file) {
            $this->assertTrue(copy($this->mainDocumentTemplate, $file));
        }
        $annexe1 = new SplFileInfo($this->cheminFilePdf . 'annexe_001.pdf');
        $annexe2 = new SplFileInfo($this->cheminFilePdf . 'annexe_002.pdf');

        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Réabilitation de la rue du Pirée',
                'stateact_id' => 1,
                'typesact_id' => 1,
                'sittings' => [6, 7],
                'matiere_id' => 1,
                'maindocument' => [
                    'codetype' => '99_AR',
                ],
                'annexes' => [
                    [
                        'istransmissible' => true,
                        'codetype' => '99_SE',
                    ],
                    [
                        'istransmissible' => false,
                        'codetype' => '99_AR',
                    ],
                ],
            ]
        );

        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $this->mainDocument,
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
            'annexes' => [
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_001.pdf',
                    $annexe1->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe1->getFilename(),
                    'application/pdf'
                )
                ,
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_002.pdf',
                    $annexe2->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe2->getFilename(),
                    'application/pdf'
                ),
            ],
        ];
        $this->post('/api/v1/projects', $data);
        $body = $this->getResponse();
        $this->assertResponseCode(201);

        $body = $this->getResponse();
        $this->assertNotEmpty($body['id']);
        $newId = $body['id'];
        // Check the records in the database and the files on disk
        $body = $this->getProject($body['id']);

        $files = [
            'containers.0.maindocument.files.0.path' => null,
            'containers.0.annexes.0.file.path' => null,
            'containers.0.annexes.1.file.path' => null,
        ];

        foreach (array_keys($files) as $path) {
            $value = Hash::get($body, $path);
            if (empty($value) === false) {
                $body = Hash::insert(
                    $body,
                    $path,
                    preg_replace('/[a-z0-9\-]+$/', $path, $value)
                );
            }
            $files[$path] = $value;
        }

        $expected = [
            'id' => $newId,
            'structure_id' => 1,
            'theme_id' => 1,
            'template_id' => null,
            'workflow_id' => null,
            'name' => 'Réabilitation de la rue du Pirée',
            'code_act' => null,
            'ismultichannel' => false,
            'created' => $body['created'],
            'modified' => $body['modified'],
            'containers' => [
                [
                    'id' => $newId,
                    'tdt_id' => null,
                    'structure_id' => 1,
                    'act_id' => null,
                    'project_id' => $newId,
                    'typesact_id' => 1,
                    'created' => $body['containers'][0]['created'],
                    'modified' => $body['containers'][0]['modified'],
                    'typesact' => [
                        'id' => 1,
                        'structure_id' => 1,
                        'nature_id' => 4,
                        'name' => 'Autre',
                        'created' => '2019-02-20T10:34:58+00:00',
                        'modified' => '2019-02-20T10:34:58+00:00',
                        'istdt' => false,
                        'active' => true,
                        'isdefault' => false,
                        'counter_id' => 1,
                        'generate_template_project_id' => 1,
                        'generate_template_act_id' => 2,
                        'isdeliberating' => false,
                    ],
                    'sittings' => [
                        [
                            'id' => 6,
                            'structure_id' => 1,
                            'typesitting_id' => 1,
                            'date' => '9999-12-10T15:00:00+00:00',
                            'date_convocation' => '2019-12-10T15:00:00+00:00',
                            'created' => '2019-02-14T15:54:00+00:00',
                            'modified' => '2019-03-05T04:20:40+00:00',
                            'president_id' => null,
                            'secretary_id' => null,
                            'place' => null,

                        ],
                        [
                            'id' => 7,
                            'structure_id' => 1,
                            'typesitting_id' => 1,
                            'date' => '9999-02-15T10:00:00+00:00',
                            'date_convocation' => '2020-02-15T10:00:00+00:00',
                            'created' => '2019-02-14T15:54:00+00:00',
                            'modified' => '2019-03-05T04:20:40+00:00',
                            'president_id' => null,
                            'secretary_id' => null,
                            'place' => null ,
                        ],
                    ],
                    'stateacts' => [
                        [
                            'id' => 1,
                            'name' => 'Brouillon',
                            'code' => 'brouillon',
                            'created' => '2019-02-18T13:57:59+00:00',
                            'modified' => '2019-02-18T13:57:59+00:00',
                        ],
                    ],
                    'maindocument' => [
                        'id' => $body['containers'][0]['maindocument']['id'],
                        'structure_id' => 1,
                        'container_id' => $newId,
                        'name' => 'Réabilitation de la rue du Pirée',
                        'current' => true,
                        'codetype' => '99_AR',
                        'files' => [
                            [
                                'id' => $body['containers'][0]['maindocument']['files'][0]['id'],
                                'structure_id' => 1,
                                'maindocument_id' => $body['containers'][0]['maindocument']['id'],
                                'annex_id' => null,
                                'generate_template_id' => null,
                                'draft_template_id' => null,
                                'project_text_id' => null,
                                'attachment_summon_id' => null,
                                'name' => 'mainDocument.pdf',
                                'path' => WORKSPACE . '/1/1/' . date('Y') . '/' . $newId . '/containers.0.maindocument.files.0.path',
                                'mimetype' => 'application/pdf',
                                'size' => 30951,
                                'created' => $body['containers'][0]['maindocument']['files'][0]['created'],
                                'modified' => $body['containers'][0]['maindocument']['files'][0]['modified'],
                            ],
                        ],
                        'created' => $body['containers'][0]['maindocument']['created'],
                        'modified' => $body['containers'][0]['maindocument']['modified'],
                    ],
                    'annexes' => [
                        [
                            'id' => $body['containers'][0]['annexes'][0]['id'],
                            'structure_id' => 1,
                            'container_id' => $newId,
                            'rank' => 1,
                            'istransmissible' => true,
                            'current' => true,
                            'codetype' => '99_SE',
                            'is_generate' => false,
                            'file' => [
                                'id' => $body['containers'][0]['annexes'][0]['file']['id'],
                                'structure_id' => 1,
                                'maindocument_id' => null,
                                'annex_id' => $body['containers'][0]['annexes'][0]['id'],
                                'generate_template_id' => null,
                                'draft_template_id' => null,
                                'project_text_id' => null,
                                'attachment_summon_id' => null,
                                'name' => 'annexe_001.pdf',
                                'path' => WORKSPACE . '/1/1/' . date('Y') . '/' . $newId . '/containers.0.annexes.0.file.path',
                                'mimetype' => 'application/pdf',
                                'size' => 30951,
                                'created' => $body['containers'][0]['annexes'][0]['file']['created'],
                                'modified' => $body['containers'][0]['annexes'][0]['file']['modified'],
                            ],
                        ],
                        [
                            'id' => $body['containers'][0]['annexes'][1]['id'],
                            'structure_id' => 1,
                            'container_id' => $newId,
                            'rank' => 2,
                            'istransmissible' => false,
                            'current' => true,
                            'codetype' => '99_AR',
                            'is_generate' => false,
                            'file' => [
                                'id' => $body['containers'][0]['annexes'][1]['file']['id'],
                                'structure_id' => 1,
                                'maindocument_id' => null,
                                'annex_id' => $body['containers'][0]['annexes'][1]['id'],
                                'generate_template_id' => null,
                                'draft_template_id' => null,
                                'project_text_id' => null,
                                'attachment_summon_id' => null,
                                'name' => 'annexe_002.pdf',
                                'path' => WORKSPACE . '/1/1/' . date('Y') . '/' . $newId . '/containers.0.annexes.1.file.path',
                                'mimetype' => 'application/pdf',
                                'size' => 30951,
                                'created' => $body['containers'][0]['annexes'][1]['file']['created'],
                                'modified' => $body['containers'][0]['annexes'][1]['file']['modified'],
                            ],
                        ],
                    ],
                    'generation_job_id' => null,
                ],
            ],
            'signature_date' => null,
            'id_flowable_instance' => null,
            'theme' => [
                'id' => 1,
                'structure_id' => 1,
                'active' => true,
                'name' => 'Administration Générale',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 6,
                'created' => '2019-02-11T10:28:29+00:00',
                'modified' => '2019-02-11T10:28:29+00:00',
                'position' => 'Lorem',
                'deleted' => null,
            ],
        ];
        unset($body['project_texts']);

        $this->assertEquals($expected, $body);

        // Check files on disk
        foreach ($files as $file) {
            $this->assertFileExists($file);
        }
    }

    /**
     * Test a failed call to the "add" method because of a missing field.
     *
     * @throws Exception
     */
    public function testAddFailedNotTypesactId()
    {
        $this->expectException(BadRequestException::class);
        $this->expectExceptionMessage("Le type de l'acte est non spécifié.");
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->assertTrue(copy($this->mainDocumentTemplate, $this->mainDocument));

        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Réabilitation de la rue du Pirée',
                'stateact_id' => 1,
                'sittings' => [6, 7],
            ]
        );
        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
        ];
        $this->post('/api/v1/projects', $data);
        $this->assertResponseCode(400);
    }

    public function testAddNotPdfSuccess()
    {
        $this->assertTrue(copy($this->cheminFileOdt . 'file.odt', $this->mainDocumentOdt));
        $this->assertTrue(copy($this->mainDocumentTemplate, $this->cheminFilePdf . 'annexe_001.pdf'));
        $this->assertTrue(copy($this->mainDocumentTemplate, $this->cheminFilePdf . 'annexe_002.pdf'));

        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Réabilitation de la rue du Pirée',
                'stateact_id' => 1,
                'typesact_id' => 1,
                'sittings' => [6, 7],
                'matiere_id' => 1,
                'maindocument' => [
                    'codetype' => '99_AR',
                ],
                'annexes' => [
                    [
                        'istransmissible' => true,
                        'codetype' => '99_SE',
                    ],
                    [
                        'istransmissible' => false,
                        'codetype' => '99_AR',
                    ],
                ],
            ]
        );

        $fileMainDocument = new SplFileInfo($this->mainDocumentOdt);
        $annexe1 = new SplFileInfo($this->cheminFilePdf . 'annexe_001.pdf');
        $annexe2 = new SplFileInfo($this->cheminFilePdf . 'annexe_002.pdf');

        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/vnd.oasis.opendocument.text'
            ),
            'annexes' => [
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_001.pdf',
                    $annexe1->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe1->getFilename(),
                    'application/pdf'
                )
                ,
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_002.pdf',
                    $annexe2->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe2->getFilename(),
                    'application/pdf'
                ),
            ],
        ];

        $this->post('/api/v1/projects', $data);
        $this->assertResponseCode(201);
    }

    public function testAddFailedMimeType()
    {
        $Projects = $this->fetchTable('Projects');
        $countInit = $Projects->find()->count();

        $this->assertTrue(copy($this->cheminFileLogo . 'false.png', $this->mainDocument));
        $this->assertTrue(copy($this->mainDocumentTemplate, $this->cheminFilePdf . 'annexe_001.pdf'));
        $this->assertTrue(copy($this->mainDocumentTemplate, $this->cheminFilePdf . 'annexe_002.pdf'));

        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Réabilitation de la rue du Pirée',
                'stateact_id' => 1,
                'typesact_id' => 1,
                'sittings' => [6, 7],
                'matiere_id' => 1,
                'maindocument' => [
                    'codetype' => '99_AR',
                ],
                'annexes' => [
                    [
                        'istransmissible' => true,
                        'codetype' => '99_SE',
                    ],
                    [
                        'istransmissible' => false,
                        'codetype' => '99_AR',
                    ],
                ],
            ]
        );
        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $annexe1 = new SplFileInfo($this->cheminFilePdf . 'annexe_001.pdf');
        $annexe2 = new SplFileInfo($this->cheminFilePdf . 'annexe_002.pdf');
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'image/png'
            ),
            'annexes' => [
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_001.pdf',
                    $annexe1->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe1->getFilename(),
                    'application/pdf'
                ),
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_002.pdf',
                    $annexe2->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe2->getFilename(),
                    'application/pdf',
                ),
            ],
        ];

        $this->post('/api/v1/projects', $data);
        $this->assertResponseCode(400);
        $countAfter = $Projects->find()->count();
        $this->assertEquals($countInit, $countAfter);
        $response = $this->getResponse();

        $expected = [
            'errors' => [
                'containers' => [
                    0 => [
                        'maindocument' => [
                            'files' => [
                                0 => [
                                    'mimetype' => [
                                        '_mimetype' => '"image/png" MIME type is not amongst the accepted types: "' . implode('"", "', Configure::read('mainDocumentAcceptedTypes')) . '"',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $response);
    }

    /**
     * Test a failed call to the "add" method because of a missing field.
     *
     * @throws Exception
     */
    public function testAddFailedBadcodetype()
    {
        $this->expectException(BadRequestException::class);
        $this->expectExceptionMessage('invalid codetype : 00_ZZ');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->assertTrue(copy($this->mainDocumentTemplate, $this->mainDocument));

        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Réabilitation de la rue du Pirée',
                'stateact_id' => 1,
                'sittings' => [6, 7],
                'typesact_id' => 1,
                'maindocument' => [
                    'codetype' => '00_ZZ',
                ],
                'annexes' => [],
            ]
        );
        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
        ];
        $this->post('/api/v1/projects', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test a failed call to the "add" method because of wrong MIME type for the main document.
     *
     * @return vo
     * id
     */
    public function testAddFailedWrongMimeMaindocument()
    {
        $this->assertTrue(copy(__FILE__, $this->mainDocument));

        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Réabilitation de la rue du Pirée',
                'stateact_id' => 1,
                'typesact_id' => 1,
                'sittings' => [6, 7],
            ]
        );
        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
        ];
        $this->post('/api/v1/projects', $data);

        $this->assertResponseCode(400);

        // Check the validation errors
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'containers' => [
                    [
                        'maindocument' => [
                            'files' => [
                                [
                                    'mimetype' => [
                                        '_mimetype' => '"text/x-php" MIME type is not amongst the accepted types: "' . implode('"", "', Configure::read('mainDocumentAcceptedTypes')) . '"',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    /**
     * Test a failed call to the "add" method because of wrong MIME type for the annexe.
     *
     * @return void
     */
    public function testAddFailedWrongMimeAnnexe()
    {
        $this->assertTrue(copy($this->mainDocumentTemplate, $this->mainDocument));
        $this->assertTrue(copy(__FILE__, ROOT . '/tests/Fixture/files/pdf/annexe_001.pdf'));

        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Réabilitation de la rue du Pirée',
                'stateact_id' => 1,
                'typesact_id' => 1,
                'sittings' => [6, 7],
                'annexes' => [],
            ]
        );
        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $annexe1 = new SplFileInfo($this->cheminFilePdf . 'annexe_001.pdf');
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/vnd.oasis.opendocument.text'
            ),
            'annexes' => [
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_001.pdf',
                    $annexe1->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe1->getFilename(),
                    'application/pdf'
                ),
            ],
        ];
        $this->post('/api/v1/projects', $data);
        $this->assertResponseCode(400);

        // Check the validation errors
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'containers' => [
                    [
                        'annexes' => [
                            [
                                'file' => [
                                    'mimetype' => [
                                        '_mimetype' => '"text/x-php" MIME type is not amongst the accepted types: "application/pdf"", "image/png"", "image/jpeg"", "application/vnd.oasis.opendocument.text"", "application/xml"", "application/msword"", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"", "application/vnd.ms-excel"", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"", "application/vnd.ms-powerpoint"", "application/vnd.openxmlformats-officedocument.presentationml.presentation"", "application/vnd.oasis.opendocument.spreadsheet"", "application/vnd.oasis.opendocument.presentation"',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testDeleteProjectSuccess()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/projects/2');
        $this->assertResponseCode(204);
    }

    public function testDeleteProjectNotDeleteable()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/projects/1');
        $this->assertResponseCode(400);
    }

    public function testDeleteProjectOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "projects"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->delete('/api/v1/projects/36');
        $this->assertResponseCode(404);
    }

    public function testDeleteProjectLockedFail()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/projects/1');
        $this->assertResponseCode(400);
        $expected = [
            'errors' => [
                'id' => 'Project is not deletable',
            ],
        ];
        $this->assertEquals($expected, $this->getResponse());
    }

    public function testFilterValideAfterProjectBackToBrouillon()
    {
        $this->assertTrue(copy($this->cheminFilePdf . 'mainDocumentTemplate.pdf', $this->mainDocument));
        $this->assertTrue(copy($this->cheminFilePdf . 'mainDocumentTemplate.pdf', $this->pdfDocument));
        $this->assertTrue(copy($this->mainDocumentTemplate, $this->cheminFilePdf . 'annexe_001.pdf'));
        $this->assertTrue(copy($this->mainDocumentTemplate, $this->cheminFilePdf . 'annexe_002.pdf'));

        $this->setJsonRequest();
        $project = [
            'theme_id' => 1,
            'name' => 'Validé puis brouillon',
            'stateact_id' => VALIDATED,
            'typesact_id' => 1,
            'sittings' => [],
            'matiere_id' => 1,
            'ismultichannel' => false,
            'code_act' => null,
            'maindocument' => [
                'codetype' => '99_AR',
            ],
            'annexes' => [],
        ];
        $fileMainDocument = new SplFileInfo($this->mainDocument);

        $data = [
            'project' => json_encode($project),
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
        ];
        $this->post('/api/v1/projects', $data);
        $this->assertResponseCode(201);
        $newId = $this->getResponse()['id'];

        $this->setJsonRequest();
        $this->get('/api/v1/projects/' . $newId);
        $body = $this->getResponse();
        $this->assertEquals($body['project']['containers'][0]['stateacts'][0]['id'], VALIDATED);

        $project['stateact_id'] = DRAFT;

        $fileMainDocument = new SplFileInfo($this->pdfDocument);
        $data = [
            'project' => json_encode($project),
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
            'annexes' => [],
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/projects/' . $newId, $data);
        $bodyAfterEdit = $this->getResponse();

        $this->setJsonRequest();
        $this->get('/api/v1/projects/' . $newId);
        $bodyAfterEdit = $this->getResponse();
        $this->assertEquals($bodyAfterEdit['project']['containers'][0]['stateacts'][0]['id'], DRAFT);
    }

    public function testManualSignatureSuccess()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/projects/1');
        $previous = $this->getResponse()['project'];

        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'signature_date' => '2019-05-28',
            ]
        );

        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
            'annexes' => [],
        ];

        $this->put('/api/v1/projects/1/manual-signature', $data);
        $this->assertResponseCode(200);

        $actual = $this->getResponse()['project'];

        $expected = array_merge(
            $this->records[1],
            [
                'isVoted' => [
                    'value' => false,
                    'data' => '',
                ],
                'signature_date' => '2019-05-28',
                'modified' => $actual['modified'],
                'availableActions' =>
                    array_merge(
                        $this->records[1]['availableActions'],
                        [
                            'can_be_send_to_workflow' => [
                                'value' => false,
                                'data' => 'Le projet n\'est pas dans l\'état "Brouillon" ou "Refusé"',
                            ],
                            'can_be_voted' => [
                                'value' => false,
                                'data' => 'Le projet a un état qui ne permet pas de le voter.',
                            ],
                            'can_generate_act_number' => [
                                'value' => false,
                                'data' => 'Le numéro d\'acte ne peu pas être généré',
                            ],
                        ]
                    ),
                'containers' => [
                    array_merge(
                        $this->records[1]['containers'][0],
                        [
                            'modified' => $actual['containers'][0]['modified'],
                        ]
                    ),
                ],
            ]
        );
        // @fixme : peut sauter si on uniformise les retours de ProjectController
        unset(
            $expected['is_deletable'],
            $expected['is_editable'],
            $expected['workflow'],
            $expected['instance'],
            //            $expected['containers'][0]['typesact'],
            //           $expected['containers'][0]['stateacts'],
            $expected['containers'][0]['sittings'],
            $expected['containers'][0]['maindocument']['typespiecesjointe'],
            $expected['containers'][0]['maindocument']['files']
        );
        $expected['containers'][0]['stateacts'] = [
            'id' => 5,
            'name' => 'Déclaré signé',
            'code' => 'declare-signe',
            'created' => '2019-02-18T13:57:59+00:00',
            'modified' => '2019-02-18T13:57:59+00:00',
        ];
        $expected['containers'][0]['typesact']['availableActions'] = [
            'can_be_edited' => [
                'value' => true,
                'data' => null,
            ],
            'can_be_deleted' => [
                'value' => false,
                'data' => 'Le type d\'acte ne peut pas être supprimé.',
            ],
            'can_be_deactivate' => [
                'value' => true,
                'data' => null,
            ],
        ];
        $actual['containers'][0]['annexes'] = [];
        $this->assertEquals($expected, $actual);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/1');
        $actual = $this->getResponse()['project'];
        $this->assertEquals(count($previous['containers'][0]['histories']) + 2, count($actual['containers'][0]['histories'])); // +2 Doc principal + signature
        $this->assertEquals(
            HistoriesTable::MANUAL_SIGNATURE,
            $actual['containers'][0]['histories'][count($actual['containers'][0]['histories']) - 1]['comment']
        );
    }

    /**
     * @throws Exception
     */
    public function testManualSignatureFail()
    {
        $this->assertTrue(copy($this->mainDocumentTemplate, $this->mainDocument));
        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'signature_date' => '',
            ]
        );

        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
        ];

        $this->put('/api/v1/projects/1/manual-signature', $data);
        $this->assertResponseCode(400);

        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'signature_date' => [
                    '_empty' => 'This field cannot be left empty',
                ],
            ],
        ];

        $this->assertEquals($expected, $actual);

        // @fixme : this one does nothing ...
        $actual = $this->Projects
            ->find()
            ->select(['id', 'signature_date'])
            ->contain(
                [
                    'Containers.Maindocuments' => [
                        'fields' => ['name'],
                    ],
                    'Containers.Stateacts',
                ]
            )
            ->where(['id' => 1])
            ->enableHydration(false)
            ->first();
        $expected = [
            'id' => 1,
            'signature_date' => null,
            'containers' => [
                0 => [
                    'id' => 1,
                    'structure_id' => 1,
                    'act_id' => 1,
                    'project_id' => 1,
                    'typesact_id' => 1,
                    'created' => '2019-02-21T14:30:14+00:00',
                    'modified' => '2019-02-21T14:30:14+00:00',
                    'stateacts' => [
                        0 => [
                            'id' => 1,
                            'name' => 'Brouillon',
                            'code' => 'brouillon',
                            'created' => '2019-02-18T13:57:59+00:00',
                            'modified' => '2019-02-18T13:57:59+00:00',
                        ],
                    ],
                    'maindocument' => [
                        'name' => 'Lorem ipsum dolor sit amet',
                    ],
                ],
            ],
        ];
    }

    public function testEditWithoutMainDocumentSuccess()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/projects/1');
        $previous = $this->getResponse()['project'];

        $changes = [
            'id' => 1,
            'theme_id' => 1,
            'name' => 'Name Modifié',
            'stateact_id' => 1,
            'typesact_id' => 1,
            'sittings' => [6, 7],
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/projects/1', ['project' => json_encode($changes)]);
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/1');
        $actual = $this->getResponse()['project'];

        $this->assertEquals($changes['name'], $actual['name']);
        $this->assertEquals([6, 7], Hash::extract($actual['containers'][0]['sittings'], '{n}.id'));
        $this->assertEquals(count($previous['containers'][0]['histories']) + 1, count($actual['containers'][0]['histories']));
        $this->assertEquals(
            HistoriesTable::EDIT_PROJECT,
            $actual['containers'][0]['histories'][count($actual['containers'][0]['histories']) - 1]['comment']
        );
    }

    public function testEditSignedWithoutMainDocumentSuccess()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/projects/27');
        $previous = $this->getResponse()['project'];

        $changes = [
            'id' => 27,
            'theme_id' => 1,
            'name' => 'Name Modifié',
            'stateact_id' => 1,
            'typesact_id' => 1,
            'sittings' => [6, 7],
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/projects/1', ['project' => json_encode($changes)]);
        $this->assertResponseCode(204);
    }

    public function testEditWithMainDocumentSuccess()
    {
        $this->assertTrue(copy($this->mainDocumentTemplate, $this->mainDocument));
        $changes = [
            'id' => 1,
            'theme_id' => 1,
            'name' => 'Name Modifié',
            'stateact_id' => 1,
            'sittings' => [6, 7],
            'typesact_id' => 1,
            'maindocument' => [
                'codetype' => '99_AR',
            ],
            'annexes' => [],
        ];
        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'id' => 1,
            'project' => json_encode($changes),
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/projects/1', $data);
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/1');
        $actual = $this->getResponse()['project'];

        $this->assertEquals($changes['name'], $actual['name']);
        $this->assertEquals([6, 7], Hash::extract($actual['containers'][0]['sittings'], '{n}.id'));

        $this->setJsonRequest();
        $this->get('/api/v1/projects/1');
        $this->assertResponseCode(200);

        $body = $this->getResponse();
        $expected = [
            'id' => $body['project']['containers'][0]['maindocument']['id'],
            'structure_id' => 1,
            'container_id' => 1,
            'name' => 'mainDocument.pdf',
            'current' => true,
            'codetype' => '99_AR',
            'files' => [
                0 => [
                    'id' => $body['project']['containers'][0]['maindocument']['files'][0]['id'],
                    'structure_id' => 1,
                    'maindocument_id' => $body['project']['containers'][0]['maindocument']['id'],
                    'annex_id' => null,
                    'generate_template_id' => null,
                    'draft_template_id' => null,
                    'project_text_id' => null,
                    'attachment_summon_id' => null,
                    'name' => 'mainDocument.pdf',
                    'path' => $body['project']['containers'][0]['maindocument']['files'][0]['path'],
                    'mimetype' => 'application/pdf',
                    'size' => 30951,
                    'created' => $body['project']['containers'][0]['maindocument']['files'][0]['created'],
                    'modified' => $body['project']['containers'][0]['maindocument']['files'][0]['modified'],
                ],
            ],
            'typespiecesjointe' => [
                'id' => 3,
                'codetype' => '99_AR',
                'libelle' => 'Acte réglementaire',
            ],
            'created' => $body['project']['containers'][0]['maindocument']['created'],
            'modified' => $body['project']['containers'][0]['maindocument']['modified'],
        ];
        $this->assertEquals('Name Modifié', $body['project']['name']);
        $this->assertEquals($expected, $body['project']['containers'][0]['maindocument']);
    }

    public function testEditMainDocNotPdfBeforeSigningFail()
    {
        $this->assertTrue(copy($this->cheminFileOdt . 'file.odt', $this->mainDocumentOdt));
        $changes = [
            'theme_id' => 1,
            'name' => 'Name Modifié',
            'stateact_id' => 1,
            'sittings' => [6, 7],
            'typesact_id' => 1,
            'maindocument' => [
                'codetype' => '99_AR',
            ],
            'annexes' => [],
        ];
        $fileMainDocument = new SplFileInfo($this->mainDocumentOdt);
        $data = [
            'project' => json_encode($changes),
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/vnd.oasis.opendocument.text'
            ),
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/projects/1', $data);
        $this->assertResponseCode(400);
    }

    public function testEditCodeTypeNotPdfAfterSigningSuccess()
    {
        $this->assertTrue(copy($this->cheminFileOdt . 'file.odt', $this->mainDocumentOdt));
        $changes = [
            'maindocument' => [
                'codetype' => '99_AR',
            ],
        ];
        $fileMainDocument = new SplFileInfo($this->mainDocumentOdt);
        $data = [
            'project' => json_encode($changes),
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/vnd.oasis.opendocument.text'
            ),
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/projects/27', $data);
        $this->assertResponseCode(204);
        $actual = $this->getResponse();

        $expected = [
            'project' => [
                'id' => 27,
                'structure_id' => 1,
                'theme_id' => 1,
                'template_id' => 1,
                'id_flowable_instance' => null,
                'workflow_id' => null,
                'name' => 'Projet déclaré signé, non transmissible',
                'code_act' => null,
                'signature_date' => null,
                'ismultichannel' => false,
                'containers' => [
                    [
                        'id' => 27,
                        'structure_id' => 1,
                        'act_id' => null,
                        'project_id' => 27,
                        'typesact_id' => 1,
                        'created' => '2019-02-11T10:28:20+00:00',
                        'tdt_id' => null,
                        'modified' => '2019-02-11T10:28:20+00:00',
                        'maindocument' => [
                            'id' => 27,
                            'structure_id' => 1,
                            'container_id' => 27,
                            'name' => 'Main document 27',
                            'current' => true,
                            'codetype' => '99_AR',
                            'created' => '2019-02-11T10:28:28+00:00',
                            'modified' => '2019-02-11T10:28:28+00:00',
                        ],
                        'annexes' => [],
                        'generation_job_id' => null,
                    ],
                ],
                'matiere' => [],
                'availableActions' => [
                    'can_be_send_to_i_parapheur' => [
                        'value' => false,
                        'data' => 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
                    ],
                    'can_be_send_to_manual_signature' => [
                        'value' => false,
                        'data' => 'Le projet n\'est pas dans l\'état "Validé" ou "Refusé par l\'i-parapheur" et/ou ne possède pas de numéro d\'acte',
                    ],
                    'can_be_send_to_workflow' => [
                        'value' => false,
                        'data' => 'Le projet n\'est pas dans l\'état "Brouillon" ou "Refusé"',
                    ],
                    'can_be_voted' => [
                        'value' => false,
                        'data' => 'Le projet a un état qui ne permet pas de le voter.',
                    ],
                    'check_signing' => [
                        'value' => false,
                        'data' => 'Le projet n\'est pas dans l\'état "En cours de signature l\'i-parapheur"',
                    ],
                    'check_refused' => [
                        'value' => false,
                        'data' => 'Le projet n\'est pas dans l\'état "Refusé par l\'i-parapheur"',
                    ],
                    'can_generate_act_number' => [
                        'value' => false,
                        'data' => 'Le numéro d\'acte ne peu pas être généré',
                    ],
                ],
                'isVoted' => [
                    'value' => false,
                    'data' => null,
                ],
            ],
        ];
        unset($actual['project']['created']);
        unset($actual['project']['modified']);
        $this->assertEquals($expected, $actual);
    }

//    public function testEditWithoutMainDocumentError()
//    {
//        $this->setJsonRequest();
//        $jsonProject = json_encode(
//            [
//                'name' => null,
//            ]
//        );
//
//        $data = [
//            'project' => $jsonProject,
//        ];
//
//        $this->put('/api/v1/projects/1', $data);
//        $this->assertResponseCode(400);
//
//        $actual = $this->getResponse();
//        $expected = [
//            'errors' => [
//                'id' => [
//                    "SQLSTATE[23502]: Not null violation: 7 ERROR:  null value in column \"stateact_id\" violates not-null constraint\n" .
//                    "DETAIL:  Failing row contains (17, 1, null, " . date('Y-m-d') . " 00:00:00).",
//                ],
//                'name' => [
//                    '_empty' => 'This field cannot be left empty',
//                ],
//                'containers' => [
//                    [
//                        'typesact_id' => [
//                            '_empty' => 'This field cannot be left empty',
//                        ],
//                    ],
//                ],
//            ],
//        ];
//        $this->assertEquals(preg_match('/(^SQLSTATE[23502])*(stateact_id)/', $expected['errors']['id'][0]), 1);
//        // $this->assertEquals($expected['errors']['name'], $actual['errors']['name']);
//        //$this->assertEquals($expected['errors']['containers'], $actual['errors']['containers']);
//    }

    public function testEditProjectOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "projects"');
        $this->disableErrorHandlerMiddleware();

        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Name Modifié',
                'stateact_id' => 1,
                'sittings' => [6],
                'typesact_id' => 1,
                'maindocument' => [
                    'codetype' => '99_AR',
                ],
                'annexes' => [],
            ]
        );
        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $fileMainDocument->getRealPath(),
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
        ];
        $this->setJsonRequest();
        $this->patch('/api/v1/projects/36', $data);
        $this->assertResponseCode(404);

        $this->setJsonRequest();
        $this->put('/api/v1/projects/36', $data);
        $this->assertResponseCode(404);
    }

    public function testGetTdtUrl()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/projects/getTdtUrl/1');
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $expected = [
            'url' => 'https://s2low.formations.libriciel.fr/modules/actes/actes_transac_post_confirm_api.php?id=',
        ];
        $this->assertEquals($expected, $response);
    }

    public function testGetTdtUrlMultiple()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/projects/getTdtUrlMultiple?ids[]=1&ids[]=2&ids[]=3');
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $expected = [
            'url' => 'https://s2low.formations.libriciel.fr/modules/actes/actes_transac_post_confirm_api.php?id[]=&id[]=&id[]=',
        ];
        $this->assertEquals($expected, $response);
    }

    public function testTeletransmissionOrdered()
    {
        $this->setJsonRequest();
        $data = [
            'projectIds' => [1],
        ];
        $this->post('/api/v1/projects/teletransmissionOrdered', $data);
        $this->assertResponseCode(200);

        //get the corresponding container
        /**
         * @var ContainersTable $Containers
         */
        $Containers = $this->fetchTable('Containers');
        $container = $Containers->find()
            ->select(['id'])
            ->where(['project_id' => 1, 'structure_id' => 1])
            ->firstOrFail();

        $stateAct = $Containers->getLastStateActEntry($container->get('id'));
        $this->assertEquals(PENDING_TDT, $stateAct[0]['id']);
    }

    public function testGetArActes()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/projects/getArActes/1');
        $this->assertResponseCode(200);
    }

    public function testRedactorCantDeleteProjectNotInDraft()
    {
        $this->switchUser(4);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/index_validated');
        $body = $this->getResponse();
        $this->assertEquals(
            [38 => false, 37 => false, 35 => false, 20 => false, 19 => false, 18 => false, 17 => false, 16 => false, 15 => false, 14 => false],
            Hash::combine($body, 'projects.{n}.id', 'projects.{n}.availableActions.can_be_deleted.value')
        );

        $this->setJsonRequest();
        $this->get('/api/v1/projects/index_drafts');
        $body = $this->getResponse();
        $this->assertEquals(
            [21 => true, 1 => false],
            Hash::combine($body, 'projects.{n}.id', 'projects.{n}.availableActions.can_be_deleted.value')
        );

        $this->switchUser(1);

        $this->setJsonRequest();
        $this->get('/api/v1/projects/index_validated');
        $body = $this->getResponse();
        $this->assertEquals(
            [38 => true, 37 => false, 35 => true, 20 => true, 19 => false, 18 => true, 17 => true, 16 => true, 15 => true, 14 => true],
            Hash::combine($body, 'projects.{n}.id', 'projects.{n}.availableActions.can_be_deleted.value')
        );

        $this->setJsonRequest();
        $this->get('/api/v1/projects/index_drafts');
        $body = $this->getResponse();
        $this->assertEquals(
            [22 => true, 21 => true, 2 => true, 1 => false],
            Hash::combine($body, 'projects.{n}.id', 'projects.{n}.availableActions.can_be_deleted.value')
        );
    }

    public function testGenerateCounterAction()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/projects/generate-act-number/38');

        $response = $this->getResponse();
        $this->assertTrue($response['success']);
        $this->assertNotEmpty($response['generatedActNumber']);
    }

    public function testProjectNotdeletableCantBeDeleted()
    {
        $this->setJsonRequest();

        $this->delete('/api/v1/projects/1');

        $this->assertResponseCode(400);
        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'id' => 'Project is not deletable',
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    //    public function testGetMenu()
    //    {
    //        $indexes = [
    //            'index_drafts',
    //            'index_validating',
    //            'index_to_validate',
    //            'index_validated',
    //            'index_to_transmit',
    //            'index_ready_to_transmit',
    //            'index_act',
    //        ];
    //
    //        $users = [
    //            1,
    //            4,
    //            5
    //        ];
    //        $viewedProjects = [];
    //
    //        foreach ($users as $user) {
    //            $this->switchUser($user);
    //
    //            foreach ($indexes as $index) {
    //                $page = 1;
    //                $this->setJsonRequest();
    //                $this->get('/api/v1/projects/' . $index . '?page=' . $page);
    //                $body = $this->getResponse();
    //                $count = $body['pagination']['count'];
    //                for ($page = 1; $page <= $count; $page++) {
    //                    foreach ($body['projects'] as $project) {
    //                        $this->setJsonRequest();
    //                        $this->get('/api/v1/projects/' . $project['id'] . '/menu');
    //                        $response = $this->getResponse();
    //                        $this->assertEquals($index, ProjectsTable::INDEXES[$response['menu']]);
    //                        $viewedProjects[$project['id']] = $project['id'];
    //                    }
    //                }
    //            }
    //            //$this->assertEquals(count($this->records), count($viewedProjects));
    //        }
    //    }

    /**
     * @dataProvider dataCheckPermissionsForSignatureAndActNumber
     */
    public function testCheckPermissionsForSignatureAndActNumber(int $projectId, string $username, int $expectedCode, array $expectedPermissions)
    {
        $this->switchUser($this->getUserId($username));

        $this->setJsonRequest();
        $this->get(sprintf('/api/v1/projects/%d', $projectId));
        $body = $this->getResponse();
        $this->assertResponseCode($expectedCode);

        $actual = [];
        foreach (array_keys($expectedPermissions) as $key) {
            $actual[$key] = $body['project']['availableActions'][$key]['value'] ?? null;
        }
        $this->assertEquals($expectedPermissions, $actual);
    }

    public function dataCheckPermissionsForSignatureAndActNumber(): array
    {
        return [
            // Projet 37, état VOTED_APPROVED
            // - Administrateur
            [37, 's.blanc', 200, ['can_be_send_to_i_parapheur' => true, 'can_be_send_to_manual_signature' => true, 'can_generate_act_number' => false]],
            // - Administrateur Fonctionnel
            [37, 'm.vert', 200, ['can_be_send_to_i_parapheur' => true, 'can_be_send_to_manual_signature' => true, 'can_generate_act_number' => false]],
            // - Secrétariat général
            [37, 'j.orange', 200, ['can_be_send_to_i_parapheur' => true, 'can_be_send_to_manual_signature' => true, 'can_generate_act_number' => false]],
            // - Rédacteur / Valideur
            [37, 'r.violet', 200, ['can_be_send_to_i_parapheur' => false, 'can_be_send_to_manual_signature' => false, 'can_generate_act_number' => false]],
            // - Super Administrateur
            [37, 'r.cyan', 200, ['can_be_send_to_i_parapheur' => true, 'can_be_send_to_manual_signature' => true, 'can_generate_act_number' => false]],
            // Projet 38, état VALIDATED
            // - Administrateur
            [38, 's.blanc', 200, ['can_be_send_to_i_parapheur' => false, 'can_be_send_to_manual_signature' => false, 'can_generate_act_number' => true]],
            // - Administrateur Fonctionnel
            [38, 'm.vert', 200, ['can_be_send_to_i_parapheur' => false, 'can_be_send_to_manual_signature' => false, 'can_generate_act_number' => true]],
            // - Secrétariat général
            [38, 'j.orange', 200, ['can_be_send_to_i_parapheur' => false, 'can_be_send_to_manual_signature' => false, 'can_generate_act_number' => true]],
            // - Rédacteur / Valideur
            [38, 'r.violet', 200, ['can_be_send_to_i_parapheur' => false, 'can_be_send_to_manual_signature' => false, 'can_generate_act_number' => false]],
            // - Super Administrateur
            [38, 'r.cyan', 200, ['can_be_send_to_i_parapheur' => false, 'can_be_send_to_manual_signature' => false, 'can_generate_act_number' => true]],
        ];
    }

    /**
     * @return void
     */
    public function testDuplicateCodeAct()
    {
        $files = [
            $this->mainDocument,
            $this->cheminFilePdf . 'annexe_001.pdf',
            $this->cheminFilePdf . 'annexe_002.pdf',
        ];

        foreach ($files as $file) {
            $this->assertTrue(copy($this->mainDocumentTemplate, $file));
        }
        $annexe1 = new SplFileInfo($this->cheminFilePdf . 'annexe_001.pdf');
        $annexe2 = new SplFileInfo($this->cheminFilePdf . 'annexe_002.pdf');

        $this->setJsonRequest();

        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Un projet au code act existant',
                'stateact_id' => VALIDATED,
                'typesact_id' => 1,
                'sittings' => [],
                'matiere_id' => 1,
                'ismultichannel' => false,
                'code_act' => 'ACT_151',
                'maindocument' => [
                    'codetype' => '99_AR',
                ],
                'annexes' => [],
            ]
        );

        $fileMainDocument = new SplFileInfo($this->mainDocument);
        $data = [
            'project' => $jsonProject,
            'mainDocument' => new UploadedFile(
                $this->mainDocument,
                $fileMainDocument->getSize(),
                UPLOAD_ERR_OK,
                $fileMainDocument->getFilename(),
                'application/pdf'
            ),
            'annexes' => [
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_001.pdf',
                    $annexe1->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe1->getFilename(),
                    'application/pdf'
                )
                ,
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_002.pdf',
                    $annexe2->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe2->getFilename(),
                    'application/pdf'
                ),
            ],
        ];

        $this->post('/api/v1/projects', $data);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $this->assertEquals('This value is already in use', $body['errors']['code_act']['_isUnique']);
    }

    public function testAddAnnexe()
    {
        $this->setJsonRequest();

        $this->assertTrue(copy($this->mainDocumentTemplate, $this->cheminFilePdf . 'annexe_001.pdf'));
        $annexe1 = new SplFileInfo($this->cheminFilePdf . 'annexe_001.pdf');

        $data = [
            'project' => json_encode([
                'typesact_id' => 1,
                'annexes' => [[
                    'codetype' => '99_AR',
                    'is_generate' => true,
                    'istransmissible' => true]],
            ]),
            'annexes' => [
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_001.pdf',
                    $annexe1->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe1->getFilename(),
                    'application/pdf'
                ),
            ],
        ];

        $this->patch('/api/v1/projects/1', $data);
        $this->assertResponseCode(204);
        $annexe1 = $this->fetchTable('Annexes')->get(1);
        $this->assertNotNull($annexe1);
        $this->assertEquals($annexe1->get('codetype'), '99_AR');
    }

    /**
     * @return void
     */
    public function testAddAndDeleteAnnexe()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->setJsonRequest();

        $this->assertTrue(copy($this->mainDocumentTemplate, $this->cheminFilePdf . 'annexe_001_test_delete.pdf'));
        $annexe1 = new SplFileInfo($this->cheminFilePdf . 'annexe_001_test_delete.pdf');

        $data = [
            'project' => json_encode([
                'typesact_id' => 1,
                'annexes' => [[
                    'is_generate' => true,
                    'istransmissible' => false]],
            ]),
            'annexes' => [
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_001_test_delete.pdf',
                    $annexe1->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe1->getFilename(),
                    'application/pdf'
                ),
            ],
        ];

        $this->patch('/api/v1/projects/1', $data);

        $this->assertResponseCode(204);

        $this->setJsonRequest();

        $this->get('/api/v1/projects/1');
        $this->assertResponseCode(200);
        $this->assertNotNull($this->fetchTable('Annexes')->get(1));

        $data = [
            'project' => json_encode([
                'typesact_id' => 1,
                'annexes' => []]),
        ];

        $this->setJsonRequest();

        $this->patch('/api/v1/projects/1', $data);
        $this->assertResponseCode(204);

        $this->fetchTable('Annexes')->get(1);
    }

    /**
     * @return void
     */
    public function testEditWIthDuplicatedCodeAct()
    {
        $data = json_encode([
            'theme_id' => 9,
            'name' => 'Un projet au code act existant',
            'stateact_id' => VALIDATED,
            'typesact_id' => 8,
            'sittings' => [],
            'matiere_id' => 3,
            'ismultichannel' => false,
            'code_act' => 'ACT_151',
            'annexes' => [],
        ]);

        $this->setJsonRequest();

        $this->put('/api/v1/projects/2', ['project' => $data]);
        $this->assertResponseCode(400);
        $expected = [
            'errors' => [
                'code_act' => [
                    '_isUnique' => 'This value is already in use',
                ],
            ],
        ];
        $this->assertSame($expected, $this->getResponse());
    }

    /**
     * @return void
     */
    public function testEditCheckHistory()
    {
        $this->switchUser(12);

        $data = json_encode([
            'theme_id' => 1,
            'name' => 'Un projet au code act existant',
            'stateact_id' => VALIDATED,
            'typesact_id' => 1,
            'sittings' => [],
            'matiere_id' => 1,
            'ismultichannel' => false,
            'code_act' => 'ACT_151',
            'annexes' => [],
        ]);

        $this->setJsonRequest();

        $this->put('/api/v1/projects/39', ['project' => $data]);
        $this->assertResponseCode(204);

        $this->switchUser(11);

        $this->setJsonRequest();
        $this->put('/api/v1/projects/39', ['project' => $data]);

        $this->setJsonRequest();

        $this->get('/api/v1/projects/39');

        $actual = [];
        foreach ($this->getResponse()['project']['containers'][0]['histories'] as $history) {
            $actual[] = [
                'user_id' => $history['user_id'],
                'comment' => $history['comment'],
            ];
        }

        $expected = [
            ['user_id' => 12, 'comment' => HistoriesTable::EDIT_PROJECT],
            ['user_id' => 11, 'comment' => HistoriesTable::EDIT_PROJECT],
        ];

        $this->assertEquals($expected, $actual);
    }
}
