<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;

/**
 * App\Controller\LogController Test Case
 */
class LogsControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/logs');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertTrue($body['success']);
    }
}
