<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\Mock\FlowableWrapperMock;
use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;

/**
 * App\Controller\WorkflowsTasksController Test Case
 */
class WorkflowsTasksControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
    ];

    public const ACTION_APPROVE = 'approve';
    public const ACTION_REJECT = 'reject';
    public const ACTION_CANCEL = 'cancel';
    public const ACTION_ROLLBACK = 'rollback_';
    public const ACTION_FORWARD = 'forward';
    public const ACTION_APPROVE_FORWARD = 'approve_forward';
    public const ACTION_BYPASS = 'bypass';
    public const STATUS_APPROVED = 'approved';
    public const STATUS_REJECTED = 'rejected';

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
        Configure::write('Flowable.wrapper_mock', '\App\Test\Mock\FlowableWrapperMock');
        FlowableWrapperMock::reset();
    }

    public function testAllTasksByCandidateUserEmpty()
    {
        $this->setJsonRequest();

        $this->get('/api/v1/tasks/allTasksByCandidateUser');
        $this->assertResponseCode(200);

        $body = $this->getResponse();
        $expected = [
            'WorkflowTasks' => [],
        ];
        $this->assertEquals($expected, $body);
    }
}
