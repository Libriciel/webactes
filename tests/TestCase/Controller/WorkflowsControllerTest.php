<?php
declare(strict_types=1);

namespace WrapperFlowable\Test\TestCase\Controller;

use App\Test\Mock\FlowableWrapperMock;
use App\Test\TestCase\AbstractTestCase;
use Cake\Controller\Exception\MissingActionException;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Routing\Exception\MissingRouteException;
use Cake\Utility\Hash;

/**
 * App\Controller\WorkflowsController Test Case
 */
class WorkflowsControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Container',
        'app.group.Project',
        'app.group.Sitting',
        'app.group.Actor',
        'app.group.File',
        'app.Stateacts',
        'app.Notifications',
        'app.Workflows',
        'app.Actors',
        'app.Templates',
        'app.Themes',
        'app.GenerateTemplates',
        'app.Natures',
        'app.Acts',
        'app.Projects',
        'app.Typesacts',
        'app.Typesittings',
        'app.Containers',
        'app.Sittings',
        'app.ContainersStateacts',
        'app.Histories',
        'app.Summons',
    ];

    /**
     * A list of all records returned by the index method. (for structure 1 only)
     *
     * @var array
     */
    protected $records = [
        0 => [
            'id' => 1,
            'name' => '4 -> 5 -> 6',
            'description' => 'description 4 -> 5 -> 6',
            'file' => '1552472001_00000000.bpmn20.xml',
            'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000001',
            'organization_id' => 1,
            'structure_id' => 1,
            'active' => true,
            'is_editable' => false,
            'is_deletable' => false,
        ],
        1 => [
            'id' => 2,
            'name' => '4, 5 -> 6',
            'description' => 'description 4, 5 -> 6',
            'file' => '1552472002_00000000.bpmn20.xml',
            'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000002',
            'organization_id' => 1,
            'structure_id' => 1,
            'active' => true,
            'is_editable' => false,
            'is_deletable' => false,
        ],
        2 => [
            'id' => 3,
            'name' => '5 -> 6',
            'description' => 'description 5 -> 6',
            'file' => '1552472003_00000000.bpmn20.xml',
            'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000003',
            'organization_id' => 1,
            'structure_id' => 1,
            'active' => true,
            'is_editable' => false,
            'is_deletable' => false,
        ],
        3 => [
            'id' => 4,
            'name' => 'not instancied circuit',
            'description' => 'description not instancied circuit',
            'file' => '1552472004_00000000.bpmn20.xml',
            'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000004',
            'organization_id' => 1,
            'structure_id' => 1,
            'active' => true,
            'is_editable' => true,
            'is_deletable' => true,
        ],
        4 => [
            'id' => 5,
            'name' => 'Inactive circuit',
            'description' => 'circuit inactif',
            'file' => '1552472005_00000000.bpmn20.xml',
            'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000005',
            'organization_id' => 1,
            'structure_id' => 1,
            'active' => false,
            'is_editable' => true,
            'is_deletable' => true,
        ],
    ];

    // use union with records when steps details are needed
    protected $stepRecords = [
        0 => [
            [
                'name' => '4',
                'validators' => [
                    [
                        'id' => 4,
                        'firstname' => 'Rémi',
                        'lastname' => 'VIOLET',
                    ],
                ],
            ],
            [
                'name' => '5',
                'validators' => [
                    [
                        'id' => 5,
                        'firstname' => 'Romain',
                        'lastname' => 'Cyan',
                    ],
                ],
            ],
            [
                'name' => '6',
                'validators' => [
                    [
                        'id' => 6,
                        'firstname' => 'Sébastien',
                        'lastname' => 'White',
                    ],
                ],
            ],
        ],
        1 => [
            [
                'name' => '4, 5',
                'validators' => [
                    [
                        'id' => 4,
                        'firstname' => 'Rémi',
                        'lastname' => 'VIOLET',
                    ],
                    [
                        'id' => 5,
                        'firstname' => 'Romain',
                        'lastname' => 'Cyan',
                    ],
                ],
            ],
            [
                'name' => '6',
                'validators' => [
                    [
                        'id' => 6,
                        'firstname' => 'Sébastien',
                        'lastname' => 'White',
                    ],
                ],
            ],
        ],
        2 => [
            [
                'name' => '5',
                'validators' => [
                    [
                        'id' => 5,
                        'firstname' => 'Romain',
                        'lastname' => 'Cyan',
                    ],
                ],
            ],
            [
                'name' => '6',
                'validators' => [
                    [
                        'id' => 6,
                        'firstname' => 'Sébastien',
                        'lastname' => 'White',
                    ],
                ],
            ],
        ],
        3 => [
            [
                'name' => '4',
                'validators' => [
                    [
                        'id' => 4,
                        'firstname' => 'Rémi',
                        'lastname' => 'VIOLET',
                    ],
                ],
            ],
        ],
    ];

    public const FLOWABLE_FILE_REGEXP = '/^[0-9]{10}_[0-9]{8}.bpmn20.xml/';

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
        Configure::write('Flowable.wrapper_mock', '\App\Test\Mock\FlowableWrapperMock');
        FlowableWrapperMock::reset();
    }

    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/circuits');
        $body = $this->getResponse();
        $this->records = $records = [
            0 => [
                'id' => 1,
                'name' => '4 -> 5 -> 6',
                'description' => 'description 4 -> 5 -> 6',
                'file' => '1552472001_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000001',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => true,
                'is_editable' => false,
                'is_deletable' => false,
            ],
            1 => [
                'id' => 2,
                'name' => '4, 5 -> 6',
                'description' => 'description 4, 5 -> 6',
                'file' => '1552472002_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000002',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => true,
                'is_editable' => false,
                'is_deletable' => false,
            ],
            2 => [
                'id' => 3,
                'name' => '5 -> 6',
                'description' => 'description 5 -> 6',
                'file' => '1552472003_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000003',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => true,
                'is_editable' => false,
                'is_deletable' => false,
            ],
            4 => [
                'id' => 4,
                'name' => 'not instancied circuit',
                'description' => 'description not instancied circuit',
                'file' => '1552472004_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000004',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => true,
                'is_editable' => true,
                'is_deletable' => true,
            ],
            3 => [
                'id' => 5,
                'name' => 'Inactive circuit',
                'description' => 'circuit inactif',
                'file' => '1552472005_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000005',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => false,
                'is_editable' => true,
                'is_deletable' => true,
            ],
        ];
        $this->assertEquals($this->records, $body['circuits']);
    }

    /**
     * @dataProvider dataAccessIndex
     */
    public function testAccessIndex(string $username, int $expected): void
    {
        $this->switchUser($this->getUserId($username));
        $this->setJsonRequest();
        $this->get('/api/v1/circuits');
        $this->assertResponseCode($expected);
    }

    public function dataAccessIndex(): array
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    public function testCreationWithNotUniqueNameFail()
    {
        $this->setJsonRequest();
        $data = [
            'name' => $this->records[0]['name'],
            'description' => 'description',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);
        $this->assertResponseCode(400);
        $response = $this->getResponse();
        $expected = [
            'errors' => [
                'name' => ['duplicate' => 'duplicate name for this structure'],
            ],
        ];
        $this->assertEquals($expected, $response);
    }

    public function testCreateSameNameOtherStructureOk()
    {
        $this->markTestSkipped('is not available.');
        $this->switchUser(2);
        $this->setJsonRequest();
        $data = [
            'name' => $this->records[0]['name'],
            'description' => 'testCreateSameNameOtherStructureOk',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [7],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);
        $this->assertResponseCode(201);
    }

    public function testCreateSameNameAfterDeleteOk()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/circuits/4');
        $this->assertResponseCode(204);
        $this->setJsonRequest();
        $data = [
            'name' => $this->records[3]['name'],
            'description' => 'description',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);
        $this->assertResponseCode(201);
    }

    public function testCreateWithNotExistentUserFail()
    {
        $this->setJsonRequest();
        $deployment = [
            'name' => 'NotExistentUser',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [999999],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [5],
                ],
                [
                    'name' => 'Etape 3',
                    'users' => [4],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $deployment);
        $this->assertResponseCode(400);
        $expected = [
            'errors' => [
                'steps' => 'invalid users data for step #0 : Etape 1',
            ],
        ];
        $this->assertEquals($expected, $this->getResponse());
    }

    public function testCreateWithOtherStructureUserFail()
    {
        $this->setJsonRequest();
        $deployment = [
            'name' => 'OtherStructureUser',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [7],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $deployment);
        $this->assertResponseCode(400);
        $expected = [
            'errors' => [
                'steps' => 'invalid users data for step #1 : Etape 2',
            ],
        ];
        $this->assertEquals($expected, $this->getResponse());
    }

    /**
     * @dataProvider dataAccessAddCircuit
     */
    public function testAccessAddCircuit(string $username, int $expected): void
    {
        $this->switchUser($this->getUserId($username));
        $this->setJsonRequest();
        $deployment = [
            'name' => 'new Circuit',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [5],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $deployment);
        $this->assertResponseCode($expected);
    }

    public function dataAccessAddCircuit(): array
    {
        return [
            ['s.blanc', 201], // Administrateur
            ['m.vert', 201], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 201], // Super Administrateur
        ];
    }

    public function testNewCircuitIsDeletable()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'newCircuit',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [5],
                ],
                [
                    'name' => 'Etape 3',
                    'users' => [4],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();
        $this->assertEquals(true, $body['circuit']['is_deletable']);
    }

    /**
     * @dataProvider dataProviderAccessActive
     */
    public function testAccessActive(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/circuits/active');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessActive()
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 200], // Secrétariat général
            ['r.violet', 200], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataProviderTestActive
     */
    public function testActiveX(string $params, int $status, array $expected): void
    {
        $this->switchUser($this->getUserId('s.blanc'));
        $this->setJsonRequest();

        $this->get('/api/v1/circuits/active' . $params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    public function dataProviderTestActive(): array
    {
        $circuits = [
            ['id' => 1, 'steps' => [['validators' => [['id' => 4]]]]],
            ['id' => 2, 'steps' => [['validators' => [['id' => 4], ['id' => 5]]], ['validators' => [['id' => 6]]]]],
            ['id' => 3, 'steps' => [['validators' => [['id' => 5]]], ['validators' => [['id' => 6]]]]],
            ['id' => 4, 'steps' => [['validators' => [['id' => 4]]]]],
        ];

        return [
            ['', 200, ['circuits' => $circuits, 'pagination' => null]],
            ['?name=4,%205%20->%206', 200, ['circuits' => [$circuits[1]], 'pagination' => null]],
            [
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "name"',
                        ],
                    ],
                ],
            ],
        ];
    }

    public function testActiveAfterDeletion()
    {
        $entries = 4;
        $this->setJsonRequest();
        $this->get('/api/v1/circuits/active');
        $body = $this->getResponse();
        $this->assertEquals($entries, count($body['circuits']));
        $expected = array_fill(0, $entries, true);
        $this->assertEquals($expected, Hash::extract($body, 'circuits.{n}.active'));
        $this->setJsonRequest();
        $this->delete('/api/v1/circuits/4');
        $this->assertResponseCode(204);
        $entries--;

        $this->setJsonRequest();
        $this->get('/api/v1/circuits/active');
        $body = $this->getResponse();

        $this->assertEquals($entries, count($body['circuits']));
        $expected = array_fill(0, $entries, true);
        $this->assertEquals($expected, Hash::extract($body, 'circuits.{n}.active'));
    }

    public function testActivate()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/circuits/4/activate/');
        $this->assertResponseCode(201);
        $this->setJsonRequest();
        $this->get('/api/v1/circuits/active');
        $body = $this->getResponse();
        $this->assertEquals(4, count($body['circuits']));
    }

    /**
     * @dataProvider dataAccessActivate
     */
    public function testAccessActivate(string $username, int $expected): void
    {
        $this->switchUser($this->getUserId($username));
        $this->setJsonRequest();
        $this->post('/api/v1/circuits/4/activate/');
        $this->assertResponseCode($expected);
    }

    public function dataAccessActivate(): array
    {
        return [
            ['s.blanc', 201], // Administrateur
            ['m.vert', 201], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 201], // Super Administrateur
        ];
    }

    /**
     * [testActivateOtherStructureFail description]
     *
     * @return void [type] [description]
     */
    public function testActivateOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "workflows"');
        $this->disableErrorHandlerMiddleware();
        $this->setJsonRequest();

        $this->post('/api/v1/circuits/7/activate/');
        $this->assertResponseCode(404);
    }

    /**
     * [testActivateByNotAdminFail description]
     *
     * @return void [type] [description]
     * @throws Exception
     */
    public function testActivateByNotAdminFail()
    {
        $this->expectException(UnauthorizedException::class);
        $this->expectExceptionMessage('Only admin can activate a circuit');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();

        $this->switchUser(3);
        $this->post('/api/v1/circuits/4/activate/');
        $this->assertResponseCode(403);

        $this->switchUser(4);
        $this->post('/api/v1/circuits/4/activate/');
        $this->assertResponseCode(403);
    }

    public function testDeactivate()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/circuits/2/deactivate/');
        $this->assertResponseCode(201);
        $this->setJsonRequest();
        $this->get('/api/v1/circuits/active');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(3, count($body['circuits']));
    }

    /**
     * @dataProvider dataAccessDeactivate
     */
    public function testAccessDeactivate(string $username, int $expected): void
    {
        $this->switchUser($this->getUserId($username));
        $this->setJsonRequest();
        $this->post('/api/v1/circuits/2/deactivate/');
        $this->assertResponseCode($expected);
    }

    public function dataAccessDeactivate(): array
    {
        return [
            ['s.blanc', 201], // Administrateur
            ['m.vert', 201], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 201], // Super Administrateur
        ];
    }

    /**
     * [testDeactivateOtherStructureFail description]
     *
     * @return void [type] [description]
     */
    public function testDeactivateOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "workflows"');
        $this->disableErrorHandlerMiddleware();
        $this->setJsonRequest();

        $this->post('/api/v1/circuits/9/deactivate/');
        $this->assertResponseCode(404);
    }

    /**
     * [testDeactivateByNotAdminFail description]
     *
     * @return void [type] [description]
     * @throws Exception
     */
    public function testDeactivateByNotAdminFail()
    {
        $this->expectException(UnauthorizedException::class);
        $this->expectExceptionMessage('Only admin can deactivate a circuit');
        $this->disableErrorHandlerMiddleware();
        $this->setJsonRequest();

        $this->switchUser(3);
        $this->post('/api/v1/circuits/1/deactivate/');
        $this->assertResponseCode(403);

        $this->switchUser(4);
        $this->post('/api/v1/circuits/1/deactivate/');
        $this->assertResponseCode(403);
    }

    public function testViewSuccess()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/circuits/1');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();
        $expected = [
            'circuit' => [
                'id' => 1,
                'name' => '4 -> 5 -> 6',
                'description' => 'description 4 -> 5 -> 6',
                'file' => '1552472001_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000001',
                'organization_id' => 1,
                'structure_id' => 1,
                'steps' => [
                    [
                        'name' => '4',
                        'validators' => [
                            [
                                'id' => 4,
                                'firstname' => 'Rémi',
                                'lastname' => 'VIOLET',
                            ],
                        ],
                    ],
                    [
                        'name' => '5',
                        'validators' => [
                            [
                                'id' => 5,
                                'firstname' => 'Romain',
                                'lastname' => 'Cyan',
                            ],
                        ],
                    ],
                    [
                        'name' => '6',
                        'validators' => [
                            [
                                'id' => 6,
                                'firstname' => 'Sébastien',
                                'lastname' => 'White',
                            ],
                        ],
                    ],
                ],
                'active' => true,
                'is_editable' => false,
                'is_deletable' => false,
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * [testView404 description]
     *
     * @return void [type] [description]
     */
    public function testView404()
    {
        $this->expectException(MissingRouteException::class);
        $this->expectExceptionMessage('A route matching "/api/vi/circuits/3" could not be found');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();

        $this->get('/api/vi/circuits/3');
        $this->assertResponseCode(404);
    }

    /**
     * [testViewOtherStructureFail description]
     *
     * @return void [type] [description]
     */
    public function testViewOtherStructureFail()
    {
        $this->expectException(MissingRouteException::class);
        $this->expectExceptionMessage('A route matching "/api/vi/circuits/2" could not be found');
        $this->disableErrorHandlerMiddleware();
        $this->setJsonRequest();

        $this->get('/api/vi/circuits/2');
        $this->assertResponseCode(404);
    }

    public function testCreateCircuit()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'newCircuit',
            'description' => 'description de newCircuit',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [1, 5],
                ],
                [
                    'name' => 'Etape 3',
                    'users' => [4],
                ],
            ],
        ];

        $this->post('/api/v1/circuits', $data);
        $this->assertResponseCode(201);
        $actual = $this->getResponse();
        $expected = [
            'circuit' => [
                'id' => 9,
                'name' => 'newCircuit',
                'description' => 'description de newCircuit',
                'file' => null,
                'process_definition_key' => 'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000002',
                'organization_id' => 1,
                'structure_id' => 1,
                'steps' => [
                    [
                        'name' => 'Etape 1',
                        'validators' => [
                            [
                              'id' => 4,
                              'firstname' => 'Rémi',
                                'lastname' => 'VIOLET',
                            ],
                        ],
                    ],
                    [
                        'name' => 'Etape 2',
                        'validators' => [
                            [
                                'id' => 5,
                                'firstname' => 'Romain',
                                'lastname' => 'Cyan',
                            ],
                            [
                                'id' => 6,
                                'firstname' => 'Sébastien',
                                'lastname' => 'White',
                            ],
                        ],
                    ],
                    [
                        'name' => 'Etape 3',
                        'validators' => [
                            [
                                'id' => 4,
                                'firstname' => 'Rémi',
                                'lastname' => 'VIOLET',
                            ],
                        ],
                    ],
                ],
                'active' => false,
                'is_editable' => true,
                'is_deletable' => true,
            ],
        ];
        if (preg_match(static::FLOWABLE_FILE_REGEXP, $actual['circuit']['file']) === 1) {
            $expected['circuit']['file'] = $actual['circuit']['file'];
        }
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider dataAccessCreateCircuit
     */
    public function testAccessCreateCircuit(string $username, int $expected): void
    {
        $this->switchUser($this->getUserId($username));

        $data = [
            'name' => 'newCircuit',
            'description' => 'description de newCircuit',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [1, 5],
                ],
                [
                    'name' => 'Etape 3',
                    'users' => [4],
                ],
            ],
        ];
        $this->setJsonRequest();
        $this->post('/api/v1/circuits', $data);
        $this->assertResponseCode($expected);
    }

    public function dataAccessCreateCircuit(): array
    {
        return [
            ['s.blanc', 201], // Administrateur
            ['m.vert', 201], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 201], // Super Administrateur
        ];
    }

    public function testCreateDeploymentWithoutDescription()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'newCircuit',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [1, 5],
                ],
                [
                    'name' => 'Etape 3',
                    'users' => [4],
                ],
            ],
        ];

        $this->post('/api/v1/circuits', $data);
        $this->assertResponseCode(201);
        $actual = $this->getResponse();
        $expected = [
            'circuit' => [
                'id' => 9,
                'name' => 'newCircuit',
                'file' => null,
                'process_definition_key' => 'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000002',
                'organization_id' => 1,
                'structure_id' => 1,
                'steps' => [
                    [
                        'name' => 'Etape 1',
                        'validators' => [
                            [
                                'id' => 4,
                                'firstname' => 'Rémi',
                                'lastname' => 'VIOLET',
                            ],
                        ],
                    ],
                    [
                        'name' => 'Etape 2',
                        'validators' => [
                            [
                                'id' => 5,
                                'firstname' => 'Romain',
                                'lastname' => 'Cyan',
                            ],
                            [
                                'id' => 6,
                                'firstname' => 'Sébastien',
                                'lastname' => 'White',
                            ],
                        ],
                    ],
                    [
                        'name' => 'Etape 3',
                        'validators' => [
                            [
                                'id' => 4,
                                'firstname' => 'Rémi',
                                'lastname' => 'VIOLET',
                            ],
                        ],
                    ],
                ],
                'active' => false,
                'is_editable' => true,
                'is_deletable' => true,
            ],
        ];
        if (preg_match(static::FLOWABLE_FILE_REGEXP, $actual['circuit']['file']) === 1) {
            $expected['circuit']['file'] = $actual['circuit']['file'];
        }
        $this->assertEquals($expected, $actual);
    }

    /**
     * [testCreateDeploymentNotAdminFail description]
     *
     * @return void [type] [description]
     */
    public function testCreateDeploymentNotAdminFail()
    {
        $this->expectException(UnauthorizedException::class);
        $this->expectExceptionMessage('Only admin can add a circuit');
        $this->disableErrorHandlerMiddleware();

        $this->switchUser(3);
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $data = [
            'name' => 'WebActes 3',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [5, 6],
                ],
                [
                    'name' => 'Etape 3',
                    'users' => [4],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);
        $this->assertResponseCode(403);
        $this->switchUser(4);
        $this->setJsonRequest();
        $data = [
            'name' => 'WebActes 3',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [5, 6],
                ],
                [
                    'name' => 'Etape 3',
                    'users' => [4],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);
        $this->assertResponseCode(403);
    }

    public function testCreateDeploymentErrorNameNull()
    {
        $this->setJsonRequest();

        $data = [
            'name' => '',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [5, 6],
                ],
                [
                    'name' => 'Etape 3',
                    'users' => [4],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);

        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'name' => 'missing or empty value',
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testCreateDeploymentErrorNameStepsDouble()
    {
        $this->setJsonRequest();

        $data = [
            'name' => 'test',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [1, 5],
                ],
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);

        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'steps' => 'duplicates in steps names',
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testCreateDeploymentErrorStepsNotArray()
    {
        $this->setJsonRequest();

        $data = [
            'name' => 'WebActes 3',
            'steps' => 'toto',
        ];
        $this->post('/api/v1/circuits', $data);

        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'steps' => 'missing, empty or invalid value',
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testCreateDeploymentErrorStepsArrayEmpty()
    {
        $this->setJsonRequest();

        $data = [
            'name' => 'WebActes 3',
            'steps' => [],
        ];
        $this->post('/api/v1/circuits', $data);

        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'steps' => 'missing, empty or invalid value',
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testCreateDeploymentErrorStepsArrayToArrayNull()
    {
        $this->setJsonRequest();

        $data = [
            'name' => 'WebActes 3',
            'steps' => [
                [],
            ],
        ];
        $this->post('/api/v1/circuits', $data);

        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'steps' => 'missing name data for step #0',
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testCreateDeploymentErrorStepsArrayToArrayToValueKeyNull()
    {
        $this->setJsonRequest();

        $data = [
            'name' => 'WebActes 3',
            'steps' => [
                [
                    'name' => '',
                    'users' => [1],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);

        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'steps' => 'missing name data for step #0',
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testCreateDeploymentErrorStepsArrayToArrayToValueSecondKeyNull()
    {
        $this->setJsonRequest();

        $data = [
            'name' => 'WebActes 3',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [1],
                ],
                [
                    'name' => '',
                    'users' => [1],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);

        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'steps' => 'missing name data for step #1',
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testCreateDeploymentErrorUsersToStepsNull()
    {
        $this->setJsonRequest();

        $data = [
            'name' => 'WebActes 3',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);

        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'steps' => 'missing users data for step #0 : Etape 1',
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testCreateDeploymentErrorUsersNotExistant()
    {
        $this->setJsonRequest();

        $data = [
            'name' => 'WebActes 3',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [99999999],
                ],
            ],
        ];
        $this->post('/api/v1/circuits', $data);
        $this->assertResponseCode(400);

        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'steps' => 'invalid users data for step #0 : Etape 1',
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testSendAProjectToCircuitMakeHimNotEditable()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/projects/1/sendToCircuit/1');
        $this->assertResponseCode(201);

        $this->setJsonRequest();
        $this->get('/api/v1/circuits/1');
        $this->assertFalse($this->getResponse()['circuit']['is_editable']);
    }

    public function testEditCircuitSuccess()
    {
        $newData = [
            'name' => 'modified name',
            'description' => 'modified description',
            'steps' => [
                [
                    'name' => 'new etape 1',
                    'users' => [5],
                ],
                [
                    'name' => 'new etape 2',
                    'users' => [4],
                ],
            ],
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/circuits/4', $newData);
        $actual = $this->getResponse();

        $this->assertResponseCode(201);

        $expected = [
            'circuit' => [
                'id' => 4,
                'name' => 'modified name',
                'description' => 'modified description',
                'file' => null,
                'process_definition_key' => 'process_bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb-0000000003',
                'organization_id' => 1,
                'structure_id' => 1,
                'steps' => [
                    [
                        'name' => 'new etape 1',
                        'validators' => [
                            [
                                'id' => 5,
                                'firstname' => 'Romain',
                                'lastname' => 'Cyan',
                            ],
                        ],
                    ],
                    [
                        'name' => 'new etape 2',
                        'validators' => [
                            [
                                'id' => 4,
                                'firstname' => 'Rémi',
                                'lastname' => 'VIOLET',
                            ],
                        ],
                    ],
                ],
                'active' => true,
                'is_editable' => true,
                'is_deletable' => true,
            ],
        ];
        if (preg_match(static::FLOWABLE_FILE_REGEXP, $actual['circuit']['file']) === 1) {
            $expected['circuit']['file'] = $actual['circuit']['file'];
        }
        $this->assertEquals($expected, $actual);

        $this->setJsonRequest();
        $this->get('/api/v1/circuits/4');
        $this->assertResponseCode(200);
        $this->assertEquals($expected, $this->getResponse());
    }

    public function testEditInstancedCircuitFail()
    {
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $newData = [
            'name' => 'modified name',
            'description' => 'modified description',
            'steps' => [
                [
                    'name' => 'new etape 1',
                    'users' => [5],
                ],
                [
                    'name' => 'new etape 2',
                    'users' => [4],
                ],
            ],
        ];

        $this->put('/api/v1/circuits/1', $newData);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'id' => ['Cannot edit a workflow with instanced projects'],
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    /**
     * [testEditCircuitNotAdminFail description]
     *
     * @return void [type] [description]
     */
    public function testEditCircuitNotAdminFail()
    {
        $this->expectException(UnauthorizedException::class);
        $this->expectExceptionMessage('Only admin can edit a circuit');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();

        $this->switchUser(3);
        $newData = [
            'name' => 'modified name',
            'description' => 'modified description',
            'steps' => [
                [
                    'name' => 'new etape 1',
                    'users' => [5],
                ],
                [
                    'name' => 'new etape 2',
                    'users' => [4],
                ],
            ],
        ];
        $this->put('/api/v1/circuits/1', $newData);
        $this->assertResponseCode(403);
        $this->switchUser(4);
        $newData = [
            'name' => 'modified name',
            'description' => 'modified description',
            'steps' => [
                [
                    'name' => 'new etape 1',
                    'users' => [5],
                ],
                [
                    'name' => 'new etape 2',
                    'users' => [4],
                ],
            ],
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/circuits/1', $newData);
        $this->assertResponseCode(403);
    }

    public function testEditCircuitOtherStructureFail()
    {
        $this->markTestSkipped('is not available.');
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "workflows"');
        $this->disableErrorHandlerMiddleware();

        $this->switchUser(7);
        $newData = [
            'name' => 'modified name',
            'description' => 'modified description',
            'steps' => [
                [
                    'name' => 'new etape 1',
                    'users' => [5],
                ],
                [
                    'name' => 'new etape 2',
                    'users' => [4],
                ],
            ],
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/circuits/4', $newData);
        $this->assertResponseCode(404);
    }

    public function testEditMissingDataFails()
    {
        $newData = [
            'description' => 'modified description',
            'steps' => [
                [
                    'name' => 'new etape 1',
                    'users' => [5],
                ],
                [
                    'name' => 'new etape 2',
                    'users' => [4],
                ],
            ],
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/circuits/4', $newData);
        $this->assertResponseCode(400);
        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'name' => 'missing or empty value',
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testEditMissingStepData()
    {
        $newData = [
            'name' => 'modified name',
            'description' => 'modified description',
            'steps' => [
                [
                    'users' => [5],
                ],
                [
                    'name' => 'new etape 2',
                    'users' => [4],
                ],
            ],
        ];
        $this->setJsonRequest();
        $this->put('/api/v1/circuits/4', $newData);
        $this->assertResponseCode(400);
        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'steps' => 'missing name data for step #0',
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testCreateInstanceSetValidationPendingStateact()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/projects/1/sendToCircuit/4');

        $this->assertResponseCode(201);

        $body = $this->getResponse();
        $this->assertNotEmpty($body['instance']);

        $this->setJsonRequest();
        $this->get('/api/v1/circuits/4');
        $body = $this->getResponse();
        $this->assertEquals(false, $body['circuit']['is_editable']);
    }

    public function testCreateInstanceAndCheckIsNotDeletable()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/projects/1/sendToCircuit/4');

        $this->assertResponseCode(201);

        $body = $this->getResponse();
        $this->assertNotEmpty($body['instance']);

        $this->setJsonRequest();
        $this->get('/api/v1/circuits/4');
        $body = $this->getResponse();
        $this->assertEquals(false, $body['circuit']['is_editable']);
    }

    public function testCreateInstanceInvalidInput()
    {
        $this->expectException(MissingActionException::class);
        $this->expectExceptionMessage('Action ProjectsController::notANumber() could not be found, or is not accessible.');
        $this->disableErrorHandlerMiddleware();

        $projectId = 'notANumber';
        $workflowId = 1;

        $this->setJsonRequest();
        $this->post('/api/v1/projects/' . $projectId . '/sendToCircuit/' . $workflowId);

        $this->assertResponseCode(404);

        $projectId = 1;
        $workflowId = 'notANumber';

        $this->setJsonRequest();
        $this->post('/api/v1/projects/' . $projectId . '/sendToCircuit/' . $workflowId);

        $this->assertResponseCode(404);
    }

    public function testCreateInstanceCode404Workflow()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "workflows"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->post('/api/v1/projects/1/sendToCircuit/999999999');
        $this->assertResponseCode(404);
    }

    public function testCreateInstanceCode404Project()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "projects"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->post('/api/v1/projects/999999999/sendToCircuit/1');
        $this->assertResponseCode(404);
    }

    public function testCreateInstanceOtherStructureProject()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "projects"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->post('/api/v1/projects/36/sendToCircuit/1');
        $this->assertResponseCode(404);
    }

    public function testCreateInstanceOtherStructureWorkflow()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "workflows"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->post('/api/v1/projects/1/sendToCircuit/7');
        $this->assertResponseCode(404);
    }

    public function testDeleteOk()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "workflows"');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();
        $this->delete('/api/v1/circuits/4');
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/circuits/4');
        $this->assertResponseCode(404);
    }

    public function testDeleteNotDeletableFail()
    {
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->delete('/api/v1/circuits/1');
        $this->assertResponseCode(400);
    }

    /**
     * [testDeleteNotAdminFail description]
     *
     * @return void [type] [description]
     */
    public function testDeleteNotAdminFail()
    {
        $this->expectException(UnauthorizedException::class);
        $this->expectExceptionMessage('Only admin can delete a circuit');
        $this->disableErrorHandlerMiddleware();

        $this->setJsonRequest();

        $this->switchUser(3);
        $this->delete('/api/v1/circuits/1');
        $this->assertResponseCode(403);

        $this->switchUser(4);
        $this->delete('/api/v1/circuits/1');
        $this->assertResponseCode(403);
    }

    /**
     * [testEditCircuitDoNotChangeActive description]
     *
     * @return void [type] [description]
     */
    public function testEditCircuitDoNotChangeActive()
    {
        $this->setJsonRequest();

        $this->get('/api/v1/circuits/4');
        $this->assertTrue($this->getResponse()['circuit']['active']);

        $this->setJsonRequest();
        $data = [
            'name' => 'newCircuit',
            'steps' => [
                [
                    'name' => 'Etape 1',
                    'users' => [4],
                ],
                [
                    'name' => 'Etape 2',
                    'users' => [5],
                ],
                [
                    'name' => 'Etape 3',
                    'users' => [4],
                ],
            ],
        ];
        $this->put('/api/v1/circuits/4', $data);
        $this->assertResponseCode(201);

        $this->setJsonRequest();
        $this->get('/api/v1/circuits/4');
        $this->assertTrue($this->getResponse()['circuit']['active']);
    }

    protected function getCompletedRecords(): array
    {
        $r = [];
        foreach ($this->records as $k => $record) {
            $r[$k] = $record;
            $r[$k]['steps'] = $this->stepRecords[$k] ?? null;
        }

        return $r;
    }
}
