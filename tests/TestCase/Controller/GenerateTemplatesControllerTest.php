<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Hash;
use SplFileInfo;

/**
 * App\Controller\GenerateTemplatesController Test Case
 *
 * @uses \App\Controller\GenerateTemplatesController
 */
class GenerateTemplatesControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.GenerateTemplateTypes',
        'app.DraftTemplateTypes',
        'app.Actors',
        'app.GenerateTemplates',
        'app.DraftTemplates',
        'app.Typesittings',
        'app.Sittings',
        'app.Summons',
    ];

    private string $generateTemplateOdt;
    private string $generateTemplateOdtTMP;

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        Configure::write('KEYCLOAK_DEV_MODE', true);

        $this->generateTemplateOdt = ROOT . '/tests/Fixture/files/templates/template.odt';
        $this->generateTemplateOdtTMP = TMP . 'tests' . DS . 'template.odt';

        $this->assertTrue(copy($this->generateTemplateOdt, $this->generateTemplateOdtTMP));
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex(): void
    {
        $this->setJsonRequest();
        $this->get('/api/v1/generate-templates');
        $body = $this->getResponse();
        $this->assertResponseCode(200);

        $expected = [
            3 => 'Modèle d\'arrêté',
            4 => 'Modèle de convocation',
            2 => 'Modèle de délibération',
            6 => 'Modèle de liste des délibérations',
            5 => 'Modèle de note de synthèse',
            7 => 'Modèle de procès-verbal',
            9 => 'Modèle de projet',
            1 => 'Modèle de projet',
        ];
        $actual = Hash::combine($body, 'generateTemplates.{n}.id', 'generateTemplates.{n}.name');
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView(): void
    {
        $this->setJsonRequest();
        $this->get('/api/v1/generate-templates/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(1, count($body));
        $this->assertNotEmpty($body['generateTemplate']['id']);
    }

    /**
     * [testViewOtherStructureFail description]
     *
     * @return [type] [description]
     */
    public function testViewOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "generate_templates"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->get('/api/v1/generate-templates/8');
        $this->assertResponseCode(404);
    }

    /**
     * [testViewNotExistentFailed description]
     *
     * @return [type] [description]
     */
    public function testViewNotExistentFailed()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "generate_templates"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->get('/api/v1/generate-templates/1000');
        $this->assertResponseCode(404);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd(): void
    {
        $this->setJsonRequest();
        //Configure::write('App.uploadedFilesAsObjects', true);
        $this->assertTrue(copy($this->generateTemplateOdt, $this->generateTemplateOdtTMP));
        $fileTemplateOdt = new SplFileInfo($this->generateTemplateOdtTMP);

        $data = ['generateTemplate' => [
            'structure_id' => 1,
            'name' => 'Projet de Décision',
            'generate_template_type_id' => 1,
            'created' => 1549880898,
            'modified' => 1549880898,
        ],
        ];

        $this->post('/api/v1/generate-templates', json_encode($data));
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['generateTemplate']);
        $this->assertEquals(10, $body['generateTemplate']['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/generate-templates');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(9, count($body['generateTemplates']));
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit(): void
    {
        $data = ['generateTemplate' => [
            'structure_id' => 1,
            'name' => 'Projet de Décision 2',
            'generate_template_type_id' => 1,
            'created' => 1549880898,
            'modified' => 1549880898,
        ],
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/generate-templates/1', json_encode($data));
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['generateTemplate']['id']);
        $this->assertStringContainsString('Projet de Décision 2', $body['generateTemplate']['name']);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete(): void
    {
        $this->switchUser(1);

        $this->setJsonRequest();
        $this->delete('/api/v1/generate-templates/9');
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/generate-templates');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(7, count($body['generateTemplates']));
    }

    public function testIndexWithTypeCode(): void
    {
        $this->setJsonRequest();
        $this->get('/api/v1/generate-templates/with-type-code/project');
        $body = $this->getResponse();

        $this->assertResponseCode(200);

        $this->assertEquals('Modèle de projet', $body['generateTemplates'][0]['name']);

        $this->setJsonRequest();
        $this->get('/api/v1/generate-templates/with-type-code/act');
        $body = $this->getResponse();

        $this->assertResponseCode(200);

        $this->assertEquals('Modèle d\'arrêté', $body['generateTemplates'][0]['name']);
        $this->assertEquals('Modèle de délibération', $body['generateTemplates'][1]['name']);
    }
}
