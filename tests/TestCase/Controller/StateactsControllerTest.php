<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Utility\Hash;

/**
 * App\Controller\StateactsController Test Case
 */
class StateactsControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Authorizeactions',
        'app.Stateacts',
        'app.AuthorizeactionsStateacts',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/stateacts');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $actual = Hash::combine($body, 'stateacts.{n}.id', 'stateacts.{n}.name');
        $expected = [
            1 => 'Brouillon',
            2 => 'En cours de validation',
            3 => 'Validé',
            4 => 'Refusé',
            5 => 'Déclaré signé',
            6 => 'Signé par le i-Parapheur',
            7 => 'Refusé par le i-Parapheur',
            8 => 'En cours de signature i-Parapheur',
            9 => 'À télétransmettre',
            10 => 'En cours de télétransmission',
            11 => 'Acquitté',
            12 => 'Annulé',
            13 => 'En erreur sur le TdT',
            14 => 'En cours dépot tdt',
            15 => 'À ordonner',
            16 => 'Annulation en cours',
            17 => 'En cours d\'envoi i-parapheur',
            18 => 'Approuvé',
            19 => 'Rejeté',
            20 => 'Prendre acte',
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testIndex_with_query_action_add()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/stateacts?action=add');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $actual = Hash::combine($body, 'stateacts.{n}.id', 'stateacts.{n}.name');
        $expected = [
            1 => 'Brouillon',
            3 => 'Validé',
            5 => 'Déclaré signé',
            9 => 'À télétransmettre',
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testIndex_with_query_action_edit()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/stateacts?action=edit');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $actual = Hash::combine($body, 'stateacts.{n}.id', 'stateacts.{n}.name');
        $expected = [
            1 => 'Brouillon',
            3 => 'Validé',
            4 => 'Refusé',
            5 => 'Déclaré signé',
            7 => 'Refusé par le i-Parapheur',
            9 => 'À télétransmettre',
            11 => 'Acquitté',
            12 => 'Annulé',
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testIndex_with_query_action_connector()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/stateacts?action=connector');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $actual = Hash::combine($body, 'stateacts.{n}.id', 'stateacts.{n}.name');
        $expected = [
            6 => 'Signé par le i-Parapheur',
            7 => 'Refusé par le i-Parapheur',
            8 => 'En cours de signature i-Parapheur',
            9 => 'À télétransmettre',
            10 => 'En cours de télétransmission',
            11 => 'Acquitté',
            12 => 'Annulé',
            13 => 'En erreur sur le TdT',
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testIndex_with_query_action_wa()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/stateacts?action=wa');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $actual = Hash::combine($body, 'stateacts.{n}.id', 'stateacts.{n}.name');
        $expected = [
            2 => 'En cours de validation',
            3 => 'Validé',
            4 => 'Refusé',
            5 => 'Déclaré signé',
            6 => 'Signé par le i-Parapheur',
            7 => 'Refusé par le i-Parapheur',
            8 => 'En cours de signature i-Parapheur',
            9 => 'À télétransmettre',
            10 => 'En cours de télétransmission',
            11 => 'Acquitté',
            12 => 'Annulé',
            13 => 'En erreur sur le TdT',
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testIndex_with_query_action_coucou()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/stateacts?action=coucou');
        $this->assertResponseCode(400);
    }
}
