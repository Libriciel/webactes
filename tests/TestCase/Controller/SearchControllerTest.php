<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Utility\Hash;

/**
 * App\Controller\SearchController Test Case
 *
 * @uses \App\Controller\SearchController
 */
class SearchControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Container',
        'app.group.TypeAct',
        'app.group.File',
        'app.Stateacts',
        'app.Workflows',
        'app.Templates',
        'app.Themes',
        'app.GenerateTemplates',
        'app.Natures',
        'app.Actors',
        'app.Acts',
        'app.Projects',
        'app.Typesacts',
        'app.Typesittings',
        'app.Containers',
        'app.Sittings',
        'app.ContainersStateacts',
        'app.Summons',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * @return void
     */
    public function testSearchByProjectName()
    {
        $data = [
            'structure_id' => 1,
            'project_name' => 'brouillon',
        ];
        $this->setJsonRequest();
        $this->post('/api/v1/search', $data);

        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals([2, 1], Hash::extract($body, 'projectsList.{n}.id'));
        $this->assertEquals('Projet brouillon créé par 5', $body['projectsList'][0]['name']);
        $this->assertEquals('Projet brouillon créé par 4', $body['projectsList'][1]['name']);
    }

    /**
     * @return void
     */
    public function testSearchByTypeact()
    {
        $this->switchUser(7);
        $data = [
            'structure_id' => 2,
            'typeacts' => [['id' => 5]],
        ];
        $this->setJsonRequest();
        $this->post('/api/v1/search', $data);

        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $this->assertEquals([36], Hash::extract($body['projectsList'], '{n}.id'));
    }

    /**
     * @return void
     */
    public function testSuperAdminSearchByTypeact()
    {
        $this->switchUser(5);

        $data = [
            'structure_id' => 2,
            'typeacts' => [['id' => 5]],
        ];
        $this->setJsonRequest();
        $this->post('/api/v1/search', $data);

        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $this->assertEquals([36], Hash::extract($body['projectsList'], '{n}.id'));
    }

    /**
     * @return void
     */
    public function testSearchByTypeacts()
    {
        $data = [
            'structure_id' => 1,
            'typeacts' => [['id' => 4], ['id' => 5]],
        ];
        $this->setJsonRequest();
        $this->post('/api/v1/search', $data);
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        // retourne les 10 premiers
        $this->assertEquals([37, 33, 28, 26, 25, 24, 23, 22, 21, 20], Hash::extract($body['projectsList'], '{n}.id'));
    }

    /**
     * @dataProvider dataTestSearchByCodeact
     * @param $user
     * @param string $codeAct codeAct
     * @param array $ids ids
     * @return void
     */
    public function testSearchByCodeact($user, string $codeAct, array $ids)
    {
        $this->switchUser(is_int($user) ? $user : $this->getUserId($user));

        $data = [
            'structure_id' => 1,
            'code_act' => $codeAct,
        ];
        $this->setJsonRequest();
        $this->post('/api/v1/search', $data);

        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals($ids, Hash::extract($body, 'projectsList.{n}.id'));
    }

    /**
     * @return array[]
     */
    public function dataTestSearchByCodeact(): array
    {
        return [
            ['s.blanc', 'AcT_151', [1]],
            ['s.blanc', 'Ct_15', [1]],
            ['s.blanc', 'aCT_151,ACt_632', [2, 1]],
        ];
    }

    /**
     * @return void
     */
    public function testSearchByProjectNameAndTypeact()
    {
        $data = [
            'structure_id' => 1,
            'project_name' => 'télétransmettre',
            'typeact_name' => 'délibération',
        ];
        $this->setJsonRequest();
        $this->post('/api/v1/search', $data);
        $this->assertResponseCode(200);

        $body = $this->getResponse();
        $this->assertEquals(1, count($body['projectsList']));
        $this->assertEquals('Délibération', $body['projectsList'][0]['Typesacts']['name']);
    }
}
