<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Model\Entity\Auth;
use App\Model\Table\RolesTable;
use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Utility\Hash;

/**
 * App\Controller\UsersController Test Case
 */
class UsersControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Container',
        'app.group.Project',
        'app.Notifications',
        'app.Workflows',
        'app.NotificationsUsers',
        'app.Services',
        'app.Templates',
        'app.Themes',
        'app.GenerateTemplates',
        'app.Natures',
        'app.Actors',
        'app.Projects',
        'app.Typesacts',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/users');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * Test index method
     *
     * @dataProvider dataProviderIndex
     * @dataProvider dataProviderGetAllOrganizationUsers
     */
    public function testIndex($user, string $params, int $expectedCode, ?array $expectedIdsOrBody = null, ?array $expectedPagination = null)
    {
        $userId = is_int($user) ? $user : $this->getUserId($user);
        $this->switchUser($userId);
        $this->setJsonRequest();

        $this->get('/api/v1/users' . $params);
        $this->assertResponseCode($expectedCode);

        $body = $this->getResponse();
        if ($expectedCode === 200) {
            if ($expectedIdsOrBody !== null) {
                $this->assertEquals($expectedIdsOrBody, Hash::extract($body, 'users.{n}.id'));
            }
            if ($expectedPagination !== null) {
                $expectedPagination += ['page' => 1, 'perPage' => 10];
                $this->assertEquals($expectedPagination, $body['pagination']);
            }
        } else {
            $this->assertEquals($expectedIdsOrBody, $body);
        }
    }

    public function dataProviderIndex()
    {
        return [
            // Liste complète
            ['r.cyan', '', 200, [1, 5, 3, 2, 4], ['count' => 5]],
            // Par nom partiel
            ['s.blanc', '?name=LAN', 200, [1], ['count' => 1]],
            // Par prénom partiel
            ['s.blanc', '?name=BAST', 200, [1], ['count' => 1]],
            // Par identifiant partiel
            ['s.blanc', '?name=.BLAN', 200, [1], ['count' => 1]],
            // Par rôle partiel
            ['s.blanc', '?name=MINISTRAT', 200, [1, 5, 2], ['count' => 3]],
            // Par nom de structure partiel
            ['s.blanc', '?name=BRICIEL', 200, [1, 5, 3, 2, 4], ['count' => 5]],
            [
                's.blanc',
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "direction", "limit", "name", "offset", "page", "sort"',
                        ],
                    ],
                ],
            ],
        ];
    }

    public function dataProviderGetAllOrganizationUsers()
    {
        return [
            // Liste complète
            ['r.cyan', '/getAllOrganizationUsers', 200, [1, 5, 7, 11, 10, 3, 9, 12, 2, 4], ['count' => 12]],
            // Par nom partiel
            ['r.cyan', '/getAllOrganizationUsers?name=LAN', 200, [1], ['count' => 1]],
            // Par prénom partiel
            ['r.cyan', '/getAllOrganizationUsers?name=BAST', 200, [1, 6], ['count' => 2]],
            // Par identifiant partiel
            ['r.cyan', '/getAllOrganizationUsers?name=.BLAN', 200, [1], ['count' => 1]],
            // Par rôle partiel
            ['r.cyan', '/getAllOrganizationUsers?name=MINISTRAT', 200, [1, 5, 7, 11, 10, 12, 2, 6], ['count' => 8]],
            // Par nom de structure partiel
            ['r.cyan', '/getAllOrganizationUsers?name=BRICIEL', 200, [1, 5, 3, 2, 4], ['count' => 5]],
            [
                'r.cyan',
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "direction", "limit", "name", "offset", "page", "sort"',
                        ],
                    ],
                ],
            ],
        ];
    }

    public function testGetAllActiveUsers()
    {
        $this->switchUser(1);
        $this->setJsonRequest();
        $this->get('/api/v1/users/getAllActiveUsers');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(5, count($body['users']));
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/users/1');
        $this->assertResponseCode(200);

        $body = $this->getResponse();
        $this->assertEquals(1, count($body));
        $this->assertNotEmpty($body['user']['id']);
        $this->assertNotEmpty($body['user']['organization_id']);

        $this->assertEquals(1, $body['user']['id']);
        $this->assertEquals(1, $body['user']['organization_id']);
        $this->assertEquals(true, $body['user']['active']);
        $this->assertEquals('M.', $body['user']['civility']);
        $this->assertEquals('BLANC', $body['user']['lastname']);
        $this->assertEquals('Sébastien', $body['user']['firstname']);
        $this->assertEquals('s.blanc', $body['user']['username']);
        $this->assertEquals('sebastien.blanc@test.fr', $body['user']['email']);
        $this->assertEquals('0467659645', $body['user']['phone']);
        $this->assertEquals('Sébastien BLANC', $body['user']['name']);
    }

    public function testViewFailed()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/users/100');
        $this->assertResponseCode(404);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddUserAdminRights()
    {
        $this->switchUser(1);
        $this->setJsonRequest();
        $data = [
            'organization_id' => 1,
            'structure_id' => 1,
            'civility' => 'Mme',
            'lastname' => 'jaune',
            'firstname' => 'émilie',
            'username' => 'e.jaune',
            'email' => 'e.jaune@example.fr',
            'keycloak_id' => 'eadf2626-e3d2-4b6f-bf9f-6d4c5c3a5949',
            'roles' => [
                0 => ['id' => 1, 'name' => RolesTable::ADMINISTRATOR],
            ],
        ];

        $this->post('/api/v1/users', $data);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['user']['id']);
        $userIdAdd = $body['user']['id'];

        $this->setJsonRequest();
        $this->get('/api/v1/users/' . $userIdAdd);
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['user']['notifications_users']);
    }

    public function testAddSuperAdmin()
    {
        $this->switchUser(5);
        $this->setJsonRequest();
        $data = [
            'organization_id' => 1,
            'structure_id' => 2,
            'civility' => 'M.',
            'lastname' => 'Super',
            'firstname' => 'Admin',
            'username' => 's.admin',
            'email' => 's.admin@example.fr',
            'keycloak_id' => 'eadf2626-e3d2-4b6f-bf9f-6d4c5c3a5949',
            'roles' => [
                0 => ['id' => 10, 'name' => RolesTable::SUPER_ADMINISTRATOR],
            ],
        ];

        $this->post('/api/v1/users', $data);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['user']['structure_id']);
        $userStructure = $body['user']['structure_id'];
        $this->assertEquals(2, $userStructure);
    }

    /**
     * Test add other structure as super admin
     */
    public function testAddOtherStructureAsSuperAdmin()
    {
        $this->switchUser(5);
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
            'civility' => 'Mme',
            'lastname' => 'Violette',
            'firstname' => 'Hélène',
            'username' => 'h.violette',
            'email' => 'h.violette@example.fr',
            'keycloak_id' => 'eadf2626-e3d2-4b6f-bf9f-6d4c5c3a5949',
            'roles' => [
                0 => ['id' => 6, 'name' => RolesTable::ADMINISTRATOR],
            ],
        ];

        $this->post('/api/v1/users', $data);
        $this->assertResponseCode(201);
    }

    /**
     * Test add other structure fails
     */
    public function testAddOtherStructureAdminFailed()
    {
        $this->expectException(UnauthorizedException::class);
        $this->expectExceptionMessage('Only an admin can add a user');
        $this->disableErrorHandlerMiddleware();

        $this->switchUser(3);
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
            'civility' => 'Mme',
            'lastname' => 'Violette',
            'firstname' => 'Hélène',
            'username' => 'h.violette',
            'email' => 'h.violette@example.fr',
            'keycloak_id' => 'eadf2626-e3d2-4b6f-bf9f-6d4c5c3a5949',
            'roles' => [
                0 => ['id' => 6, 'name' => RolesTable::ADMINISTRATOR],
            ],
        ];
        $this->post('/api/v1/users', $data);
    }

//    /**
//     * Test add method
//     *
//     * @return void
//     */
//    public function testAddFailedStructureIdMissing()
//    {
//        $this->setJsonRequest();
//        $data = [
//            'civility' => 'Mme',
//            'lastname' => 'jaune',
//            'firstname' => 'émilie',
//            'username' => 'e.jaune',
//            'email' => 'e.jaune@example.fr',
//            'keycloak_id' => 'eadf2626-e3d2-4b6f-bf9f-6d4c5c3a5949',
//            'roles' => [
//                0 => ['id' => 1, 'name' => RolesTable::ADMINISTRATOR]
//            ]
//        ];
//        $this->post('/api/v1/users', $data);
//        $this->assertResponseCode(400);
//    }


    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->skipIfNoKeyCloakConfig();

        $this->switchUser(1);

        $data = [
            'active' => false,
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/users/2', $data);
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $this->assertEquals(false, $body['user']['active']);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditSuperAdministratorAsAdministrator()
    {
        $this->skipIfNoKeyCloakConfig();

        $this->switchUser(1);

        $data = [
            'active' => false,
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/users/5', $data);
        $this->assertResponseCode(401);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testDeleteSuperAdministratorAsAdministrator()
    {
        $this->skipIfNoKeyCloakConfig();

        $this->switchUser(1);

        $this->setJsonRequest();
        $this->delete('/api/v1/users/5');
        $this->assertResponseCode(401);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testDeleteAdministratorAsSuperAdministrator()
    {
        $this->skipIfNoKeyCloakConfig();

        $this->switchUser(5);

        $this->setJsonRequest();
        $this->delete('/api/v1/users/1');
        $this->assertResponseCode(204);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditWrongEmailFails()
    {
        $this->skipIfNoKeyCloakConfig();

        $this->switchUser(5);

        $data = [
            'id' => 3,
            'email' => 'emailuser_è//(@mail.fr',
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/users/3', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testAdminDeactivateHimself()
    {
        $this->skipIfNoKeyCloakConfig();

        $this->switchUser(1);

        $data = [
            'active' => false,
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/users/1', $data);
        $this->assertResponseCode(403);
        $body = $this->getResponse();
        $this->assertMatchesRegularExpression(
            '/impossible de modifier l\'utilisateur dans keycloak : .* \'success\' => false,/',
            $body['message']
        );
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit_Failed_phone()
    {
        $this->skipIfNoKeyCloakConfig();

        $this->switchUser(1);
        $data = [
            'organization_id' => 1,
            'active' => 0,
            'civility' => 'Mme',
            'lastname' => 'cvbc',
            'firstname' => 'cvb',
            'username' => 'cvb',
            'email' => 'cvb@example.fr',
            'phone' => '0203060',
        ];

        $this->setJsonRequest();
        $this->put('/api/v1/users/2', $data);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $this->assertEquals("Le numéro de téléphone n'est pas valide", $body['errors']['phone']['checkPhone']);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testSoftDelete()
    {
        $this->skipIfNoKeyCloakConfig();

        $this->switchUser(1);

        $this->setJsonRequest();
        $this->delete('/api/v1/users/2');
        $this->assertResponseCode(204);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDeleteFailed()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "users"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->delete('/api/v1/users/100');
        $this->assertResponseCode(404);
    }

    public function testGetKcUsers()
    {
        $this->skipIfNoKeyCloakConfig();

        //generate token
        $auth = new Auth();
        $token = $auth->getToken(KEYCLOAK_TEST_USER, KEYCLOAK_TEST_PASSWORD);
        $this->configRequest(
            ['headers' => [
                'Accept' => 'application/json',
                'Authorization' => $token,
            ]]
        );

        $this->get('/api/v1/users/getKcUsers');
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $this->assertNotNull($response);
        $this->assertNotEmpty($response);
    }

    public function testGetName()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/users/getInfo');
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $expected = [
            'userName' => 'Sébastien BLANC',
            'structureName' => 'Libriciel SCOP',
        ];
        $this->assertEquals($expected, $response);
    }

    public function testSetRoleChangeRole()
    {
        $this->setJsonRequest();
        $data = ['roleId' => 1];

        $this->patch('/api/v1/users/setRole', $data);
        $this->assertResponseCode(200);

        $RolesUsers = $this->fetchTable('RolesUsers');
        $res = $RolesUsers->find()->toArray();
        $this->assertEquals(12, count($res));
        $this->assertEquals(2, $res[0]['id']);
    }

    public function testSetRoleMissingRoleId()
    {
        $this->setJsonRequest();
        $data = ['noRoleId' => 1];
        $this->patch('/api/v1/users/setRole', $data);
        $response = $this->getResponse();
        $this->assertResponseCode(400);
        $this->assertNotEmpty($response['error']);
    }
}
