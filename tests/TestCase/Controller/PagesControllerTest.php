<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     1.2.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Test\TestCase\Controller;

use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestCase;

/**
 * PagesControllerTest class
 */
class PagesControllerTest extends IntegrationTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * testMultipleGet method
     *
     * @return void
     */
    public function testMultipleGet()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/api/v1/');
        $this->assertResponseOk();
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/api/v1/');
        $this->assertResponseOk();
    }

    /**
     * testDisplay method
     *
     * @return void
     */
    /*    public function testDisplay()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/pages/home');
        $this->assertResponseOk();
        $this->assertResponseContains('Bienvenue sur webACTES');
        $this->assertEquals("application/json", $this->_response->getType());
    }*/

    /**
     * Test that missing template renders 404 page in production
     *
     * @return void
     */
    /*    public function testMissingTemplate()
    {
        Configure::write('debug', false);
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/pages/not_existing');
        $this->assertResponseError();
        $this->assertResponseContains('Not Found');
    }*/

    /**
     * Test that missing template in debug mode renders missing_template error page
     *
     * @return void
     */
    /*    public function testMissingTemplateInDebug()
    {
        Configure::write('debug', true);
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/pages/not_existing');
        $this->assertResponseContains('Template file');
        $this->assertResponseContains('is missing');
        $this->assertResponseContains('not_existing.ctp');
    }*/

    /**
     * Test directory traversal protection
     *
     * @return void
     */
    /*    public function testDirectoryTraversalProtection()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/pages/../Layout/ajax');
        $this->assertResponseCode(403);
        $this->assertResponseContains('Forbidden');
    }*/
}
