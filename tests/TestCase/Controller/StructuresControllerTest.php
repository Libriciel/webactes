<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Model\Table\RolesTable;
use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Laminas\Diactoros\UploadedFile;

/**
 * App\Controller\StructuresController Test Case
 */
class StructuresControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.TypeAct',
        'app.group.Container',
        'app.Stateacts',
        'app.ConnectorTypes',
        'app.Workflows',
        'app.Classifications',
        'app.Matieres',
        'app.Connecteurs',
        'app.Natures',
        'app.Typespiecesjointes',
        'app.Pastellfluxtypes',
        'app.Templates',
        'app.Themes',
        'app.GenerateTemplates',
        'app.Actors',
        'app.Acts',
        'app.Projects',
        'app.Typesacts',
        'app.Containers',
        'app.ContainersStateacts',
        'app.Maindocuments',
        'app.Annexes',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->switchUser(5);
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/structures');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 401], // Administrateur
            ['m.vert', 401], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataProviderTestIndex
     */
    public function testIndex(string $params, int $status, array $expected): void
    {
        $this->switchUser($this->getUserId('r.cyan'));
        $this->setJsonRequest();

        $this->get('/api/v1/structures' . $params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    public function dataProviderTestIndex(): array
    {
        $structures = [
            [
                'id' => 1,
                'roles' => [
                    ['id' => 1],
                    ['id' => 2],
                    ['id' => 4],
                    ['id' => 3],
                    ['id' => 5],
                ],
            ],
            [
                'id' => 2,
                'roles' => [
                    ['id' => 6],
                    ['id' => 7],
                    ['id' => 9],
                    ['id' => 8],
                    ['id' => 10],
                ],
            ],
        ];

        return [
            ['', 200, ['structures' => $structures, 'pagination' => ['count' => 2]]],
            ['?paginate=false', 200, ['structures' => $structures, 'pagination' => null]],
            ['?name=foo', 200, ['pagination' => ['count' => 0]]],
            ['?name=libriciel', 200, ['pagination' => ['count' => 1]]],
            ['?name=ternum', 200, ['pagination' => ['count' => 1]]],
            [
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "direction", "limit", "name", "offset", "page", "paginate", "sort"',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Test view method with success
     *
     * @return void
     */
    public function testViewSuccess(): void
    {
        $this->setJsonRequest();
        $this->get('/api/v1/structures/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $this->assertEquals(1, $body['structure']['id']);
        $this->assertEquals(1, $body['structure']['organization_id']);
        $this->assertEquals('Libriciel SCOP', $body['structure']['business_name']);
        $this->assertNotEmpty($body['structure']['roles']);
    }

    public function testViewFailedRecordNotFound(): void
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "structures"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/structures/666');
        $this->assertResponseCode(404);
    }

    // TO DO

    public function testAddStructuresWithOrchestrationOne(): void
    {
        $this->setJsonRequest();
        $this->put('/api/v1/structures/withStructures', [9]);

        $this->assertResponseCode(201);
        $this->assertNull($this->getResponse());

        $this->setJsonRequest();
        $newId = 1;
        $this->session(['structure_id' => $newId]);
        $this->get('/api/v1/structures/' . $newId);

        $this->assertResponseCode(200);

        $actual = $this->getResponse();

        $this->assertCount(5, $actual['structure']['roles']);
    }

    // TO DO

    public function testAddStructuresWithOrchestrationMany(): void
    {
        $this->setJsonRequest();
        $data = [
            9,
            13,
        ];
        $this->put('/api/v1/structures/withStructures', $data);

        $this->assertResponseCode(201);
        $this->assertNull($this->getResponse());

        $this->setJsonRequest();
        $newId1 = 1;
        $this->session(['structure_id' => $newId1]);
        $this->get('/api/v1/structures/' . $newId1);
        $this->assertResponseCode(200);
        $actual = $this->getResponse();

        $this->assertCount(5, $actual['structure']['roles']);

        $this->setJsonRequest();
        $newId2 = 2;
        $this->session(['structure_id' => $newId2]);
        $this->get('/api/v1/structures/' . $newId2);
        $this->assertResponseCode(200);
        $actual = $this->getResponse();

        $this->assertCount(5, $actual['structure']['roles']);
        $this->assertEquals($newId2, $actual['structure']['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/organizations');
        $this->assertResponseCode(200);
        $organizations = $this->getResponse();
        $this->assertCount(3, $organizations['organizations']);
    }

    /**
     * Test delete method with an inexistant record.
     *
     * @return void
     */
    public function testDeleteFailedRecordNotFound(): void
    {
        $this->switchUser(1);
        $this->setJsonRequest();
        $this->delete('/api/v1/structures/666');
        $this->assertResponseCode(401);
    }

    public function testUpdateClassification(): void
    {
        $this->switchUser(5);

        $Classifications = $this->fetchTable('Classifications');
        $initialeDateClassification = $Classifications->find()
            ->select(['dateclassification'])
            ->where(['structure_id' => 1])
            ->toArray();

        $Matieres = $this->fetchTable('Matieres');

        $initialeMatieres = $Matieres->find()->where()->toArray();
        $this->assertCount(3, $initialeMatieres);

        $this->setJsonRequest();
        $this->get('/api/v1/structures/updateClassification');
        $this->assertResponseCode(200);

        $configNaturesTypeAbregeAccepted = Configure::read('naturesTypeAbregeAccepted');
        $this->assertCount(5, $configNaturesTypeAbregeAccepted);

        $success = true;

        $Natures = $this->fetchTable('Natures');
        foreach ($configNaturesTypeAbregeAccepted as $conf) {
            $natures = $Natures->find()
                ->where(['typeabrege' => $conf])
                ->toArray();

            if (empty($natures)) {
                $success = false;
                break;
            }
        }
        $this->assertTrue($success);

        $matieres = $Matieres->find()->toArray();
        $this->assertNotCount(count($initialeMatieres), $matieres);

        $dateClassification = $Classifications->find()
            ->select(['dateclassification'])
            ->where(['structure_id' => 1])
            ->toArray();
        $this->assertNotEquals($initialeDateClassification, $dateClassification);
        $Typespiecesjointes = $this->fetchTable('Typespiecesjointes');
        $typespiecesjointes = $Typespiecesjointes->find()
            ->where(['structure_id' => 1])
            ->toArray();
        $this->assertCount(95, $typespiecesjointes);
    }

    /**
     * @dataProvider dataAccessUpdateClassification
     */
    public function testAccessUpdateClassification(string $username, int $expected): void
    {
        $this->switchUser($this->getUserId($username));
        $this->setJsonRequest();
        $this->get('/api/v1/structures/updateClassification');
        $this->assertResponseCode($expected);
    }

    public function dataAccessUpdateClassification(): array
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 200], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    public function testGetTypesPiecesJointesFailedNatureID(): void
    {
        $expected = [
            'typespiecesjointes' => [],
        ];

        $nature_id = 9999;

        $this->setJsonRequest();
        $this->get('/api/v1/structures/updateClassification');
        $this->assertResponseCode(200);

        $this->setJsonRequest();
        $this->get('/api/v1/structures/getTypesPiecesJointes/' . $nature_id);
        $this->assertResponseCode(200);

        $body = $this->getResponse();
        $this->assertEquals($expected, $body);
    }

    public function testFailedGetClassificationErrorConfigurationEmpty(): void
    {
        // NATURES
        Configure::write('naturesTypeAbregeAccepted', []);

        $this->setJsonRequest();
        $this->get('/api/v1/structures/updateClassification');
        $this->assertResponseCode(400);
    }

    public function testFailedPostClassification(): void
    {
        $this->setJsonRequest();
        $this->post('/api/v1/structures/updateClassification');
        $this->assertResponseCode(404);
    }

    public function testSetLogo(): void
    {
        $file = new UploadedFile(
            $this->createTempFile(static::LOGO_TEMPLATE_PATH, 'logo.png'),
            static::LOGO_TEMPLATE_SIZE,
            0,
            'logo.png',
            'image/png'
        );

        $this->setJsonRequest();
        $this->post('/api/v1/structures/setLogo', ['logo' => $file]);
        $this->assertResponseCode(200);
    }

    public function testSetLogoMissingFile(): void
    {
        $data = [];

        $this->setJsonRequest();
        $this->post('/api/v1/structures/setLogo', $data);
        $this->assertResponseCode(400);
    }

    public function testSetLogoBadMime(): void
    {
        $file = new UploadedFile(
            $this->createTempFile(static::PDF_TEMPLATE_PATH, 'false.pdf'),
            static::LOGO_TEMPLATE_SIZE,
            0,
            'false.pdf',
            'application/pdf'
        );

        $this->setJsonRequest();
        $this->post('/api/v1/structures/setLogo', ['logo' => $file]);
        $this->assertResponseCode(400);
    }

    /**
     * @dataProvider dataAccessSetLogo
     */
    public function testAccessSetLogo(string $username, int $expected): void // @fixme: testAccessSetLogo
    {
        $this->switchUser($this->getUserId($username));

        $file = new UploadedFile(
            $this->createTempFile(static::LOGO_TEMPLATE_PATH, 'logo.png'),
            static::LOGO_TEMPLATE_SIZE,
            0,
            'logo.png',
            'image/png'
        );

        $this->setJsonRequest();
        $this->post('/api/v1/structures/setLogo', ['logo' => $file]);
        $this->assertResponseCode($expected);
    }

    public function dataAccessSetLogo(): array
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    public function testDeleteLogo(): void
    {
        $file = new UploadedFile(
            $this->createTempFile(static::LOGO_TEMPLATE_PATH, 'logo.png'),
            static::LOGO_TEMPLATE_SIZE,
            0,
            'logo.png',
            'image/png'
        );

        $this->setJsonRequest();
        $this->post('/api/v1/structures/setLogo', ['logo' => $file]);
        $this->assertResponseCode(200);

        $this->setJsonRequest();
        $this->delete('/api/v1/structures/deleteLogo');
        $expected = ['success' => 'logo deleted'];
        $actual = $this->getResponse();
        $this->assertEquals($expected, $actual);
    }

    public function testSetCustomName(): void
    {
        $data = ['customname' => 'myStructure'];
        $this->setJsonRequest();
        $this->post('/api/v1/structures/setCustomName', $data);
        $this->assertResponseCode(200);
        $this->setJsonRequest();
        $this->get('/api/v1/structures/getCustomName');
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $this->assertEquals('myStructure', $response['customname']);
    }

    /**
     * @dataProvider dataAccessSetCustomName
     */
    public function testAccessSetCustomName(string $username, int $expected): void
    {
        $this->switchUser($this->getUserId($username));
        $data = ['customname' => 'myStructure'];
        $this->setJsonRequest();
        $this->post('/api/v1/structures/setCustomName', $data);
        $this->assertResponseCode($expected);
    }

    public function dataAccessSetCustomName(): array
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    public function testSetSpinner(): void
    {
        $data = ['spinner' => 'feu'];
        $this->setJsonRequest();
        $this->post('/api/v1/structures/setSpinner', $data);
        $this->assertResponseCode(200);
    }

    /**
     * @dataProvider dataAccessSetSpinner
     */
    public function testAccessSetSpinner(string $username, int $expected): void
    {
        $this->switchUser($this->getUserId($username));
        $data = ['spinner' => 'feu'];
        $this->setJsonRequest();
        $this->post('/api/v1/structures/setSpinner', $data);
        $this->assertResponseCode($expected);
    }

    public function dataAccessSetSpinner(): array
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    public function testGetStructureInfo(): void
    {
        $this->switchUser(1);

        $this->setJsonRequest();
        $this->get('/api/v1/structures/getStructureInfo');
        $this->assertResponseCode(200);
        $response = $this->getResponse();

        $this->assertEquals(1, $response['structure']['id']);
        $this->assertEquals(1, $response['structure']['organization_id']);
        $this->assertCount(5, $response['structure']['roles']);
        $this->assertEquals('Libriciel SCOP', $response['structure']['name']);
        $this->assertEquals('Sébastien', $response['user']['firstname']);
        $this->assertEquals('BLANC', $response['user']['lastname']);
        $this->assertEquals(1, $response['user']['role']['id']);
        $this->assertEquals(RolesTable::ADMINISTRATOR, $response['user']['role']['name']);
        $this->assertCount(5, $response['roles']);
        $this->assertNotEmpty($response['roles']);
        unset($response['structureSetting']);
    }

    public function testAddStructuresWithOrchestrationFailedChildNotExist(): void
    {
        $this->setJsonRequest();
        $data = [
            1 => [
                2,
                3,
            ],
        ];
        $this->put('/api/v1/structures/withStructures', $data);
        $this->assertResponseCode(400);
    }

    public function testAddStructuresWithOrchestrationFailedArrayNotFoundChildNotExist(): void
    {
        $this->setJsonRequest();
        $data = [
            1 => [
                2 => [
                    3,
                ],
            ],
        ];
        $this->put('/api/v1/structures/withStructures', $data);
        $this->assertResponseCode(400);
    }

    public function testAddStructuresWithOrchestrationFailedOneBadId(): void
    {
        $this->setJsonRequest();
        $data = [
            999,
        ];
        $this->put('/api/v1/structures/withStructures', $data);
        $this->assertResponseCode(400);
    }

    public function testAddStructuresWithOrchestrationFailedManyBadIds(): void
    {
        $this->setJsonRequest();
        $data = [
            10,
            999, //bad ID
            18,
            666, // bad ID
        ];
        $this->put('/api/v1/structures/withStructures', $data);
        $this->assertResponseCode(400);
    }

    public function testGetLastClassificationDate(): void
    {
        $this->setJsonRequest();
        $this->get('/api/v1/structures/getLastClassificationDate');
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $expected = [
            'dateclassification' => '2019-05-28',
        ];
        $this->assertEquals($expected, $response);
    }

    public function testSavePastellConnexion(): void
    {
        $this->setJsonRequest();
        $data = [
            'username' => 'foo',
            'password' => 'bar',
            'pastellUrl' => 'http://localhost',
            'pastellEntityId' => '42',
            'slowUrl' => 'http://s2low.local',

        ];
        $this->post('/api/v1/structures/savePastellConnexion', $data);
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $this->assertEquals(['success' => true], $response);
    }

    public function testSavePastellConnexionWhenNotPasswordProvided(): void
    {
        $table = $this->fetchTable('Connecteurs');
        $result = $table->find()->where(['structure_id' => 1])->first();
        $originalPassword = $result['password'];
        self::assertNotEmpty($originalPassword);
        $this->setJsonRequest();
        $data = [
            'username' => 'foo',
            'pastellUrl' => 'http://localhost',
            'pastellEntityId' => '42',
            'slowUrl' => 'http://s2low.local',

        ];
        $this->post('/api/v1/structures/savePastellConnexion', $data);
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $this->assertEquals(['success' => true], $response);
        $table = $this->fetchTable('Connecteurs');
        $result = $table->find()->where(['structure_id' => 1])->first();
        $this->assertEquals($originalPassword, $result->password);
    }

    /**
     * @dataProvider dataAccessSavePastellConnexion
     */
    public function testAccessSavePastellConnexion(string $username, int $expected): void
    {
        $this->switchUser($this->getUserId($username));
        $this->setJsonRequest();
        $data = [
            'username' => 'foo',
            'password' => 'bar',
            'pastellUrl' => 'http://localhost',
            'pastellEntityId' => '42',
            'slowUrl' => 'http://s2low.local',

        ];
        $this->post('/api/v1/structures/savePastellConnexion', $data);
        $this->assertResponseCode($expected);
    }

    public function dataAccessSavePastellConnexion(): array
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général - Ça devrait être 403, mais bon...
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataAccessGetPastellConnexion
     */
    public function testAccessGetPastellConnexion(string $username, int $expected): void
    {
        $this->switchUser($this->getUserId($username));
        $this->setJsonRequest();
        $this->get('/api/v1/structures/getPastellConnexion');
        $this->assertResponseCode($expected);
    }

    public function dataAccessGetPastellConnexion(): array
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 401], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    public function testGetPastellConnexion(): void
    {
        $this->setJsonRequest();
        $this->get('/api/v1/structures/getPastellConnexion');
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $this->assertEquals(['pastell' => [
            'username' => 'webactes',
            'password' => '',
            'pastellUrl' => 'https://pastell.partenaire.libriciel.fr',
            'pastellEntityId' => 9,
            'slowUrl' => 'https://s2low.formations.libriciel.fr',
        ],
        ], $response);
    }

    public function testCheckPastellConnexion(): void
    {
        $this->setJsonRequest();
        $this->post(
            '/api/v1/structures/checkPastellConnexion',
            ['username' => 'bbb', 'pastellUrl' => 'http://test.test', 'pastellEntityId' => 42]
        );
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $this->assertEquals(
            ['success' => false, 'error' => 'cURL Error (6) Could not resolve host: test.test'],
            $response
        );
    }

    public function testSaveIdelibreConnexion(): void
    {
        $this->setJsonRequest();
        $data = [
            'username' => 'foo',
            'password' => 'bar',
            'connexion_option' => 'baz',
            'url' => 'http://test.test',
        ];
        $this->post('/api/v1/structures/saveIdelibreConnexion', $data);
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $this->assertEquals(['success' => true], $response);
    }

    public function testSaveIdelibreConnexionWhenNotPasswordProvided(): void
    {
        $this->testSaveIdelibreConnexion();
        $this->setJsonRequest();
        $data = [
            'username' => 'foo',
            'connexion_option' => 'baz',
            'url' => 'http://test.test',
        ];
        $this->post('/api/v1/structures/saveIdelibreConnexion', $data);
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $this->assertEquals(['success' => true], $response);
        $table = $this->fetchTable('Connecteurs');
        $find = $table->find()->where(['connexion_option' => 'baz']);
        self::assertCount(1, $find);
        $result = $find->first();
        $this->assertEquals('bar', $result->password);
    }

    public function testGetIdelibreConnexion(): void
    {
        $this->testSaveIdelibreConnexion();
        $this->setJsonRequest();
        $this->get('/api/v1/structures/getIdelibreConnexion');
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $this->assertEquals(['idelibre' => [
            'username' => 'foo',
            'url' => 'http://test.test',
            'connexion_option' => 'baz',
        ],
        ], $response);
    }

    public function testCheckIdelibreConnexion(): void
    {
        $this->testSaveIdelibreConnexion();
        $this->setJsonRequest();
        $this->post(
            '/api/v1/structures/checkIdelibreConnexion',
            ['username' => 'bbb', 'url' => 'http://test.test', 'connexion_option' => 'baz']
        );
        $this->assertResponseCode(200);
        $response = $this->getResponse();
        $this->assertEquals(
            ['success' => false, 'error' => 'cURL Error (6) Could not resolve host: test.test'],
            $response
        );
    }
}
