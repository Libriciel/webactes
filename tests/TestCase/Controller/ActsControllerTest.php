<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;

/**
 * App\Controller\ActsController Test Case
 */
class ActsControllerTest extends AbstractTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Act',
        'app.Projects',
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/acts');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(5, count($body['acts']));
    }

    public function testcheckCode()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/checkCodeAct/NEW_CODE');
        $body = $this->getResponse();
        $this->assertResponseCode(200);
        $this->assertTrue($body['valid']);
    }

    /**
     * existing act code in act
     */
    public function testcheckCodeFailed()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/checkCodeAct/ACT_5');
        $body = $this->getResponse();
        $this->assertResponseCode(200);
        $this->assertFalse($body['valid']);
    }

    /**
     * existing act code in project
     */
    public function testcheckCodeProjectFailed()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/checkCodeAct/ACT_151');
        $body = $this->getResponse();
        $this->assertResponseCode(200);
        $this->assertFalse($body['valid']);
    }
}
