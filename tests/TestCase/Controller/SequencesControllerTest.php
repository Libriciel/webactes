<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Hash;

/**
 * App\Controller\SequencesController Test Case
 */
class SequencesControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Sequences',
        'app.Counters',
    ];

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/sequences');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $actual = Hash::extract($body, 'sequences.{n}.name');
        $this->assertEquals(
            ['Séq_arr_individuel',
                'Séq_arr_réglementaire',
                'Séq_conventions',
                'Seq_delib',
                'Séq_test réinit année',
                'Séq_test réinit jour',
                'Séq_test réinit mois',
                'Séquence_contrats'],
            $actual
        );
    }

    /**
     * Test availableSequences method
     *
     * @return void
     */
    public function testAvailableSequences()
    {
        $this->switchUser(5);

        $this->setJsonRequest();
        $this->get('/api/v1/sequences/availableSequences');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(1, count($body['sequences']));
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/sequences/1');
        $this->assertResponseCode(200);

        $body = $this->getResponse();
        $this->assertEquals('Seq_delib', $body['sequence']['name']);
    }

    /**
     * Test view other structure fails
     *
     * @return [type] [description]
     */
    public function testViewOtherStructureFails()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "sequences"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/sequences/9');
        $this->assertResponseCode(404);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'name' => 'Nouvelle séquence',
            'comment' => 'Un nouveau test de création',
            'sequence_num' => 1,
            'created' => 1607597401,
            'modified' => 1607597401,
            'starting_date' => '2020-12-10',
            'expiry_date' => '2021-12-10',
        ];

        $this->post('/api/v1/sequences', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['sequence']['id']);
        $id = $body['sequence']['id'];

        $this->setJsonRequest();
        $this->get('api/v1/sequences/' . $id);
        $body = $this->getResponse();
        $this->assertEquals(10, $body['sequence']['id']);
    }

    /**
     * Test add method fails
     *
     * @return void
     */
    public function testAddFails()
    {
        $data = [
            'structure_id' => 1,
            'name' => 'Nouvelle séquence',
            'comment' => 'Un nouveau test de création',
            'sequence_num' => 'Quatorze',
            'starting_date' => '2020-12-10',
            'expiry_date' => '2021-12-10',
        ];
        $this->setJsonRequest();
        $this->post('/api/v1/sequences', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test add method fails
     *
     * @return void
     */
    public function testAddNotAdminFails()
    {
        $this->switchUser(3);
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'name' => 'Nouvelle séquence',
            'comment' => 'Un nouveau test de création',
            'sequence_num' => 1,
            'starting_date' => '2020-12-10',
            'expiry_date' => '2021-12-10',
        ];
        $this->post('/api/v1/sequences', $data);
        $this->assertResponseCode(403);
    }

    /**
     * Test add other structure fails
     *
     * @return void
     */
    public function testAddOtherStructureFails()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
            'name' => 'contrats',
            'comment' => 'Une séquence réservée à tous types de contrats',
            'sequence_num' => 1,
            'created' => 1607597401,
            'modified' => 1607597401,
            'starting_date' => '2020-12-14',
            'expiry_date' => '2021-12-14',
        ];
        $this->post('/api/v1/sequences', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test put edit method
     *
     * @return void
     */
    public function testPutEdit()
    {
        $this->setJsonRequest();
        $data = [
            'comment' => 'Un autre commentaire',
        ];
        $this->put('/api/v1/sequences/1', $data);
        $this->assertResponseCode(200);

        $this->setJsonRequest();
        $this->get('/api/v1/sequences/1');
        $body = $this->getResponse();
        $this->assertEquals(1, $body['sequence']['id']);
        $this->assertEquals('Un autre commentaire', $body['sequence']['comment']);
        $this->assertEquals('Seq_delib', $body['sequence']['name']);
    }

    /**
     * Test put method fails
     *
     * @return void
     */
    public function testPutEditNotAdminFails()
    {
        $this->switchUser(4);
        $this->setJsonRequest();
        $data = [
            'name' => 'Super_séquence',
        ];

        $this->put('/api/v1/sequences/1', $data);
        $this->assertResponseCode(403);
    }

    /**
     * Test patch method
     *
     * @return void
     */
    public function testPatchEdit()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'Changement de nom',
        ];
        $this->patch('api/v1/sequences/3', $data);
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/sequences/3');
        $body = $this->getResponse();
        $this->assertNotEmpty($body['sequence']['id']);
        $this->assertNotEmpty($body['sequence']['structure_id']);
        $this->assertNotEmpty($body['sequence']['name']);
        $this->assertNotEmpty($body['sequence']['comment']);
        $this->assertNotEmpty($body['sequence']['sequence_num']);
        $this->assertNotEmpty($body['sequence']['created']);
        $this->assertNotEmpty($body['sequence']['modified']);
        $this->assertNotEmpty($body['sequence']['starting_date']);
        $this->assertNotEmpty($body['sequence']['expiry_date']);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/sequences/5');
        $this->assertResponseCode(204);
    }

    /**
     * Test delete method fails
     *
     * @return void
     */
    public function testDeleteFails()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/sequences/2');
        $this->assertResponseCode(400);
    }
}
