<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Hash;

/**
 * App\Controller\ActorsController Test Case
 */
class ActorsControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Actor',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Tests d'accès à l'URL /api/v1/actors
     *
     * @dataProvider dataAccessIndex
     * @param $userId
     * @param $responseCode
     * @param array $actors
     * @throws \Exception
     */
    public function testAccessIndex($username, $responseCode, array $actors): void
    {
        $this->switchUser($this->getUserId($username));

        $this->setJsonRequest();
        $this->get('/api/v1/actors');
        $body = $this->getResponse();
        $this->assertResponseCode($responseCode);

        if ($responseCode === 200) {
            $this->assertEquals(count($actors), count($body['actors']));
            $actual = Hash::extract($body, 'actors.{n}.lastname');
            sort($actual);
            sort($actors);
            $this->assertEquals($actors, $actual);
        }
    }

    /**
     * Données pour les tests d'accès à l'URL /api/v1/actors
     *
     * @return array[]
     */
    public function dataAccessIndex()
    {
        return [
            ['s.blanc', 200, ['ROUGE', 'ROUGE3', 'ROUGE4']], // Administrateur
            ['m.vert', 200, ['ROUGE', 'ROUGE3', 'ROUGE4']], // Administrateur Fonctionnel
            ['j.orange', 200, ['ROUGE', 'ROUGE3', 'ROUGE4']], // Secrétariat général
            ['r.violet', 403, []], // Rédacteur / Valideur
            ['r.cyan', 200, ['ROUGE', 'ROUGE3', 'ROUGE4']], // Super Administrateur
        ];
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/actors/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(1, count($body));
        $this->assertNotEmpty($body['actor']['id']);
    }

    /**
     * [testViewOtherStructureFail description]
     *
     * @return [type] [description]
     */
    public function testViewOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "actors"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->get('/api/v1/actors/2');
        $this->assertResponseCode(404);
    }

    /**
     * [testViewNotExistentFailed description]
     *
     * @return [type] [description]
     */
    public function testViewNotExistentFailed()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "actors"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->get('/api/v1/actors/1000');
        $this->assertResponseCode(404);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->setJsonRequest();
        $data = ['actor' => [
            'structure_id' => 1,
            'active' => 1,
            'rank' => 4,
            'civility' => 'M',
            'lastname' => 'Ines',
            'firstname' => 'gfhfght',
            'birthday' => '2019-02-11',
            'email' => 'voicimonemail@example.fr',
            'title' => 'Lorem ipsum dsdfsdolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'address_supplement' => 'Lorem ipsum dolor sit amet',
            'post_code' => 'Lor',
            'city' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum d',
            'cellphone' => 'Lorem ipsum d',
            'created' => 1549880898,
            'modified' => 1549880898,
            'actor_groups' => [
                ['id' => 1],
                ['id' => 2],
            ],
        ]];
        $this->post('/api/v1/actors', json_encode($data));
        $this->assertResponseCode(201);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['actor']);
        $this->assertEquals(5, $body['actor']['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/actors');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(4, count($body['actors']));
    }

    /**
     * [testAddFailed description]
     *
     * @return [type] [description]
     */
    public function testAddFailed()
    {
        $this->setJsonRequest();

        $data = ['actor' => [
            'active' => 1,
            'rank' => 4,
            'civility' => 'M',
            'lastname' => 'Ines',
            'firstname' => '',
            'birthday' => '2019-02-11',
            'email' => 'voicimonemail@example.fr',
            'title' => 'Lorem ipsum dsdfsdolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'address_supplement' => 'Lorem ipsum dolor sit amet',
            'post_code' => 'Lor',
            'city' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum d',
            'cellphone' => 'Lorem ipsum d',
            'created' => 1549880898,
            'modified' => 1549880898,
        ]];
        $this->post('/api/v1/actors', json_encode($data));
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'firstname' => [
                    '_empty' => 'This field cannot be left empty',
                ],
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    /**
     * [testAddOtherStructureFail description]
     *
     * @return [type] [description]
     */
    public function testAddOtherStructureFail()
    {
        $this->setJsonRequest();
        $data = ['actor' => [
            'structure_id' => 2,
            'active' => 1,
            'rank' => 4,
            'civility' => 'M',
            'lastname' => 'Ines',
            'firstname' => 'gfhfght',
            'birthday' => '2019-02-11',
            'email' => 'voicimonemail@example.fr',
            'title' => 'Lorem ipsum dsdfsdolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'address_supplement' => 'Lorem ipsum dolor sit amet',
            'post_code' => 'Lor',
            'city' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum d',
            'cellphone' => 'Lorem ipsum d',
            'created' => 1549880898,
            'modified' => 1549880898,
        ]];
        $this->post('/api/v1/actors', json_encode($data));
        $this->assertResponseCode(400);

        $body = $this->getResponse();
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $this->assertEquals($expected, $body['errors']);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $data = ['actor' => [
            'structure_id' => 1,
            'active' => 0,
            'rank' => 4,
            'civility' => 'M',
            'lastname' => 'Ines',
            'firstname' => 'gfhfght',
            'birthday' => '2019-02-11',
            'email' => 'voicimonemail@example.fr',
            'title' => 'Lorem ipsum dsdfsdolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'address_supplement' => 'Lorem ipsum dolor sit amet',
            'post_code' => 'Lor',
            'city' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum d',
            'cellphone' => 'Lorem ipsum d',
            'created' => 1549880898,
            'modified' => 1549880898,
            'actor_groups' => [
                ['id' => 1],
                ['id' => 2],
            ],
        ]];

        $this->setJsonRequest();
        $this->put('/api/v1/actors/1', json_encode($data));
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['actor']['id']);
        $this->assertFalse($body['actor']['active']);
    }

    public function testEditWrongEmailFormat()
    {
        $data = ['actor' => [
            'structure_id' => 1,
            'active' => 0,
            'lastname' => 'Ines',
            'firstname' => 'gfhfght',
            'birthday' => '2019-02-11',
            'email' => 'voicim/"#?l@example.fr',
            'created' => 1549880898,
            'modified' => 1549880898,
            'actor_groups' => [
                ['id' => 1],
                ['id' => 2],
            ],
        ]];

        $this->setJsonRequest();
        $this->put('/api/v1/actors/1', json_encode($data));
        $this->assertResponseCode(400);
    }

    /**
     * [testEditOtherStructureFail description]
     *
     * @return [type] [description]
     */
    public function testEditOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "actors"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $data = ['actor' => [
            'structure_id' => 1,
            'active' => 0,
            'rank' => 4,
            'civility' => 'M',
            'lastname' => 'Ines',
            'firstname' => 'gfhfght',
            'birthday' => '2019-02-11',
            'email' => 'voicimonemail@example.fr',
            'title' => 'Lorem ipsum dsdfsdolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'address_supplement' => 'Lorem ipsum dolor sit amet',
            'post_code' => 'Lor',
            'city' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum d',
            'cellphone' => 'Lorem ipsum d',
            'created' => 1549880898,
            'modified' => 1549880898,
        ]];

        $this->put('/api/v1/actors/2', json_encode($data));
        $this->assertResponseCode(404);
    }

    /**
     * [testEditChangeStructureFail description]
     *
     * @return [type] [description]
     */
    public function testEditChangeStructureFail()
    {
        $this->setJsonRequest();

        $data = ['actor' => [
            'structure_id' => 2,
            'active' => 0,
            'rank' => 4,
            'civility' => 'M',
            'lastname' => 'Ines',
            'firstname' => 'gfhfght',
            'birthday' => '2019-02-11',
            'email' => 'voicimonemail@example.fr',
            'title' => 'Lorem ipsum dsdfsdolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'address_supplement' => 'Lorem ipsum dolor sit amet',
            'post_code' => 'Lor',
            'city' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum d',
            'cellphone' => 'Lorem ipsum d',
            'created' => 1549880898,
            'modified' => 1549880898,
        ]];

        $this->put('/api/v1/actors/1', json_encode($data));
        $this->assertResponseCode(400);

        $body = $this->getResponse();
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $this->assertEquals($expected, $body['errors']);
    }

    /**
     * Test method activate
     */
    public function testActivate()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/actors/1/activate/');
        $this->assertResponseCode(201);
        //        $this->setJsonRequest();
        //        $this->get('/api/v1/themes/active');
        //        $body = $this->getResponse();
        //        $this->assertEquals(8, count($body['themes']));
    }

    /**
     * Test method deactivate
     */
    public function testDeactivate()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/actors/1/deactivate/');
        $this->assertResponseCode(201);
        //        $body = $this->getResponse();
        //        $this->assertEquals(false, $body['theme']['active']);
    }
}
