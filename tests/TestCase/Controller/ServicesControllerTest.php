<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * App\Controller\ServicesController Test Case
 */
class ServicesControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Services',
        'app.UsersServices',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/services');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();
        $expected = [
            'services' => [
                0 => [
                    'id' => 1,
                    'structure_id' => 1,
                    'active' => true,
                    'name' => 'DSI',
                    'parent_id' => null,
                    'lft' => 1,
                    'rght' => 6,
                    'created' => '2019-02-11T10:28:26+00:00',
                    'modified' => '2019-02-11T10:28:26+00:00',
                    'children' => [
                        0 => [
                            'id' => 2,
                            'structure_id' => 1,
                            'active' => true,
                            'name' => 'RH',
                            'parent_id' => 1,
                            'lft' => 2,
                            'rght' => 3,
                            'created' => '2019-02-11T10:28:26+00:00',
                            'modified' => '2019-02-11T10:28:26+00:00',
                            'children' => [],
                        ],
                        1 => [
                            'id' => 3,
                            'structure_id' => 1,
                            'active' => true,
                            'name' => 'Facturation',
                            'parent_id' => 1,
                            'lft' => 4,
                            'rght' => 5,
                            'created' => '2019-02-11T10:28:26+00:00',
                            'modified' => '2019-02-11T10:28:26+00:00',
                            'children' => [],
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/services/1');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();
        $expected = [
            'service' => [
                'id' => 1,
                'structure_id' => 1,
                'active' => true,
                'name' => 'DSI',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 6,
                'created' => '2019-02-11T10:28:26+00:00',
                'modified' => '2019-02-11T10:28:26+00:00',
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test viewWithChildren method
     *
     * @return void
     */
    public function testViewWithChildren()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/services/1/withChildren');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();
        $this->assertEquals(2, count($actual['service'][0]['children']));
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'active' => 1,
            'name' => 'Service techniques municipaux',
        ];
        $this->post('/api/v1/services', $data);
        $this->assertResponseCode(201);
        $actual = $this->getResponse();
        $this->assertEquals(['id' => 7], $actual);

        $this->setJsonRequest();
        $this->get('/api/v1/services/7');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();
        $expected = [
            'service' => [
                'id' => 7,
                'structure_id' => 1,
                'active' => true,
                'name' => 'Service techniques municipaux',
                'parent_id' => null,
                'lft' => 13,
                'rght' => 14,
                'created' => $actual['service']['created'],
                'modified' => $actual['service']['modified'],
            ],
        ];
        $this->assertEquals($expected, $actual);

        $this->setJsonRequest();
        $this->get('/api/v1/services');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();
        $this->assertEquals(2, count($actual['services']));
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddSousService()
    {
        // Ajout du service racine
        $this->setJsonRequest();
        $dataServiceRacine = [
            'structure_id' => 1,
            'active' => 1,
            'name' => 'Service techniques municipaux',
        ];
        $this->post('/api/v1/services', $dataServiceRacine);
        $this->assertResponseCode(201);
        $actualServiceRacine = $this->getResponse();
        $this->assertEquals(['id' => 7], $actualServiceRacine);

        /*----------------------------------------------------------------------------------------------*/

        // Ajout du premier sous service racine
        $this->setJsonRequest();
        $dataPremierSousServiceRacine = [
            'structure_id' => 1,
            'active' => 1,
            'name' => 'Service Propreté - gestion des déchets',
            'parent_id' => $actualServiceRacine['id'],
        ];
        $this->post('/api/v1/services', $dataPremierSousServiceRacine);
        $this->assertResponseCode(201);
        $actualPremierSousServiceRacine = $this->getResponse();
        $this->assertEquals(['id' => 8], $actualPremierSousServiceRacine);

        /*----------------------------------------------------------------------------------------------*/

        // Ajout du deuxieme sous service racine
        $this->setJsonRequest();
        $dataDeuxiemeSousServiceRacine = [
            'structure_id' => 1,
            'active' => 1,
            'name' => 'Service voirie',
            'parent_id' => $actualServiceRacine['id'],
        ];
        $this->post('/api/v1/services', $dataDeuxiemeSousServiceRacine);
        $this->assertResponseCode(201);
        $actualDeuxiemeSousServiceRacine = $this->getResponse();
        $this->assertEquals(['id' => 9], $actualDeuxiemeSousServiceRacine);

        /*----------------------------------------------------------------------------------------------*/

        // Vérification de service et sous service
        $this->setJsonRequest();
        $this->get('/api/v1/services');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();

        $expected = $dataServiceRacine +
            [
                'id' => 7,
                'active' => true,
                'lft' => 13,
                'rght' => 18,
                'parent_id' => null,
                'created' => $actual['services'][1]['created'],
                'modified' => $actual['services'][1]['modified'],
                'children' => [
                    $dataPremierSousServiceRacine +
                    [
                        'id' => 8,
                        'active' => true,
                        'lft' => 14,
                        'rght' => 15,
                        'parent_id' => 7,
                        'created' => $actual['services'][1]['children'][0]['created'],
                        'modified' => $actual['services'][1]['children'][0]['modified'],
                        'children' => [],
                    ],
                    $dataDeuxiemeSousServiceRacine +
                    [
                        'id' => 9,
                        'active' => true,
                        'lft' => 16,
                        'rght' => 17,
                        'parent_id' => 7,
                        'created' => $actual['services'][1]['children'][1]['created'],
                        'modified' => $actual['services'][1]['children'][1]['modified'],
                        'children' => [],
                    ],
                ],
            ];

        $this->assertEquals($expected, $actual['services'][1]);
    }

    public function testAddOtherStructureFail()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
            'active' => 1,
            'name' => 'Other structure',
        ];
        $this->post('/api/v1/services', $data);
        $this->assertResponseCode(400);
        $actual = $this->getResponse();
        $expected = [
            'structure_id' => ['wrong_structure_id'],
        ];
        $this->assertEquals($expected, $actual['errors']);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testPatchEditName()
    {
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'name' => 'DSI1',
        ];
        $this->patch('/api/v1/services/1', $data);
        $this->assertResponseCode(204);
        $this->assertNull($this->getResponse());

        $this->setJsonRequest();
        $this->get('/api/v1/services/1');
        $this->assertResponseCode(200);

        $actual = $this->getResponse();
        $expected = [
            'service' => [
                'id' => 1,
                'structure_id' => 1,
                'active' => true,
                'name' => 'DSI1',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 6,
                'created' => $actual['service']['created'],
                'modified' => $actual['service']['modified'],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testPatchEditChangeChildren()
    {
        $this->setJsonRequest();
        $data = [
            'parent_id' => 3,
        ];
        $this->patch('/api/v1/services/2', $data);
        $this->assertResponseCode(204);
        $this->assertNull($this->getResponse());

        // Vérification de service et sous service
        $this->setJsonRequest();
        $this->get('/api/v1/services');
        $this->assertResponseCode(200);

        $actual = $this->getResponse();
        $expected = [
            'services' => [
                0 => [
                    'id' => 1,
                    'structure_id' => 1,
                    'active' => true,
                    'name' => 'DSI',
                    'parent_id' => null,
                    'lft' => 1,
                    'rght' => 6,
                    'created' => '2019-02-11T10:28:26+00:00',
                    'modified' => '2019-02-11T10:28:26+00:00',
                    'children' => [
                        0 => [
                            'id' => 3,
                            'structure_id' => 1,
                            'active' => true,
                            'name' => 'Facturation',
                            'parent_id' => 1,
                            'lft' => 2,
                            'rght' => 5,
                            'created' => '2019-02-11T10:28:26+00:00',
                            'modified' => '2019-02-11T10:28:26+00:00',
                            'children' => [
                                0 => [
                                    'id' => 2,
                                    'structure_id' => 1,
                                    'active' => true,
                                    'name' => 'RH',
                                    'parent_id' => 3,
                                    'lft' => 3,
                                    'rght' => 4,
                                    'created' => '2019-02-11T10:28:26+00:00',
                                    'modified' => $actual['services'][0]['children'][0]['children'][0]['modified'],
                                    'children' => [],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testPutEdit()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'DSI2',
        ];
        $this->put('/api/v1/services/1', $data);
        $this->assertResponseCode(200);

        $actual = $this->getResponse();
        $expected = [
            'service' => [
                'id' => 1,
                'structure_id' => 1,
                'active' => true,
                'name' => 'DSI2',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 6,
                'created' => '2019-02-11T10:28:26+00:00',
                'modified' => $actual['service']['modified'],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testPutEditChangeChildren()
    {
        $this->setJsonRequest();
        $data = [
            'parent_id' => 3,
        ];
        $this->put('/api/v1/services/2', $data);
        $this->assertResponseCode(200);

        $actual = $this->getResponse();
        $expected = [
            'service' => [
                'id' => 2,
                'structure_id' => 1,
                'active' => true,
                'name' => 'RH',
                'parent_id' => 3,
                'lft' => 3,
                'rght' => 4,
                'created' => '2019-02-11T10:28:26+00:00',
                'modified' => $actual['service']['modified'],
            ],
        ];
        $this->assertEquals($expected, $actual);

        // Vérification de service et sous service
        $this->setJsonRequest();
        $this->get('/api/v1/services');
        $this->assertResponseCode(200);

        $actual = $this->getResponse();
        $expected = [
            'services' => [
                0 => [
                    'id' => 1,
                    'structure_id' => 1,
                    'active' => true,
                    'name' => 'DSI',
                    'parent_id' => null,
                    'lft' => 1,
                    'rght' => 6,
                    'created' => '2019-02-11T10:28:26+00:00',
                    'modified' => '2019-02-11T10:28:26+00:00',
                    'children' => [
                        0 => [
                            'id' => 3,
                            'structure_id' => 1,
                            'active' => true,
                            'name' => 'Facturation',
                            'parent_id' => 1,
                            'lft' => 2,
                            'rght' => 5,
                            'created' => '2019-02-11T10:28:26+00:00',
                            'modified' => '2019-02-11T10:28:26+00:00',
                            'children' => [
                                0 => [
                                    'id' => 2,
                                    'structure_id' => 1,
                                    'active' => true,
                                    'name' => 'RH',
                                    'parent_id' => 3,
                                    'lft' => 3,
                                    'rght' => 4,
                                    'created' => '2019-02-11T10:28:26+00:00',
                                    'modified' => $actual['services'][0]['children'][0]['children'][0]['modified'],
                                    'children' => [],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testEditOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "services"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'name' => 'DSI1',
        ];
        $this->patch('/api/v1/services/4', $data);
        $this->assertResponseCode(404);
    }

    public function testEditChangeStructureFail()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'Test',
            'structure_id' => 2,
        ];
        $this->patch('/api/v1/services/1', $data);
        $this->assertResponseCode(400);
        $actual = $this->getResponse();
        $expected = [
            'structure_id' => ['wrong_structure_id'],
        ];
        $this->assertEquals($expected, $actual['errors']);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDeleteChildren()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/services/3');
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/services');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();

        $this->assertEquals($actual['services'][0]['id'], 1);
        $this->assertEquals($actual['services'][0]['structure_id'], 1);
        $this->assertEquals($actual['services'][0]['active'], true);
        $this->assertEquals($actual['services'][0]['name'], 'DSI');
        $this->assertEquals($actual['services'][0]['parent_id'], 0);
        $this->assertEquals($actual['services'][0]['lft'], 1);
        $this->assertEquals($actual['services'][0]['rght'], 4);

        $this->assertEquals($actual['services'][0]['children'][0]['id'], 2);
        $this->assertEquals($actual['services'][0]['children'][0]['structure_id'], 1);
        $this->assertEquals($actual['services'][0]['children'][0]['active'], true);
        $this->assertEquals($actual['services'][0]['children'][0]['name'], 'RH');
        $this->assertEquals($actual['services'][0]['children'][0]['parent_id'], 1);
        $this->assertEquals($actual['services'][0]['children'][0]['lft'], 2);
        $this->assertEquals($actual['services'][0]['children'][0]['rght'], 3);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDeleteParentChildren()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/services/1');
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/services');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();

        $this->assertEmpty($actual['services']);
    }

    public function testDeleteOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "services"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->delete('/api/v1/services/4');
        $this->assertResponseCode(404);
    }

    public function testDeleteFailedInexistantRecord()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "services"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->delete('/api/v1/services/666');
        $this->assertResponseCode(404);
    }
}
