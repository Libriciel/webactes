<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Utility\Hash;

/**
 * App\Controller\ThemesController Test Case
 */
class ThemesControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Container',
        'app.Stateacts',
        'app.Workflows',
        'app.Templates',
        'app.GenerateTemplates',
        'app.Natures',
        'app.Themes',
        'app.Acts',
        'app.Projects',
        'app.Typesacts',
        'app.Containers',
        'app.ContainersStateacts',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/themes');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $actual = Hash::extract($body, 'themes.{n}.id');
        $this->assertEquals([1, 4, 8], $actual);
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/themes/6');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(6, $body['theme']['id']);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'active' => 0,
            'name' => 'A new parent theme',
            'parent_id' => null,
            'position' => 'D',
        ];

        $this->post('/api/v1/themes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['Themes']['id']);
        $themeId = $body['Themes']['id'];

        $this->setJsonRequest();
        $this->get('/api/v1/themes/' . $themeId);
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $this->assertEquals(12, $body['theme']['id']);
        $this->assertEquals(1, $body['theme']['structure_id']);
        $this->assertEquals(0, $body['theme']['active']);
        $this->assertEquals('A new parent theme', $body['theme']['name']);
        $this->assertEquals(null, $body['theme']['parent_id']);
        $this->assertEquals('D', $body['theme']['position']);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddNotAdminFails()
    {
        $this->switchUser(3);
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'active' => 0,
            'name' => 'A new parent theme',
            'parent_id' => null,
            'position' => 'D',
        ];
        $this->post('/api/v1/themes', $data);
        $this->assertResponseCode(403);
    }

    /**
     * Test add children to theme
     *
     * @return void
     */
    public function testAddWithChildren()
    {
        $this->switchUser(1);
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'active' => 0,
            'name' => 'Another parent theme',
            'parent_id' => null,
            'position' => 'D',
        ];
        $this->post('/api/v1/themes', $data);
        $this->assertResponseCode(201);
        $parentBody = $this->getResponse();
        $this->assertEquals(12, $parentBody['Themes']['id']);
        $this->assertNotEmpty($parentBody['Themes']['id']);

        //add first subTheme
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'active' => 0,
            'name' => 'FirstSubTheme',
            'parent_id' => $parentBody['Themes']['id'],
            'created' => 1549880909,
            'modified' => 1549880909,
            'position' => 'D1',
        ];

        $this->post('/api/v1/themes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['Themes']['id']);
        $this->assertEquals(13, $body['Themes']['id']);

        // Add second subtheme
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'active' => 0,
            'name' => 'SecondSubTheme',
            'parent_id' => $parentBody['Themes']['id'],
            'created' => 1549880909,
            'modified' => 1549880909,
            'position' => 'D2',
        ];

        $this->post('/api/v1/themes', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();
        $this->assertNotEmpty($parentBody['Themes']['id']);
        $this->assertEquals(14, $body['Themes']['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/themes');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(3, count($body['themes']));
    }

    public function testAddOtherStructureFail()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
            'active' => 0,
            'name' => 'A new parent theme',
            'parent_id' => null,
            'created' => 1549880909,
            'modified' => 1549880909,
            'position' => 'Lorem',
        ];
        $this->post('/api/v1/themes', $data);
        $this->assertResponseCode(400);
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $body = $this->getResponse();
        $this->assertEquals($expected, $body['errors']);
    }

    /**
     * Test patch edit method
     *
     * @return void
     */
    public function testPatchEdit()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'ChangeName',
        ];
        $this->patch('/api/v1/themes/1', $data);
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/themes/1');
        $body = $this->getResponse();
        $this->assertEquals(1, $body['theme']['id']);
        $this->assertEquals('ChangeName', $body['theme']['name']);
    }

    /**
     * Test put edit method
     *
     * @return void
     */
    public function testPutEdit()
    {
        $this->setJsonRequest();
        $data = [
            'parent_id' => 4,
        ];
        $this->put('/api/v1/themes/2', $data);
        $this->assertResponseCode(200);

        $this->setJsonRequest();
        $this->get('/api/v1/themes/2');
        $body = $this->getResponse();
        $this->assertEquals(2, $body['theme']['id']);
        $this->assertEquals(4, $body['theme']['parent_id']);
    }

    /**
     * Test delete method
     */
    public function testDelete()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/themes/2');
        $this->assertResponseCode(204);
    }

    public function testDeleteNotAdminFails()
    {
        $this->switchUser(4);
        $this->setJsonRequest();
        $this->delete('/api/v1/themes/2');
        $this->assertResponseCode(403);
    }

    public function testEditOtherStructureFail()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'ChangeName',
        ];
        $this->put('/api/v1/themes/9', $data);
        $this->assertResponseCode(400);
    }

    public function testEditChangeStructureFail()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
        ];
        $this->put('/api/v1/themes/1', $data);
        $this->assertResponseCode(400);
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $body = $this->getResponse();
        $this->assertEquals($expected, $body['errors']);
    }

    /**
     * Test method activate
     */
    public function testActivate()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/themes/1/activate/');
        $this->assertResponseCode(201);

        $this->setJsonRequest();
        $this->get('/api/v1/themes/active');
        $body = $this->getResponse();
        $this->assertEquals(8, count($body['themes']));
    }

    /**
     * Test method deactivate
     */
    public function testDeactivate()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/themes/7/deactivate/');
        $this->assertResponseCode(201);
        $body = $this->getResponse();
        $this->assertEquals(false, $body['theme']['active']);
    }

    /**
     * Test method deactivate
     */
    public function testDeactivateFailsIfIsParent()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/themes/4/deactivate/');
        $this->assertResponseCode(400);

        $this->setJsonRequest();
        $this->get('/api/v1/themes/4');
        $body = $this->getResponse();
        $this->assertResponseCode(200);
        $this->assertEquals(true, $body['theme']['active']);
    }

    /**
     * Test method deactivate
     */
    public function testCreateSameLibelle()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'active' => 0,
            'name' => 'Administration Générale',
            'parent_id' => null,
            'position' => 'Lorem',
        ];

        $this->post('/api/v1/themes', $data);
        $this->assertResponseCode(201);
    }
}
