<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Controller\Exception\MissingActionException;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Hash;

/**
 * App\Controller\TypesittingsController Test Case
 */
class TypesittingsControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Sitting',
        'app.group.Actor',
        'app.group.Project',
        'app.group.TypeAct',
        'app.ActorGroups',
        'app.Natures',
        'app.GenerateTemplates',
        'app.Typesacts',
        'app.Typesittings',
        'app.TypesactsTypesittings',
        'app.ActorGroupsTypesittings',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/typesittings');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $actual = Hash::extract($body, 'typeSittings.{n}.id');
        $this->assertEquals([1, 5, 3, 2], $actual);
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/typesittings/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(count($body), 1);
        $this->assertEquals($body['typesitting']['id'], 1);
    }

    /**
     * Test view doesn't exist fails
     */
    public function testViewNotExists()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "typesittings"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/typesittings/999999');
        $this->assertResponseCode(404);
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testViewFailedBadId()
    {
        $this->expectException(MissingActionException::class);
        $this->expectExceptionMessage(
            'Action TypesittingsController::notFoundId() could not be found, or is not accessible.'
        );
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/typesittings/notFoundId');
        $this->assertResponseCode(404);
    }

    /**
     * Test view other structure fails
     */
    public function testViewOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "typesittings"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/typesittings/4');
        $this->assertResponseCode(404);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'AddType',
            'typeActs' => [
                ['id' => 1],
            ],
        ];
        $this->post('/api/v1/typesittings', $data);
        $body = $this->getResponse();
        $this->assertResponseCode(201);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['typesitting']['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/typesittings');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(5, count($body['typeSittings']));
    }

    /**
     * Test already used name fails
     */
    public function testAddAlreadyExistingNameFails()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'Conseil Municipal',
            'typeActs' => [
                ['id' => 3],
            ],
        ];
        $this->post('/api/v1/typesittings', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test add method fails
     *
     * @return void
     */
    public function testAddFailed()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/typesittings', []);
        $this->assertResponseCode(400);
        $expected = [
            'name' => [
                '_required' => 'This field is required',
            ],
        ];
        $body = $this->getResponse();
        $this->assertEquals($expected, $body['errors']);
    }

    public function testAddOtherStructureFail()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
            'name' => 'AddType',
        ];
        $this->post('/api/v1/typesittings', $data);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $this->assertEquals($expected, $body['errors']);
    }

    public function testPatchEdit()
    {
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'name' => 'NewNameTypeSitting',
        ];
        $this->patch('/api/v1/typesittings/1', $data);
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/typesittings/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals($body['typesitting']['id'], 1);
        $this->assertEquals($body['typesitting']['name'], 'NewNameTypeSitting');
    }

    public function testPatchEditExistingNameFails()
    {
        $this->setJsonRequest();
        $data = [
            'name' => 'Conseil Municipal',
        ];
        $this->patch('/api/v1/typesittings/1', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testPutEdit()
    {
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'name' => 'TOTO',
        ];
        $this->put('/api/v1/typesittings/1', $data);
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['typesitting']['id']);
        $this->assertNotEmpty($body['typesitting']['structure_id']);
        $this->assertNotEmpty($body['typesitting']['name']);
        $this->assertNotEmpty($body['typesitting']['created']);
        $this->assertNotEmpty($body['typesitting']['modified']);

        $this->setJsonRequest();
        $this->get('/api/v1/typesittings/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals($body['typesitting']['id'], 1);
        $this->assertEquals($body['typesitting']['name'], 'TOTO');
    }

    /**
     * Test patch other structure fails
     */
    public function testPatchOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "typesittings"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'name' => 'NewNameTypeSitting',
        ];
        $this->patch('/api/v1/typesittings/4', $data);
        $this->assertResponseCode(404);
    }

    /**
     * Test put other structure fails
     */
    public function testPutOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "typesittings"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'name' => 'TOTO',
        ];
        $this->put('/api/v1/typesittings/4', $data);
        $this->assertResponseCode(404);
    }

    public function testPatchChangeStructureFail()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
        ];
        $this->put('/api/v1/typesittings/1', $data);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $this->assertEquals($expected, $body['errors']);
    }

    public function testPutChangeStructureFail()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
        ];
        $this->put('/api/v1/typesittings/1', $data);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $this->assertEquals($expected, $body['errors']);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->switchUser(1);

        $this->setJsonRequest();
        $this->delete('/api/v1/typesittings/5');
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('/api/v1/typesittings');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(3, count($body['typeSittings']));
    }

    /**
     * Test delete other structure fails
     */
    public function testDeleteOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "typesittings"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->delete('/api/v1/typesittings/4');
        $this->assertResponseCode(404);
    }
}
