<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;

/**
 * App\Controller\DraftTemplateTypesController Test Case
 *
 * @uses \App\Controller\DraftTemplateTypesController
 */
class DraftTemplateTypesControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.DraftTemplateTypes',
        'app.DraftTemplates',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex(): void
    {
        $this->setJsonRequest();
        $this->get('/api/v1/draft-template-types');
        $body = $this->getResponse();
        $this->assertResponseCode(200);

        $this->assertEquals(2, count($body['draftTemplateTypes']));
        $this->assertEquals('Acte', $body['draftTemplateTypes'][0]['name']);
    }
}
