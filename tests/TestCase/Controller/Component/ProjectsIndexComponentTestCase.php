<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Component;

use App\Controller\AppController;
use App\Controller\Component\ProjectsIndexComponent;
use App\Test\Mock\FlowableWrapperMock;
use App\Test\TestCase\AbstractTestCase;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Cake\Controller\Component\AuthComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\Http\Session;

class ProjectsIndexComponentTestCase extends AbstractTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @var \App\Controller\Component\ProjectsIndexComponent
     */
    protected $component;

    /**
     * @var \Cake\Controller\Controller
     */
    protected $controller;

    /**
     * @var ProjectsTable
     */
    protected $Projects;

    /**
     * @var string[]
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Generationerrors',
        'app.Stateacts',
        'app.Templates',
        'app.Themes',
        'app.Workflows',
        'app.Actors',
        'app.Acts',
        'app.GenerateTemplates',
        'app.Matieres',
        'app.Natures',
        'app.Projects',
        'app.Typesacts',
        'app.Typesittings',
        'app.Typespiecesjointes',
        'app.Containers',
        'app.ContainersStateacts',
        'app.Histories',
        'app.Maindocuments',
        'app.Sittings',
        'app.ContainersSittings',
    ];

    public function setUp(): void
    {
        parent::setUp();

        Configure::write('KEYCLOAK_DEV_MODE', true);
        Configure::write('Flowable.wrapper_mock', '\App\Test\Mock\FlowableWrapperMock');

        FlowableWrapperMock::reset();

        $this->Projects = $this->fetchTable('Projects');

        $request = new ServerRequest([
            'url' => '/',
            'environment' => [
                'REQUEST_METHOD' => 'GET',
            ],
            'params' => [
                'plugin' => null,
                'controller' => 'Foo',
                'action' => 'index',
            ],
            'session' => new Session(),
            'webroot' => '/',
        ]);
        $response = new Response();
        /*$this->controller = $this->getMockBuilder('Cake\Controller\Controller')
            ->setConstructorArgs([$request, $response])
            ->setMethods(null)
            ->getMock();*/
        $this->controller = new AppController($request, $response);
        $registry = new ComponentRegistry($this->controller);
        $this->component = new ProjectsIndexComponent($registry);
        $this->controller->Auth = new AuthComponent($registry);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->component, $this->controller);
    }
}
