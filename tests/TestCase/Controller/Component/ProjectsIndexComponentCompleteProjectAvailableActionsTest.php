<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Component;

use Cake\ORM\Query;
use Cake\Utility\Hash;

/**
 * @group ProjectsIndex
 */
class ProjectsIndexComponentCompleteProjectAvailableActionsTest extends ProjectsIndexComponentTestCase
{
    protected function getProject(int $id)
    {
        return $this->Projects
            ->find()
            ->select(['id', 'structure_id'])
            ->contain([
                'Containers',
                'Containers.Stateacts' => function (Query $query) {
                    return $query
                        ->select(['Stateacts.id'])
                        ->where([
                            'ContainersStateacts.id' => $this->fetchTable('ContainersStateacts')
                                ->getLastByContainersIdSubquery('ContainersStateacts.container_id'),
                        ]);
                },
            ])
            ->where(['Projects.id' => $id])
            ->first()
            ->toArray();
    }

    /**
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project1
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project2
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project3
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project4
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project5
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project6
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project7
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project8
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project9
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project10
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project11
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project12
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project13
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project14
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project15
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project16
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project17
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project18
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project19
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project20
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project21
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project22
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project23
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project24
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project25
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project26
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project27
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project28
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project29
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project30
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project31
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project32
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project33
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project34
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project35
     * @dataProvider dataCompleteProjectAvailableActionsStructure2Project36
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project37
     * @dataProvider dataCompleteProjectAvailableActionsStructure1Project38
     */
    public function testCompleteProjectAvailableActions($user, $projectId, array $expected): void
    {
        $this->switchUser(is_int($user) ? $user : $this->getUserId($user));
        $this->controller->getRequest()->getSession()->write($this->_session);

        $project = $this->getProject($projectId);

        $this->component->completeProjectAvailableActions($project);

        $actual = array_combine(
            array_keys($project['availableActions']),
            Hash::extract($project['availableActions'], '{s}.value')
        );

        $this->assertEquals($expected, $actual);
    }

    protected function getDataWithProjectId(string $method, $id)
    {
        $data = $this->{$method}();
        foreach (array_keys($data) as $idx) {
            $data[$idx][1] = $id;
        }

        return $data;
    }

    /**
     * Projet à l'état DRAFT (brouillon), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project1()
    {
        $id = 1;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => true,
            'can_be_voted' => true,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => false,
            'can_be_deleted' => false,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            ['j.orange', $id, $defaultAvailableActions],
            ['r.violet', $id, $defaultAvailableActions],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état DRAFT (brouillon), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project2()
    {
        $id = 2;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => true,
            'can_be_voted' => true,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => false,
            'can_be_deleted' => true,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            ['j.orange', $id, $defaultAvailableActions],
            ['r.violet', $id, $defaultAvailableActions],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état VALIDATION_PENDING (en-cours-de-validation), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project3()
    {
        $id = 3;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => true,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => true,
            'can_be_deleted' => true,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            [
                'j.orange',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_edited' => false, 'can_approve_project' => false, 'can_be_deleted' => false]
                ),
            ],
            [
                'r.violet',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_edited' => false, 'can_approve_project' => false, 'can_be_deleted' => false]
                ),
            ],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état VALIDATION_PENDING (en-cours-de-validation), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project4()
    {
        $id = 4;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => true,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => true,
            'can_be_deleted' => false,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            [
                'j.orange',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_edited' => false, 'can_approve_project' => false]
                ),
            ],
            [
                'r.violet',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_edited' => false, 'can_approve_project' => false]
                ),
            ],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état VALIDATION_PENDING (en-cours-de-validation), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project5()
    {
        $id = 5;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => true,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => true,
            'can_be_deleted' => false,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            [
                'j.orange',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_edited' => false, 'can_approve_project' => false]
                ),
            ],
            [
                'r.violet',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_edited' => false, 'can_approve_project' => false]
                ),
            ],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état VALIDATION_PENDING (en-cours-de-validation), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project6()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project5', 6);
    }

    /**
     * Projet à l'état VALIDATION_PENDING (en-cours-de-validation), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project7()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project5', 7);
    }

    /**
     * Projet à l'état VALIDATION_PENDING (en-cours-de-validation), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project8()
    {
        $id = 8;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => true,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => true,
            'can_be_deleted' => true,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            [
                'j.orange',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_edited' => false, 'can_approve_project' => false, 'can_be_deleted' => false]
                ),
            ],
            [
                'r.violet',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_edited' => false, 'can_approve_project' => false, 'can_be_deleted' => false]
                ),
            ],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état VALIDATION_PENDING (en-cours-de-validation), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project9()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project8', 9);
    }

    /**
     * Projet à l'état VALIDATION_PENDING (en-cours-de-validation), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project10()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project8', 10);
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project11()
    {
        $id = 11;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => true,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => false,
            'can_be_deleted' => true,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            [
                'j.orange',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_deleted' => false]
                ),
            ],
            [
                'r.violet',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_deleted' => false]
                ),
            ],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project12()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project11', 12);
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project13()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project11', 13);
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project14()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project11', 14);
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project15()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project11', 15);
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project16()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project11', 16);
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project17()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project11', 17);
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project18()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project11', 18);
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project19()
    {
        $id = 19;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => true,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => false,
            'can_be_deleted' => false,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            ['j.orange', $id, $defaultAvailableActions],
            ['r.violet', $id, $defaultAvailableActions],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project20()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project11', 20);
    }

    /**
     * Projet à l'état REFUSED (refuse), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project21()
    {
        $id = 21;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => true,
            'can_be_voted' => false,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => false,
            'can_be_deleted' => true,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            ['j.orange', $id, $defaultAvailableActions],
            ['r.violet', $id, $defaultAvailableActions],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état REFUSED (refuse), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project22()
    {
        $id = 22;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => true,
            'can_be_voted' => false,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => false,
            'can_be_deleted' => true,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            ['j.orange', $id, $defaultAvailableActions],
            ['r.violet', $id, $defaultAvailableActions],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état TO_TDT (a-teletransmettre), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project23()
    {
        $id = 23;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => false,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => false,
            'can_approve_project' => false,
            'can_be_deleted' => true,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            ['j.orange', $id, array_merge($defaultAvailableActions, ['can_be_deleted' => false])],
            ['r.violet', $id, array_merge($defaultAvailableActions, ['can_be_deleted' => false])],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état PENDING_PASTELL (en-cours-depot-tdt), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project24()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project23', 24);
    }

    /**
     * Projet à l'état TDT_ERROR (erreur-tdt), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project25()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project23', 25);
    }

    /**
     * Projet à l'état TDT_ERROR (erreur-tdt), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project26()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project23', 26);
    }

    /**
     * Projet à l'état DECLARE_SIGNED (declare-signe), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project27()
    {
        $id = 27;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => false,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => false,
            'can_approve_project' => false,
            'can_be_deleted' => false,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            ['j.orange', $id, $defaultAvailableActions],
            ['r.violet', $id, $defaultAvailableActions],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état PARAPHEUR_SIGNED (signe-i-parapheur), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project28()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project27', 28);
    }

    /**
     * Projet à l'état PARAPHEUR_SIGNED (signe-i-parapheur), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project29()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project27', 29);
    }

    /**
     * Projet à l'état TO_ORDER (a-ordonner), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project30()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project23', 30);
    }

    /**
     * Projet à l'état PENDING_TDT (en-cours-teletransmission), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project31()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project23', 31);
    }

    /**
     * Projet à l'état ACQUITTED (acquitte), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project32()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project23', 32);
    }

    /**
     * Projet à l'état TDT_CANCEL (annule), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project33()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project23', 33);
    }

    /**
     * Projet à l'état TDT_CANCEL (annule), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project34()
    {
        return $this->getDataWithProjectId('dataCompleteProjectAvailableActionsStructure1Project23', 34);
    }

    /**
     * Projet à l'état PARAPHEUR_REFUSED (refuse-i-parapheur), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project35()
    {
        $id = 35;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => true,
            'can_be_send_to_manual_signature' => true,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => false,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => true,
            'can_be_edited' => true,
            'can_approve_project' => false,
            'can_be_deleted' => true,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            [
                'j.orange',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_edited' => false, 'can_be_deleted' => false]
                ),
            ],
            [
                'r.violet',
                $id,
                array_merge(
                    $defaultAvailableActions,
                    ['can_be_edited' => false, 'can_be_deleted' => false, 'can_be_send_to_i_parapheur' => false, 'can_be_send_to_manual_signature' => false]
                ),
            ],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état DRAFT (brouillon), structure 2
     */
    public function dataCompleteProjectAvailableActionsStructure2Project36()
    {
        $id = 36;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => true,
            'can_be_voted' => false,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => false,
            'can_be_deleted' => true,
        ];

        return [
            ['s.white', $id, $defaultAvailableActions],
            ['m.green', $id, $defaultAvailableActions],
            ['v.yellow', $id, $defaultAvailableActions],
            ['s.purple', $id, $defaultAvailableActions],
            ['e.lime', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état VOTED_APPROVED (vote-et-approuve), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project37()
    {
        $id = 37;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => true,
            'can_be_send_to_manual_signature' => true,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => true,
            'can_generate_act_number' => false,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => false,
            'can_be_deleted' => false,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            ['j.orange', $id, array_merge($defaultAvailableActions, ['can_be_edited' => false])],
            ['r.violet', $id, array_merge($defaultAvailableActions, ['can_be_edited' => false, 'can_be_send_to_i_parapheur' => false, 'can_be_send_to_manual_signature' => false])],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }

    /**
     * Projet à l'état VALIDATED (valide), structure 1
     */
    public function dataCompleteProjectAvailableActionsStructure1Project38()
    {
        $id = 38;
        $defaultAvailableActions = [
            'can_be_send_to_i_parapheur' => false,
            'can_be_send_to_manual_signature' => false,
            'can_be_send_to_workflow' => false,
            'can_be_voted' => false,
            'can_generate_act_number' => true,
            'check_signing' => false,
            'check_refused' => false,
            'can_be_edited' => true,
            'can_approve_project' => false,
            'can_be_deleted' => true,
        ];

        return [
            ['s.blanc', $id, $defaultAvailableActions],
            ['m.vert', $id, $defaultAvailableActions],
            ['j.orange', $id, array_merge($defaultAvailableActions, ['can_be_deleted' => false])],
            ['r.violet', $id, array_merge($defaultAvailableActions, ['can_be_deleted' => false, 'can_generate_act_number' => false])],
            ['r.cyan', $id, $defaultAvailableActions],
        ];
    }
}
