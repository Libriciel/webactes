<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Component;

use Cake\Utility\Hash;

/**
 * @group ProjectsIndex
 */
class ProjectsIndexComponentGetQueryTest extends ProjectsIndexComponentTestCase
{
    /**
     * @dataProvider dataProviderGetQueryDrafts
     * @dataProvider dataProviderGetQueryValidating
     * @dataProvider dataProviderGetQueryToValidate
     * @dataProvider dataProviderGetQueryValidated
     * @dataProvider dataProviderGetQueryUnassociated
     * @dataProvider dataProviderGetQueryToTransmit
     * @dataProvider dataProviderGetQueryReadyToTransmit
     * @dataProvider dataProviderGetQueryAct
     */
    public function testGetQuery(string $method, $user, array $expected)
    {
        $this->switchUser(is_int($user) ? $user : $this->getUserId($user));
        $this->controller->getRequest()->getSession()->write($this->_session);

        $query = $this->component->{$method}();

        $results = $query->select(['Projects.id'])->enableHydration(false)->toArray();
        $actual = Hash::extract($results, '{n}.id');
        $this->assertEquals($expected, $actual);
    }

    /**
     * Projets à l'état "Brouillon"
     *
     * @deprecated
     * @return array[]
     */
    public function dataProviderGetQueryDrafts()
    {
        $method = 'getQueryDrafts';

        return [
            // Organization 1, Structure 1
            [$method, 's.blanc', [22, 21, 2, 1]],
            [$method, 'm.vert', [22, 21, 2, 1]],
            [$method, 'j.orange', [22, 21, 2, 1]],
            [$method, 'r.violet', [21, 1]],
            [$method, 'r.cyan', [22, 21, 2, 1]],
            // Organization 1, Structure 2
            [$method, 's.white', [36]],
            [$method, 'm.green', [36]],
            [$method, 'v.yellow', [36]],
            [$method, 's.purple', []],
            [$method, 'e.lime', [36]],
        ];
    }

    /**
     * Projets à l'état "En cours dans un circuit"
     *
     * @return array[]
     */
    public function dataProviderGetQueryValidating()
    {
        $method = 'getQueryValidating';

        return [
            // Organization 1, Structure 1
            [$method, 's.blanc', [10, 9, 8, 7, 6, 5, 4, 3]],
            [$method, 'm.vert', [10, 9, 8, 7, 6, 5, 4, 3]],
            [$method, 'j.orange', []],
            [$method, 'r.violet', [9, 5]],
            [$method, 'r.cyan', [7, 3]],
            // Organization 1, Structure 2
            [$method, 's.white', []],
            [$method, 'm.green', []],
            [$method, 'v.yellow', []],
            [$method, 's.purple', []],
            [$method, 'e.lime', []],
        ];
    }

    /**
     * Projets à l'état "À traiter"
     *
     * @return array[]
     */
    public function dataProviderGetQueryToValidate()
    {
        $method = 'getQueryToValidate';

        return [
            // Organization 1, Structure 1
            [$method, 's.blanc', []],
            [$method, 'm.vert', []],
            [$method, 'j.orange', []],
            [$method, 'r.violet', [8, 7, 4, 3]],
            [$method, 'r.cyan', [10, 9, 8, 6, 5, 4]],
            // Organization 1, Structure 2
            [$method, 's.white', []],
            [$method, 'm.green', []],
            [$method, 'v.yellow', []],
            [$method, 's.purple', []],
            [$method, 'e.lime', []],
        ];
    }

    /**
     * Projets à l'état "Validés"
     *
     * @return array[]
     */
    public function dataProviderGetQueryValidated()
    {
        $method = 'getQueryValidated';

        return [
            // Organization 1, Structure 1
            [$method, 's.blanc', [38, 37, 35, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11]],
            [$method, 'm.vert', [38, 37, 35, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11]],
            [$method, 'j.orange', [38, 37, 35, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11]],
            [$method, 'r.violet', [38, 37, 35, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11]],
            [$method, 'r.cyan', [38, 37, 35, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11]],
            // Organization 1, Structure 2
            [$method, 's.white', [39]],
            [$method, 'm.green', [39]],
            [$method, 'v.yellow', [39]],
            [$method, 's.purple', [39]],
            [$method, 'e.lime', [39]],
        ];
    }

    /**
     * Projets à l'état "Projets sans séances"
     *
     * @return array[]
     */
    public function dataProviderGetQueryUnassociated()
    {
        $method = 'getQueryUnassociated';

        return [
            // Organization 1, Structure 1
            [$method, 's.blanc', [33, 22, 21, 20, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 3, 2]],
            [$method, 'm.vert', [33, 22, 21, 20, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 3, 2]],
            [$method, 'j.orange', [33, 22, 21, 20, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 3, 2]],
            [$method, 'r.violet', [33, 22, 21, 20, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 3, 2]],
            [$method, 'r.cyan', [33, 22, 21, 20, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 3, 2]],
            // Organization 1, Structure 2
            [$method, 's.white', []],
            [$method, 'm.green', []],
            [$method, 'v.yellow', []],
            [$method, 's.purple', []],
            [$method, 'e.lime', []],
        ];
    }

    /**
     * Projets à l'état "À déposer sur le Tdt"
     *
     * @return array[]
     */
    public function dataProviderGetQueryToTransmit()
    {
        $method = 'getQueryToTransmit';

        return [
            // Organization 1, Structure 1
            [$method, 's.blanc', [28, 26, 25, 24, 23]],
            [$method, 'm.vert', [28, 26, 25, 24, 23]],
            [$method, 'j.orange', [28, 26, 25, 24, 23]],
            [$method, 'r.violet', [28, 26, 25, 24, 23]],
            [$method, 'r.cyan', [28, 26, 25, 24, 23]],
            // Organization 1, Structure 2
            [$method, 's.white', []],
            [$method, 'm.green', []],
            [$method, 'v.yellow', []],
            [$method, 's.purple', []],
            [$method, 'e.lime', []],
        ];
    }

    /**
     * Projets à l'état "À envoyer en préfecture"
     *
     * @return array[]
     */
    public function dataProviderGetQueryReadyToTransmit()
    {
        $method = 'getQueryReadyToTransmit';

        return [
            // Organization 1, Structure 1
            [$method, 's.blanc', [30]],
            [$method, 'm.vert', [30]],
            [$method, 'j.orange', [30]],
            [$method, 'r.violet', [30]],
            [$method, 'r.cyan', [30]],
            // Organization 1, Structure 2
            [$method, 's.white', []],
            [$method, 'm.green', []],
            [$method, 'v.yellow', []],
            [$method, 's.purple', []],
            [$method, 'e.lime', []],
        ];
    }

    /**
     * Projets à l'état "Liste des actes"
     *
     * @return array[]
     */
    public function dataProviderGetQueryAct()
    {
        $method = 'getQueryAct';

        return [
            // Organization 1, Structure 1
            [$method, 's.blanc', [34, 33, 32, 31, 29, 27]],
            [$method, 'm.vert', [34, 33, 32, 31, 29, 27]],
            [$method, 'j.orange', [34, 33, 32, 31, 29, 27]],
            [$method, 'r.violet', [34, 33, 32, 31, 29, 27]],
            [$method, 'r.cyan', [34, 33, 32, 31, 29, 27]],
            // Organization 1, Structure 2
            [$method, 's.white', []],
            [$method, 'm.green', []],
            [$method, 'v.yellow', []],
            [$method, 's.purple', []],
            [$method, 'e.lime', []],
        ];
    }
}
