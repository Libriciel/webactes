<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;

/**
 * App\Controller\UsersController Test Case
 */

class SoclesControllerTest extends AbstractTestCase
{
    public const XML_DIR = TESTS . 'TestCase' . DS . 'Socle' . DS . 'xml' . DS;
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Sitting',
        'app.group.Container',
        'app.group.TypeAct',
        'app.group.Pastell',
        'app.Notifications',
        'app.Workflows',
        'app.GenerateTemplates',
        'app.Templates',
        'app.Themes',
        'app.Natures',
        'app.Actors',
        'app.NotificationsUsers',
        'app.Typesittings',
        'app.Projects',
        'app.Acts',
        'app.Typesacts',
        'app.Containers',
    ];

    public function setUp(): void
    {
        $this->skipIfNoSocleConfig();

        parent::setUp();
        $this->setDefaultSession();
        $this->locales = Configure::read('App.paths.locales');
        $locales = TESTS . DS . 'test_app' . DS . 'Locale' . DS;
        Configure::write('App.paths.locales', $locales);
        $this->defaultLocale = Configure::read('App.defaultLocale');
        Configure::write('App.defaultLocale', 'fr_FR');
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test add structure
     *
     * @return void
     */
    public function testAddStructure()
    {
        $xml = file_get_contents(self::XML_DIR . 'organism_new.xml');
        $this->assertNotEmpty($xml);
        $this->post('/api/v1/sync-myec3/organism', $xml);
        $this->assertResponseCode(201);
        $Structures = $this->fetchTable('Structures');
        $this->assertEquals(3, $Structures->find()->count());
    }

    /**
     * Test add structure
     *
     * @return void
     */
    public function testAddStructureAlreadyExists()
    {
        $xml = file_get_contents(self::XML_DIR . 'organism.xml');
        $this->assertNotEmpty($xml);
        $this->post('/api/v1/sync-myec3/organism', $xml);
        $this->assertResponseCode(409);
        $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>ORGANISM</classType><errorCode>005</errorCode><errorLabel>RESOURCE_ALREADY_EXISTS</errorLabel><errorMessage>La resource existe déjà</errorMessage><methodType>POST</methodType><resourceId>100000005</resourceId></error>
';
        $this->assertEquals($expected, (string)$this->_response->getBody());
    }

    public function testUpdateStructure()
    {
        $xml = file_get_contents(self::XML_DIR . 'organism.xml');
        $this->assertNotEmpty($xml);
        $this->put('/api/v1/sync-myec3/organism/existingId', $xml);
        $this->assertResponseCode(200);
        $Structures = $this->fetchTable('Structures');
        $this->assertEquals(2, $Structures->find()->count());
    }

    public function testUpdateStructureNotExists()
    {
        $this->markTestSkipped('@fixme: got 200, not 404');
        $xml = file_get_contents(self::XML_DIR . 'organism_new.xml');
        $this->assertNotEmpty($xml);
        $this->put('/api/v1/sync-myec3/organism/newId', $xml);
        $this->assertResponseCode(404);
        $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>ORGANISM</classType><errorCode>003</errorCode><errorLabel>RESOURCE_MISSING</errorLabel><errorMessage>La ressource spécifiée est introuvable</errorMessage><methodType>PUT</methodType><resourceId>12525</resourceId></error>
';
        $this->assertEquals($expected, (string)$this->_response->getBody());
    }

    public function testDeleteStructure()
    {
        $this->delete('/api/v1/sync-myec3/organism/100000005');
        $this->assertResponseCode(204);
    }

    public function testDeleteStructureNotExists()
    {
        $this->delete('/api/v1/sync-myec3/organism/0000254');
        $this->assertResponseCode(404);
        $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>ORGANISM</classType><errorCode>003</errorCode><errorLabel>RESOURCE_MISSING</errorLabel><errorMessage>La ressource spécifiée est introuvable</errorMessage><methodType>DELETE</methodType><resourceId>0000254</resourceId></error>
';
        $this->assertEquals($expected, $this->_response->getBody());
    }

    public function testAddUser()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent.xml');
        $this->assertNotEmpty($xml);
        $this->post('/api/v1/sync-myec3/agent', $xml);
        $this->assertResponseCode(201);
    }

    public function testAddUserAlreadyExists()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_existing.xml');
        $this->assertNotEmpty($xml);
        $this->post('/api/v1/sync-myec3/agent', $xml);
        $this->assertResponseCode(409);
        $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>005</errorCode><errorLabel>RESOURCE_ALREADY_EXISTS</errorLabel><errorMessage>La resource existe déjà</errorMessage><methodType>POST</methodType><resourceId>000000001</resourceId></error>
';
        $this->assertEquals($expected, (string)$this->_response->getBody());
    }

    public function testAddAgentMissingData()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_missingData.xml');
        $this->assertNotEmpty($xml);
        $this->post('/api/v1/sync-myec3/agent', $xml);
        $this->assertResponseCode(400);
        $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>unknown</classType><errorCode>001</errorCode><errorLabel>SYNTAX_ERROR</errorLabel><errorMessage>Syntaxe du message en entrée incorrecte</errorMessage><methodType>unknown</methodType><resourceId>unknown</resourceId></error>
';
        $this->assertEquals($expected, (string)$this->_response->getBody());
    }

    public function testCreateAgent_NotExistsRole()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_notExistsRole.xml');
        $this->assertNotEmpty($xml);
        $this->post('/api/v1/sync-myec3/agent', $xml);
        $this->assertResponseCode(400);
        $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>004</errorCode><errorLabel>RELATION_MISSING</errorLabel><errorMessage>Une relation avec l\'entité est manquante</errorMessage><methodType>POST</methodType><resourceId>300005473</resourceId></error>
';
        $this->assertEquals($expected, (string)$this->_response->getBody());
    }

    public function testUpdateAgent()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_existing.xml');
        $this->assertNotEmpty($xml);
        $this->put('/api/v1/sync-myec3/agent/existingId', $xml);
        $this->assertResponseCode(200);
    }

    public function testUpdateAgentNotExists()
    {
        $this->markTestSkipped('@fixme: got 200, not 404');
        $xml = file_get_contents(self::XML_DIR . 'agent.xml');
        $this->assertNotEmpty($xml);
        $this->put('/api/v1/sync-myec3/agent/notExist', $xml);
        $this->assertResponseCode(404);
        $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>003</errorCode><errorLabel>RESOURCE_MISSING</errorLabel><errorMessage>La ressource spécifiée est introuvable</errorMessage><methodType>PUT</methodType><resourceId>300005473</resourceId></error>
';
        $this->assertEquals($expected, (string)$this->_response->getBody());
    }

    public function testUpdateAgentFalseRole()
    {
        $xml = file_get_contents(self::XML_DIR . 'agent_existingFalseRole.xml');
        $this->assertNotEmpty($xml);
        $this->put('/api/v1/sync-myec3/agent/falseRole', $xml);
        $this->assertResponseCode(400);
        $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>004</errorCode><errorLabel>RELATION_MISSING</errorLabel><errorMessage>Une relation avec l\'entité est manquante</errorMessage><methodType>PUT</methodType><resourceId>000000001</resourceId></error>
';
        $this->assertEquals($expected, (string)$this->_response->getBody());
    }

    public function testDeleteUser()
    {
        $this->delete('/api/v1/sync-myec3/agent/000000001');
        $this->assertResponseCode(204);
    }

    public function testDeleteUserBadId()
    {
        $this->delete('/api/v1/sync-myec3/agent/9999999');
        $this->assertResponseCode(404);
        $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT</classType><errorCode>003</errorCode><errorLabel>RESOURCE_MISSING</errorLabel><errorMessage>La ressource spécifiée est introuvable</errorMessage><methodType>DELETE</methodType><resourceId>9999999</resourceId></error>
';
        $this->assertEquals($expected, (string)$this->_response->getBody());
    }
}
