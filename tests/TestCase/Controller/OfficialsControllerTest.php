<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\MethodNotAllowedException;

/**
 * App\Controller\OfficialsController Test Case
 */
class OfficialsControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Officials',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/officials');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(1, count($body['officials']));
    }

    public function testViewSuccess()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/officials/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();

        $expected = [
            'official' => [
                'id' => 1,
                'structure_id' => 1,
                'civility' => 'M.',
                'lastname' => 'Patrick',
                'firstname' => 'ORANGE',
                'email' => 'p.orange@test.fr',
                'address' => '777 rue des orangeades',
                'address_supplement' => '',
                'post_code' => '34000',
                'city' => 'MONTPELLIER',
                'phone' => '0400000000',
                'created' => '2019-02-12T15:16:11+00:00',
                'modified' => '2019-02-12T15:16:11+00:00',
            ],
        ];
        $this->assertEquals($expected, $body);
        $this->assertNotEmpty($body['official']['id']);
    }

    public function testViewNotExistsFailed()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "officials"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/officials/100');
        $this->assertResponseCode(404);
    }

    public function testViewOtherStructureFailed()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "officials"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/officials/2');
        $this->assertResponseCode(404);
    }

    public function testAdd()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'civility' => 'Mme.',
            'lastname' => 'Jeanne',
            'firstname' => 'ROUGE',
            'email' => 'j.rouge@test.fr',
        ];
        $this->post('/api/v1/officials', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/officials');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(2, count($body['officials']));
    }

    public function testAddFail()
    {
        $this->setJsonRequest();
        $this->post('/api/v1/officials');
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'errors' => [
                'structure_id' => [
                    '_required' => 'This field is required',
                ],
                'civility' => [
                    '_required' => 'This field is required',
                ],
                'lastname' => [
                    '_required' => 'This field is required',
                ],
                'firstname' => [
                    '_required' => 'This field is required',
                ],
                'email' => [
                    '_required' => 'This field is required',
                ],
            ],
        ];
        $this->assertEquals($expected, $body);
    }

    public function testAddOtherStructureFail()
    {
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'structure_id' => 2,
            'civility' => 'Mme.',
            'lastname' => 'Jeanne',
            'firstname' => 'ROUGE',
            'email' => 'j.rouge@test.fr',
        ];
        $this->post('/api/v1/officials', $data);
        $this->assertResponseCode(400);
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $body = $this->getResponse();
        $this->assertEquals($expected, $body['errors']);
    }

    public function testEditSuccess()
    {
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'structure_id' => 1,
            'civility' => 'Mme.',
            'lastname' => 'Jeanne',
            'firstname' => 'BLEU',
            'email' => 'j.bleu@test.fr',
            'address' => '1 rue différente',
            'address_supplement' => 'bis',
            'post_code' => '34001',
            'city' => 'PALAVAS',
            'phone' => '0400000001',
        ];
        $this->patch('/api/v1/officials/1', $data);
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $expected = [
            'official' => [
                'id' => 1,
                'structure_id' => 1,
                'civility' => 'Mme.',
                'lastname' => 'Jeanne',
                'firstname' => 'BLEU',
                'email' => 'j.bleu@test.fr',
                'address' => '1 rue différente',
                'address_supplement' => 'bis',
                'post_code' => '34001',
                'city' => 'PALAVAS',
                'phone' => '0400000001',
                'created' => '2019-02-12T15:16:11+00:00',
                'modified' => $body['official']['modified'],
            ],
        ];

        $this->assertEquals($expected, $body);

        $this->setJsonRequest();
        $this->get('/api/v1/officials/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals($expected, $body);
    }

    public function testEditOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "officials"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'structure_id' => 2,
            'civility' => 'Mme.',
            'lastname' => 'Jeanne',
            'firstname' => 'BLEU',
            'email' => 'j.bleu@test.fr',
        ];
        $this->patch('/api/v1/officials/2', $data);
        $this->assertResponseCode(404);
    }

    public function testEditChangeStructureFail()
    {
        $data = [
            'structure_id' => 2,
            'civility' => 'Mme.',
            'lastname' => 'Jeanne',
            'firstname' => 'BLEU',
            'email' => 'j.bleu@test.fr',
        ];

        $this->setJsonRequest();
        $this->patch('/api/v1/officials/1', $data);
        $this->assertResponseCode(400);

        $body = $this->getResponse();
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $this->assertEquals($expected, $body['errors']);
    }

    public function testPutEditFail()
    {
        $this->expectException(MethodNotAllowedException::class);
        $this->expectExceptionMessage('Method Not Allowed');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'structure_id' => 1,
            'civility' => 'Mme.',
            'lastname' => 'Jeanne',
            'firstname' => 'ROUGE',
            'email' => 'jeanne.rouge@test.fr',
        ];
        $this->put('/api/v1/officials/1', $data);
        $this->assertResponseCode(405);
    }
}
