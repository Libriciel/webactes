<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\MethodNotAllowedException;

/**
 * App\Controller\DposController Test Case
 */
class DposControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Dpos',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/dpos');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(1, count($body['dpos']));
    }

    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/dpos/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(1, count($body));
        $this->assertNotEmpty($body['dpo']['id']);
    }

    /**
     * [testViewOtherStructureFail description]
     *
     * @return [type] [description]
     */
    public function testViewOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "dpos"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->get('/api/v1/dpos/2');
        $this->assertResponseCode(404);
    }

    public function testAdd()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'civility' => 'Mme.',
            'lastname' => 'Anouk',
            'firstname' => 'Vert',
            'email' => 'a.vert@test.fr',
        ];
        $this->post('/api/v1/dpos', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();

        $this->assertNotEmpty($body['id']);

        $this->setJsonRequest();
        $this->get('/api/v1/dpos');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals(2, count($body['dpos']));
    }

    public function testAddOtherStructureFail()
    {
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'structure_id' => 2,
            'civility' => 'Mme.',
            'lastname' => 'Anouk',
            'firstname' => 'Vert',
            'email' => 'a.vert@test.fr',
        ];
        $this->post('/api/v1/dpos', $data);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $this->assertEquals($expected, $body['errors']);
    }

    public function testEditSuccess()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'civility' => 'Mme.',
            'lastname' => 'Anouk',
            'firstname' => 'Vert',
            'email' => 'anouk.vert@test.fr',
        ];
        $this->patch('/api/v1/dpos/1', $data);
        $this->assertResponseCode(200);

        $body = $this->getResponse();

        $expected = ['dpo' => array_merge(
            $data,
            [
                'id' => 1,
                'address' => '836 rue des blancs',
                'address_supplement' => 'Résidence le Sud',
                'post_code' => '34000',
                'city' => 'MONTPELLIER',
                'phone' => '0400000000',
                'created' => $body['dpo']['created'],
                'modified' => $body['dpo']['modified'],
            ]
        )];
        $this->assertEquals($expected, $body);

        $this->setJsonRequest();
        $this->get('/api/v1/dpos/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals($expected, $body);

        $this->setJsonRequest();
        $this->get('/api/v1/dpos/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $expected = ['dpo' => array_merge(
            $data,
            [
                'id' => 1,
                'address' => '836 rue des blancs',
                'address_supplement' => 'Résidence le Sud',
                'post_code' => '34000',
                'city' => 'MONTPELLIER',
                'phone' => '0400000000',
                'created' => $body['dpo']['created'],
                'modified' => $body['dpo']['modified'],
            ]
        )];
        $this->assertEquals($expected, $body);
    }

    /**
     * [testEditOtherStructureFail description]
     *
     * @return [type] [description]
     */
    public function testEditOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "dpos"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $data = [
            'structure_id' => 2,
            'civility' => 'Mme.',
            'lastname' => 'Anouk',
            'firstname' => 'Vert',
            'email' => 'anouk.vert@test.fr',
        ];
        $this->patch('/api/v1/dpos/2', $data);
        $this->assertResponseCode(404);
    }

    /**
     * [testEditChangeStructureFail description]
     *
     * @return [type] [description]
     */
    public function testEditChangeStructureFail()
    {
        $data = [
            'structure_id' => 2,
            'civility' => 'Mme.',
            'lastname' => 'Anouk',
            'firstname' => 'Vert',
            'email' => 'anouk.vert@test.fr',
        ];

        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->patch('/api/v1/dpos/1', $data);
        $this->assertResponseCode(400);

        $body = $this->getResponse();
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $this->assertEquals($expected, $body['errors']);
    }

    public function testEditFailedWithPutMethod()
    {
        $this->expectException(MethodNotAllowedException::class);
        $this->expectExceptionMessage('Method Not Allowed');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();

        $this->put('/api/v1/dpos/1');
        $this->assertResponseCode(405);
    }
}
