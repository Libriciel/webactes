<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\Utility\Hash;

/**
 * App\Controller\SummonsController Test Case
 *
 * @uses \App\Controller\SummonsController
 */
class SummonsControllerTest extends AbstractTestCase
{
    use IntegrationTestTrait;

    public $fixtures = [
        'app.group.All',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/summons?summon_type_id=1&sitting_id=1');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 200], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataProviderIndex
     */
    public function testIndex(?string $params, int $status, array $expected)
    {
        $this->switchUser($this->getUserId('s.blanc'));
        $this->setJsonRequest();

        $this->get('/api/v1/summons' . (string)$params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    public function dataProviderIndex()
    {
        return [
            ['?summon_type_id=1&sitting_id=1', 200, [['id' => 1]]],
            ['?summon_type_id=1&sitting_id=1&format=short', 200, [['id' => 1]]],
            ['?summon_type_id=1&sitting_id=1&format=short', 200, [['id' => 1]]],
            [
                '?summon_type_id=1&sitting_id=1&format=choucroute',
                400,
                [
                    'errors' => [
                        'format' => [
                            'inList' => 'The provided value is invalid, use one of "full", "short"',
                        ],
                    ],

                ],
            ],
        ];
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd(): void
    {
        $this->setJsonRequest();
        $data = [
                'summon_type_id' => 1,
                'sitting_id' => 1,
                'is_sent_individually' => false,
                'attachments_summons' => [
                [
                    'rank' => 1,
                    'attachment' => [
                        'name' => 'convocation',
                    ],
                ],
                [
                    'rank' => 2,
                    'attachment' => [
                        'name' => 'executive_summary',
                    ],
                ],
                ],
        ];

        $this->post('/api/v1/summons', json_encode($data));
        $body = $this->getResponse();
        $this->assertResponseCode(201);
        $this->assertNotEmpty($body['summon']);

        $this->setJsonRequest();
        $this->get('/api/v1/summons?summon_type_id=1&sitting_id=1');
        $body = $this->getResponse();
        $this->assertResponseCode(200);

        $this->assertEquals([1, 2, 3], Hash::extract($body, '0.convocations.{n}.id'));
        $this->assertEquals([4, 5, 6], Hash::extract($body, '1.convocations.{n}.id'));
    }
}
