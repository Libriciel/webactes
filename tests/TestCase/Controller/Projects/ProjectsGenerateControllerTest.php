<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestTrait;
use Laminas\Diactoros\UploadedFile;
use SplFileInfo;

/**
 * App\Controller\ProjectsController Test Case
 */
class ProjectsGenerateControllerTest extends AbstractTestCase
{
    use IntegrationTestTrait;

    /**
     * @var string[]
     */
    public $fixtures = [
        'app.group.All',
    ];

    /**
     * Saved configuration values, to be saved in ::setUp and restored in ::tearDown.
     *
     * @var array
     */
    protected $cheminFilePdf = '';
    protected $mainDocument = '';

    /**
     * Set up the configuration.
     */
    public function setUp(): void
    {
        $this->skipIfNoGenerationConfig();

        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);

        $this->cheminFilePdf = ROOT . '/tests/Fixture/files/pdf/';

        // enable event tracking
        //$this->Project->getEventManager()->setEventList(new EventList());
    }

    public function testAddSuccessWithOutMainDocument()
    {
        $this->switchStructureSettings(1, 'project', 'project_writing', true);

        $files = [
            $this->cheminFilePdf . 'annexe_001.pdf',
            $this->cheminFilePdf . 'annexe_002.pdf',
        ];

        foreach ($files as $file) {
            $this->assertTrue(copy(static::MAIN_DOCUMENT_TEMPLATE_PATH, $file));
        }
        $annexe1 = new SplFileInfo($this->cheminFilePdf . 'annexe_001.pdf');
        $annexe2 = new SplFileInfo($this->cheminFilePdf . 'annexe_002.pdf');

        $this->setJsonRequest();
        $jsonProject = json_encode(
            [
                'theme_id' => 1,
                'name' => 'Réabilitation de la rue du Pirée',
                'stateact_id' => 1,
                'typesact_id' => 1,
                'sittings' => [6, 7],
                'matiere_id' => 1,
                'maindocument' => [
                    'codetype' => '99_AR',
                ],
                'annexes' => [
                    [
                        'istransmissible' => true,
                        'codetype' => '99_SE',
                    ],
                    [
                        'istransmissible' => false,
                        'codetype' => '99_AR',
                    ],
                ],
            ]
        );

        $data = [
            'project' => $jsonProject,
            'annexes' => [
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_001.pdf',
                    $annexe1->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe1->getFilename(),
                    'application/pdf'
                ),
                new UploadedFile(
                    $this->cheminFilePdf . 'annexe_002.pdf',
                    $annexe2->getSize(),
                    UPLOAD_ERR_OK,
                    $annexe2->getFilename(),
                    'application/pdf'
                ),
            ],
        ];
        $this->post('/api/v1/projects', $data);
        $this->assertResponseCode(201);

        $body = $this->getResponse();
        $this->assertNotEmpty($body['id']);
    }
}
