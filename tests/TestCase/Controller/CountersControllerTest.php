<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * App\Controller\CountersController Test Case
 */
class CountersControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Sequences',
        'app.Counters',
        'app.GenerateTemplates',
        'app.Natures',
        'app.Typesacts',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/counters');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 403], // Administrateur Fonctionnel
            ['j.orange', 403], // Secrétariat général
            ['r.violet', 403], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataProviderTestIndex
     */
    public function testIndex(string $params, int $status, array $expected): void
    {
        $this->switchUser($this->getUserId('s.blanc'));
        $this->setJsonRequest();

        $this->get('/api/v1/counters' . $params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    public function dataProviderTestIndex(): array
    {
        $counters = [
            ['id' => 6, 'sequence' => ['id' => 6]],
            ['id' => 7, 'sequence' => ['id' => 6]],
            ['id' => 5, 'sequence' => ['id' => 2]],
            ['id' => 10, 'sequence' => ['id' => 8]],
            ['id' => 11, 'sequence' => ['id' => 8]],
            ['id' => 8, 'sequence' => ['id' => 7]],
            ['id' => 9, 'sequence' => ['id' => 7]],
            ['id' => 2, 'sequence' => ['id' => 2]],
            ['id' => 1, 'sequence' => ['id' => 4]],
        ];

        return [
            ['', 200, ['counters' => $counters, 'pagination' => ['count' => 11, 'perPage' => 10]]],
            ['?paginate=false', 200, ['counters' => $counters, 'pagination' => null]],
            ['?name=partage', 200, ['pagination' => ['count' => 6]]],
            [
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "direction", "limit", "name", "offset", "page", "paginate", "sort"',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->setJsonRequest();
        $this->get('/api/v1/counters/1');
        $this->assertResponseCode(200);
        $body = $this->getResponse();
        $this->assertEquals('#AAAA#_#0000#', $body['counter']['counter_def']);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->setJsonRequest();

        $data = [
            'structure_id' => 1,
            'name' => 'Compteurs des nouvelles délibérations',
            'comment' => 'Un autre système de compteur',
            'counter_def' => 'DELIB_#p#_#00#_#p#',
            'sequence_id' => 4,
            'reinit_def' => '#AAAA#',
            'created' => 1606513539,
            'modified' => 1606513539,
        ];

        $this->post('/api/v1/counters', $data);
        $this->assertResponseCode(201);
        $body = $this->getResponse();
        $this->assertNotEmpty($body['counter']['id']);
        $id = $body['counter']['id'];

        $this->setJsonRequest();
        $this->get('/api/v1/counters/' . $id);
        $body = $this->getResponse();
        $this->assertEquals(14, $body['counter']['id']);
        $this->assertEquals('DELIB_#p#_#00#_#p#', $body['counter']['counter_def']);
        $this->assertEquals('#AAAA#', $body['counter']['reinit_def']);
    }

    /**
     * @return void
     */
    public function testAddWrongCounterDef()
    {
        // Pourquoi ça ne chope pas le L'exception
//        $this->expectException(Exception::class);
//        $this->expectExceptionMessage('La définition du compteur ne respecte pas le protocole @cte.');

        $this->setJsonRequest();

        $data = [
            'structure_id' => 1,
            'name' => 'Compteurs des nouvelles délibérations',
            'comment' => 'Un autre système de compteur',
            'counter_def' => '_delib_#0000#_*)',
            'sequence_id' => 4,
            'reinit_def' => '#AAAA#',
            'created' => 1606513539,
            'modified' => 1606513539,
        ];

        $this->post('/api/v1/counters', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditPut()
    {
        $this->setJsonRequest();

        $data = [
            'counter_def' => 'DELIB_#00#_#AA#_#p#',
        ];

        $this->put('api/v1/counters/2', $data);
        $this->assertResponseCode(200);

        $this->setJsonRequest();
        $this->get('api/v1/counters/2');
        $body = $this->getResponse();
        $this->assertEquals('DELIB_#00#_#AA#_#p#', $body['counter']['counter_def']);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditPutSequence()
    {
        $this->setJsonRequest();

        $data = [
            'id' => 2,
            'sequence' => [
                'id' => 2,
                'structure_id' => 1,
                'name' => 'Séquence_contrats',
                'comment' => 'Pour les contrats uniquement',
                'sequence_num' => 1,
                'created' => 1607597401,
                'modified' => 1607597401,
                'starting_date' => '2020-01-01',
                'expiry_date' => '2020-12-31',
            ],
            'sequence_id' => 4,
        ];

        $this->put('api/v1/counters/2', $data);
        $this->assertResponseCode(200);

        $this->setJsonRequest();
        $this->get('api/v1/counters/2');
        $body = $this->getResponse();
        $this->assertEquals(4, $body['counter']['sequence_id']);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditPatch()
    {
        $this->setJsonRequest();

        $data = [
            'name' => 'Un autre nom',
        ];

        $this->patch('api/v1/counters/3', $data);
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('api/v1/counters/3');
        $body = $this->getResponse();
        $this->assertEquals('Un autre nom', $body['counter']['name']);
        $this->assertEquals('Le compteur pour les arrêtés', $body['counter']['comment']);
    }

    /**
     * Test edit patch other structure fails
     *
     * @return void
     */
    public function testEditPatchOtherStructureFails()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
            'comment' => 'Changement de nom de séquence par une autre structure',
        ];

        $this->patch('/api/v1/sequences/3', $data);
        $this->assertResponseCode(400);
        $body = $this->getResponse();
        $expected = [
            'structure_id' => [
                'wrong_structure_id',
            ],
        ];
        $this->assertEquals($expected, $body['errors']);
    }

    /**
     * Test view other structure fails
     *
     * @return [type] [description]
     */
    public function testViewOtherStructureFails()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "counters"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->get('/api/v1/counters/12');
        $this->assertResponseError('404');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDeleteUsedByTypeActFails()
    {
        $this->setJsonRequest();
        $this->delete('api/v1/counters/1');
        $this->assertResponseCode(400);
    }

    public function testDelete()
    {
        $this->setJsonRequest();
        $this->delete('api/v1/counters/5');
        $this->assertResponseCode(204);

        $this->setJsonRequest();
        $this->get('api/v1/counters');
        $body = $this->getResponse();
        $this->assertEquals(10, count($body['counters']));
    }

    /**
     * @throws \PHPUnit\Exception
     */
    public function testDeleteNotAdminFails()
    {
        $this->switchUser(4);
        $this->setJsonRequest();
        $this->delete('/api/v1/counters/2');
        $this->assertResponseCode(403);
    }
}
