<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\Mock\FlowableWrapperMock;
use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;

class AbstractProjectsControllerTestCase extends AbstractTestCase
{
    use ProjectsAddFixtureTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.Pastell',
        'app.group.Container',
        'app.group.Project',
        'app.group.File',
        'app.group.TypeAct',
        'app.group.Sitting',
        'app.group.Actor',
        'app.Organizations',
        'app.Structures',
        'app.Users',
        'app.Stateacts',
        'app.UsersInstances',
        'app.Notifications',
        'app.DraftTemplateTypes',
        'app.Counters',
        'app.Sequences',
        'app.Generationerrors',
        'app.ConnectorTypes',
        'app.Workflows',
        'app.Natures',
        'app.DraftTemplates',
        'app.Actors',
        'app.GenerateTemplates',
        'app.Templates',
        'app.Themes',
        'app.Typespiecesjointes',
        'app.Matieres',
        'app.Systemlogs',
        'app.NotificationsUsers',
        'app.Connecteurs',
        'app.Typesacts',
        'app.Projects',
        'app.Acts',
        'app.Typesittings',
        'app.DraftTemplatesTypesacts',
        'app.Containers',
        'app.Sittings',
        'app.ContainersStateacts',
        'app.ContainersSittings',
        'app.Histories',
        'app.ActorsProjectsSittings',
        'app.Maindocuments',
        'app.StructuresUsers',
        'app.Roles',
        'app.RolesUsers',
    ];

    /**
     * Saved configuration values, to be saved in ::setUp and restored in ::tearDown.
     *
     * @var array
     */
    protected $savedConfiguration = [];
    protected $cheminFilePdf = '';
    protected $cheminFileOdt = '';
    protected $cheminFileLogo = '';
    protected $mainDocumentTemplate = '';
    protected $mainDocument = '';
    protected $pdfDocument = '';
    /**
     * @var string
     */
    protected $mainDocumentOdt;

    /**
     * Set up the configuration.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        Configure::write('KEYCLOAK_DEV_MODE', true);
        Configure::write('Flowable.wrapper_mock', '\App\Test\Mock\FlowableWrapperMock');

        FlowableWrapperMock::reset();

        $this->savedConfiguration = [
            'annexeAcceptedTypes' => Configure::read('annexeAcceptedTypes'),
            'mainDocumentAcceptedTypes' => Configure::read('mainDocumentAcceptedTypes'),
            'mainDocumentAcceptedTypesForSigned' => Configure::read('mainDocumentAcceptedTypesForSigned'),
        ];

        $this->cheminFilePdf = ROOT . '/tests/Fixture/files/pdf/';
        $this->cheminFileOdt = ROOT . '/tests/Fixture/files/odt/';
        $this->cheminFileLogo = ROOT . '/tests/Fixture/files/logo/';
        $this->mainDocumentTemplate = $this->cheminFilePdf . 'mainDocumentTemplate.pdf';
        $this->mainDocument = TMP . 'tests' . DS . 'mainDocument.pdf';
        $this->pdfDocument = TMP . 'tests' . DS . 'pdfDocument.pdf';
        $this->mainDocumentOdt = TMP . 'tests' . DS . 'mainDocument.odt';

        $this->assertTrue(copy($this->mainDocumentTemplate, $this->mainDocument));

        Configure::write('annexeAcceptedTypes', [
            'application/pdf',
            'image/png',
            'image/jpeg',
            'application/vnd.oasis.opendocument.text',
            'application/xml',
            'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-powerpoint',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'application/vnd.oasis.opendocument.spreadsheet',
            'application/vnd.oasis.opendocument.presentation',
        ]);

        Configure::write(
            'mainDocumentAcceptedTypes',
            [
                'application/pdf',
                'application/msword',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/vnd.oasis.opendocument.text',
            ]
        );

        Configure::write(
            'mainDocumentAcceptedTypesForSigned',
            [
                'application/pdf',
            ]
        );

        $this->Projects = $this->fetchTable('Projects');

        // enable event tracking
        //$this->Project->getEventManager()->setEventList(new EventList());
    }

    /**
     * Restore the configuration.
     */
    public function tearDown(): void
    {
        parent::tearDown();

        foreach ($this->savedConfiguration as $key => $value) {
            Configure::write($key, $value);
        }
    }
}
