<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;

/**
 * App\Controller\GenerateTemplateTypesController Test Case
 *
 * @uses \App\Controller\GenerateTemplateTypesController
 */
class GenerateTemplateTypesControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.GenerateTemplateTypes',
        'app.GenerateTemplates',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex(): void
    {
        $this->setJsonRequest();
        $this->get('/api/v1/generate-template-types');
        $body = $this->getResponse();
        $this->assertResponseCode(200);

        $this->assertEquals(6, count($body['generateTemplateTypes']));
        $this->assertEquals('Acte', $body['generateTemplateTypes'][0]['name']);
    }
}
