<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;

/**
 * App\Controller\NotificationsUsersController Test Case
 */
class NotificationsUsersControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.Notifications',
        'app.NotificationsUsers',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->setJsonRequest();
        $this->get('api/v1/notifications');
        $body = $this->getResponse();
        $this->assertResponseCode(200);
    }

    /**
     * @throws Exception
     */
    public function testActivate()
    {
        $this->setJsonRequest();
        $this->post('api/v1/notifications/1/activate');
        $body = $this->getResponse();
        $this->assertTrue($body['success']);
        $this->assertSame($body['notification']['id'], 1);
        $this->assertSame($body['notification']['active'], true);
        $this->assertResponseCode(201);
    }

    /**
     * @throws Exception
     */
    public function testDeactivate()
    {
        $this->setJsonRequest();
        $this->post('api/v1/notifications/1/deactivate');
        $body = $this->getResponse();
        $this->assertTrue($body['success']);
        $this->assertSame($body['notification']['id'], 1);
        $this->assertSame($body['notification']['active'], false);
        $this->assertResponseCode(201);
    }
}
