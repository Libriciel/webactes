<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\MethodNotAllowedException;
use Cake\Utility\Hash;

/**
 * App\Controller\TypesactsController Test Case
 */
class TypesactsControllerTest extends AbstractTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.Auth',
        'app.group.TypeAct',
        'app.DraftTemplateTypes',
        'app.GenerateTemplates',
        'app.Natures',
        'app.DraftTemplates',
        'app.Systemlogs',
        'app.Typesacts',
        'app.Typesittings',
        'app.DraftTemplatesTypesacts',
        'app.TypesactsTypesittings',
        'app.group.Container',
        'app.Projects',
        'app.Containers',
        'app.group.Project',
    ];

    private $records = [
        'typesacts' => [
            [
                'id' => 1,
                'nature_id' => 4,
                'name' => 'Autre',
                'active' => true,
                'istdt' => false,
                'isdeliberating' => false,
                //'is_deletable' => false,
                /* 'is_updatable' => false,
                 'is_linked' => true,*/
                'nature' => [
                    'id' => 4,
                    'libelle' => 'Autres',
                    'typeabrege' => 'AU',
                ],
                'isdefault' => false,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => false,
                        'data' => 'Le type d\'acte ne peut pas être supprimé.',
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 1,
                'draftTemplates' => [
                    [
                        'id' => 1,
                        'name' => 'Gabarit de projet de délibération',
                        'draft_template_type_id' => 1,
                    ],
                    [
                        'id' => 2,
                        'name' => 'Gabarit d\'acte de délibération',
                        'draft_template_type_id' => 2,
                    ],
                ],
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
            ],
            [

                'id' => 2,
                'nature_id' => 2,
                'name' => 'Arrêté réglementaire',
                'active' => true,
                'istdt' => true,
                'isdeliberating' => false,
                //    'is_deletable' => true,
                /* 'is_updatable' => true,
                 'is_linked' => false,*/
                'nature' => [
                    'id' => 2,
                    'libelle' => 'Actes réglementaires',
                    'typeabrege' => 'AR',
                ],
                'draftTemplates' => [],
                'isdefault' => false,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 2,
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
            ],
            [
                'id' => 3,
                'nature_id' => 3,
                'name' => 'Contrat et convention',
                'active' => true,
                'istdt' => true,
                'isdeliberating' => false,
                //   'is_deletable' => true,
                /* 'is_updatable' => true,
                 'is_linked' => false,*/
                'nature' => [
                    'id' => 3,
                    'libelle' => 'Contrats,conventions et avenants',
                    'typeabrege' => 'CC',

                ],
                'isdefault' => false,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 3,
                'draftTemplates' => [],
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
            ],
            [
                'id' => 4,
                'nature_id' => 1,
                'name' => 'Délibération',
                'active' => true,
                'istdt' => true,
                'isdeliberating' => true,
                //  'is_deletable' => false,
                /*  'is_updatable' => false,
                  'is_linked' => true,*/
                'nature' => [
                    'id' => 1,
                    'libelle' => 'Délibérations',
                    'typeabrege' => 'DE',
                ],
                'isdefault' => true,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => false,
                        'data' => 'Le type d\'acte ne peut pas être supprimé.',
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 1,
                'draftTemplates' => [],
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
            ],
        ],
        'pagination' => [
            'page' => 1,
            'count' => 4,
            'perPage' => PAGINATION_LIMIT,
        ],
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();
        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    /**
     * @dataProvider dataProviderAccessIndex
     */
    public function testAccessIndex(string $user, int $expected): void
    {
        $this->switchUser($this->getUserId($user));
        $this->setJsonRequest();

        $this->get('/api/v1/typesacts');
        $this->assertResponseCode($expected);
    }

    public function dataProviderAccessIndex()
    {
        return [
            ['s.blanc', 200], // Administrateur
            ['m.vert', 200], // Administrateur Fonctionnel
            ['j.orange', 200], // Secrétariat général
            ['r.violet', 200], // Rédacteur / Valideur
            ['r.cyan', 200], // Super Administrateur
        ];
    }

    /**
     * @dataProvider dataProviderTestIndex
     */
    public function testIndex(string $params, int $status, array $expected): void
    {
        $this->switchUser($this->getUserId('s.blanc'));
        $this->setJsonRequest();

        $this->get('/api/v1/typesacts' . $params);
        $this->assertResponseCode($status);

        $body = $this->getResponse();
        $this->assertArraySubset($expected, $body);
    }

    public function dataProviderTestIndex(): array
    {
        $typesacts = [
            ['id' => 2, 'draftTemplates' => [['id' => null]]],
            ['id' => 1, 'draftTemplates' => [['id' => 1], ['id' => 2]]],
            ['id' => 3, 'draftTemplates' => [['id' => null]]],
            ['id' => 4, 'draftTemplates' => [['id' => null]]],
        ];

        return [
            ['', 200, ['typesacts' => $typesacts, 'pagination' => ['count' => 4, 'perPage' => 10]]],
            ['?paginate=false', 200, ['typesacts' => $typesacts, 'pagination' => null]],
            ['?name=Autre', 200, ['pagination' => ['count' => 1]]],
            ['?active=false', 200, ['pagination' => ['count' => 0]]],
            ['?isdeliberating=true', 200, ['typesacts' => array_slice($typesacts, 3, 1), 'pagination' => ['count' => 1]]],
            ['?isdeliberating=false', 200, ['typesacts' => array_slice($typesacts, 0, 3), 'pagination' => ['count' => 3]]],
            [
                '?foo=bar',
                400,
                [
                    'errors' => [
                        'foo' => [
                            '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "active", "direction", "isdeliberating", "limit", "name", "offset", "page", "paginate", "sort"',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'nature_id' => 1,
            'name' => 'added',
            'istdt' => true,
            'isdeliberating' => true,
            'counter_id' => 1,
            'draft_templates' => [
                ['id' => 1],
                ['id' => 2],
            ],
            'generate_template_act_id' => 1,
            'generate_template_project_id' => 1,
        ];
        $this->post('/api/v1/typesacts', $data);
        $this->assertResponseCode(201);

        $actual = $this->getResponse();
        $this->assertEquals(['id' => 9], $actual);

        $this->setJsonRequest();
        $this->get('/api/v1/typesacts');
        $this->assertResponseCode(200);
        $actual = $this->getResponse();

        $expected = [
            9 => 'added',
            2 => 'Arrêté réglementaire',
            1 => 'Autre',
            3 => 'Contrat et convention',
            4 => 'Délibération',
        ];
        $this->assertEquals($expected, Hash::combine($actual, 'typesacts.{n}.id', 'typesacts.{n}.name'));

        $expected = [
            'id' => 9,
            'nature_id' => 1,
            'name' => 'added',
            'active' => false,
            'istdt' => true,
            'isdeliberating' => true,
            'nature' => [
                'id' => 1,
                'libelle' => 'Délibérations',
                'typeabrege' => 'DE',
            ],
            'draftTemplates' => [
                [
                    'id' => 1,
                    'name' => 'Gabarit de projet de délibération',
                    'draft_template_type_id' => 1,
                ],
                [
                    'id' => 2,
                    'name' => 'Gabarit d\'acte de délibération',
                    'draft_template_type_id' => 2,
                ],
            ],
            'isdefault' => false,
            'availableActions' => [
                'can_be_edited' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deleted' => [
                    'value' => true,
                    'data' => null,
                ],
                'can_be_deactivate' => [
                    'value' => true,
                    'data' => null,
                ],
            ],
            'counter_id' => 1,
            'generate_template_project_id' => 1,
            'generate_template_act_id' => 1,
        ];
        $this->assertEquals($expected, $actual['typesacts'][0]);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddFailedRequiredGenerateAct()
    {
        $this->switchUser(6);

        $this->setJsonRequest();
        $data = [
            'structure_id' => 2,
            'nature_id' => 4,
            'name' => 'added',
            'istdt' => true,
            'isdeliberating' => true,
            'counter_id' => 12,
        ];
        $this->patch('/api/v1/typesacts/5', $data);
        $actual = $this->getResponse();
        $this->assertResponseCode(400);

        $this->assertEquals('Le modèle de génération de l\'acte est obligatoire', $actual['errors']['generate_template_act_id']['_required']);
    }

    /**
     * @return void
     */
    public function testAddWithAlreadyExistingNameFails()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'nature_id' => 1,
            'name' => 'Contrat et convention',
            'istdt' => true,
            'isdeliberating' => true,
            'counter_id' => 1,
            'draft_templates' => [
                ['id' => 1],
                ['id' => 2],
            ],
        ];
        $this->post('/api/v1/typesacts', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddFailedWithMissingOrEmptyRequiredFields()
    {
        $this->setJsonRequest();
        $data = [
            'name' => '',
        ];
        $this->post('/api/v1/typesacts', $data);
        $this->assertResponseCode(400);

        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'istdt' => [
                    '_required' => 'This field is required',
                ],
                'nature_id' => [
                    '_required' => 'This field is required',
                ],
                'name' => [
                    '_empty' => 'This field cannot be left empty',
                ],
                'isdeliberating' => [
                    '_required' => 'This field is required',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddFailedWithForeignKeyNotFound()
    {
        $this->setJsonRequest();
        $data = [
            'structure_id' => 1,
            'nature_id' => 666,
            'name' => 'test',
            'istdt' => false,
            'isdeliberating' => false,
            'counter_id' => 1,
            'draft_templates' => ['id' => 1],
            'generate_template_act_id' => 1,
            'generate_template_project_id' => 1,
        ];
        $this->post('/api/v1/typesacts', $data);
        $this->assertResponseCode(400);

        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'nature_id' => [
                    '_existsIn' => 'This value does not exist',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditSuccessWithLinkedRecords()
    {
        $data = [
            'structure_id' => 1,
            'nature_id' => 4,
            'name' => 'newName',
            'istdt' => true,
            'active' => false,
            'counter_id' => 1,
            'draft_templates' => [
                ['id' => 1],
                ['id' => 2],
            ],
            'generate_template_project_id' => 1,
            'generate_template_act_id' => 2,
        ];

        $this->setJsonRequest();
        $this->patch('/api/v1/typesacts/1', $data);

        $this->assertResponseCode(200);

        $actual = $this->getResponse();
        $expected = [
            'typesact' => [
                'id' => 1,
                'structure_id' => 1,
                'nature_id' => 4,
                'name' => 'newName',
                'created' => $actual['typesact']['created'],
                'modified' => $actual['typesact']['modified'],
                'istdt' => true,
                'isdeliberating' => false,
                'active' => false,
                'isdefault' => false,
                // 'is_deletable' => false,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => false,
                        'data' => 'Le type d\'acte ne peut pas être supprimé.',
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 1,
                'draftTemplates' => [
                    [
                        'id' => 1,
                        'name' => 'Gabarit de projet de délibération',
                        'draft_template_type_id' => 1,
                        'structure_id' => 1,
                        'created' => $actual['typesact']['draftTemplates'][0]['created'],
                        'modified' => $actual['typesact']['draftTemplates'][0]['modified'],
                    ],
                    [
                        'id' => 2,
                        'name' => 'Gabarit d\'acte de délibération',
                        'draft_template_type_id' => 2,
                        'structure_id' => 1,
                        'created' => $actual['typesact']['draftTemplates'][1]['created'],
                        'modified' => $actual['typesact']['draftTemplates'][1]['modified'],
                    ],
                ],
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditSuccessWithoutLinkedRecords()
    {
        $data = [
            'structure_id' => 1,
            'nature_id' => 2,
            'name' => 'newName',
            'istdt' => true,
            'active' => false,
            'counter_id' => 1,
            'draft_templates' => [
                ['id' => 1],
            ],
            'generate_template_act_id' => 2,
            'generate_template_project_id' => 1,
        ];

        $this->setJsonRequest();
        $this->patch('/api/v1/typesacts/2', $data);
        $this->assertResponseCode(200);

        $actual = $this->getResponse();

        $expected = [
            'typesact' => [
                'id' => 2,
                'structure_id' => 1,
                'nature_id' => 2,
                'name' => 'newName',
                'created' => $actual['typesact']['created'],
                'modified' => $actual['typesact']['modified'],
                'istdt' => true,
                'isdeliberating' => false,
                'active' => false,
                'isdefault' => false,
                // 'is_deletable' => true,
                'availableActions' => [
                    'can_be_edited' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deleted' => [
                        'value' => true,
                        'data' => null,
                    ],
                    'can_be_deactivate' => [
                        'value' => true,
                        'data' => null,
                    ],
                ],
                'counter_id' => 1,
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
                'draftTemplates' => [
                    [
                        'id' => 1,
                        'structure_id' => 1,
                        'draft_template_type_id' => 1,
                        'name' => 'Gabarit de projet de délibération',
                        'created' => $actual['typesact']['draftTemplates'][0]['created'],
                        'modified' => null,
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return [type] [description]
     */
    public function testEditOtherStructureFail()
    {
        $data = [
            'structure_id' => 2,
            'nature_id' => 2,
            'name' => 'newName',
            'istdt' => true,
            'active' => false,
        ];

        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "typesacts"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->patch('/api/v1/typesacts/5', $data);
        $this->assertResponseCode(404);
    }

    /**
     * Test edit structure fails
     */
    public function testEditChangeStructureFail()
    {
        $data = [
            'structure_id' => 2,
            'nature_id' => 2,
            'name' => 'newName',
            'istdt' => true,
            'active' => false,
        ];

        $this->setJsonRequest();
        $this->patch('/api/v1/typesacts/1', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test edit with already used name fails
     */
    public function testEditAlreadyExistingNameFails()
    {
        $data = [
            'structure_id' => 1,
            'nature_id' => 4,
            'name' => 'Contrat et convention',
            'istdt' => true,
            'active' => false,
        ];

        $this->setJsonRequest();
        $this->patch('/api/v1/typesacts/1', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test edit method
     */
    public function testEditFailedWithPutMethod()
    {
        $data = [
            'name' => 'newName',
        ];

        $this->expectException(MethodNotAllowedException::class);
        $this->expectExceptionMessage('Method Not Allowed');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->put('/api/v1/typesacts/1', $data);
        $this->assertResponseCode(405);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditFailedWithMissingRequiredField()
    {
        $data = [
            'name' => null,
        ];

        $this->setJsonRequest();
        $this->patch('/api/v1/typesacts/1', $data);
        $this->assertResponseCode(400);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditFailedWithForeignKeyNotFound()
    {
        $data = [
            'nature_id' => 666,
            'name' => 'newName',
            'draft_templates' => [
                ['id' => 1],
            ],
            'generate_template_act_id' => 2,
            'generate_template_project_id' => 1,
        ];

        $this->setJsonRequest();
        $this->patch('/api/v1/typesacts/1', $data);
        $this->assertResponseCode(400);

        $actual = $this->getResponse();
        $expected = [
            'errors' => [
                'nature_id' => ['_existsIn' => 'This value does not exist'],
            ],
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/typesacts/2');
        $this->assertResponseCode(204);
    }

    /**
     * @return void
     */
    public function testDeleteOtherStructureFail()
    {
        $this->expectException(RecordNotFoundException::class);
        $this->expectExceptionMessage('Record not found in table "typesacts"');
        $this->setJsonRequest();
        $this->disableErrorHandlerMiddleware();
        $this->delete('/api/v1/typesacts/5');
        $this->assertResponseCode(404);
    }

    public function testDeleteFailedWhenIsUsed()
    {
        $this->setJsonRequest();
        $this->delete('/api/v1/typesacts/1');
        $this->assertResponseCode(403);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditWithoutInitialDraftTemplates()
    {
        $data = [
            'nature_id' => 4,
            'name' => 'newName',
            'draft_templates' => [
                ['id' => 1],
            ],
            'generate_template_act_id' => 2,
            'generate_template_project_id' => 1,
        ];

        $this->setJsonRequest();
        $this->patch('/api/v1/typesacts/2', $data);
        $this->assertResponseCode(200);
    }
}
