<?php
declare(strict_types=1);

namespace App\Test\TestCase;

use App\TestSuite\Fixture\FixtureGroupTrait;
use App\TestSuite\UsersTrait;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Cake\Utility\Hash;
use RuntimeException;
use function App\safe_shell_exec;

/**
 * Class AbstractTestCase
 */
abstract class AbstractTestCase extends TestCase
{
    use UsersTrait;
    use FixtureGroupTrait;
    use IntegrationTestTrait;

    public const MAIN_DOCUMENT_TEMPLATE_PATH = ROOT . '/tests/Fixture/files/pdf/mainDocumentTemplate.pdf';

    public const PDF_TEMPLATE_PATH = ROOT . '/tests/Fixture/files/pdf/annexe_2.pdf';

    public const PDF_PROTEGE_MDP_123456 = ROOT . '/tests/Fixture/files/pdf/protege_mdp_123456.pdf';

    public const LOGO_TEMPLATE_PATH = ROOT . '/tests/Fixture/files/logo/logoTemplate.png';

    public const LOGO_TEMPLATE_SIZE = 7551;

    public const REGEXP_UUID = '[a-f0-9]{8}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{12}';

    /**
     * Copie le fichier source vers un dossier temporaire (par exemple pour de l'upload de fichiers) et retourne le
     * chemin du fichier copié.
     *
     * @param string $source Le chemin complet vers le fichier original
     * @param string $basename Le nom du fichier copié
     * @return string
     */
    protected function createTempFile(string $source, string $basename)
    {
        $tempFile = TMP . 'tests' . DS . $basename;
        $this->assertTrue(copy($source, $tempFile));

        return $tempFile;
    }

    /**
     * Lance une exception lorsqu'une entité contient des erreurs suite à la tentative d'insertion de données via la
     * méthode insert.
     *
     * @param string $tableName Le nom de la table CakePHP
     * @param array $entities Les entités à sauvegarder ou que l'on a essayé de sauvegarder
     */
    protected function throwInsertExceptionIfNeeded(string $tableName, array $entities)
    {
        foreach ($entities as $idx => $entity) {
            if ($entity->hasErrors() === true) {
                $message = sprintf(
                    'Entity from table %s with index %d has errors: %s',
                    $tableName,
                    $idx,
                    var_export($entity->getErrors(), true)
                );
                throw new RuntimeException($message);
            }
        }
    }

    /**
     * Fonction d'enregistrement en masse de données ne pas surcharger les données des fixtures.
     *
     * @param string $tableName Le nom de la table CakePHP
     * @param array $records Les enregistrements à effectuer (utilisation de la méthode saveMany)
     * @param array $options Les options à passer à la méthode saveMany
     * @return \Cake\Datasource\EntityInterface[]|\Cake\Datasource\ResultSetInterface|false|iterable
     * @throws \Exception
     */
    protected function insert(string $tableName, array $records, array $options = [])
    {
        $table = $this->fetchTable($tableName);

        $entities = $table->newEntities($records);
        $this->throwInsertExceptionIfNeeded($tableName, $entities);

        $result = $table->saveMany($entities, $options);
        if ($result === false) {
            $this->throwInsertExceptionIfNeeded($tableName, $entities);
        }

        return $result;
    }

    /**
     * Marque le test comme "skipped" si la constante "TEST_GENERATION" n'est pas à true.
     */
    protected function skipIfNoGenerationConfig()
    {
        if (!TEST_GENERATION) {
            $this->markTestSkipped('The GENERATION config is not available.');
        }
    }

    /**
     * Marque le test comme "skipped" si la constante "TEST_KEYCLOAK" n'est pas à true.
     */
    protected function skipIfNoKeyCloakConfig()
    {
        if (!TEST_KEYCLOAK) {
            $this->markTestSkipped('The KEYCLOAK config is not available.');
        }
    }

    /**
     * Marque le test comme "skipped" si la constante "TEST_SOCLE" n'est pas à true.
     */
    protected function skipIfNoSocleConfig()
    {
        if (!TEST_SOCLE) {
            $this->markTestSkipped('The SOCLE config is not available.');
        }
    }

    /**
     * Création d'un dossier temporaire et retour du chemin vers celui-ci.
     *
     * @return string
     */
    public function tempdirnam()
    {
        return trim(safe_shell_exec('mktemp -d'));
    }

    /**
     * Vérifie que l'array attendue soit bien un sous-ensemble de l'array obtenue.
     *
     * @param array $expected L'array attendue
     * @param array $array L'array obtenue
     * @return void
     */
    protected function assertArraySubset(array $expected, array $array): void
    {
        $flat = Hash::flatten($expected);
        $actual = [];
        foreach (array_keys($flat) as $path) {
            $actual = Hash::insert($actual, $path, Hash::get($array, $path));
        }

        $this->assertEquals($expected, $actual, sprintf("Array was:\n%s", print_r($array, true)));
    }
}
