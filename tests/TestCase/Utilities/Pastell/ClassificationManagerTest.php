<?php
declare(strict_types=1);

namespace App\Test\TestCase\Utilities\Pastell;

use App\Utilities\Pastell\ClassificationManager;
use PHPUnit\Framework\TestCase;

class ClassificationManagerTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testNominalCase(): void
    {
        $result = (new ClassificationManager())
            ->getClassificationPastell(
                file_get_contents(__DIR__ . '/fixtures/classification_cas_nominal.xml')
            );
        self::assertJsonStringEqualsJsonFile(
            __DIR__ . '/fixtures/cas_nominal.json',
            json_encode($result)
        );
    }

    public function testWindowsEncoding(): void
    {
        $result = (new ClassificationManager())
            ->getClassificationPastell(
                file_get_contents(__DIR__ . '/fixtures/classification-windows-1252.xml')
            );
        self::assertJsonStringEqualsJsonFile(
            __DIR__ . '/fixtures/cas_nominal.json',
            json_encode($result)
        );
    }
}
