<?php
declare(strict_types=1);

namespace App\Test\TestCase\Utilities;

use App\Test\TestCase\AbstractTestCase;
use App\Utilities\Common\PdfUtils;
use Cake\Core\Configure;
use RuntimeException;

class PdfUtilsTest extends AbstractTestCase
{
    protected ?string $tempdirnam = null;

    public function tearDown(): void
    {
        if (empty($this->tempdirnam) === false) {
            foreach (glob("{$this->tempdirnam}/*") as $file) {
                unlink($file);
            }
            rmdir($this->tempdirnam);
            $this->tempdirnam = null;
        }

        parent::tearDown();
    }

    public function testGetPageCountWithSuccess()
    {
        $this->assertEquals(2, PdfUtils::getPageCount(static::PDF_TEMPLATE_PATH));
    }

    /**
     * @dataProvider dataTestGetPageCountWithError
     */
    public function testGetPageCountWithError($executable, $path, $message = 'Erreur lors de l\'extraction du nombre de pages du PDF')
    {
        Configure::write('PDFINFO_EXEC', $executable);
        $this->expectException(RuntimeException::class);
        $regexp = '/^' . preg_quote($message, '/') . '$/';
        $this->expectExceptionMessageMatches($regexp);
        PdfUtils::getPageCount($path);
    }

    public function dataTestGetPageCountWithError()
    {
        return [
            [
                Configure::read('PDFINFO_EXEC'),
                __FILE__,
                'Erreur lors de l\'extraction du nombre de pages du PDF (le fichier ne semble pas être un fichier PDF)',
            ],
            ['', static::PDF_TEMPLATE_PATH],
            ['/dev/null', static::PDF_TEMPLATE_PATH],
            [
                Configure::read('PDFINFO_EXEC'),
                static::PDF_PROTEGE_MDP_123456,
                'Erreur lors de l\'extraction du nombre de pages du PDF (le fichier est protégé par mot de passe)',
            ],
        ];
    }
}
