<?php
declare(strict_types=1);

namespace App\Test\TestCase\Utilities;

use App\Model\Entity\Auth;
use App\Test\TestCase\AbstractTestCase;
use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestTrait;

/**
 * Petite doc d'info de configuration de keycloak
 *
 * Pour pouvoir valider un token un client (/token/introspect ne doit pas etre en public.
 * il est necessaire que sa connection s'effectue avec un client secret
 *
 * Pour pouvoir lister les utilisateur il vous faut un role du client realm management ou alors creer un role composite
 * qui le contient et bien sur l'ajouter au client (pour eviter d'utiliser le client realm management.
 *
 * Pour les test c'est le meme client qui est utilisé pour le login est le reste.
 * Dans la réalité il faut bien sur un client dedier pour les operation d'admin qui ne soit pas accessible direcement
 * depuis l'exterieur (voir qui n'autorise meme pas de se logger dessus (bearer only)
 * Pour se faire il est nessessaire de recreer un object keycloak avec les bonnes infos !
 *
 * get the client secret
 * requests.get("https://mylink.com/auth/admin/realms/{myrealm}/clients/{myclientid}/client-secret", data=data, headers= {"Authorization": "Bearer " + token.get('access_token'), "Content-Type": "application/json"})
 */
class KeycloakTest extends AbstractTestCase
{
    use IntegrationTestTrait;

    public $fixtures = [
        'app.group.All',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->skipIfNoKeyCloakConfig();

        $this->setDefaultSession();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testLogin()
    {
        $auth = new Auth();
        $token = $auth->getToken(KEYCLOAK_TEST_USER, KEYCLOAK_TEST_PASSWORD);
        $this->assertNotEmpty($token);
        $res = $auth->verifyToken($token);

        $this->assertTrue($res['success']);
        $this->assertNotEmpty($res['data']['email']);
    }

    public function testMiddlewareSuccess()
    {
        // generate token
        $auth = new Auth();
        $token = $auth->getToken(KEYCLOAK_TEST_USER, KEYCLOAK_TEST_PASSWORD);

        Configure::write('KEYCLOAK_DEV_MODE', false);

        $this->configRequest(
            ['headers' => [
            'Accept' => 'application/json',
            'Authorization' => $token,
            ]]
        );
        $this->get('/api/v1/sittings?statesitting=ouverture_seance');

        $this->assertResponseCode(200);
    }

    public function testMiddlewareBadToken()
    {
        $token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJmVjhsX3RQeWpfTVpaWmNmSDgyTzA4eld1eU5MQjdrZGs0T3BWdlNPS2Z3In0.eyJqdGkiOiIyZTAzOWNlMC1kZDg4LTQ1ZTUtOTM2Zi1kZmFjNjJjMGQ1Y2MiLCJleHAiOjE1NTY2MzM1OTgsIm5iZiI6MCwiaWF0IjoxNTU2NjMzMjk4LCJpc3MiOiJodHRwczovL3dlYmFjdC5sb2NhbC9hdXRoL3JlYWxtcy9yZWFsbVdBIiwiYXVkIjoid2ViLWNsaWVudCIsInN1YiI6Ijg5MzJhM2RiLTZkZDMtNDY4Zi1hNTk5LTE5YzNiNTk2Y2U5ZCIsInR5cCI6IkJlYXJlciIsImF6cCI6IndlYi1jbGllbnQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIwYjQzYWU4OS1jMWQ4LTQ1ZDktOTk1YS05ODMzOTk2N2I0OWEiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbInVzZXIiXX0sInJlc291cmNlX2FjY2VzcyI6eyJyZWFsbS1tYW5hZ2VtZW50Ijp7InJvbGVzIjpbInZpZXctaWRlbnRpdHktcHJvdmlkZXJzIiwidmlldy1yZWFsbSIsIm1hbmFnZS1pZGVudGl0eS1wcm92aWRlcnMiLCJpbXBlcnNvbmF0aW9uIiwicmVhbG0tYWRtaW4iLCJjcmVhdGUtY2xpZW50IiwibWFuYWdlLXVzZXJzIiwicXVlcnktcmVhbG1zIiwidmlldy1hdXRob3JpemF0aW9uIiwicXVlcnktY2xpZW50cyIsInF1ZXJ5LXVzZXJzIiwibWFuYWdlLWV2ZW50cyIsIm1hbmFnZS1yZWFsbSIsInZpZXctZXZlbnRzIiwidmlldy11c2VycyIsInZpZXctY2xpZW50cyIsIm1hbmFnZS1hdXRob3JpemF0aW9uIiwibWFuYWdlLWNsaWVudHMiLCJxdWVyeS1ncm91cHMiXX0sImFwaS1jbGllbnQiOnsicm9sZXMiOlsiYWRtaW4tYXBpIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InByb2ZpbGUgZW1haWwiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJTYW1wbGUgVXNlciIsInByZWZlcnJlZF91c2VybmFtZSI6InVzZXIiLCJnaXZlbl9uYW1lIjoiU2FtcGxlIiwiZmFtaWx5X25hbWUiOiJVc2VyIiwiZW1haWwiOiJzYW1wbGUtdXNlckBleGFtcGxlIn0.LSn86U8w3I05W-jlDAmOpEEaYb7butS4Gm7sWKKgNgps_ZUGYjZoROyMe2gOj5cXG4oRhMRmvNKnN1x6OLRGsX7sGxoykx5Xbiawl-GP0nVc3HF4mvTSWA75mFD16JBTTRdtKDkyonB-fgt7uwXpr6CjjsdE0DW6Wc-KPhhhgJmO9NWRMpPkdKbOA_ry_sX0f_VL7pjg8n_Aib4pGR9zUpusVnzSM2X55L-YFx9JBk5mok-D8rgClR4KkbQ_2yIj9na2NEMz9GoofAG37v5OEUbEw3Q6f1Kfgf6432vHxaKJ-Xb5CYDfJno0oiMV9cbENxS7MKjgtpYSXL77UgK5og';

        Configure::write('KEYCLOAK_DEV_MODE', false);

        $this->configRequest(
            ['headers' => [
            'Accept' => 'application/json',
            'Authorization' => $token,
            ]]
        );
        $this->get('/api/v1/sittings?statesitting=ouverture_seance');

        $this->assertResponseCode(401);
    }

    public function testMiddlewareNoToken()
    {
        Configure::write('KEYCLOAK_DEV_MODE', false);

        $this->configRequest(
            ['headers' => [
            'Accept' => 'application/json',
            ]]
        );
        $this->get('/api/v1/sittings?statesitting=ouverture_seance');

        $this->assertResponseCode(401);
    }

    public function testMiddlewareDisabled()
    {
        Configure::write('KEYCLOAK_DEV_MODE', true);

        $this->configRequest(
            ['headers' => [
            'Accept' => 'application/json',
            ]]
        );
        $this->get('/api/v1/sittings?statesitting=ouverture_seance');

        $this->assertResponseCode(200);
    }

    public function testListUsers()
    {
        $auth = new Auth();
        $token = $auth->getToken(KEYCLOAK_TEST_USER, KEYCLOAK_TEST_PASSWORD);
        $auth->setToken($token);
        $users = $auth->getUsers();
        $this->assertNotEmpty($users);
    }

    public function testListUsersQuery()
    {
        $auth = new Auth();
        $token = $auth->getToken(KEYCLOAK_TEST_USER, KEYCLOAK_TEST_PASSWORD);
        $auth->setToken($token);
        $users = $auth->getUsersQuery('?username=s.blanc');
        $this->assertNotEmpty($users);
    }

    public function testCreateShowDeleteUser()
    {
        $auth = new Auth();
        $token = $auth->getToken(KEYCLOAK_TEST_USER, KEYCLOAK_TEST_PASSWORD);
        $auth->setToken($token);

        $user = [
            'firstName' => 'firstname',
            'lastName' => 'lastname',
            'username' => 'username',
            'email' => 'email',
            'enabled' => 'true',
            'credentials' => [[
                'type' => 'password',
                'value' => 'Aqwxsz123!er',
                'temporary' => false,
            ]],
        ];

        $res = $auth->addUser($user);
        $this->assertNotEmpty($res['id']);

        $user = $auth->getUser($res['id']);
        $this->assertEquals('username', $user['username']);

        $res = $auth->deleteUser($res['id']);
        $expected = [
            'success' => true,
            'data' => null,
            'code' => 204,
        ];
        $this->assertEquals($expected, $res);
    }
}
