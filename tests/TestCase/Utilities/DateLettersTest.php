<?php
declare(strict_types=1);

namespace App\Test\TestCase\Utilities;

use App\Test\TestCase\AbstractTestCase;
use App\Utilities\Common\DateLetters;
use Cake\I18n\FrozenTime;

class DateLettersTest extends AbstractTestCase
{
    /**
     * @dataProvider dataProviderFormat
     */
    public function testFormat(FrozenTime $time, string $expected)
    {
        $this->assertEquals($expected, DateLetters::format($time));
    }

    public function dataProviderFormat()
    {
        return [
            [new FrozenTime('2023-05-02'), 'L\'an deux mille vingt trois, le deux mai '],
            [new FrozenTime('1985-11-08'), 'L\'an mille neuf cent quatre vingt cinq, le huit novembre '],
        ];
    }
}
