<?php
declare(strict_types=1);

namespace App\Test\TestCase\Utilities;

use App\Test\TestCase\AbstractTestCase;
use App\Utilities\Common\Bytesize;

class BytesizeTest extends AbstractTestCase
{
    /**
     * @dataProvider dataTestIsValid
     */
    public function testIsValid($value, bool $expected)
    {
        $this->assertEquals($expected, Bytesize::isValid($value));
    }

    public function dataTestIsValid()
    {
        return [
            ['1', true],
            ['1B', true],
            ['1k', true],
            ['1M', true],
            ['1G', true],
            ['1T', true],
            ['32M', true],
            ['32.5M', true],
            ['256M', true],
            ['', false],
            ['M', false],
        ];
    }

    /**
     * @dataProvider dataTestTo
     */
    public function testTo($value, int $expected)
    {
        $this->assertEquals($expected, Bytesize::to($value));
    }

    public function dataTestTo()
    {
        return [
            ['1', 1],
            ['1B', 1],
            ['1k', 1024],
            ['1M', 1024 * 1024],
            ['1G', 1024 * 1024 * 1024],
            ['1T', 1024 * 1024 * 1024 * 1024],
            ['32M', 32 * 1024 * 1024],
            ['32.5M', (int)(32.5 * 1024 * 1024)],
            ['256M', 256 * 1024 * 1024],
        ];
    }

    /**
     * @dataProvider dataTestFrom
     */
    public function testFrom(int $bytes, int $precision, string $expected)
    {
        $this->assertEquals($expected, Bytesize::from($bytes, $precision));
    }

    public function dataTestFrom()
    {
        return [
            [1, 2, '1B'],
            [1024, 2, '1KB'],
            [1024 * 1024, 2, '1MB'],
            [1024 * 1024 * 1024, 2, '1GB'],
            [1024 * 1024 * 1024 * 1024, 2, '1TB'],
            [32 * 1024 * 1024, 2, '32MB'],
            [(int)(32.5 * 1024 * 1024), 2, '32.5MB'],
            [256 * 1024 * 1024, 2, '256MB'],
        ];
    }
}
