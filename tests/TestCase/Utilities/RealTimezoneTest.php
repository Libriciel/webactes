<?php
declare(strict_types=1);

namespace App\Test\TestCase\Utilities;

use App\Test\TestCase\AbstractTestCase;
use App\Utilities\Common\RealTimezone;
use Cake\I18n\FrozenTime;

class RealTimezoneTest extends AbstractTestCase
{
    /**
     * @dataProvider dataFormat
     */
    public function testFormat($appRealTimezone, $time, $format, $expected)
    {
        $_SERVER['APP_REAL_TIMEZONE'] = $appRealTimezone;
        $this->assertEquals($expected, RealTimezone::format(new FrozenTime($time), $format));
    }

    /**
     * Données pour les tests de la méthode format.
     */
    public function dataFormat()
    {
        return [
            ['Europe/Paris', '2023-04-05 03:08:09', 'd/m/Y', '05/04/2023'],
            ['UTC', '2023-04-05 03:08:09', 'd/m/Y', '05/04/2023'],
            ['Europe/Paris', '2023-04-05 03:08:09', 'Y-m-d H:i', '2023-04-05 05:08'],
            ['UTC', '2023-04-05 03:08:09', 'Y-m-d H:i', '2023-04-05 03:08'],
        ];
    }

    /**
     * @dataProvider dataFromPastellToSql
     */
    public function testFromPastellToSql($appRealTimezone, $pastellTimezone, $time, $expected)
    {
        $_SERVER['APP_DEFAULT_TIMEZONE'] = $appRealTimezone;
        $_SERVER['PASTELL_TIMEZONE'] = $pastellTimezone;
        $this->assertEquals($expected, RealTimezone::fromPastellToSql($time));
    }

    /**
     * Données pour les tests de la méthode fromPastell.
     */
    public function dataFromPastellToSql()
    {
        return [
            ['UTC', 'Europe/Paris', '2023-04-05 03:08:09', '2023-04-05 01:08:09'],
            ['Europe/Paris', 'Europe/Paris', '2023-04-05 03:08:09', '2023-04-05 03:08:09'],
            ['Europe/Paris', 'UTC', '2023-04-05 03:08:09', '2023-04-05 05:08:09'],
        ];
    }

    /**
     * @dataProvider dataI18nFormat
     */
    public function testI18nFormat($appRealTimezone, $time, $format, $expected)
    {
        $_SERVER['APP_REAL_TIMEZONE'] = $appRealTimezone;
        $this->assertEquals($expected, RealTimezone::i18nFormat(new FrozenTime($time), $format));
    }

    /**
     * Données pour les tests de la méthode i18nFormat.
     */
    public function dataI18nFormat()
    {
        return [
            ['Europe/Paris', '2023-04-05 03:08:09', 'HH', '05'],
            ['UTC', '2023-04-05 03:08:09', 'HH', '03'],
            ['Europe/Paris', '2023-04-05 03:08:09', 'mm', '08'],
            ['Europe/Paris', '2023-04-05 03:08:09', 'ss', '09'],
            ['Europe/Paris', '2023-04-05 03:08:09', 'dd/MM/Y', '05/04/2023'],
        ];
    }
}
