<?php
declare(strict_types=1);

namespace App\Test\Validation;

use App\Test\TestCase\AbstractTestCase;
use App\Validation\Validator;

class ValidatorTest extends AbstractTestCase
{
    /**
     * @dataProvider dataTestInList
     * @param array $params
     * @param array $expected
     */
    public function testInList(array $params, array $expected)
    {
        $validator = new Validator();
        $validator
            ->requirePresence('format', false)
            ->inList('format', ['full', 'list']);
        $actual = $validator->validate($params);

        $this->assertEquals($expected, $actual);
    }

    public function dataTestInList()
    {
        return [
            [[], []],
            [['format' => 'full'], []],
            [['format' => 'list'], []],
            [
                ['format' => null],
                [
                    'format' => [
                        '_empty' => 'This field cannot be left empty',
                    ],
                ],
            ],
            [
                ['format' => ''],
                [
                    'format' => [
                        'inList' => 'The provided value is invalid, use one of "full", "list"',
                    ],
                ],
            ],
            [
                ['format' => 'foo'],
                [
                    'format' => [
                        'inList' => 'The provided value is invalid, use one of "full", "list"',
                    ],
                ],
            ],
        ];
    }

    public function testAllowPresenceAndOnly()
    {
        $validator = new Validator();
        $validator
            ->allowPresence('format')
            ->requirePresence('statesitting')
            ->only();

        $actual = $validator->validate(['foo' => 'bar', 'statesitting' => 'seance_close']);
        $expected = [
            'foo' => [
                '_unexpected' => 'Unexpected parameter "foo", accepted parameters: "format", "statesitting"',
            ],
        ];

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider dataTestAllowPaginationOnly
     * @param array $params
     * @param array $expected
     */
    public function testAllowPaginationOnly(bool $allowPagination, array $params, array $expected)
    {
        $validator = new Validator();
        $validator->allowPagination($allowPagination)->only();
        $actual = $validator->validate($params);

        $this->assertEquals($expected, $actual);
    }

    public function dataTestAllowPaginationOnly()
    {
        return [
            [true, [], []],
            [true, ['direction' => 'asc', 'limit' => 10, 'page' => 2, 'sort' => 'id'], []],
            [
                false,
                ['direction' => 'asc', 'limit' => 10, 'page' => 2, 'sort' => 'id'],
                [
                    'direction' => [
                        '_unexpected' => 'Unexpected parameter "direction", there is no accepted parameter',
                    ],
                    'limit' => [
                        '_unexpected' => 'Unexpected parameter "limit", there is no accepted parameter',
                    ],
                    'page' => [
                        '_unexpected' => 'Unexpected parameter "page", there is no accepted parameter',
                    ],
                    'sort' => [
                        '_unexpected' => 'Unexpected parameter "sort", there is no accepted parameter',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider dataTestAllowPaginationLimit
     * @param array $params
     * @param array $expected
     */
    public function testAllowPaginationLimit(array $params, array $expected)
    {
        $validator = new Validator();
        $validator->allowPagination(true);
        $actual = $validator->validate($params);

        $this->assertEquals($expected, $actual);
    }

    public function dataTestAllowPaginationLimit()
    {
        return [
            [[], []],
            [['limit' => 10], []],
            [
                ['limit' => ''],
                [
                    'limit' => [
                        'integer' => 'The provided value is invalid',
                    ],
                ],
            ],
            [
                ['limit' => 'foo'],
                [
                    'limit' => [
                        'integer' => 'The provided value is invalid',
                    ],
                ],
            ],
            [
                ['limit' => 3.66],
                [
                    'limit' => [
                        'integer' => 'The provided value is invalid',
                    ],
                ],
            ],
            [
                ['limit' => -1],
                [
                    'limit' => [
                        'greaterThan' => 'The provided value is invalid, must be > 0',
                    ],
                ],
            ],
            [
                ['limit' => 32768],
                [
                    'limit' => [
                        'lessThanOrEqual' => 'The provided value is invalid, must be <= 32767',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider dataTestAllowPaginationPage
     * @param array $params
     * @param array $expected
     */
    public function testAllowPaginationPage(array $params, array $expected)
    {
        $validator = new Validator();
        $validator->allowPagination(true);
        $actual = $validator->validate($params);

        $this->assertEquals($expected, $actual);
    }

    public function dataTestAllowPaginationPage()
    {
        return [
            [[], []],
            [['page' => 10], []],
            [
                ['page' => ''],
                [
                    'page' => [
                        'integer' => 'The provided value is invalid',
                    ],
                ],
            ],
            [
                ['page' => 'foo'],
                [
                    'page' => [
                        'integer' => 'The provided value is invalid',
                    ],
                ],
            ],
            [
                ['page' => 3.66],
                [
                    'page' => [
                        'integer' => 'The provided value is invalid',
                    ],
                ],
            ],
            [
                ['page' => -1],
                [
                    'page' => [
                        'greaterThan' => 'The provided value is invalid, must be > 0',
                    ],
                ],
            ],
            [
                ['page' => 32768],
                [
                    'page' => [
                        'lessThanOrEqual' => 'The provided value is invalid, must be <= 32767',
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider dataTestAllowPaginationDirection
     * @param array $params
     * @param array $expected
     */
    public function testAllowPaginationDirection(array $params, array $expected)
    {
        $validator = new Validator();
        $validator->allowPagination(true);
        $actual = $validator->validate($params);

        $this->assertEquals($expected, $actual);
    }

    public function dataTestAllowPaginationDirection()
    {
        $expected = [
            '_empty' => [
                'direction' => [
                    '_empty' => 'This field cannot be left empty',
                ],
            ],
            'inList' => [
                'direction' => [
                    'inList' => 'The provided value is invalid, use one of "asc", "desc"',
                ],
            ],
        ];

        return [
            [[], []],
            [['direction' => 'asc'], []],
            [['direction' => 'desc'], []],
            [['direction' => ''], $expected['inList']],
            [['direction' => 'foo'], $expected['inList']],
            [['direction' => 'ASC'], $expected['inList']],
            [['direction' => null], $expected['_empty']],
        ];
    }
}
