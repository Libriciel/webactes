<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service;

use App\Test\TestCase\AbstractTestCase;
use App\Utilities\Common\FilesSeederTrait;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestTrait;

/**
 * Class ConvertsServiceTest
 *
 * @phpi
 * @package App\Test\TestCase\Utilities
 */
class BuildJsonIdelibreServiceTest extends AbstractTestCase
{
    use FilesSeederTrait;
    use IntegrationTestTrait;
    use ServiceAwareTrait;

    public $fixtures = [
        'app.group.All',
    ];

    public function setUp(): void
    {
        $this->skipIfNoGenerationConfig();

        parent::setUp();
        $this->setDefaultSession();

        Configure::write('KEYCLOAK_DEV_MODE', true);
    }

    public function testBuild()
    {
        $this->loadService('BuildJsonIdelibre', ['id' => 1]);

        $json = $this->BuildJsonIdelibre->getJson();

        $this->assertEquals(
            ['convocation', 'projet_0_rapport', 'projet_1_rapport', 'projet_2_rapport', 'jsonData'],
            array_keys($json)
        );
    }
}
