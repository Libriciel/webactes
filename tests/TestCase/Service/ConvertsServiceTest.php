<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service;

use App\Test\TestCase\AbstractTestCase;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\TestSuite\IntegrationTestTrait;
use function App\mimetype;
use function App\safe_shell_exec;

/**
 * Class ConvertsServiceTest
 *
 * @phpi
 * @package App\Test\TestCase\Utilities
 */
class ConvertsServiceTest extends AbstractTestCase
{
    use IntegrationTestTrait;
    use LocatorAwareTrait;
    use ServiceAwareTrait;

    public $fixtures = [
        'app.group.All',
    ];

    public function setUp(): void
    {
        parent::setUp();
        $this->setDefaultSession();

        $this->Annexes = $this->fetchTable('Annexes');
    }

    public function testConvertAnnex()
    {
        $annexe = $this->Annexes->newEntity(
            [
                'structure_id' => 1,
                'container_id' => 1,
                'rank' => 1,
                'istransmissible' => false,
                'current' => true,
                'codetype' => null,
                'is_generate' => true,
                'file' => [
                    'structure_id' => 1,
                    'maindocument_id' => null,
                    'annex_id' => null,
                    'name' => 'annexe.pdf',
                    'path' => $this->createTempFile(static::PDF_TEMPLATE_PATH, 'annexe_001.pdf'),
                    'mimetype' => 'application/pdf',
                    'size' => 30951,
                    'generate_template_id' => null,
                    'draft_template_id' => null,
                    'project_text_id' => null,
                    'attachment_summon_id' => null,
                ],
            ],
        );
        $this->assertNotFalse($this->Annexes->save($annexe, ['associated' => ['Files']]));

        $expected = dirname($annexe->file->path) . DS . 'odt' . DS . basename($annexe->file->path);
        $this->assertFileDoesNotExist($expected);

        $this->Converts = $this->loadService('Converts', ['annex_id', [$annexe->id]]);
        $this->Converts->convertFiles();

        $this->assertFileExists($expected);
        $this->assertEquals('application/vnd.oasis.opendocument.text', mimetype($expected));

        safe_shell_exec(sprintf('rm -rf %s', escapeshellarg(dirname($annexe->file->path))));
    }
}
