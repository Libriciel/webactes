<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\ThemesFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class ThemeFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderBuild
     */
    public function testBuild(?int $id, PartType $expected)
    {
        $actual = (new ThemesFusionPart($this->getTemplateStub(), $id))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuild()
    {
        return [
            [
                1,
                (
                (new FusionBuilder())
                    ->addTextField('theme', 'Administration Générale')
                    ->addTextField('theme_critere_trie', 'Lorem')
                    ->addTextField('T1_theme', 'Administration Générale')
                    ->addTextField('theme1', 'Administration Générale')
                    ->addTextField('theme2', '')
                    ->addTextField('theme3', '')
                    ->addTextField('theme4', '')
                    ->addTextField('theme5', '')
                    ->addTextField('theme6', '')
                    ->addTextField('theme7', '')
                    ->addTextField('theme8', '')
                    ->addTextField('theme9', '')
                    ->addTextField('theme10', '')
                )->getPart(),
            ],
        ];
    }
}
