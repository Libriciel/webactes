<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\ProjectSittingsFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class ProjectSittingsFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderBuildFull
     */
    public function testBuildFull(?int $id, PartType $expected)
    {
        $actual = (new ProjectSittingsFusionPart($this->getTemplateStub(), $id))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuildFull()
    {
        return [
            [ // avec séances
                1,
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            (
                            (new FusionBuilder())
                                ->addDateField('seances_date', '10/02/2019')
                                ->addTextField('seances_date_lettres', 'L\'an deux mille dix neuf, le dix  février ')
                                ->addTextField('seances_hh', '16')
                                ->addTextField('seances_mm', '01')
                                ->addTextField('seances_position', '1')
                                ->addTextField('seances_type', 'Conseil Municipal')
                                ->addDateField('seances_date_convocation', '10/02/2019')
                                ->addStringField('seances_lieu', 'Sale du conseil municipal de la mairie de Libriciel SCOP à Castelnau-le-Lez')
                            )->getPart(),
                            (
                            (new FusionBuilder())
                                ->addDateField('seances_date', '21/02/2019')
                                ->addTextField('seances_date_lettres', 'L\'an deux mille dix neuf, le vingt et un février ')
                                ->addTextField('seances_hh', '15')
                                ->addTextField('seances_mm', '34')
                                ->addTextField('seances_position', '1')
                                ->addTextField('seances_type', 'Bureau')
                                ->addDateField('seances_date_convocation', '21/02/2019')
                                ->addStringField('seances_lieu', '')
                            )->getPart(),
                        ]
                    )
                    ->addDateField('seance_deliberante_date', '10/02/2019')
                    ->addTextField('seance_deliberante_type', 'Conseil Municipal')
                    ->addTextField('seance_deliberante_position', '1')
                    ->addDateField('seance_deliberante_date_convocation', '10/02/2019')
                    ->addStringField('seance_deliberante_lieu', 'Sale du conseil municipal de la mairie de Libriciel SCOP à Castelnau-le-Lez')
                )->getPart(),
            ],
            [ // Pas de séance
                2,
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '0')
                    ->addIteration(
                        'Seances',
                        [
                            (
                            (new FusionBuilder())
                            )->getPart(),
                        ]
                    )
                    ->addDateField('seance_deliberante_date', '')
                    ->addTextField('seance_deliberante_type', '')
                    ->addTextField('seance_deliberante_position', '')
                    ->addTextField('seance_deliberante_date_convocation', '')
                    ->addStringField('seance_deliberante_lieu', '')
                )->getPart(),
            ],
        ];
    }

    /**
     * @dataProvider dataProviderBuildOneFieldAtATime
     */
    public function testBuildOneFieldAtATime(?int $id, string $only, array $sections, PartType $expected)
    {
        $hasUserFieldDeclared = function ($key) use ($only): bool {
            return $key === $only;
        };
        $template = $this->getTemplateStub($hasUserFieldDeclared, false, $sections);
        $actual = (new ProjectSittingsFusionPart($template, $id))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuildOneFieldAtATime()
    {
        return [
            [
                1,
                'seances_nombre',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder()))->getPart(),
                            ((new FusionBuilder()))->getPart(),
                        ]
                    )
                )->getPart(),
            ],
            [
                1,
                'seances_date',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder())
                                ->addDateField('seances_date', '10/02/2019')
                            )->getPart(),
                            ((new FusionBuilder())
                                ->addDateField('seances_date', '21/02/2019')
                            )->getPart(),
                        ]
                    )
                )->getPart(),
            ],
            [
                1,
                'seances_date_lettres',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder())
                                ->addTextField(
                                    'seances_date_lettres',
                                    'L\'an deux mille dix neuf, le dix  février '
                                )
                            )->getPart(),
                            ((new FusionBuilder())
                                ->addTextField(
                                    'seances_date_lettres',
                                    'L\'an deux mille dix neuf, le vingt et un février '
                                )
                            )->getPart(),
                        ]
                    )
                )->getPart(),
            ],
            [
                1,
                'seances_hh',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder())->addTextField('seances_hh', '16'))->getPart(),
                            ((new FusionBuilder())->addTextField('seances_hh', '15'))->getPart(),
                        ]
                    )
                )->getPart(),
            ],
            [
                1,
                'seances_mm',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder())->addTextField('seances_mm', '01'))->getPart(),
                            ((new FusionBuilder())->addTextField('seances_mm', '34'))->getPart(),
                        ]
                    )
                )->getPart(),
            ],
            [
                1,
                'seances_position',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder())->addTextField('seances_position', '1'))->getPart(),
                            ((new FusionBuilder())->addTextField('seances_position', '1'))->getPart(),
                        ]
                    )
                )->getPart(),
            ],
            [
                1,
                'seances_type',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder())->addTextField('seances_type', 'Conseil Municipal'))->getPart(),
                            ((new FusionBuilder())->addTextField('seances_type', 'Bureau'))->getPart(),
                        ]
                    )
                )->getPart(),
            ],
            [
                1,
                'seances_date_convocation',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder())->addDateField('seances_date_convocation', '10/02/2019'))->getPart(),
                            ((new FusionBuilder())->addDateField('seances_date_convocation', '21/02/2019'))->getPart(),
                        ]
                    )
                )->getPart(),
            ],
            [
                1,
                'seance_deliberante_date',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder()))->getPart(),
                            ((new FusionBuilder()))->getPart(),
                        ]
                    )
                    ->addDateField('seance_deliberante_date', '10/02/2019')
                )->getPart(),
            ],
            [
                1,
                'seance_deliberante_type',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder()))->getPart(),
                            ((new FusionBuilder()))->getPart(),
                        ]
                    )
                    ->addTextField('seance_deliberante_type', 'Conseil Municipal')
                )->getPart(),
            ],
            [
                1,
                'seance_deliberante_position',
                ['Seances'],
                (
                (new FusionBuilder())
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            ((new FusionBuilder()))->getPart(),
                            ((new FusionBuilder()))->getPart(),
                        ]
                    )
                    ->addTextField('seance_deliberante_position', '1')
                )->getPart(),
            ],
        ];
    }
}
