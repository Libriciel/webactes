<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\AttendanceListFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class AttendanceListFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderBuild
     */
    public function testBuild(int $sittingId, int $projectId, PartType $expected)
    {
        $actual = (new AttendanceListFusionPart($this->getTemplateStub(), $sittingId, $projectId))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuild()
    {
        return [
            [
                1,
                1,
                (
                (new FusionBuilder())
                    ->addTextField('acteur_present_nombre', '1')
                    ->addIteration(
                        'ActeursPresents',
                        [
                            (
                            (new FusionBuilder())
                                ->addTextField('acteur_present_acteur_salutation', 'M')
                                ->addTextField('acteur_present_acteur_nom', 'ROUGE2')
                                ->addTextField('acteur_present_acteur_prenom', 'Théo2')
                                ->addTextField('acteur_present_acteur_email', 't.rouge2@exemple.fr')
                                ->addTextField('acteur_present_acteur_titre', 'Maire')
                                ->addTextField('acteur_present_acteur_position', '1')
                                ->addTextField('acteur_present_acteur_telmobile', '0602030406')
                            )->getPart(),
                        ]
                    )
                    ->addTextField('acteur_mandataire_nombre', '1')
                    ->addIteration(
                        'ActeursMandataires',
                        [
                            (
                            (new FusionBuilder())
                                ->addTextField('acteur_mandant_acteur_salutation', 'M')
                                ->addTextField('acteur_mandant_acteur_nom', 'ROUGE3')
                                ->addTextField('acteur_mandant_acteur_prenom', 'Théo2')
                                ->addTextField('acteur_mandant_acteur_email', 't.rouge3@exemple.fr')
                                ->addTextField('acteur_mandant_acteur_titre', 'Maire')
                                ->addTextField('acteur_mandant_acteur_position', '2')
                                ->addTextField('acteur_mandant_acteur_telmobile', '0602030406')
                                ->addTextField('acteur_mandataire_acteur_salutation', 'M')
                                ->addTextField('acteur_mandataire_acteur_nom', 'ROUGE4')
                                ->addTextField('acteur_mandataire_acteur_prenom', 'Théo4')
                                ->addTextField('acteur_mandataire_acteur_email', 't.rouge4@exemple.fr')
                                ->addTextField('acteur_mandataire_acteur_titre', 'Maire')
                                ->addTextField('acteur_mandataire_acteur_position', '3')
                                ->addTextField('acteur_mandataire_acteur_telmobile', '0602030406')
                            )->getPart(),
                        ]
                    )
                    ->addTextField('acteur_absent_nombre', '1')
                    ->addIteration(
                        'ActeursAbsents',
                        [
                            (
                            (new FusionBuilder())
                                ->addTextField('acteur_absent_acteur_salutation', 'M')
                                ->addTextField('acteur_absent_acteur_nom', 'ROUGE')
                                ->addTextField('acteur_absent_acteur_prenom', 'Théo')
                                ->addTextField('acteur_absent_acteur_email', 't.rouge@exemple.fr')
                                ->addTextField('acteur_absent_acteur_titre', 'Maire')
                                ->addTextField('acteur_absent_acteur_position', '1')
                                ->addTextField('acteur_absent_acteur_telmobile', '0602030405')
                            )->getPart(),
                        ]
                    )
                )->getPart(),
            ],
            [
                1,
                2,
                (
                (new FusionBuilder())
                    ->addTextField('acteur_present_nombre', '0')
                    ->addIteration('ActeursPresents', $this->getEmptyActorsList('acteur_present'))
                    ->addTextField('acteur_mandataire_nombre', '0')
                    ->addIteration(
                        'ActeursMandataires',
                        $this->getEmptyActorsList('acteur_mandant', 'acteur_mandataire')
                    )
                    ->addTextField('acteur_absent_nombre', '0')
                    ->addIteration('ActeursAbsents', $this->getEmptyActorsList('acteur_absent'))
                )->getPart(),
            ],
        ];
    }
}
