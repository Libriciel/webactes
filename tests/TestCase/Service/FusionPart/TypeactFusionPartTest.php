<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\TypeactFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class TypeactFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderBuild
     */
    public function testBuild(?int $id, PartType $expected)
    {
        $actual = (new TypeactFusionPart($this->getTemplateStub(), $id))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuild()
    {
        return [
            [
                1,
                ((new FusionBuilder())->addTextField('type_acte', 'Délibération'))->getPart(),
            ],
        ];
    }
}
