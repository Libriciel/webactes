<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\ProjectTextsFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class ProjectTextsFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderBuildFull
     */
    public function testBuildFull(?int $id, PartType $expected)
    {
        $actual = (new ProjectTextsFusionPart($this->getTemplateStub(), $id))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuildFull()
    {
        return [
            [
                1,
                (
                (new FusionBuilder())
                    ->addOdtContent(
                        'texte_projet',
                        'texte_projet.odt',
                        $this->getFileContent(TESTS . 'Fixture/files/project_texts/gabarit_projet_v1.odt')
                    )
                    ->addOdtContent(
                        'texte_acte',
                        'texte_acte.odt',
                        $this->getFileContent(TESTS . 'Fixture/files/project_texts/gabarit_acte_v1.odt')
                    )
                )->getPart(),
            ],
        ];
    }
}
