<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\AnnexesFusionPart;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;

class AnnexesFusionPartTest extends AbstractFusionPartTestCase
{
    public $files = [];

    public function setUp(): void
    {
        parent::setUp();

        $max = 3;

        // Insertion des annexes
        $data = [];
        for ($idx = 1; $idx <= $max; $idx++) {
            $data[] = [
                'structure_id' => 1,
                'container_id' => 1,
                'rank' => $idx,
                'istransmissible' => true,
                'current' => true,
                'codetype' => '21_DA',
                'is_generate' => $idx !== $max,
            ];
        }
        $annexes = $this->insert('Annexes', $data);

        // Copie des fichiers
        for ($idx = 1; $idx <= $max; $idx++) {
            $path = TESTS . 'Fixture/files/pdf/annexe_' . $idx . '.pdf';
            //print_r(compact('path'));
            $this->files[] = $this->fileInfo(
                $this->putFilePath(
                    $path,
                    '/data/workspace/1/1/' . date('Y') . DS . $annexes[$idx - 1]->id . DS . 'odt' . DS
                )
            );
            // Pour que l'application considère que le fichier a été converti, il faut aussi qu'il soit sur le même chemin moins /odt/
            copy($path, str_replace(DS . 'odt' . DS, DS, $this->files[$idx - 1]->getRealPath()));
        }

        // Insertion des files
        $data = [];
        for ($idx = 1; $idx <= $max; $idx++) {
            $data[] = [
                'structure_id' => 1,
                'draft_template_id' => null,
                'annex_id' => $annexes[$idx - 1]->id,
                'generate_template_id' => null,
                'name' => 'annex_' . $idx . '.odt',
                'path' => str_replace(DS . 'odt' . DS, DS, $this->files[$idx - 1]->getRealPath()),
                'mimetype' => 'application/vnd.oasis.opendocument.text',
                'size' => $this->files[$idx - 1]->getSize(),
            ];
        }
        $this->insert('Files', $data, ['inFS' => true]);
    }

    public function testBuild()
    {
        $actual = (new AnnexesFusionPart($this->getTemplateStub(true, false, ['Annexes']), 1))->build();

        $expected = (
        (new FusionBuilder())
            ->addTextField('annexes_nombre', '2')
            ->addIteration(
                'Annexes',
                [
                    (
                    (new FusionBuilder())
                        ->addTextField('nom_fichier', 'annex_1.odt')
                        ->addOdtContent(
                            'fichier',
                            'fichier.odt',
                            $this->getFileContent($this->files[0]->getRealPath())
                        )
                    )->getPart(),
                    (
                    (new FusionBuilder())
                        ->addTextField('nom_fichier', 'annex_2.odt')
                        ->addOdtContent(
                            'fichier',
                            'fichier.odt',
                            $this->getFileContent($this->files[1]->getRealPath())
                        )
                    )->getPart(),
                ]
            )
        )->getPart();

        $this->assertEquals($expected, $actual);
    }
}
