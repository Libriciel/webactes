<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Test\TestCase\AbstractTestCase;
use App\Utilities\Common\FilesSeederTrait;
use App\Utilities\Common\PhpOdtApi;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\TestSuite\IntegrationTestTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use SplFileObject;

/**
 * Class AbstractFusionPartTestCase
 */
abstract class AbstractFusionPartTestCase extends AbstractTestCase
{
    use FilesSeederTrait;
    use IntegrationTestTrait;
    use ServiceAwareTrait;

    public $fixtures = [
        'app.group.All',
    ];

    protected $phpOdtApiExport = null;

    public function setUp(): void
    {
        $this->phpOdtApiExport = $_SERVER['PHP_ODT_API_EXPORT'] ?? 'false';
        $_SERVER['PHP_ODT_API_EXPORT'] = 'false';
        parent::setUp();
        $this->setDefaultSession();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $_SERVER['PHP_ODT_API_EXPORT'] = $this->phpOdtApiExport;
    }

    protected function getFileContent(string $path): string
    {
        if ($path !== '') {
            $file = new SplFileObject($path, 'r');

            return (string)$file->fread($file->getSize());
        }

        return '';
    }

    protected function getEmptyActorsList(string $prefix, ?string $additionalPrefix = null): array
    {
        if (!$additionalPrefix) {
            return [
                (
                (new FusionBuilder())
                    ->addTextField("{$prefix}_acteur_salutation", '')
                    ->addTextField("{$prefix}_acteur_nom", '')
                    ->addTextField("{$prefix}_acteur_prenom", '')
                    ->addTextField("{$prefix}_acteur_email", '')
                    ->addTextField("{$prefix}_acteur_titre", '')
                    ->addTextField("{$prefix}_acteur_position", '')
                    ->addTextField("{$prefix}_acteur_telmobile", '')
                )->getPart(),
            ];
        } else {
            return [
                (
                (new FusionBuilder())
                    ->addTextField("{$prefix}_acteur_salutation", '')
                    ->addTextField("{$prefix}_acteur_nom", '')
                    ->addTextField("{$prefix}_acteur_prenom", '')
                    ->addTextField("{$prefix}_acteur_email", '')
                    ->addTextField("{$prefix}_acteur_titre", '')
                    ->addTextField("{$prefix}_acteur_position", '')
                    ->addTextField("{$prefix}_acteur_telmobile", '')
                    ->addTextField("{$additionalPrefix}_acteur_salutation", '')
                    ->addTextField("{$additionalPrefix}_acteur_nom", '')
                    ->addTextField("{$additionalPrefix}_acteur_prenom", '')
                    ->addTextField("{$additionalPrefix}_acteur_email", '')
                    ->addTextField("{$additionalPrefix}_acteur_titre", '')
                    ->addTextField("{$additionalPrefix}_acteur_position", '')
                    ->addTextField("{$additionalPrefix}_acteur_telmobile", '')
                )->getPart(),
            ];
        }
    }

    protected function getTemplateStub($hasUserFieldDeclared = true, $hasUserField = true, $getSections = []): PhpOdtApi
    {
        $template = $this->getMockBuilder(PhpOdtApi::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock();

        if (is_callable($getSections) === true) {
            $template->method('getSections')->willReturnCallback($getSections);
        } else {
            $template->method('getSections')->willReturn($getSections);
        }

        if (is_callable($hasUserField) === true) {
            $template->method('hasUserField')->willReturnCallback($hasUserField);
        } else {
            $template->method('hasUserField')->willReturn($hasUserField);
        }

        if (is_callable($hasUserFieldDeclared) === true) {
            $template->method('hasUserFieldDeclared')->willReturnCallback($hasUserFieldDeclared);
        } else {
            $template->method('hasUserFieldDeclared')->willReturn($hasUserFieldDeclared);
        }

        return $template;
    }
}
