<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\ActorsVotesFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;

class ActorsVotesFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @var VotesTable
     */
    protected $Votes;

    public function setUp(): void
    {
        parent::setUp();

        $this->Votes = $this->fetchTable('Votes');
    }

    /**
     * Création et récupération d'un vote au détail des voix
     */
    public function testBuildMethodDetails()
    {
        $vote = $this->Votes->save(
            $this->Votes->newEntity(
                [
                    'president_id' => null,
                    'project_id' => 1,
                    'structure_id' => 1,
                    'yes_counter' => 1,
                    'no_counter' => 1,
                    'abstentions_counter' => 1,
                    'no_words_counter' => null,
                    'take_act' => true,
                    'result' => null,
                    'comment' => null,
                    'method' => 'details',
                ]
            )
        );
        $this->Votes->ActorsVotes->saveMany(
            [
                $this->Votes->ActorsVotes->newEntity(
                    [
                        'actor_id' => 1,
                        'vote_id' => $vote->id,
                        'result' => 'yes',
                    ]
                ),
                $this->Votes->ActorsVotes->newEntity(
                    [
                        'actor_id' => 3,
                        'vote_id' => $vote->id,
                        'result' => 'no',
                    ]
                ),
                $this->Votes->ActorsVotes->newEntity(
                    [
                        'actor_id' => 4,
                        'vote_id' => $vote->id,
                        'result' => 'abstention',
                    ]
                ),
            ]
        );

        $actual = (new ActorsVotesFusionPart($this->getTemplateStub(), 1, 1))->build();

        $expected = (
        (new FusionBuilder())
            ->addTextField('nombre_acteur_contre', '1')
            ->addIteration(
                'ActeursContre',
                [
                    (
                    (new FusionBuilder())
                        ->addTextField('acteur_contre_acteur_salutation', 'M')
                        ->addTextField('acteur_contre_acteur_nom', 'ROUGE3')
                        ->addTextField('acteur_contre_acteur_prenom', 'Théo2')
                        ->addTextField('acteur_contre_acteur_email', 't.rouge3@exemple.fr')
                        ->addTextField('acteur_contre_acteur_titre', 'Maire')
                        ->addTextField('acteur_contre_acteur_position', '2')
                        ->addTextField('acteur_contre_acteur_telmobile', '0602030406')
                    )->getPart(),
                ]
            )
            ->addTextField('nombre_acteur_pour', '1')
            ->addIteration(
                'ActeursPour',
                [
                    (
                    (new FusionBuilder())
                        ->addTextField('acteur_pour_acteur_salutation', 'M')
                        ->addTextField('acteur_pour_acteur_nom', 'ROUGE')
                        ->addTextField('acteur_pour_acteur_prenom', 'Théo')
                        ->addTextField('acteur_pour_acteur_email', 't.rouge@exemple.fr')
                        ->addTextField('acteur_pour_acteur_titre', 'Maire')
                        ->addTextField('acteur_pour_acteur_position', '1')
                        ->addTextField('acteur_pour_acteur_telmobile', '0602030405')
                    )->getPart(),
                ]
            )
            ->addTextField('nombre_acteur_abstention', '1')
            ->addIteration(
                'ActeursAbstention',
                [
                    (
                    (new FusionBuilder())
                        ->addTextField('acteur_abstention_acteur_salutation', 'M')
                        ->addTextField('acteur_abstention_acteur_nom', 'ROUGE4')
                        ->addTextField('acteur_abstention_acteur_prenom', 'Théo4')
                        ->addTextField('acteur_abstention_acteur_email', 't.rouge4@exemple.fr')
                        ->addTextField('acteur_abstention_acteur_titre', 'Maire')
                        ->addTextField('acteur_abstention_acteur_position', '3')
                        ->addTextField('acteur_abstention_acteur_telmobile', '0602030406')
                    )->getPart(),
                ]
            )
            ->addTextField('nombre_acteur_sans_participation', '0')
            ->addIteration('ActeursSansParticipation', $this->getEmptyActorsList('acteur_sans_participation'))
        )->getPart();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Création et récupération d'un vote au total des voix
     */
    public function testBuildMethodTotal()
    {
        $this->Votes->save(
            $this->Votes->newEntity(
                [
                    'president_id' => null,
                    'project_id' => 1,
                    'structure_id' => 1,
                    'yes_counter' => 4,
                    'no_counter' => 3,
                    'abstentions_counter' => 2,
                    'no_words_counter' => 1,
                    'take_act' => null,
                    'result' => null,
                    'comment' => null,
                    'method' => 'total',
                ]
            )
        );

        $actual = (new ActorsVotesFusionPart($this->getTemplateStub(), 1, 1))->build();

        $expected = (
        (new FusionBuilder())
            ->addTextField('nombre_acteur_contre', '3')
            ->addIteration('ActeursContre', $this->getEmptyActorsList('acteur_contre'))
            ->addTextField('nombre_acteur_pour', '4')
            ->addIteration('ActeursPour', $this->getEmptyActorsList('acteur_pour'))
            ->addTextField('nombre_acteur_abstention', '2')
            ->addIteration('ActeursAbstention', $this->getEmptyActorsList('acteur_abstention'))
            ->addTextField('nombre_acteur_sans_participation', '1')
            ->addIteration('ActeursSansParticipation', $this->getEmptyActorsList('acteur_sans_participation'))
        )->getPart();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Création et récupération d'un vote au résultat
     */
    public function testBuildMethodResult()
    {
        $this->Votes->save(
            $this->Votes->newEntity(
                [
                    'president_id' => null,
                    'project_id' => 1,
                    'structure_id' => 1,
                    'yes_counter' => null,
                    'no_counter' => null,
                    'abstentions_counter' => null,
                    'no_words_counter' => null,
                    'take_act' => null,
                    'result' => true,
                    'comment' => null,
                    'method' => 'result',
                ]
            )
        );

        $actual = (new ActorsVotesFusionPart($this->getTemplateStub(), 1, 1))->build();

        $expected = (
        (new FusionBuilder())
            ->addTextField('nombre_acteur_contre', '')
            ->addIteration('ActeursContre', $this->getEmptyActorsList('acteur_contre'))
            ->addTextField('nombre_acteur_pour', '')
            ->addIteration('ActeursPour', $this->getEmptyActorsList('acteur_pour'))
            ->addTextField('nombre_acteur_abstention', '')
            ->addIteration('ActeursAbstention', $this->getEmptyActorsList('acteur_abstention'))
            ->addTextField('nombre_acteur_sans_participation', '')
            ->addIteration('ActeursSansParticipation', $this->getEmptyActorsList('acteur_sans_participation'))
        )->getPart();

        $this->assertEquals($expected, $actual);
    }

    /**
     * Création et récupération d'un vote à la prise d'acte
     */
    public function testBuildMethodTakeAct()
    {
        $this->Votes->save(
            $this->Votes->newEntity(
                [
                    'president_id' => null,
                    'project_id' => 1,
                    'structure_id' => 1,
                    'yes_counter' => null,
                    'no_counter' => null,
                    'abstentions_counter' => null,
                    'no_words_counter' => null,
                    'take_act' => true,
                    'result' => null,
                    'comment' => null,
                    'method' => 'take_act',
                ]
            )
        );

        $actual = (new ActorsVotesFusionPart($this->getTemplateStub(), 1, 1))->build();

        $expected = (
        (new FusionBuilder())
            ->addTextField('nombre_acteur_contre', '')
            ->addIteration('ActeursContre', $this->getEmptyActorsList('acteur_contre'))
            ->addTextField('nombre_acteur_pour', '')
            ->addIteration('ActeursPour', $this->getEmptyActorsList('acteur_pour'))
            ->addTextField('nombre_acteur_abstention', '')
            ->addIteration('ActeursAbstention', $this->getEmptyActorsList('acteur_abstention'))
            ->addTextField('nombre_acteur_sans_participation', '')
            ->addIteration('ActeursSansParticipation', $this->getEmptyActorsList('acteur_sans_participation'))
        )->getPart();

        $this->assertEquals($expected, $actual);
    }
}
