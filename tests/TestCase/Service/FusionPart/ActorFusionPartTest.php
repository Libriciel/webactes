<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\ActorFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class ActorFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderBuild
     */
    public function testBuild(?int $id, string $prefix, PartType $expected)
    {
        $actual = (new ActorFusionPart($this->getTemplateStub(), $id, $prefix))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuild()
    {
        return [
            [
                null,
                'acteur_present',
                (
                (new FusionBuilder())
                    ->addTextField('acteur_present_acteur_salutation', '')
                    ->addTextField('acteur_present_acteur_nom', '')
                    ->addTextField('acteur_present_acteur_prenom', '')
                    ->addTextField('acteur_present_acteur_email', '')
                    ->addTextField('acteur_present_acteur_titre', '')
                    ->addTextField('acteur_present_acteur_position', '')
                    ->addTextField('acteur_present_acteur_telmobile', '')
                )->getPart(),
            ],
            [
                1,
                'acteur_present',
                (
                (new FusionBuilder())
                    ->addTextField('acteur_present_acteur_salutation', 'M')
                    ->addTextField('acteur_present_acteur_nom', 'ROUGE')
                    ->addTextField('acteur_present_acteur_prenom', 'Théo')
                    ->addTextField('acteur_present_acteur_email', 't.rouge@exemple.fr')
                    ->addTextField('acteur_present_acteur_titre', 'Maire')
                    ->addTextField('acteur_present_acteur_position', '1')
                    ->addTextField('acteur_present_acteur_telmobile', '0602030405')
                )->getPart(),
            ],
            [
                2,
                'acteur_mandant',
                (
                (new FusionBuilder())
                    ->addTextField('acteur_mandant_acteur_salutation', 'M')
                    ->addTextField('acteur_mandant_acteur_nom', 'ROUGE2')
                    ->addTextField('acteur_mandant_acteur_prenom', 'Théo2')
                    ->addTextField('acteur_mandant_acteur_email', 't.rouge2@exemple.fr')
                    ->addTextField('acteur_mandant_acteur_titre', 'Maire')
                    ->addTextField('acteur_mandant_acteur_position', '1')
                    ->addTextField('acteur_mandant_acteur_telmobile', '0602030406')
                )->getPart(),
            ],
            [
                3,
                'acteur_mandataire',
                (
                (new FusionBuilder())
                    ->addTextField('acteur_mandataire_acteur_salutation', 'M')
                    ->addTextField('acteur_mandataire_acteur_nom', 'ROUGE3')
                    ->addTextField('acteur_mandataire_acteur_prenom', 'Théo2')
                    ->addTextField('acteur_mandataire_acteur_email', 't.rouge3@exemple.fr')
                    ->addTextField('acteur_mandataire_acteur_titre', 'Maire')
                    ->addTextField('acteur_mandataire_acteur_position', '2')
                    ->addTextField('acteur_mandataire_acteur_telmobile', '0602030406')
                )->getPart(),
            ],
            [
                4,
                'acteur_absent',
                (
                (new FusionBuilder())
                    ->addTextField('acteur_absent_acteur_salutation', 'M')
                    ->addTextField('acteur_absent_acteur_nom', 'ROUGE4')
                    ->addTextField('acteur_absent_acteur_prenom', 'Théo4')
                    ->addTextField('acteur_absent_acteur_email', 't.rouge4@exemple.fr')
                    ->addTextField('acteur_absent_acteur_titre', 'Maire')
                    ->addTextField('acteur_absent_acteur_position', '3')
                    ->addTextField('acteur_absent_acteur_telmobile', '0602030406')
                )->getPart(),
            ],
        ];
    }

    /**
     * @dataProvider dataProviderBuildOneFieldAtATime
     */
    public function testBuildOneFieldAtATime(?int $id, string $prefix, string $only, array $sections, PartType $expected)
    {
        $hasUserFieldDeclared = function ($key) use ($only): bool {
            return $key === $only;
        };
        $template = $this->getTemplateStub($hasUserFieldDeclared, false, $sections);
        $actual = (new ActorFusionPart($template, $id, $prefix))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuildOneFieldAtATime()
    {
        return [
            [
                1,
                'acteur_present',
                'acteur_present_acteur_salutation',
                [],
                ((new FusionBuilder())->addTextField('acteur_present_acteur_salutation', 'M'))->getPart(),
            ],
            [
                1,
                'acteur_present',
                'acteur_present_acteur_nom',
                [],
                ((new FusionBuilder())->addTextField('acteur_present_acteur_nom', 'ROUGE'))->getPart(),
            ],
            [
                1,
                'acteur_present',
                'acteur_present_acteur_prenom',
                [],
                ((new FusionBuilder())->addTextField('acteur_present_acteur_prenom', 'Théo'))->getPart(),
            ],
            [
                1,
                'acteur_present',
                'acteur_present_acteur_email',
                [],
                ((new FusionBuilder())->addTextField('acteur_present_acteur_email', 't.rouge@exemple.fr'))->getPart(),
            ],
            [
                1,
                'acteur_present',
                'acteur_present_acteur_titre',
                [],
                ((new FusionBuilder())->addTextField('acteur_present_acteur_titre', 'Maire'))->getPart(),
            ],
            [
                1,
                'acteur_present',
                'acteur_present_acteur_position',
                [],
                ((new FusionBuilder())->addTextField('acteur_present_acteur_position', '1'))->getPart(),
            ],
            [
                1,
                'acteur_present',
                'acteur_present_acteur_telmobile',
                [],
                ((new FusionBuilder())->addTextField('acteur_present_acteur_telmobile', '0602030405'))->getPart(),
            ],
        ];
    }
}
