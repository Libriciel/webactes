<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\RepresentativeFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class RepresentativeFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderBuildFull
     */
    public function testBuildFull(?int $id, ?string $prefix, PartType $expected)
    {
        $actual = (new RepresentativeFusionPart($this->getTemplateStub(), $id, $prefix))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuildFull()
    {
        return [
            [
                1,
                'president_seance',
                (
                (new FusionBuilder())
                    ->addTextField('president_seance_salutation', 'M')
                    ->addTextField('president_seance_nom', 'ROUGE')
                    ->addTextField('president_seance_prenom', 'Théo')
                    ->addTextField('president_seance_email', 't.rouge@exemple.fr')
                    ->addTextField('president_seance_titre', 'Maire')
                    ->addTextField('president_seance_telmobile', '0602030405')
                    ->addTextField('president_seance_adresse1', '6 rue des acassias')
                    ->addTextField('president_seance_adresse2', '')
                    ->addTextField('president_seance_cp', '34000')
                    ->addTextField('president_seance_ville', 'Montpellier')
                    ->addTextField('president_seance_telfixe', '0467659644')
                )->getPart(),
            ],
            [
                2,
                'secretaire_seance',
                (
                (new FusionBuilder())
                    ->addTextField('secretaire_seance_salutation', 'M')
                    ->addTextField('secretaire_seance_nom', 'ROUGE2')
                    ->addTextField('secretaire_seance_prenom', 'Théo2')
                    ->addTextField('secretaire_seance_email', 't.rouge2@exemple.fr')
                    ->addTextField('secretaire_seance_titre', 'Maire')
                    ->addTextField('secretaire_seance_telmobile', '0602030406')
                    ->addTextField('secretaire_seance_adresse1', '6bis rue des acassias')
                    ->addTextField('secretaire_seance_adresse2', '')
                    ->addTextField('secretaire_seance_cp', '34000')
                    ->addTextField('secretaire_seance_ville', 'Montpellier')
                    ->addTextField('secretaire_seance_telfixe', '0467659645')
                )->getPart(),
            ],
            [
                3,
                'president',
                (
                (new FusionBuilder())
                    ->addTextField('president_salutation', 'M')
                    ->addTextField('president_nom', 'ROUGE3')
                    ->addTextField('president_prenom', 'Théo2')
                    ->addTextField('president_email', 't.rouge3@exemple.fr')
                    ->addTextField('president_titre', 'Maire')
                    ->addTextField('president_telmobile', '0602030406')
                    ->addTextField('president_adresse1', '6bis rue des acassias')
                    ->addTextField('president_adresse2', '')
                    ->addTextField('president_cp', '34000')
                    ->addTextField('president_ville', 'Montpellier')
                    ->addTextField('president_telfixe', '0467659645')
                )->getPart(),
            ],
        ];
    }

    /**
     * @dataProvider dataProviderBuildOneFieldAtATime
     */
    public function testBuildOneFieldAtATime(?int $id, string $prefix, string $only, PartType $expected)
    {
        $hasUserFieldDeclared = function ($key) use ($only): bool {
            return $key === $only;
        };
        $template = $this->getTemplateStub($hasUserFieldDeclared);
        $actual = (new RepresentativeFusionPart($template, $id, $prefix))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuildOneFieldAtATime()
    {
        return [
            [
                1,
                'president',
                'president_salutation',
                ((new FusionBuilder())->addTextField('president_salutation', 'M'))->getPart(),
            ],
            [
                1,
                'president',
                'president_nom',
                ((new FusionBuilder())->addTextField('president_nom', 'ROUGE'))->getPart(),
            ],
            [
                1,
                'president',
                'president_prenom',
                ((new FusionBuilder())->addTextField('president_prenom', 'Théo'))->getPart(),
            ],
            [
                1,
                'president',
                'president_email',
                ((new FusionBuilder())->addTextField('president_email', 't.rouge@exemple.fr'))->getPart(),
            ],
            [
                1,
                'president',
                'president_titre',
                ((new FusionBuilder())->addTextField('president_titre', 'Maire'))->getPart(),
            ],
            [
                1,
                'president',
                'president_telmobile',
                ((new FusionBuilder())->addTextField('president_telmobile', '0602030405'))->getPart(),
            ],
            [
                1,
                'president',
                'president_adresse1',
                ((new FusionBuilder())->addTextField('president_adresse1', '6 rue des acassias'))->getPart(),
            ],
            [
                1,
                'president',
                'president_adresse2',
                ((new FusionBuilder())->addTextField('president_adresse2', ''))->getPart(),
            ],
            [
                1,
                'president',
                'president_cp',
                ((new FusionBuilder())->addTextField('president_cp', '34000'))->getPart(),
            ],
            [
                1,
                'president',
                'president_ville',
                ((new FusionBuilder())->addTextField('president_ville', 'Montpellier'))->getPart(),
            ],
            [
                1,
                'president',
                'president_telfixe',
                ((new FusionBuilder())->addTextField('president_telfixe', '0467659644'))->getPart(),
            ],
        ];
    }
}
