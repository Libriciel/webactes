<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\SittingsActsFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class SittingsActsFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderBuildFull
     */
    public function testBuildFull(?int $id, PartType $expected)
    {
        $actual = (new SittingsActsFusionPart($this->getTemplateStub(), $id))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuildFull()
    {
        return [
            [
                1,
                (
                (new FusionBuilder())
                    ->addIteration(
                        'Projets',
                        [
                            (
                            (new FusionBuilder())
                                ->addStringField('libelle', 'Projet brouillon créé par 4')
                                ->addTextField('classification', '')
                                ->addDateField('date_creation', '11/02/2019')
                                ->addTextField('theme', 'Administration Générale')
                                ->addTextField('theme_critere_trie', 'Lorem')
                                ->addTextField('T1_theme', 'Administration Générale')
                                ->addTextField('theme1', 'Administration Générale')
                                ->addTextField('theme2', '')
                                ->addTextField('theme3', '')
                                ->addTextField('theme4', '')
                                ->addTextField('theme5', '')
                                ->addTextField('theme6', '')
                                ->addTextField('theme7', '')
                                ->addTextField('theme8', '')
                                ->addTextField('theme9', '')
                                ->addTextField('theme10', '')
                                ->addOdtContent(
                                    'texte_projet',
                                    'texte_projet.odt',
                                    $this->getFileContent(TESTS . 'Fixture/files/project_texts/gabarit_projet_v1.odt')
                                )
                                ->addOdtContent(
                                    'texte_acte',
                                    'texte_acte.odt',
                                    $this->getFileContent(TESTS . 'Fixture/files/project_texts/gabarit_acte_v1.odt')
                                )
                                ->addTextField('annexes_nombre', '0')
                                ->addTextField('type_acte', 'Délibération')
                                ->addTextField('seances_nombre', '2')
                                ->addIteration(
                                    'Seances',
                                    [
                                        (
                                        (new FusionBuilder())
                                            ->addDateField('seances_date', '10/02/2019')
                                            ->addTextField('seances_date_lettres', 'L\'an deux mille dix neuf, le dix  février ')
                                            ->addTextField('seances_hh', '16')
                                            ->addTextField('seances_mm', '01')
                                            ->addTextField('seances_position', '1')
                                            ->addTextField('seances_type', 'Conseil Municipal')
                                            ->addDateField('seances_date_convocation', '10/02/2019')
                                            ->addStringField('seances_lieu', 'Sale du conseil municipal de la mairie de Libriciel SCOP à Castelnau-le-Lez')
                                        )->getPart(),
                                        (
                                        (new FusionBuilder())
                                            ->addDateField('seances_date', '21/02/2019')
                                            ->addTextField('seances_date_lettres', 'L\'an deux mille dix neuf, le vingt et un février ')
                                            ->addTextField('seances_hh', '15')
                                            ->addTextField('seances_mm', '34')
                                            ->addTextField('seances_position', '1')
                                            ->addTextField('seances_type', 'Bureau')
                                            ->addDateField('seances_date_convocation', '21/02/2019')
                                            ->addStringField('seances_lieu', '')
                                        )->getPart(),
                                    ]
                                )
                                ->addDateField('seance_deliberante_date', '10/02/2019')
                                ->addTextField('seance_deliberante_type', 'Conseil Municipal')
                                ->addTextField('seance_deliberante_position', '1')
                                ->addDateField('seance_deliberante_date_convocation', '10/02/2019')
                                ->addStringField('seance_deliberante_lieu', 'Sale du conseil municipal de la mairie de Libriciel SCOP à Castelnau-le-Lez')
                                ->addTextField('numero_acte', 'ACT_151')
                                ->addTextField('acte_signature', '0')
                                ->addDateField('acte_signature_date', '')
                                ->addTextField('convocation_date_envoi', '')
                                ->addTextField('acteur_present_nombre', '1')
                                ->addIteration(
                                    'ActeursPresents',
                                    [
                                        (
                                        (new FusionBuilder())
                                            ->addTextField('acteur_present_acteur_salutation', 'M')
                                            ->addTextField('acteur_present_acteur_nom', 'ROUGE2')
                                            ->addTextField('acteur_present_acteur_prenom', 'Théo2')
                                            ->addTextField('acteur_present_acteur_email', 't.rouge2@exemple.fr')
                                            ->addTextField('acteur_present_acteur_titre', 'Maire')
                                            ->addTextField('acteur_present_acteur_position', '1')
                                            ->addTextField('acteur_present_acteur_telmobile', '0602030406')
                                        )->getPart(),
                                    ]
                                )
                                ->addTextField('acteur_mandataire_nombre', '1')
                                ->addIteration(
                                    'ActeursMandataires',
                                    [
                                        (
                                        (new FusionBuilder())
                                            ->addTextField('acteur_mandant_acteur_salutation', 'M')
                                            ->addTextField('acteur_mandant_acteur_nom', 'ROUGE3')
                                            ->addTextField('acteur_mandant_acteur_prenom', 'Théo2')
                                            ->addTextField('acteur_mandant_acteur_email', 't.rouge3@exemple.fr')
                                            ->addTextField('acteur_mandant_acteur_titre', 'Maire')
                                            ->addTextField('acteur_mandant_acteur_position', '2')
                                            ->addTextField('acteur_mandant_acteur_telmobile', '0602030406')
                                            ->addTextField('acteur_mandataire_acteur_salutation', 'M')
                                            ->addTextField('acteur_mandataire_acteur_nom', 'ROUGE4')
                                            ->addTextField('acteur_mandataire_acteur_prenom', 'Théo4')
                                            ->addTextField('acteur_mandataire_acteur_email', 't.rouge4@exemple.fr')
                                            ->addTextField('acteur_mandataire_acteur_titre', 'Maire')
                                            ->addTextField('acteur_mandataire_acteur_position', '3')
                                            ->addTextField('acteur_mandataire_acteur_telmobile', '0602030406')
                                        )->getPart(),
                                    ]
                                )
                                ->addTextField('acteur_absent_nombre', '1')
                                ->addIteration(
                                    'ActeursAbsents',
                                    [
                                        (
                                        (new FusionBuilder())
                                            ->addTextField('acteur_absent_acteur_salutation', 'M')
                                            ->addTextField('acteur_absent_acteur_nom', 'ROUGE')
                                            ->addTextField('acteur_absent_acteur_prenom', 'Théo')
                                            ->addTextField('acteur_absent_acteur_email', 't.rouge@exemple.fr')
                                            ->addTextField('acteur_absent_acteur_titre', 'Maire')
                                            ->addTextField('acteur_absent_acteur_position', '1')
                                            ->addTextField('acteur_absent_acteur_telmobile', '0602030405')
                                        )->getPart(),
                                    ]
                                )
                                ->addTextField('vote_type', 'DETAIL')
                                ->addTextField('acte_adopte', '1')
                                ->addTextField('nombre_pour', '3')
                                ->addTextField('nombre_abstention', '0')
                                ->addTextField('nombre_contre', '0')
                                ->addTextField('nombre_sans_participation', '0')
                                ->addTextField('nombre_votant', '3')
                                ->addTextField('commentaire_vote', 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.')
                                ->addTextField('president_vote_salutation', 'M')
                                ->addTextField('president_vote_nom', 'ROUGE')
                                ->addTextField('president_vote_prenom', 'Théo')
                                ->addTextField('president_vote_email', 't.rouge@exemple.fr')
                                ->addTextField('president_vote_titre', 'Maire')
                                ->addTextField('president_vote_telmobile', '0602030405')
                                ->addTextField('president_vote_adresse1', '6 rue des acassias')
                                ->addTextField('president_vote_adresse2', '')
                                ->addTextField('president_vote_cp', '34000')
                                ->addTextField('president_vote_ville', 'Montpellier')
                                ->addTextField('president_vote_telfixe', '0467659644')
                                ->addTextField('president_seance_salutation', 'M')
                                ->addTextField('president_seance_nom', 'ROUGE')
                                ->addTextField('president_seance_prenom', 'Théo')
                                ->addTextField('president_seance_email', 't.rouge@exemple.fr')
                                ->addTextField('president_seance_titre', 'Maire')
                                ->addTextField('president_seance_telmobile', '0602030405')
                                ->addTextField('president_seance_adresse1', '6 rue des acassias')
                                ->addTextField('president_seance_adresse2', '')
                                ->addTextField('president_seance_cp', '34000')
                                ->addTextField('president_seance_ville', 'Montpellier')
                                ->addTextField('president_seance_telfixe', '0467659644')
                                ->addTextField('secretaire_seance_salutation', 'M')
                                ->addTextField('secretaire_seance_nom', 'ROUGE')
                                ->addTextField('secretaire_seance_prenom', 'Théo')
                                ->addTextField('secretaire_seance_email', 't.rouge@exemple.fr')
                                ->addTextField('secretaire_seance_titre', 'Maire')
                                ->addTextField('secretaire_seance_telmobile', '0602030405')
                                ->addTextField('secretaire_seance_adresse1', '6 rue des acassias')
                                ->addTextField('secretaire_seance_adresse2', '')
                                ->addTextField('secretaire_seance_cp', '34000')
                                ->addTextField('secretaire_seance_ville', 'Montpellier')
                                ->addTextField('secretaire_seance_telfixe', '0467659644')
                                ->addIteration('ActeursContre', $this->getEmptyActorsList('acteur_contre'))
                                ->addTextField('nombre_acteur_contre', '0')
                                ->addIteration(
                                    'ActeursPour',
                                    [
                                        (
                                        (new FusionBuilder())
                                            ->addTextField('acteur_pour_acteur_salutation', 'M')
                                            ->addTextField('acteur_pour_acteur_nom', 'ROUGE')
                                            ->addTextField('acteur_pour_acteur_prenom', 'Théo')
                                            ->addTextField('acteur_pour_acteur_email', 't.rouge@exemple.fr')
                                            ->addTextField('acteur_pour_acteur_titre', 'Maire')
                                            ->addTextField('acteur_pour_acteur_position', '1')
                                            ->addTextField('acteur_pour_acteur_telmobile', '0602030405')
                                        )->getPart(),
                                        (
                                        (new FusionBuilder())
                                            ->addTextField('acteur_pour_acteur_salutation', 'M')
                                            ->addTextField('acteur_pour_acteur_nom', 'ROUGE2')
                                            ->addTextField('acteur_pour_acteur_prenom', 'Théo2')
                                            ->addTextField('acteur_pour_acteur_email', 't.rouge2@exemple.fr')
                                            ->addTextField('acteur_pour_acteur_titre', 'Maire')
                                            ->addTextField('acteur_pour_acteur_position', '1')
                                            ->addTextField('acteur_pour_acteur_telmobile', '0602030406')
                                        )->getPart(),
                                        (
                                        (new FusionBuilder())
                                            ->addTextField('acteur_pour_acteur_salutation', 'M')
                                            ->addTextField('acteur_pour_acteur_nom', 'ROUGE3')
                                            ->addTextField('acteur_pour_acteur_prenom', 'Théo2')
                                            ->addTextField('acteur_pour_acteur_email', 't.rouge3@exemple.fr')
                                            ->addTextField('acteur_pour_acteur_titre', 'Maire')
                                            ->addTextField('acteur_pour_acteur_position', '2')
                                            ->addTextField('acteur_pour_acteur_telmobile', '0602030406')
                                        )->getPart(),
                                    ]
                                )
                                ->addTextField('nombre_acteur_pour', '3')
                                ->addIteration(
                                    'ActeursAbstention',
                                    $this->getEmptyActorsList('acteur_abstention')
                                )
                                ->addTextField('nombre_acteur_abstention', '0')
                                ->addIteration(
                                    'ActeursSansParticipation',
                                    $this->getEmptyActorsList('acteur_sans_participation')
                                )
                                ->addTextField('nombre_acteur_sans_participation', '0')
                            )->getPart(),
                            (
                            (new FusionBuilder())
                                ->addStringField('libelle', 'Projet validable par 4,5 créé par 4, avec sitting')
                                ->addTextField('classification', '')
                                ->addDateField('date_creation', '11/02/2019')
                                ->addTextField('theme', 'Administration Générale')
                                ->addTextField('theme_critere_trie', 'Lorem')
                                ->addTextField('T1_theme', 'Administration Générale')
                                ->addTextField('theme1', 'Administration Générale')
                                ->addTextField('theme2', '')
                                ->addTextField('theme3', '')
                                ->addTextField('theme4', '')
                                ->addTextField('theme5', '')
                                ->addTextField('theme6', '')
                                ->addTextField('theme7', '')
                                ->addTextField('theme8', '')
                                ->addTextField('theme9', '')
                                ->addTextField('theme10', '')
                                ->addOdtContent(
                                    'texte_projet',
                                    'texte_projet.odt',
                                    $this->getFileContent($this->pathFile . 'odt' . DS . 'OdtVide.odt')
                                )
                                ->addOdtContent(
                                    'texte_acte',
                                    'texte_acte.odt',
                                    $this->getFileContent($this->pathFile . 'odt' . DS . 'OdtVide.odt')
                                )
                                ->addTextField('annexes_nombre', '0')
                                ->addTextField('type_acte', 'Délibération')
                                ->addTextField('seances_nombre', '1')
                                ->addIteration(
                                    'Seances',
                                    [
                                        (
                                        (new FusionBuilder())
                                            ->addDateField('seances_date', '21/02/2019')
                                            ->addTextField('seances_date_lettres', 'L\'an deux mille dix neuf, le vingt et un février ')
                                            ->addTextField('seances_hh', '15')
                                            ->addTextField('seances_mm', '34')
                                            ->addTextField('seances_position', '2')
                                            ->addTextField('seances_type', 'Bureau')
                                            ->addDateField('seances_date_convocation', '21/02/2019')
                                            ->addStringField('seances_lieu', '')
                                        )->getPart(),
                                    ]
                                )
                                ->addDateField('seance_deliberante_date', '')
                                ->addTextField('seance_deliberante_type', '')
                                ->addTextField('seance_deliberante_position', '')
                                ->addTextField('seance_deliberante_date_convocation', '')
                                ->addStringField('seance_deliberante_lieu', '')
                                ->addTextField('numero_acte', '')
                                ->addTextField('acte_signature', '0')
                                ->addDateField('acte_signature_date', '')
                                ->addTextField('convocation_date_envoi', '')
                                ->addTextField('acteur_present_nombre', '1')
                                ->addIteration(
                                    'ActeursPresents',
                                    [
                                        (
                                        (new FusionBuilder())
                                            ->addTextField('acteur_present_acteur_salutation', 'M')
                                            ->addTextField('acteur_present_acteur_nom', 'ROUGE2')
                                            ->addTextField('acteur_present_acteur_prenom', 'Théo2')
                                            ->addTextField('acteur_present_acteur_email', 't.rouge2@exemple.fr')
                                            ->addTextField('acteur_present_acteur_titre', 'Maire')
                                            ->addTextField('acteur_present_acteur_position', '1')
                                            ->addTextField('acteur_present_acteur_telmobile', '0602030406')
                                        )->getPart(),
                                    ]
                                )
                                ->addTextField('acteur_mandataire_nombre', '1')
                                ->addIteration(
                                    'ActeursMandataires',
                                    [
                                        (
                                        (new FusionBuilder())
                                            ->addTextField('acteur_mandant_acteur_salutation', 'M')
                                            ->addTextField('acteur_mandant_acteur_nom', 'ROUGE3')
                                            ->addTextField('acteur_mandant_acteur_prenom', 'Théo2')
                                            ->addTextField('acteur_mandant_acteur_email', 't.rouge3@exemple.fr')
                                            ->addTextField('acteur_mandant_acteur_titre', 'Maire')
                                            ->addTextField('acteur_mandant_acteur_position', '2')
                                            ->addTextField('acteur_mandant_acteur_telmobile', '0602030406')
                                            ->addTextField('acteur_mandataire_acteur_salutation', 'M')
                                            ->addTextField('acteur_mandataire_acteur_nom', 'ROUGE')
                                            ->addTextField('acteur_mandataire_acteur_prenom', 'Théo')
                                            ->addTextField('acteur_mandataire_acteur_email', 't.rouge@exemple.fr')
                                            ->addTextField('acteur_mandataire_acteur_titre', 'Maire')
                                            ->addTextField('acteur_mandataire_acteur_position', '1')
                                            ->addTextField('acteur_mandataire_acteur_telmobile', '0602030405')
                                        )->getPart(),
                                    ]
                                )
                                ->addTextField('acteur_absent_nombre', '1')
                                ->addIteration(
                                    'ActeursAbsents',
                                    [
                                        (
                                        (new FusionBuilder())
                                            ->addTextField('acteur_absent_acteur_salutation', 'M')
                                            ->addTextField('acteur_absent_acteur_nom', 'ROUGE')
                                            ->addTextField('acteur_absent_acteur_prenom', 'Théo')
                                            ->addTextField('acteur_absent_acteur_email', 't.rouge@exemple.fr')
                                            ->addTextField('acteur_absent_acteur_titre', 'Maire')
                                            ->addTextField('acteur_absent_acteur_position', '1')
                                            ->addTextField('acteur_absent_acteur_telmobile', '0602030405')
                                        )->getPart(),
                                    ]
                                )
                                ->addIteration('ActeursContre', $this->getEmptyActorsList('acteur_contre'))
                                ->addTextField('nombre_acteur_contre', '0')
                                ->addIteration('ActeursPour', $this->getEmptyActorsList('acteur_pour'))
                                ->addTextField('nombre_acteur_pour', '0')
                                ->addIteration('ActeursAbstention', $this->getEmptyActorsList('acteur_abstention'))
                                ->addTextField('nombre_acteur_abstention', '0')
                                ->addIteration('ActeursSansParticipation', $this->getEmptyActorsList('acteur_sans_participation'))
                                ->addTextField('nombre_acteur_sans_participation', '0')
                            )->getPart(),
                            (
                            (new FusionBuilder())
                                ->addStringField('libelle', 'Projet validable par 5 avec 4 validateur créé par 4, avec sitting')
                                ->addTextField('classification', '')
                                ->addDateField('date_creation', '11/02/2019')
                                ->addTextField('theme', 'Administration Générale')
                                ->addTextField('theme_critere_trie', 'Lorem')
                                ->addTextField('T1_theme', 'Administration Générale')
                                ->addTextField('theme1', 'Administration Générale')
                                ->addTextField('theme2', '')
                                ->addTextField('theme3', '')
                                ->addTextField('theme4', '')
                                ->addTextField('theme5', '')
                                ->addTextField('theme6', '')
                                ->addTextField('theme7', '')
                                ->addTextField('theme8', '')
                                ->addTextField('theme9', '')
                                ->addTextField('theme10', '')
                                ->addOdtContent(
                                    'texte_projet',
                                    'texte_projet.odt',
                                    $this->getFileContent($this->pathFile . 'odt' . DS . 'OdtVide.odt')
                                )
                                ->addOdtContent(
                                    'texte_acte',
                                    'texte_acte.odt',
                                    $this->getFileContent($this->pathFile . 'odt' . DS . 'OdtVide.odt')
                                )
                                ->addTextField('annexes_nombre', '0')
                                ->addTextField('type_acte', 'Délibération')
                                ->addTextField('seances_nombre', '1')
                                ->addIteration(
                                    'Seances',
                                    [
                                        (
                                        (new FusionBuilder())
                                            ->addDateField('seances_date', '21/02/2019')
                                            ->addTextField('seances_date_lettres', 'L\'an deux mille dix neuf, le vingt et un février ')
                                            ->addTextField('seances_hh', '15')
                                            ->addTextField('seances_mm', '34')
                                            ->addTextField('seances_position', '3')
                                            ->addTextField('seances_type', 'Bureau')
                                            ->addDateField('seances_date_convocation', '21/02/2019')
                                            ->addStringField('seances_lieu', '')
                                        )->getPart(),
                                    ]
                                )
                                ->addDateField('seance_deliberante_date', '')
                                ->addTextField('seance_deliberante_type', '')
                                ->addTextField('seance_deliberante_position', '')
                                ->addTextField('seance_deliberante_date_convocation', '')
                                ->addStringField('seance_deliberante_lieu', '')
                                ->addTextField('numero_acte', '')
                                ->addTextField('acte_signature', '0')
                                ->addDateField('acte_signature_date', '')
                                ->addTextField('convocation_date_envoi', '')
                                ->addTextField('acteur_present_nombre', '0')
                                ->addIteration('ActeursPresents', $this->getEmptyActorsList('acteur_present'))
                                ->addTextField('acteur_mandataire_nombre', '0')
                                ->addIteration('ActeursMandataires', $this->getEmptyActorsList('acteur_mandant', 'acteur_mandataire'))
                                ->addTextField('acteur_absent_nombre', '0')
                                ->addIteration('ActeursAbsents', $this->getEmptyActorsList('acteur_absent'))
                                ->addIteration('ActeursContre', $this->getEmptyActorsList('acteur_contre'))
                                ->addTextField('nombre_acteur_contre', '0')
                                ->addIteration('ActeursPour', $this->getEmptyActorsList('acteur_pour'))
                                ->addTextField('nombre_acteur_pour', '0')
                                ->addIteration('ActeursAbstention', $this->getEmptyActorsList('acteur_abstention'))
                                ->addTextField('nombre_acteur_abstention', '0')
                                ->addIteration('ActeursSansParticipation', $this->getEmptyActorsList('acteur_sans_participation'))
                                ->addTextField('nombre_acteur_sans_participation', '0')
                            )->getPart(),
                        ],
                    )
                )->getPart(),
            ],
        ];
    }
}
