<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\VoteFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class VoteFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderBuildFull
     */
    public function testBuildFull(?int $sittingId, ?int $projectId, PartType $expected)
    {
        $actual = (new VoteFusionPart($this->getTemplateStub(), $sittingId, $projectId))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuildFull()
    {
        return [
            [
                1,
                1,
                (
                (new FusionBuilder())
                    ->addTextField('vote_type', 'DETAIL')
                    ->addTextField('acte_adopte', '1')
                    ->addTextField('nombre_pour', '3')
                    ->addTextField('nombre_abstention', '0')
                    ->addTextField('nombre_contre', '0')
                    ->addTextField('nombre_sans_participation', '0')
                    ->addTextField('nombre_votant', '3')
                    ->addTextField('commentaire_vote', 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.')
                    ->addTextField('president_vote_salutation', 'M')
                    ->addTextField('president_vote_nom', 'ROUGE')
                    ->addTextField('president_vote_prenom', 'Théo')
                    ->addTextField('president_vote_email', 't.rouge@exemple.fr')
                    ->addTextField('president_vote_titre', 'Maire')
                    ->addTextField('president_vote_telmobile', '0602030405')
                    ->addTextField('president_vote_adresse1', '6 rue des acassias')
                    ->addTextField('president_vote_adresse2', '')
                    ->addTextField('president_vote_cp', '34000')
                    ->addTextField('president_vote_ville', 'Montpellier')
                    ->addTextField('president_vote_telfixe', '0467659644')
                    ->addTextField('president_seance_salutation', 'M')
                    ->addTextField('president_seance_nom', 'ROUGE')
                    ->addTextField('president_seance_prenom', 'Théo')
                    ->addTextField('president_seance_email', 't.rouge@exemple.fr')
                    ->addTextField('president_seance_titre', 'Maire')
                    ->addTextField('president_seance_telmobile', '0602030405')
                    ->addTextField('president_seance_adresse1', '6 rue des acassias')
                    ->addTextField('president_seance_adresse2', '')
                    ->addTextField('president_seance_cp', '34000')
                    ->addTextField('president_seance_ville', 'Montpellier')
                    ->addTextField('president_seance_telfixe', '0467659644')
                    ->addTextField('secretaire_seance_salutation', 'M')
                    ->addTextField('secretaire_seance_nom', 'ROUGE')
                    ->addTextField('secretaire_seance_prenom', 'Théo')
                    ->addTextField('secretaire_seance_email', 't.rouge@exemple.fr')
                    ->addTextField('secretaire_seance_titre', 'Maire')
                    ->addTextField('secretaire_seance_telmobile', '0602030405')
                    ->addTextField('secretaire_seance_adresse1', '6 rue des acassias')
                    ->addTextField('secretaire_seance_adresse2', '')
                    ->addTextField('secretaire_seance_cp', '34000')
                    ->addTextField('secretaire_seance_ville', 'Montpellier')
                    ->addTextField('secretaire_seance_telfixe', '0467659644')
                )->getPart(),
            ],
        ];
    }

    /**
     * @dataProvider dataProviderBuildOneFieldAtATime
     */
    public function testBuildOneFieldAtATime(?int $sittingId, ?int $projectId, string $only, PartType $expected)
    {
        $hasUserFieldDeclared = function ($key) use ($only): bool {
            return $key === $only;
        };
        $template = $this->getTemplateStub($hasUserFieldDeclared);
        $actual = (new VoteFusionPart($template, $sittingId, $projectId))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuildOneFieldAtATime()
    {
        return [
            [
                1,
                1,
                'vote_type',
                ((new FusionBuilder())->addTextField('vote_type', 'DETAIL'))->getPart(),
            ],
            [
                1,
                1,
                'acte_adopte',
                ((new FusionBuilder())->addTextField('acte_adopte', '1'))->getPart(),
            ],
            [
                1,
                1,
                'prendre_acte',
                (new FusionBuilder())->getPart(),
            ],
            [
                1,
                1,
                'nombre_pour',
                ((new FusionBuilder())->addTextField('nombre_pour', '3'))->getPart(),
            ],
            [
                1,
                1,
                'nombre_abstention',
                ((new FusionBuilder())->addTextField('nombre_abstention', '0'))->getPart(),
            ],
            [
                1,
                1,
                'nombre_contre',
                ((new FusionBuilder())->addTextField('nombre_contre', '0'))->getPart(),
            ],
            [
                1,
                1,
                'nombre_sans_participation',
                ((new FusionBuilder())->addTextField('nombre_sans_participation', '0'))->getPart(),
            ],
            [
                1,
                1,
                'nombre_votant',
                ((new FusionBuilder())->addTextField('nombre_votant', '3'))->getPart(),
            ],
            [
                1,
                1,
                'commentaire_vote',
                ((new FusionBuilder())->addTextField('commentaire_vote', 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.'))->getPart(),
            ],
        ];
    }
}
