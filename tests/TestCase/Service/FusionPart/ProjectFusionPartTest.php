<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service\FusionPart;

use App\Service\FusionPart\ProjectFusionPart;
use App\TestSuite\LoadOncePerClassFixturesTrait;
use Libriciel\OfficeClients\Fusion\Helper\Builder as FusionBuilder;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class ProjectFusionPartTest extends AbstractFusionPartTestCase
{
    use LoadOncePerClassFixturesTrait;

    /**
     * @dataProvider dataProviderBuild
     */
    public function testBuild(?int $id, PartType $expected)
    {
        $actual = (new ProjectFusionPart($this->getTemplateStub(), $id))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuild()
    {
        return [
            [
                1,
                (
                (new FusionBuilder())
                    ->addStringField('libelle', 'Projet brouillon créé par 4')
                    ->addTextField('classification', '')
                    ->addDateField('date_creation', '11/02/2019')
                    ->addTextField('theme', 'Administration Générale')
                    ->addTextField('theme_critere_trie', 'Lorem')
                    ->addTextField('T1_theme', 'Administration Générale')
                    ->addTextField('theme1', 'Administration Générale')
                    ->addTextField('theme2', '')
                    ->addTextField('theme3', '')
                    ->addTextField('theme4', '')
                    ->addTextField('theme5', '')
                    ->addTextField('theme6', '')
                    ->addTextField('theme7', '')
                    ->addTextField('theme8', '')
                    ->addTextField('theme9', '')
                    ->addTextField('theme10', '')
                    ->addTextField('annexes_nombre', '0')
                    ->addTextField('type_acte', 'Délibération')
                    ->addTextField('seances_nombre', '2')
                    ->addIteration(
                        'Seances',
                        [
                            (
                            (new FusionBuilder())
                                ->addDateField('seances_date', '10/02/2019')
                                ->addTextField('seances_date_lettres', 'L\'an deux mille dix neuf, le dix  février ')
                                ->addTextField('seances_hh', '16')
                                ->addTextField('seances_mm', '01')
                                ->addTextField('seances_position', '1')
                                ->addTextField('seances_type', 'Conseil Municipal')
                                ->addDateField('seances_date_convocation', '10/02/2019')
                                ->addStringField('seances_lieu', 'Sale du conseil municipal de la mairie de Libriciel SCOP à Castelnau-le-Lez')
                            )->getPart(),
                            (
                            (new FusionBuilder())
                                ->addDateField('seances_date', '21/02/2019')
                                ->addTextField('seances_date_lettres', 'L\'an deux mille dix neuf, le vingt et un février ')
                                ->addTextField('seances_hh', '15')
                                ->addTextField('seances_mm', '34')
                                ->addTextField('seances_position', '1')
                                ->addTextField('seances_type', 'Bureau')
                                ->addDateField('seances_date_convocation', '21/02/2019')
                                ->addStringField('seances_lieu', '')
                            )->getPart(),
                        ]
                    )
                    ->addDateField('seance_deliberante_date', '10/02/2019')
                    ->addTextField('seance_deliberante_type', 'Conseil Municipal')
                    ->addTextField('seance_deliberante_position', '1')
                    ->addDateField('seance_deliberante_date_convocation', '10/02/2019')
                    ->addStringField('seance_deliberante_lieu', 'Sale du conseil municipal de la mairie de Libriciel SCOP à Castelnau-le-Lez')
                    ->addOdtContent(
                        'texte_projet',
                        'texte_projet.odt',
                        $this->getFileContent(TESTS . 'Fixture/files/project_texts/gabarit_projet_v1.odt')
                    )
                    ->addOdtContent(
                        'texte_acte',
                        'texte_acte.odt',
                        $this->getFileContent(TESTS . 'Fixture/files/project_texts/gabarit_acte_v1.odt')
                    )
                )->getPart(),
            ],
        ];
    }

    /**
     * @dataProvider dataProviderBuildOneFieldAtATime
     */
    public function testBuildOneFieldAtATime(?int $id, string $only, array $sections, PartType $expected)
    {
        $hasUserFieldDeclared = function ($key) use ($only): bool {
            return $key === $only;
        };
        $template = $this->getTemplateStub($hasUserFieldDeclared, false, $sections);
        $actual = (new ProjectFusionPart($template, $id))->build();
        $this->assertEquals($expected, $actual);
    }

    public function dataProviderBuildOneFieldAtATime()
    {
        $completeBase = function (PartType $part) {
            return (
            (new FusionBuilder($part))
                ->addTextField('T1_theme', 'Administration Générale')
                ->addTextField('theme1', 'Administration Générale')
                ->addTextField('theme2', '')
                ->addTextField('theme3', '')
                ->addTextField('theme4', '')
                ->addTextField('theme5', '')
                ->addTextField('theme6', '')
                ->addTextField('theme7', '')
                ->addTextField('theme8', '')
                ->addTextField('theme9', '')
                ->addTextField('theme10', '')
                ->addTextField('seances_nombre', '2')
                ->addIteration(
                    'Seances',
                    [
                        ((new FusionBuilder()))->getPart(),
                        ((new FusionBuilder()))->getPart(),
                    ]
                )
                ->addOdtContent(
                    'texte_projet',
                    'texte_projet.odt',
                    $this->getFileContent($this->pathFile . 'odt' . DS . 'OdtVide.odt')
                )
                ->addOdtContent(
                    'texte_acte',
                    'texte_acte.odt',
                    $this->getFileContent($this->pathFile . 'odt' . DS . 'OdtVide.odt')
                )
            )->getPart();
        };

        return [
            [
                1,
                'libelle',
                [],
                $completeBase(((new FusionBuilder())
                    ->addStringField('libelle', 'Projet brouillon créé par 4'))->getPart()),
            ],
            [
                1,
                'classification',
                [],
                $completeBase(((new FusionBuilder())->addTextField('classification', ''))->getPart()),
            ],
            [
                1,
                'date_creation',
                [],
                $completeBase(((new FusionBuilder())->addDateField('date_creation', '11/02/2019'))->getPart()),
            ],
        ];
    }
}
