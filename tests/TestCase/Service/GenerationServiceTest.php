<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service;

use App\Model\Enum\DocumentType;
use App\Service\GenerationsService;
use App\Test\TestCase\AbstractTestCase;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\TestSuite\IntegrationTestTrait;

class GenerationServiceTest extends AbstractTestCase
{
    use IntegrationTestTrait;
    use LocatorAwareTrait;
    use ServiceAwareTrait;

    /**
     * @var \App\Service\GenerationsService
     */
    private $GenerationsService;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.All',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->GenerationsService = new GenerationsService();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->GenerationsService);

        parent::tearDown();
    }

    /**
     * Data provider for testGetDocumentFileName method
     *
     * @return array
     */
    public function getDocumentFileNameDataProvider(): array
    {
        return [
            [DocumentType::DELIBERATIONS_LIST, 1, 'liste-des-deliberations-21-02-2019-15h34-1'],
            [DocumentType::VERBAL_TRIAL, 1, 'proces-verbal-21-02-2019-15h34-1'],
            [DocumentType::ACT, 1, 'ACT_151'], // acte avec numéro d'acte
            [DocumentType::ACT, 3, 'acte-3'], // acte sans numéro d'acte
            [DocumentType::PROJECT, 3, 'projet-3'], // projet sans numéro d'acte
            [DocumentType::PROJECT, 1, 'ACT_151'], // projet avec numéro d'acte
            [DocumentType::CONVOCATION, 1, 'document'], // convocation
            [DocumentType::EXECUTIVE_SUMMARY, 1, 'document'], // note de synthèse
            ['test', 1, 'document'],
        ];
    }

    /**
     * Test getDocumentFileName method
     *
     * @dataProvider getDocumentFileNameDataProvider
     * @param string $prefix
     * @param string $expected
     * @return void
     */
    public function testGetDocumentFileName(string $documentType, int $primaryKey, string $expected): void
    {
        $result = $this->GenerationsService->getDocumentFileName($documentType, $primaryKey);

        $this->assertSame($expected, $result);
    }
}
