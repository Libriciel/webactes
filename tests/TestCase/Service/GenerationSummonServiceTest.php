<?php
declare(strict_types=1);

namespace App\Test\TestCase\Service;

use App\Service\GenerateSummonsService;
use App\Test\TestCase\AbstractTestCase;
use Burzum\CakeServiceLayer\Service\ServiceAwareTrait;
use Cake\I18n\FrozenDate;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\TestSuite\IntegrationTestTrait;

class GenerationSummonServiceTest extends AbstractTestCase
{
    use IntegrationTestTrait;
    use LocatorAwareTrait;
    use ServiceAwareTrait;

    /**
     * @var \App\Service\GenerateSummonsService
     */
    private $generateSummonsService;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.group.All',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->generateSummonsService = new GenerateSummonsService();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->generateSummonsService);

        parent::tearDown();
    }

    /**
     * Data provider for generateDocumentFileName method
     *
     * @return array
     */
    public function generateFileNameDataProvider(): array
    {
        return [
            ['note-de-synthese', new FrozenDate(1550759640) , '1' ,'note-de-synthese-21-02-2019-00h00-1.pdf'],
            ['convocation', new FrozenDate(1550759640) , '1' ,'convocation-21-02-2019-00h00-1.pdf'],
        ];
    }

    /**
     * Test generateDocumentFileName method
     *
     * @dataProvider generateFileNameDataProvider
     * @param string $prefix
     * @param \DateTimeInterface $date
     * @param string $expected
     * @return void
     */
    public function testGenerateFileName(string $prefix, \DateTimeInterface $date, string $id, string $expected): void
    {
        $result = $this->generateSummonsService->generateFileName($prefix, $date, $id);
        $this->assertSame($expected, $result);
    }
}
