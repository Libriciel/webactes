<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SystemlogsFixture
 */
class SystemlogsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'keycloak_id' => 'aaaa',
                'plugin' => null,
                'controller' => 'Projects',
                'action' => 'add',
                'pass' => 'a:0:{}',
                'code' => 201,
                'message' => 'Ajout du projet "Nouvelle gare SNCF" (1) par "azdaae" (1)',
                'extra' => 'a:1:{i:0;i:1;}',
                'created' => 1549880905,
                'modified' => 1549880905,
            ],
            [
                'structure_id' => 1,
                'keycloak_id' => 'aaab',
                'plugin' => null,
                'controller' => 'Projects',
                'action' => 'edit',
                'pass' => 'a:1:{i:0;s:1:"1";}',
                'code' => 204,
                'message' => 'Modification du projet "Nouvelle gare SNCF" (1) par "aaaa" (2)',
                'extra' => 'a:1:{i:0;s:1:"1";}',
                'created' => 1549880905,
                'modified' => 1549880905,
            ],
            [
                'structure_id' => 1,
                'keycloak_id' => 'this_user_left',
                'plugin' => null,
                'controller' => 'Projects',
                'action' => 'edit',
                'pass' => 'a:1:{i:0;s:1:"1";}',
                'code' => 204,
                'message' => 'Modification du projet "Nouvelle gare SNCF" (1) par "this_user_left" (666)',
                'extra' => 'a:1:{i:0;s:1:"1";}',
                'created' => 1549880905,
                'modified' => 1549880905,
            ],
        ];

        parent::init();
    }
}
