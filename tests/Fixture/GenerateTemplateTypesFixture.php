<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GenerateTemplateTypesFixture
 */
class GenerateTemplateTypesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'name' => 'Projet',
                'code' => 'project',
                'description' => 'Génération d\'un projet d\'acte',
            ],
            [
                'name' => 'Acte',
                'code' => 'act',
                'description' => 'Génération d\'un acte',
            ],
            [
                'name' => 'Convocation',
                'code' => 'convocation',
                'description' => 'Génération d\'une convocation',
            ],
            [
                'name' => 'Note de synthèse',
                'code' => 'executive_summary',
                'description' => 'Génération d\'une note de synthèse',
            ],
            [
                'name' => 'Liste des délibérations',
                'code' => 'deliberations_list',
                'description' => 'Génération d\'une liste des délibérations',
            ],
            [
                'name' => 'Procès-verbal',
                'code' => 'verbal_trial',
                'description' => 'Génération d\'un procès-verbal',
            ],
        ];
        parent::init();
    }
}
