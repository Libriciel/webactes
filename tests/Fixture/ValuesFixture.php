<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ValuesFixture
 */
class ValuesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'template_id' => 1,
                'json' => '',
                'created' => 1549880911,
                'modified' => 1549880911,
            ],
        ];
        parent::init();
    }
}
