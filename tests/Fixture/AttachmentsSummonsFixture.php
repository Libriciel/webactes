<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * attachmentsSummonsFixture
 */
class AttachmentsSummonsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'attachment_id' => 1,
                'summon_id' => 1,
                'rank' => 1,
            ],
            [
                'attachment_id' => 2,
                'summon_id' => 1,
                'rank' => 2,
            ],
        ];
        parent::init();
    }
}
