<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DraftTemplateTypesFixture
 */
class DraftTemplateTypesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'name' => 'Projet',
                'code' => 'TXT_PROJ',
                'description' => 'Modèle de rédaction d\'un texte de projet',
            ],
            [
                'name' => 'Acte',
                'code' => 'TXT_ACT',
                'description' => 'Modèle de rédaction d\'un texte d\'acte',
            ],
        ];
        parent::init();
    }
}
