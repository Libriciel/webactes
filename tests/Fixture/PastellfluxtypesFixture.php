<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PastellfluxtypesFixture
 */
class PastellfluxtypesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => 1,
                'name' => 'actes-generique',
                'description' => '',
                'created' => 1562664911,
                'modified' => 1562664911,
            ],
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => 0,
                'name' => 'mailsec',
                'description' => '',
                'created' => 1562664911,
                'modified' => 1562664911,
            ],
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => 0,
                'name' => 'signature',
                'description' => '',
                'created' => 1562664911,
                'modified' => 1562664911,
            ],
        ];
        parent::init();
    }
}
