<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TabsFixture
 */
class TabsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'template_id' => 1,
                'position' => 1,
                'tab' => 'Lorem ip',
                'created' => 1549880908,
                'modified' => 1549880908,
            ],
        ];
        parent::init();
    }
}
