<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConvocationsAttachmentsFixture
 */
class ConvocationsAttachmentsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'convocation_id' => 1,
                'attachment_id' => 1,
            ],
        ];
        parent::init();
    }
}
