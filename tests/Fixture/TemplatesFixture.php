<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TemplatesFixture
 */
class TemplatesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'name' => 'Template',
                'version' => 1,
                'created' => 1549880909,
                'modified' => 1549880909,
            ],
            [
                'structure_id' => 2,
                'name' => 'Template2',
                'version' => 1,
                'created' => 1549880909,
                'modified' => 1549880909,
            ],
        ];
        parent::init();
    }
}
