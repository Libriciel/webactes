<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TypesactsFixture
 */
class TypesactsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'structure_id' => 1,
                'nature_id' => 4,
                'name' => 'Autre',
                'created' => 1550658898,
                'modified' => 1550658898,
                'istdt' => false,
                'active' => true,
                'isdefault' => false,
                'counter_id' => 1,
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
                'isdeliberating' => false,
            ],
            2 => [
                'structure_id' => 1,
                'nature_id' => 2,
                'name' => 'Arrêté réglementaire',
                'created' => 1550658898,
                'modified' => 1550658898,
                'istdt' => true,
                'active' => true,
                'isdefault' => false,
                'counter_id' => 2,
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
                'isdeliberating' => false,
            ],
            3 => [
                'structure_id' => 1,
                'nature_id' => 3,
                'name' => 'Contrat et convention',
                'created' => 1550658898,
                'modified' => 1550658898,
                'istdt' => true,
                'active' => true,
                'isdefault' => false,
                'counter_id' => 3,
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
                'isdeliberating' => false,
            ],
            4 => [
                'structure_id' => 1,
                'nature_id' => 1,
                'name' => 'Délibération',
                'created' => 1550658898,
                'modified' => 1550658898,
                'istdt' => true,
                'active' => true,
                'isdefault' => true,
                'counter_id' => 1,
                'generate_template_project_id' => 1,
                'generate_template_act_id' => 2,
                'isdeliberating' => true,
            ],
            5 => [
                'structure_id' => 2,
                'nature_id' => 4,
                'name' => 'Autre',
                'created' => 1550658898,
                'modified' => 1550658898,
                'istdt' => false,
                'active' => true,
                'isdefault' => false,
                'counter_id' => 2,
                'isdeliberating' => false,
            ],
            6 => [
                'structure_id' => 2,
                'nature_id' => 2,
                'name' => 'Arrêté réglementaire',
                'created' => 1550658898,
                'modified' => 1550658898,
                'istdt' => true,
                'active' => true,
                'isdefault' => false,
                'counter_id' => 3,
                'isdeliberating' => false,
            ],
            7 => [
                'structure_id' => 2,
                'nature_id' => 3,
                'name' => 'Contrat et convention',
                'created' => 1550658898,
                'modified' => 1550658898,
                'istdt' => true,
                'active' => true,
                'isdefault' => false,
                'counter_id' => 2,
                'isdeliberating' => false,
            ],
            8 => [
                'structure_id' => 2,
                'nature_id' => 1,
                'name' => 'Délibération',
                'created' => 1550658898,
                'modified' => 1550658898,
                'istdt' => true,
                'active' => true,
                'isdefault' => true,
                'counter_id' => 1,
                'isdeliberating' => true,
            ],
        ];
        parent::init();
    }
}
