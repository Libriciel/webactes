<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ActorsVotesFixture
 */
class ActorsVotesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'actor_id' => 1,
                'vote_id' => 1,
                'result' => 'yes',
            ],
            [
                'actor_id' => 2,
                'vote_id' => 1,
                'result' => 'yes',
            ],
            [
                'actor_id' => 3,
                'vote_id' => 1,
                'result' => 'yes',
            ],
            [
                'actor_id' => 1,
                'vote_id' => 2,
                'result' => 'yes',
            ],
            [
                'actor_id' => 2,
                'vote_id' => 2,
                'result' => 'yes',
            ],
            [
                'actor_id' => 3,
                'vote_id' => 2,
                'result' => 'yes',
            ],
            [
                'actor_id' => 1,
                'vote_id' => 3,
                'result' => 'yes',
            ],
            [
                'actor_id' => 2,
                'vote_id' => 3,
                'result' => 'yes',
            ],
            [
                'actor_id' => 3,
                'vote_id' => 3,
                'result' => 'yes',
            ],
        ];
        parent::init();
    }
}
