<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PastellfluxactionsFixture
 */
class PastellfluxactionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'pastellflux_id' => 1,
                'action' => 'send-iparapheur',
                'state' => 'DONE',
                'comments' => 'Le document a été envoyé au TdT',
                'created' => 1562664886,
                'modified' => 1562664886,
            ],
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'pastellflux_id' => 1,
                'action' => 'verif-iparapheur',
                'state' => 'DONE',
                'comments' => '',
                'created' => 1562664886,
                'modified' => 1562664886,
            ],
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'pastellflux_id' => 1,
                'action' => 'send-tdt',
                'state' => 'DONE',
                'comments' => '',
                'created' => 1562664886,
                'modified' => 1562664886,
            ],
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'pastellflux_id' => 1,
                'action' => 'verif-reponse-tdt',
                'state' => 'DONE',
                'comments' => '',
                'created' => 1562664886,
                'modified' => 1562664886,
            ],
        ];
        parent::init();
    }
}
