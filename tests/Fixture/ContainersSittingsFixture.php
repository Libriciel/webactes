<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ContainersSittingsFixture
 */
class ContainersSittingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'container_id' => 1,
                'sitting_id' => 1,
                'rank' => 1,
            ],
            2 => [
                'container_id' => 4,
                'sitting_id' => 1,
                'rank' => 2,
            ],
            3 => [
                'container_id' => 5,
                'sitting_id' => 1,
                'rank' => 3,
            ],
            4 => [
                'container_id' => 6,
                'sitting_id' => 2,
                'rank' => 1,
            ],
            5 => [
                'container_id' => 7,
                'sitting_id' => 2,
                'rank' => 2,
            ],
            6 => [
                'container_id' => 37,
                'sitting_id' => 3,
                'rank' => 2,
            ],
            7 => [
                'container_id' => 19,
                'sitting_id' => 3,
                'rank' => 2,
            ],
            8 => [
                'container_id' => 1,
                'sitting_id' => 3,
                'rank' => 1,
            ],
        ];
        parent::init();
    }
}
