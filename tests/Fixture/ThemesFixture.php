<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ThemesFixture
 */
class ThemesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [ //id : 1
                'structure_id' => 1,
                'active' => 1,
                'name' => 'Administration Générale',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 6,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
            [ //id : 2
                'structure_id' => 1,
                'active' => 1,
                'name' => 'Ressources Humaines',
                'parent_id' => 4,
                'lft' => 2,
                'rght' => 3,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
            [ //id : 3
                'structure_id' => 1,
                'active' => 1,
                'name' => 'Affaires juridiques',
                'parent_id' => 4,
                'lft' => 4,
                'rght' => 5,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
            [ //id : 4
                'structure_id' => 1,
                'active' => 1,
                'name' => 'Finances',
                'parent_id' => null,
                'lft' => 7,
                'rght' => 14,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
            [ //id : 5
                'structure_id' => 1,
                'active' => 1,
                'name' => 'Budget',
                'parent_id' => 4,
                'lft' => 8,
                'rght' => 9,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
            [ //id : 6
                'structure_id' => 1,
                'active' => 1,
                'name' => 'Subvention',
                'parent_id' => 1,
                'lft' => 10,
                'rght' => 11,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
            [ //id : 7
                'structure_id' => 1,
                'active' => 1,
                'name' => 'Dotation',
                'parent_id' => 4,
                'lft' => 12,
                'rght' => 13,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
            [ //id : 8
                'structure_id' => 1,
                'active' => 1,
                'name' => 'Urbanisme',
                'parent_id' => null,
                'lft' => 15,
                'rght' => 16,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
            [ //id : 9
                'structure_id' => 2,
                'active' => 1,
                'name' => 'Administration Générale',
                'parent_id' => null,
                'lft' => 17,
                'rght' => 22,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
            [
                'structure_id' => 2,
                'active' => 1,
                'name' => 'Ressources Humaines',
                'parent_id' => 9,
                'lft' => 18,
                'rght' => 21,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
            [
                'structure_id' => 2,
                'active' => 1,
                'name' => 'Affaires juridiques',
                'parent_id' => 9,
                'lft' => 19,
                'rght' => 20,
                'created' => 1549880909,
                'modified' => 1549880909,
                'position' => 'Lorem',
                'deleted' => null,
            ],
        ];
        parent::init();
    }
}
