<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AuthorizeactionsFixture
 */
class AuthorizeactionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'action' => 'add',
                'created' => 1558511885,
                'modified' => 1558511885,
            ],
            [
                'action' => 'edit',
                'created' => 1558511885,
                'modified' => 1558511885,
            ],
            [
                'action' => 'connector',
                'created' => 1558511885,
                'modified' => 1558511885,
            ],
            [
                'action' => 'wa',
                'created' => 1558511885,
                'modified' => 1558511885,
            ],
        ];
        parent::init();
    }
}
