<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TypesittingsFixture
 */
class TypesittingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'structure_id' => 1,
                'name' => 'Bureau',
                'created' => 1549880910,
                'modified' => 1549880910,
                'active' => 1,
                'isdeliberating' => 0,
                'generate_template_convocation_id' => 4,
                'generate_template_executive_summary_id' => 5,
                'generate_template_deliberations_list_id' => 6,
                'generate_template_verbal_trial_id' => 7,
            ],
            2 => [
                'structure_id' => 1,
                'name' => 'Conseil Municipal',
                'created' => 1549880910,
                'modified' => 1549880910,
                'active' => 1,
                'isdeliberating' => 1,
                'generate_template_convocation_id' => 4,
                'generate_template_executive_summary_id' => 5,
                'generate_template_deliberations_list_id' => 6,
                'generate_template_verbal_trial_id' => 7,

            ],
            3 => [
                'structure_id' => 1,
                'name' => 'Commission',
                'created' => 1549880910,
                'modified' => 1549880910,
                'active' => 1,
                'isdeliberating' => 1,
                'generate_template_convocation_id' => 4,
                'generate_template_executive_summary_id' => 5,
                'generate_template_deliberations_list_id' => 6,
                'generate_template_verbal_trial_id' => 7,

            ],
            4 => [
                'structure_id' => 2,
                'name' => 'Bureau',
                'created' => 1549880910,
                'modified' => 1549880910,
                'active' => 1,
                'isdeliberating' => 1,
                'generate_template_convocation_id' => 4,
                'generate_template_executive_summary_id' => 5,
                'generate_template_deliberations_list_id' => 6,
                'generate_template_verbal_trial_id' => 7,

            ],
            5 => [
                'structure_id' => 1,
                'name' => 'Bureau sans séance',
                'created' => 1549880910,
                'modified' => 1549880910,
                'active' => 1,
                'isdeliberating' => 1,
                'generate_template_convocation_id' => 4,
                'generate_template_executive_summary_id' => 5,
                'generate_template_deliberations_list_id' => 6,
                'generate_template_verbal_trial_id' => 7,
            ],
        ];
        parent::init();
    }
}
