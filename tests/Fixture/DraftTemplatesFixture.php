<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DraftTemplatesFixture
 */
class DraftTemplatesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'structure_id' => 1,
                'draft_template_type_id' => 1,
                'name' => 'Gabarit de projet de délibération',
            ],
            2 => [
                'structure_id' => 1,
                'draft_template_type_id' => 2,
                'name' => 'Gabarit d\'acte de délibération',
            ],
            3 => [
                'structure_id' => 2,
                'draft_template_type_id' => 1,
                'name' => 'Gabarit de projet de délibération',
            ],
        ];
        parent::init();
    }
}
