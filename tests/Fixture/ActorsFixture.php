<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ActorsFixture
 */
class ActorsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'active' => 1,
                'rank' => 1,
                'civility' => 'M',
                'lastname' => 'ROUGE',
                'firstname' => 'Théo',
                'birthday' => '2000-02-12',
                'email' => 't.rouge@exemple.fr',
                'title' => 'Maire',
                'address' => '6 rue des acassias',
                'address_supplement' => '',
                'post_code' => '34000',
                'city' => 'Montpellier',
                'phone' => '0467659644',
                'cellphone' => '0602030405',
                'created' => 1549980442,
                'modified' => 1549980442,
            ],
            [
                'structure_id' => 2,
                'active' => 1,
                'rank' => 1,
                'civility' => 'M',
                'lastname' => 'ROUGE2',
                'firstname' => 'Théo2',
                'birthday' => '2000-02-12',
                'email' => 't.rouge2@exemple.fr',
                'title' => 'Maire',
                'address' => '6bis rue des acassias',
                'address_supplement' => '',
                'post_code' => '34000',
                'city' => 'Montpellier',
                'phone' => '0467659645',
                'cellphone' => '0602030406',
                'created' => 1549980442,
                'modified' => 1549980442,
            ],
            [
                'structure_id' => 1,
                'active' => 1,
                'rank' => 2,
                'civility' => 'M',
                'lastname' => 'ROUGE3',
                'firstname' => 'Théo2',
                'birthday' => '2000-02-12',
                'email' => 't.rouge3@exemple.fr',
                'title' => 'Maire',
                'address' => '6bis rue des acassias',
                'address_supplement' => '',
                'post_code' => '34000',
                'city' => 'Montpellier',
                'phone' => '0467659645',
                'cellphone' => '0602030406',
                'created' => 1549980442,
                'modified' => 1549980442,
            ],
            [
                'structure_id' => 1,
                'active' => 1,
                'rank' => 3,
                'civility' => 'M',
                'lastname' => 'ROUGE4',
                'firstname' => 'Théo4',
                'birthday' => '2000-02-12',
                'email' => 't.rouge4@exemple.fr',
                'title' => 'Maire',
                'address' => '6bis rue des acassias',
                'address_supplement' => '',
                'post_code' => '34000',
                'city' => 'Montpellier',
                'phone' => '0467659645',
                'cellphone' => '0602030406',
                'created' => 1549980442,
                'modified' => 1549980442,
            ],
        ];
        parent::init();
    }
}
