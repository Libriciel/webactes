<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GenerateTemplatesFixture
 */
class GenerateTemplatesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'structure_id' => 1,
                'generate_template_type_id' => 1,
                'name' => 'Modèle de projet',
            ],
            2 => [
                'structure_id' => 1,
                'generate_template_type_id' => 2,
                'name' => 'Modèle de délibération',
            ],
            3 => [
                'structure_id' => 1,
                'generate_template_type_id' => 2,
                'name' => 'Modèle d\'arrêté',
            ],
            4 => [
                'structure_id' => 1,
                'generate_template_type_id' => 3,
                'name' => 'Modèle de convocation',
            ],
            5 => [
                'structure_id' => 1,
                'generate_template_type_id' => 4,
                'name' => 'Modèle de note de synthèse',
            ],
            6 => [
                'structure_id' => 1,
                'generate_template_type_id' => 5,
                'name' => 'Modèle de liste des délibérations',
            ],
            7 => [
                'structure_id' => 1,
                'generate_template_type_id' => 6,
                'name' => 'Modèle de procès-verbal',
            ],
            8 => [
                'structure_id' => 2,
                'generate_template_type_id' => 1,
                'name' => 'Modèle de projet',
            ],
            9 => [
                'structure_id' => 1,
                'generate_template_type_id' => 1,
                'name' => 'Modèle de projet',
            ],
        ];
        parent::init();
    }
}
