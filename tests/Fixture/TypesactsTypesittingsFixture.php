<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TypesactsTypesittingsFixture
 */
class TypesactsTypesittingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'typesact_id' => 4,
                'typesitting_id' => 1,
            ],
            [
                'typesact_id' => 4,
                'typesitting_id' => 2,
            ],
        ];
        parent::init();
    }
}
