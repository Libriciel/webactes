<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * NotificationsFixture
 */
class NotificationsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'name' => 'Un projet d\'acte est en attente de validation à mon étape',
                'code' => 'validation_to_do',
                'is_global' => false,
            ],
            [
                'name' => 'Mon projet d\'acte a été validé / refusé lors d\'une étape de circuit.',
                'code' => 'Envoi dans un circuit',
                'is_global' => false,
            ],
            [
                'name' => 'Mon acte non télétransmissible a été signé',
                'code' => 'signed',
                'is_global' => false,
            ],
            [
                'name' => 'Mon acte a été signé et est en attente de dépôt sur le TdT',
                'code' => 'Transmission TDT',
                'is_global' => false,
            ],
            [
                'name' => 'Mon projet d\'acte a été validé et est en attente de signature',
                'code' => 'Validé',
                'is_global' => false,
            ],
        ];
        parent::init();
    }
}
