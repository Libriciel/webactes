<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TdtMessagesFixture
 */
class TdtMessagesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'tdt_id' => 1,
                'tdt_type' => 1,
                'tdt_etat' => 1,
                'date_message' => '2019-02-11',
                'tdt_data' => 'Lorem ipsum dolor sit amet',
                'created' => 1549880908,
                'modified' => 1549880908,
            ],
        ];
        parent::init();
    }
}
