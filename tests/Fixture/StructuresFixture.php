<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * StructuresFixture
 */
class StructuresFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'organization_id' => 1,
                'active' => true,
                'logo' => null,
                'business_name' => 'Libriciel SCOP',
                'id_orchestration' => 9,
            ],
            [
                'organization_id' => 1,
                'active' => true,
                'logo' => null,
                'business_name' => 'TERNUM',
                'id_orchestration' => 11,
                'id_external' => 100000005,
            ],
        ];
        parent::init();
    }
}
