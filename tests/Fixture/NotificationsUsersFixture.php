<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * NotificationsUsersFixture
 */
class NotificationsUsersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                //                'id' => 1,
                'user_id' => 1,
                'notification_id' => 1,
                'active' => true,
            ],
            [
                //                'id' => 2,
                'user_id' => 1,
                'notification_id' => 2,
                'active' => false,
            ],
            [
                //                'id' => 2,
                'user_id' => 2,
                'notification_id' => 1,
                'active' => true,
            ],
        ];
        parent::init();
    }
}
