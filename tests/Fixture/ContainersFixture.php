<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ContainersFixture
 */
class ContainersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 1,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            2 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 2,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            3 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 3,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            4 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 4,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            5 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 5,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            6 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 6,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            7 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 7,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            8 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 8,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            9 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 9,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            10 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 10,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            11 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 11,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            12 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 12,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            13 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 13,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            14 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 14,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            15 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 15,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            16 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 16,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            17 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 17,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            18 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 18,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            19 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 19,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            20 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 20,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            21 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 21,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            22 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 22,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            23 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 23,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            24 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 24,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            25 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 25,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            26 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 26,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            27 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 27,
                'typesact_id' => 1,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            28 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 28,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            29 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 29,
                'typesact_id' => 1,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            30 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 30,
                'typesact_id' => 1,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            31 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 31,
                'typesact_id' => 1,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            32 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 32,
                'typesact_id' => 1,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            33 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 33,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            34 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 34,
                'typesact_id' => 1,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            35 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 35,
                'typesact_id' => 1,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            36 => [
                'structure_id' => 2,
                'act_id' => null,
                'project_id' => 36,
                'typesact_id' => 5,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            37 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 37,
                'typesact_id' => 4,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],

            38 => [
                'structure_id' => 1,
                'act_id' => null,
                'project_id' => 38,
                'typesact_id' => 1,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            39 => [
                'structure_id' => 2,
                'act_id' => null,
                'project_id' => 39,
                'typesact_id' => 1,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
        ];

         // autre structure
        //        $this->records[] = [
        //            'structure_id' => 2,
        //            'act_id' => null,
        //            'project_id' => 21,
        //            'typesact_id' => 8,
        //            'created' => 1550759414,
        //            'modified' => 1550759414
        //        ];
        //        $this->records[] = [
        //            'structure_id' => 2,
        //            'act_id' => null,
        //            'project_id' => 22,
        //            'typesact_id' => 8,
        //            'created' => 1550759414,
        //            'modified' => 1550759414
        //        ];
        parent::init();
    }
}
