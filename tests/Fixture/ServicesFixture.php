<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ServicesFixture
 */
class ServicesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'active' => 1,
                'name' => 'DSI',
                'parent_id' => null,
                'lft' => 1,
                'rght' => 6,
                'created' => 1549880906,
                'modified' => 1549880906,
            ],
            [
                'structure_id' => 1,
                'active' => 1,
                'name' => 'RH',
                'parent_id' => 1,
                'lft' => 2,
                'rght' => 3,
                'created' => 1549880906,
                'modified' => 1549880906,
            ],
            [
                'structure_id' => 1,
                'active' => 1,
                'name' => 'Facturation',
                'parent_id' => 1,
                'lft' => 4,
                'rght' => 5,
                'created' => 1549880906,
                'modified' => 1549880906,
            ],
            [ // id : 4
                'structure_id' => 2,
                'active' => 1,
                'name' => 'DSI',
                'parent_id' => null,
                'lft' => 7,
                'rght' => 12,
                'created' => 1549880906,
                'modified' => 1549880906,
            ],
            [
                'structure_id' => 2,
                'active' => 1,
                'name' => 'RH',
                'parent_id' => 4,
                'lft' => 8,
                'rght' => 9,
                'created' => 1549880906,
                'modified' => 1549880906,
            ],
            [
                'structure_id' => 2,
                'active' => 1,
                'name' => 'Facturation',
                'parent_id' => 5,
                'lft' => 10,
                'rght' => 11,
                'created' => 1549880906,
                'modified' => 1549880906,
            ],
        ];
        parent::init();
    }
}
