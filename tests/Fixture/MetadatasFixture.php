<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MetadatasFixture
 */
class MetadatasFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'template_id' => 1,
                'tab_id' => 1,
                'field_id' => 1,
                'number_line' => 1,
                'number_column' => 1,
                'created' => 1550219156,
                'modified' => 1550219156,
            ],
        ];
        parent::init();
    }
}
