<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AnnexesFixture
 */
class AnnexesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
        //            1 => [
        //                'structure_id' => 1,
        //                'container_id' => 20,
        //                'rank' => 1,
        //                'istransmissible' => 1,
        //                'codetype' => '21_EP'
        //            ],
        ];
        parent::init();
    }
}
