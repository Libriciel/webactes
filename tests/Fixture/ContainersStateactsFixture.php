<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ContainersStateactsFixture
 */
class ContainersStateactsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        //  1 : Brouillon
        //  2 : En cours de validation
        //  3 : Validé
        //  4 : Refusé
        $this->records = [
            // Projet 1 : Projet brouillon créé par 4
            [
                'container_id' => 1,
                'stateact_id' => DRAFT,
                'created' => 1549880899,
                'modified' => 1549880899,
            ],
            // Projet 2 : Projet brouillon créé par 5
            [
                'container_id' => 2,
                'stateact_id' => DRAFT,
                'created' => 1549880900,
                'modified' => 1549880900,
            ],
            // Projet 3 : Projet validable par 4 créé par 4
            [
                'container_id' => 3,
                'stateact_id' => DRAFT,
                'created' => 1549880901,
                'modified' => 1549880901,
            ],
            [
                'container_id' => 3,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880902,
                'modified' => 1549880902,
            ],
            // Projet 4 : Projet validable par 4,5 créé par 4
            [
                'container_id' => 4,
                'stateact_id' => DRAFT,
                'created' => 1549880903,
                'modified' => 1549880903,
            ],
            [
                'container_id' => 4,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880904,
                'modified' => 1549880904,
            ],
            // Projet 5 : Projet validable par 5 avec 4 validateur créé par 4
            [
                'container_id' => 5,
                'stateact_id' => DRAFT,
                'created' => 1549880905,
                'modified' => 1549880905,
            ],
            [
                'container_id' => 5,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880906,
                'modified' => 1549880906,
            ],
            // Projet 6 : Projet validable par 5 sans 4 validateur créé par 4
            [
                'container_id' => 6,
                'stateact_id' => DRAFT,
                'created' => 1549880907,
                'modified' => 1549880907,
            ],
            [
                'container_id' => 6,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880908,
                'modified' => 1549880908,
            ],
            // Projet 7 : Projet validable par 4 créé par 5
            [
                'container_id' => 7,
                'stateact_id' => DRAFT,
                'created' => 1549880909,
                'modified' => 1549880909,
            ],
            [
                'container_id' => 7,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880910,
                'modified' => 1549880910,
            ],
            // Projet 8 : Projet validable par 4,5 créé par 5
            [
                'container_id' => 8,
                'stateact_id' => DRAFT,
                'created' => 1549880911,
                'modified' => 1549880911,
            ],
            [
                'container_id' => 8,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880912,
                'modified' => 1549880912,
            ],
            // Projet 9 : Projet validable par 5 avec 4 validateur créé par 5
            [
                'container_id' => 9,
                'stateact_id' => DRAFT,
                'created' => 1549880913,
                'modified' => 1549880913,
            ],
            [
                'container_id' => 9,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880914,
                'modified' => 1549880914,
            ],
            // Projet 10 : Projet validable par 5 sans 4 validateur créé par 5
            [
                'container_id' => 10,
                'stateact_id' => DRAFT,
                'created' => 1549880915,
                'modified' => 1549880915,
            ],
            [
                'container_id' => 10,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880916,
                'modified' => 1549880916,
            ],
            // Projet 11 : Projet validé après circuit avec 4 créé par 4
            [
                'container_id' => 11,
                'stateact_id' => DRAFT,
                'created' => 1549880917,
                'modified' => 1549880917,
            ],
            [
                'container_id' => 11,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880918,
                'modified' => 1549880918,
            ],
            [
                'container_id' => 11,
                'stateact_id' => VALIDATED,
                'created' => 1549880919,
                'modified' => 1549880919,
            ],
            // Projet 12 : Projet validé après circuit sans 4 créé par 4
            [
                'container_id' => 12,
                'stateact_id' => DRAFT,
                'created' => 1549880920,
                'modified' => 1549880920,
            ],
            [
                'container_id' => 12,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880921,
                'modified' => 1549880921,
            ],
            [
                'container_id' => 12,
                'stateact_id' => VALIDATED,
                'created' => 1549880922,
                'modified' => 1549880922,
            ],
            // Projet 13 : Projet validé après circuit avec 4 créé par 5
            [
                'container_id' => 13,
                'stateact_id' => DRAFT,
                'created' => 1549880923,
                'modified' => 1549880923,
            ],
            [
                'container_id' => 13,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880924,
                'modified' => 1549880924,
            ],
            [
                'container_id' => 13,
                'stateact_id' => VALIDATED,
                'created' => 1549880925,
                'modified' => 1549880925,
            ],
            // Projet 14 : Projet validé après circuit sans 4 créé par 5
            [
                'container_id' => 14,
                'stateact_id' => DRAFT,
                'created' => 1549880926,
                'modified' => 1549880926,
            ],
            [
                'container_id' => 14,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880927,
                'modified' => 1549880927,
            ],
            [
                'container_id' => 14,
                'stateact_id' => VALIDATED,
                'created' => 1549880928,
                'modified' => 1549880928,
            ],
            // Projet 15 : Projet validé en urgence après circuit avec 4 créé par 4
            [
                'container_id' => 15,
                'stateact_id' => DRAFT,
                'created' => 1549880929,
                'modified' => 1549880929,
            ],
            [
                'container_id' => 15,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880930,
                'modified' => 1549880930,
            ],
            [
                'container_id' => 15,
                'stateact_id' => VALIDATED,
                'created' => 1549880931,
                'modified' => 1549880931,
            ],
            // Projet 16 : Projet validé en urgence après circuit sans 4 créé par 4
            [
                'container_id' => 16,
                'stateact_id' => DRAFT,
                'created' => 1549880932,
                'modified' => 1549880932,
            ],
            [
                'container_id' => 16,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880933,
                'modified' => 1549880933,
            ],
            [
                'container_id' => 16,
                'stateact_id' => VALIDATED,
                'created' => 1549880934,
                'modified' => 1549880934,
            ],
            // Projet 17 : Projet validé en urgence après circuit avec 4 créé par 5
            [
                'container_id' => 17,
                'stateact_id' => DRAFT,
                'created' => 1549880935,
                'modified' => 1549880935,
            ],
            [
                'container_id' => 17,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880936,
                'modified' => 1549880936,
            ],
            [
                'container_id' => 17,
                'stateact_id' => VALIDATED,
                'created' => 1549880937,
                'modified' => 1549880937,
            ],
            // Projet 18 : Projet validé en urgence après circuit sans 4 créé par 5
            [
                'container_id' => 18,
                'stateact_id' => DRAFT,
                'created' => 1549880938,
                'modified' => 1549880938,
            ],
            [
                'container_id' => 18,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880939,
                'modified' => 1549880939,
            ],
            [
                'container_id' => 18,
                'stateact_id' => VALIDATED,
                'created' => 1549880940,
                'modified' => 1549880940,
            ],
            // Projet 19 : Projet créé validé créé par 4
            [
                'container_id' => 19,
                'stateact_id' => VALIDATED,
                'created' => 1549880941,
                'modified' => 1549880941,
            ],
            // Projet 20 : Projet créé validé créé par 5
            [
                'container_id' => 20,
                'stateact_id' => VALIDATED,
                'created' => 1549880942,
                'modified' => 1549880942,
            ],
            // Projet 21 : Projet refusé par 4, créé par 4
            [
                'container_id' => 21,
                'stateact_id' => DRAFT,
                'created' => 1549880943,
                'modified' => 1549880943,
            ],
            [
                'container_id' => 21,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880944,
                'modified' => 1549880944,
            ],
            [
                'container_id' => 21,
                'stateact_id' => REFUSED,
                'created' => 1549880945,
                'modified' => 1549880945,
            ],
            // Projet 22 : Projet refusé par 5, créé par 5
            [
                'container_id' => 22,
                'stateact_id' => DRAFT,
                'created' => 1549880946,
                'modified' => 1549880946,
            ],
            [
                'container_id' => 22,
                'stateact_id' => VALIDATION_PENDING,
                'created' => 1549880947,
                'modified' => 1549880947,
            ],
            [
                'container_id' => 22,
                'stateact_id' => REFUSED,
                'created' => 1549880948,
                'modified' => 1549880948,
            ],
            // Projet 23 : Projet à télétransmettre
            [
                'container_id' => 23,
                'stateact_id' => TO_TDT,
                'created' => 1549880949,
                'modified' => 1549880949,
            ],
            // Projet 24 : Projet en cours dépôt TDT
            [
                'container_id' => 24,
                'stateact_id' => PENDING_PASTELL,
                'created' => 1549880950,
                'modified' => 1549880950,
            ],
            // Projet 25 : Projet erreur TDT
            [
                'container_id' => 25,
                'stateact_id' => TDT_ERROR,
                'created' => 1549880951,
                'modified' => 1549880951,
            ],
            // Projet 26 : Projet déclaré signé, transmissible
            [
                'container_id' => 26,
                'stateact_id' => TDT_ERROR,
                'created' => 1549880952,
                'modified' => 1549880952,
            ],
            // Projet 27 : Projet déclaré signé, non transmissible
            [
                'container_id' => 27,
                'stateact_id' => DECLARE_SIGNED,
                'created' => 1549880953,
                'modified' => 1549880953,
            ],
            // Projet 28 : Projet signé i-parapheur, transmissible
            [
                'container_id' => 28,
                'stateact_id' => PARAPHEUR_SIGNED,
                'created' => 1549880954,
                'modified' => 1549880954,
            ],
            // Projet 29 : Projet signé i-parapheur, non transmissible
            [
                'container_id' => 29,
                'stateact_id' => PARAPHEUR_SIGNED,
                'created' => 1549880955,
                'modified' => 1549880955,
            ],
            // Projet 30 : Projet à ordonner
            [
                'container_id' => 30,
                'stateact_id' => TO_ORDER,
                'created' => 1549880956,
                'modified' => 1549880956,
            ],
            // Projet 31 : Projet en cours télétransmission
            [
                'container_id' => 31,
                'stateact_id' => PENDING_TDT,
                'created' => 1549880957,
                'modified' => 1549880957,
            ],
            // Projet 32 : Projet acquité
            [
                'container_id' => 32,
                'stateact_id' => ACQUITTED,
                'created' => 1549880958,
                'modified' => 1549880958,
            ],
            // Projet 33 : Projet annulé
            [
                'container_id' => 33,
                'stateact_id' => TDT_CANCEL,
                'created' => 1549880959,
                'modified' => 1549880959,
            ],
            // Projet 34 : 'Projet annulé non délib',
            [
                'container_id' => 34,
                'stateact_id' => TDT_CANCEL,
                'created' => 1549880960,
                'modified' => 1549880960,
            ],
            // Projet 35 : Projet refusé i-parapheur
            [
                'container_id' => 35,
                'stateact_id' => PARAPHEUR_REFUSED,
                'created' => 1549880961,
                'modified' => 1549880961,
            ],
            // Projet 36 : Autre structure
            [
                'container_id' => 36,
                'stateact_id' => DRAFT,
                'created' => 1549880962,
                'modified' => 1549880962,
            ],
            // Projet 37 : Autre structure
            [
                'container_id' => 37,
                'stateact_id' => VOTED_APPROVED,
                'created' => 1549880963,
                'modified' => 1549880963,
            ],
            // Projet 38 : Autre structure
            [
                'container_id' => 38,
                'stateact_id' => VALIDATED,
                'created' => 1549880964,
                'modified' => 1549880964,
            ],
            // Projet 39 : Autre structure
            [
                'container_id' => 39,
                'stateact_id' => VALIDATED,
                'created' => 1549880964,
                'modified' => 1549880964,
            ],
        ];
        parent::init();
    }
}
