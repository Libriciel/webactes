<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ActorsFixture
 */
class ActorGroupsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => 1,
                'name' => 'Structure 1 - Groupe 1',
                'created' => 1549980442,
                'modified' => 1549980442,
            ],
            [
                'organization_id' => 1,
                'structure_id' => 2,
                'active' => 1,
                'name' => 'Structure 2 - Groupe 1',
                'created' => 1549980442,
                'modified' => 1549980442,
            ],
        ];
        parent::init();
    }
}
