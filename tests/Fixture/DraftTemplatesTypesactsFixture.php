<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DraftTemplatesTypesactsFixture
 */
class DraftTemplatesTypesactsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'draft_template_id' => 1,
                'typesact_id' => 1,
            ],
            [
                'draft_template_id' => 2,
                'typesact_id' => 1,
            ],
        ];
        parent::init();
    }
}
