<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ActorsProjectsSittingsFixture
 */
class ActorsProjectsSittingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'actor_id' => 1,
                'mandataire_id' => 1,
                'project_id' => 19,
                'sitting_id' => 1,
                'structure_id' => 1,
                'is_present' => 1,
            ],
            2 => [
                'actor_id' => 2,
                'mandataire_id' => 1,
                'project_id' => 19,
                'sitting_id' => 1,
                'structure_id' => 1,
                'is_present' => 1,
            ],
            3 => [
                'actor_id' => 3,
                'mandataire_id' => 1,
                'project_id' => 19,
                'sitting_id' => 1,
                'structure_id' => 1,
                'is_present' => 1,
            ],
            4 => [
                'actor_id' => 1,
//                'mandataire_id' => 1,
                'project_id' => 6,
                'sitting_id' => 2,
                'structure_id' => 1,
                'is_present' => 1,
            ],
            5 => [
//                'id' => ,
                'actor_id' => 2,
//                'mandataire_id' => 1,
                'project_id' => 6,
                'sitting_id' => 2,
                'structure_id' => 1,
                'is_present' => 1,
            ],
            6 => [
//                'id' => ,
                'actor_id' => 3,
                'mandataire_id' => null,
                'project_id' => 6,
                'sitting_id' => 2,
                'structure_id' => 1,
                'is_present' => 1,
            ],
            7 => [
//                'id' => ,
                'actor_id' => 1,
//                'mandataire_id' => 1,
                'project_id' => 7,
                'sitting_id' => 2,
                'structure_id' => 1,
                'is_present' => 0,
            ],
            8 => [
//                'id' => ,
                'actor_id' => 2,
//                'mandataire_id' => 1,
                'project_id' => 7,
                'sitting_id' => 2,
                'structure_id' => 1,
                'is_present' => 0,
            ],
            9 => [
//                'id' => ,
                'actor_id' => 3,
                'mandataire_id' => null,
                'project_id' => 7,
                'sitting_id' => 2,
                'structure_id' => 1,
                'is_present' => 0,
            ],
            10 => [
                'actor_id' => 1,
                'mandataire_id' => null,
                'project_id' => 1,
                'sitting_id' => 1,
                'structure_id' => 1,
                'is_present' => false,
            ],
            11 => [
                'actor_id' => 2,
                'mandataire_id' => null,
                'project_id' => 1,
                'sitting_id' => 1,
                'structure_id' => 1,
                'is_present' => true,
            ],
            12 => [
                'actor_id' => 3,
                'mandataire_id' => 4,
                'project_id' => 1,
                'sitting_id' => 1,
                'structure_id' => 1,
                'is_present' => false,
            ],
            [
                'actor_id' => 1,
                'mandataire_id' => null,
                'project_id' => 4,
                'sitting_id' => 1,
                'structure_id' => 1,
                'is_present' => false,
            ],
            [
                'actor_id' => 2,
                'mandataire_id' => null,
                'project_id' => 4,
                'sitting_id' => 1,
                'structure_id' => 1,
                'is_present' => true,
            ],
            [
                'actor_id' => 3,
                'mandataire_id' => 1,
                'project_id' => 4,
                'sitting_id' => 1,
                'structure_id' => 1,
                'is_present' => false,
            ],
        ];
        parent::init();
    }
}
