<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use App\Model\Table\RolesTable;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * RolesFixture
 */
class RolesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [//1
                //'id' => 1,
                'structure_id' => 1,
                'active' => 1,
                'name' => RolesTable::ADMINISTRATOR,
            ],
            [//2
                //'id' => 2,
                'structure_id' => 1,
                'active' => 1,
                'name' => RolesTable::FUNCTIONNAL_ADMINISTRATOR,
            ],
            [//3
                //'id' => 3,
                'structure_id' => 1,
                'active' => 1,
                'name' => RolesTable::GENERAL_SECRETARIAT,
            ],
            [//4
                //'id' => 4,
                'structure_id' => 1,
                'active' => 1,
                'name' => RolesTable::VALIDATOR_REDACTOR_NAME,
            ],
            [//5
                //'id' => 5,
                'structure_id' => 1,
                'active' => 1,
                'name' => RolesTable::SUPER_ADMINISTRATOR,
            ],
            [
                //'id' => 6,
                'structure_id' => 2,
                'active' => 1,
                'name' => RolesTable::ADMINISTRATOR,
            ],
            [
                //'id' => 7,
                'structure_id' => 2,
                'active' => 1,
                'name' => RolesTable::FUNCTIONNAL_ADMINISTRATOR,
            ],
            [
               // 'id' => 8,
                'structure_id' => 2,
                'active' => 1,
                'name' => RolesTable::GENERAL_SECRETARIAT,
            ],
            [
               // 'id' => 9,
                'structure_id' => 2,
                'active' => 1,
                'name' => RolesTable::VALIDATOR_REDACTOR_NAME,
            ],
            [
               // 'id' => 10,
                'structure_id' => 2,
                'active' => 1,
                'name' => RolesTable::SUPER_ADMINISTRATOR,
            ],
        ];
        parent::init();
    }
}
