<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ActorsActorsgroupsFixture
 */
class ActorsActorGroupsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'actor_id' => 1,
                'actor_group_id' => 1,
            ],
            [
                'actor_id' => 2,
                'actor_group_id' => 2,
            ],
            [
                'actor_id' => 3,
                'actor_group_id' => 1,
            ],
            [
                'actor_id' => 4,
                'actor_group_id' => 1,
            ],
        ];
        parent::init();
    }
}
