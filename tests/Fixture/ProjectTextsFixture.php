<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProjectTextsFixture
 */
class ProjectTextsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'project_id' => 1,
                'draft_template_id' => 1,
            ],
            [
                'structure_id' => 1,
                'project_id' => 1,
                'draft_template_id' => 2,
            ],
            [
                'structure_id' => 1,
                'project_id' => 1,
                'draft_template_id' => 3,
            ],
        ];
        parent::init();
    }
}
