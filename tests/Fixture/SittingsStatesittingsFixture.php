<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SittingsStatesittingsFixture
 */
class SittingsStatesittingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'sitting_id' => 1,
                'statesitting_id' => 1,
            ],
            [
                'sitting_id' => 2,
                'statesitting_id' => 1,
            ],
            [
                'sitting_id' => 3,
                'statesitting_id' => 1,
            ],
            [
                'sitting_id' => 4,
                'statesitting_id' => 2,
            ],
        ];
        parent::init();
    }
}
