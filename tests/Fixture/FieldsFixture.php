<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FieldsFixture
 */
class FieldsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'name' => 'select',
                'created' => 1549880902,
                'modified' => 1549880902,
            ],
            [
                'structure_id' => 1,
                'name' => 'input',
                'created' => 1549880902,
                'modified' => 1549880902,
            ],
            [
                'structure_id' => 1,
                'name' => 'number',
                'created' => 1549880902,
                'modified' => 1549880902,
            ],
            [
                'structure_id' => 1,
                'name' => 'textaera',
                'created' => 1549880902,
                'modified' => 1549880902,
            ],
        ];
        parent::init();
    }
}
