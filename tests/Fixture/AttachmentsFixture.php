<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AttachmentsFixture
 */
class AttachmentsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'name' => 'Convocation',
                'created' => 1549880899,
                'modified' => 1549880899,
            ],
            [
                'structure_id' => 1,
                'name' => 'Note de synthèse',
                'created' => 1549880899,
                'modified' => 1549880899,
            ],
        ];
        parent::init();
    }
}
