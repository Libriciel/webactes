<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MatieresFixture
 */
class MatieresFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'parent_id' => null,
                'codematiere' => '7',
                'libelle' => '7 Finances locales',
                'lft' => 1,
                'rght' => 1,
                'created' => 1559122756,
                'modified' => 1559122756,
            ],
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'parent_id' => null,
                'codematiere' => '1.1',
                'libelle' => '1.1 Marches publics',
                'lft' => 2,
                'rght' => 3,
                'created' => 1559122756,
                'modified' => 1559122756,
            ],
            [
                'organization_id' => 1,
                'structure_id' => 2,
                'parent_id' => null,
                'codematiere' => '2.1',
                'libelle' => '2.1 Marches publics',
                'lft' => 1,
                'rght' => 1,
                'created' => 1559122756,
                'modified' => 1559122756,
            ],
        ];
        parent::init();
    }
}
