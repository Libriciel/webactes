<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PastellfluxsFixture
 */
class PastellfluxsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'organization_id' => 1,
                'structure_id' => 1,
                'pastellfluxtype_id' => 1,
                'container_id' => 1,
                'summon_id' => 1,
                'id_d' => 'Lorem ipsum dolor sit amet',
                'state' => 'Lorem ip',
                'comments' => 'Lorem ipsum dolor sit amet',
                'created' => 1617980614,
                'modified' => 1617980614,
                'summon_id' => 1,
            ],
        ];
        parent::init();
    }
}
