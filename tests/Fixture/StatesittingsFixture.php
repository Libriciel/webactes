<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * StatesittingsFixture
 */
class StatesittingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'state' => 'Ouverture de la séance',
                'code' => 'ouverture_seance',
                'created' => 1556028313,
                'modified' => 1556028313,
            ],
            [
                'state' => 'Séance close',
                'code' => 'seance_close',
                'created' => 1556028313,
                'modified' => 1556028313,
            ],
        ];
        parent::init();
    }
}
