<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * StructureSettingsFixture
 */
class StructureSettingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                //'id' => 1,
                'structure_id' => 1,
                'name' => 'project',
                'value' => ['project_writing' => false],
                'created' => 1615910733,
                'modified' => 1615910733,
            ],
            [
                //'id' => 2,
                'structure_id' => 1,
                'name' => 'act',
                'value' => ['act_generate' => false, 'generate_number' => true],
                'created' => 1615910733,
                'modified' => 1615910733,
            ],
            [
                //'id' => 2,
                'structure_id' => 2,
                'name' => 'act',
                'value' => ['act_generate' => true, 'generate_number' => true],
                'created' => 1615910733,
                'modified' => 1615910733,
            ],
        ];
        parent::init();
    }
}
