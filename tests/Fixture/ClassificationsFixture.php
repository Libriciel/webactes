<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ClassificationsFixture
 */
class ClassificationsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'created' => 1559054578,
                'modified' => 1559054578,
                'dateclassification' => '2019-05-28',
            ],
        ];
        parent::init();
    }
}
