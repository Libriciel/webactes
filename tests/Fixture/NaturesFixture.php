<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * NaturesFixture
 */
class NaturesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'created' => 1559054281,
                'modified' => 1559054281,
                'codenatureacte' => '1',
                'libelle' => 'Délibérations',
                'typeabrege' => 'DE',
            ],
            [
                'structure_id' => 1,
                'created' => 1559054281,
                'modified' => 1559054281,
                'codenatureacte' => '2',
                'libelle' => 'Actes réglementaires',
                'typeabrege' => 'AR',
            ],
            [
                'structure_id' => 1,
                'created' => 1559054281,
                'modified' => 1559054281,
                'codenatureacte' => '4',
                'libelle' => 'Contrats,conventions et avenants',
                'typeabrege' => 'CC',
            ],
            [
                'structure_id' => 1,
                'created' => 1559054281,
                'modified' => 1559054281,
                'codenatureacte' => '6',
                'libelle' => 'Autres',
                'typeabrege' => 'AU',
            ],
            [ // id : 5
                'structure_id' => 2,
                'created' => 1559054281,
                'modified' => 1559054281,
                'codenatureacte' => '1',
                'libelle' => 'Délibérations',
                'typeabrege' => 'DE',
            ],
            [ // id : 6
                'structure_id' => 1,
                'created' => 1559054281,
                'modified' => 1559054281,
                'codenatureacte' => '3',
                'libelle' => 'Arrêtés Individuels',
                'typeabrege' => 'AI',
            ],
        ];
        parent::init();
    }
}
