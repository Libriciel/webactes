<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AuthorizeactionsStateactsFixture
 */
class AuthorizeactionsStateactsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'authorizeaction_id' => 1,
                'stateact_id' => 1,
            ],
            [
                'authorizeaction_id' => 1,
                'stateact_id' => 3,
            ],
            [
                'authorizeaction_id' => 1,
                'stateact_id' => 5,
            ],
            [
                'authorizeaction_id' => 1,
                'stateact_id' => 9,
            ],
            [
                'authorizeaction_id' => 2,
                'stateact_id' => 1,
            ],
            [
                'authorizeaction_id' => 2,
                'stateact_id' => 3,
            ],
            [
                'authorizeaction_id' => 2,
                'stateact_id' => 4,
            ],
            [
                'authorizeaction_id' => 2,
                'stateact_id' => 5,
            ],
            [
                'authorizeaction_id' => 2,
                'stateact_id' => 7,
            ],
            [
                'authorizeaction_id' => 2,
                'stateact_id' => 9,
            ],
            [
                'authorizeaction_id' => 2,
                'stateact_id' => 11,
            ],
            [
                'authorizeaction_id' => 2,
                'stateact_id' => 12,
            ],
            [
                'authorizeaction_id' => 3,
                'stateact_id' => 6,
            ],
            [
                'authorizeaction_id' => 3,
                'stateact_id' => 7,
            ],
            [
                'authorizeaction_id' => 3,
                'stateact_id' => 8,
            ],
            [
                'authorizeaction_id' => 3,
                'stateact_id' => 9,
            ],
            [
                'authorizeaction_id' => 3,
                'stateact_id' => 10,
            ],
            [
                'authorizeaction_id' => 3,
                'stateact_id' => 11,
            ],
            [
                'authorizeaction_id' => 3,
                'stateact_id' => 12,
            ],
            [
                'authorizeaction_id' => 3,
                'stateact_id' => 13,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 2,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 3,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 4,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 5,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 6,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 7,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 8,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 9,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 10,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 11,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 12,
            ],
            [
                'authorizeaction_id' => 4,
                'stateact_id' => 13,
            ],
        ];
        parent::init();
    }
}
