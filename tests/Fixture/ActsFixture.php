<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ActsFixture
 */
class ActsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'template_id' => 1,
                'name' => 'monPremierAct',
                'code_act' => 'ACT_1',
                'date_send' => 1549880899,
                'created' => 1549880899,
                'modified' => 1549880899,
            ],
            [
                'structure_id' => 1,
                'template_id' => 1,
                'name' => 'Second act',
                'code_act' => 'ACT_2',
                'date_send' => 1549880899,
                'created' => 1549880899,
                'modified' => 1549880899,
            ],
            [
                'structure_id' => 1,
                'template_id' => 1,
                'name' => 'Act trois',
                'code_act' => 'ACT_3',
                'date_send' => 1549880899,
                'created' => 1549880899,
                'modified' => 1549880899,
            ],
            [
                'structure_id' => 1,
                'template_id' => 1,
                'name' => 'Act quatre',
                'code_act' => 'ACT_4',
                'date_send' => 1549880899,
                'created' => 1549880899,
                'modified' => 1549880899,
            ],
            [
                'structure_id' => 1,
                'template_id' => 1,
                'name' => 'Act cinq',
                'code_act' => 'ACT_5',
                'date_send' => 1549880899,
                'created' => 1549880899,
                'modified' => 1549880899,
            ],
            [
                'structure_id' => 2,
                'template_id' => 1,
                'name' => 'actStructure2',
                'code_act' => 'STR_2',
                'date_send' => 1549880899,
                'created' => 1549880899,
                'modified' => 1549880899,
            ],
        ];
        parent::init();
    }
}
