<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DposFixture
 */
class DposFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'civility' => 'M.',
                'lastname' => 'Jean',
                'firstname' => 'BLANC',
                'email' => 'j.blanc@test.fr',
                'address' => '836 rue des blancs',
                'address_supplement' => 'Résidence le Sud',
                'post_code' => '34000',
                'city' => 'MONTPELLIER',
                'phone' => '0400000000',
                'created' => 1549984548,
                'modified' => 1549984548,
            ],
            [
                'structure_id' => 2,
                'civility' => 'M.',
                'lastname' => 'John',
                'firstname' => 'Dpo',
                'email' => 'j.dpo@test.fr',
                'address' => '836 rue des dpos',
                'address_supplement' => 'Résidence le Dpo',
                'post_code' => '34000',
                'city' => 'MONTPELLIER',
                'phone' => '0400000001',
                'created' => 1549984548,
                'modified' => 1549984548,
            ],
        ];
        parent::init();
    }
}
