<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TypespiecesjointesFixture
 */
class TypespiecesjointesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'structure_id' => 1,
                'nature_id' => 1,
                'codetype' => '99_AU',
                'libelle' => 'Autre document',
                'created' => 1559054711,
                'modified' => 1559054711,
            ],
            2 => [
                'structure_id' => 1,
                'nature_id' => 4,
                'codetype' => '99_SE',
                'libelle' => 'Fichier de signature électronique',
                'created' => 1559054711,
                'modified' => 1559054711,
            ],
            3 => [
                'structure_id' => 1,
                'nature_id' => 4,
                'codetype' => '99_AR',
                'libelle' => 'Acte réglementaire',
                'created' => 1559054711,
                'modified' => 1559054711,
            ],
            4 => [
                'structure_id' => 1,
                'nature_id' => 4,
                'codetype' => '21_EP',
                'libelle' => 'Enquete publique',
                'created' => 1559054711,
                'modified' => 1559054711,
            ],
            5 => [
                'structure_id' => 2,
                'nature_id' => 1,
                'codetype' => '99_AU',
                'libelle' => 'Autre document',
                'created' => 1559054711,
                'modified' => 1559054711,
            ],
            6 => [
                'structure_id' => 2,
                'nature_id' => 4,
                'codetype' => '99_SE',
                'libelle' => 'Fichier de signature électronique',
                'created' => 1559054711,
                'modified' => 1559054711,
            ],
            7 => [
                'structure_id' => 2,
                'nature_id' => 4,
                'codetype' => '99_AR',
                'libelle' => 'Acte réglementaire',
                'created' => 1559054711,
                'modified' => 1559054711,
            ],
            8 => [
                'structure_id' => 2,
                'nature_id' => 4,
                'codetype' => '21_EP',
                'libelle' => 'Enquete publique',
                'created' => 1559054711,
                'modified' => 1559054711,
            ],
        ];
        parent::init();
    }
}
