<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SequencesFixture
 */
class SequencesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [ // id : 1
                'structure_id' => 1,
                'name' => 'Seq_delib',
                'comment' => 'Pour les délibérations',
                'sequence_num' => 1,
                'created' => 1607597401,
                'modified' => 1607597401,
                'starting_date' => '2020-01-01',
                'expiry_date' => '2020-12-31',
            ],
            [ // id : 2
                'structure_id' => 1,
                'name' => 'Séquence_contrats',
                'comment' => 'Pour les contrats uniquement',
                'sequence_num' => 1,
                'created' => 1607597401,
                'modified' => 1607597401,
                'starting_date' => '2020-01-01',
                'expiry_date' => '2020-12-31',
            ],
            [ // id : 3
                'structure_id' => 1,
                'name' => 'Séq_arr_réglementaire',
                'comment' => 'Pour dissocier arrêts réglementaires et individuels',
                'sequence_num' => 1,
                'created' => 1607597401,
                'modified' => 1607597401,
                'starting_date' => '2020-01-01',
                'expiry_date' => '2020-12-31',
            ],
            [ // id : 4
                'structure_id' => 1,
                'name' => 'Séq_arr_individuel',
                'comment' => 'Pour dissocier arrêts réglementaires et individuels',
                'sequence_num' => 1,
                'created' => 1607597401,
                'modified' => 1607597401,
                'starting_date' => '2020-01-01',
                'expiry_date' => '2020-12-31',
            ],
            [ // id : 5
                'structure_id' => 1,
                'name' => 'Séq_conventions',
                'comment' => 'Séquence pas utilisée par un compteur',
                'sequence_num' => 1,
                'created' => 1607597401,
                'modified' => 1607597401,
                'starting_date' => '2020-01-01',
                'expiry_date' => '2020-12-31',
            ],
            [ // id : 6
                'structure_id' => 1,
                'name' => 'Séq_test réinit année',
                'comment' => 'Séquence utilisée par 2 compteurs (année)',
                'sequence_num' => 0,
                'created' => 1607597401,
                'modified' => 1607597401,
                'starting_date' => '2021-01-01',
                'expiry_date' => '2021-12-31',
            ],
            [ // id : 7
                'structure_id' => 1,
                'name' => 'Séq_test réinit mois',
                'comment' => 'Séquence utilisée par 2 compteurs (mois)',
                'sequence_num' => 0,
                'created' => 1607597401,
                'modified' => 1607597401,
                'starting_date' => '2021-01-01',
                'expiry_date' => '2021-12-31',
            ],
            [ // id : 8
                'structure_id' => 1,
                'name' => 'Séq_test réinit jour',
                'comment' => 'Séquence utilisée par 2 compteurs (jours)',
                'sequence_num' => 0,
                'created' => 1607597401,
                'modified' => 1607597401,
                'starting_date' => '2021-01-01',
                'expiry_date' => '2021-12-31',
            ],
            [ // id : 9
                'structure_id' => 2,
                'name' => 'Seq_delib',
                'comment' => 'Pour les délibérations',
                'sequence_num' => 1,
                'created' => 1607597401,
                'modified' => 1607597401,
                'starting_date' => '2020-01-01',
                'expiry_date' => '2020-12-31',
            ],
        ];
        parent::init();
    }
}
