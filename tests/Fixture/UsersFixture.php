<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [//1
//                'id' => 1,
                'organization_id' => 1,
                'keycloak_id' => '4882bade-72bb-40d8-b896-36a6dd03bbd9',
                'civility' => 'M.',
                'lastname' => 'BLANC',
                'firstname' => 'Sébastien',
                'username' => 's.blanc',
                'email' => 'sebastien.blanc@test.fr',
                'phone' => '0467659645',
                'data_security_policy' => true,
                'id_external' => 000000001,
            ],
            [//2
//                'id' => 2,
                'organization_id' => 1,
                'keycloak_id' => '18c45a75-2852-4637-8e78-6ff3ed949164',
                'civility' => 'M.',
                'lastname' => 'VERT',
                'firstname' => 'Mickael',
                'username' => 'm.vert',
                'email' => 'mickael.vert@test.fr',
                'phone' => '0467659645',
                'data_security_policy' => true,
            ],
            [//3
//                'id' => 3,
                'organization_id' => 1,
                'keycloak_id' => '684dd96c-cdfd-4304-8869-b1567f9c5286',
                'civility' => 'M.',
                'lastname' => 'ORANGE',
                'firstname' => 'Julien',
                'username' => 'j.orange',
                'email' => 'julien.orange@test.fr',
                'phone' => '0467659645',
                'data_security_policy' => true,
            ],
            [//4
//                'id' => 4,
                'organization_id' => 1,
                'keycloak_id' => 'c8832f5d-4f54-4e95-9714-8715b734c514',
                'civility' => 'M.',
                'lastname' => 'VIOLET',
                'firstname' => 'Rémi',
                'username' => 'r.violet',
                'email' => 'remi.violet@test.fr',
                'phone' => '0467659645',
                'data_security_policy' => true,
            ],
            [//5
//                'id' => 5,
                'organization_id' => 1,
                'keycloak_id' => 'c8172f5d-4f54-4e95-9714-8715b734c515',
                'civility' => 'M.',
                'lastname' => 'Cyan',
                'firstname' => 'Romain',
                'username' => 'r.cyan',
                'email' => 'romain.cyan@test.fr',
                'data_security_policy' => true,
            ],
            [//6
//                'id' => 6,
                'organization_id' => 1,
                'keycloak_id' => 'c8172f5d-4f54-erty-9714-8715b734c516',
                'civility' => 'M.',
                'lastname' => 'White',
                'firstname' => 'Sébastien',
                'username' => 's.white',
                'email' => 'sebastien.white@test.fr',
                'data_security_policy' => true,
            ],
            [//7
//                'id' => 7,
                'organization_id' => 1,
                'keycloak_id' => 'c8172f5d-4f54-thyi-9714-8715b734c517',
                'civility' => 'M.',
                'lastname' => 'Green',
                'firstname' => 'Mickael',
                'username' => 'm.green',
                'email' => 'mickael.green@test.fr',
                'data_security_policy' => true,
            ],
            [//8
//                'id' => 8,
                'organization_id' => 1,
                'keycloak_id' => 'c8172f5d-4f54-pn89-9714-8715b734c518',
                'civility' => 'M.',
                'lastname' => 'Yellow',
                'firstname' => 'Valérie',
                'username' => 'v.yellow',
                'email' => 'valerie.yellow@test.fr',
                'data_security_policy' => true,
            ],
            [//9
//                'id' => 9,
                'organization_id' => 1,
                'keycloak_id' => 'c8172f5d-4f54-pn89-9794-8715b734c519',
                'civility' => 'M.',
                'lastname' => 'Purple',
                'firstname' => 'Stéphane',
                'username' => 's.purple',
                'email' => 'stephane.purple@test.fr',
                'data_security_policy' => true,
            ],
            [//10
//                'id' => 10,
                'organization_id' => 1,
                'keycloak_id' => 'c8172o5d-4f54-pn89-9794-8715b734c520',
                'civility' => 'Mme',
                'lastname' => 'Lime',
                'firstname' => 'Emilie',
                'username' => 'e.lime',
                'email' => 'emilie.lime@test.fr',
                'data_security_policy' => true,
            ],
            [//11
//                'id' => 11,
                'organization_id' => 1,
                'keycloak_id' => 'c8172o5d-4f54-pn89-9794-8715b734cu20',
                'civility' => 'Mme',
                'lastname' => 'lilo',
                'firstname' => 'Emilie',
                'username' => 'e.lme',
                'email' => 'emilie.lme@test.fr',
                'data_security_policy' => true,
            ],
            [//12
//                'id' => 12,
                'organization_id' => 1,
                'keycloak_id' => 'c8172o5d-4f54-pn8ezr-9794-8715beezzerzerzer20',
                'civility' => 'M',
                'lastname' => 'test',
                'firstname' => 'admin',
                'username' => 'test.admin',
                'email' => 'test.admin@test.fr',
                'data_security_policy' => true,
            ],
        ];
        parent::init();
    }
}
