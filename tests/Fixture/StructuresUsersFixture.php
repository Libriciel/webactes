<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * StructuresUsersFixture
 */
class StructuresUsersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'user_id' => 1,
            ],
            [
                'structure_id' => 1,
                'user_id' => 2,
            ],
            [
                'structure_id' => 1,
                'user_id' => 3,
            ],
            [
                'structure_id' => 1,
                'user_id' => 4,
            ],
            [
                'structure_id' => 1,
                'user_id' => 5,
            ],
            [
                'structure_id' => 2,
                'user_id' => 6,
            ],
            [
                'structure_id' => 2,
                'user_id' => 7,
            ],
            [
                'structure_id' => 2,
                'user_id' => 8,
            ],
            [
                'structure_id' => 2,
                'user_id' => 9,
            ],
            [
                'structure_id' => 2,
                'user_id' => 10,
            ],
            [
                'structure_id' => 2,
                'user_id' => 11,
            ],
            [
                'structure_id' => 2,
                'user_id' => 12,
            ],
        ];
        parent::init();
    }
}
