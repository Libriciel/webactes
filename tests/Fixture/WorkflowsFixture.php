<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * WorkflowsFixture
 */
class WorkflowsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'name' => '4 -> 5 -> 6',
                'description' => 'description 4 -> 5 -> 6',
                'file' => '1552472001_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000001',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => true,
                'deleted' => null,
            ],
            2 => [
                'name' => '4, 5 -> 6',
                'description' => 'description 4, 5 -> 6',
                'file' => '1552472002_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000002',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => true,
                'deleted' => null,
            ],
            3 => [
                'name' => '5 -> 6',
                'description' => 'description 5 -> 6',
                'file' => '1552472003_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000003',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => true,
                'deleted' => null,
            ],
            4 => [
                'name' => 'not instancied circuit',
                'description' => 'description not instancied circuit',
                'file' => '1552472004_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000004',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => true,
                'deleted' => null,
            ],
            5 => [
                'name' => 'Inactive circuit',
                'description' => 'circuit inactif',
                'file' => '1552472005_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000005',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => false,
                'deleted' => null,
            ],
            6 => [
                'name' => 'Deleted circuit',
                'description' => 'circuit deleted',
                'file' => '1552472005_00000001.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000006',
                'organization_id' => 1,
                'structure_id' => 1,
                'active' => false,
                'deleted' => 1552472005,
            ],
            7 => [
                'name' => '10 -> 11 -> 12 struct 2',
                'description' => 'description 10 -> 11 -> 12 struct 2',
                'file' => '1552472006_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000007',
                'organization_id' => 1,
                'structure_id' => 2,
                'active' => true,
                'deleted' => null,
            ],
            8 => [
                'name' => 'Inactive circuit structure 2',
                'description' => 'circuit inactif struct 2',
                'file' => '1552472007_00000000.bpmn20.xml',
                'process_definition_key' => 'process_aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa-0000000008',
                'organization_id' => 1,
                'structure_id' => 2,
                'active' => false,
                'deleted' => null,
            ],
        ];
        parent::init();
    }
}
