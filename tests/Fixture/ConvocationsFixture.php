<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConvocationsFixture
 */
class ConvocationsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'actor_id' => 1,
                'sitting_id' => 1,
                'jeton' => 1,
                'summon_id' => 1,
            ],
            [
                'structure_id' => 1,
                'actor_id' => 3,
                'sitting_id' => 1,
                'jeton' => 1,
                'summon_id' => 1,
            ],
            [
                'structure_id' => 1,
                'actor_id' => 4,
                'sitting_id' => 1,
                'jeton' => 1,
                'summon_id' => 1,
            ],
        ];
        parent::init();
    }
}
