<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SittingsFixture
 */
class SittingsFixture extends TestFixture
{
    public const BUREAU_STRUCTURE_1 = 1;
    public const CONSEIL_MUNICIPAL_STUCTURE_1 = 2;
    public const COMMISSION_STRUCTURE_1 = 3;
    public const BUREAU_STRUCTURE_2 = 4;
    public const BUREAU_SANS_SEANCE = 5;

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'structure_id' => 1,
                'typesitting_id' => self::BUREAU_STRUCTURE_1,
                'date' => 1550759640,
                'date_convocation' => 1550759640,
                'created' => 1550759640,
                'modified' => 1550759640,
                'president_id' => 1,
                'secretary_id' => 1,
                'place' => null,
            ],
            2 => [
                'structure_id' => 1,
                'typesitting_id' => self::BUREAU_STRUCTURE_1,
                'date' => 1549880906,
                'date_convocation' => 1549880906,
                'created' => 1550759640,
                'modified' => 1550759640,
                'place' => null,
            ],
            3 => [
                'structure_id' => 1,
                'typesitting_id' => self::CONSEIL_MUNICIPAL_STUCTURE_1,
                'date' => 1549810906,
                'date_convocation' => 1549810906,
                'created' => 1550159640,
                'modified' => 1551759640,
                'president_id' => 1,
                'secretary_id' => 1,
                'place' => 'Sale du conseil municipal de la mairie de Libriciel SCOP à Castelnau-le-Lez',
            ],
            4 => [
                'structure_id' => 1,
                'typesitting_id' => self::BUREAU_STRUCTURE_1,
                'date' => 1549810906,
                'date_convocation' => 1549810906,
                'created' => 1550159640,
                'modified' => 1551759640,
                'place' => null,
            ],
            5 => [
                'structure_id' => 1,
                'typesitting_id' => self::COMMISSION_STRUCTURE_1,
                'date' => 1549810906,
                'date_convocation' => 1549810906,
                'created' => 1550159640,
                'modified' => 1551759640,
                'place' => null,
            ],
            6 => [
                'structure_id' => 1,
                'typesitting_id' => self::BUREAU_STRUCTURE_1,
                'date' => '9999-12-10 15:00:00',
                'date_convocation' => '2019-12-10 15:00:00',
                'created' => 1550159640,
                'modified' => 1551759640,
                'place' => null,
            ],
            7 => [
                'structure_id' => 1,
                'typesitting_id' => self::BUREAU_STRUCTURE_1,
                'date' => '9999-02-15 10:00:00',
                'date_convocation' => '2020-02-15 10:00:00',
                'created' => 1550159640,
                'modified' => 1551759640,
                'place' => null,
            ],
            8 => [
                'structure_id' => 2,
                'typesitting_id' => self::BUREAU_STRUCTURE_2,
                'date' => 1550759640,
                'date_convocation' => 1550759640,
                'created' => 1550759640,
                'modified' => 1550759640,
                'place' => null,
            ],
            9 => [
                'structure_id' => 2,
                'typesitting_id' => self::BUREAU_STRUCTURE_2,
                'date' => 1550759641,
                'date_convocation' => 1550759640,
                'created' => 1550759640,
                'modified' => 1550759640,
                'place' => null,
            ],
        ];
        parent::init();
    }
}
