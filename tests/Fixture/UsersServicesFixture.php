<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersServicesFixture
 */
class UsersServicesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'user_id' => 1,
                'service_id' => 1,
            ],
            [
                'user_id' => 2,
                'service_id' => 1,
            ],
            [
                'user_id' => 3,
                'service_id' => 1,
            ],
            [
                'user_id' => 4,
                'service_id' => 1,
            ],
            [
                'user_id' => 5,
                'service_id' => 1,
            ],
            [
                'user_id' => 6,
                'service_id' => 1,
            ],
            [
                'user_id' => 7,
                'service_id' => 1,
            ],
            [
                'user_id' => 8,
                'service_id' => 4,
            ],
            [
                'user_id' => 9,
                'service_id' => 4,
            ],
            [
                'user_id' => 10,
                'service_id' => 4,
            ],
        ];
        parent::init();
    }
}
