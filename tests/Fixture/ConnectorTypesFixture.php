<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConnectorTypesFixture
 */
class ConnectorTypesFixture extends TestFixture
{
    public $records = [
        [
            'name' => 'PASTELL',
            'code' => 'pastell',
            'description' => 'Pastell',
        ],
        [
            'name' => 'IdélibRE',
            'code' => 'idelibre',
            'description' => 'IdélibRE',
        ],
    ];
}
