<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SummonTypesFixture
 */
class SummonTypesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'name' => 'Pastell',
                'code' => 'pastell',
                'description' => '',
            ],
            [
                'name' => 'IdélibRE',
                'code' => 'idelibre',
                'description' => '',
            ],
        ];
        parent::init();
    }
}
