<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * VotesFixture
 */
class VotesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'president_id' => 1,
                'project_id' => 1,
                'structure_id' => 1,
                'yes_counter' => 1,
                'no_counter' => 1,
                'abstentions_counter' => 1,
                'no_words_counter' => 1,
                'prendre_acte' => 1,
                'resultat' => 1,
                'comment' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'created' => 1621513748,
                'modified' => 1621513748,
                'method' => 'details',
            ],
            [
                'president_id' => 1,
                'project_id' => 1,
                'structure_id' => 1,
                'yes_counter' => 1,
                'no_counter' => 1,
                'abstentions_counter' => 1,
                'no_words_counter' => 1,
                'prendre_acte' => 1,
                'resultat' => 1,
                'comment' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'created' => 1621513749,
                'modified' => 1621513749,
                'method' => 'details',
            ],
            [
                'president_id' => 1,
                'project_id' => 1,
                'structure_id' => 1,
                'yes_counter' => 1,
                'no_counter' => 1,
                'abstentions_counter' => 1,
                'no_words_counter' => 1,
                'prendre_acte' => 1,
                'resultat' => 1,
                'comment' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'created' => 1621513750,
                'modified' => 1621513750,
                'method' => 'details',
            ],
            [
                'president_id' => 1,
                'project_id' => 7,
                'structure_id' => 1,
                'yes_counter' => 1,
                'no_counter' => 1,
                'abstentions_counter' => 1,
                'no_words_counter' => 1,
                'prendre_acte' => 1,
                'resultat' => 1,
                'comment' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'created' => 1621513751,
                'modified' => 1621513751,
                'method' => 'details',
            ],
        ];
        parent::init();
    }
}
