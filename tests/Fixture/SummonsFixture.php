<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SummonsFixture
 */
class SummonsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'summon_type_id' => 1,
                'structure_id' => 1,
                'sitting_id' => 1,
                'date_send' => '2021-04-08',
                'is_sent_individually' => 1,
                'created' => 1617921923,
                'modified' => 1617921923,
            ],
        ];
        parent::init();
    }
}
