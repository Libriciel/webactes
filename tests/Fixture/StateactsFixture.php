<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * StateactsFixture
 */
class StateactsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [ // 1
                'name' => 'Brouillon',
                'code' => 'brouillon',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 2
                'name' => 'En cours de validation',
                'code' => 'en-cours-de-validation',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 3
                'name' => 'Validé',
                'code' => 'valide',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 4
                'name' => 'Refusé',
                'code' => 'refuse',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 5
                'name' => 'Déclaré signé',
                'code' => 'declare-signe',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 6
                'name' => 'Signé par le i-Parapheur',
                'code' => 'signe-i-parapheur',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 7
                'name' => 'Refusé par le i-Parapheur',
                'code' => 'refuse-i-parapheur',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 8
                'name' => 'En cours de signature i-Parapheur',
                'code' => 'en-cours-signature-i-parapheur',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 9
                'name' => 'À télétransmettre',
                'code' => 'a-teletransmettre',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 10
                'name' => 'En cours de télétransmission',
                'code' => 'en-cours-teletransmission',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 11
                'name' => 'Acquitté',
                'code' => 'acquitte',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 12
                'name' => 'Annulé',
                'code' => 'annule',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 13
                'name' => 'En erreur sur le TdT',
                'code' => 'erreur-tdt',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 14
                'name' => 'En cours dépot tdt',
                'code' => 'en-cours-depot-tdt',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 15
                'name' => 'À ordonner',
                'code' => 'a-ordonner',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 16
                'name' => 'Annulation en cours',
                'code' => 'annulation-en-cours',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 17
                'name' => 'En cours d\'envoi i-parapheur',
                'code' => 'en-cours-envoi-i-parapheur',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 18
                'name' => 'Approuvé',
                'code' => 'vote-et-approuve',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 19
                'name' => 'Rejeté',
                'code' => 'vote-et-rejete',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
            [ // 20
                'name' => 'Prendre acte',
                'code' => 'prendre-acte',
                'created' => 1550498279,
                'modified' => 1550498279,
            ],
        ];
        parent::init();
    }
}
