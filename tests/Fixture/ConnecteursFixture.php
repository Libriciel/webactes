<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConnecteursFixture
 */
class ConnecteursFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'name' => 'pastell',
                'url' => 'https://pastell.partenaire.libriciel.fr',
                'tdt_url' => 'https://s2low.formations.libriciel.fr/modules/actes/actes_transac_post_confirm_api.php',
                'username' => PASTELL_TEST_USER,
                'password' => PASTELL_TEST_PASSWORD,
                'structure_id' => 1,
                'connector_type_id' => 1,
            ],
            [
                'name' => 'pastell',
                'url' => 'https://pastell.partenaire.libriciel.fr',
                'tdt_url' => 'https://s2low.formations.libriciel.fr/modules/actes/actes_transac_post_confirm_api.php',
                'username' => PASTELL_TEST_USER,
                'password' => PASTELL_TEST_PASSWORD,
                'structure_id' => 2,
                'connector_type_id' => 1,
            ],
        ];
        parent::init();
    }
}
