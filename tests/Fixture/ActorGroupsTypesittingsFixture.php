<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ActorsProjectsSittingsFixture
 */
class ActorGroupsTypesittingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'actor_group_id' => 1,
                'typesitting_id' => 1,
            ],
        ];
        parent::init();
    }
}
