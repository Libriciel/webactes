<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OfficialsFixture
 */
class OfficialsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'structure_id' => 1,
                'civility' => 'M.',
                'lastname' => 'Patrick',
                'firstname' => 'ORANGE',
                'email' => 'p.orange@test.fr',
                'address' => '777 rue des orangeades',
                'address_supplement' => '',
                'post_code' => '34000',
                'city' => 'MONTPELLIER',
                'phone' => '0400000000',
                'created' => 1549984571,
                'modified' => 1549984571,
            ],
            [
                'structure_id' => 2,
                'civility' => 'M.',
                'lastname' => 'Patrick2',
                'firstname' => 'ORANGE2',
                'email' => 'p.orange2@test.fr',
                'address' => '777 rue des orangeades2',
                'address_supplement' => '',
                'post_code' => '34000',
                'city' => 'MONTPELLIER',
                'phone' => '0400000002',
                'created' => 1549984571,
                'modified' => 1549984571,
            ],
        ];
        parent::init();
    }
}
