<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersInstancesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            // Projet 3
            1 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0003-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            2 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0003-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            3 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0003-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 4
            4 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0004-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            5 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0004-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            6 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0004-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 5
            7 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0005-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            8 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0005-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            9 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0005-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 6
            10 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0006-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            11 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0006-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 7
            12 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0007-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            13 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0007-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            14 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0007-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 8
            15 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0008-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            16 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0008-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            17 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0008-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 9
            18 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0009-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            19 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0009-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            20 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0009-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 10
            21 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0010-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            22 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0010-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 11
            23 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0011-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            24 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0011-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            25 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0011-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 12
            26 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0012-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            27 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0012-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 13
            28 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0013-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            29 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0013-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            30 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0013-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 14
            31 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0014-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            32 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0014-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 15
            33 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0015-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            34 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0015-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            35 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0015-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 16
            36 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0016-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            37 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0016-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 17
            38 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0017-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            39 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0017-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            40 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0017-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 18
            41 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0018-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            42 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0018-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 21
            43 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0021-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            44 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0021-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            45 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0021-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            // Projet 22
            46 => [
                'user_id' => 4,
                'id_flowable_instance' => 'projects-0022-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            47 => [
                'user_id' => 5,
                'id_flowable_instance' => 'projects-0022-aaaa-aaaa-aaaaaaaaaaaa',
            ],
            48 => [
                'user_id' => 6,
                'id_flowable_instance' => 'projects-0022-aaaa-aaaa-aaaaaaaaaaaa',
            ],
        ];
        parent::init();
    }
}
