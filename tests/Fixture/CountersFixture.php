<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CountersFixture
 */
class CountersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [ // id : 1
                'structure_id' => 1,
                'name' => 'Compteurs délib',
                'comment' => 'Un système de compteur pour les délibérations',
                'counter_def' => '#AAAA#_#0000#',
                'sequence_id' => 4,
                'reinit_def' => '#AAAA#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 2
                'structure_id' => 1,
                'name' => 'Compteurs de commissions',
                'comment' => 'Le compteur des commissions',
                'counter_def' => 'COM_#0000#',
                'sequence_id' => 2,
                'reinit_def' => '#JJ#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 3
                'structure_id' => 1,
                'name' => 'Compteurs des arrêtés',
                'comment' => 'Le compteur pour les arrêtés',
                'counter_def' => 'DELIB#AAAA##MM##JJ#_#S#',
                'sequence_id' => 1,
                'reinit_def' => '#AAAA#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 4
                'structure_id' => 1,
                'name' => 'Compteurs des conventions',
                'comment' => 'Les conventions ont un système de compteur',
                'counter_def' => 'DELIB#AAAA##MM##JJ#_#S#',
                'sequence_id' => 3,
                'reinit_def' => '#MM#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 5
                'structure_id' => 1,
                'name' => 'Compteur autres',
                'comment' => 'Les délib autres ont un système de compteur',
                'counter_def' => 'D#AAAA##MM##JJ#_#S#',
                'sequence_id' => 2,
                'reinit_def' => '#AAAA#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 6
                'structure_id' => 1,
                'name' => 'Compteur 1 qui partage la séquence 6',
                'comment' => 'Séquence partagée avec autre compteur',
                'counter_def' => 'D#AAAA##MM##JJ#_#S#',
                'sequence_id' => 6,
                'reinit_def' => '#AAAA#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 7
                'structure_id' => 1,
                'name' => 'Compteur 2 qui partage la séquence 6',
                'comment' => 'Séquence partagée avec autre compteur',
                'counter_def' => 'D#AAAA##MM##JJ#_#S#',
                'sequence_id' => 6,
                'reinit_def' => '#AAAA#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 8
                'structure_id' => 1,
                'name' => 'Compteur mois n°1 qui partage la séquence 7',
                'comment' => '',
                'counter_def' => '#AAAA#_#0000#',
                'sequence_id' => 7,
                'reinit_def' => '#MM#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 9
                'structure_id' => 1,
                'name' => 'Compteur mois n°2 qui partage la séquence 7',
                'comment' => '',
                'counter_def' => 'D#AAAA##MM##JJ#_#S#',
                'sequence_id' => 7,
                'reinit_def' => '#MM#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 10
                'structure_id' => 1,
                'name' => 'Compteur jour n°1 qui partage la séquence 8',
                'comment' => '',
                'counter_def' => '#AAAA#_#0000#',
                'sequence_id' => 8,
                'reinit_def' => '#JJ#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 11
                'structure_id' => 1,
                'name' => 'Compteur jour n°2 qui partage la séquence 8',
                'comment' => 'Séquence partagée avec autre compteur',
                'counter_def' => 'D#AAAA##MM##JJ#_#S#',
                'sequence_id' => 8,
                'reinit_def' => '#JJ#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 12
                'structure_id' => 2,
                'name' => 'Compteurs délib',
                'comment' => 'Un système de compteur pour les délibérations',
                'counter_def' => '#AAAA#_#0000#',
                'sequence_id' => 4,
                'reinit_def' => '#AAAA#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
            [ // id : 13
                'structure_id' => 2,
                'name' => 'Compteurs de commissions',
                'comment' => 'Le compteur des commissions',
                'counter_def' => 'COM_#0000#',
                'sequence_id' => 2,
                'reinit_def' => '#JJ#',
                'created' => 1606513539,
                'modified' => 1606513539,
            ],
        ];
        parent::init();
    }
}
