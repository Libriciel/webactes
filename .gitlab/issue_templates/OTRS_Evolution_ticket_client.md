<details>
<summary>Sommaire</summary>

[[_TOC_]]
</details>

# Demande de fonctionnalité
Suggérer une idée pour web-delib. Si cela ne semble pas correct, choisissez un autre template.
### Problème à résoudre
<!-- Une description claire et concise de la nature du problème. Ex. Je suis toujours frustré quand [...] -->

## Listing des tickets OTRS ayant le même problème
- Ville [Ticket](lien vers le ticket)

### Checklist produit
**Avant T0**
- [ ] Toutes les demandes sont qualifiées par l'équipe Product Owner ~"Roadmap::A qualifier"
- [ ] Toutes les demandes sont par l'équipe projet ~"Roadmap::A étudier"
- [ ] Toutes les demandes sont mise en attente par l'équipe projet ~"Roadmap::Backlog"

**Après développement**
- [ ] Requalifier le ticket OTRS

/label ~"Type:Evolution" ~"Origine: Ticket client"  ~"Priorité::Basse" ~"Roadmap::A qualifier"
/cc @mhuetter @sbernard @mpastor @vbabot @splaza @dlolio @rbarlet @spicard @dchantalou @cbuffin @pmathieu
/assign @splaza
