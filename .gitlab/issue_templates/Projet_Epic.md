<details>
<summary>Sommaire</summary>

[[_TOC_]]
</details>

## Demande de fonctionnalité
<!-- Une epic est un travail important qui peut être découpé en un certain nombre de petites storie ou sub-Epic. Si cela ne semble pas correct, choisissez un autre template. -->
### Problème à résoudre
<!-- Une description claire et concise de la nature du problème. Ex. Je suis toujours frustré quand [...] -->

## Listing des demandes clientes
<!-- liens vers les problèmes sur le sujet (ou le ticket otrs directement à éviter)
- Ville [Ticket](lien vers le ticket)
-->

### Checklist produit
**Avant T0**
- [ ] Toutes les demandes sont qualifiées par l'équipe Product Owner ~"Roadmap::A qualifier"
- [ ] Toutes les demandes sont par l'équipe projet ~"Roadmap::A étudier"
- [ ] Toutes les demandes sont mise en attente par l'équipe projet ~"Roadmap::Backlog"
- [ ] Toutes les demandes sont proposée par l'équipe projet ~"Roadmap::N+1"

**Avant mise en design**
- [ ] Toutes les demandes sont étudiées par l'équipe Product Owner ~"Métier::Prêt pour le dev"
- [ ] Toutes les demandes sont validées par l'équipe projet ~"Design::A maquetter"

**Avant mise en développement**
- [ ] validation de la maquette par l'équipe projet ~"Design::Test OK"
- [ ] Ajouter le temps estimé `/estimate`
- [ ] Planifier la demande ~"Dev::A Faire"

**Après développement**
- [ ] Clôturer le temps réel `/spend`
- [ ] Vérifier que toutes les demandes soient mise en place dans le ChangeLog

**Avant mise en recette continue**
- [ ] Vérifier que toutes les rétrospectives ont présentées à l'équipe projet
- [ ] Toutes les demandes sont validées par l'équipe projet ~"Recette::A tester"

/label ~"Agile: Epic" ~"Origine: Dev" ~"Priorité::Haute"
/cc @mhuetter @sbernard @mpastor @vbabot @splaza @dlolio @rbarlet @spicard @dchantalou @cbuffin @pmathieu
/assign @splaza



