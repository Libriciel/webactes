<details>
<summary>Sommaire</summary>

[[_TOC_]]
</details>

## Demande de correction
### Problème à résoudre
<!-- Une description claire et concise de la nature du problème. Ex. Je suis bloqué [...] -->

## Erreur

### Url(s)

- https://webactes.recette.libriciel.net/administration/utilisateurs

### Liste des actions
<!-- Lister ici les actions permettant de reproduire le bug -->

*En tant que [ARTHUR](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/wikis/personae#administrateur-mutualisant-de-groupe-gere-les-tickets-n1)* ...   
*Je ne veux pas* ...    
*Afin de* visualiser ...

### Copie d'écran :
<!-- Lister ici le(s) message(s) d'erreur(s) afficher à l'utilisateur, si possible mettre un imprime écran ou des logs -->

### Message d'erreur
<!-- Indiquer le message url d'erreurs. -->

### Remarques
<!-- Remarques ou les solutions ou fonctionnalités alternatives que vous avez envisagées. -->

### Contexte supplémentaires
<!-- Ajoutez tout autre contexte ou capture d'écran sur la demande de fonctionnalité ici. -->

## Résolution du problème

## Checklist produit

**Avant mise en développement**
- [ ] Planifier la demande ~"Dev::A Faire"

### Checklist de développement
*Le développeur doit ajouter la progression à chaque étape importante du développement*
**Avant une demande de Merge Request**
- [ ] Clôturer le temps réel `/spend`
- [ ] Vérifier que la demande soit mise en place dans le ChangeLog

**Après une demande de Merge Request**
- [ ] Faire une rétrospective avec l'équipe projet
- [ ] La correction doit avoir le label ~"Recette::A tester"

**Après développement**
- [ ] Requalifier le ticket OTRS

## Tickets OTRS

- Ville [Ticket](lien vers le ticket)

/label ~"Type:Evolution" ~"Origine: Ticket client"  ~"Priorité::Basse" ~"Roadmap::A qualifier"
/cc @splaza @mpastor @vbabot @rbarlet @sbernard
/assign @splaza