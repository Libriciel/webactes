<details>
<summary>Sommaire</summary>

[[_TOC_]]
</details>

## Demande de fonctionnalité
<!-- Suggérer une idée pour web-delib. Si cela ne semble pas correct, choisissez un autre template. -->
### Problème à résoudre
<!-- Une description claire et concise de la nature du problème. Ex. Je suis toujours frustré quand [...] -->

*En tant que [ARTHUR](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/wikis/personae#administrateur-mutualisant-de-groupe-gere-les-tickets-n1)* ...   
*Je veux* ...    
*Afin de* visualiser ...

### Remarques
<!-- Remarques ou les solutions ou fonctionnalités alternatives que vous avez envisagées. -->

### Contexte supplémentaires
<!-- Ajoutez tout autre contexte ou capture d'écran sur la demande de fonctionnalité ici. -->

## Résolution du problème
### Solution sélectionnée
<!-- Une description claire et détaillé (règles métier) de ce qu'il doit se passer -->
 
### Paramétrage
<!-- Une description du paramétrage -->

### Critères d'acceptation

<!--  liens vers les critères d'acceptation) -->
https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/webACTES/-/blob/201-creer-modifier-un-groupe-d-acteur/features/admin/exemple.feature

### Proposition de design
<!-- (Exemple d'action pour aider le design pour les propositions envisagées)
- Bouton d'action depuis ....
 -->

## Checklist produit
**Avant mise en design**

- [ ] La demande est suffisamment définie par l'équipe Product Owner ~"Métier::Prêt pour le dev"
- [ ] La demande est validée par l'équipe projet ~"Design::A maquetter"

**Avant mise en développement**
- [ ] validation de la maquette par l'équipe projet ~"Design::Test OK"
- [ ] Ajouter le temps estimé `/estimate`
- [ ] Planifier la demande ~"Dev::A Faire"

## Développement
*Le développeur peut indiquer un statut bloqué si celui trouve une incohérence dans la solution proposé.*
### Bloque un autre problème "blocks" (Blocking another issue)
<!-- liens vers les problèmes
- Add new feature part 3 #35
-->

### Bloqué par un autre problème "is blocked by" (Blocked by another issue)
<!-- liens vers les problèmes
- Add new feature part 1 #33
-->

### Problème à résoudre "Relates to" (Related to another issue)
<!-- liens vers les problèmes
- Delete old documentation #32
- Add documentation for new feature #31
-->
- [Voir les problèmes liées](#related-issues)

### Checklist de développement
*Le développeur doit ajouter la progression à chaque étape importante du développement*
**Avant une demande de Merge Request**
- [ ] Clôturer le temps réel `/spend`
- [ ] Vérifier que la demande soit mise en place dans le ChangeLog

**Après une demande de Merge Request**
- [ ] Faire une rétrospective avec l'équipe projet
- [ ] Toutes les demandes sont validées par l'équipe projet ~"Recette::A tester"

/label ~"Agile : User Story"  ~"Métier:Infos à fournir"
/cc @mhuetter @sbernard @mpastor @vbabot @splaza @cbuffin
/assign @splaza 