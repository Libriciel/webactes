<?php
try {
    $valuesJson = json_encode($dataForView);
} catch (Exception $e) {
    $error = [
        'status' => 500,
        'title' => '$e->getMessage()'
    ];
    echo json_encode($error);
}

echo $valuesJson;
