<?=
__('Bonjour ') . h($userName) . h(',')
?>
<br/><br/>
<?= "Le projet d'acte " . h($libelle) . '  que vous avez créé a été validé et est en attente de signature. ' ?>
<br/><br/>
<?=
h('Vous pouvez le visualiser en suivant ce lien ') . $this->Html->link(
    $projectUrl,
    $projectUrl
) ?>
<br/><br/>
<?=
h('Très cordialement') ?>
<br/><br/>
<?=
h('webactes');
?>
