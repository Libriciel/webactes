<?=
__("Bonjour ") . h($userName) . h(",")
?>
<br/><br/>
<?=
h("Le projet d'acte ") . h($libelle)
?>
<?=
($isMine == true) ? h(" que vous avez créé") : h("")
?>

<?=
h(" a été signé et est en attente de dépôt sur le TdT.")
?>
<br/><br/>
<?=
h("Vous pouvez le visualiser en suivant ce lien ") . $this->Html->link(
    $projectUrl,
    $projectUrl) ?>
<br/><br/>
<?=
h("Très cordialement") ?>
<br/><br/>
<?=
h("webactes");
?>
