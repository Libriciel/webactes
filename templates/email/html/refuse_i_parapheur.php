<?=
__("Bonjour ") . h($userName) . h(",")
?>
<br/><br/>
<?= "Le projet d'acte " . h($libelle) . " est en attente de validation à votre étape." ?>
<br/><br/>
<?= $commentaire != '' ? h("le commentaire suivant a été déposé par ") . h($backUserName) . ' : ' . h($commentaire) : '' ?>
<br/><br/>
<?=
h("Vous pouvez le traiter en suivant ce lien ") . ($this->Html->link(
    $projectUrl,
    $projectUrl)) ?>
<br/><br/>
<?=
h("Très cordialement") ?>
<br/><br/>
<?=
h("webactes");
?>
