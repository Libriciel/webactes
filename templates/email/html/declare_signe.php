<?=
__("Bonjour ") . h($userName) . h(",")
?>
<br/><br/>
<?=
"Le projet d'acte " . h($libelle)
?>
<?=
($isMine == true) ? h(" que vous avez créé") : h("")
?>
<?=
" a été signé."
?>
<br/><br/>
<?=
h("Vous pouvez le visualiser en suivant ce lien ") . $this->Html->link(
    $projectUrl,
    $projectUrl) ?>
<br/><br/>
<?=
h("Très cordialement") ?>
<br/><br/>
<?=
h("webactes");
?>
