<?=
__("Bonjour ") . h($userName) . h(",")
?>
<br/><br/>
<?= "Le projet d'acte " . h($libelle) . "  que vous avez créé a été refusé par " . h($backUserName) ?>
<br/><br/>
<?= $commentaire != '' ? h("le commentaire suivant a été déposé par ") . h($backUserName) . ' : ' . h($commentaire) : '' ?>
<?=
h("Vous pouvez le visualiser en suivant ce lien ") . ($this->Html->link(
    $projectUrl,
    $projectUrl)) ?>
<br/><br/>
<?=
h("Très cordialement") ?>
<br/><br/>
<?=
h("webactes");
?>
