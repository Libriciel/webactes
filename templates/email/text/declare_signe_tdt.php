<?=
__("Bonjour ") . h($userName) . h(",\n\n")
?>

<?=
h("Le projet d'acte ") . h($libelle)
?>
<?=
($isMine == true) ? h(" que vous avez créé") : h("")
?>

<?=
h(" a été signé et est en attente de dépôt sur le TdT.") . h("\n\n")
?>
<?=
h("Vous pouvez le visualiser en cliquant  ") . $this->Html->link(
    'ici',
    $projectUrl) . h("\n\n") .

h("\nTrès cordialement.\n

webactes");

?>
