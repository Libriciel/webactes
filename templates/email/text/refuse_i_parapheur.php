<?=
__("Bonjour ") . h($userName) . h(",\n\n")
?>

<?= "Le projet d'acte " . h($libelle) . " est en attente de validation à votre étape." . h("\n\n") ?>
<?= $commentaire != '' ? h("le commentaire suivant a été déposé par ") . h($backUserName) . ' : ' . h($commentaire) . h("\n\n") : '' ?>
<?=
h("Vous pouvez le traiter en suivant ce lien ") . h($projectUrl) . h("\n\n") .

h("\nTrès cordialement.\n

webactes");

?>
