<?=
__("Bonjour ") . h($userName) . h(",\n\n")
?>

<?=
"Le projet d'acte " . h($libelle)
?>
<?=
($isMine == true) ? h(" que vous avez créé") : h("")
?>
<?=
" a été signé." . h("\n\n")
?>
<?=
h("Vous pouvez le visualiser en cliquant  ") . h($this->Html->link(
    'ici',
    $projectUrl)) . h("\n\n") .

h("\nTrès cordialement.\n

webactes");

?>
