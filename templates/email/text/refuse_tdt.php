<?=
__("Bonjour ") . h($userName) . h(",\n\n")
?>

<?= "Le projet d'acte " . h($libelle) . "  que vous avez créé a été refusé par " . h($backUserName) . h("\n\n") ?>
<?= $commentaire != '' ? h("le commentaire suivant a été déposé par ") . h($backUserName) . ' : ' . h($commentaire) . h("\n\n") : '' ?>
<?=
h("Vous pouvez le visualiser en suivant ce lien ") . h($projectUrl) . h("\n\n") .

h("\nTrès cordialement.\n

webactes");

?>
