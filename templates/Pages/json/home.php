<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$message = ["message" => "Bienvenue sur webACTES"];

if (!Configure::read('debug')) :
    $message = ["message" => "Bienvenue sur webACTES"];
    echo json_encode($message);
    return 1;
endif;

$message["erreur"] = false;
$message["messagesErreur"] = "";

$message["version"] = Configure::version();

$message["Environment"]["PHP_VERSION"] = PHP_VERSION;
if (version_compare(PHP_VERSION, '5.6.0', '<')) {
    $message["erreur"] = true;
    $message["messagesErreur"] += "Your version of PHP is too low. You need PHP 5.6.0 or higher to use CakePHP (detected <?= PHP_VERSION ?>";
}

$extensions = ['mbstring', 'openssl', 'mcrypt', 'intl'];
$unloadedExtensions = [];
foreach ($extensions as $extension) {
    $message["Environment"][$extension] = extension_loaded($extension);
    if (!extension_loaded($extension)) {
        $unloadedExtensions[] = $extension;
    }
}
if (!empty($unloadedExtensions)) {
    $message["erreur"] = true;
    $message["messagesErreur"] += "Extensions non chargees : " + implode(" ,", $unloadedExtensions);
}
$message["Environment"]["mbstring"] = extension_loaded('mbstring');

$message["Filesystem"]["writeable_tmp"] = is_writable(TMP);
if (!is_writable(TMP)) {
    $message["erreur"] = true;
    $message["messagesErreur"] += "Your tmp directory is NOT writable.";
}
$message["Filesystem"]["writeable_logs"] = is_writable(LOGS);
if (!is_writable(LOGS)) {
    $message["erreur"] = true;
    $message["messagesErreur"] += "Your logs directory is NOT writable.";
}

$settings = Cache::getConfig('_cake_core_');
if (!empty($settings)) {
    $message["Filesystem"]["cache_settings"] = $settings['className'];
} else {
    $message["erreur"] = true;
    $message["messagesErreur"] += "Your cache is NOT working.";
}

try {
    $connection = ConnectionManager::get('default');
    $connected = $connection->connect();
} catch (Exception $connectionError) {
    $connected = false;
    $errorMsg = $connectionError->getMessage();
    if (method_exists($connectionError, 'getAttributes')) :
        $attributes = $connectionError->getAttributes();
        if (isset($errorMsg['message'])) :
            $errorMsg .= '<br />' . $attributes['message'];
        endif;
    endif;
}

if ($connected) {
    $message["Database"]["connection"] = "OK";
} else {
    $message["Database"]["connection"] = "KO";
    $message["erreur"] = true;
    $message["messagesErreur"] += "CakePHP is NOT able to connect to the database";
}

if (Plugin::isLoaded('DebugKit')) {
    $message["DebugKit"]["loaded"] = "OK";
} else {
    $message["DebugKit"]["loaded"] = "KO";
}

echo json_encode($message);
