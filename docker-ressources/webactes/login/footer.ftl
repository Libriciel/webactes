<footer>
    <div class="box">
        <span class="products">
            <a class="products-logo" title="En savoir plus sur le logiciel asalae" href="https://www.libriciel.fr/logiciels/asalae" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/as.svg" alt="asalae-logo"/>
            </a>
            <a class="products-logo" title="En savoir plus sur le logiciel comélus" href="https://www.libriciel.fr/logiciels/comelus" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/ce.svg" alt="comelus-logo"/>
            </a>
            <a class="products-logo" title="En savoir plus sur le logiciel idelibre" href="https://www.libriciel.fr/logiciels/idelibre" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/id.svg" alt="idelibre-logo"/>
            </a>
            <a class="products-logo" title="En savoir plus sur le logiciel iparapheur" href="https://www.libriciel.fr/logiciels/iparapheur" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/ip.svg" alt="iparapheur-logo"/>
            </a>
            <a class="products-logo" title="En savoir plus sur le logiciel pastell" href="https://www.libriciel.fr/logiciels/pastell" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/pa.svg" alt="pastell-logo"/>
            </a>
            <a class="products-logo" title="En savoir plus sur le logiciel refae" href="https://www.libriciel.fr/logiciels/refae" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/re.svg" alt="refae-logo"/>
            </a>
            <a class="products-logo" title="En savoir plus sur le logiciel S²LOW" href="https://www.libriciel.fr/logiciels/slow" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/sl.svg" alt="s2low-logo"/>
            </a>
            <a class="products-logo" title="En savoir plus sur le logiciel versae" href="https://www.libriciel.fr/logiciels/versae" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/ve.svg" alt="versae-logo"/>
            </a>
            <a class="products-logo" title="En savoir plus sur le logiciel webactes" href="https://www.libriciel.fr/logiciels/webactes" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/wa.svg" alt="webactes-logo"/>
            </a>
            <a class="products-logo" title="En savoir plus sur le logiciel webdelib" href="https://www.libriciel.fr/logiciels/webdelib" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/wd.svg" alt="webdelib-logo"/>
            </a>
            <a class="products-logo" title="En savoir plus sur le logiciel webgfc" href="https://www.libriciel.fr/logiciels/webgfc" target="_blank">
                <img height="22" src="${url.resourcesPath}/images/wg.svg" alt="webgfc-logo"/>
            </a>
        </span>
    </div>
    <div class="copyright box">
        <span>
            © Libriciel SCOP 2006-<script>document.write(new Date().getFullYear())</script>
        </span>
    </div>
    <div class="box">
        <span>
            <a class="products-logo" title="Site internet de Libriciel SCOP" href="https://www.libriciel.fr" target="_blank">
                <img height="19" src="${url.resourcesPath}/images/libriciel-light.svg" alt="libriciel-logo">
            </a>
        </span>
    </div>
</footer>
