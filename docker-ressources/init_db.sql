-- Créer la base de données pour Flowable
CREATE DATABASE flowable OWNER webactes;

-- Créer la base de données pour Keycloak
CREATE DATABASE keycloak OWNER webactes;
