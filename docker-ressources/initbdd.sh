#!/bin/bash
# initbdd.sh

__SCRIPT__=`basename $0`
__DIR__=`dirname $0`

#FUNCTIONS
#
usage(){
  echo "DESCRIPTION"
  echo "    Script de démarrage de l'application webACTES (docker)"
  echo "    Certaines parties sont facultatives et donc non exécutées, voir les options ci-dessous pour les exécuter"
  echo "SYNOPSIS"
  echo "    $__SCRIPT__ [OPTION]"
  echo "OPTIONS"
  echo "    -c | --composer-install Installation des dépendences avec composer"
  echo "                            (double installation, voir ${__DIR__}/composer-install.sh)"
  echo "                            Utilisé ici pour faciliter l'installation ou la mise à jour sur le poste de développement"
  echo "    --deploy-flowable       Initialisation de Flowable (voir bin/cake flowable_deployments init)"
  echo "    -h | --help             Affiche cette aide"
  echo "    -r | --razBdd           (déprécié) Suppression et re-création de la base de données"
  echo "    -s | --start            Démarrage des services (apache, cron, ...) via supervisord"
  echo "    -u | --useMain_URL      Modification de l'URL de KeyCloak dans les fichiers config/KeycloakConfig/keycloak*.json"
  echo "EXEMPLES"
  echo "    $__SCRIPT__ -h"
  echo "    $__SCRIPT__ --composer-install --deploy-flowable --start --useMain_URL"
}

wait_for_postgres()
{
  local hostname="${1}"
  local database="${2}"
  local username="${3}"
  local password="${4}"

  (
    set +o errexit
    status="1"

    echo "Trying to reach postgresql database ${database} on ${hostname}" >&2
    while [ $status != "0" ]
    do
      sleep 1
      psql postgres://${username}:${password}@${hostname}/${database} -c "\q" > /dev/null 2>&1
      status=$?
#      echo "-> status ${status}"
    done
  )

  echo "BDD ${database} correctement activee sur ${hostname}" >&2
}

wait_for_postgres_data_host()
{
  wait_for_postgres "${POSTGRES_DATA_HOST}" "${POSTGRES_DB}" "${POSTGRES_USER}" "${POSTGRES_PASSWORD}"
}

wait_for_postgres_flowable_host()
{
  wait_for_postgres "${DB_FLOWABLE_HOST}" "${DB_FLOWABLE_DATABASE}" "${DB_FLOWABLE_USER}" "${POSTGRES_FLOWABLE_PASSWORD}"
}

wait_for_postgres_keycloak_host()
{
  # @todo: import env
  wait_for_postgres "${DB_ADDR}" "${DB_DATABASE}" "${DB_USER}" "${DB_PASSWORD}"
}

createBddTest(){
  echo "------Creating test Database-------------------------------------------"
  psql postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_TEST_HOST -c "CREATE DATABASE ${POSTGRES_TEST_DB} OWNER ${POSTGRES_USER};"
  psql postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_TEST_HOST -c "ALTER DATABASE ${POSTGRES_TEST_DB} SET DateStyle=iso, dmy;"
}

# @deprecated
razBDD(){
  echo "------Resetting Database-----------------------------------------------"
  psql postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_DATA_HOST -c "DROP DATABASE ${POSTGRES_DB};"
  psql postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_DATA_HOST -c "CREATE DATABASE ${POSTGRES_DB} OWNER ${POSTGRES_USER};"

  if [ $? -eq 0 ]; then
      echo "Création BDD OK"
  else
      echo "Création BDD FAIL"
  fi

  psql postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_DATA_HOST -c "ALTER DATABASE ${POSTGRES_DB} SET DateStyle=iso, dmy;"

  bin/cake migrations migrate
  bin/cake migrations migrate --plugin WrapperFlowable

  if [ $? -eq 0 ]; then
      echo "Initialisation OK"
  else
      echo "Initialisation FAIL"
  fi
}

initBdd(){
    echo "------Migrating database-----------------------------------------------"
    psql postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_DATA_HOST -c "ALTER DATABASE ${POSTGRES_DB} SET DateStyle=iso, dmy;"
    psql postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_DATA_HOST -c "ALTER DATABASE ${POSTGRES_DB} owner to ${POSTGRES_USER};"
    bin/cake migrations migrate
    bin/cake migrations migrate --plugin Beanstalk
    bin/cake migrations migrate --plugin WrapperFlowable
    if [ "${PROD}" = true ] ; then
        bin/cake migrations seed --seed DatabaseProdSeed
    fi
    bin/cake trees_recovery
    bin/cake cache clear_all

    if [ $? -eq 0 ]; then
        echo "Migration OK"
    else
        echo "Migration FAIL"
    fi
}
##### MAIN

start=0
razBdd=0
useMain_URL=0
COMPOSER_INSTALL=0
DEPLOY_FLOWABLE=0

while [ "$1" != "" ]; do
    echo $1
    case $1 in
        -c | --composer-install )   COMPOSER_INSTALL=1
                                    ;;
        --deploy-flowable )         DEPLOY_FLOWABLE=1
                                    ;;
        -s | --start )              start=1
                                    ;;
        -r | --razBdd )             razBdd=1
                                    ;;
        -u | --useMain_URL )        useMain_URL=1
                                    ;;
        -h | --help )               usage
                                    exit
                                    ;;
        * )                         >&2 usage
                                    >&2 echo "Option inconnue: $1"
                                    exit 1
                                    ;;
    esac
    shift
done

cd /var/www/webACTES

crontab ./docker-ressources/cron/worker

if [ "$COMPOSER_INSTALL" = "1" ] ; then
    # @info: utilisé ici pour faciliter l'installation ou la mise à jour sur le poste de développement
    ${__DIR__}/composer-install.sh
fi

# Wait for databases to be initialised
wait_for_postgres_data_host
#wait_for_postgres_flowable_host
#wait_for_postgres_keycloak_host

if [ "$razBdd" = "1" ] ; then
    razBDD
else
    initBdd
fi

if ! test -z "$POSTGRES_TEST_DB" ; then
    createBddTest
fi

if [ "$useMain_URL" = "1" ] ; then
    echo "URL Used by Keycloak : $MAIN_URL"
    sed -i.bak "s/webact.local/$MAIN_URL/" config/KeycloakConfig/keycloak*.json
fi

wait_for_url()
{
  local url="${1}"
  local expected="${2}"

  (
    set +o errexit
    status=""

    echo "Waiting for URL ${url} to be accessible (code ${expected})..."
    while [ "${status}" != "$expected" ]
    do
      sleep 1
      status="`curl -s -o /dev/null -w "%{http_code}" --connect-timeout 1 -m 1 "${url}"`"
    done
  )

  echo "Success: URL ${url} is accessible (code ${expected})"
}

if [ "$DEPLOY_FLOWABLE" = "1" ] ; then
    wait_for_url "http://$FLOWABLE_HOST/flowable-rest/service/runtime/tasks" "401"
    bin/cake flowable_deployments init --host $FLOWABLE_HOST --username $FLOWABLE_ADMIN --password $FLOWABLE_PASSWORD
fi

mkdir -p "/data/workspace"
chown -R www-data:www-data "/data/workspace";

if [ "$start" = "1" ]; then
    echo "put environment variables inside /etc/environment for cron jobs"
    printenv | grep --color=never -v '\(^#\|^[^=]\+=[^"].*\s\)' > /etc/environment

    echo "starting supervisord"
    exec supervisord -n -c /etc/supervisord.conf
fi
