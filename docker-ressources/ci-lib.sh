#!/usr/bin/env bash

#-----------------------------------------------------------------------------------------------------------------------
# Common code for the ci-*.sh shells (namely ci-schemas_diffs.sh)
#-----------------------------------------------------------------------------------------------------------------------

set -o errexit
set -o nounset

DEBUG=false

opts=`getopt --longoptions debug -- d "$@"` \
    || ( >&2 usage ; exit 1 )
eval set -- "$opts"
while true ; do
    case "$1" in
        -d|--debug)
            DEBUG=true
            shift
        ;;
        --)
            shift
            break
        ;;
    esac
done

if [ "${DEBUG}" == "true" ] ; then
    set -o xtrace
fi

__FILE__="`realpath "$0"`"
__SCRIPT__="`basename "${__FILE__}"`"
__ROOT__="`dirname "${__FILE__}"`/.."
__ROOT__="`realpath "${__ROOT__}"`"

# @see https://stackoverflow.com/a/17841619
function implode
{
    local IFS="$1"
    shift
    echo "$*"
}
