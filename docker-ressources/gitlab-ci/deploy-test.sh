#!/usr/bin/env bash

set -o errexit
set -o nounset

docker login -u gitlab-ci-token -p '$CI_JOB_TOKEN' $CI_REGISTRY \
&& cd /home/gitlab/ \
&& export IMAGE_API_TEST=$IMAGE_API_TEST \
&& export IMAGE_CLIENT_TEST=$IMAGE_CLIENT_TEST \
&& export IMAGE_API_DEV=$IMAGE_API_DEV \
&& export IMAGE_CLIENT_DEV=$IMAGE_CLIENT_DEV \
&& export MAIN_URL=webactes.test.libriciel.fr \
&& docker-compose pull \
&& docker-compose up -f ./docker-compose-deploytest.yml -d webact-angular-test
