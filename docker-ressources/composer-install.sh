#!/bin/bash

export COMPOSER_ALLOW_SUPERUSER=1

# @info: cette "double" commande d'installation permet d'éviter l'arrêt de l'installation en plein milieu avec le message
# Running PHPCodeSniffer Composer Installer
# Failed to set PHP CodeSniffer installed_paths Config
composer install \
         --no-interaction \
         --no-plugins \
         --no-progress \
         --no-scripts \
         --verbose
composer install \
    --no-interaction \
    --no-progress \
    --verbose
