#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

if [ "`getopt --longoptions xtrace -- x "$@" 2> /dev/null | grep --color=none "\(^\|\s\)\(\-x\|\-\-xtrace\)\($\|\s\)"`" != "" ] ; then
    declare -r _XTRACE_=1
    set -o xtrace
else
    declare -r _XTRACE_=0
fi

declare -r _APT_DIR_="/var/lib/apt/lists/"
declare -r _FILE_="$(realpath "$0")"
declare -r _SCRIPT_="$(basename "${_FILE_}")"
declare _EXIT_=0

_apt-policy_()
{
    local package="$(echo "${1}" | sed 's/^\([^=]\+\)=.*$/\1/g' )"
    local policy="$(apt policy ${package} 2>/dev/null)"
    local version="$(echo "${policy}" | grep --color=none '^\s\+Candidat[^:]*:' | sed 's/^\s\+Candidat[^:]*:\s*\(\S*\)$/\1/g')"
    if [[ -z "${version}" ]] || [[ "${version}" == "(none)" ]] ; then
        >&2 echo "${package}="
        _EXIT_=2
    else
        echo "${package}=${version} \\"
    fi
}


_apt-policies_()
{
    local packages=$(echo "${@}" | sed 's/\s\+/\n/g' | sort -f -u )
    local package
    local version

    for package in ${packages} ; do
        _apt-policy_ "${package}"
    done
}

_help_()
{
    printf "NAME\n"
    printf "  %s\n" "${_SCRIPT_}"
    printf "\nDESCRIPTION\n"
    printf "  Get the current candidate version numbers for package names\n"
    printf "  Be sure to use apt update before running this script.\n"
    printf "\nSYNOPSIS\n"
    printf "  %s [OPTION]... [PACKAGE]...\n" "${_SCRIPT_}"
    printf "\nOPTIONS\n"
    printf "  -h|--help\t\tDisplay this help and exit\n"
    printf "  -x|--xtrace\t\tTraces commands before executing them (set -o xtrace)\n"
    printf "\nEXEMPLES\n"
    printf "  %s -h\n" "${_SCRIPT_}"
    printf "  %s postgresql-client-14 nano pdftk\n" "${_SCRIPT_}"
    printf "  %s postgresql-client-14=14.11* nano=6.2-* pdftk=2.*\n" "${_SCRIPT_}"
}

_main_()
{
    opts=$(getopt --longoptions help,xtrace --options hx -- "$@") || ( >&2 _help_ ; exit 1)
    eval set -- "$opts"
    while true; do
        case "${1}" in
            -h|--help)
                _help_
                exit 0
                ;;
            -x|--xtrace)
                shift
                ;;
            --)
                shift
                break
                ;;
        esac
    done

    found=$(find "${_APT_DIR_}" -type f | wc -l)
    if [[ ${found} -eq 0 ]] ; then
        >&2 _help_
        >&2 printf "\nNo file found in %s. You need to do apt update before using this script\n" "${_APT_DIR_}"
        exit 1
    else
        _apt-policies_ "${@}"
    fi
}

_main_ "${@}"
if [[ ${_EXIT_} -ne 0 ]] ; then
    >&2 printf "\nWarning: not all package versions could be found\n"
fi
exit ${_EXIT_}
