#!/usr/bin/env bash

source "`dirname "${0}"`/ci-lib.sh"

IGNORE_CONSTRAINTS="services_structure_id_fkey,structures_id_external,systemlogs_keycloak_id_fkey,users_id_external,projects_workflow_id_fkey,users_instances_id_flowable_instance_fkey"
IGNORE_TABLES="acos,aros,aros_acos,beanstalk_jobs,beanstalk_phinxlog,beanstalk_workers,phinxlog,wrapper_flowable_phinxlog,workflows"

(
    exit_code=0

    cd "${__ROOT__}"

    bin/cake SchemasDiffs classes \
        --ignore-constraints ${IGNORE_CONSTRAINTS} \
        --ignore-tables ${IGNORE_TABLES} \
    && exit_code=$exit_code || exit_code=$?

    bin/cake SchemasDiffs fixtures \
        --ignore-constraints ${IGNORE_CONSTRAINTS} \
        --ignore-tables ${IGNORE_TABLES} \
    && exit_code=$exit_code || exit_code=$?

    bin/cake SchemasDiffs rules \
        --ignore-constraints ${IGNORE_CONSTRAINTS} \
        --ignore-tables ${IGNORE_TABLES} \
    && exit_code=$exit_code || exit_code=$?

    exit $exit_code
)
